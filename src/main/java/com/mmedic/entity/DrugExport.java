package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Drug Export entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class DrugExport extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "export_from", nullable = false)
    private Establishment exportFrom;

    @OneToOne(mappedBy = "drugExport")
    private DrugInternalExport drugInternalExport;

    @OneToMany(mappedBy = "drugExport")
    private List<DrugExportDetail> drugExportDetails;

    @OneToOne(mappedBy = "drugExport")
    private DrugOrderExport drugOrderExport;

    @Column
    @LastModifiedDate
    private LocalDateTime createAt;
}
