package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * Variable Medical Info entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class VariableMedicalInfo extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String infoKey;

    @NotNull
    @Column(columnDefinition = "json", nullable = false)
    private String infoValue;

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String infoUnit;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
