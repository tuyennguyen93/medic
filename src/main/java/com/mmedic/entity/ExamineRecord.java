package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Examine Record entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class ExamineRecord extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "medical_record_id", nullable = false)
    private MedicalRecord medicalRecord;

    @Column(columnDefinition = "text")
    private String symptom;

    @Column(columnDefinition = "text")
    private String diagnose;

    @Column(columnDefinition = "text")
    private String remind;

    @Column(name = "skip_preclinical")
    private Boolean skipPreclinical;

    @OneToOne
    @JoinColumn(name = "medical_service_order_id", nullable = false)
    private MedicalServiceOrder medicalServiceOrder;

    @Column
    @CreatedDate
    private LocalDateTime createAt;

    @Column
    @LastModifiedDate
    private LocalDateTime updateAt;
}
