package com.mmedic.entity;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Medical service Order entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class MedicalServiceOrder extends BaseEntity {

    @Column
    private Integer totalAmount;

    @NotNull
    @Column(columnDefinition = "enum('PENDING', 'SUCCESS', 'FAILED')", nullable = false)
    private PaymentStatus paymentStatus;

    @Column(columnDefinition = "text")
    private String paymentReference;

    @Column
    private LocalDateTime paymentDate;

    @OneToMany(mappedBy = "medicalServiceOrder")
    private List<MedicalServiceOrderDetail> medicalServiceOrderDetails;

    @OneToOne(mappedBy = "medicalServiceOrder")
    private ExamineRecord examineRecord;

    @OneToOne(mappedBy = "medicalServiceOrder")
    private PreclinicalRecord preclinicalRecord;

    @OneToOne(mappedBy = "medicalServiceOrder")
    private PrescriptionRecord prescriptionRecord;

    @OneToOne(mappedBy = "medicalServiceOrder")
    private ServiceRecord serviceRecord;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
