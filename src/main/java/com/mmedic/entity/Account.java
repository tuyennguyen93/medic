package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Account entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Account extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "establishment_id")
    private Establishment establishment;

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String name;

    @Size(max = 255)
    @Column
    private String title;

    @Column
    private LocalDate dateOfBirth;

    @Column(columnDefinition = "text")
    private String avatarUrl;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "is_actived")
    private Boolean isActived;

    @Column(columnDefinition = "text")
    private String secret;

    @Column
    private LocalDateTime secretCreateTime;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

    @Size(max = 255)
    @Column(columnDefinition = "text")
    private String degree;

    @Column(columnDefinition = "text")
    private String notificationToken;

    @Column
    private String language;

    @OneToMany(mappedBy = "account", cascade = CascadeType.PERSIST)
    private List<AuthMethod> authMethods;

    @OneToOne(mappedBy = "account")
    private Patient patient;

    @OneToMany(mappedBy = "messageSender")
    private List<MedicalServiceOrderDiscussion> medicalServiceOrderDiscussions;

    @Column
    @CreatedDate
    private LocalDateTime createAt;

    @Size(max = 255)
    @Column(columnDefinition = "text")
    private String houseAddress;

    @Column(columnDefinition = "decimal(10,8)")
    private double latitude;

    @Column(columnDefinition = "decimal(11,8)")
    private double longitude;

}
