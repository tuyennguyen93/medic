package com.mmedic.entity;

import com.mmedic.enums.EstablishmentType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Establishment entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Establishment extends BaseEntity {

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String name;

    @Size(max = 255)
    @Column(nullable = false)
    private String address;

    @Size(max = 255)
    @Pattern(regexp = "\\d{0,11}")
    @Column
    private String phoneNumber;

    @Size(max = 255)
    @Column(columnDefinition = "text")
    private String avatarUrl;

    @Column
    private float ratingAverage;

    @Column(columnDefinition = "json")
    private String workingSchedule;

    @NotNull
    @Column(columnDefinition = "enum('MEDICAL_TREATMENT', 'PRECLINICAL', 'DRUG_STORE', 'DRUG_WAREHOUSE')", nullable = false)
    private EstablishmentType establishmentType;

    @ManyToOne
    @JoinColumn(name = "medical_department_id")
    private MedicalDepartment medicalDepartment;

    @Column(columnDefinition = "decimal(10,8)")
    private double latitude;

    @Column(columnDefinition = "decimal(11,8)")
    private double longitude;

    @Column
    private Integer averageTime;

    @Column
    private Boolean isDisabled;

    @OneToMany(mappedBy = "establishment")
    private List<Account> accounts;

    @OneToMany(mappedBy = "establishment")
    private List<EstablishmentDayOff> establishmentDayOffs;

    @OneToMany(mappedBy = "establishment")
    private List<EstablishmentWorkingSchedule> establishmentWorkingSchedules;

    @OneToMany(mappedBy = "establishment")
    private List<MedicalServiceRegistry> medicalServiceRegistries;

    @OneToMany(mappedBy = "establishment")
    private List<EstablishmentRating> establishmentRatings;

    @OneToMany(mappedBy = "establishment")
    private List<EstablishmentCertification> establishmentCertifications;

    @OneToMany(mappedBy = "establishment")
    private List<FavoriteEstablishment> favoriteEstablishments;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
