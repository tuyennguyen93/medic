package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Drug Export Detail entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
public class DrugExportDetail extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "drug_export_id", nullable = false)
    private DrugExport drugExport;

    @ManyToOne
    @JoinColumn(name = "drug_unit_id", nullable = false)
    private DrugUnit drugUnit;

    @ManyToOne
    @JoinColumn(name = "drug_import_detail_id", nullable = false)
    private DrugImportDetail drugImportDetail;

    @NotNull
    @Column(nullable = false)
    private int amount;

    @NotNull
    @Column(nullable = false)
    private int pricePerUnit;
}
