package com.mmedic.entity;

import com.mmedic.enums.Day;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.List;

/**
 * Establishment Working Schedule entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
public class EstablishmentWorkingSchedule extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "establishment_id", nullable = false)
    private Establishment establishment;

    @NotNull
    @Column(nullable = false)
    private LocalTime startTime;

    @NotNull
    @Column(nullable = false)
    private LocalTime endTime;

    @NotNull
    @Column(columnDefinition = "enum('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN')", nullable = false)
    private Day dayOfWeek;

    @Column
    private boolean isDeleted;

//    @OneToMany(mappedBy = "establishmentWorkingSchedule")
//    private List<MedicalServiceOrder> medicalServiceOrders;
}
