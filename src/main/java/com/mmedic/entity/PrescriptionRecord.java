package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Prescription Record entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class PrescriptionRecord extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "medical_record_id")
    private MedicalRecord medicalRecord;

    @OneToOne
    @JoinColumn(name = "medical_service_order_id", nullable = false)
    private MedicalServiceOrder medicalServiceOrder;

    @OneToMany(mappedBy = "prescriptionRecord")
    private List<PrescriptionDetail> prescriptionDetails;

    @Column
    private Boolean isConfirmed;

    @OneToMany(mappedBy = "prescriptionRecord")
    private List<DrugOrderExport> drugOrderExport;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
