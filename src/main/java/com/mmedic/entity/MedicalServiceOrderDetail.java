package com.mmedic.entity;

import com.mmedic.enums.MedicalServiceOrderStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Medical service order detail entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class MedicalServiceOrderDetail extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "medical_service_id", nullable = false)
    private MedicalService medicalService;

    @ManyToOne
    @JoinColumn(name = "medical_service_order_id", nullable = false)
    private MedicalServiceOrder medicalServiceOrder;

    @ManyToOne
    @JoinColumn(name = "establishment_id")
    private Establishment establishment;

    @ManyToOne
    @JoinColumn(name = "handle_by")
    private Account handleBy;

    @Column
    private Integer sequenceNumber;

    @Column
    private LocalDateTime appointmentDate;

    @Column
    private LocalDateTime proccessDate;

    @Column
    private LocalDateTime resultDate;

    @Column(columnDefinition = "text")
    private String result;

    @NotNull
    @Column(columnDefinition = "enum('REQUEST_SENT', 'REQUEST_CANCELLED', 'REQUEST_DENIED', 'REQUEST_ACCEPTED', 'REQUEST_PROCESSING', 'REQUEST_FULFILLED')", nullable = false)
    private MedicalServiceOrderStatus status;

    @Column
    private Integer txAmount;

    @Column
    private Integer parentId;

    @OneToMany(mappedBy = "medicalServiceOrderDetail")
    private List<MedicalServiceOrderDiscussion> medicalServiceOrderDiscussions;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
