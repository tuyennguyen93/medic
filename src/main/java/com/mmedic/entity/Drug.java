package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Drug entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Drug extends BaseEntity {

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String name;

    @Column
    private Integer parentId;

    @NotNull
    @Column(nullable = false)
    private boolean canPrescribe;

    @Size(max = 255)
    @Column
    private String path;

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String origin;

    @Column(columnDefinition = "text")
    private String contraindication;

    @Column(columnDefinition = "text")
    private String composition;

    @Column(columnDefinition = "text")
    private String concentration;

    @Column(columnDefinition = "text")
    private String form;

    @Column(columnDefinition = "text")
    private String dosage;


    @OneToMany(mappedBy = "drug")
    private List<DrugUnit> drugUnits;

    @Column(name = "is_disabled")
    private Boolean isDisabled;

    @Column
    @CreatedDate
    private LocalDateTime createAt;

    @Column
    @LastModifiedDate
    private LocalDateTime updateAt;
}
