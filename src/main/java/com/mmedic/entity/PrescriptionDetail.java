package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Prescription Detail entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
public class PrescriptionDetail extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "prescription_record_id", nullable = false)
    private PrescriptionRecord prescriptionRecord;

    //    @ManyToOne
//    @JoinColumn(name = "drug_id", nullable = false)
//    private Drug drug;

    @ManyToOne
    @JoinColumn(name = "drug_unit_id", nullable = false)
    private DrugUnit drugUnit;

    @NotNull
    @Column
    private int amount;

    @Column(name = "`usage`", columnDefinition = "text")
    private String usage;

    @Column(columnDefinition = "text")
    private String note;

    @OneToOne
    @JoinColumn(name = "medical_service_order_detail_id")
    private MedicalServiceOrderDetail medicalServiceOrderDetail;
}
