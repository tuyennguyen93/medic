package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Preclinical Record entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class PreclinicalRecord extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "medical_record_id", nullable = false)
    private MedicalRecord medicalRecord;

    @OneToOne
    @JoinColumn(name = "medical_service_order_id", nullable = false)
    private MedicalServiceOrder medicalServiceOrder;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
