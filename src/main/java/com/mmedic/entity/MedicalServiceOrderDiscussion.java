package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * Medical Service Order Discussion entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class MedicalServiceOrderDiscussion extends BaseEntity {

//    @ManyToOne
//    @JoinColumn(name = "medical_service_order_id", nullable = false)
//    private MedicalServiceOrder medicalServiceOrder;

    @ManyToOne
    @JoinColumn(name = "medical_service_order_detail_id", nullable = false)
    private MedicalServiceOrderDetail medicalServiceOrderDetail;

    @ManyToOne
    @JoinColumn(name = "message_sender", nullable = false)
    private Account messageSender;

    @Column(columnDefinition = "text")
    private String message;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
