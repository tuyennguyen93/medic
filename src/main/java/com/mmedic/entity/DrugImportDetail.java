package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Drug Import Detail entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
public class DrugImportDetail extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "drug_import_id", nullable = false)
    private DrugImport drugImport;

//    @ManyToOne
//    @JoinColumn(name = "drug_id", nullable = false)
//    private Drug drug;

    @ManyToOne
    @JoinColumn(name = "drug_unit_id", nullable = false)
    private DrugUnit drugUnit;

    @NotNull
    @Column(nullable = false)
    private int amount;

    @NotNull
    @Column(nullable = false)
    private int pricePerUnit;

    @NotNull
    @Column(nullable = false)
    private LocalDateTime expiration;

    @OneToMany(mappedBy = "drugImportDetail")
    private List<DrugExportDetail> drugExportDetails;

}
