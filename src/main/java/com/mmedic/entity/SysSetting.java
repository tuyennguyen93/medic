package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * System Setting entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class SysSetting extends BaseEntity {

    @NotNull
    @Size(max = 255)
    @Column(name = "`key`",nullable = false)
    private String key;

    @NotNull
    @Size(max = 255)
    @Column(name="value",nullable = false)
    private String value;

    @Column
    @CreatedBy
    private int updateBy;

    @Column
    @CreatedDate
    private LocalDateTime updateAt;
}
