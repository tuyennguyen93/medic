package com.mmedic.entity;

import com.mmedic.enums.Gender;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Patient entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Patient extends BaseEntity {

    @OneToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    private LocalDate dateOfBirth;

    @NotNull
    @Column(columnDefinition = "enum('M', 'F')", nullable = false)
    private Gender gender;

    @Size(max = 255)
    @Column
    private String address;

    @Column(columnDefinition = "text")
    private String email;

    @Size(max = 255)
    @Pattern(regexp = "\\d{0,11}")
    @Column
    private String phoneNumber;

    @Column(columnDefinition = "decimal(10,8)")
    private double latitude;

    @Column(columnDefinition = "decimal(11,8)")
    private double longitude;

    @OneToMany(mappedBy = "patient")
    private List<MedicalRecord> medicalRecords;

    @OneToMany(mappedBy = "patient")
    private List<ConstantMedicalInfo> constantMedicalInfos;

    @OneToMany(mappedBy = "patient")
    private List<VariableMedicalInfo> variableMedicalInfos;

    @OneToMany(mappedBy = "patient")
    private List<MedicineAllergy> medicineAllergies;

    @OneToMany(mappedBy = "patient")
    private List<FavoriteEstablishment> favoriteEstablishments;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
