package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Medical Service Price History entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class MedicalServicePriceHistory extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "medical_service_registry_id", nullable = false)
    private MedicalServiceRegistry medicalServiceRegistry;

    @NotNull
    @Column(nullable = false)
    private int price;

    @NotNull
    @Column(nullable = false)
    private LocalDateTime effectiveAt;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
