package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Drug Export entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
public class DrugOrderExport extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "prescription_record_id", nullable = false)
    private PrescriptionRecord prescriptionRecord;

    @OneToOne
    @JoinColumn(name = "drug_export_id", nullable = false)
    private DrugExport drugExport;


}
