package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Drug Import entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class DrugImport extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "to_establishment", nullable = false)
    private Establishment toEstablishment;

    @OneToMany(mappedBy = "drugImport")
    private List<DrugImportDetail> drugImportDetails;

    @ManyToOne
    @JoinColumn(name = "create_by", nullable = false)
    private Account createBy;

    @Column
    @LastModifiedDate
    private LocalDateTime createAt;
}
