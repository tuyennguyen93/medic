package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Drug Export entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
public class DrugInternalExport extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "create_by", nullable = false)
    private Account createBy;

    @ManyToOne
    @JoinColumn(name = "export_to", nullable = false)
    private Establishment exportTo;

    @OneToOne
    @JoinColumn(name = "export_id")
    private DrugExport drugExport;


}
