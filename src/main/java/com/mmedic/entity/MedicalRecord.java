package com.mmedic.entity;

import com.mmedic.enums.MedicalRecordType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Medical Record entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class MedicalRecord extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    @NotNull
    @Column(columnDefinition = "enum('TREATMENT', 'SERVICE')", nullable = false)
    private MedicalRecordType recordType;

    @OneToOne(mappedBy = "medicalRecord")
    private ExamineRecord examineRecord;

    @OneToOne(mappedBy = "medicalRecord")
    private PreclinicalRecord preclinicalRecord;

    @OneToOne(mappedBy = "medicalRecord")
    private PrescriptionRecord prescriptionRecord;

    @OneToOne(mappedBy = "medicalRecord")
    private ServiceRecord serviceRecord;

    @OneToMany(mappedBy = "medicalRecord")
    private List<EstablishmentRating> establishmentRating;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
