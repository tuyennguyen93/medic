package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Drug unit entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class DrugUnit extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "drug_id", nullable = false)
    private Drug drug;

    @Column
    private Integer parentId;

    @Column
    @NotNull
    @Size(max = 255)
    private String name;

    @Size(max = 255)
    @Column
    private String path;

    @Column
    private int parentEquivalentAmount;


    @OneToMany(mappedBy = "drugUnit")
    private List<DrugUnitPriceHistory> drugUnitPriceHistories;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
