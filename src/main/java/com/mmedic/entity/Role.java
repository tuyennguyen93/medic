package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Role entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
public class Role extends BaseEntity {

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String name;

    @Size(max = 255)
    @Column
    private String description;

    @OneToMany(mappedBy = "role")
    private List<RolePermission> rolePermissions;
}
