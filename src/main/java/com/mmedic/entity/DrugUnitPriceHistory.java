package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Drug Price History entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class DrugUnitPriceHistory extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "drug_unit_id", nullable = false)
    private DrugUnit drugUnit;

    @NotNull
    @Column(nullable = false)
    private int pricePerUnit;

    @NotNull
    @Column(nullable = false)
    private LocalDateTime effectiveAt;

    @Column
    @CreatedDate
    private LocalDateTime createAt;
}
