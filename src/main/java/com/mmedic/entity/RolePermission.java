package com.mmedic.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Role Permission entity.
 *
 * @author hungp
 */
@Entity
@Table
@Getter
@Setter
public class RolePermission extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

    @Size(max = 255)
    @NotNull
    @Column(nullable = false)
    private String scope;

    @Size(max = 255)
    @Column
    private String condition;

    @Size(max = 255)
    @Column
    private String conditionValue;
}
