package com.mmedic.rest.medicalRecord;

import com.mmedic.batch.zalopayUpdateStatus.dto.PaymentResultDTO;
import com.mmedic.rest.medicalRecord.dto.DeliveryPriceDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordHistoryDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalServiceRecordDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalServiceRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.PaymentDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalDetailRecordDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalTransportFeeDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalTransportTypeDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDTO;
import com.mmedic.rest.medicalRecord.dto.UpdatePreclinicalRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.UpdatePrescriptionRecordParamDTO;
import com.mmedic.service.payment.zalopay.dto.PaymentOrderResultDTO;
import com.mmedic.spring.errors.MedicException;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Medical Record Service.
 */
public interface MedicalRecordService {

    /**
     * Create new medical record.
     *
     * @param medicalRecordParamDTO medical record data.
     * @return medical record
     * @throws MedicException
     */
    MedicalRecordDTO createMedicalRecord(MedicalRecordParamDTO medicalRecordParamDTO) throws MedicException;

    /**
     * Pay examine (treatment0.
     *
     * @param medicalRecordId medical record id
     * @param paymentDTO      payment data
     * @return true if payment successfully
     * @throws MedicException
     */
    boolean payExamine(int medicalRecordId, PaymentDTO paymentDTO) throws MedicException;

    /**
     * Create payment order for examine.
     *
     * @param medicalRecordId medical record id.
     * @return payment order result
     * @throws MedicException
     */
    PaymentOrderResultDTO createPaymentOrderExamine(int medicalRecordId) throws MedicException;

    /**
     * Get medical record history.
     *
     * @param patientId patient id
     * @param pageable  pagination
     * @return page data
     * @throws MedicException
     */
    Page<MedicalRecordHistoryDTO> getMedicalRecordHistory(int patientId, Pageable pageable) throws MedicException;

    /**
     * Get medical record detail.
     *
     * @param medicalRecordId medical record id
     * @return medical record detail
     * @throws MedicException
     */
    MedicalRecordDTO getMedicalRecordDetail(int medicalRecordId) throws MedicException;

    /**
     * Get list prescription detail.
     *
     * @param medicalRecordId medical record
     * @return list prescription
     * @throws MedicException
     */
    List<PrescriptionRecordDetailDTO> getPrescription(int medicalRecordId) throws MedicException;

    /**
     * Get list preclinical of medical record.
     *
     * @param medicalRecordId medical record id.
     * @return list preclinical
     * @throws MedicException
     */
    List<PreclinicalDetailRecordDTO> getPreclinicalDetail(int medicalRecordId) throws MedicException;

    /**
     * Get preclinical transport fee.
     *
     * @param medicalRecordId medical record id
     * @param preclinicalId   preclinical id
     * @return transport fee
     * @throws MedicException
     */
    List<PreclinicalTransportFeeDTO> getPreclinicalTransportFee(int medicalRecordId, int preclinicalId) throws MedicException;

    /**
     * Update preclinical of medical record.
     *
     * @param medicalRecordId        medical record id
     * @param orderDetailId          order detail id
     * @param preclinicalRecordParam data to update
     * @return true if it is updated successfully
     * @throws MedicException
     */
    boolean updatePreclinicalForMedicalRecord(int medicalRecordId, int orderDetailId, UpdatePreclinicalRecordParamDTO preclinicalRecordParam) throws MedicException;

    /**
     * Update preclinical transport type.
     *
     * @param medicalRecordId medical record id
     * @param orderDetailId   order detail id
     * @param testType        test type
     * @return result
     * @throws MedicException
     */
    boolean updatePreclinicalTransportType(int medicalRecordId, int orderDetailId, PreclinicalTransportTypeDTO testType) throws MedicException;

    /**
     * Pay preclinical.
     *
     * @param medicalRecordId medical record id.
     * @param paymentDTO      payment data
     * @return true if it is payment successfully
     * @throws MedicException
     */
    boolean payPreclinical(int medicalRecordId, PaymentDTO paymentDTO) throws MedicException;

    /**
     * Creates payment order preclinical.
     *
     * @param medicalRecordId the medical record id
     * @return payment order result
     * @throws MedicException
     */
    PaymentOrderResultDTO createPaymentOrderPreclinical(int medicalRecordId) throws MedicException;

    /**
     * Update prescription of medical record.
     *
     * @param medicalRecordId               medical record
     * @param orderDetailId                 order detail id
     * @param updatePrescriptionRecordParam data to update
     * @return true if it is updated successfully
     * @throws MedicException
     */
    boolean updatePrescriptionForMedicalRecord(int medicalRecordId, int orderDetailId, UpdatePrescriptionRecordParamDTO updatePrescriptionRecordParam) throws MedicException;

    /**
     * Update prescription delivery.
     *
     * @param medicalRecordId medical record
     * @param isDelivery      delivery
     * @return true if updated successfully
     * @throws MedicException
     */
    boolean updatePrescriptionDeliveryForMedicalRecord(int medicalRecordId, boolean isDelivery) throws MedicException;

    /**
     * Get list delivery price.
     *
     * @param medicalRecordId medical record id
     * @param isDelivery      is delivery
     * @return list delivery price
     * @throws MedicException
     */
    List<DeliveryPriceDTO> getListDeliveryPrice(int medicalRecordId, boolean isDelivery) throws MedicException;

    /**
     * Pay prescription.
     *
     * @param medicalRecordId medical record id
     * @param paymentDTO      payment data
     * @return true if it payment successfully
     * @throws MedicException
     */
    boolean payPrescription(int medicalRecordId, PaymentDTO paymentDTO) throws MedicException;

    /**
     * Create payment order for prescription.
     *
     * @param medicalRecordId the medical record id
     * @return payment order result
     * @throws MedicException
     */
    PaymentOrderResultDTO createPaymentOrderPrescription(int medicalRecordId) throws MedicException;

    /**
     * Create medical health care service record.
     *
     * @param medicalServiceRecordParam data to create new medical health care service
     * @return medical health care service is created
     * @throws MedicException
     */
    MedicalServiceRecordDTO createMedicalServiceRecord(MedicalServiceRecordParamDTO medicalServiceRecordParam) throws MedicException;

    /**
     * Payment health care service.
     *
     * @param medicalRecordId medical record id
     * @param paymentDTO      payment data
     * @return true if it payment successfully
     * @throws MedicException
     */
    boolean payHealthCareService(int medicalRecordId, PaymentDTO paymentDTO) throws MedicException;

    /**
     * Create payment order for health care service.
     *
     * @param medicalRecordId the medical record id
     * @return payment order result
     * @throws MedicException
     */
    PaymentOrderResultDTO createPaymentOrderHealthCareService(int medicalRecordId) throws MedicException;

    /**
     * Cancel health care service.
     *
     * @param medicalRecordId medical record id
     * @return true if cancel successfully
     * @throws MedicException
     */
    boolean cancelHealthCareService(int medicalRecordId) throws MedicException;

    /**
     * Cancel treatment service.
     *
     * @param medicalRecordId medical record id
     * @return true if cancel successfully
     * @throws MedicException
     */
    boolean cancelTreatmentService(int medicalRecordId) throws MedicException;

    /**
     * Update payment status for order examine/preclinical/prescription/health care service.
     *
     * @param paymentResult payment result
     * @return true if update successfully
     * @throws MedicException
     */
    boolean updateOrderStatus(JSONObject paymentResult) throws MedicException;

    /**
     * Update status for order.
     *
     * @param paymentResults list payment result
     * @throws MedicException
     */
    void batchUpdateOrderStatus(List<? extends PaymentResultDTO> paymentResults) throws MedicException;
}
