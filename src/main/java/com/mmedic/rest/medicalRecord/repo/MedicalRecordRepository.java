package com.mmedic.rest.medicalRecord.repo;

import com.mmedic.entity.MedicalRecord;
import com.mmedic.rest.doctor.dto.HealthCareNotificationDTO;
import com.mmedic.rest.establishment.dto.ExamineNotificationDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordHistoryDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalPriceQueryDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalQueryRecordDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalTransportLocationDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT data.id                    AS id, " +
                    "       data.type                  AS type, " +
                    "       data.medicalDepartmentName AS medicalDepartmentName, " +
                    "       data.serviceName           AS serviceName, " +
                    "       data.appointmentDate       AS appointmentDate, " +
                    "       data.status                AS status, " +
                    "       data.paymentStatus         AS paymentStatus " +
                    "FROM (SELECT mr.id                 AS id, " +
                    "             mr.record_type        AS type, " +
                    "             md.name               AS medicalDepartmentName, " +
                    "             ''                    AS serviceName, " +
                    "             msod.appointment_date AS appointmentDate, " +
                    "             msod.status           AS status, " +
                    "             mso.payment_status    AS paymentStatus " +
                    "      from medical_record mr " +
                    "               LEFT OUTER JOIN examine_record er ON mr.id = er.medical_record_id " +
                    "               LEFT OUTER JOIN medical_service_order mso ON er.medical_service_order_id = mso.id " +
                    "               LEFT OUTER JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "               LEFT OUTER JOIN establishment es ON msod.establishment_id = es.id " +
                    "               LEFT OUTER JOIN medical_department md ON es.medical_department_id = md.id " +
                    "      WHERE mr.record_type = 'TREATMENT' " +
                    "        AND mr.patient_id = :patientId " +
                    "        AND msod.medical_service_id = 1 " +
                    "      UNION ALL " +
                    "      SELECT mr.id                 AS id, " +
                    "             mr.record_type        AS type, " +
                    "             ''                    AS medicalDepartmentName, " +
                    "             msg.name              AS serviceName, " +
                    "             msod.appointment_date AS appointmentDate, " +
                    "             msod.status           AS status, " +
                    "             mso.payment_status    AS paymentStatus " +
                    "      from medical_record mr " +
                    "               LEFT OUTER JOIN service_record sr ON mr.id = sr.medical_record_id " +
                    "               LEFT OUTER JOIN medical_service_order mso ON sr.medical_service_order_id = mso.id " +
                    "               LEFT OUTER JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "               LEFT OUTER JOIN medical_service ms ON msod.medical_service_id = ms.id " +
                    "               LEFT OUTER JOIN medical_service_group msg ON ms.medical_service_group_id = msg.id " +
                    "      WHERE mr.record_type = 'SERVICE' " +
                    "        AND mr.patient_id = :patientId " +
                    "        AND msg.id >= 5 " +
                    "     ) AS data",
            countQuery = "SELECT COUNT(*) " +
                    "FROM (SELECT mr.id                 AS id " +
                    "      from medical_record mr " +
                    "               LEFT OUTER JOIN examine_record er ON mr.id = er.medical_record_id " +
                    "               LEFT OUTER JOIN medical_service_order mso ON er.medical_service_order_id = mso.id " +
                    "               LEFT OUTER JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "               LEFT OUTER JOIN establishment es ON msod.establishment_id = es.id " +
                    "               LEFT OUTER JOIN medical_department md ON es.medical_department_id = md.id " +
                    "      WHERE mr.record_type = 'TREATMENT' " +
                    "        AND mr.patient_id = :patientId " +
                    "        AND msod.medical_service_id = 1 " +
                    "      UNION ALL " +
                    "      SELECT mr.id                 AS id " +
                    "      from medical_record mr " +
                    "               LEFT OUTER JOIN service_record sr ON mr.id = sr.medical_record_id " +
                    "               LEFT OUTER JOIN medical_service_order mso ON sr.medical_service_order_id = mso.id " +
                    "               LEFT OUTER JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "               LEFT OUTER JOIN medical_service ms ON msod.medical_service_id = ms.id " +
                    "               LEFT OUTER JOIN medical_service_group msg ON ms.medical_service_group_id = msg.id " +
                    "      WHERE mr.record_type = 'SERVICE' " +
                    "        AND mr.patient_id = :patientId " +
                    "        AND msg.id >= 5 " +
                    "     ) AS data")
    Page<MedicalRecordHistoryDTO> findMedicalRecordHistoryByPatientId(@Param("patientId") int patientId, Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT data.id                                               AS id, " +
                    "       CASE WHEN data.ratingId IS NOT NULL THEN '1' ELSE '0' END AS hasRating, " +
                    "       data.drugId                                           AS drugId, " +
                    "       data.name                                             AS name, " +
                    "       data.`usage`                                          AS `usage`, " +
                    "       data.note                                             AS note, " +
                    "       data.unit                                             AS unit, " +
                    "       data.pricePerUnit                                     AS pricePerUnit, " +
                    "       data.amount                                           AS amount, " +
                    "       data.drugStoreId                                      AS drugStoreId, " +
                    "       data.drugStoreName                                    AS drugStoreName," +
                    "       data.drugRating                                       AS drugRating " +
                    "FROM ( " +
                    "         SELECT DISTINCT pd.`usage`          AS `usage`, " +
                    "                msod.id             AS id, " +
                    "                dr.id               AS drugId, " +
                    "                dr.name             AS name, " +
                    "                pd.note             AS note, " +
                    "                du.name             AS unit, " +
                    "                duph.price_per_unit AS pricePerUnit, " +
                    "                pd.amount           AS amount, " +
                    "                es.id               AS drugStoreId, " +
                    "                es.name             AS drugStoreName, " +
                    "                es.rating_average   AS drugRating, " +
                    "                er.id               AS ratingId, " +
                    "                du.id               AS drugUnitId, " +
                    "                duph.effective_at   AS effectiveAt " +
                    "         FROM prescription_record pr " +
                    "                  LEFT JOIN medical_record mr ON pr.medical_record_id = mr.id " +
                    "                  LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "                  LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "                  LEFT JOIN establishment es ON msod.establishment_id = es.id " +
                    "                  LEFT JOIN establishment_rating er ON es.id = er.establishment_id AND mr.id = er.medical_record_id " +
                    "                  LEFT JOIN prescription_detail pd ON pr.id = pd.prescription_record_id AND msod.id = pd.medical_service_order_detail_id " +
                    "                  LEFT JOIN drug_unit du ON pd.drug_unit_id = du.id " +
                    "                  LEFT JOIN drug_unit_price_history duph ON du.id = duph.drug_unit_id " +
                    "                  LEFT JOIN drug dr ON du.drug_id = dr.id " +
                    "         WHERE pr.medical_record_id = :medicalRecordId " +
                    "         AND pr.create_at <= :timeGetPrescription " +
                    "     ) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT drug_unit_id, " +
                    "           MAX(effective_at) AS max_eff " +
                    "    FROM drug_unit_price_history " +
                    "    WHERE effective_at <= :effDate " +
                    "    GROUP BY drug_unit_id " +
                    ") AS price ON data.drugUnitId = price.drug_unit_id AND data.effectiveAt = price.max_eff " +
                    "ORDER BY data.id")
    List<PrescriptionRecordDetailDTO> findPrescriptionDetails(@Param("medicalRecordId") int medicalRecordId,
                                                              @Param("effDate") LocalDateTime effDate,
                                                              @Param("timeGetPrescription") LocalDateTime timeGetPrescription);

    @Query(nativeQuery = true,
            value = "SELECT data.id            AS id, " +
                    "       data.drugId        AS drugId, " +
                    "       data.unit          AS unit, " +
                    "       data.pricePerUnit  AS pricePerUnit, " +
                    "       data.amount        AS amount " +
                    "FROM ( " +
                    "         SELECT DISTINCT msod.id               AS id, " +
                    "                dr.id               AS drugId, " +
                    "                dr.name             AS name, " +
                    "                du.name             AS unit, " +
                    "                duph.price_per_unit AS pricePerUnit, " +
                    "                pd.amount           AS amount, " +
                    "                du.id               AS drugUnitId, " +
                    "                duph.effective_at   AS effectiveAt " +
                    "         FROM prescription_record pr " +
                    "                  LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "                  LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "                  LEFT JOIN establishment es ON msod.establishment_id = es.id " +
                    "                  LEFT JOIN prescription_detail pd ON pr.id = pd.prescription_record_id AND msod.id = pd.medical_service_order_detail_id " +
                    "                  LEFT JOIN drug_unit du ON pd.drug_unit_id = du.id " +
                    "                  LEFT JOIN drug_unit_price_history duph ON du.id = duph.drug_unit_id " +
                    "                  LEFT JOIN drug dr ON du.drug_id = dr.id " +
                    "         WHERE pr.medical_record_id = :medicalRecordId " +
                    "     ) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT drug_unit_id, " +
                    "           MAX(effective_at) AS max_eff " +
                    "    FROM drug_unit_price_history " +
                    "    WHERE effective_at <= :effDate " +
                    "    GROUP BY drug_unit_id " +
                    ") AS price ON data.drugUnitId = price.drug_unit_id AND data.effectiveAt = price.max_eff")
    List<PrescriptionRecordDetailDTO> findPrescriptionPrice(@Param("medicalRecordId") int medicalRecordId, @Param("effDate") LocalDateTime effDate);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT data.id                                          AS id, " +
                    "       data.establishmentId                                      AS establishmentId, " +
                    "       data.establishmentName                                    AS establishmentName, " +
                    "       data.establishmentAddress                                 AS establishmentAddress, " +
                    "       data.establishmentPhoneNumber                             AS establishmentPhoneNumber, " +
                    "       data.establishmentAvatarUrl                               AS establishmentAvatarUrl, " +
                    "       data.establishmentRating                                  AS establishmentRating, " +
                    "       data.serviceId                                            AS serviceId, " +
                    "       data.serviceName                                          AS serviceName, " +
                    "       data.appointmentDate                                      AS appointmentDate, " +
                    "       data.sequenceNumber                                       AS sequenceNumber, " +
                    "       data.price                                                AS price, " +
                    "       data.resultDate                                           AS resultDate, " +
                    "       data.result                                               AS result, " +
                    "       data.status                                               AS status, " +
                    "       CASE WHEN data.ratingId IS NOT NULL THEN '1' ELSE '0' END AS hasRating," +
                    "       data.paymentStatus                                        AS paymentStatus " +
                    "FROM ( " +
                    "         SELECT msod.id               AS id, " +
                    "                e.id                  AS establishmentId, " +
                    "                e.name                AS establishmentName, " +
                    "                e.address             AS establishmentAddress, " +
                    "                e.phone_number        AS establishmentPhoneNumber, " +
                    "                e.avatar_url          AS establishmentAvatarUrl, " +
                    "                e.rating_average      AS establishmentRating, " +
                    "                ms.id                 AS serviceId, " +
                    "                ms.name               AS serviceName, " +
                    "                msod.appointment_date AS appointmentDate, " +
                    "                msod.sequence_number  AS sequenceNumber, " +
                    "                msph.price            AS price, " +
                    "                msod.result_date      AS resultDate, " +
                    "                msod.result           AS result, " +
                    "                msod.status           AS status, " +
                    "                mso.payment_status    AS paymentStatus, " +
                    "                msr.id                AS serviceRegistryId, " +
                    "                er.id                 AS ratingId," +
                    "                msph.effective_at     AS effectiveAt " +
                    "         FROM preclinical_record pr " +
                    "                  LEFT JOIN medical_record mr ON pr.medical_record_id = mr.id " +
                    "                  LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "                  LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "                  LEFT JOIN medical_service ms ON msod.medical_service_id = ms.id " +
                    "                  LEFT JOIN establishment e ON msod.establishment_id = e.id " +
                    "                  LEFT JOIN establishment_rating er ON e.id = er.establishment_id AND mr.id = er.medical_record_id" +
                    "                  LEFT JOIN medical_service_registry msr " +
                    "                            ON ms.id = msr.medical_service_id AND msr.establishment_id = e.id " +
                    "                  LEFT JOIN medical_service_price_history msph on msr.id = msph.medical_service_registry_id " +
                    "         WHERE pr.medical_record_id = :medicalRecordId " +
                    "            AND msod.parent_id IS NULL " +
                    "     ) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT medical_service_registry_id, " +
                    "           MAX(effective_at) AS max_eff " +
                    "    FROM medical_service_price_history " +
                    "    WHERE effective_at <= :effDate " +
                    "    GROUP BY medical_service_registry_id " +
                    ") AS price ON data.establishmentId IS NULL OR (data.serviceRegistryId = price.medical_service_registry_id AND data.effectiveAt = price.max_eff)" +
                    "ORDER BY data.id")
    List<PreclinicalQueryRecordDTO> findPreclinicalDetails(@Param("medicalRecordId") int medicalRecordId, @Param("effDate") LocalDateTime effDate);

    @Query(nativeQuery = true,
            value = "SELECT data.id                   AS id, " +
                    "       data.serviceId            AS serviceId, " +
                    "       data.price                AS price " +
                    "FROM ( " +
                    "         SELECT msod.id               AS id, " +
                    "                ms.id                 AS serviceId, " +
                    "                msph.price            AS price, " +
                    "                msr.id                AS serviceRegistryId, " +
                    "                msph.effective_at     AS effectiveAt " +
                    "         FROM preclinical_record pr " +
                    "                  LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "                  LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "                  LEFT JOIN medical_service ms ON msod.medical_service_id = ms.id " +
                    "                  LEFT JOIN establishment e ON msod.establishment_id = e.id " +
                    "                  LEFT JOIN medical_service_registry msr " +
                    "                            ON ms.id = msr.medical_service_id AND msr.establishment_id = e.id " +
                    "                  LEFT JOIN medical_service_price_history msph on msr.id = msph.medical_service_registry_id " +
                    "         WHERE pr.medical_record_id = :medicalRecordId " +
                    "            AND msod.parent_id IS NULL" +
                    "     ) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT medical_service_registry_id, " +
                    "           MAX(effective_at) AS max_eff " +
                    "    FROM medical_service_price_history " +
                    "    WHERE effective_at <= :effDate " +
                    "    GROUP BY medical_service_registry_id " +
                    ") AS price ON data.serviceRegistryId = price.medical_service_registry_id AND data.effectiveAt = price.max_eff")
    List<PreclinicalPriceQueryDTO> findPreclinicalPrice(@Param("medicalRecordId") int medicalRecordId, @Param("effDate") LocalDateTime effDate);

    Optional<MedicalRecord> findByIdAndPatient_Account_Id(int id, int principalId);

    /**
     * Find examine order have price is changed.
     *
     * @param doctorId doctor id
     * @return list order
     */
    @Query(nativeQuery = true,
            value = "SELECT mr.id                AS medicalRecordId, " +
                    "       p.id                 AS patientId, " +
                    "       p.name               AS patientName, " +
                    "       a.notification_token AS notificationToken," +
                    "       a.language           AS language," +
                    "       a2.id                AS doctorId, " +
                    "       a2.name              AS doctorName " +
                    "FROM examine_record er " +
                    "         LEFT JOIN medical_record mr ON er.medical_record_id = mr.id " +
                    "         LEFT JOIN patient p ON mr.patient_id = p.id " +
                    "         LEFT JOIN account a ON p.account_id = a.id " +
                    "         LEFT JOIN medical_service_order mso ON er.medical_service_order_id = mso.id " +
                    "         LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "         LEFT JOIN account a2 ON msod.handle_by = a2.id " +
                    "WHERE mso.payment_status = 'PENDING' " +
                    "  AND msod.handle_by = :doctorId " +
                    "  AND msod.medical_service_id = 1")
    List<ExamineNotificationDTO> findExaminePriceChange(@Param("doctorId") int doctorId);

    /**
     * Find health care service order have price is changed.
     *
     * @param staffId staff id
     * @return list order
     */
    @Query(nativeQuery = true,
            value = "SELECT mr.id                AS medicalRecordId, " +
                    "       p.id                 AS patientId, " +
                    "       p.name               AS patientName, " +
                    "       a.notification_token AS notificationToken, " +
                    "       a.language           AS language, " +
                    "       a2.id                AS staffId, " +
                    "       a2.name              AS staffName, " +
                    "       ms.id                AS packageId, " +
                    "       ms.name              AS packageName " +
                    "FROM service_record sr " +
                    "         LEFT JOIN medical_record mr ON sr.medical_record_id = mr.id " +
                    "         LEFT JOIN patient p ON mr.patient_id = p.id " +
                    "         LEFT JOIN account a ON p.account_id = a.id " +
                    "         LEFT JOIN medical_service_order mso ON sr.medical_service_order_id = mso.id " +
                    "         LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "         LEFT JOIN account a2 ON msod.handle_by = a2.id " +
                    "         LEFT JOIN medical_service ms ON msod.medical_service_id = ms.id " +
                    "WHERE mso.payment_status = 'PENDING' " +
                    "  AND msod.handle_by = :staffId " +
                    "  AND msod.medical_service_id = :packageId")
    List<HealthCareNotificationDTO> findHealthCarePriceChange(@Param("staffId") int staffId, @Param("packageId") int packageId);

    /**
     * Find doctor, patient, preclinical location.
     *
     * @param medicalRecordId medical record id
     * @param preclinicalEstablishmentId   preclinical id
     * @return location
     */
    @Query(nativeQuery = true,
            value = "SELECT de.latitude  AS doctorLatitude, " +
                    "       de.longitude AS doctorLongitude, " +
                    "       p.latitude   AS patientLatitude, " +
                    "       p.longitude  AS patientLongitude, " +
                    "       pe.latitude  AS preclinicalLatitude, " +
                    "       pe.longitude AS preclinicalLongitude " +
                    "FROM medical_record mr " +
                    "         LEFT JOIN examine_record er ON mr.id = er.medical_record_id " +
                    "         LEFT JOIN medical_service_order mso ON er.medical_service_order_id = mso.id " +
                    "         LEFT JOIN medical_service_order_detail msod " +
                    "                   ON mso.id = msod.medical_service_order_id AND msod.medical_service_id = 1 " +
                    "         LEFT JOIN establishment de ON msod.establishment_id = de.id " +
                    "         LEFT JOIN patient p ON mr.patient_id = p.id " +
                    "         LEFT JOIN establishment pe ON pe.id = :preclinicalId AND pe.establishment_type = 'PRECLINICAL' " +
                    "WHERE mr.id = :medicalRecordId")
    Optional<PreclinicalTransportLocationDTO> findLocationOfDoctorAndPatientAndPreclinical(@Param("medicalRecordId") int medicalRecordId,
                                                                                           @Param("preclinicalId") int preclinicalEstablishmentId);


    /**
     * Find all doctor, patient, preclinical location.
     *
     * @param medicalRecordId medical record id
     * @param orderDetailIds  preclinical order detail ids
     * @return all location
     */
    @Query(nativeQuery = true,
            value = "SELECT DISTINCT msod2.id                 AS orderDetailId, " +
                    "                msod2.medical_service_id AS serviceId, " +
                    "                de.latitude              AS doctorLatitude, " +
                    "                de.longitude             AS doctorLongitude, " +
                    "                p.latitude               AS patientLatitude, " +
                    "                p.longitude              AS patientLongitude, " +
                    "                pe.latitude              AS preclinicalLatitude, " +
                    "                pe.longitude             AS preclinicalLongitude " +
                    "FROM medical_record mr " +
                    "         LEFT JOIN examine_record er ON mr.id = er.medical_record_id " +
                    "         LEFT JOIN medical_service_order mso ON er.medical_service_order_id = mso.id " +
                    "         LEFT JOIN medical_service_order_detail msod " +
                    "                   ON mso.id = msod.medical_service_order_id AND msod.medical_service_id = 1 " +
                    "         LEFT JOIN establishment de ON msod.establishment_id = de.id " +
                    "         LEFT JOIN patient p ON mr.patient_id = p.id " +
                    "         LEFT JOIN preclinical_record pr ON mr.id = pr.medical_record_id " +
                    "         LEFT JOIN medical_service_order mso2 ON pr.medical_service_order_id = mso2.id " +
                    "         LEFT JOIN medical_service_order_detail msod2 " +
                    "                   ON mso2.id = msod2.medical_service_order_id AND msod2.id IN (:orderDetailIds) " +
                    "         LEFT JOIN establishment pe ON msod2.establishment_id = pe.id " +
                    "WHERE mr.id = :medicalRecordId")
    List<PreclinicalTransportLocationDTO> findAllLocationOfDoctorAndPatientAndPreclinical(@Param("medicalRecordId") int medicalRecordId,
                                                                                          @Param("orderDetailIds") List<Integer> orderDetailIds);

    /**
     * Find transport order detail id by order detail id.
     *
     * @param orderDetailId order detail id
     * @return transport order detail id
     */
    @Query(nativeQuery = true,
            value = "SELECT msod.medical_service_id " +
                    "FROM medical_service_order_detail msod " +
                    "WHERE msod.parent_id = :orderDetailId")
    Optional<Integer> findTransportTypeByOrderDetailId(@Param("orderDetailId") int orderDetailId);
}
