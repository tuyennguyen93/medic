package com.mmedic.rest.medicalRecord.repo;

import com.mmedic.entity.PreclinicalRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PreclinicalRecordRepository extends JpaRepository<PreclinicalRecord, Integer> {

    Optional<PreclinicalRecord> findByMedicalRecord_Id(int medicalRecordId);

}
