package com.mmedic.rest.medicalRecord.repo;

import com.mmedic.entity.PrescriptionRecord;
import com.mmedic.rest.healthFacility.dto.PrescriptionOrderDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PrescriptionRecordRepository extends JpaRepository<PrescriptionRecord, Integer> {

//    @Query("SELECT prescriptionRecord.id AS id, serviceOrder.paymentStatus AS paymentStatus, orderDetail.status AS status " +
//            "FROM PrescriptionRecord prescriptionRecord " +
//            "LEFT JOIN prescriptionRecord.medicalServiceOrder serviceOrder " +
//            "LEFT JOIN serviceOrder.medicalServiceOrderDetails orderDetail " +
//            "WHERE prescriptionRecord.medicalRecord.id = :medicalRecordId")
//    Optional<PrescriptionRecordLittleDTO> findPrescriptionRecordByMedicalRecordId(@Param("medicalRecordId") int medicalRecordId);

    Optional<PrescriptionRecord> findByMedicalRecord_Id(int medicalRecordId);

    /**
     * Find Prescription is ordered before date.
     *
     * @param dateTime the date time
     * @return List Prescription Ordered
     */
    @Query(nativeQuery = true,
            value = "SELECT DISTINCT data.id            AS id, " +
                    "       data.name          AS name, " +
                    "       data.avatarUrl     AS avatarUrl, " +
                    "       data.dateOfBirth   AS dateOfBirth, " +
                    "       data.patientId     AS patientId, " +
                    "       data.paymentStatus AS paymentStatus, " +
                    "       data.status        AS status " +
                    "FROM ( " +
                    "         SELECT pr.id                                                          AS id, " +
                    "                (CASE WHEN pa.name IS NULL then 'Đơn bán lẻ' ELSE pa.name END) AS name, " +
                    "                ac.avatar_url                                                  AS avatarUrl, " +
                    "                pa.date_of_birth                                               AS dateOfBirth, " +
                    "                pa.id                                                          AS patientId, " +
                    "                mso.payment_status                                             AS paymentStatus, " +
                    "                msod.status                                                    AS status " +
                    "         FROM prescription_record pr " +
                    "                  LEFT OUTER JOIN medical_record mr ON pr.medical_record_id = mr.id " +
                    "                  LEFT OUTER JOIN patient pa ON mr.patient_id = pa.id " +
                    "                  LEFT OUTER JOIN account ac ON pa.account_id = ac.id " +
                    "                  LEFT OUTER JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "                  LEFT OUTER JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "                  LEFT OUTER JOIN medical_service ms ON msod.medical_service_id = ms.id " +
                    "                  LEFT OUTER JOIN establishment es ON msod.establishment_id = es.id " +
                    "         WHERE es.id = :establishmentId " +
                    "           AND ms.id = :medicineSupplyServiceId " +
                    "           AND ( " +
                    "                    mso.payment_status = 'SUCCESS' " +
                    "                    OR mr.id IS NULL " +
                    "             ) " +
                    "           AND (" +
                    "              :all = 1 " +
                    "              OR (" +
                    "                 msod.status IN ('REQUEST_ACCEPTED', 'REQUEST_PROCESSING') " +
                    "                 OR (msod.status = 'REQUEST_FULFILLED' AND DATE(msod.result_date) = CURRENT_DATE)" +
                    "              )" +
                    "           ) " +
                    "         ORDER BY msod.result_date, msod.proccess_date, pr.create_at ASC" +
                    "     ) AS data " +
                    "WHERE data.name LIKE :condition",
            countQuery = "SELECT COUNT(DISTINCT data.id, data.name, data.status) " +
                    "FROM ( " +
                    "         SELECT pr.id                                                          AS id, " +
                    "                (CASE WHEN ac.name IS NULL then 'Đơn bán lẻ' ELSE ac.name END) AS name," +
                    "                msod.status                                                    AS status " +
                    "         FROM prescription_record pr " +
                    "                  LEFT OUTER JOIN medical_record mr ON pr.medical_record_id = mr.id " +
                    "                  LEFT OUTER JOIN patient pa ON mr.patient_id = pa.id " +
                    "                  LEFT OUTER JOIN account ac ON pa.account_id = ac.id " +
                    "                  LEFT OUTER JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "                  LEFT OUTER JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "                  LEFT OUTER JOIN medical_service ms ON msod.medical_service_id = ms.id " +
                    "                  LEFT OUTER JOIN establishment es ON msod.establishment_id = es.id " +
                    "         WHERE es.id = :establishmentId " +
                    "           AND ms.id = :medicineSupplyServiceId " +
                    "           AND ( " +
                    "                    mso.payment_status = 'SUCCESS' " +
                    "                    OR mr.id IS NULL " +
                    "             ) " +
                    "           AND (" +
                    "              :all = 1 " +
                    "              OR (" +
                    "                 msod.status IN ('REQUEST_ACCEPTED', 'REQUEST_PROCESSING') " +
                    "                 OR (msod.status = 'REQUEST_FULFILLED' AND DATE(msod.result_date) = CURRENT_DATE)" +
                    "              )" +
                    "           ) " +
                    "     ) AS data " +
                    "WHERE data.name LIKE :condition")
    Page<PrescriptionOrderDTO> findPrescriptionOrderedInDate(@Param("condition") String condition,
                                                             @Param("establishmentId") int establishmentId,
                                                             @Param("medicineSupplyServiceId") int medicineSupplyServiceId,
                                                             @Param("all") Boolean all,
                                                             Pageable pageable);

    /**
     * Find establishment id and prescription record id of transport service.
     *
     * @param prescriptionIds prescription id
     * @return map prescriptionId_establishmentId
     */
    @Query(nativeQuery = true,
            value = "SELECT CONCAT(pr.id, '_', msod.establishment_id) AS mapId " +
                    "FROM medical_service_order_detail msod " +
                    "         LEFT JOIN medical_service_order mso ON msod.medical_service_order_id = mso.id " +
                    "         LEFT JOIN prescription_record pr ON mso.id = pr.medical_service_order_id " +
                    "WHERE pr.id IN (:prescriptionIds) " +
                    "  AND msod.medical_service_id = 4")
    List<String> findEstablishmentIdOfTransportServiceByMedicalRecordId(@Param("prescriptionIds") List<Integer> prescriptionIds);

    @Query(nativeQuery = true,
            value = "SELECT data.id            AS id, " +
                    "       data.unit          AS unit, " +
                    "       data.pricePerUnit  AS pricePerUnit, " +
                    "       data.amount        AS amount " +
                    "FROM ( " +
                    "         SELECT DISTINCT dr.id               AS id, " +
                    "                dr.name             AS name, " +
                    "                du.name             AS unit, " +
                    "                duph.price_per_unit AS pricePerUnit, " +
                    "                pd.amount           AS amount, " +
                    "                du.id               AS drugUnitId, " +
                    "                duph.effective_at   AS effectiveAt " +
                    "         FROM prescription_record pr " +
                    "                  LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "                  LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "                  LEFT JOIN establishment es ON msod.establishment_id = es.id " +
                    "                  LEFT JOIN prescription_detail pd ON pr.id = pd.prescription_record_id " +
                    "                  LEFT JOIN drug_unit du ON pd.drug_unit_id = du.id " +
                    "                  LEFT JOIN drug_unit_price_history duph ON du.id = duph.drug_unit_id " +
                    "                  LEFT JOIN drug dr ON du.drug_id = dr.id " +
                    "         WHERE pr.id = :prescriptionRecordId " +
                    "     ) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT drug_unit_id, " +
                    "           MAX(effective_at) AS max_eff " +
                    "    FROM drug_unit_price_history " +
                    "    WHERE effective_at <= :effDate " +
                    "    GROUP BY drug_unit_id " +
                    ") AS price ON data.drugUnitId = price.drug_unit_id AND data.effectiveAt = price.max_eff")
    List<PrescriptionRecordDetailDTO> findPrescriptionPrice(@Param("prescriptionRecordId") int prescriptionRecordId, @Param("effDate") LocalDateTime effDate);

}
