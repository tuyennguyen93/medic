package com.mmedic.rest.medicalRecord.repo;

import com.mmedic.entity.ExamineRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ExamineRecordRepository extends JpaRepository<ExamineRecord, Integer> {

    @Query("SELECT examineRecord " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.medicalServiceOrder serviceOrder " +
            "INNER JOIN serviceOrder.examineRecord examineRecord " +
            "WHERE orderDetail.establishment.id = :estId AND orderDetail.id = :orderDetailId ")
    Optional<ExamineRecord> findByOrderDetailId (@Param("estId") Integer estId,
                                             @Param("orderDetailId") Integer orderDetailId);
}
