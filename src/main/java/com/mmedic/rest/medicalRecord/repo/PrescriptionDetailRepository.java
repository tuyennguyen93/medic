package com.mmedic.rest.medicalRecord.repo;

import com.mmedic.entity.PrescriptionDetail;
import com.mmedic.rest.drug.dto.DrugAmountWaitingDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionDetailDoctorDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PrescriptionDetailRepository extends JpaRepository<PrescriptionDetail, Integer> {


    @Query("SELECT new com.mmedic.rest.medicalRecord.dto.PrescriptionDetailDoctorDTO(  detail.id, " +
            "drug.name      AS drugName, " +
            "drug.id        AS drugId, " +
            "unit.id        AS unitId, " +
            "unit.name      AS unitName, " +
            "orderDetail.establishment.id       AS drugStoreId, " +
            "orderDetail.establishment.name     AS drugStoreName, " +
            "detail.usage                       AS usage, " +
            "detail.note    AS note, " +
            "detail.amount  AS amount ) " +
            "FROM PrescriptionDetail detail " +
            "INNER JOIN  detail.prescriptionRecord record " +
            "INNER JOIN  detail.drugUnit unit " +
            "INNER JOIN  detail.medicalServiceOrderDetail orderDetail " +
            "INNER JOIN  unit.drug drug " +
            "WHERE record.id = :prescriptionRecordId ")
    List<PrescriptionDetailDoctorDTO> findByPrescriptionRecord(@Param("prescriptionRecordId") Integer id);

    Integer countByDrugUnit_Id(Integer drugId);

    @Query("SELECT  new  com.mmedic.rest.drug.dto.DrugAmountWaitingDTO(" +
            "       drugUnit.id AS drugUnitId , " +
            "       CASE WHEN 1 = 0 THEN 0 ELSE SUM(detail.amount) END AS waitingAmount ) " +
            "   FROM PrescriptionDetail detail " +
            "   INNER JOIN detail.prescriptionRecord  prescriptionRecord " +
            "   INNER JOIN detail.medicalServiceOrderDetail  medicalServiceOrderDetail " +
            "   INNER JOIN medicalServiceOrderDetail.establishment establishment " +
            "   INNER JOIN medicalServiceOrderDetail.medicalService medicalService " +
            "   INNER JOIN detail.drugUnit drugUnit " +
            "   INNER JOIN drugUnit.drug drug " +
            "   WHERE " +
            "       drug.id = :drugId " +
            "       AND establishment.id = :estId " +
            "       AND medicalService.id = 3 " +
            "       AND medicalServiceOrderDetail.status IN ('REQUEST_SENT', 'REQUEST_ACCEPTED', 'REQUEST_PROCESSING') " +
            "   GROUP BY drugUnit.id " +
            "")
    List<DrugAmountWaitingDTO> findDrugAmountWaiting(@Param("drugId") Integer drugId,
                                                     @Param("estId") Integer estId);

}
