package com.mmedic.rest.medicalRecord.repo;

import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.rest.medicalRecord.dto.DeliveryDistanceDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalDetailDoctorDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface MedicalServiceOrderDetailRepository extends JpaRepository<MedicalServiceOrderDetail, Integer> {

    @Query("SELECT orderDetail " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.establishment establishment " +
            "INNER JOIN orderDetail.medicalServiceOrder serviceOrder " +
            "WHERE " +
            "orderDetail.medicalService.id = :mServiceId " +
            "AND  establishment.id = :estId " +
            "AND  serviceOrder.paymentStatus = 'SUCCESS' " +
            "AND ( orderDetail.status IN ('REQUEST_ACCEPTED','REQUEST_PROCESSING') OR ( orderDetail.status = 'REQUEST_FULFILLED' AND date(orderDetail.resultDate) = CURRENT_DATE ) ) " +
            "ORDER BY orderDetail.resultDate, orderDetail.proccessDate, orderDetail.appointmentDate ASC  " +
            "")
    List<MedicalServiceOrderDetail> findAllTreatmentRequest(@Param("estId") Integer estId,
                                                            @Param("mServiceId") Integer mServiceId);

    @Query(nativeQuery = true,
            value = "SELECT msod.medical_service_order_id " +
                    "FROM medical_service_order_detail msod " +
                    "WHERE msod.medical_service_order_id IN (:orderIds) " +
                    "  AND msod.medical_service_id = 2")
    List<Integer> findTransportExamineServiceByOrderId(@Param("orderIds") List<Integer> orderIds);


    Page<MedicalServiceOrderDetail> findAllByEstablishment_IdAndMedicalService_IdAndStatusIn(Pageable pageable,
                                                                                             Integer estId,
                                                                                             Integer mServiceId,
                                                                                             MedicalServiceOrderStatus... statuses);

    List<MedicalServiceOrderDetail> findAllByMedicalServiceOrder_IdAndStatusIn(Integer serviceOrderId, MedicalServiceOrderStatus... statuses);


    Optional<MedicalServiceOrderDetail> findByEstablishment_IdAndId(Integer estId, Integer detailId);

    @Query("SELECT orderDetail " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.establishment establishment " +
            "INNER JOIN orderDetail.medicalServiceOrder serviceOrder " +
            "INNER JOIN serviceOrder.examineRecord examineRecord " +
            "INNER JOIN examineRecord.medicalRecord medicalRecord " +
            "INNER JOIN medicalRecord.patient patient " +
            "WHERE orderDetail.medicalService.id = 1 " +
            "AND ((:medicalDepartmentId IS NULL) OR (establishment.medicalDepartment.id = :medicalDepartmentId ) ) " +
            "AND patient.id = :patientId " +
            "AND orderDetail.status IN :orderStatus ")
    Page<MedicalServiceOrderDetail> findByPatientId(Pageable pageable,
                                                    @Param("medicalDepartmentId") Integer medicalDepartmentId,
                                                    @Param("patientId") Integer patientId,
                                                    @Param("orderStatus") MedicalServiceOrderStatus... statuses);


    @Query("SELECT new com.mmedic.rest.medicalRecord.dto.PreclinicalDetailDoctorDTO(detail.id, " +
            "establishment.id       AS establishmentId, " +
            "establishment.name     AS establishmentName, " +
            "establishment.address  AS establishmentAddress, " +
            "medicalService.id      AS serviceId, " +
            "medicalService.name    AS serviceName, " +
            "(CASE " +
            " WHEN medicalServiceGroup.id = 2 THEN 'ANALYSIS' " +
            " WHEN medicalServiceGroup.id = 3 THEN 'DIAGNOSE' " +
            " ELSE '' END )         AS serviceType, " +
            "detail.appointmentDate AS appointmentDate, " +
            "detail.sequenceNumber  AS sequenceNumber, " +
            "detail.resultDate      AS resultDate, " +
            "detail.result          AS result, " +
            "detail.status          AS orderStatus " +
            " ) " +
            "FROM MedicalServiceOrderDetail detail " +
            "LEFT JOIN  detail.establishment establishment " +
            "INNER JOIN  detail.medicalService medicalService " +
            "INNER JOIN  medicalService.medicalServiceGroup medicalServiceGroup " +
            "INNER JOIN  detail.medicalServiceOrder serviceOrder " +
            "INNER JOIN  serviceOrder.preclinicalRecord record " +
            "WHERE record.id = :preclinicalRecordId " +
            "AND detail.medicalService.id NOT IN (5, 6)")
    List<PreclinicalDetailDoctorDTO> findPreclinicalServiceByPreclinicalRecord(@Param("preclinicalRecordId") Integer id);

    boolean existsMedicalServiceOrderDetailByMedicalService_IdAndMedicalServiceOrder_Id(Integer serviceId, Integer orderId);

    /**
     * Update sequence number of order.
     *
     * @param id             the id of medical service order detail
     * @param sequenceNumber the sequence number
     * @return number of record is updated
     */
    @Transactional
    @Modifying
    @Query("UPDATE MedicalServiceOrderDetail msod SET msod.sequenceNumber = :sequenceNumber WHERE msod.id = :id")
    int updateSequenceNumber(@Param("id") int id, @Param("sequenceNumber") int sequenceNumber);

    /**
     * Find distance from drug store to patient.
     *
     * @param medicalRecordId   medical record id
     * @param deliveryServiceId id of medical service
     * @return list distance
     */
    @Query(nativeQuery = true,
            value = "SELECT mr.id       AS medicalRecordId, " +
                    "       mso.id      AS orderId, " +
                    "       msod.id     AS orderDetailId, " +
                    "       e.id        AS drugStoreId, " +
                    "       e.latitude  AS esLatitude, " +
                    "       e.longitude AS esLongitude, " +
                    "       p.latitude  AS paLatitude, " +
                    "       p.longitude AS paLongitude, " +
                    "       ( " +
                    "               6371 * ACOS( " +
                    "                           COS(RADIANS(p.latitude)) * COS(RADIANS(e.latitude)) * " +
                    "                           COS(RADIANS(e.longitude) - RADIANS(p.longitude)) + " +
                    "                           SIN(RADIANS(p.latitude)) * SIN(RADIANS(e.latitude)) " +
                    "               ) " +
                    "       )      AS distance " +
                    "FROM medical_record mr " +
                    "         LEFT JOIN prescription_record pr ON mr.id = pr.medical_record_id " +
                    "         LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "         LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "         LEFT JOIN establishment e ON msod.establishment_id = e.id " +
                    "         LEFT JOIN patient p on mr.patient_id = p.id " +
                    "WHERE mr.id = :medicalRecordId " +
                    "  AND msod.medical_service_id = :deliveryServiceId")
    List<DeliveryDistanceDTO> findDeliveryDistance(@Param("medicalRecordId") int medicalRecordId, @Param("deliveryServiceId") int deliveryServiceId);
}

