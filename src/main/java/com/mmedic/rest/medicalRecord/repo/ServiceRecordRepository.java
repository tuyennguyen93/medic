package com.mmedic.rest.medicalRecord.repo;

import com.mmedic.entity.ServiceRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRecordRepository extends JpaRepository<ServiceRecord, Integer> {
}
