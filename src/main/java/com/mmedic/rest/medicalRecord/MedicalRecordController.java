package com.mmedic.rest.medicalRecord;

import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.medicalRecord.dto.DeliveryDTO;
import com.mmedic.rest.medicalRecord.dto.DeliveryPriceDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordHistoryDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalServiceRecordDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalServiceRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalDetailRecordDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalTransportFeeDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalTransportTypeDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDTO;
import com.mmedic.rest.medicalRecord.dto.UpdatePreclinicalRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.UpdatePrescriptionRecordParamDTO;
import com.mmedic.spring.errors.MedicException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Medical Record Controller.
 */
@Slf4j
@Api(value = "Medical Record Controller", description = "Medical Records Management")
@RestController
@RequestMapping("/api/v1/medical-records")
public class MedicalRecordController {

    private MedicalRecordService medicalRecordService;

    public MedicalRecordController(MedicalRecordService medicalRecordService) {
        this.medicalRecordService = medicalRecordService;
    }

    @ApiOperation("Create new medical record (DONE)")
    @PostMapping
    public MedicResponse<MedicalRecordDTO> createMedicalRecord(@RequestBody @ApiParam("Medical Record Param") MedicalRecordParamDTO medicalRecordParamDTO) throws MedicException {
        return MedicResponse.okStatus(medicalRecordService.createMedicalRecord(medicalRecordParamDTO));
    }

    @ApiOperation("Get medical record history (DONE)")
    @GetMapping("/filter")
    public MedicResponse<Page<MedicalRecordHistoryDTO>> getMedicalRecordHistory(@ApiParam("Patient Id") int patientId,
                                                                                @PageableDefault(sort = "time") @ApiParam("Pagination") Pageable pageable) throws MedicException {
        // Prepare sort column
        Sort.Direction direction = Sort.Direction.ASC;
        String property = "data.appointmentDate";
        Sort sort = pageable.getSort();
        if (sort.isSorted()) {
            if (null != sort.getOrderFor("time")) {
                direction = sort.getOrderFor("time").getDirection();
            }
        }
        return MedicResponse.okStatus(medicalRecordService.getMedicalRecordHistory(patientId,
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), direction, property)));
    }

    @ApiOperation("Get medical record (DONE)")
    @GetMapping("/{id}")
    public MedicResponse<MedicalRecordDTO> getMedicalRecord(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId) throws MedicException {
        return MedicResponse.okStatus(medicalRecordService.getMedicalRecordDetail(medicalRecordId));
    }

    @ApiOperation("Get prescription of medical record (DONE)")
    @GetMapping("/{id}/prescriptions")
    public MedicResponse<List<PrescriptionRecordDetailDTO>> getPrescription(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId) throws MedicException {
        return MedicResponse.okStatus(medicalRecordService.getPrescription(medicalRecordId));
    }

    @ApiOperation("Update drug store for medical record (DONE)")
    @PutMapping("/{id}/prescriptions/{orderDetailId}")
    public MedicResponse<Boolean> updatePrescriptionForMedicalRecord(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId,
                                                                     @PathVariable("orderDetailId") @ApiParam("Service Order Detail Id") int orderDetailId,
                                                                     @RequestBody UpdatePrescriptionRecordParamDTO updatePrescriptionRecordParam) throws MedicException {
        return MedicResponse.okStatus(medicalRecordService.updatePrescriptionForMedicalRecord(medicalRecordId, orderDetailId, updatePrescriptionRecordParam));
    }

    @ApiOperation("Create preclinical delivery for medical record (DONE)")
    @PutMapping("/{id}/prescriptions/delivery")
    public MedicResponse<List<DeliveryPriceDTO>> updatePrescriptionDeliveryForMedicalRecord(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId,
                                                                                            @RequestBody DeliveryDTO delivery) throws MedicException {
        List<DeliveryPriceDTO> result = new ArrayList<>();
        if (medicalRecordService.updatePrescriptionDeliveryForMedicalRecord(medicalRecordId, delivery.isDelivery())) {
            result = medicalRecordService.getListDeliveryPrice(medicalRecordId, delivery.isDelivery());
        }
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get preclinicals of medical record (DONE)")
    @GetMapping("/{id}/preclinicals")
    public MedicResponse<List<PreclinicalDetailRecordDTO>> getPreclinical(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId) {
        return MedicResponse.okStatus(medicalRecordService.getPreclinicalDetail(medicalRecordId));
    }

    @ApiOperation("Get preclinical transport fee of medical record (DONE)")
    @GetMapping("/{id}/preclinicals/transport-fee")
    public MedicResponse<List<PreclinicalTransportFeeDTO>> getPreclinicalTransportFee(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId,
                                                                                      @RequestParam @ApiParam("Preclinical Id") int preclinicalId) {
        return MedicResponse.okStatus(medicalRecordService.getPreclinicalTransportFee(medicalRecordId, preclinicalId));
    }

    @ApiOperation("Update preclinical for medical record (DONE)")
    @PutMapping("/{id}/preclinicals/{orderDetailId}")
    public MedicResponse<Boolean> updatePreclinicalForMedicalRecord(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId,
                                                                    @PathVariable("orderDetailId") @ApiParam("Service Order Detail Id") int orderDetailId,
                                                                    @RequestBody UpdatePreclinicalRecordParamDTO preclinicalRecordParam) throws MedicException {
        return MedicResponse.okStatus(medicalRecordService.updatePreclinicalForMedicalRecord(medicalRecordId, orderDetailId, preclinicalRecordParam));
    }

    @ApiOperation("Update preclinical transport type (DONE)")
    @PutMapping("/{id}/preclinicals/{orderDetailId}/transport")
    public MedicResponse<Boolean> updatePreclinicalTransportTypeForMedicalRecord(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId,
                                                                                 @PathVariable("orderDetailId") @ApiParam("Service Order Detail Id") int orderDetailId,
                                                                                 @RequestBody PreclinicalTransportTypeDTO testType) throws MedicException {
        return MedicResponse.okStatus(medicalRecordService.updatePreclinicalTransportType(medicalRecordId, orderDetailId, testType));
    }

    @ApiOperation("Cancel treatment service (DONE)")
    @DeleteMapping("/{id}")
    public MedicResponse<Boolean> cancelTreatmentService(@PathVariable("id") int medicalRecordId) {
        return MedicResponse.okStatus(medicalRecordService.cancelTreatmentService(medicalRecordId));
    }

    @ApiOperation("Create new health care service medical record (DONE)")
    @PostMapping("/health-cares")
    public MedicResponse<MedicalServiceRecordDTO> createHealthCareServiceOrder(@RequestBody @ApiParam("Medical Health Care Param") MedicalServiceRecordParamDTO medicalServiceRecordParam) {
        return MedicResponse.okStatus(medicalRecordService.createMedicalServiceRecord(medicalServiceRecordParam));
    }

    @ApiOperation("Cancel health care service (DONE)")
    @DeleteMapping("/health-cares/{id}")
    public MedicResponse<Boolean> cancelHealthCareService(@PathVariable("id") int medicalRecordId) {
        return MedicResponse.okStatus(medicalRecordService.cancelHealthCareService(medicalRecordId));
    }
}
