package com.mmedic.rest.medicalRecord.impl;

import com.mmedic.batch.zalopayUpdateStatus.dto.PaymentResultDTO;
import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.EstablishmentWorkingSchedule;
import com.mmedic.entity.ExamineRecord;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.entity.MedicalServiceOrderDiscussion;
import com.mmedic.entity.MedicalServiceRegistry;
import com.mmedic.entity.PrescriptionRecord;
import com.mmedic.entity.ServiceRecord;
import com.mmedic.entity.TravelPricePolicy;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.enums.MedicalRecordType;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.enums.PaymentType;
import com.mmedic.enums.PreclinicalTestType;
import com.mmedic.enums.TreatmentType;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.doctor.DoctorService;
import com.mmedic.rest.doctor.dto.TreatmentBillDTO;
import com.mmedic.rest.doctor.repo.DoctorRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.rest.medicalRecord.MedicalRecordService;
import com.mmedic.rest.medicalRecord.dto.DeliveryDistanceDTO;
import com.mmedic.rest.medicalRecord.dto.DeliveryPriceDTO;
import com.mmedic.rest.medicalRecord.dto.ExamineRecordDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordHistoryDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalServiceRecordDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalServiceRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.PaymentDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalDetailRecordDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalPriceQueryDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalQueryRecordDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalRecordLittleDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalTransportFeeDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalTransportLocationDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalTransportTypeDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordLittleDTO;
import com.mmedic.rest.medicalRecord.dto.ServiceRecordDTO;
import com.mmedic.rest.medicalRecord.dto.UpdatePreclinicalRecordParamDTO;
import com.mmedic.rest.medicalRecord.dto.UpdatePrescriptionRecordParamDTO;
import com.mmedic.rest.medicalRecord.repo.ExamineRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalServiceOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.ServiceRecordRepository;
import com.mmedic.rest.medicalService.dto.OrderDetailDTO;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderDiscussionRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.rest.travelprice.repo.TravelPricePolicyRepository;
import com.mmedic.service.payment.zalopay.CreateOrder;
import com.mmedic.service.payment.zalopay.dto.PaymentOrderDTO;
import com.mmedic.service.payment.zalopay.dto.PaymentOrderResultDTO;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Medical record service implementation.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class MedicalRecordServiceImpl implements MedicalRecordService {

    private final MedicalRecordRepository medicalRecordRepository;

    private final AccountRepository accountRepository;

    private final EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    private final MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    private final MedicalServiceOrderRepository medicalServiceOrderRepository;

    private final MedicalServiceOrderDetailRepository medicalServiceOrderDetailRepository;

    private final ExamineRecordRepository examineRecordRepository;

    private final DoctorService doctorService;

    private final DoctorRepository doctorRepository;

    private final EstablishmentRepository establishmentRepository;

    private final ServiceRecordRepository serviceRecordRepository;

    private final MedicalServiceOrderDiscussionRepository medicalServiceOrderDiscussionRepository;

    private final CreateOrder createOrder;

    private final NotificationService notificationService;

    private final TravelPricePolicyRepository travelPricePolicyRepository;

    @Override
    @Transactional
    public MedicalRecordDTO createMedicalRecord(MedicalRecordParamDTO medicalRecordParamDTO) throws MedicException {
        // Get information of doctor, patient, establishment working schedule
        Account doctor = accountRepository.findDoctorAccountById(medicalRecordParamDTO.getDoctorId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_EXIST, "Doctor account don't exist"));
        Account patient = accountRepository.findPatientAccountByPatientId(medicalRecordParamDTO.getPatientId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient account don't exist"));
        EstablishmentWorkingSchedule workingSchedule = establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(medicalRecordParamDTO.getWorkingScheduleId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_WORKING_SCHEDULE_NOT_EXIST, "Working Schedule don't exist"));
        List<EstablishmentWorkingSchedule> workingSchedules = establishmentWorkingScheduleRepository.findAllByEstablishment_IdOrderById(workingSchedule.getEstablishment().getId());
        int indexOfWorkingSchedule = IntStream.range(0, workingSchedules.size()).
                filter(index -> workingSchedule.getId() == workingSchedules.get(index).getId()).
                findFirst().
                getAsInt();

        // Checking working schedule time is belong to doctor
        if (doctor.getEstablishment().getId() != workingSchedule.getEstablishment().getId()) {
            throw new MedicException(MedicException.ERROR_WORKING_SCHEDULE_NOT_BELONG_DOCTOR, "Working schedule don't belong to doctor account");
        }

        // Get TREATMENT medical service is registered for doctor
        MedicalServiceRegistry treatmentServiceRegistry = medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_MEDICAL).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_REGISTRY_TREATMENT_SERVICE, "Doctor don't registry treatment service or it's disabled"));

        // Get treatment average time
        int treatmentAverageTime = getAverageTime(doctor.getEstablishment());

        // Create medical record
        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setPatient(patient.getPatient());
        medicalRecord.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecord = medicalRecordRepository.save(medicalRecord);

        // Create medical service order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        serviceOrder = medicalServiceOrderRepository.save(serviceOrder);

        // Create medical service order detail
        List<MedicalServiceOrderDetail> medicalServiceOrderDetails = new ArrayList<>();

        // Treatment
        MedicalServiceOrderDetail treatmentServiceOrderDetail = new MedicalServiceOrderDetail();
        treatmentServiceOrderDetail.setMedicalServiceOrder(serviceOrder);
        treatmentServiceOrderDetail.setMedicalService(treatmentServiceRegistry.getMedicalService());
        treatmentServiceOrderDetail.setEstablishment(doctor.getEstablishment());
        treatmentServiceOrderDetail.setHandleBy(doctor);
        treatmentServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_SENT);

        // Calculate appointment time
        LocalDateTime startTime = LocalDateTime.of(medicalRecordParamDTO.getAppointment(), workingSchedule.getStartTime());
        LocalDateTime endTime = LocalDateTime.of(medicalRecordParamDTO.getAppointment(), workingSchedule.getEndTime());
        LocalDateTime appointmentTime = LocalDateTime.of(medicalRecordParamDTO.getAppointment(), medicalRecordParamDTO.getTime());

        if (appointmentTime.isBefore(startTime) || appointmentTime.isAfter(endTime)) {
            throw new MedicException(MedicException.ERROR_OUT_OF_WORK_SCHEDULE, "Out of working schedule");
        }
        if (appointmentTime.isBefore(LocalDateTime.now())) {
            throw new MedicException(MedicException.ERROR_DAY_IN_PAST, "Day in past");
        }

//        if (startTime.compareTo(LocalDateTime.now()) < 0) {
//            throw new MedicException(MedicException.ERROR_DAY_IN_PAST, "Day in past");
//        }
//
//        int numberOfOrder = medicalServiceOrderRepository.countTreatmentServiceOrderByInAppointmentDate(treatmentServiceOrderDetail.getEstablishment().getId(), doctor.getId(), startTime, endTime);
//        long timeLength = ChronoUnit.MINUTES.between(workingSchedule.getStartTime(), workingSchedule.getEndTime());
//        if ((int) (timeLength / treatmentAverageTime) > numberOfOrder) {
//            LocalDateTime appointmentTime = LocalDateTime.of(medicalRecordParamDTO.getAppointment(), workingSchedule.getStartTime());
//            appointmentTime = appointmentTime.plusMinutes(numberOfOrder * treatmentAverageTime);
//            treatmentServiceOrderDetail.setAppointmentDate(appointmentTime);
//            treatmentServiceOrderDetail.setSequenceNumber(Helper.formatSequenceNumber(Constants.SEQUENCE_NUMBER_PROCESS, indexOfWorkingSchedule + 1, null));
//        } else {
//            throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_TIME_FRAME_FULL, "This time frame is full");
//        }
//        medicalServiceOrderDetails.add(treatmentServiceOrderDetail);


        List<OrderDetailDTO> orderDetails = medicalServiceOrderRepository.findTreatmentServiceOrderByInAppointmentDate(treatmentServiceOrderDetail.getEstablishment().getId(), doctor.getId(), startTime, endTime);
        if (checkAppointmentTime(workingSchedule, orderDetails, medicalRecordParamDTO.getAppointment(), medicalRecordParamDTO.getTime(), treatmentAverageTime)) {
            treatmentServiceOrderDetail.setAppointmentDate(appointmentTime);
            treatmentServiceOrderDetail.setSequenceNumber(Helper.formatSequenceNumber(Constants.SEQUENCE_NUMBER_PROCESS, indexOfWorkingSchedule + 1, null));
        } else {
            throw new MedicException(MedicException.ERROR_TIME_HAD_ORDERED, "This time had ordered");
        }
        medicalServiceOrderDetails.add(treatmentServiceOrderDetail);

        // Transport
        if (medicalRecordParamDTO.getTreatmentType().equals(TreatmentType.HOME)) {
            MedicalServiceRegistry transportServiceRegistry = medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_TRANSPORT).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_REGISTRY_TRANSPORT_SERVICE, "Doctor don't registry transport service or it's disabled"));

            MedicalServiceOrderDetail transportServiceOrderDetail = new MedicalServiceOrderDetail();
            transportServiceOrderDetail.setMedicalService(transportServiceRegistry.getMedicalService());
            transportServiceOrderDetail.setMedicalServiceOrder(serviceOrder);
            transportServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_SENT);
            transportServiceOrderDetail.setEstablishment(doctor.getEstablishment());
            transportServiceOrderDetail.setHandleBy(doctor);
//            int transportPrice = bills.stream().
//                    filter(treatmentBillDTO -> treatmentBillDTO.getType().equals(Constants.TRANSPORT_FEE_TYPE)).
//                    map(TreatmentBillDTO::getPrice).findFirst().orElse(0);
//            transportServiceOrderDetail.setTxAmount(transportPrice);
            medicalServiceOrderDetails.add(transportServiceOrderDetail);
        }
        List<MedicalServiceOrderDetail> medicalServiceOrderDetailsResult = medicalServiceOrderDetailRepository.saveAll(medicalServiceOrderDetails);

        // Create examine record
        ExamineRecord examineRecord = new ExamineRecord();
        examineRecord.setMedicalRecord(medicalRecord);
        examineRecord.setMedicalServiceOrder(serviceOrder);
        examineRecordRepository.save(examineRecord);

        // Result
        MedicalRecordDTO result = new MedicalRecordDTO();
        result.setId(medicalRecord.getId());
        result.setPatientId(patient.getPatient().getId());
        result.setPatientName(patient.getPatient().getName());

        return result;
    }

    /**
     * Check appointment time has ordered.
     *
     * @param workingSchedule working schedule (start time, end time)
     * @param orderDetails    orders detail (id, appointment time)
     * @param date            appointment date
     * @param time            appointment time
     * @param averageTime     average time
     * @return true if the time is free
     */
    private boolean checkAppointmentTime(EstablishmentWorkingSchedule workingSchedule,
                                         List<OrderDetailDTO> orderDetails,
                                         LocalDate date,
                                         LocalTime time,
                                         int averageTime) {
        boolean result = false;
        LocalDateTime scheduleStartTime = LocalDateTime.of(date, workingSchedule.getStartTime());
        LocalDateTime scheduleEndTime = LocalDateTime.of(date, workingSchedule.getEndTime());
        LocalDateTime appointmentTime = LocalDateTime.of(date, time);
        LocalDateTime currentTime = LocalDateTime.now();

        if (!appointmentTime.isBefore(scheduleStartTime) && !appointmentTime.isAfter(scheduleEndTime) && !appointmentTime.isBefore(currentTime)) {
            result = true;
            for (OrderDetailDTO orderDetail : orderDetails) {
                if (null != orderDetail.getAppointmentDate() && !orderDetail.getAppointmentDate().isBefore(currentTime)) {
                    LocalDateTime examineStart = orderDetail.getAppointmentDate();
                    LocalDateTime examineEnd = orderDetail.getAppointmentDate().plusMinutes(averageTime);
                    LocalDateTime appointmentTimeEnd = appointmentTime.plusMinutes(averageTime);

                    if (appointmentTimeEnd.isAfter(examineStart) && appointmentTime.isBefore(examineEnd)) {
                        result = false;
                        break;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Page<MedicalRecordHistoryDTO> getMedicalRecordHistory(int patientId, Pageable pageable) throws MedicException {
        return medicalRecordRepository.findMedicalRecordHistoryByPatientId(patientId, pageable);
    }

    @Override
    public MedicalRecordDTO getMedicalRecordDetail(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        // Convert MedicalRecord object to DTO
        MedicalRecordDTO medicalRecordDTO = new MedicalRecordDTO();
        medicalRecordDTO.setId(medicalRecord.getId());
        medicalRecordDTO.setPatientId(medicalRecord.getPatient().getId());
        medicalRecordDTO.setPatientName(medicalRecord.getPatient().getName());
        medicalRecordDTO.setRecordType(medicalRecord.getRecordType());

        if (medicalRecord.getRecordType().equals(MedicalRecordType.TREATMENT)) {
            convertTreatmentMedicalRecord(medicalRecordDTO, medicalRecord);
        } else {
            convertServiceMedicalRecord(medicalRecordDTO, medicalRecord);
        }

        return medicalRecordDTO;
    }

    /**
     * Convert medical record to health care dto.
     *
     * @param medicalRecordDTO health care record dto
     * @param medicalRecord    medical record entity
     */
    private void convertServiceMedicalRecord(MedicalRecordDTO medicalRecordDTO, MedicalRecord medicalRecord) {
        if (null != medicalRecord.getServiceRecord()) {
            MedicalServiceOrderDetail serviceOrderDetail = medicalRecord.getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().
                    stream().findFirst().orElse(null);

            if (null != serviceOrderDetail) {
                ServiceRecordDTO serviceRecordDTO = new ServiceRecordDTO();
                serviceRecordDTO.setMedicalStaffId(serviceOrderDetail.getHandleBy().getId());
                serviceRecordDTO.setMedicalStaffName(serviceOrderDetail.getHandleBy().getName());
                serviceRecordDTO.setPackageId(serviceOrderDetail.getMedicalService().getId());
                serviceRecordDTO.setPackageName(serviceOrderDetail.getMedicalService().getName());
                serviceRecordDTO.setPaymentStatus(serviceOrderDetail.getMedicalServiceOrder().getPaymentStatus());
                serviceRecordDTO.setStatus(serviceOrderDetail.getStatus());
                serviceRecordDTO.setServiceId(serviceOrderDetail.getMedicalService().getMedicalServiceGroup().getId());
                serviceRecordDTO.setServiceName(serviceOrderDetail.getMedicalService().getMedicalServiceGroup().getName());
                serviceRecordDTO.setStartDate(serviceOrderDetail.getAppointmentDate().toLocalDate());

                if (!CollectionUtils.isEmpty(serviceOrderDetail.getMedicalServiceOrderDiscussions())) {
                    serviceOrderDetail.getMedicalServiceOrderDiscussions().forEach(discussion -> {
                        if (discussion.getMessageSender().getId() == serviceOrderDetail.getHandleBy().getId()) {
                            serviceRecordDTO.setMedicalStaffMessage(discussion.getMessage());
                        } else if (discussion.getMessageSender().getId() == medicalRecord.getPatient().getAccount().getId()) {
                            serviceRecordDTO.setPatientMessage(discussion.getMessage());
                        }
                    });
                }

                Integer price = doctorRepository.findMedicalStaffPriceByPackageId(serviceRecordDTO.getPackageId(), serviceOrderDetail.getEstablishment().getId());
                if (null != price) {
                    serviceRecordDTO.setPrice(price);
                }

                if (!CollectionUtils.isEmpty(medicalRecord.getEstablishmentRating())) {
                    serviceRecordDTO.setHasRating(medicalRecord.getEstablishmentRating().stream().
                            anyMatch(establishmentRating -> establishmentRating.getEstablishment().getId() == serviceOrderDetail.getEstablishment().getId()));
                }

                medicalRecordDTO.setServicePaymentStatus(serviceOrderDetail.getMedicalServiceOrder().getPaymentStatus());
                medicalRecordDTO.setServiceRecord(serviceRecordDTO);
            }
        }
    }

    /**
     * Convert treatment medical record dto.
     *
     * @param medicalRecordDTO medical record dto
     * @param medicalRecord    medical record entity
     */
    private void convertTreatmentMedicalRecord(MedicalRecordDTO medicalRecordDTO, MedicalRecord medicalRecord) {
        // Convert ExamineRecordDTO
        if (null != medicalRecord.getExamineRecord()) {
            MedicalServiceOrderDetail treatmentService = medicalRecord.getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().
                    stream().filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_MEDICAL).
                    findFirst().
                    orElse(null);
            if (null != treatmentService) {
                ExamineRecordDTO examineRecordDTO = new ExamineRecordDTO();
                examineRecordDTO.setDoctorId(treatmentService.getHandleBy().getId());
                examineRecordDTO.setDoctorName(treatmentService.getHandleBy().getName());
                examineRecordDTO.setAppointmentDate(treatmentService.getAppointmentDate().toLocalDate());
                examineRecordDTO.setAppointmentTime(treatmentService.getAppointmentDate().toLocalTime());
//                examineRecordDTO.setExamineTimeStart(medicalRecord.getExamineRecord().getMedicalServiceOrder().getEstablishmentWorkingSchedule().getStartTime());
//                examineRecordDTO.setExamineTimeEnd(medicalRecord.getExamineRecord().getMedicalServiceOrder().getEstablishmentWorkingSchedule().getEndTime());
                Map<String, Integer> mapSequence = Helper.getSequenceNumber(treatmentService.getSequenceNumber());
                if (!CollectionUtils.isEmpty(mapSequence) && mapSequence.get(Constants.SEQUENCE_NUMBER) > 0) {
                    examineRecordDTO.setSequenceNumber(mapSequence.get(Constants.SEQUENCE_NUMBER));
                }
                examineRecordDTO.setStatus(treatmentService.getStatus());
                examineRecordDTO.setRemind(medicalRecord.getExamineRecord().getRemind());
                examineRecordDTO.setDiagnose(medicalRecord.getExamineRecord().getDiagnose());
                examineRecordDTO.setPaymentStatus(medicalRecord.getExamineRecord().getMedicalServiceOrder().getPaymentStatus());

                if (medicalRecord.getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().
                        anyMatch(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_TRANSPORT)) {
                    examineRecordDTO.setTreatmentType(TreatmentType.HOME);
                } else {
                    examineRecordDTO.setTreatmentType(TreatmentType.CLINIC);
                }

                if (!CollectionUtils.isEmpty(medicalRecord.getEstablishmentRating())) {
                    examineRecordDTO.setHasRating(medicalRecord.getEstablishmentRating().stream().
                            anyMatch(establishmentRating -> establishmentRating.getEstablishment().getId() == treatmentService.getEstablishment().getId()));
                }

                medicalRecordDTO.setExaminePaymentStatus(medicalRecord.getExamineRecord().getMedicalServiceOrder().getPaymentStatus());
                medicalRecordDTO.setExamineRecord(examineRecordDTO);
            }
        }

        // Convert PreclinicalRecordDTOs
        List<PreclinicalRecordLittleDTO> preclinicalRecordLittleDTOS = new ArrayList<>();
        if (null != medicalRecord.getPreclinicalRecord() && null != medicalRecord.getPreclinicalRecord().getMedicalServiceOrder()) {
            List<MedicalServiceOrderDetail> preclinicalOrderDetail = medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
            preclinicalOrderDetail = preclinicalOrderDetail.stream().
                    filter(Helper::checkPreclinicalOrderDetailIsService).
                    collect(Collectors.toList());
            medicalRecordDTO.setPreclinicalPaymentStatus(medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getPaymentStatus());
            preclinicalOrderDetail.stream().forEach(medicalServiceOrderDetail -> {
                PreclinicalRecordLittleDTO preclinicalRecordLittleDTO = new PreclinicalRecordLittleDTO();
                preclinicalRecordLittleDTO.setId(medicalServiceOrderDetail.getId());
                preclinicalRecordLittleDTO.setName(medicalServiceOrderDetail.getMedicalService().getName());
                preclinicalRecordLittleDTO.setStatus(medicalServiceOrderDetail.getStatus());
                preclinicalRecordLittleDTO.setPaymentStatus(medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getPaymentStatus());
                preclinicalRecordLittleDTOS.add(preclinicalRecordLittleDTO);
            });
        }
        medicalRecordDTO.setPreclinicalRecords(preclinicalRecordLittleDTOS);

        // Convert PrescriptionRecordDTOs
        if (null != medicalRecord.getPrescriptionRecord()) {
            PrescriptionRecordLittleDTO prescriptionRecordLittleDTO = new PrescriptionRecordLittleDTO();
            prescriptionRecordLittleDTO.setId(medicalRecord.getPrescriptionRecord().getId());
            if (null != medicalRecord.getPrescriptionRecord().getMedicalServiceOrder()) {
                prescriptionRecordLittleDTO.setStatus(medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().
                        stream().
                        map(MedicalServiceOrderDetail::getStatus).
                        findFirst().
                        orElse(null));
                prescriptionRecordLittleDTO.setPaymentStatus(medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getPaymentStatus());
                medicalRecordDTO.setPrescriptionPaymentStatus(medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getPaymentStatus());
            }
            // Set number of seconds to prescription is effect
            if (null == medicalRecord.getPrescriptionRecord().getIsConfirmed() || !medicalRecord.getPrescriptionRecord().getIsConfirmed()) {
                prescriptionRecordLittleDTO.setNumberOfSecondPrescriptionEffect(getTimePrescriptionEffect(medicalRecord.getPrescriptionRecord().getCreateAt()));
            }
            medicalRecordDTO.setPrescriptionRecord(prescriptionRecordLittleDTO);
        }
    }

    /**
     * Get time prescription is effect.
     *
     * @param createAt the create time of prescription
     * @return the different time between create time and current time
     */
    private long getTimePrescriptionEffect(LocalDateTime createAt) {
        long diffTimeInSecond = 0;
        if (null != createAt) {
            diffTimeInSecond = 3 * 60 - ChronoUnit.SECONDS.between(createAt, LocalDateTime.now());
        }
        return diffTimeInSecond > 0 ? diffTimeInSecond : 0;
    }

    @Override
    public List<PrescriptionRecordDetailDTO> getPrescription(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));
        LocalDateTime priceDate = LocalDateTime.now();
        if (null != medicalRecord.getPrescriptionRecord() &&
                null != medicalRecord.getPrescriptionRecord().getMedicalServiceOrder() &&
                null != medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getPaymentDate()) {
            priceDate = medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getPaymentDate();
        }
        LocalDateTime timeForGetPrescription = LocalDateTime.now();
        return medicalRecordRepository.findPrescriptionDetails(medicalRecordId, priceDate, timeForGetPrescription);
    }

    @Override
    public List<PreclinicalDetailRecordDTO> getPreclinicalDetail(int medicalRecordId) throws MedicException {
        List<PreclinicalDetailRecordDTO> results = new ArrayList<>();

        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));
        LocalDateTime priceDate = LocalDateTime.now();
        if (null != medicalRecord.getPreclinicalRecord() &&
                null != medicalRecord.getPreclinicalRecord().getMedicalServiceOrder() &&
                null != medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getPaymentDate()) {
            priceDate = medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getPaymentDate();
        }
        List<PreclinicalQueryRecordDTO> preclinicalQueryDetails = medicalRecordRepository.findPreclinicalDetails(medicalRecordId, priceDate);

        preclinicalQueryDetails.forEach(preclinicalQueryRecordDTO -> {
            PreclinicalDetailRecordDTO detail = new PreclinicalDetailRecordDTO();
            detail.setId(preclinicalQueryRecordDTO.getId());
            detail.setEstablishmentId(preclinicalQueryRecordDTO.getEstablishmentId());
            detail.setEstablishmentName(preclinicalQueryRecordDTO.getEstablishmentName());
            detail.setEstablishmentAddress(preclinicalQueryRecordDTO.getEstablishmentAddress());
            detail.setEstablishmentPhoneNumber(preclinicalQueryRecordDTO.getEstablishmentPhoneNumber());
            detail.setEstablishmentAvatarUrl(preclinicalQueryRecordDTO.getEstablishmentAvatarUrl());
            detail.setEstablishmentRating(preclinicalQueryRecordDTO.getEstablishmentRating());
            detail.setServiceId(preclinicalQueryRecordDTO.getServiceId());
            detail.setServiceName(preclinicalQueryRecordDTO.getServiceName());
            if (null != preclinicalQueryRecordDTO.getAppointmentDate()) {
                detail.setAppointmentDate(preclinicalQueryRecordDTO.getAppointmentDate().toLocalDate());
                detail.setAppointmentTime(preclinicalQueryRecordDTO.getAppointmentDate().toLocalTime());
            }
            Map<String, Integer> mapSequence = Helper.getSequenceNumber(preclinicalQueryRecordDTO.getSequenceNumber());
            if (!CollectionUtils.isEmpty(mapSequence) && mapSequence.get(Constants.SEQUENCE_NUMBER) > 0) {
                detail.setSequenceNumber(mapSequence.get(Constants.SEQUENCE_NUMBER));
            }
            detail.setResultDate(preclinicalQueryRecordDTO.getResultDate());
            if (!StringUtils.isEmpty(preclinicalQueryRecordDTO.getResult())) {
                String[] preclinicalResults = Helper.convertNullToEmpty(preclinicalQueryRecordDTO.getResult()).split(Constants.SEPARATE_PRECLINICAL_RESULT_LINK);
                detail.setResults(Arrays.asList(preclinicalResults));
            }
            detail.setPrice(preclinicalQueryRecordDTO.getPrice());
            detail.setStatus(preclinicalQueryRecordDTO.getStatus());
            detail.setPaymentStatus(preclinicalQueryRecordDTO.getPaymentStatus());
            detail.setHasRating(preclinicalQueryRecordDTO.getHasRating());

            // Set type and re-calculate price = preclinical price + transport fee (if exist)
            detail.setPreclinicalTestType(PreclinicalTestType.PRECLINICAL);
            medicalRecordRepository.findTransportTypeByOrderDetailId(detail.getId()).ifPresent(transportTypeId -> {
                if (transportTypeId == Constants.SERVICE_TEST_HOME) {
                    detail.setPreclinicalTestType(PreclinicalTestType.HOME);
                    // calculate transport fee
                    PreclinicalTransportLocationDTO location = medicalRecordRepository.findLocationOfDoctorAndPatientAndPreclinical(medicalRecordId, detail.getEstablishmentId()).
                            orElse(null);
                    if (null != location) {
                        if ((null == location.getPreclinicalLatitude() || location.getPreclinicalLatitude() == 0) &&
                                (null == location.getPreclinicalLongitude() || location.getPreclinicalLongitude() == 0)) {
                            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Preclinical don't have location");
                        } else if ((null == location.getPatientLatitude() || location.getPatientLatitude() == 0) &&
                                (null == location.getPatientLongitude() || location.getPatientLongitude() == 0)) {
                            throw new MedicException(MedicException.ERROR_PATIENT_DONT_HAVE_LOCATION, "Patient don't have location");
                        }
                        long homeDistance = Math.round(Helper.distance(location.getPreclinicalLatitude(), location.getPreclinicalLongitude(),
                                location.getPatientLatitude(), location.getPatientLongitude()) / 1000);
                        detail.setTransportFee(calculateDistancePrice(homeDistance));
                    }
                } else if (transportTypeId == Constants.SERVICE_TEST_CLINIC) {
                    detail.setPreclinicalTestType(PreclinicalTestType.CLINIC);
                    PreclinicalTransportLocationDTO location = medicalRecordRepository.findLocationOfDoctorAndPatientAndPreclinical(medicalRecordId, detail.getEstablishmentId()).
                            orElse(null);
                    if (null != location) {
                        if ((null == location.getPreclinicalLatitude() || location.getPreclinicalLatitude() == 0) &&
                                (null == location.getPreclinicalLongitude() || location.getPreclinicalLongitude() == 0)) {
                            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Preclinical don't have location");
                        } else if ((null == location.getDoctorLatitude() || location.getDoctorLatitude() == 0) &&
                                (null == location.getDoctorLongitude() || location.getDoctorLongitude() == 0)) {
                            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Clinic don't have location");
                        }
                        long clinicDistance = Math.round(Helper.distance(location.getPreclinicalLatitude(), location.getPreclinicalLongitude(),
                                location.getDoctorLatitude(), location.getDoctorLongitude()) / 1000);
                        detail.setTransportFee(calculateDistancePrice(clinicDistance));
                    }
                }
            });

            results.add(detail);
        });
        return results;
    }

    @Override
    public List<PreclinicalTransportFeeDTO> getPreclinicalTransportFee(int medicalRecordId, int preclinicalId) throws MedicException {
        List<PreclinicalTransportFeeDTO> fees = new ArrayList<>();
        PreclinicalTransportLocationDTO location = medicalRecordRepository.findLocationOfDoctorAndPatientAndPreclinical(medicalRecordId, preclinicalId).
                orElse(null);
        if (null != location) {
            // At Preclinical
            PreclinicalTransportFeeDTO preclinical = new PreclinicalTransportFeeDTO();
            preclinical.setName("Xét nghiệm tại phòng xét nghiệm");
            preclinical.setType(PreclinicalTestType.PRECLINICAL);
            preclinical.setPrice(0);
            fees.add(preclinical);

            if ((null == location.getPreclinicalLatitude() || location.getPreclinicalLatitude() == 0) &&
                    (null == location.getPreclinicalLongitude() || location.getPreclinicalLongitude() == 0)) {
                throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Preclinical don't have location");
            } else if ((null == location.getDoctorLatitude() || location.getDoctorLatitude() == 0) &&
                    (null == location.getDoctorLongitude() || location.getDoctorLongitude() == 0)) {
                throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Clinic don't have location");
            } else if ((null == location.getPatientLatitude() || location.getPatientLatitude() == 0) &&
                    (null == location.getPatientLongitude() || location.getPatientLongitude() == 0)) {
                throw new MedicException(MedicException.ERROR_PATIENT_DONT_HAVE_LOCATION, "Patient don't have location");
            }

            // At Home
            PreclinicalTransportFeeDTO home = new PreclinicalTransportFeeDTO();
            home.setName("Xét nghiệm tại nhà");
            home.setType(PreclinicalTestType.HOME);
            long homeDistance = Math.round(Helper.distance(location.getPreclinicalLatitude(), location.getPreclinicalLongitude(),
                    location.getPatientLatitude(), location.getPatientLongitude()) / 1000);
            home.setPrice(calculateDistancePrice(homeDistance));
            fees.add(home);

            // At Clinic
            PreclinicalTransportFeeDTO clinic = new PreclinicalTransportFeeDTO();
            clinic.setName("Xét nghiệm tại phòng khám");
            clinic.setType(PreclinicalTestType.CLINIC);
            long clinicDistance = Math.round(Helper.distance(location.getPreclinicalLatitude(), location.getPreclinicalLongitude(),
                    location.getDoctorLatitude(), location.getDoctorLongitude()) / 1000);
            clinic.setPrice(calculateDistancePrice(clinicDistance));
            fees.add(clinic);
        }
        return fees;
    }

    /**
     * Calculate distance price.
     *
     * @param distanceKm distance in km
     * @return price
     */
    private int calculateDistancePrice(long distanceKm) {
        int distancePrice = 0;
        List<TravelPricePolicy> travelPricePolicies = travelPricePolicyRepository.findAll();
        travelPricePolicies = travelPricePolicies.stream().sorted(Comparator.comparingInt(TravelPricePolicy::getOrder)).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(travelPricePolicies)) {
            int order = 0;
            while (distanceKm > 0) {
                int pricePerKm = 0;
                int addKm = 0;
                if (order <= travelPricePolicies.size() - 1) {
                    pricePerKm = travelPricePolicies.get(order).getPricePerKm();
                    addKm = travelPricePolicies.get(order).getAddedKm();
                    if (order < travelPricePolicies.size() - 1) {
                        order += 1;
                    }
                }
                int km = (int) Math.min(distanceKm, addKm);
                distancePrice += km * pricePerKm;
                distanceKm -= km;
            }
        }
        return distancePrice;
    }

    @Override
    @Transactional
    public boolean payExamine(int medicalRecordId, PaymentDTO paymentDTO) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getExamineRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if (CollectionUtils.isEmpty(medicalRecord.getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        if (!medicalRecord.getRecordType().equals(MedicalRecordType.TREATMENT)) {
            throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST, "Treatment service don't exist");
        }

        MedicalServiceOrder serviceOrder = medicalRecord.getExamineRecord().getMedicalServiceOrder();

        if (PaymentStatus.SUCCESS.equals(paymentDTO.getStatus())) {
            List<MedicalServiceOrderDetail> serviceOrderDetails = medicalRecord.getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();

            MedicalServiceOrderDetail treatmentServiceOrderDetail = serviceOrderDetails.stream().
                    filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_MEDICAL).
                    findFirst().
                    orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST));

            Account doctor = treatmentServiceOrderDetail.getHandleBy();

            // Get treatment average time
            int treatmentAverageTime = getAverageTime(doctor.getEstablishment());

            TreatmentType treatmentType = TreatmentType.CLINIC;
            if (serviceOrderDetails.stream().anyMatch(serviceOrderDetail -> Constants.SERVICE_TREATMENT_TRANSPORT == serviceOrderDetail.getMedicalService().getId())) {
                treatmentType = TreatmentType.HOME;
            }
            List<TreatmentBillDTO> bills = doctorService.calculateCost(doctor.getId(), medicalRecord.getPatient().getId(), treatmentType);
            int totalPrice = bills.stream().mapToInt(TreatmentBillDTO::getPrice).sum();

            if (totalPrice != paymentDTO.getTotalAmount()) {
                throw new MedicException(MedicException.ERROR_AMOUNT_NOT_MATCH, "The total amount don't match");
            }

            // Update tx amount
            treatmentServiceOrderDetail.setTxAmount(bills.stream().
                    filter(treatmentBillDTO -> treatmentBillDTO.getType().equals(Constants.TREATMENT_FEE_TYPE)).
                    map(TreatmentBillDTO::getPrice).findFirst().orElse(0));

            if (TreatmentType.HOME.equals(treatmentType)) {
                MedicalServiceOrderDetail transportServiceOrderDetail = serviceOrderDetails.stream().
                        filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_TRANSPORT).
                        findFirst().
                        orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_TREATMENT_TRANSPORT_NOT_EXIST));
                int transportPrice = bills.stream().
                        filter(treatmentBillDTO -> treatmentBillDTO.getType().equals(Constants.TRANSPORT_FEE_TYPE)).
                        map(TreatmentBillDTO::getPrice).findFirst().orElse(0);
                transportServiceOrderDetail.setTxAmount(transportPrice);
            }

            // Update status
            serviceOrderDetails.forEach(serviceDetail -> {
                serviceDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
            });
            medicalServiceOrderDetailRepository.saveAll(serviceOrderDetails);

            // Calculate sequence number
            List<EstablishmentWorkingSchedule> workingSchedules = establishmentWorkingScheduleRepository.findAllByEstablishment_IdOrderById(treatmentServiceOrderDetail.getEstablishment().getId());
//            int sequenceNumber = 0;
            Map<String, Integer> mapSequence = Helper.getSequenceNumber(treatmentServiceOrderDetail.getSequenceNumber());
            if (!CollectionUtils.isEmpty(mapSequence) && mapSequence.get(Constants.SEQUENCE_NUMBER_INDEX) > 0) {
                Integer indexSequence = mapSequence.get(Constants.SEQUENCE_NUMBER_INDEX);
                EstablishmentWorkingSchedule workingSchedule = workingSchedules.get(indexSequence - 1);
                if (null != workingSchedule) {
                    LocalDateTime startTime = LocalDateTime.of(treatmentServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
                    LocalDateTime endTime = LocalDateTime.of(treatmentServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getEndTime());

                    List<OrderDetailDTO> orderDetails = medicalServiceOrderRepository.findTreatmentServiceOrderByInAppointmentDate(treatmentServiceOrderDetail.getEstablishment().getId(), doctor.getId(), startTime, endTime);
                    LocalDate appointDate = treatmentServiceOrderDetail.getAppointmentDate().toLocalDate();
                    LocalTime appointTime = treatmentServiceOrderDetail.getAppointmentDate().toLocalTime();
                    if (checkAppointmentTime(workingSchedule, orderDetails, appointDate, appointTime, treatmentAverageTime)) {
                        OrderDetailDTO currentOrderDetail = new OrderDetailDTO() {
                            @Override
                            public int getId() {
                                return treatmentServiceOrderDetail.getId();
                            }

                            @Override
                            public LocalDateTime getAppointmentDate() {
                                return treatmentServiceOrderDetail.getAppointmentDate();
                            }

                            @Override
                            public Integer getSequenceNumber() {
                                return treatmentServiceOrderDetail.getSequenceNumber();
                            }
                        };
                        orderDetails.add(currentOrderDetail);
                        updateSequenceNumber(orderDetails, indexSequence);
                    }

//                    sequenceNumber = medicalServiceOrderRepository.countTreatmentServiceOrderByInAppointmentDate(treatmentServiceOrderDetail.getEstablishment().getId(), doctor.getId(), startTime, endTime);
//                    long timeLength = ChronoUnit.MINUTES.between(workingSchedule.getStartTime(), workingSchedule.getEndTime());
//                    if ((int) (timeLength / treatmentAverageTime) > sequenceNumber) {
//                        // Set appointment date
//                        LocalDateTime appointmentTime = LocalDateTime.of(treatmentServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
//                        appointmentTime = appointmentTime.plusMinutes(sequenceNumber * treatmentAverageTime);
//                        treatmentServiceOrderDetail.setAppointmentDate(appointmentTime);
//
//                        // Set sequence number
//                        sequenceNumber += 1;
//                        int number = Helper.formatSequenceNumber(Constants.SEQUENCE_NUMBER_COMPLETE, indexSequence, sequenceNumber);
//                        treatmentServiceOrderDetail.setSequenceNumber(number);
//                    } else {
//                        throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_TIME_FRAME_FULL, "This time frame is full");
//                    }
                }
            }

            serviceOrder.setTotalAmount(totalPrice);
            serviceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
            if (null != paymentDTO.getPaymentResult()) {
                serviceOrder.setPaymentReference(paymentDTO.getPaymentResult().toString());
            }
            serviceOrder.setPaymentDate(paymentDTO.getPaymentDate());

            // Send notification
            notificationService.notificationTreatment(medicalRecord, treatmentServiceOrderDetail, doctor);

        } else {
            serviceOrder.setPaymentStatus(PaymentStatus.FAILED);
        }
        medicalServiceOrderRepository.save(serviceOrder);

        return true;
    }

    private void updateSequenceNumber(List<OrderDetailDTO> orderDetails, int indexSequence) {
        if (!CollectionUtils.isEmpty(orderDetails)) {
            // Sort
            orderDetails = orderDetails.stream().
                    sorted(Comparator.comparing(OrderDetailDTO::getAppointmentDate)).
                    collect(Collectors.toList());

            int sequenceNumber = 0;
            for (OrderDetailDTO orderDetail : orderDetails) {
                if (!orderDetail.getAppointmentDate().isBefore(LocalDateTime.now())) {
                    sequenceNumber++;
                    int number = Helper.formatSequenceNumber(Constants.SEQUENCE_NUMBER_COMPLETE, indexSequence, sequenceNumber);
                    medicalServiceOrderDetailRepository.updateSequenceNumber(orderDetail.getId(), number);
                } else if (null != orderDetail.getSequenceNumber()) {
                    Map<String, Integer> mapSequence = Helper.getSequenceNumber(orderDetail.getSequenceNumber());
                    Integer orderSequence = mapSequence.get(Constants.SEQUENCE_NUMBER);
                    sequenceNumber = null != orderSequence ? orderSequence : sequenceNumber + 1;
                } else {
                    sequenceNumber++;
                }
            }
        }
    }

    @Override
    @Transactional
    public PaymentOrderResultDTO createPaymentOrderExamine(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        MedicalServiceOrder serviceOrder = medicalRecord.getExamineRecord().getMedicalServiceOrder();

        if (serviceOrder.getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if (CollectionUtils.isEmpty(serviceOrder.getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        if (!medicalRecord.getRecordType().equals(MedicalRecordType.TREATMENT)) {
            throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST, "Treatment service don't exist");
        }

        List<MedicalServiceOrderDetail> serviceOrderDetails = serviceOrder.getMedicalServiceOrderDetails();

        MedicalServiceOrderDetail treatmentServiceOrderDetail = serviceOrderDetails.stream().
                filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_MEDICAL).
                findFirst().
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST));

        Account doctor = treatmentServiceOrderDetail.getHandleBy();

        // Get treatment average time
        int treatmentAverageTime = getAverageTime(doctor.getEstablishment());

        // Calculator the working time of doctor
        List<EstablishmentWorkingSchedule> workingSchedules = establishmentWorkingScheduleRepository.findAllByEstablishment_IdOrderById(treatmentServiceOrderDetail.getEstablishment().getId());
        Map<String, Integer> mapSequence = Helper.getSequenceNumber(treatmentServiceOrderDetail.getSequenceNumber());
        if (!CollectionUtils.isEmpty(mapSequence) && mapSequence.get(Constants.SEQUENCE_NUMBER_INDEX) > 0) {
            Integer indexSequence = mapSequence.get(Constants.SEQUENCE_NUMBER_INDEX);
            EstablishmentWorkingSchedule workingSchedule = workingSchedules.get(indexSequence - 1);

            if (null != workingSchedule && !workingSchedule.isDeleted()) {
                LocalDateTime startTime = LocalDateTime.of(treatmentServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
                LocalDateTime endTime = LocalDateTime.of(treatmentServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getEndTime());

                // Check time not order
                List<OrderDetailDTO> orderDetails = medicalServiceOrderRepository.findTreatmentServiceOrderByInAppointmentDate(treatmentServiceOrderDetail.getEstablishment().getId(), doctor.getId(), startTime, endTime);
                LocalDate appointDate = treatmentServiceOrderDetail.getAppointmentDate().toLocalDate();
                LocalTime appointTime = treatmentServiceOrderDetail.getAppointmentDate().toLocalTime();
                if (!checkAppointmentTime(workingSchedule, orderDetails, appointDate, appointTime, treatmentAverageTime)) {
                    throw new MedicException(MedicException.ERROR_TIME_HAD_ORDERED, "This time had ordered, in past or out of working schedule");
                }

                // Check doctor not off
                Integer countDayOff = establishmentRepository.establishmentIsOff(treatmentServiceOrderDetail.getAppointmentDate(),
                        treatmentServiceOrderDetail.getEstablishment().getId());
                if (null != countDayOff && countDayOff > 0) {
                    throw new MedicException(MedicException.ERROR_DOCTOR_IS_OFF, "Doctor is off this day");
                }

//
//                LocalDateTime startTime = LocalDateTime.of(treatmentServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
//                LocalDateTime endTime = LocalDateTime.of(treatmentServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getEndTime());
//                int sequenceNumber = medicalServiceOrderRepository.countTreatmentServiceOrderByInAppointmentDate(treatmentServiceOrderDetail.getEstablishment().getId(), doctor.getId(), startTime, endTime);
//
//                long timeLength = ChronoUnit.MINUTES.between(workingSchedule.getStartTime(), workingSchedule.getEndTime());
//                if ((int) (timeLength / treatmentAverageTime) > sequenceNumber) {
//                    // Set appointment date
//                    LocalDateTime appointmentTime = LocalDateTime.of(treatmentServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
//                    appointmentTime = appointmentTime.plusMinutes(sequenceNumber * treatmentAverageTime);
//
//                    Integer countDayOff = establishmentRepository.establishmentIsOff(appointmentTime, treatmentServiceOrderDetail.getEstablishment().getId());
//                    if (null != countDayOff && countDayOff > 0) {
//                        throw new MedicException(MedicException.ERROR_DOCTOR_IS_OFF, "Doctor is off this day");
//                    }
//                } else {
//                    throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_TIME_FRAME_FULL, "This time frame is full");
//                }
            } else {
                throw new MedicException(MedicException.ERROR_DATA_INVALID, "Can't find working schedule");
            }
        } else {
            throw new MedicException(MedicException.ERROR_DATA_INVALID, "Can't find working schedule");
        }


        TreatmentType treatmentType = TreatmentType.CLINIC;
        if (serviceOrderDetails.stream().anyMatch(serviceOrderDetail -> Constants.SERVICE_TREATMENT_TRANSPORT == serviceOrderDetail.getMedicalService().getId())) {
            treatmentType = TreatmentType.HOME;
        }
        List<TreatmentBillDTO> bills = doctorService.calculateCost(doctor.getId(), medicalRecord.getPatient().getId(), treatmentType);
        int totalPrice = bills.stream().mapToInt(TreatmentBillDTO::getPrice).sum();

        if (totalPrice <= 0) {
            throw new MedicException(MedicException.ERROR_AMOUNT_IS_ZERO, "The total amount is zero or null");
        }

        // Create item
        Map<String, Object> item = new HashMap<>();
        item.put("type", PaymentType.TREATMENT.getCode());
        item.put("medicalRecordId", medicalRecord.getId());
        item.put("orderId", serviceOrder.getId());
        item.put("doctorId", doctor.getId());
        item.put("patientId", medicalRecord.getPatient().getId());
        item.put("price", totalPrice);

        // Create embed data
        Map<String, String> embedData = createOrder.getDefaultEmbedData();

        // Init transaction
        String transaction = createOrder.generateTransaction(serviceOrder.getId());

        // Create PaymentOrderDTO
        PaymentOrderDTO paymentOrder = new PaymentOrderDTO();
        paymentOrder.setPaymentType(PaymentType.TREATMENT);
        paymentOrder.setItem(new Map[]{item});
        paymentOrder.setEmbedData(embedData);
        paymentOrder.setOrderId(serviceOrder.getId());
        paymentOrder.setPatientId(medicalRecord.getPatient().getId());
        paymentOrder.setTotalAmount(totalPrice);
        paymentOrder.setTransaction(transaction);
        paymentOrder.setDescription("[MMedic/Khám Bệnh] Thanh toán hóa đơn " + transaction);

        PaymentOrderResultDTO result = createOrder.create(paymentOrder);

        // Update transaction id to order
        if (null != result.getReturncode() && result.getReturncode() == 1) {
            JSONObject data = new JSONObject();
            data.put("apptransid", transaction);
            serviceOrder.setPaymentReference(data.toString());
            medicalServiceOrderRepository.save(serviceOrder);
        }

        return result;
    }

    @Override
    @Transactional
    public boolean updatePreclinicalForMedicalRecord(int medicalRecordId, int orderDetailId, UpdatePreclinicalRecordParamDTO preclinicalRecordParam) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }
        if (CollectionUtils.isEmpty(medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        // Get establishment schedule
        EstablishmentWorkingSchedule workingSchedule = establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(preclinicalRecordParam.getWorkingScheduleId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_WORKING_SCHEDULE_NOT_EXIST, "Working Schedule don't exist"));
        List<EstablishmentWorkingSchedule> workingSchedules = establishmentWorkingScheduleRepository.findAllByEstablishment_IdOrderById(workingSchedule.getEstablishment().getId());
        int indexOfWorkingSchedule = IntStream.range(0, workingSchedules.size()).
                filter(index -> workingSchedule.getId() == workingSchedules.get(index).getId()).
                findFirst().
                getAsInt();

        List<MedicalServiceOrderDetail> serviceOrderDetails = medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();

        // Get medical service order
        MedicalServiceOrderDetail service = serviceOrderDetails.stream().
                filter(medicalServiceOrderDetail -> Helper.checkPreclinicalOrderDetailIsService(medicalServiceOrderDetail) &&
                        medicalServiceOrderDetail.getId() == orderDetailId).
                findFirst().
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRECLINICAL_NOT_EXIST, "Can't found service order"));

        // Get establishment
        Establishment establishment;
        if (null == preclinicalRecordParam.getPreclinicalId()) {
            establishment = establishmentRepository.findById(service.getEstablishment().getId()).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Preclinical don't exist"));
        } else {
            establishment = establishmentRepository.findById(preclinicalRecordParam.getPreclinicalId()).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Preclinical don't exist"));
        }

        // Get average time
//        int serviceAverageTime = getAverageTime(establishment);

        if (!EstablishmentType.PRECLINICAL.equals(establishment.getEstablishmentType())) {
            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_IS_NOT_PRECLINICAL, "Establishment isn't preclinical");
        }
        // Checking working schedule time is belong to establishment
        if (establishment.getId() != workingSchedule.getEstablishment().getId()) {
            throw new MedicException(MedicException.ERROR_WORKING_SCHEDULE_NOT_BELONG_ESTABLISHMENT, "Working schedule don't belong to establishment");
        }

        medicalServiceRegistryRepository.findByMedicalService_IdAndEstablishment_IdAndActiveTrue(service.getMedicalService().getId(), establishment.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PRECLINICAL_NOT_REGISTRY_SERVICE, "New Preclinical don't registry the service or it's disable"));

        // Calculate appointment time
        LocalDateTime startTime = LocalDateTime.of(preclinicalRecordParam.getAppointment(), workingSchedule.getStartTime());
        LocalDateTime endTime = LocalDateTime.of(preclinicalRecordParam.getAppointment(), workingSchedule.getEndTime());
        LocalDateTime appointmentTime = LocalDateTime.of(preclinicalRecordParam.getAppointment(), preclinicalRecordParam.getTime());

        if (appointmentTime.isBefore(startTime) || appointmentTime.isAfter(endTime)) {
            throw new MedicException(MedicException.ERROR_OUT_OF_WORK_SCHEDULE, "Out of working schedule");
        }
        if (appointmentTime.isBefore(LocalDateTime.now())) {
            throw new MedicException(MedicException.ERROR_DAY_IN_PAST, "Day in past");
        }

//        List<OrderDetailDTO> orderDetails = medicalServiceOrderRepository.findServiceOrderByInAppointmentDate(establishment.getId(), startTime, endTime);
//        if (checkAppointmentTime(workingSchedule, orderDetails, preclinicalRecordParam.getAppointment(), preclinicalRecordParam.getTime(), serviceAverageTime)) {
        service.setAppointmentDate(appointmentTime);
        service.setSequenceNumber(Helper.formatSequenceNumber(Constants.SEQUENCE_NUMBER_PROCESS, indexOfWorkingSchedule + 1, null));
        service.setEstablishment(establishment);
//        } else {
//            throw new MedicException(MedicException.ERROR_TIME_HAD_ORDERED, "This time had ordered");
//        }

//        int numberOfOrder = medicalServiceOrderRepository.countServiceOrderByInAppointmentDate(establishment.getId(), startTime, endTime);
//        long timeLength = ChronoUnit.MINUTES.between(workingSchedule.getStartTime(), workingSchedule.getEndTime());
//        if ((int) (timeLength / serviceAverageTime) > numberOfOrder) {
//            LocalDateTime appointmentTime = LocalDateTime.of(preclinicalRecordParam.getAppointment(), workingSchedule.getStartTime());
//            appointmentTime = appointmentTime.plusMinutes(numberOfOrder * serviceAverageTime);
//            service.setAppointmentDate(appointmentTime);
//            service.setSequenceNumber(Helper.formatSequenceNumber(Constants.SEQUENCE_NUMBER_PROCESS, indexOfWorkingSchedule + 1, null));
//            service.setEstablishment(establishment);
//        } else {
//            throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_TIME_FRAME_FULL, "This time frame is full");
//        }
        medicalServiceOrderDetailRepository.save(service);

        return true;
    }

    @Override
    public boolean updatePreclinicalTransportType(int medicalRecordId, int orderDetailId, PreclinicalTransportTypeDTO testType) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (CollectionUtils.isEmpty(medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        List<MedicalServiceOrderDetail> serviceOrderDetails = medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();

        // Get medical service order
        MedicalServiceOrderDetail service = serviceOrderDetails.stream().
                filter(medicalServiceOrderDetail -> Helper.checkPreclinicalOrderDetailIsService(medicalServiceOrderDetail) &&
                        medicalServiceOrderDetail.getId() == orderDetailId).
                findFirst().
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRECLINICAL_NOT_EXIST, "Can't found service order"));

        MedicalServiceOrderDetail typeService = serviceOrderDetails.stream().
                filter(serviceOrderDetail -> Helper.checkPreclinicalOrderDetailIsNotService(serviceOrderDetail) &&
                        null != serviceOrderDetail.getParentId() && serviceOrderDetail.getParentId() == orderDetailId).
                findFirst().
                orElse(null);

        // Create transport order for preclinical
        PreclinicalTestType type = testType.getTestType();
        if (PreclinicalTestType.HOME.equals(type) || PreclinicalTestType.CLINIC.equals(type)) {
            int transportServiceId = getTransportServiceForPreclinical(type);
            MedicalServiceRegistry serviceRegistry = medicalServiceRegistryRepository.
                    findByEstablishment_IdAndMedicalService_IdAndActiveTrue(service.getEstablishment().getId(), transportServiceId).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_PRECLINICAL_NOT_REGISTRY_TRANSPORT_SERVICE, "Establishment don't registry transport service or it's disabled"));
            if (null == typeService) {
                typeService = new MedicalServiceOrderDetail();
                typeService.setEstablishment(service.getEstablishment());
                typeService.setStatus(MedicalServiceOrderStatus.REQUEST_SENT);
                typeService.setMedicalService(serviceRegistry.getMedicalService());
                typeService.setMedicalServiceOrder(service.getMedicalServiceOrder());
                typeService.setParentId(service.getId());
            } else {
                typeService.setEstablishment(service.getEstablishment());
                typeService.setMedicalService(serviceRegistry.getMedicalService());
            }
            medicalServiceOrderDetailRepository.save(typeService);
        } else if (null != typeService) {
            medicalServiceOrderDetailRepository.delete(typeService);
        }
        return true;
    }

    /**
     * Get transport service id for preclinical.
     *
     * @param testType test type (HOME/CLINIC/PRECLINICAL)
     * @return id of transport service
     */
    private int getTransportServiceForPreclinical(PreclinicalTestType testType) {
        switch (testType) {
            case HOME:
                return Constants.SERVICE_TEST_HOME;
            case CLINIC:
                return Constants.SERVICE_TEST_CLINIC;
            default:
                return 0;
        }
    }

    @Override
    @Transactional
    public boolean payPreclinical(int medicalRecordId, PaymentDTO paymentDTO) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if (CollectionUtils.isEmpty(medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }
        MedicalServiceOrder serviceOrder = medicalRecord.getPreclinicalRecord().getMedicalServiceOrder();

        if (PaymentStatus.SUCCESS.equals(paymentDTO.getStatus())) {
            List<MedicalServiceOrderDetail> serviceDetails = serviceOrder.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();

            LocalDateTime priceDate = LocalDateTime.now();
            List<PreclinicalPriceQueryDTO> preclinicalPriceQueryDTOs = medicalRecordRepository.findPreclinicalPrice(medicalRecordId, priceDate);
            int preclinicalPrice = preclinicalPriceQueryDTOs.stream().mapToInt(PreclinicalPriceQueryDTO::getPrice).sum();

            // Transport price
            List<MedicalServiceOrderDetail> transportServices = serviceDetails.stream().filter(Helper::checkPreclinicalOrderDetailIsNotService).
                    collect(Collectors.toList());
            List<PreclinicalTransportFeeDTO> transportFees = calculatePreclinicalTransportFeeOfMedicalRecord(medicalRecordId, transportServices);
            Map<Integer, PreclinicalTransportFeeDTO> mapOrderDetailIdAndTransportFee = transportFees.stream().
                    collect(Collectors.toMap(PreclinicalTransportFeeDTO::getOrderDetailId, transportFee -> transportFee));
            int transportPrice = transportFees.stream().mapToInt(PreclinicalTransportFeeDTO::getPrice).sum();

            int totalPrice = preclinicalPrice + transportPrice;
            if (totalPrice != paymentDTO.getTotalAmount()) {
                throw new MedicException(MedicException.ERROR_AMOUNT_NOT_MATCH, "The total amount don't match");
            }
            Map<Integer, Integer> mapServicePrice = preclinicalPriceQueryDTOs.stream().
                    collect(Collectors.toMap(PreclinicalPriceQueryDTO::getId, PreclinicalPriceQueryDTO::getPrice));

            serviceDetails.forEach(medicalServiceOrderDetail -> {
                if (Helper.checkPreclinicalOrderDetailIsService(medicalServiceOrderDetail)) {
                    medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
                    medicalServiceOrderDetail.setTxAmount(mapServicePrice.get(medicalServiceOrderDetail.getId()));
                    medicalServiceOrderDetailRepository.save(medicalServiceOrderDetail);

                    // Update sequence number
                    // Calculate appointment time
                    List<EstablishmentWorkingSchedule> workingSchedules = establishmentWorkingScheduleRepository.findAllByEstablishment_IdOrderById(medicalServiceOrderDetail.getEstablishment().getId());
//                int sequenceNumber = 0;
                    Map<String, Integer> mapSequence = Helper.getSequenceNumber(medicalServiceOrderDetail.getSequenceNumber());
                    if (!CollectionUtils.isEmpty(mapSequence) && mapSequence.get(Constants.SEQUENCE_NUMBER_INDEX) > 0) {
                        Integer indexSequence = mapSequence.get(Constants.SEQUENCE_NUMBER_INDEX);
                        EstablishmentWorkingSchedule workingSchedule = workingSchedules.get(indexSequence - 1);
                        if (null != workingSchedule) {
                            int serviceAverageTime = getAverageTime(medicalServiceOrderDetail.getEstablishment());
                            LocalDateTime startTime = LocalDateTime.of(medicalServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
                            LocalDateTime endTime = LocalDateTime.of(medicalServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getEndTime());

                            List<OrderDetailDTO> orderDetails = medicalServiceOrderRepository.findServiceOrderByInAppointmentDate(medicalServiceOrderDetail.getEstablishment().getId(), startTime, endTime);
//                            LocalDate appointDate = medicalServiceOrderDetail.getAppointmentDate().toLocalDate();
//                            LocalTime appointTime = medicalServiceOrderDetail.getAppointmentDate().toLocalTime();

//                            if (checkAppointmentTime(workingSchedule, orderDetails, appointDate, appointTime, serviceAverageTime)) {
                            List<OrderDetailDTO> lstOrderDetail = getOrderDetailDTO(serviceDetails, startTime, endTime);
                            orderDetails.addAll(lstOrderDetail);
                            updateSequenceNumber(orderDetails, indexSequence);
//                            }

//                        sequenceNumber = medicalServiceOrderRepository.countServiceOrderByInAppointmentDate(medicalServiceOrderDetail.getEstablishment().getId(), startTime, endTime);
//
//                        long timeLength = ChronoUnit.MINUTES.between(workingSchedule.getStartTime(), workingSchedule.getEndTime());
//                        if ((int) (timeLength / serviceAverageTime) > sequenceNumber) {
//                            // Set appointment date
//                            LocalDateTime appointmentTime = LocalDateTime.of(medicalServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
//                            appointmentTime = appointmentTime.plusMinutes(sequenceNumber * serviceAverageTime);
//                            medicalServiceOrderDetail.setAppointmentDate(appointmentTime);
//
//                            // Set sequence number
//                            sequenceNumber += 1;
//                            int number = Helper.formatSequenceNumber(Constants.SEQUENCE_NUMBER_COMPLETE, indexSequence, sequenceNumber);
//                            medicalServiceOrderDetail.setSequenceNumber(number);
//                        }
                        }
                    }
                } else if (Helper.checkPreclinicalOrderDetailIsNotService(medicalServiceOrderDetail)) {
                    medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
                    PreclinicalTransportFeeDTO fee = mapOrderDetailIdAndTransportFee.get(medicalServiceOrderDetail.getParentId());
                    if (null != fee) {
                        medicalServiceOrderDetail.setTxAmount(fee.getPrice());
                    }
                    medicalServiceOrderDetailRepository.save(medicalServiceOrderDetail);
                }
            });
            // Update service order detail
//            medicalServiceOrderDetailRepository.saveAll(serviceDetails);
            // Set payment status SUCCESS
            serviceOrder.setTotalAmount(totalPrice);
            serviceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
            serviceOrder.setPaymentDate(paymentDTO.getPaymentDate());
            if (null != paymentDTO.getPaymentResult()) {
                serviceOrder.setPaymentReference(paymentDTO.getPaymentResult().toString());
            }

            // Send notification
            notificationService.notificationPreclinical(medicalRecord, serviceDetails);
        } else {
            serviceOrder.setPaymentStatus(PaymentStatus.FAILED);
        }

        // Update Service Order
        medicalServiceOrderRepository.save(serviceOrder);
        return true;
    }

    private List<OrderDetailDTO> getOrderDetailDTO(List<MedicalServiceOrderDetail> orderDetails, LocalDateTime startTime, LocalDateTime endTime) {
        List<OrderDetailDTO> result = new ArrayList<>();
        orderDetails.stream().
                filter(Helper::checkPreclinicalOrderDetailIsService).
                forEach(serviceOrderDetail -> {
                    if (!serviceOrderDetail.getAppointmentDate().isBefore(startTime) &&
                            !serviceOrderDetail.getAppointmentDate().isAfter(endTime)) {
                        result.add(new OrderDetailDTO() {
                            @Override
                            public int getId() {
                                return serviceOrderDetail.getId();
                            }

                            @Override
                            public LocalDateTime getAppointmentDate() {
                                return serviceOrderDetail.getAppointmentDate();
                            }

                            @Override
                            public Integer getSequenceNumber() {
                                return serviceOrderDetail.getSequenceNumber();
                            }
                        });
                    }
                });
        return result;
    }

    /**
     * Get average time of establishment. If it's null or equal zero then throw exception.
     *
     * @param establishment the establishment
     * @return average time
     */
    private int getAverageTime(Establishment establishment) {
        if (null == establishment || null == establishment.getAverageTime() || establishment.getAverageTime() <= 0) {
            throw new MedicException(MedicException.ERROR_AVERAGE_TIME_IS_NULL, "Average time of establishment is empty");
        }
        return establishment.getAverageTime();
    }

    @Override
    @Transactional
    public PaymentOrderResultDTO createPaymentOrderPreclinical(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if (CollectionUtils.isEmpty(medicalRecord.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }
        MedicalServiceOrder serviceOrder = medicalRecord.getPreclinicalRecord().getMedicalServiceOrder();

        // Check establishment is off on appointment date
        List<MedicalServiceOrderDetail> serviceDetails = serviceOrder.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().
                filter(Helper::checkPreclinicalOrderDetailIsService).
                collect(Collectors.toList());
        serviceDetails.forEach(medicalServiceOrderDetail -> {
            if (null == medicalServiceOrderDetail.getAppointmentDate()) {
                throw new MedicException(MedicException.ERROR_APPOINTMENT_DATE_EMPTY);
            }
            List<EstablishmentWorkingSchedule> workingSchedules = establishmentWorkingScheduleRepository.findAllByEstablishment_IdOrderById(medicalServiceOrderDetail.getEstablishment().getId());
            Map<String, Integer> mapSequence = Helper.getSequenceNumber(medicalServiceOrderDetail.getSequenceNumber());
            if (!CollectionUtils.isEmpty(mapSequence) && mapSequence.get(Constants.SEQUENCE_NUMBER_INDEX) > 0) {
                Integer indexSequence = mapSequence.get(Constants.SEQUENCE_NUMBER_INDEX);
                EstablishmentWorkingSchedule workingSchedule = workingSchedules.get(indexSequence - 1);
                if (null != workingSchedule && !workingSchedule.isDeleted()) {
//                    int serviceAverageTime = getAverageTime(medicalServiceOrderDetail.getEstablishment());
//                    LocalDateTime startTime = LocalDateTime.of(medicalServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
//                    LocalDateTime endTime = LocalDateTime.of(medicalServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getEndTime());

//                    List<OrderDetailDTO> orderDetails = medicalServiceOrderRepository.findServiceOrderByInAppointmentDate(medicalServiceOrderDetail.getEstablishment().getId(), startTime, endTime);
//                    LocalDate appointDate = medicalServiceOrderDetail.getAppointmentDate().toLocalDate();
//                    LocalTime appointTime = medicalServiceOrderDetail.getAppointmentDate().toLocalTime();
//                    if (!checkAppointmentTime(workingSchedule, orderDetails, appointDate, appointTime, serviceAverageTime)) {
//                        throw new MedicException(MedicException.ERROR_TIME_HAD_ORDERED, "This time had ordered, in past or out of working schedule");
//                    }


//                    int sequenceNumber = medicalServiceOrderRepository.countServiceOrderByInAppointmentDate(medicalServiceOrderDetail.getEstablishment().getId(), startTime, endTime);
//
//                    long timeLength = ChronoUnit.MINUTES.between(workingSchedule.getStartTime(), workingSchedule.getEndTime());
//                    if ((int) (timeLength / serviceAverageTime) > sequenceNumber) {
//                        // Set appointment date
//                        LocalDateTime appointmentTime = LocalDateTime.of(medicalServiceOrderDetail.getAppointmentDate().toLocalDate(), workingSchedule.getStartTime());
//                        appointmentTime = appointmentTime.plusMinutes(sequenceNumber * serviceAverageTime);
//
//                        Integer countDayOff = establishmentRepository.establishmentIsOff(appointmentTime, medicalServiceOrderDetail.getEstablishment().getId());
//                        if (null != countDayOff && countDayOff > 0) {
//                            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_IS_OFF, "Establishment is off this day");
//                        }
//                    } else {
//                        throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_TIME_FRAME_FULL, "This time frame is full");
//                    }
                } else {
                    throw new MedicException(MedicException.ERROR_DATA_INVALID, "Can't find working schedule");
                }
            } else {
                throw new MedicException(MedicException.ERROR_DATA_INVALID, "Can't find working schedule");
            }
        });

        LocalDateTime priceDate = LocalDateTime.now();

        List<PreclinicalPriceQueryDTO> preclinicalPriceQueryDTOs = medicalRecordRepository.findPreclinicalPrice(medicalRecordId, priceDate);
        int preclinicalPrice = preclinicalPriceQueryDTOs.stream().mapToInt(PreclinicalPriceQueryDTO::getPrice).sum();

        if (preclinicalPrice <= 0) {
            throw new MedicException(MedicException.ERROR_AMOUNT_IS_ZERO, "The total amount is zero or null");
        }

        // Calculate transport price for each service
        List<MedicalServiceOrderDetail> transportServiceDetails = serviceOrder.getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().
                filter(Helper::checkPreclinicalOrderDetailIsNotService).
                collect(Collectors.toList());
        List<PreclinicalTransportFeeDTO> preclinicalTransportPrice = calculatePreclinicalTransportFeeOfMedicalRecord(medicalRecordId, transportServiceDetails);
        int transportPrice = preclinicalTransportPrice.stream().mapToInt(PreclinicalTransportFeeDTO::getPrice).sum();

        int totalPrice = preclinicalPrice + transportPrice;

        // Create item
        Map<String, Object> item = new HashMap<>();
        item.put("type", PaymentType.PRECLINICAL.getCode());
        item.put("medicalRecordId", medicalRecord.getId());
        item.put("orderId", serviceOrder.getId());
        item.put("patientId", medicalRecord.getPatient().getId());
        item.put("price", totalPrice);

        // Create embed data
        Map<String, String> embedData = createOrder.getDefaultEmbedData();

        // Init transaction
        String transaction = createOrder.generateTransaction(serviceOrder.getId());

        // Create PaymentOrderDTO
        PaymentOrderDTO paymentOrder = new PaymentOrderDTO();
        paymentOrder.setPaymentType(PaymentType.PRECLINICAL);
        paymentOrder.setItem(new Map[]{item});
        paymentOrder.setEmbedData(embedData);
        paymentOrder.setOrderId(serviceOrder.getId());
        paymentOrder.setPatientId(medicalRecord.getPatient().getId());
        paymentOrder.setTotalAmount(totalPrice);
        paymentOrder.setTransaction(transaction);
        paymentOrder.setDescription("[MMedic/Xét Nghiệm] Thanh toán hóa đơn " + transaction);

        PaymentOrderResultDTO result = createOrder.create(paymentOrder);

        // Update transaction id to order
        if (null != result.getReturncode() && result.getReturncode() == 1) {
            JSONObject data = new JSONObject();
            data.put("apptransid", transaction);
            serviceOrder.setPaymentReference(data.toString());
            medicalServiceOrderRepository.save(serviceOrder);
        }

        return result;
    }

    private List<PreclinicalTransportFeeDTO> calculatePreclinicalTransportFeeOfMedicalRecord(int medicalRecordId, List<MedicalServiceOrderDetail> transportServiceDetails) {
        List<PreclinicalTransportFeeDTO> result = new ArrayList<>();

        transportServiceDetails = transportServiceDetails.stream().
                filter(serviceOrderDetail -> null != serviceOrderDetail.getParentId()).
                collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(transportServiceDetails)) {
            List<Integer> orderDetailIds = transportServiceDetails.stream().
                    map(MedicalServiceOrderDetail::getParentId).
                    distinct().
                    collect(Collectors.toList());

            List<PreclinicalTransportLocationDTO> locations = medicalRecordRepository.
                    findAllLocationOfDoctorAndPatientAndPreclinical(medicalRecordId, orderDetailIds);
            Map<Integer, PreclinicalTransportLocationDTO> mapOrderDetailIdAndLocation = locations.stream().
                    collect(Collectors.toMap(PreclinicalTransportLocationDTO::getOrderDetailId, location -> location));

            transportServiceDetails.forEach(serviceOrderDetail -> {
                PreclinicalTransportLocationDTO location = mapOrderDetailIdAndLocation.get(serviceOrderDetail.getParentId());

                if (null != location) {
                    if ((null == location.getPreclinicalLatitude() || location.getPreclinicalLatitude() == 0) &&
                            (null == location.getPreclinicalLongitude() || location.getPreclinicalLongitude() == 0)) {
                        throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Preclinical don't have location");
                    }
                    PreclinicalTransportFeeDTO fee = new PreclinicalTransportFeeDTO();
                    fee.setOrderDetailId(location.getOrderDetailId());
                    fee.setServiceId(location.getServiceId());
                    if (serviceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TEST_HOME) {
                        if ((null == location.getPatientLatitude() || location.getPatientLatitude() == 0) &&
                                (null == location.getPatientLongitude() || location.getPatientLongitude() == 0)) {
                            throw new MedicException(MedicException.ERROR_PATIENT_DONT_HAVE_LOCATION, "Patient don't have location");
                        }
                        fee.setType(PreclinicalTestType.HOME);
                        long distance = Math.round(Helper.distance(location.getPreclinicalLatitude(), location.getPreclinicalLongitude(),
                                location.getPatientLatitude(), location.getPatientLongitude()) / 1000);
                        fee.setPrice(calculateDistancePrice(distance));
                    } else if (serviceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TEST_CLINIC) {
                        if ((null == location.getDoctorLatitude() || location.getDoctorLatitude() == 0) &&
                                (null == location.getDoctorLongitude() || location.getDoctorLongitude() == 0)) {
                            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Clinic don't have location");
                        }
                        fee.setType(PreclinicalTestType.CLINIC);
                        long distance = Math.round(Helper.distance(location.getPreclinicalLatitude(), location.getPreclinicalLongitude(),
                                location.getDoctorLatitude(), location.getDoctorLongitude()) / 1000);
                        fee.setPrice(calculateDistancePrice(distance));
                    }
                    result.add(fee);
                }
            });
        }
        return result;
    }

    @Override
    @Transactional
    public boolean updatePrescriptionForMedicalRecord(int medicalRecordId, int orderDetailId, UpdatePrescriptionRecordParamDTO updatePrescriptionRecordParam) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }
        if (CollectionUtils.isEmpty(medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        Establishment newDrugStore = establishmentRepository.findById(updatePrescriptionRecordParam.getDrugStoreId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Preclinical don't exist"));
        if (!EstablishmentType.DRUG_STORE.equals(newDrugStore.getEstablishmentType())) {
            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_IS_NOT_DRUG_STORE, "Establishment isn't drug store");
        }

        MedicalServiceOrderDetail prescriptionService = medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().
                filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getId() == orderDetailId).
                findFirst().
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRESCRIPTION_NOT_EXIST, "Can't found service order"));

        if (prescriptionService.getEstablishment().getId() != newDrugStore.getId()) {
            prescriptionService.setEstablishment(newDrugStore);
            medicalServiceOrderDetailRepository.save(prescriptionService);
        }

        return true;
    }

    @Override
    @Transactional
    public boolean updatePrescriptionDeliveryForMedicalRecord(int medicalRecordId, boolean isDelivery) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        List<MedicalServiceOrderDetail> orderDetails = medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();

        if (CollectionUtils.isEmpty(orderDetails)) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        if (orderDetails.stream().anyMatch(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY &&
                null == orderDetail.getEstablishment())) {
            throw new MedicException(MedicException.ERROR_PRESCRIPTION_DONT_HAVE_ESTABLISHMENT, "Prescription don't have drug store");
        }

        List<Establishment> establishments = orderDetails.stream().
                filter(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY).
                map(MedicalServiceOrderDetail::getEstablishment).
                distinct().collect(Collectors.toList());
        List<Integer> establishmentIds = establishments.stream().
                map(Establishment::getId).
                collect(Collectors.toList());

        MedicalServiceOrderDetail deliveryOrderDetail = medicalRecord.getPrescriptionRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().
                stream().
                filter(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_DELIVERY).
                findFirst().
                orElse(null);

        if (isDelivery && null == deliveryOrderDetail) {
            if (medicalRecord.getPatient().getLatitude() == 0 && medicalRecord.getPatient().getLongitude() == 0) {
                throw new MedicException(MedicException.ERROR_PATIENT_DONT_HAVE_LOCATION, "Patient don't have location (latitude, longitude)");
            }
            if (establishments.stream().anyMatch(establishment -> establishment.getLatitude() == 0 && establishment.getLongitude() == 0)) {
                throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Establishment don't have location (latitude, longitude)");
            }
            List<MedicalServiceRegistry> medicalServiceRegistries = medicalServiceRegistryRepository.
                    findAllByEstablishment_IdInAndMedicalService_IdAndActiveTrue(establishmentIds, Constants.SERVICE_MEDICINE_DELIVERY);

            if (medicalServiceRegistries.size() == 0 || medicalServiceRegistries.size() != establishmentIds.size()) {
                throw new MedicException(MedicException.ERROR_DRUG_STORE_NOT_REGISTRY_DELIVERY_SERVICE, "Drug store don't registry delivery service");
            }

            List<MedicalServiceOrderDetail> drugDeliveries = new ArrayList<>();
            establishments.forEach(establishment -> {
                MedicalServiceOrderDetail drugDelivery = new MedicalServiceOrderDetail();
                drugDelivery.setEstablishment(establishment);
                drugDelivery.setMedicalServiceOrder(medicalRecord.getPrescriptionRecord().getMedicalServiceOrder());
                drugDelivery.setMedicalService(medicalServiceRegistries.get(0).getMedicalService());
                drugDelivery.setStatus(MedicalServiceOrderStatus.REQUEST_SENT);
                drugDeliveries.add(drugDelivery);
            });
            medicalServiceOrderDetailRepository.saveAll(drugDeliveries);
        } else if (!isDelivery && null != deliveryOrderDetail) {
            medicalServiceOrderDetailRepository.delete(deliveryOrderDetail);
        }

        return true;
    }

    @Override
    public List<DeliveryPriceDTO> getListDeliveryPrice(int medicalRecordId, boolean isDelivery) throws MedicException {
        List<DeliveryPriceDTO> result = new ArrayList<>();

        if (isDelivery) {
            List<DeliveryDistanceDTO> lstDistance = medicalServiceOrderDetailRepository.findDeliveryDistance(medicalRecordId, Constants.SERVICE_MEDICINE_DELIVERY);

            if (!CollectionUtils.isEmpty(lstDistance)) {
                lstDistance.forEach(distance -> {
                    if (distance.getEsLatitude() == 0 && distance.getEsLongitude() == 0) {
                        throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Establishment don't have location (latitude, longtitude)");
                    } else if (distance.getPaLatitude() == 0 && distance.getPaLongitude() == 0) {
                        throw new MedicException(MedicException.ERROR_PATIENT_DONT_HAVE_LOCATION, "Patient don't have location (latitude, longtitude)");
                    }
                    DeliveryPriceDTO price = new DeliveryPriceDTO();
                    price.setMedicalRecordId(distance.getMedicalRecordId());
                    price.setOrderId(distance.getOrderId());
                    price.setOrderDetailId(distance.getOrderDetailId());
                    price.setDrugStoreId(distance.getDrugStoreId());

                    // calculate price
                    int distancePrice = 0;
                    List<TravelPricePolicy> travelPricePolicies = travelPricePolicyRepository.findAll();
                    travelPricePolicies = travelPricePolicies.stream().sorted(Comparator.comparingInt(TravelPricePolicy::getOrder)).collect(Collectors.toList());
                    if (!CollectionUtils.isEmpty(travelPricePolicies)) {
                        int order = 0;
                        double distanceKm = Math.round(distance.getDistance());
                        while (distanceKm > 0) {
                            int pricePerKm = 0;
                            int addKm = 0;
                            if (order <= travelPricePolicies.size() - 1) {
                                pricePerKm = travelPricePolicies.get(order).getPricePerKm();
                                addKm = travelPricePolicies.get(order).getAddedKm();
                                if (order < travelPricePolicies.size() - 1) {
                                    order += 1;
                                }
                            }
                            int km = (int) Math.min(distanceKm, addKm);
                            distancePrice += km * pricePerKm;
                            distanceKm -= km;
                        }
                    }
                    price.setPrice(distancePrice);
                    result.add(price);
                });
            }
        }
        return result;
    }

    @Override
    @Transactional
    public boolean payPrescription(int medicalRecordId, PaymentDTO paymentDTO) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));
        PrescriptionRecord prescriptionRecord = medicalRecord.getPrescriptionRecord();

        if (null == prescriptionRecord) {
            throw new MedicException(MedicException.ERROR_PRESCRIPTION_NOT_EXIST, "Prescription don't exist");
        }

        if (prescriptionRecord.getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if ((null == prescriptionRecord.getIsConfirmed() || !prescriptionRecord.getIsConfirmed()) &&
                !prescriptionRecord.getCreateAt().plusMinutes(3L).isBefore(LocalDateTime.now())) {
            throw new MedicException(MedicException.ERROR_PRESCRIPTION_NOT_YET_EFFECT, "The prescription not yet effect");
        }

        if (CollectionUtils.isEmpty(prescriptionRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        MedicalServiceOrder serviceOrder = prescriptionRecord.getMedicalServiceOrder();
        List<MedicalServiceOrderDetail> orderDetails = prescriptionRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails();

        if (PaymentStatus.SUCCESS.equals(paymentDTO.getStatus())) {
            LocalDateTime priceDate = LocalDateTime.now();
            // Drug price
            List<PrescriptionRecordDetailDTO> prices = medicalRecordRepository.findPrescriptionPrice(medicalRecordId, priceDate);
            Map<Integer, PrescriptionRecordDetailDTO> mapDrugOrderIdAndPrice = prices.stream().collect(Collectors.toMap(PrescriptionRecordDetailDTO::getId, prescriptionRecordDetailDTO -> prescriptionRecordDetailDTO));
            int drugPrice = prices.stream().mapToInt(value -> value.getAmount() * value.getPricePerUnit()).sum();
            // Delivery price
            int deliveryPrice = 0;
            Map<Integer, DeliveryPriceDTO> mapDeliveryOrderIdAndPrice = null;
            if (orderDetails.stream().anyMatch(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_DELIVERY)) {
                List<DeliveryPriceDTO> deliveryPrices = getListDeliveryPrice(medicalRecordId, true);
                mapDeliveryOrderIdAndPrice = deliveryPrices.stream().collect(Collectors.toMap(DeliveryPriceDTO::getOrderDetailId, price -> price));
                deliveryPrice = deliveryPrices.stream().mapToInt(DeliveryPriceDTO::getPrice).sum();
            }

            // Total price
            int totalPrice = drugPrice + deliveryPrice;
            if (totalPrice != paymentDTO.getTotalAmount()) {
                throw new MedicException(MedicException.ERROR_AMOUNT_NOT_MATCH, "The total amount don't match");
            }
            List<MedicalServiceOrderDetail> serviceOrderDetails = new ArrayList<>();
            for (MedicalServiceOrderDetail medicalServiceOrderDetail : orderDetails) {
                if (medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY) {
                    medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
                    PrescriptionRecordDetailDTO price = mapDrugOrderIdAndPrice.get(medicalServiceOrderDetail.getId());
                    if (null != price) {
                        medicalServiceOrderDetail.setTxAmount(price.getAmount() * price.getPricePerUnit());
                    }
                    serviceOrderDetails.add(medicalServiceOrderDetail);
                } else if (medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_DELIVERY) {
                    medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
                    DeliveryPriceDTO price = mapDeliveryOrderIdAndPrice.get(medicalServiceOrderDetail.getId());
                    if (null != price) {
                        medicalServiceOrderDetail.setTxAmount(price.getPrice());
                    }
                    serviceOrderDetails.add(medicalServiceOrderDetail);
                }
            }
            medicalServiceOrderDetailRepository.saveAll(serviceOrderDetails);

//            if (paymentDTO.isDrugDelivery()) {
//                MedicalServiceRegistry deliveryServiceRegistry = medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(prescriptionService.getEstablishment().getId(), Constants.MEDICINE_DELIVERY_SERVICE).
//                        orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_STORE_NOT_REGISTRY_DELIVERY_SERVICE, "Drug Store don't registry delivery service or it's disabled"));
//
//                MedicalServiceOrderDetail drugDelivery = new MedicalServiceOrderDetail();
//                drugDelivery.setMedicalServiceOrder(serviceOrder);
//                drugDelivery.setEstablishment(prescriptionService.getEstablishment());
//                drugDelivery.setMedicalService(deliveryServiceRegistry.getMedicalService());
//                drugDelivery.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
//                medicalServiceOrderDetailRepository.save(drugDelivery);
//            }

            // Set payment status SUCCESS
            serviceOrder.setTotalAmount(totalPrice);
            serviceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
            serviceOrder.setPaymentDate(paymentDTO.getPaymentDate());
            if (null != paymentDTO.getPaymentResult()) {
                serviceOrder.setPaymentReference(paymentDTO.getPaymentResult().toString());
            }

            // Notification
            notificationService.notificationPrescription(medicalRecord, serviceOrderDetails);
        } else {
            serviceOrder.setPaymentStatus(paymentDTO.getStatus());
        }
        medicalServiceOrderRepository.save(serviceOrder);
        return true;
    }

    @Override
    @Transactional
    public PaymentOrderResultDTO createPaymentOrderPrescription(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        PrescriptionRecord prescriptionRecord = medicalRecord.getPrescriptionRecord();

        if (null == prescriptionRecord) {
            throw new MedicException(MedicException.ERROR_PRESCRIPTION_NOT_EXIST, "Prescription don't exist");
        }

        if (prescriptionRecord.getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if ((null == prescriptionRecord.getIsConfirmed() || !prescriptionRecord.getIsConfirmed()) &&
                !prescriptionRecord.getCreateAt().plusMinutes(3L).isBefore(LocalDateTime.now())) {
            throw new MedicException(MedicException.ERROR_PRESCRIPTION_NOT_YET_EFFECT, "The prescription not yet effect");
        }

        if (CollectionUtils.isEmpty(prescriptionRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }
        MedicalServiceOrder serviceOrder = prescriptionRecord.getMedicalServiceOrder();
        List<MedicalServiceOrderDetail> orderDetails = prescriptionRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails();

        LocalDateTime priceDate = LocalDateTime.now();
        // Drug price
        List<PrescriptionRecordDetailDTO> prices = medicalRecordRepository.findPrescriptionPrice(medicalRecordId, priceDate);
        int drugPrice = prices.stream().mapToInt(value -> value.getAmount() * value.getPricePerUnit()).sum();
        // Delivery price
        int deliveryPrice = 0;
        if (orderDetails.stream().anyMatch(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_DELIVERY)) {
            List<DeliveryPriceDTO> deliveryPrices = getListDeliveryPrice(medicalRecordId, true);
            deliveryPrice = deliveryPrices.stream().mapToInt(DeliveryPriceDTO::getPrice).sum();
        }

        // Total price
        int totalPrice = drugPrice + deliveryPrice;

        if (drugPrice <= 0) {
            throw new MedicException(MedicException.ERROR_AMOUNT_IS_ZERO, "The total amount is zero or null");
        }

        // Create item
        Map<String, Object> item = new HashMap<>();
        item.put("type", PaymentType.PRESCRIPTION.getCode());
        item.put("medicalRecordId", medicalRecord.getId());
        item.put("orderId", serviceOrder.getId());
        item.put("patientId", medicalRecord.getPatient().getId());
        item.put("price", totalPrice);

        // Create embed data
        Map<String, String> embedData = createOrder.getDefaultEmbedData();

        // Init transaction
        String transaction = createOrder.generateTransaction(serviceOrder.getId());

        // Create PaymentOrderDTO
        PaymentOrderDTO paymentOrder = new PaymentOrderDTO();
        paymentOrder.setPaymentType(PaymentType.PRESCRIPTION);
        paymentOrder.setItem(new Map[]{item});
        paymentOrder.setEmbedData(embedData);
        paymentOrder.setOrderId(serviceOrder.getId());
        paymentOrder.setPatientId(medicalRecord.getPatient().getId());
        paymentOrder.setTotalAmount(totalPrice);
        paymentOrder.setTransaction(transaction);
        paymentOrder.setDescription("[MMedic/Đơn Thuốc] Thanh toán hóa đơn " + transaction);

        PaymentOrderResultDTO result = createOrder.create(paymentOrder);

        // Update transaction id to order
        if (null != result.getReturncode() && result.getReturncode() == 1) {
            JSONObject data = new JSONObject();
            data.put("apptransid", transaction);
            serviceOrder.setPaymentReference(data.toString());
            medicalServiceOrderRepository.save(serviceOrder);
        }

        return result;
    }

    @Override
    @Transactional
    public MedicalServiceRecordDTO createMedicalServiceRecord(MedicalServiceRecordParamDTO medicalServiceRecordParam) throws MedicException {
        Account medicalStaff = accountRepository.findMedicalStaffAccountById(medicalServiceRecordParam.getMedicalStaffId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_STAFF_NOT_EXIST, "Can't find medical staff"));
        Account patient = accountRepository.findPatientAccountByPatientId(medicalServiceRecordParam.getPatientId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient account don't exist"));

        MedicalServiceRegistry serviceRegistry = medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(medicalStaff.getEstablishment().getId(), medicalServiceRecordParam.getPackageId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_STAFF_NOT_REGISTRY_SERVICE, "Medical Staff don't registry this service or it's disabled"));

        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setPatient(patient.getPatient());
        medicalRecord.setRecordType(MedicalRecordType.SERVICE);
        medicalRecord = medicalRecordRepository.save(medicalRecord);

        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        serviceOrder = medicalServiceOrderRepository.save(serviceOrder);

        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setMedicalServiceOrder(serviceOrder);
        serviceOrderDetail.setMedicalService(serviceRegistry.getMedicalService());
        serviceOrderDetail.setEstablishment(medicalStaff.getEstablishment());
        serviceOrderDetail.setHandleBy(medicalStaff);
        serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_SENT);
        if (medicalServiceRecordParam.getStartDate().isBefore(LocalDate.now())) {
            throw new MedicException(MedicException.ERROR_DAY_IN_PAST, "Day in past");
        }
        serviceOrderDetail.setAppointmentDate(LocalDateTime.of(medicalServiceRecordParam.getStartDate(), LocalTime.of(0, 0)));
        medicalServiceOrderDetailRepository.save(serviceOrderDetail);

        if (!StringUtils.isEmpty(medicalServiceRecordParam.getMessage())) {
            MedicalServiceOrderDiscussion discussion = new MedicalServiceOrderDiscussion();
            discussion.setMessageSender(patient);
            discussion.setMedicalServiceOrderDetail(serviceOrderDetail);
            discussion.setMessage(medicalServiceRecordParam.getMessage());
            medicalServiceOrderDiscussionRepository.save(discussion);
        }

        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setMedicalRecord(medicalRecord);
        serviceRecord.setMedicalServiceOrder(serviceOrder);
        serviceRecordRepository.save(serviceRecord);

        // Result
        MedicalServiceRecordDTO result = new MedicalServiceRecordDTO();
        result.setId(medicalRecord.getId());
        result.setPatientId(patient.getPatient().getId());
        result.setPatientName(patient.getPatient().getName());

        // Notification
        notificationService.notificationHealthCare(medicalRecord, serviceOrderDetail, medicalStaff);
        return result;
    }

    @Override
    @Transactional
    public boolean payHealthCareService(int medicalRecordId, PaymentDTO paymentDTO) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getServiceRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if (CollectionUtils.isEmpty(medicalRecord.getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        if (!medicalRecord.getRecordType().equals(MedicalRecordType.SERVICE)) {
            throw new MedicException(MedicException.ERROR_ORDER_HEALTH_CARE_NOT_EXIST, "Health care service don't exist");
        }

        MedicalServiceOrder serviceOrder = medicalRecord.getServiceRecord().getMedicalServiceOrder();

        if (PaymentStatus.SUCCESS.equals(paymentDTO.getStatus())) {
            MedicalServiceOrderDetail serviceOrderDetail = medicalRecord.getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().findFirst().orElse(null);

            if (!serviceOrderDetail.getStatus().equals(MedicalServiceOrderStatus.REQUEST_ACCEPTED)) {
                throw new MedicException(MedicException.ERROR_HEALTH_CARE_SERVICE_NOT_APPROVED, "This service has not been approved yet");
            }

            Integer price = doctorRepository.findMedicalStaffPriceByPackageId(serviceOrderDetail.getMedicalService().getId(), serviceOrderDetail.getEstablishment().getId());

            if (price == null || price != paymentDTO.getTotalAmount()) {
                throw new MedicException(MedicException.ERROR_AMOUNT_NOT_MATCH, "The total amount don't match");
            }
            serviceOrderDetail.setTxAmount(price);
            serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_FULFILLED);
            serviceOrderDetail.setResultDate(LocalDateTime.now());
            medicalServiceOrderDetailRepository.save(serviceOrderDetail);

            serviceOrder.setTotalAmount(price);
            serviceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
            if (null != paymentDTO.getPaymentResult()) {
                serviceOrder.setPaymentReference(paymentDTO.getPaymentResult().toString());
            }
            serviceOrder.setPaymentDate(paymentDTO.getPaymentDate());

            // Notification
            notificationService.notificationHealthCareHadPayment(serviceOrderDetail);
        } else {
            serviceOrder.setPaymentStatus(PaymentStatus.FAILED);
        }
        medicalServiceOrderRepository.save(serviceOrder);

        return true;
    }

    @Override
    @Transactional
    public PaymentOrderResultDTO createPaymentOrderHealthCareService(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getServiceRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if (CollectionUtils.isEmpty(medicalRecord.getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        if (!medicalRecord.getRecordType().equals(MedicalRecordType.SERVICE)) {
            throw new MedicException(MedicException.ERROR_ORDER_HEALTH_CARE_NOT_EXIST, "Health care service don't exist");
        }

        MedicalServiceOrder serviceOrder = medicalRecord.getServiceRecord().getMedicalServiceOrder();

        MedicalServiceOrderDetail serviceOrderDetail = medicalRecord.getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().findFirst().orElse(null);

        if (!serviceOrderDetail.getStatus().equals(MedicalServiceOrderStatus.REQUEST_ACCEPTED)) {
            throw new MedicException(MedicException.ERROR_HEALTH_CARE_SERVICE_NOT_APPROVED, "This service has not been approved yet");
        }

        Integer price = doctorRepository.findMedicalStaffPriceByPackageId(serviceOrderDetail.getMedicalService().getId(), serviceOrderDetail.getEstablishment().getId());

        if (null == price || price <= 0) {
            throw new MedicException(MedicException.ERROR_AMOUNT_IS_ZERO, "The total amount is zero or null");
        }

        // Create item
        Map<String, Object> item = new HashMap<>();
        item.put("type", PaymentType.HEALTHCARE.getCode());
        item.put("medicalRecordId", medicalRecord.getId());
        item.put("orderId", serviceOrder.getId());
        item.put("patientId", medicalRecord.getPatient().getId());
        item.put("price", price);

        // Create embed data
        Map<String, String> embedData = createOrder.getDefaultEmbedData();

        // Init transaction
        String transaction = createOrder.generateTransaction(serviceOrder.getId());

        // Create PaymentOrderDTO
        PaymentOrderDTO paymentOrder = new PaymentOrderDTO();
        paymentOrder.setPaymentType(PaymentType.HEALTHCARE);
        paymentOrder.setItem(new Map[]{item});
        paymentOrder.setEmbedData(embedData);
        paymentOrder.setOrderId(serviceOrder.getId());
        paymentOrder.setPatientId(medicalRecord.getPatient().getId());
        paymentOrder.setTotalAmount(price);
        paymentOrder.setTransaction(transaction);
        paymentOrder.setDescription("[MMedic/CSSK] Thanh toán hóa đơn " + transaction);

        PaymentOrderResultDTO result = createOrder.create(paymentOrder);

        // Update transaction id to order
        if (null != result.getReturncode() && result.getReturncode() == 1) {
            JSONObject data = new JSONObject();
            data.put("apptransid", transaction);
            serviceOrder.setPaymentReference(data.toString());
            medicalServiceOrderRepository.save(serviceOrder);
        }

        return result;
    }

    @Override
    @Transactional
    public boolean cancelHealthCareService(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getServiceRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if (CollectionUtils.isEmpty(medicalRecord.getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        if (!medicalRecord.getRecordType().equals(MedicalRecordType.SERVICE)) {
            throw new MedicException(MedicException.ERROR_ORDER_HEALTH_CARE_NOT_EXIST, "Health care service don't exist");
        }

        MedicalServiceOrderDetail serviceOrderDetail = medicalRecord.getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().findFirst().orElse(null);

        if (null != serviceOrderDetail && !MedicalServiceOrderStatus.REQUEST_DENIED.equals(serviceOrderDetail.getStatus())) {
            serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_CANCELLED);
            serviceOrderDetail.setResultDate(LocalDateTime.now());
            medicalServiceOrderDetailRepository.save(serviceOrderDetail);

            // Notification
            notificationService.notificationHealthCareStatus(serviceOrderDetail, serviceOrderDetail.getHandleBy());
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean cancelTreatmentService(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Medical Record don't exist"));

        if (medicalRecord.getExamineRecord().getMedicalServiceOrder().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID, "This order has been paid");
        }

        if (CollectionUtils.isEmpty(medicalRecord.getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "This order don't contain order details");
        }

        if (!medicalRecord.getRecordType().equals(MedicalRecordType.TREATMENT)) {
            throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST, "Treatment service don't exist");
        }

        List<MedicalServiceOrderDetail> serviceOrderDetails = medicalRecord.getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
        serviceOrderDetails.forEach(serviceOrderDetail -> {
            serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_CANCELLED);
        });
        medicalServiceOrderDetailRepository.saveAll(serviceOrderDetails);

        return true;
    }

    @Override
    public boolean updateOrderStatus(JSONObject paymentResult) throws MedicException {
        if (null != paymentResult) {
            int orderId = 0;
            int medicalRecordId = 0;
            String transactionId = paymentResult.getString(Constants.ZALOPAY_TRANSACTION);
            log.info("### ITEM: " + paymentResult.getString(Constants.ZALOPAY_ITEM));
            JSONArray item = new JSONArray(paymentResult.getString(Constants.ZALOPAY_ITEM));
            String paymentType = Constants.EMPTY_STRING;
            if (item.length() > 0) {
                medicalRecordId = item.getJSONObject(0).getInt(Constants.ZALOPAY_ITEM_MEDICAL_RECORD_ID);
                orderId = item.getJSONObject(0).getInt(Constants.ZALOPAY_ITEM_ORDER_ID);
                paymentType = item.getJSONObject(0).getString(Constants.ZALOPAY_ITEM_TYPE);
            }

            if (orderId > 0 && medicalRecordId > 0) {
                MedicalServiceOrder serviceOrder = medicalServiceOrderRepository.findById(orderId).
                        orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_NOT_EXIST, "Order don't exist"));

                JSONObject paymentRef = new JSONObject(Helper.convertNullToEmpty(serviceOrder.getPaymentReference()));
                if (null != paymentRef.getString(Constants.ZALOPAY_TRANSACTION) &&
                        paymentRef.getString(Constants.ZALOPAY_TRANSACTION).equals(transactionId)) {
                    int amount = paymentResult.getInt(Constants.ZALOPAY_AMOUNT);
                    long time = paymentResult.getLong(Constants.ZALOPAY_SERVER_TIME);
                    LocalDateTime paymentDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());

                    PaymentDTO paymentDTO = new PaymentDTO();
                    paymentDTO.setStatus(PaymentStatus.SUCCESS);
                    paymentDTO.setTotalAmount(amount);
                    paymentDTO.setPaymentDate(paymentDate);
                    paymentDTO.setPaymentResult(paymentResult);

                    log.info("## Update status for order: " + paymentType);
                    if (PaymentType.TREATMENT.getCode().equals(paymentType)) {
                        return payExamine(medicalRecordId, paymentDTO);
                    } else if (PaymentType.PRECLINICAL.getCode().equals(paymentType)) {
                        return payPreclinical(medicalRecordId, paymentDTO);
                    } else if (PaymentType.PRESCRIPTION.getCode().equals(paymentType)) {
                        return payPrescription(medicalRecordId, paymentDTO);
                    } else if (PaymentType.HEALTHCARE.getCode().equals(paymentType)) {
                        return payHealthCareService(medicalRecordId, paymentDTO);
                    }
                } else {
                    throw new MedicException(MedicException.ERROR_PAYMENT_TRANSACTION_DIFF, "Transaction is different");
                }
            }
        }
        return false;
    }

    @Override
    public void batchUpdateOrderStatus(List<? extends PaymentResultDTO> paymentResults) throws MedicException {
        if (!CollectionUtils.isEmpty(paymentResults)) {
            List<Integer> ids = paymentResults.stream().filter(paymentResultDTO -> !Objects.isNull(paymentResultDTO)).
                    map(PaymentResultDTO::getOrderId).
                    collect(Collectors.toList());
            List<MedicalServiceOrder> serviceOrders = medicalServiceOrderRepository.findByIdIn(ids);


            if (!CollectionUtils.isEmpty(serviceOrders)) {
                Map<Integer, PaymentResultDTO> mapData = paymentResults.stream().
                        collect(Collectors.toMap(PaymentResultDTO::getOrderId, paymentResultDTO -> paymentResultDTO));
                serviceOrders.forEach(serviceOrder -> {
                    PaymentResultDTO paymentResultDTO = mapData.get(serviceOrder.getId());
                    PaymentType paymentType = getPaymentType(serviceOrder);
                    if (null != paymentResultDTO && null != paymentType) {
                        int medicalRecordId = paymentResultDTO.getMedicalRecordId();

                        PaymentDTO paymentDTO = new PaymentDTO();
                        paymentDTO.setStatus(paymentResultDTO.getPaymentStatus());
                        paymentDTO.setTotalAmount(paymentResultDTO.getAmount());
                        paymentDTO.setPaymentDate(paymentResultDTO.getPaymentDate());
                        if (null != paymentResultDTO.getPaymentReference()) {
                            paymentDTO.setPaymentResult(new JSONObject(paymentResultDTO.getPaymentReference()));
                        }

                        log.info("## Update status for order: " + paymentType);
                        if (PaymentType.TREATMENT.equals(paymentType)) {
                            payExamine(medicalRecordId, paymentDTO);
                        } else if (PaymentType.PRECLINICAL.equals(paymentType)) {
                            payPreclinical(medicalRecordId, paymentDTO);
                        } else if (PaymentType.PRESCRIPTION.equals(paymentType)) {
                            payPrescription(medicalRecordId, paymentDTO);
                        } else if (PaymentType.HEALTHCARE.equals(paymentType)) {
                            payHealthCareService(medicalRecordId, paymentDTO);
                        }
                    }
                });
            }
        }
    }

    /**
     * Get payment type base on record.
     *
     * @param serviceOrder service order
     * @return payment type
     */
    private PaymentType getPaymentType(MedicalServiceOrder serviceOrder) {
        if (null != serviceOrder.getExamineRecord()) {
            return PaymentType.TREATMENT;
        } else if (null != serviceOrder.getPreclinicalRecord()) {
            return PaymentType.PRECLINICAL;
        } else if (null != serviceOrder.getPrescriptionRecord()) {
            return PaymentType.PRESCRIPTION;
        } else if (null != serviceOrder.getServiceRecord()) {
            return PaymentType.HEALTHCARE;
        }
        return null;
    }
}
