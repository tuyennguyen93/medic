package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.MedicalRecordType;
import com.mmedic.enums.PaymentStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("Medical Record")
public class MedicalRecordDTO {

    @ApiModelProperty("Medical Record Id")
    private int id;

    @ApiModelProperty("Patient Id")
    private int patientId;

    @ApiModelProperty("Patient Name")
    private String patientName;

    private MedicalRecordType recordType;

    private PaymentStatus examinePaymentStatus;

    private PaymentStatus prescriptionPaymentStatus;

    private PaymentStatus preclinicalPaymentStatus;

    private PaymentStatus servicePaymentStatus;

    @ApiModelProperty("Examine Record")
    private ExamineRecordDTO examineRecord;

    @ApiModelProperty("Prescription Records")
    private PrescriptionRecordLittleDTO prescriptionRecord;

    @ApiModelProperty("Preclinical Records")
    private List<PreclinicalRecordLittleDTO> preclinicalRecords;

    private ServiceRecordDTO serviceRecord;

}
