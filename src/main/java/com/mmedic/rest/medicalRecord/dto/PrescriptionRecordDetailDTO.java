package com.mmedic.rest.medicalRecord.dto;

public interface PrescriptionRecordDetailDTO {

    int getId();

    int getDrugId();

    String getName();

    String getUsage();

    String getNote();

    String getUnit();

    Integer getPricePerUnit();

    Integer getAmount();

    int getDrugStoreId();

    String getDrugStoreName();

    Float getDrugRating();

    boolean getHasRating();
}
