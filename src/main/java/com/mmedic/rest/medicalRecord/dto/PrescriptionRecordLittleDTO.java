package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PrescriptionRecordLittleDTO {

    private int id;

    private MedicalServiceOrderStatus status;

    private PaymentStatus paymentStatus;

    private long numberOfSecondPrescriptionEffect;

}
