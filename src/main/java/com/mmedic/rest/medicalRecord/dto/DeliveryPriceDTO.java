package com.mmedic.rest.medicalRecord.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DeliveryPriceDTO {

    private int medicalRecordId;

    private int orderId;

    private int orderDetailId;

    private int drugStoreId;

    private int price;
}
