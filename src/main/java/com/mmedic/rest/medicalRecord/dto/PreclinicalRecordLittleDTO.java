package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("Preclinical Record")
public class PreclinicalRecordLittleDTO {

    @ApiModelProperty("Preclinical Id")
    private int id;

    @ApiModelProperty("Preclinical Name")
    private String name;

    @ApiModelProperty("Preclinical Status")
    private MedicalServiceOrderStatus status;

    private PaymentStatus paymentStatus;
}
