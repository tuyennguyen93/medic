package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.PaymentStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class MedicalServiceRecordDTO {

    @ApiModelProperty("Medical Record Id")
    private int id;

    @ApiModelProperty("Patient Id")
    private int patientId;

    @ApiModelProperty("Patient Name")
    private String patientName;

    private PaymentStatus servicePaymentStatus;

    private ServiceRecordDTO serviceRecord;
}
