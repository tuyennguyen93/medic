package com.mmedic.rest.medicalRecord.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mmedic.enums.PreclinicalTestType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UpdatePreclinicalRecordParamDTO {

    @ApiModelProperty("Preclinical Id")
    private Integer preclinicalId;

    @ApiModelProperty("Working Schedule Id")
    private int workingScheduleId;

    @ApiModelProperty("Appointment Date")
    private LocalDate appointment;

    @ApiModelProperty("Appointment Time")
    @JsonFormat(pattern = "H:m")
    private LocalTime time;

//    @ApiModelProperty("Preclinical Test Type")
//    private PreclinicalTestType type;

}
