package com.mmedic.rest.medicalRecord.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mmedic.enums.MedicalRecordType;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

@ApiModel("Medical Record History")
public interface MedicalRecordHistoryDTO {

    @ApiModelProperty("Medical Record Id")
    int getId();

    @ApiModelProperty("Medical Record Type")
    MedicalRecordType getType();

    @ApiModelProperty("Medical Department Name")
    String getMedicalDepartmentName();

    @ApiModelProperty("Health Care Service Name")
    String getServiceName();

    @ApiModelProperty("Examine Date")
    @JsonFormat(pattern="yyyy-MM-dd")
    LocalDateTime getAppointmentDate();

    @ApiModelProperty("Examine Status")
    MedicalServiceOrderStatus getStatus();

    PaymentStatus getPaymentStatus();

}
