package com.mmedic.rest.medicalRecord.dto;

public interface DeliveryDistanceDTO {

    int getMedicalRecordId();

    int getOrderId();

    int getOrderDetailId();

    int getDrugStoreId();

    double getEsLatitude();

    double getEsLongitude();

    double getPaLatitude();

    double getPaLongitude();

    double getDistance();
}
