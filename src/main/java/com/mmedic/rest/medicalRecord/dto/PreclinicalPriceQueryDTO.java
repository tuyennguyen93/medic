package com.mmedic.rest.medicalRecord.dto;

public interface PreclinicalPriceQueryDTO {

    int getId();

    Integer getServiceId();

    Integer getPrice();
}
