package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.PaymentStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.json.JSONObject;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("Payment Information")
public class PaymentDTO {

    @ApiModelProperty("Payment Status")
    private PaymentStatus status;

    @ApiModelProperty("Total Amount")
    private int totalAmount;

//    @ApiModelProperty("Transaction Id")
//    private String transactionId;

//    @ApiModelProperty("Drug Delivery (Use to prescription payment)")
//    private boolean drugDelivery;

    @ApiModelProperty("Payment Result")
    private JSONObject paymentResult;

    @ApiModelProperty("Payment Date")
    private LocalDateTime paymentDate;
}
