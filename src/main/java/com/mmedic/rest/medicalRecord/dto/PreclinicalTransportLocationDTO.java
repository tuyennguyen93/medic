package com.mmedic.rest.medicalRecord.dto;

public interface PreclinicalTransportLocationDTO {

    Integer getOrderDetailId();

    Integer getServiceId();

    Double getDoctorLatitude();

    Double getDoctorLongitude();

    Double getPreclinicalLatitude();

    Double getPreclinicalLongitude();

    Double getPatientLatitude();

    Double getPatientLongitude();
}
