package com.mmedic.rest.medicalRecord.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DeliveryDTO {

    private boolean isDelivery;
}
