package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PreclinicalDetailDoctorDTO {

    private int id;

    private Integer establishmentId;

    private String establishmentName;

    private String establishmentAddress;

    private Integer serviceId;

    private String serviceName;

    private String serviceType;

    private LocalDateTime appointmentDate;

    private Integer sequenceNumber;

    private LocalTime startTime;

    private LocalTime endTime;

    private LocalDateTime resultDate;

    private String result;

    private String[] multiResult;

    private MedicalServiceOrderStatus orderStatus;

    private Integer workTimeId;


    public PreclinicalDetailDoctorDTO(int id,
                                      Integer establishmentId,
                                      String establishmentName,
                                      String establishmentAddress,
                                      Integer serviceId,
                                      String serviceName,
                                      String serviceType,
                                      LocalDateTime appointmentDate,
                                      Integer sequenceNumber,
                                      LocalDateTime resultDate,
                                      String result,
                                      MedicalServiceOrderStatus orderStatus) {
        this.id = id;
        this.establishmentId = establishmentId;
        this.establishmentName = establishmentName;
        this.establishmentAddress = establishmentAddress;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.serviceType = serviceType;
        this.appointmentDate = appointmentDate;
        this.sequenceNumber = sequenceNumber;
        this.resultDate = resultDate;
        this.result = result;
        if (StringUtils.isNotEmpty(result)){
            multiResult = result.trim().split("  ");
        }
        this.orderStatus = orderStatus;
    }
}
