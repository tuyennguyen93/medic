package com.mmedic.rest.medicalRecord.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PrescriptionRecordDetailDoctorDTO {

    private Integer id;

    private Integer establishmentId;

    private String establishmentName;

    private Integer drugUnitId;

    private Integer amount;

    private String usage;

    private String note;
}
