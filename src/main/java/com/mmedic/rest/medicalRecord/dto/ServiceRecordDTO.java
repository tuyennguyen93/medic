package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ServiceRecordDTO {

    private int serviceId;

    private String serviceName;

    private int packageId;

    private String packageName;

    private LocalDate startDate;

    @ApiModelProperty("Medical Staff Id")
    private int medicalStaffId;

    @ApiModelProperty("Medical Staff Name")
    private String medicalStaffName;

    private int price;

    private PaymentStatus paymentStatus;

    private MedicalServiceOrderStatus status;

    private String patientMessage;

    private String medicalStaffMessage;

    private boolean hasRating;
}
