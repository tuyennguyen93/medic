package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.PreclinicalTestType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@ApiModel("Preclinical Detail Record")
public class PreclinicalDetailRecordDTO extends PreclinicalRecordLittleDTO {

    @ApiModelProperty("Establishment Id")
    private Integer establishmentId;

    @ApiModelProperty("Establishment Name")
    private String establishmentName;

    @ApiModelProperty("Establishment Address")
    private String establishmentAddress;

    private String establishmentPhoneNumber;

    private String establishmentAvatarUrl;

    private Float establishmentRating;

    @ApiModelProperty("Preclinical Id")
    private Integer serviceId;

    @ApiModelProperty("Preclinical Name")
    private String serviceName;

//    @ApiModelProperty("Schedule Time")
//    private ScheduleTimeDTO scheduleTime;

    @ApiModelProperty("Appointment Date")
    private LocalDate appointmentDate;

    @ApiModelProperty("Sequence Number")
    private Integer sequenceNumber;

    @ApiModelProperty("Test Time")
    private LocalTime appointmentTime;

    @ApiModelProperty("Result Date")
    private LocalDateTime resultDate;

    @ApiModelProperty("Result Link")
    private List<String> results;

    @ApiModelProperty("Price")
    private Integer price;

    private boolean hasRating;

    private PreclinicalTestType preclinicalTestType;

    private int transportFee;
}
