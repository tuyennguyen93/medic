package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;

import java.time.LocalDateTime;

public interface PreclinicalQueryRecordDTO {

    int getId();

    Integer getEstablishmentId();

    String getEstablishmentName();

    String getEstablishmentAddress();

    String getEstablishmentPhoneNumber();

    String getEstablishmentAvatarUrl();

    Float getEstablishmentRating();

    Integer getServiceId();

    String getServiceName();

    LocalDateTime getAppointmentDate();

    Integer getSequenceNumber();

    Integer getPrice();

    LocalDateTime getResultDate();

    String getResult();

    MedicalServiceOrderStatus getStatus();

    PaymentStatus getPaymentStatus();

    boolean getHasRating();
}
