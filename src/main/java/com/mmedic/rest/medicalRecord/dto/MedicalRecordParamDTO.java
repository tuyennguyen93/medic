package com.mmedic.rest.medicalRecord.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mmedic.enums.TreatmentType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("Request Create New Medical Record ")
public class MedicalRecordParamDTO {

    @ApiModelProperty("Patient Id")
    private int patientId;

    @ApiModelProperty("Examine Type")
    private TreatmentType treatmentType;

    @ApiModelProperty("Doctor Id")
    private int doctorId;

    @ApiModelProperty("Working Schedule Id")
    private int workingScheduleId;

    @ApiModelProperty("Appointment Date")
    private LocalDate appointment;

    @ApiModelProperty("Appointment Time")
    @JsonFormat(pattern = "H:m")
    private LocalTime time;

//    @ApiModelProperty("Transaction Id")
//    private int transactionId;
}
