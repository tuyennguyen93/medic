package com.mmedic.rest.medicalRecord.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class MedicalServiceRecordParamDTO {

    @ApiModelProperty("Patient Id")
    private int patientId;

    @ApiModelProperty("Medical Staff Id")
    private int medicalStaffId;

    @ApiModelProperty("Service Package Id")
    private int packageId;

    @ApiModelProperty("Start Date")
    private LocalDate startDate;

    @ApiModelProperty("Patient message")
    private String message;
}
