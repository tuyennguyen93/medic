package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.enums.TreatmentType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("Examine record")
public class ExamineRecordDTO {

    @ApiModelProperty("Appointment Date")
    private LocalDate appointmentDate;

//    @ApiModelProperty("Schedule Time")
//    private ScheduleTimeDTO scheduleTime;

    @ApiModelProperty("Sequence Number")
    private Integer sequenceNumber;

//    @ApiModelProperty("Examine Time Start")
//    private LocalTime examineTimeStart;
//
//    @ApiModelProperty("Examine Time Start")
//    private LocalTime examineTimeEnd;

    private LocalTime appointmentTime;

    @ApiModelProperty("Examine Status")
    private MedicalServiceOrderStatus status;

    private PaymentStatus paymentStatus;

    @ApiModelProperty("Doctor remind")
    private String remind;

    @ApiModelProperty("Doctor diagnose")
    private String diagnose;

    @ApiModelProperty("Doctor Id")
    private int doctorId;

    @ApiModelProperty("Doctor Name")
    private String doctorName;

    @ApiModelProperty("Treatment Type")
    private TreatmentType treatmentType;

    private boolean hasRating;
}
