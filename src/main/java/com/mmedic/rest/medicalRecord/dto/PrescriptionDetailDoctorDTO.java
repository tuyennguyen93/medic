package com.mmedic.rest.medicalRecord.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PrescriptionDetailDoctorDTO {

    private Integer id;

    private String drugName;

    private Integer drugId;

    private Integer unitId;

    private String unitName;

    private Integer drugStoreId;

    private String drugStoreName;

    private String usage;

    private String note;

    private Integer amount;

}
