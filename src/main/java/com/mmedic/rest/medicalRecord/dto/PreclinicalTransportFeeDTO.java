package com.mmedic.rest.medicalRecord.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmedic.enums.PreclinicalTestType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PreclinicalTransportFeeDTO {

    @JsonIgnore
    private Integer orderDetailId;

    @JsonIgnore
    private Integer serviceId;

    private String name;

    private PreclinicalTestType type;

    private int price;
}
