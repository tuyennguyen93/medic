package com.mmedic.rest.medicalRecord.dto;

import com.mmedic.enums.PreclinicalTestType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PreclinicalTransportTypeDTO {

    private PreclinicalTestType testType;
}
