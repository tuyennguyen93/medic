package com.mmedic.rest.authentication.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Otp Request.
 *
 * @author hungp
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("OTP")
public class OtpDTO {

    @NotNull
    @ApiModelProperty(value = "Patient Id", required = true)
    private int patientId;
}
