package com.mmedic.rest.authentication.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
@ApiModel("OTP Verification")
public class OtpVerifyDTO extends OtpDTO {

    @ApiModelProperty(value = "Otp value (number)", required = true)
    private String verifyValue;;
}
