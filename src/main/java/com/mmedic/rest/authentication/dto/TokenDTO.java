package com.mmedic.rest.authentication.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Token request.
 */
@Getter
@Setter
public class TokenDTO {

    @NotNull
    private String token;

    @Override
    public String toString() {
        return "TokenRequest{" +
                "token='" + token + '\'' +
                "}";
    }
}
