package com.mmedic.rest.authentication;

import com.mmedic.entity.Account;
import com.mmedic.entity.AuthMethod;
import com.mmedic.enums.AuthType;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.authentication.dto.LoginRequestDTO;
import com.mmedic.rest.authentication.dto.OtpDTO;
import com.mmedic.rest.authentication.dto.TokenDTO;
import com.mmedic.rest.authentication.dto.OtpVerifyDTO;
import com.mmedic.security.OtpAuthenticationToken;
import com.mmedic.security.jwt.AccessToken;
import com.mmedic.security.jwt.TokenProvider;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Authentication Controller.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/authentications")
@Api(value = "Authentication Controller", description = "It have methods use to authentication")
public class AuthenticationController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private AccountService accountService;

    public AuthenticationController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder,
                                    AccountService accountService) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.accountService = accountService;
    }

    /**
     * Authentication method for web admin.
     *
     * @param loginRequestDTO it's contain username and password of user.
     * @return Access token
     */
    @ApiOperation(value = "Authentication method for web admin (DONE)")
    @PostMapping("/admin")
    public MedicResponse<AccessToken> authorizeAdmin(@Valid @RequestBody LoginRequestDTO loginRequestDTO) {
        UsernamePasswordAuthenticationToken authenticationToken = createUsernamePasswordAuthenticationToken(loginRequestDTO);

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        if (!Helper.authenticationHasAnyRole(authentication, Constants.ROLE_SUPER_ADMIN, Constants.ROLE_ADMIN)) {
            throw new MedicException(MedicException.ERROR_ADMIN_ACCOUNT_NOT_HAVE_ROLE_ADMIN, "This account don't have admin role");
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginRequestDTO.getRememberMe() == null) ? false : loginRequestDTO.getRememberMe();

        AccessToken accessToken = tokenProvider.createAccessToken(authentication, rememberMe);
        return MedicResponse.okStatus(accessToken);
    }

    /**
     * Authentication method for web staff.
     *
     * @param loginRequestDTO it's contain username and password of user.
     * @return Access token
     */
    @ApiOperation(value = "Authentication method for web staff (doctor, preclinical, drug store (DONE)")
    @PostMapping("/staff")
    public MedicResponse<AccessToken> authorizeStaff(@Valid @RequestBody LoginRequestDTO loginRequestDTO) {
        UsernamePasswordAuthenticationToken authenticationToken = createUsernamePasswordAuthenticationToken(loginRequestDTO);

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        if (Helper.authenticationHasAnyRole(authentication, Constants.ROLE_SUPER_ADMIN, Constants.ROLE_ADMIN, Constants.ROLE_PATIENT)) {
            throw new MedicException(MedicException.ERROR_STAFF_ACCOUNT_NOT_HAVE_ROLE_STAFF, "This account don't have staff role");
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginRequestDTO.getRememberMe() == null) ? false : loginRequestDTO.getRememberMe();

        AccessToken accessToken = tokenProvider.createAccessToken(authentication, rememberMe);
        return MedicResponse.okStatus(accessToken);
    }

    @ApiOperation("Authentication method for app user (patient) (DONE)")
    @PostMapping("/patient")
    public MedicResponse<AccessToken> authorizePatient(@RequestBody @ApiParam("OTP Verification Value") OtpVerifyDTO otpVerifyDTO) {
        Account account = accountService.findPatientAccountByPatientId(otpVerifyDTO.getPatientId());
        AuthMethod phoneAuth = account.getAuthMethods().stream().
                filter(authMethod -> authMethod.getAuthType().equals(AuthType.PHONE_NUMBER)).
                findFirst().orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_HAVE_PHONE_NUMBER_AUTH_METHOD, "Not found phone number authentication"));
        OtpAuthenticationToken authenticationToken =
                new OtpAuthenticationToken(phoneAuth.getAuthData1(), otpVerifyDTO.getVerifyValue(), otpVerifyDTO.getPatientId());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        AccessToken accessToken = tokenProvider.createAccessToken(authentication, true);
        return MedicResponse.okStatus(accessToken);
    }

    @ApiOperation("Generation OTP code and send it to user (DONE)")
    @PostMapping("/patient/sns")
    public MedicResponse<Boolean> connect(@RequestBody @ApiParam("OTP patient information") OtpDTO otpDTO) throws MedicException {
        return MedicResponse.okStatus(accountService.generateAndSendTOTP(otpDTO));
    }

    /**
     * Create UsernamePasswordAuthenticationToken object.
     *
     * @param loginRequestDTO it's contain username and password of user.
     * @return UsernamePasswordAuthenticationToken
     */
    private UsernamePasswordAuthenticationToken createUsernamePasswordAuthenticationToken(LoginRequestDTO loginRequestDTO) {
        return new UsernamePasswordAuthenticationToken(loginRequestDTO.getUsername(), loginRequestDTO.getPassword());
    }

    /**
     * Refresh access token of user base on refresh token.
     *
     * @param tokenDTO the refresh token value
     * @return new access token.
     * @throws MedicException exception
     */
    @ApiOperation(value = "Refresh access token (DONE)")
    @PutMapping
    public MedicResponse<AccessToken> refreshAccessToken(
            @Valid @RequestBody @ApiParam("Refresh token") TokenDTO tokenDTO) throws MedicException {
        AccessToken accessToken = tokenProvider.refreshAccessToken(tokenDTO.getToken());
        return MedicResponse.okStatus(accessToken);
    }

    /**
     * Revoke access token.
     *
     * @param tokenDTO the access token need to revoke
     * @return true if revoke success.
     */
    @ApiOperation(value = "Remove access token (DONE)")
    @DeleteMapping
    public MedicResponse<Boolean> revokeToken(
            @Valid @RequestBody @ApiParam("Access token") TokenDTO tokenDTO) {
        return MedicResponse.okStatus(tokenProvider.revokeToken(tokenDTO.getToken()));
    }
}
