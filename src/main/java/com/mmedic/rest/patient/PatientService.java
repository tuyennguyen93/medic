package com.mmedic.rest.patient;

import com.mmedic.rest.doctor.dto.PatientDiseaseHistoryDTO;
import com.mmedic.rest.drug.dto.AllergyDrugDTO;
import com.mmedic.rest.patient.dto.*;

import java.util.List;

/**
 * Patient Service.
 */
public interface PatientService {

    /**
     * Get patient information.
     *
     * @param id patient id
     * @return
     */
    PatientDTO getPatientInfo(Integer id);

    /**
     * Update patient information
     *
     * @param id      id of patient
     * @param info    info to update
     * @param infoKey info key
     * @return patient dto
     */
    PatientDTO updatePatientInfo(Integer id, BaseInfo info, String infoKey);

    /**
     * Find patient by id or name.
     *
     * @param condition condition value
     * @return list patient
     */
    List<PatientSearchCensorDTO> findPatientsByIdOrName(String condition);

    /**
     * Update patient allergy drug.
     *
     * @param patientId patient id
     * @param baseInfo  info
     * @return
     */
    PatientDTO updatePatientAllergyDrugInfo(Integer patientId, List<AllergyDrugDTO> baseInfo);

    /**
     * Update patient allergy food.
     *
     * @param patientId patient id
     * @param baseInfo  info
     * @return
     */
    PatientDTO updatePatientAllergyFoodInfo(Integer patientId, List<String> baseInfo);

    /**
     * Update patient medical history.
     *
     * @param patientId patient id
     * @param baseInfo  info
     * @return
     */
    PatientDTO updatePatientMedicalHistoryInfo(Integer patientId, List<PatientDiseaseHistoryDTO> baseInfo);

    /**
     * Update patient address.
     *
     * @param patientId patient id
     * @param baseInfo  info
     * @return
     */
    PatientDTO updatePatientAddressInfo(Integer patientId, AddressInfoDTO baseInfo);

    /**
     * Update patient email.
     *
     * @param patientId patient id
     * @param baseInfo  email
     * @return
     */
    PatientDTO updatePatientEmailInfo(Integer patientId, BaseInfo baseInfo);

    /**
     * Update patient avatar.
     *
     * @param patientId patient id
     * @param baseInfo  avatar
     * @return
     */
    PatientDTO updatePatientAvatarInfo(Integer patientId, BaseInfo baseInfo);

    /**
     * Update patient phone number.
     *
     * @param patientId patient id
     * @param baseInfo  info
     * @return
     */
    PatientDTO updatePatientPhoneInfo(Integer patientId, BaseInfo baseInfo);

    /**
     * Update patient blood type.
     *
     * @param patientId patient id
     * @param baseInfo  blood type
     * @return
     */
    PatientDTO updatePatientBloodGroupInfo(Integer patientId, BaseInfo baseInfo);

    /**
     * Get patient prescription report title.
     *
     * @param locale locale
     * @return title
     */
    PatientPrescriptionReportTitleDTO getPrescriptionTitle(String locale);

}
