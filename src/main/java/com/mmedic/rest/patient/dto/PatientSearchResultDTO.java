package com.mmedic.rest.patient.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Patient Search Result")
public interface PatientSearchResultDTO {

    @ApiModelProperty("Patient Id")
    Integer getId();

    @ApiModelProperty("Patient Name")
    String getName();

    @ApiModelProperty("Phone number")
    String getPhoneNumber();
}
