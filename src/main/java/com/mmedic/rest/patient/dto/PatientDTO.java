package com.mmedic.rest.patient.dto;

import com.mmedic.enums.Gender;
import com.mmedic.rest.doctor.dto.PatientDiseaseHistoryDTO;
import com.mmedic.rest.drug.dto.AllergyDrugDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PatientDTO {

    private int id;

    private String name;

    private LocalDate dateOfBirth;

    private Integer age;

    private Gender gender;

    private String address;

    private double latitude;

    private double longitude;

    private String phoneNumber;

    private String email;

    private String avatarUrl;

    private String bloodType;

    private BaseInfo weight;

    private BaseInfo height;

    private List<String> allergyFood;

    private List<AllergyDrugDTO> allergyDrugs;

    private List<PatientDiseaseHistoryDTO> medicalHistory;

}
