package com.mmedic.rest.patient.dto;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AddressInfoDTO {

    private String address;

    private double latitude;

    private double longitude;

}
