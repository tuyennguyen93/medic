package com.mmedic.rest.patient.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PatientPrescriptionReportTitleDTO {
    private String  title;
    private String  medicalRecord;
    private String  patientName;
    private String  address;
    private String  dob;
    private String  doctorName;
    private String  diagnose;
    private String  drugName;
    private String  drugUnit;
    private String  pricePerUnit;
    private String  amount;
    private String  totalPrice;
    private String  pharmacy;
    private String  createDate;
    private String  totalPaymentAmount;
}
