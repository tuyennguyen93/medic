package com.mmedic.rest.patient.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("Patient base info")
public class BaseInfo {

    @ApiModelProperty("Value patient info")
    private String value;

    @ApiModelProperty("Unit patient info")
    private String unit;
}
