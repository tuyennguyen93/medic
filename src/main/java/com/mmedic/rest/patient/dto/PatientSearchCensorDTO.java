package com.mmedic.rest.patient.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PatientSearchCensorDTO {

    private int id;

    private String name;

    private String phoneNumber;
}
