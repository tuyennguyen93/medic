package com.mmedic.rest.patient.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mmedic.entity.*;
import com.mmedic.enums.PatientMedicalInfoType;
import com.mmedic.message.Message;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.doctor.dto.PatientDiseaseHistoryDTO;
import com.mmedic.rest.drug.repo.DrugRepository;
import com.mmedic.rest.drug.dto.AllergyDrugDTO;
import com.mmedic.rest.drug.mapper.DrugMapper;
import com.mmedic.rest.patient.*;
import com.mmedic.rest.patient.dto.*;
import com.mmedic.rest.patient.repo.ConstantMedicalInfoRepository;
import com.mmedic.rest.patient.repo.MedicineAllergyRepository;
import com.mmedic.rest.patient.repo.PatientRepository;
import com.mmedic.rest.patient.repo.VariableMedicalInfoRepository;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.mmedic.enums.PatientMedicalInfoType.BLOOD_TYPE;

/**
 * Patient Service implementation.
 */
@RequiredArgsConstructor
@Service
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    private final AccountRepository accountRepository;

    private final VariableMedicalInfoRepository variableMedicalInfoRepository;

    private final ConstantMedicalInfoRepository constantMedicalInfoRepository;

    private final MedicineAllergyRepository medicineAllergyRepository;

    private final DrugRepository drugRepository;

    private final DrugMapper drugMapper;

    private final Message message;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PatientDTO getPatientInfo(Integer id) {
        Patient patient = patientRepository.findPatientInfoById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));

        PatientDTO dto = convertEntityToPatientDTO(patient);
        return dto;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public PatientDTO updatePatientAllergyDrugInfo(Integer patientId, List<AllergyDrugDTO> baseInfo) {
        Patient patient = patientRepository.findPatientInfoById(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));

        // Delete old allergy drug
        List<MedicineAllergy> allergyOldList = medicineAllergyRepository.findByPatient_Id(patientId);
        Set<Integer> currentAllergyDrugId = baseInfo.stream().map(a -> a.getId()).collect(Collectors.toSet());
        for (MedicineAllergy allergy : allergyOldList) {
            if (!currentAllergyDrugId.contains(allergy.getId())) {
                medicineAllergyRepository.delete(allergy);
            }
        }

        // Save allergy drug
        for (AllergyDrugDTO allergy : baseInfo) {
            if (!medicineAllergyRepository.findByPatient_IdAndDrug_Id(patientId, allergy.getId()).isPresent()) {
                MedicineAllergy mA = new MedicineAllergy();
                mA.setPatient(patient);
                mA.setDrug(drugRepository.findById(allergy.getId()).get());
                mA.setCreateAt(LocalDateTime.now());
                medicineAllergyRepository.save(mA);
            }
        }

        PatientDTO dto = getPatientInfo(patientId);
        dto.setAllergyDrugs(baseInfo);

        return dto;
    }

    private PatientDTO save(Patient patient, String value, String unit, String infoKey) {
        VariableMedicalInfo rawInfo = null;
        boolean isAddnew = false;
        Map wMap = new HashMap();

        Optional<VariableMedicalInfo> optRaw = patient.getVariableMedicalInfos().
                stream().
                filter(r -> r.getInfoKey().equals(infoKey)).
                findFirst();

        if (optRaw.isPresent()) {
            rawInfo = optRaw.get();
        } else {
            isAddnew = true;
            rawInfo = new VariableMedicalInfo();
        }


        wMap.put(infoKey, value);
        rawInfo.setInfoKey(infoKey);

        rawInfo.setInfoUnit(unit);
        rawInfo.setInfoValue(convertToJsonString(wMap));
        rawInfo.setPatient(patientRepository.findById(patient.getId()).get());
        VariableMedicalInfo updatedInfo = variableMedicalInfoRepository.save(rawInfo);

        if (isAddnew) {
            patient.getVariableMedicalInfos().add(updatedInfo);
            return convertEntityToPatientDTO(patient);
        } else {
            return getPatientInfo(patient.getId());
        }
    }

    @Override
    @Transactional
    public PatientDTO updatePatientBloodGroupInfo(Integer patientId, BaseInfo baseInfo) {
        Patient patient = patientRepository.findPatientInfoById(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));
        String value = String.valueOf(baseInfo.getValue());

        ConstantMedicalInfo rawInfo = null;
        boolean isAddnew = false;
        Map wMap = new HashMap();

        Optional<ConstantMedicalInfo> optRaw = patient.getConstantMedicalInfos().
                stream().
                filter(r -> r.getInfoKey().equals(BLOOD_TYPE.getCode())).
                findFirst();

        if (optRaw.isPresent()) {
            rawInfo = optRaw.get();
        } else {
            isAddnew = true;
            rawInfo = new ConstantMedicalInfo();
        }


        wMap.put(BLOOD_TYPE.getCode(), value);
        rawInfo.setInfoKey(BLOOD_TYPE.getCode());

        rawInfo.setInfoValue(convertToJsonString(wMap));
        rawInfo.setPatient(patientRepository.findById(patient.getId()).get());
        ConstantMedicalInfo updatedInfo = constantMedicalInfoRepository.save(rawInfo);

        if (isAddnew) {
            patient.getConstantMedicalInfos().add(updatedInfo);
            return convertEntityToPatientDTO(patient);
        } else {
            return getPatientInfo(patient.getId());
        }

    }

    @Override
    @Transactional
    public PatientDTO updatePatientAllergyFoodInfo(Integer patientId, List<String> baseInfo) {
        Patient patient = patientRepository.findPatientInfoById(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));
        String jsonString = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonString = mapper.writeValueAsString(baseInfo);
        } catch (Exception e) {
            throw new MedicException(MedicException.ERROR_CONVERT_TO_JSON, "Error convert to json string.");
        }

        return save(patient, jsonString, "", PatientMedicalInfoType.ALLERGY_FOOD.getCode());

    }

    @Override
    @Transactional
    public PatientDTO updatePatientMedicalHistoryInfo(Integer patientId, List<PatientDiseaseHistoryDTO> baseInfo) {
        Patient patient = patientRepository.findPatientInfoById(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(baseInfo);
        } catch (Exception e) {
            throw new MedicException(MedicException.ERROR_CONVERT_TO_JSON, "Error convert to json string.");
        }

        return save(patient, jsonString, "", PatientMedicalInfoType.MEDICAL_HISTORY.getCode());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public PatientDTO updatePatientInfo(Integer id, BaseInfo info, String infoKey) {
        Patient patient = patientRepository.findPatientInfoById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));

        String value = String.valueOf(info.getValue());
        String unit = info.getUnit();
        return save(patient, value, unit, infoKey);
    }

    private String convertToJsonString(Map<String, String> model) {
        String value = "";
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.createObjectNode();
        model.forEach((k, v) -> {
            ((ObjectNode) rootNode).put(k, v);
        });
        try {
            value = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
            ;
        } catch (JsonProcessingException ex) {
            throw new MedicException(MedicException.ERROR_CONVERT_TO_JSON, "Convert to json string error.");
        }
        return value;
    }

    private String readValueFromJsonString(String key, String jsonData) {
        if (StringUtils.isEmpty(jsonData)) {
            return "";
        }

        String value = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> jsonMap = mapper.readValue(jsonData, Map.class);
            value = jsonMap.get(key).toString();
        } catch (IOException ex) {
            throw new MedicException(MedicException.ERROR_READ_VALUE_FROM_JSON, "Read value from json string error.");
        }
        return value;
    }


    private <T> List<T> readValueArrayFromJson(String key, String jsonData) {
        List<T> vals = new ArrayList<>();
        if (StringUtils.isEmpty(jsonData)) {
            return vals;
        }
        ObjectMapper mapper = new ObjectMapper();

        try {
            Map<String, Object> jsonMap = mapper.readValue(jsonData, Map.class);
            vals = mapper.readValue(jsonMap.get(key).
                    toString(), new TypeReference<List<T>>() {
            });

        } catch (IOException ex) {
            throw new MedicException(MedicException.ERROR_READ_VALUE_FROM_JSON, "Read value from json string error.");
        }
        return vals;
    }

    @Override
    @Transactional
    public PatientDTO updatePatientAvatarInfo(Integer patientId, BaseInfo baseInfo) {
        Patient patient = patientRepository.findPatientInfoById(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));
        String value = String.valueOf(baseInfo.getValue());
        patient.getAccount().setAvatarUrl(value);
        accountRepository.save(patient.getAccount());
        return convertEntityToPatientDTO(patient);
    }

    @Override
    @Transactional
    public PatientDTO updatePatientEmailInfo(Integer patientId, BaseInfo baseInfo) {
        Patient patient = patientRepository.findPatientInfoById(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));
        String value = String.valueOf(baseInfo.getValue());
        patient.setEmail(value);
        patientRepository.save(patient);
        return convertEntityToPatientDTO(patient);
    }

    @Override
    @Transactional
    public PatientDTO updatePatientAddressInfo(Integer patientId, AddressInfoDTO info) {
        Patient patient = patientRepository.findPatientInfoById(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));

        patient.setAddress(info.getAddress());
        patient.setLatitude(info.getLatitude());
        patient.setLongitude(info.getLongitude());
        patientRepository.save(patient);
        return convertEntityToPatientDTO(patient);
    }

    @Override
    @Transactional
    public PatientDTO updatePatientPhoneInfo(Integer patientId, BaseInfo info) {
        Patient patient = patientRepository.findPatientInfoById(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient is not exist."));

        String value = String.valueOf(info.getValue());
        patient.setAddress(value);

        // TODO : GENERATE OTP
        //
        //
        //

        return null;
    }

    private PatientDTO convertEntityToPatientDTO(Patient patient) {

        String bloodTypeJson = patient.getConstantMedicalInfos().
                stream().
                filter(r -> r.getInfoKey().equals(BLOOD_TYPE.getCode())).
                findFirst().
                orElseGet(ConstantMedicalInfo::new).getInfoValue();

        VariableMedicalInfo weightInfo = patient.getVariableMedicalInfos().
                stream().
                filter(r -> r.getInfoKey().equals(PatientMedicalInfoType.WEIGHT.getCode())).
                findFirst().
                orElseGet(VariableMedicalInfo::new);

        BaseInfo wInfo = new BaseInfo();
        wInfo.setValue(readValueFromJsonString(PatientMedicalInfoType.WEIGHT.getCode(), weightInfo.getInfoValue()));
        wInfo.setUnit(weightInfo.getInfoUnit());

        VariableMedicalInfo heightInfo = patient.getVariableMedicalInfos().
                stream().
                filter(r -> r.getInfoKey().equals(PatientMedicalInfoType.HEIGHT.getCode())).
                findFirst().
                orElseGet(VariableMedicalInfo::new);

        BaseInfo hInfo = new BaseInfo();
        hInfo.setValue(readValueFromJsonString(PatientMedicalInfoType.HEIGHT.getCode(), heightInfo.getInfoValue()));
        hInfo.setUnit(heightInfo.getInfoUnit());

        String allergyFoodsJson = patient.getVariableMedicalInfos().
                stream().
                filter(r -> r.getInfoKey().equals(PatientMedicalInfoType.ALLERGY_FOOD.getCode())).
                findFirst().
                orElseGet(VariableMedicalInfo::new).getInfoValue();

        String medicalHistoryJson = patient.getVariableMedicalInfos().
                stream().
                filter(r -> r.getInfoKey().equals(PatientMedicalInfoType.MEDICAL_HISTORY.getCode())).
                findFirst().
                orElseGet(VariableMedicalInfo::new).getInfoValue();

        List<AllergyDrugDTO> alleryDrugs = new ArrayList<>();
        patient.getMedicineAllergies().forEach(d -> alleryDrugs.add(drugMapper.convertToAllergyDTO(d.getDrug())));

        PatientDTO dto = new PatientDTO();
        dto.setId(patient.getId());
        dto.setName(patient.getName());
        dto.setAge(Helper.getCurrentAge(patient.getDateOfBirth()));
        dto.setGender(patient.getGender());
        dto.setDateOfBirth(patient.getDateOfBirth());
        dto.setAddress(patient.getAddress());
        dto.setLatitude(patient.getLatitude());
        dto.setLongitude(patient.getLongitude());
        dto.setEmail(patient.getEmail());
        dto.setPhoneNumber(patient.getPhoneNumber());
        dto.setAvatarUrl(patient.getAccount().getAvatarUrl());
        dto.setBloodType(readValueFromJsonString(BLOOD_TYPE.getCode(), bloodTypeJson));
        dto.setAllergyFood(readValueArrayFromJson(PatientMedicalInfoType.ALLERGY_FOOD.getCode(), allergyFoodsJson));
        dto.setAllergyDrugs(alleryDrugs);
        dto.setWeight(wInfo);
        dto.setHeight(hInfo);

        dto.setMedicalHistory(readValueArrayFromJson(PatientMedicalInfoType.MEDICAL_HISTORY.getCode(), medicalHistoryJson));

        return dto;
    }

    @Override
    public List<PatientSearchCensorDTO> findPatientsByIdOrName(String condition) {
        List<PatientSearchCensorDTO> resultPatients = new ArrayList<>();
        List<PatientSearchResultDTO> patients = patientRepository.findPatientsByIdOrName("%" + Helper.convertNullToEmpty(condition) + "%");
        if (!CollectionUtils.isEmpty(patients)) {
            patients.forEach(patient -> {
                PatientSearchCensorDTO censorPatient = new PatientSearchCensorDTO();
                censorPatient.setId(patient.getId());
                censorPatient.setName(Helper.censorData(patient.getName()));
                censorPatient.setPhoneNumber(Helper.censorData(patient.getPhoneNumber()));
                resultPatients.add(censorPatient);
            });
        }
        return resultPatients;
    }

    @Override
    public PatientPrescriptionReportTitleDTO getPrescriptionTitle(String locale) {
        PatientPrescriptionReportTitleDTO titleDTO = new PatientPrescriptionReportTitleDTO();

        titleDTO.setTitle(message.getMessage("prescription.report.title",locale));
        titleDTO.setMedicalRecord(message.getMessage("prescription.report.medical-record",locale));
        titleDTO.setPatientName(message.getMessage("prescription.report.patient-name",locale));
        titleDTO.setAddress(message.getMessage("prescription.report.address",locale));
        titleDTO.setDob(message.getMessage("prescription.report.dob",locale));
        titleDTO.setDoctorName(message.getMessage("prescription.report.doctor-name",locale));
        titleDTO.setDiagnose(message.getMessage("prescription.report.diagnose",locale));
        titleDTO.setDrugName(message.getMessage("prescription.report.drug-name",locale));
        titleDTO.setDrugUnit(message.getMessage("prescription.report.drug-unit",locale));
        titleDTO.setPricePerUnit(message.getMessage("prescription.report.price-per-unit",locale));
        titleDTO.setAmount(message.getMessage("prescription.report.amount",locale));
        titleDTO.setTotalPrice(message.getMessage("prescription.report.total-price",locale));
        titleDTO.setPharmacy(message.getMessage("prescription.report.pharmacy",locale));
        titleDTO.setCreateDate(message.getMessage("prescription.report.create-date",locale));
        titleDTO.setTotalPaymentAmount(message.getMessage("prescription.report.total-payment",locale));

        return titleDTO;
    }
}
