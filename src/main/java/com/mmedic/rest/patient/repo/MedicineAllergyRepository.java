package com.mmedic.rest.patient.repo;

import com.mmedic.entity.MedicineAllergy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MedicineAllergyRepository extends JpaRepository<MedicineAllergy, Integer> {

    Optional<MedicineAllergy> findByPatient_IdAndDrug_Id(Integer patientId, Integer drugId);

    List<MedicineAllergy> findByPatient_Id(Integer patientId);

}
