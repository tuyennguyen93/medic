package com.mmedic.rest.patient.repo;

import com.mmedic.entity.ConstantMedicalInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConstantMedicalInfoRepository extends JpaRepository<ConstantMedicalInfo,Integer> {

}
