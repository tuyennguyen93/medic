package com.mmedic.rest.patient.repo;

import com.mmedic.entity.VariableMedicalInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VariableMedicalInfoRepository extends JpaRepository<VariableMedicalInfo,Integer> {

}
