package com.mmedic.rest.patient.repo;

import com.mmedic.entity.Patient;
import com.mmedic.rest.patient.dto.PatientSearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Patient repository.
 *
 * @author hungp
 */
public interface PatientRepository extends JpaRepository<Patient, Integer> {

//    @Query("SELECT patient FROM Patient patient " +
//            "LEFT JOIN FETCH patient.medicineAllergies medicineAllergies " +
//            "LEFT JOIN FETCH patient.constantMedicalInfos constantMedicalInfos " +
//            "LEFT JOIN FETCH patient.variableMedicalInfos variableMedicalInfos " +
//            "WHERE patient.id = :id ")
//    Optional<Patient> findPatientInfoById(@Param("id") Integer id);

    @Query("SELECT patient FROM Patient patient " +
            "LEFT JOIN patient.constantMedicalInfos  " +
            "LEFT JOIN patient.medicineAllergies  " +
            "LEFT JOIN patient.variableMedicalInfos  " +
            "WHERE patient.id = :idPatient ")
    Optional<Patient> findPatientInfoById(@Param("idPatient") Integer idPatient);


    @Query(value = "SELECT id as id, name as name, phone_number as phoneNumber FROM patient " +
            "WHERE id LIKE ?1 " +
            "OR name LIKE ?1", nativeQuery = true)
    List<PatientSearchResultDTO> findPatientsByIdOrName(String condition);

    Optional<Patient> findPatientByAccountId(int accountId);

}
