package com.mmedic.rest.patient;

import com.mmedic.entity.Account;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.doctor.dto.PatientDiseaseHistoryDTO;
import com.mmedic.rest.drug.dto.AllergyDrugDTO;
import com.mmedic.rest.healthFacility.HealthFacilityService;
import com.mmedic.rest.healthFacility.dto.PrescriptionImageDto;
import com.mmedic.rest.patient.dto.AddressInfoDTO;
import com.mmedic.rest.patient.dto.BaseInfo;
import com.mmedic.rest.patient.dto.PatientDTO;
import com.mmedic.rest.patient.dto.PatientSearchCensorDTO;
import com.mmedic.service.report.ReportExportService;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Helper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Patient Controller.
 */
@Slf4j
@Api(value = "Patient Controller", description = "Patient Management")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/patients")
public class PatientController {

    private final AccountService accountService;

    private final PatientService patientService;

    private final HealthFacilityService healthFacilityService;

    private final ReportExportService reportExportService;

    @ApiOperation("Create new patient account (DONE)")
    @PostMapping
    public MedicResponse<AccountDTO> createPatientAccount(@RequestBody PatientDTO patientDTO) throws MedicException {
        return MedicResponse.okStatus(accountService.savePatientAccount(patientDTO));
    }

    /**
     * Find patients by patient id or name
     *
     * @param query
     * @return
     * @throws MedicException
     */
    @ApiOperation("Find patients (DONE)")
    @GetMapping
    public MedicResponse<List<PatientSearchCensorDTO>> findPatients(@RequestParam(value = "query", required = false)
                                                                    @ApiParam("Patient Id or Patient Name") String query) throws MedicException {
        return MedicResponse.okStatus(patientService.findPatientsByIdOrName(query));
    }

    @ApiOperation("Get patient info follow ID (DONE)")
    @GetMapping("{id}")
    public MedicResponse<PatientDTO> getPatientInfo(@PathVariable("id") Integer patientId) throws Exception {
        PatientDTO result = patientService.getPatientInfo(patientId);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update weight info of patient follow ID (DONE)")
    @PutMapping("{id}/weight")
    public MedicResponse<PatientDTO> updatePatientWeightInfo(@PathVariable("id") Integer patientId,
                                                             @RequestBody BaseInfo baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientInfo(patientId, baseInfo, "weight");
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update height info of patient follow ID (DONE)")
    @PutMapping("{id}/height")
    public MedicResponse<PatientDTO> updatePatientHeightInfo(@PathVariable("id") Integer patientId,
                                                             @RequestBody BaseInfo baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientInfo(patientId, baseInfo, "height");
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update allergy drug info of patient follow ID (DONE)")
    @PutMapping("{id}/allergy-drugs")
    public MedicResponse<PatientDTO> updatePatientAllergyDrugInfo(@PathVariable("id") Integer patientId,
                                                                  @RequestBody List<AllergyDrugDTO> baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientAllergyDrugInfo(patientId, baseInfo);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update allergy food of patient follow ID (DONE)")
    @PutMapping("{id}/allergy-foods")
    public MedicResponse<PatientDTO> updatePatientAllergyFoodInfo(@PathVariable("id") Integer patientId,
                                                                  @RequestBody List<String> baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientAllergyFoodInfo(patientId, baseInfo);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update medical history of patient follow ID (DONE)")
    @PutMapping("{id}/medical-histories")
    public MedicResponse<PatientDTO> updatePatientMedicalHistoryInfo(@PathVariable("id") Integer patientId,
                                                                     @RequestBody List<PatientDiseaseHistoryDTO> baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientMedicalHistoryInfo(patientId, baseInfo);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update address of patient follow ID (DONE)")
    @PutMapping("{id}/address")
    public MedicResponse<PatientDTO> updatePatientAddressInfo(@PathVariable("id") Integer patientId,
                                                              @RequestBody AddressInfoDTO baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientAddressInfo(patientId, baseInfo);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update email of patient follow ID (DONE)")
    @PutMapping("{id}/email")
    public MedicResponse<PatientDTO> updatePatientEmailInfo(@PathVariable("id") Integer patientId,
                                                            @RequestBody BaseInfo baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientEmailInfo(patientId, baseInfo);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update avatar of patient follow ID (DONE)")
    @PutMapping("{id}/avatar")
    public MedicResponse<PatientDTO> updatePatientAvatarInfo(@PathVariable("id") Integer patientId,
                                                             @RequestBody BaseInfo baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientAvatarInfo(patientId, baseInfo);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update medical history of patient follow ID (PENDING)")
    @PutMapping("{id}/phone-number")
    public MedicResponse<PatientDTO> updatePatientPhoneInfo(@PathVariable("id") Integer patientId,
                                                            @RequestBody BaseInfo baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientPhoneInfo(patientId, baseInfo);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update bood group of patient follow ID (DONE)")
    @PutMapping("{id}/blood-group")
    public MedicResponse<PatientDTO> updatePatientBloodGroupInfo(@PathVariable("id") Integer patientId,
                                                                 @RequestBody BaseInfo baseInfo) throws Exception {
        PatientDTO result = patientService.updatePatientBloodGroupInfo(patientId, baseInfo);
        return MedicResponse.okStatus(result);
    }


    @ApiOperation("Export prescription (DONE)")
    @GetMapping("{id}/prescriptions/{prescriptionId}")
    public ResponseEntity<Resource> updatePatientBloodGroupInfo(@PathVariable("id") Integer patientId,
                                                               @PathVariable("prescriptionId") Integer prescriptionId) throws Exception {

        PatientDTO patientDTO = patientService.getPatientInfo(patientId);
        PrescriptionImageDto imageDto = healthFacilityService.getPrescriptionImgDto(prescriptionId, patientDTO);

        Account account = accountService.findPatientAccountByPatientId(patientId);

        Map m = new HashMap<String, PrescriptionImageDto>();
        m.put("report", patientService.getPrescriptionTitle(account.getLanguage()));
        m.put("data",imageDto);
        String filename = "PrescriptionRecord_" +
                patientId +
                "_" +
                prescriptionId +
                "_" +
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        reportExportService.generatePDFFromHTML(filename,"tempReport", m);
        reportExportService.generateImageFromPDF(filename);

        File file = new File(Helper.getTempFilePath("img", filename + ".jpeg"));
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName())
                .body(resource);
    }

}
