package com.mmedic.rest.account.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResetPasswordDTO {

    private String code;

    private String password;

}
