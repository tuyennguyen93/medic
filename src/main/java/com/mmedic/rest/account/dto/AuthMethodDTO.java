package com.mmedic.rest.account.dto;

import com.mmedic.enums.AuthType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Auth Method data transfer object.
 *
 * @author hungp
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AuthMethodDTO {

    @NotNull
    private AuthType authType;

    @NotNull
    private String authData1;

    private String authData2;

    private String authData3;
}
