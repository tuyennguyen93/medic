package com.mmedic.rest.account.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TreatmentDTO {

    private Integer id;

    private String email;

    private String name;

    private String defaultPassword;

    private Boolean isActived;

    private String position;

    private Integer idDepartment;

    private String nameDepartment;

}
