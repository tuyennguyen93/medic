package com.mmedic.rest.account.dto;

import com.mmedic.enums.Day;

import java.time.LocalTime;

public interface StaffWorkingScheduleDTO {

    int getId();

    int getAccountId();

    int getEstablishmentId();

    LocalTime getStartTime();

    LocalTime getEndTime();

    Day getDay();
}
