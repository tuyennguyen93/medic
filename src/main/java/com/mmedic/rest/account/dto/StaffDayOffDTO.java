package com.mmedic.rest.account.dto;

import java.time.LocalDateTime;

public interface StaffDayOffDTO {

    int getAccountId();

    int getEstablishmentId();

    LocalDateTime getStartDate();

    LocalDateTime getEndDate();

}
