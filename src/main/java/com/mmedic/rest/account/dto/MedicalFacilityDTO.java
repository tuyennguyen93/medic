package com.mmedic.rest.account.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicalFacilityDTO {

    private Integer id;

    private String email;

    private String name;

    private String address;

    private Boolean isActived;

    private String defaultPassword;

}
