package com.mmedic.rest.account.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private Integer id;

    private String email;

    private String name;

    private String defaultPassword;

    private Boolean isActived;

    private Boolean isAdmin;

}
