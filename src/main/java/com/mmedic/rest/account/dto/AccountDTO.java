package com.mmedic.rest.account.dto;

import com.mmedic.rest.patient.dto.PatientDTO;
import com.mmedic.rest.establishment.dto.EstablishmentDTO;
import com.mmedic.rest.role.dto.RoleDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class AccountDTO {

    private int id;

    private EstablishmentDTO establishment;

    private String name;

    private String title;

    private LocalDate dateOfBirth;

    private String avatarUrl;

    private RoleDTO role;

    private List<AuthMethodDTO> authMethods;

    private PatientDTO patient;

}
