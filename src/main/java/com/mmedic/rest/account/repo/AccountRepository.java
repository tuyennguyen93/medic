package com.mmedic.rest.account.repo;

import com.mmedic.entity.Account;
import com.mmedic.entity.Role;
import com.mmedic.rest.account.dto.StaffDayOffDTO;
import com.mmedic.rest.account.dto.StaffWorkingScheduleDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Account Repository.
 *
 * @author hungp
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    /**
     * Find Account base on id.
     *
     * @param id the id of account
     * @return Account instance
     */
    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE acc.id = :id " +
            "AND (acc.isDeleted = 0 OR acc.isDeleted IS NULL)")
    Optional<Account> findAccountById(@Param("id") int id);

    /**
     * Find Account base on email.
     *
     * @param email the email of account
     * @return Account instance
     */
    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE authMethod.authData1 = :email " +
            "AND authMethod.authType = '1' AND acc.isDeleted = 0")
    Optional<Account> findAccountByEmail(@Param("email") String email);

    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role role " +
            "LEFT JOIN FETCH acc.patient patient " +
            "WHERE patient.id = :patientId " +
            "AND role.name = 'PATIENT' " +
            "AND authMethod.authType = '2'")
    Optional<Account> findPatientAccountByPatientId(@Param("patientId") int patientId);

    /**
     * Find Account base on phone number and account id.
     *
     * @param patientId the patient id
     * @param phone     the phone number of authentication method
     * @return the account
     */
    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role " +
            "LEFT JOIN FETCH acc.patient patient " +
            "WHERE patient.id = :patientId " +
            "AND authMethod.authData1 = :phone ")
    Optional<Account> findAccountByIdAndPhone(@Param("patientId") int patientId, @Param("phone") String phone);

    /**
     * Counts number of account by role.
     *
     * @param role the role of account
     * @return number of account
     */
    int countAccountByRole(Role role);

    /**
     * Finds account by roles name.
     *
     * @param roleNames the roles name
     * @return the accounts
     */
    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE role.name IN (:roleNames)")
    List<Account> findAccountsByRole_Name(@Param("roleNames") String... roleNames);

    /**
     * Find Account by secret code.
     *
     * @param code the secret code
     * @return Account
     */
    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE acc.secret = :code ")
    Optional<Account> findAccountBySecret(@Param("code") String code);

    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE acc.id = :id " +
            "AND role.name IN ('DOCTOR_ADMIN', 'DOCTOR_STAFF')")
    Optional<Account> findDoctorAccountById(@Param("id") int id);

    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE acc.id = :id " +
            "AND role.name IN ('DOCTOR_ADMIN', 'DOCTOR_STAFF', 'NURSE_ADMIN', 'NURSE_STAFF')")
    Optional<Account> findDoctorOrNurseAccountById(@Param("id") int id);

    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE acc.id = :id " +
            "AND role.name IN ('DOCTOR_ADMIN', 'DOCTOR_STAFF', 'NURSE_ADMIN', 'NURSE_STAFF')")
    Optional<Account> findMedicalStaffAccountById(@Param("id") int id);

    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE acc.id = :id " +
            "AND role.name = 'PATIENT'")
    Optional<Account> findPatientAccountById(@Param("id") int id);

    @Query("SELECT account.id AS accountId, establishment.id AS establishmentId, dayOff.startDate AS startDate, dayOff.endDate AS endDate " +
            "FROM Account account " +
            "LEFT JOIN account.establishment establishment " +
            "INNER JOIN establishment.establishmentDayOffs dayOff " +
            "WHERE account.id IN (:ids) " +
            "AND dayOff.startDate < dayOff.endDate " +
            "AND dayOff.endDate >= :startDate " +
            "AND dayOff.startDate <= :endDate AND " +
            ":startDate <= :endDate")
    List<StaffDayOffDTO> findAccountsDayOff(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate, @Param("ids") int... accountId);

    @Query("SELECT workingSchedule.id AS id, account.id AS accountId, establishment.id AS establishmentId, workingSchedule.startTime AS startTime, workingSchedule.endTime AS endTime, workingSchedule.dayOfWeek AS day " +
            "FROM Account account " +
            "LEFT JOIN account.establishment establishment " +
            "INNER JOIN establishment.establishmentWorkingSchedules workingSchedule " +
            "WHERE account.id IN (:ids) AND workingSchedule.isDeleted = 0 OR workingSchedule.isDeleted IS NULL")
    List<StaffWorkingScheduleDTO> findAccountsWorkingSchedule(@Param("ids") int... accountId);

    /**
     * Get notification token of account.
     *
     * @param account the account
     * @return the notification token
     */
    @Query("SELECT acc.notificationToken FROM Account acc WHERE acc = :account")
    String findNotificationToken(@Param("account") Account account);

    @Query(nativeQuery = true, value = "" +
            "SELECT CASE WHEN " +
            " ( " +
            "   EXISTS(SELECT 1 FROM patient WHERE account_id = :accountId ) + " +
            "   EXISTS(SELECT 1 FROM medical_service_order_discussion WHERE message_sender = :accountId ) + " +
            "   EXISTS(SELECT 1 FROM medical_service_order_detail WHERE handle_by = :accountId ) + " +
            "   EXISTS(SELECT 1 FROM drug_import WHERE create_by = :accountId ) + " +
            "   EXISTS(SELECT 1 FROM drug_internal_export WHERE create_by = :accountId )  " +
            "   > 0) THEN 'TRUE' ELSE 'FALSE' END AS isUsing")
    Boolean isUsingAccount(@Param("accountId") Integer accountId);

    @Query(nativeQuery = true, value = "" +
            "SELECT CASE WHEN " +
            " ( " +
            "   EXISTS(SELECT 1 FROM drug_internal_export WHERE export_to = :estId ) + " +
            "   EXISTS(SELECT 1 FROM favorite_establishment WHERE establishment_id = :estId ) + " +
            "   EXISTS(SELECT 1 FROM establishment_rating WHERE establishment_id = :estId ) + " +
            "   EXISTS(SELECT 1 FROM medical_service_order_detail WHERE establishment_id = :estId ) + " +
            "   EXISTS(SELECT 1 FROM medical_service_registry WHERE establishment_id = :estId ) + " +
            "   EXISTS(SELECT 1 FROM drug_export WHERE export_from = :estId ) + " +
            "   EXISTS(SELECT 1 FROM drug_import WHERE to_establishment = :estId )  " +
            "   > 0) THEN 'TRUE' ELSE 'FALSE' END AS isUsing")
    Boolean isUsingEstablishment(@Param("estId") Integer estId);

    /**
     * Find All account have notification like condition.
     *
     * @param notificationToken notification
     * @return accounts
     */
    @Query("SELECT acc FROM Account acc " +
            "LEFT JOIN FETCH acc.authMethods authMethod " +
            "LEFT JOIN FETCH acc.role " +
            "LEFT JOIN FETCH acc.patient " +
            "WHERE acc.notificationToken LIKE :condition ")
    List<Account> findByNotificationTokenLike(@Param("condition") String notificationToken);
}
