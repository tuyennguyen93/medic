package com.mmedic.rest.account.repo;

import com.mmedic.entity.AuthMethod;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Authentication method repository.
 *
 * @author hungp
 */
public interface AuthMethodRepository extends JpaRepository<AuthMethod, Integer> {

    /**
     * Select auth method base on code in authData2 column.
     *
     * @param code the code reset password
     * @return AuthMethod
     */
    Optional<AuthMethod> findByAuthData2(String code);
}
