package com.mmedic.rest.account.repo;

import com.mmedic.entity.Account;
import com.mmedic.rest.account.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Account,Integer> {

    @Query("SELECT new com.mmedic.rest.account.dto.UserDTO(acc.id AS id, " +
            "auth.authData1 AS email, " +
            "acc.name AS name, " +
            "'' AS defaultPassword, " +
            "acc.isActived AS isActived, (role.id = 2) AS isAdmin) FROM Account acc " +
            "LEFT JOIN acc.role role " +
            "LEFT JOIN acc.authMethods auth " +
            "WHERE role.id > 1 AND acc.isDeleted = 0 ")
    Page<UserDTO> findAvailableUserAccounts(Pageable pageable);


    @Query("SELECT new com.mmedic.rest.account.dto.UserDTO(acc.id AS id, " +
            "auth.authData1 AS email, " +
            "acc.name AS name, " +
            "'' AS defaultPassword, " +
            "acc.isActived AS isActived, (role.id = 2) AS isAdmin) FROM Account acc " +
            "LEFT JOIN acc.role role " +
            "LEFT JOIN acc.authMethods auth " +
            "WHERE role.id > 1 AND acc.isDeleted = :deleteStatus AND acc.id = :accountId ")
    Optional<UserDTO> findUserAccountsById(@Param("deleteStatus") Boolean deleteStatus, @Param("accountId") Integer id);

}
