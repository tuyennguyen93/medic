package com.mmedic.rest.account.repo;

import com.mmedic.entity.Account;
import com.mmedic.rest.account.dto.TreatmentDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface TreatmentRepository extends JpaRepository<Account, Integer> {


    @Query("SELECT new com.mmedic.rest.account.dto.TreatmentDTO(acc.id AS id, " +
            "auth.authData1 AS email, " +
            "acc.name AS name, " +
            "'' AS defaultPassword, " +
            "acc.isActived AS isActived, " +
            "role.name AS position,  " +
            "medicalDepartment.id AS idDepartment,  " +
            "medicalDepartment.name AS nameDepartment ) " +
            "FROM Account acc " +
            "LEFT JOIN acc.role role " +
            "LEFT JOIN acc.authMethods auth " +
            "LEFT JOIN acc.establishment establishment " +
            "LEFT JOIN establishment.medicalDepartment medicalDepartment " +
            "WHERE acc.isDeleted = 0 AND role.id IN ('3', '4', '5', '6')")
    Page<TreatmentDTO> findAvailableTreatmentAccounts(Pageable pageable);


    @Query("SELECT new com.mmedic.rest.account.dto.TreatmentDTO(acc.id AS id, " +
            "auth.authData1 AS email, " +
            "acc.name AS name, " +
            "'' AS defaultPassword, " +
            "acc.isActived AS isActived, " +
            "role.name AS position,  " +
            "medicalDepartment.id AS idDepartment,  " +
            "medicalDepartment.name AS nameDepartment ) " +
            "FROM Account acc " +
            "LEFT JOIN acc.role role " +
            "LEFT JOIN acc.authMethods auth " +
            "LEFT JOIN acc.establishment establishment " +
            "LEFT JOIN establishment.medicalDepartment medicalDepartment " +
            "WHERE acc.isDeleted = :deleteStatus  AND acc.id = :accountId ")
    Optional<TreatmentDTO> findTreatmentAccountsById(@Param("deleteStatus") Boolean deleteStatus, @Param("accountId") Integer id);

}
