package com.mmedic.rest.account.repo;

import com.mmedic.entity.Account;
import com.mmedic.rest.account.dto.MedicalFacilityDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MedicalFacilityRepository extends JpaRepository<Account, Integer> {

    @Query("SELECT new com.mmedic.rest.account.dto.MedicalFacilityDTO(acc.id AS id, " +
            "auth.authData1 AS email, " +
            "establishment.name AS name, " +
            "establishment.address AS address, " +
            "acc.isActived AS isActived, " +
            "'' AS defaultPassword ) " +
            "FROM Account acc " +
            "LEFT JOIN acc.role role " +
            "LEFT JOIN acc.authMethods auth " +
            "LEFT JOIN acc.establishment establishment " +
            "WHERE acc.isDeleted = 0 AND role.id IN :roleList ")
    Page<MedicalFacilityDTO> findAvailableMedicalFacilityAccounts(Pageable pageable, @Param("roleList") List<Integer> roleList);


    @Query("SELECT new com.mmedic.rest.account.dto.MedicalFacilityDTO(acc.id AS id, " +
            "auth.authData1 AS email, " +
            "establishment.name AS name, " +
            "establishment.address AS address, " +
            "acc.isActived AS isActived, " +
            "'' AS defaultPassword ) " +
            "FROM Account acc " +
            "LEFT JOIN acc.role role " +
            "LEFT JOIN acc.authMethods auth " +
            "LEFT JOIN acc.establishment establishment " +
            "LEFT JOIN establishment.medicalDepartment medicalDepartment " +
            "WHERE acc.isDeleted = :deleteStatus AND acc.id = :accountId")
    Optional<MedicalFacilityDTO> findMedicalFacilityAccountsById(@Param("deleteStatus") Boolean deleteStatus, @Param("accountId") Integer id);


}
