package com.mmedic.rest.account.mapper;

import com.mmedic.entity.Account;
import com.mmedic.enums.AuthType;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.mapper.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Account Mapper Decorator.
 */
public abstract class AccountMapperDecorator implements AccountMapper {

    @Autowired
    @Qualifier("delegate")
    private AccountMapper delegate;

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Account toAccount(AccountDTO accountDTO) {
        Account account = delegate.toAccount(accountDTO);
        if (!CollectionUtils.isEmpty(account.getAuthMethods())) {
            account.getAuthMethods().forEach(authMethod -> {
                authMethod.setAccount(account);
                if (authMethod.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD) && !StringUtils.isEmpty(authMethod.getAuthData2())) {
                    authMethod.setAuthData2(passwordEncoder.encode(authMethod.getAuthData2()));
                }
            });
        }
        return account;
    }

    @Override
    public AccountDTO toAccountDto(Account account) {
        AccountDTO accountDTO = delegate.toAccountDto(account);
        removePrivateData(accountDTO);
        return accountDTO;
    }

    @Override
    public List<AccountDTO> toListAccountDTO(List<Account> accounts) {
        List<AccountDTO> accountDTOS = delegate.toListAccountDTO(accounts);
        accountDTOS.forEach(this::removePrivateData);
        return accountDTOS;
    }

    private void removePrivateData(AccountDTO accountDTO) {
        if (null != accountDTO) {
            if (!CollectionUtils.isEmpty(accountDTO.getAuthMethods())) {
                accountDTO.getAuthMethods().forEach(authMethodDTO -> {
                    authMethodDTO.setAuthData2(null);
                    authMethodDTO.setAuthData3(null);
                });
            }
        }
    }
}
