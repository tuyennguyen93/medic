package com.mmedic.rest.account.mapper;

import com.mmedic.entity.Account;
import com.mmedic.entity.AuthMethod;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.MedicalDepartment;
import com.mmedic.entity.Patient;
import com.mmedic.rest.account.dto.AuthMethodDTO;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.establishment.dto.EstablishmentDTO;
import com.mmedic.rest.doctor.dto.DepartmentDTO;
import com.mmedic.rest.patient.dto.PatientDTO;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Account Mapper.
 */
@Mapper(componentModel = "spring")
@DecoratedWith(AccountMapperDecorator.class)
public interface AccountMapper {

    AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    /**
     * Convert Account entity to Account dto.
     *
     * @param account the account entity
     * @return account dto
     */
    AccountDTO toAccountDto(Account account);

    /**
     * Convert account dto to account entity.
     *
     * @param accountDTO the account dto
     * @return account entity
     */
    Account toAccount(AccountDTO accountDTO);

    /**
     * Convert list account entity to account dto.
     *
     * @param accounts list account entity
     * @return list account dto
     */
    List<AccountDTO> toListAccountDTO(List<Account> accounts);

    /**
     * Convert list auth method entity to dto
     *
     * @param authMethods list auth method entity
     * @return list auth method dto
     */
    List<AuthMethodDTO> toListAuthMethodDto(List<AuthMethod> authMethods);

    /**
     * Convert list auth method dto to entity.
     *
     * @param authMethods list auth method dto
     * @return list auth method entity
     */
    List<AuthMethod> toListAuthMethod(List<AuthMethodDTO> authMethods);

    /**
     * Convert establishment entity to dto.
     *
     * @param establishment the establishment entity
     * @return the establishment dto
     */
    EstablishmentDTO toEstablishmentDto(Establishment establishment);

    /**
     * Convert establishment dto to entity.
     *
     * @param establishmentDTO establishment dto
     * @return establishment entity
     */
    Establishment toEstablishment(EstablishmentDTO establishmentDTO);

    /**
     * Convert patient entity to dto.
     *
     * @param patient patient entity
     * @return patient dto
     */
    PatientDTO toPatientDto(Patient patient);

    /**
     * Convert patient dto to entity.
     *
     * @param patientDTO patient dto
     * @return patient entity
     */
    Patient toPatient(PatientDTO patientDTO);

    /**
     * Convert Medical Department entity to dto.
     *
     * @param medicalDepartment medical department entity
     * @return medical department dto
     */
    DepartmentDTO toMedicalDepartmentDto(MedicalDepartment medicalDepartment);

    /**
     * Convert medical Department dto to entity.
     *
     * @param departmentDTO department dto
     * @return department entity
     */
    MedicalDepartment toMedicalDepartment(DepartmentDTO departmentDTO);
}
