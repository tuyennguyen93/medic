package com.mmedic.rest.account;

import com.mmedic.entity.Account;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.dto.ResetPasswordDTO;
import com.mmedic.rest.authentication.dto.OtpDTO;
import com.mmedic.rest.patient.dto.PatientDTO;
import com.mmedic.spring.errors.MedicException;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.List;
import java.util.Optional;

/**
 * Account Service.
 *
 * @author hungp
 */
public interface AccountService extends UserDetailsService, AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    /**
     * Find Account, which use email as authentication method.
     *
     * @param email the email of account
     * @return Account instance
     */
    Optional<Account> findAccountByEmail(String email);

    /**
     * Find patient account by patient id.
     *
     * @param patientId patient id
     * @return patient account
     */
    Account findPatientAccountByPatientId(int patientId);

    /**
     * Load user by patient id and phone number.
     *
     * @param patientId patient id
     * @param phone     phone number
     * @return user detail
     */
    UserDetails loadUserByPatientIdAndPhone(int patientId, String phone);

    /**
     * This method is used to save new admin account.
     *
     * @param accountDTO the admin account
     * @return AccountDTO instance
     * @throws Exception exception
     */
    AccountDTO saveAdminAccount(AccountDTO accountDTO) throws MedicException;

    /**
     * Create new account.
     *
     * @param accountDTO the account data
     * @return the account is created
     * @throws MedicException
     */
    AccountDTO createNewAccount(AccountDTO accountDTO) throws MedicException;

    /**
     * Create new patient account.
     *
     * @param patientDTO the patient data
     * @return account is created
     * @throws MedicException
     */
    AccountDTO savePatientAccount(PatientDTO patientDTO) throws MedicException;

    /**
     * Create new staff account.
     *
     * @param accountDTO the staff data
     * @return account is created
     * @throws MedicException
     */
    AccountDTO saveStaffAccount(AccountDTO accountDTO) throws MedicException;

    /**
     * Request reset password.
     *
     * @param email the email of account
     * @return email
     * @throws MedicException
     */
    String requestResetPassword(String email) throws MedicException;

    /**
     * Reset password of account.
     *
     * @param resetPasswordDTO the data
     * @return true if password is reset
     * @throws MedicException
     */
    boolean resetPassword(ResetPasswordDTO resetPasswordDTO) throws MedicException;

    /**
     * Generate and send OTP value to user.
     *
     * @param otpDTO the otp data
     * @return true if otp is sent successfully
     * @throws MedicException
     */
    boolean generateAndSendTOTP(OtpDTO otpDTO) throws MedicException;

    /**
     * Find admin account.
     *
     * @return list admin account
     */
    List<AccountDTO> findAdminAccounts();

    /**
     * Verify email account.
     *
     * @param resetPasswordDTO data to verify email
     * @return true if success
     * @throws MedicException
     */
    boolean verifyEmailAccount(ResetPasswordDTO resetPasswordDTO) throws MedicException;

    /**
     * Resend verify email account.
     *
     * @param email the email of account
     * @return true if success
     * @throws MedicException
     */
    boolean resendVerifyEmailAccount(String email) throws MedicException;

    /**
     * Registration token.
     *
     * @param registrationToken registration token value.
     * @param currentAccount    current login account
     * @return true if registration is success
     * @throws MedicException
     */
    boolean appendRegistrationTokenForNotification(String registrationToken, Account currentAccount) throws MedicException;

    /**
     * Registration token.
     *
     * @param registrationToken registration token value.
     * @param currentAccount    current login account
     * @return true if registration is success
     * @throws MedicException
     */
    boolean registrationTokenForNotification(String registrationToken, Account currentAccount) throws MedicException;

    /**
     * Get notification token.
     *
     * @param currentAccount the current account
     * @return list the notification token
     * @throws MedicException
     */
    List<String> getNotificationToken(Account currentAccount) throws MedicException;

    /**
     * Remove registration token.
     *
     * @param registrationToken registration token
     * @return true if registration is removed successfully
     * @throws MedicException
     */
    boolean removeRegistrationTokenForNotification(String registrationToken) throws MedicException;

    /**
     * Update language for account.
     *
     * @param currentAccount current account
     * @param language language
     * @return true if updated successfully
     * @throws MedicException
     */
    boolean updateLanguage(Account currentAccount, String language) throws MedicException;
}
