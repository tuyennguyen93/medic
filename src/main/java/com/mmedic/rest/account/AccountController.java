package com.mmedic.rest.account;

import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.account.dto.MedicalFacilityDTO;
import com.mmedic.rest.account.dto.ResetPasswordDTO;
import com.mmedic.rest.account.dto.TreatmentDTO;
import com.mmedic.rest.account.dto.UserDTO;
import com.mmedic.rest.account.impl.MedicalFacilityService;
import com.mmedic.rest.account.impl.TreatmentService;
import com.mmedic.rest.account.impl.UserService;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Account Controller.
 *
 * @author hungp
 */
@Api(value = "Account Management", description = "Account Management")
@RestController
@RequestMapping("/api/v1/accounts")
public class AccountController {

    private TreatmentService treatmentService;
    private MedicalFacilityService medicalFacilityService;
    private UserService userService;
    private AccountService accountService;


    public AccountController(AccountService accountService,
                             MedicalFacilityService medicalFacilityService,
                             TreatmentService treatmentService,
                             UserService userService) {
        this.accountService = accountService;
        this.treatmentService = treatmentService;
        this.userService = userService;
        this.medicalFacilityService = medicalFacilityService;
    }

    @ApiOperation("Get list doctor/nurse accounts (DONE)")
    @GetMapping("/medical-staffs")
    public MedicResponse<Page<TreatmentDTO>> getMedicalStaffAccountList(Pageable pageable) {
        Page<TreatmentDTO> result = treatmentService.getAccountList(pageable);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create doctor/nurse accounts (DONE)")
    @PostMapping("/medical-staffs")
    public MedicResponse<TreatmentDTO> createMedicalStaffAccount(@RequestBody TreatmentDTO reqDto) {
        TreatmentDTO result = treatmentService.createAccount(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update doctor/nurse account (DONE)")
    @PutMapping("/medical-staffs/{id}")
    public MedicResponse<TreatmentDTO> updateMedicalStaffAccount(@PathVariable(value = "id") Integer id,
                                                                 @RequestBody TreatmentDTO reqDto) {
        TreatmentDTO result = treatmentService.updateAccount(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete doctor/nurse account (DONE)")
    @DeleteMapping("/medical-staffs/{id}")
    public MedicResponse<TreatmentDTO> deleteMedicalStaffAccount(@PathVariable("id") Integer id) {
        TreatmentDTO result = (TreatmentDTO)treatmentService.deleteAccount(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get user accounts list (DONE)")
    @GetMapping("/users")
    public MedicResponse<Page<UserDTO>> getUsersList(Pageable pageable) {
        Page<UserDTO> result = userService.getAccountList(pageable);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create user account (DONE)")
    @PostMapping("/users")
    public MedicResponse<UserDTO> createUser(@RequestBody UserDTO reqDto) {
        UserDTO result = userService.createAccount(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update user account (DONE)")
    @PutMapping("/users/{id}")
    public MedicResponse<UserDTO> updateUser(@PathVariable(value = "id") Integer id, @RequestBody UserDTO reqDto) {
        UserDTO result = userService.updateAccount(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete user account (DONE)")
    @DeleteMapping("/users/{id}")
    public MedicResponse<UserDTO> deleteUser(@PathVariable("id") Integer id) {
        UserDTO result = (UserDTO) userService.deleteAccount(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get list preclinical accounts (DONE)")
    @GetMapping("/preclinicals")
    public MedicResponse<Page<MedicalFacilityDTO>> getPreclinicalAccountList(Pageable pageable) {
        Page<MedicalFacilityDTO> result = medicalFacilityService.getAccountList(pageable, Constants.ROLE_PRECLINIC_ADMIN);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create preclinical accounts (DONE)")
    @PostMapping("/preclinicals")
    public MedicResponse<MedicalFacilityDTO> createPreclinicalAccount(@RequestBody MedicalFacilityDTO reqDto) {
        MedicalFacilityDTO result = medicalFacilityService.createAccount(reqDto, Constants.ROLE_PRECLINIC_ADMIN);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update preclinical accounts (DONE)")
    @PutMapping("/preclinicals/{id}")
    public MedicResponse<MedicalFacilityDTO> updatePreclinicalAccount(@PathVariable(value = "id") Integer id, @RequestBody MedicalFacilityDTO reqDto) {
        MedicalFacilityDTO result = medicalFacilityService.updateAccount(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete preclinical accounts (DONE)")
    @DeleteMapping("/preclinicals/{id}")
    public MedicResponse<MedicalFacilityDTO> deletePreclinicalAccount(@PathVariable("id") Integer id) {
        MedicalFacilityDTO result = (MedicalFacilityDTO)medicalFacilityService.deleteAccount(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get list pharmacy accounts (DONE)")
    @GetMapping("/pharmacies")
    public MedicResponse<Page<MedicalFacilityDTO>> getAccountList(Pageable pageable) {
        Page<MedicalFacilityDTO> result = medicalFacilityService.getAccountList(pageable, Constants.ROLE_DRUG_STORE_ADMIN);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create pharmacy accounts (DONE)")
    @PostMapping("/pharmacies")
    public MedicResponse<MedicalFacilityDTO> createAccount(@RequestBody MedicalFacilityDTO reqDto) {
        MedicalFacilityDTO result = medicalFacilityService.createAccount(reqDto, Constants.ROLE_DRUG_STORE_ADMIN);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update pharmacy account (DONE)")
    @PutMapping("/pharmacies/{id}")
    public MedicResponse<MedicalFacilityDTO> updateAccount(@PathVariable(value = "id") Integer id, @RequestBody MedicalFacilityDTO reqDto) {
        MedicalFacilityDTO result = medicalFacilityService.updateAccount(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete pharmacy account (DONE)")
    @DeleteMapping("/pharmacies/{id}")
    public MedicResponse<MedicalFacilityDTO> deleteAccount(@PathVariable("id") Integer id) {
        MedicalFacilityDTO result = (MedicalFacilityDTO)medicalFacilityService.deleteAccount(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Request reset password (DONE)")
    @PostMapping("/reset-password")
    public MedicResponse<String> requestResetPassword(@RequestBody String email) throws MedicException {
        return MedicResponse.okStatus(accountService.requestResetPassword(email));
    }

    @ApiOperation("Change password (DONE)")
    @PutMapping("/reset-password")
    public MedicResponse<Boolean> resetPassword(@RequestBody ResetPasswordDTO resetPasswordDTO) throws MedicException {
        return MedicResponse.okStatus(accountService.resetPassword(resetPasswordDTO));
    }

    @ApiOperation("Verify Email of Account (DONE)")
    @PutMapping("/verify-account")
    public MedicResponse<Boolean> verifyEmailAccount(@RequestBody @ApiParam("Verify Param") ResetPasswordDTO resetPasswordDTO) throws MedicException {
        return MedicResponse.okStatus(accountService.verifyEmailAccount(resetPasswordDTO));
    }

    @ApiOperation("Resend email verify (DONE)")
    @PostMapping("/verify-account")
    public MedicResponse<String> resendVerifyEmailAccount(@RequestBody @ApiParam("Email To Verify") String email) throws MedicException {
        accountService.resendVerifyEmailAccount(email);
        return MedicResponse.okStatus(email);
    }
}
