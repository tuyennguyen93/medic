package com.mmedic.rest.account.impl;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.enums.DeleteStatus;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.dto.MedicalFacilityDTO;
import com.mmedic.rest.account.repo.MedicalFacilityRepository;
import com.mmedic.rest.role.dto.RoleDTO;
import com.mmedic.utils.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Medical Facility Service.
 */
@RequiredArgsConstructor
@Service
public class MedicalFacilityService extends AbstractStaffAccountService {

    private final MedicalFacilityRepository medicalFacilityRepository;

    /**
     * Get all role is in group.
     *
     * @param roleName the role name
     * @return list id of roles
     */
    private List<Integer> getRoleList(String roleName) {
        List<Integer> list = new ArrayList<>();
        if (roleName.equals(Constants.ROLE_PRECLINIC_ADMIN)) {
            list.add(7); // PRECLINIC_ADMIN
            list.add(8); // PRECLINIC_STAFF
        } else {
            list.add(9); // DRUG_STORE_ADMIN
            list.add(10); // DRUG_STORE_STAFF
        }

        return list;
    }

    /**
     * Get all accounts by role.
     *
     * @param pageable pagination
     * @param roleName role name
     * @return list accounts
     */
    public Page<MedicalFacilityDTO> getAccountList(Pageable pageable, String roleName) {
        Page<MedicalFacilityDTO> accountList = medicalFacilityRepository.
                findAvailableMedicalFacilityAccounts(pageable, getRoleList(roleName));
        return accountList;
    }

    /**
     * Create new account (PRECLINIC/DRUG_STORE).
     *
     * @param dto      the account data
     * @param roleName role name
     * @return the account is created successfully
     */
    @Transactional
    public MedicalFacilityDTO createAccount(MedicalFacilityDTO dto, String roleName) {
        AccountDTO accountDTO = convertToAccountDTO(dto.getEmail(), dto.getName());
        RoleDTO roleDTO = new RoleDTO();

        roleDTO.setName(roleName);
        accountDTO.setRole(roleDTO);
        AccountDTO newAccountDTO = save(accountDTO);

        // Create establishment for ROLE_PRECLINIC_ADMIN / NURSE_ADMIN
        Establishment establishment = new Establishment();
        establishment.setName(accountDTO.getName());
        establishment.setAddress(dto.getAddress());
        establishment.setEstablishmentType(roleName.equals(Constants.ROLE_PRECLINIC_ADMIN) ?
                EstablishmentType.PRECLINICAL : EstablishmentType.DRUG_STORE);
        establishment.setCreateAt(LocalDateTime.now());
        Establishment savedEst = establishmentRepository.save(establishment);

        // Update establishment for account
        Account account = medicalFacilityRepository.findById(newAccountDTO.getId()).get();
        account.setEstablishment(savedEst);
        medicalFacilityRepository.saveAndFlush(account);

        return medicalFacilityRepository.findMedicalFacilityAccountsById(DeleteStatus.AVAILABLE.getValue(),
                newAccountDTO.getId()).get();
    }

    /**
     * Update account.
     *
     * @param id  the id of account
     * @param dto the data to update
     * @return the account is updated successfully
     */
    @Transactional
    public MedicalFacilityDTO updateAccount(Integer id, MedicalFacilityDTO dto) {
        update(id, dto.getName(), dto.getDefaultPassword());

        // Update address
        Account account = medicalFacilityRepository.findById(id).get();
        account.getEstablishment().setAddress(dto.getAddress());
        account.getEstablishment().setName(dto.getName());
        medicalFacilityRepository.saveAndFlush(account);
        // End update address

        return medicalFacilityRepository.
                findMedicalFacilityAccountsById(DeleteStatus.AVAILABLE.getValue(), id).get();

    }

    @Override
    protected Object getDtoAfterDelete(Integer id){
        return medicalFacilityRepository.
                findMedicalFacilityAccountsById(DeleteStatus.IS_DELETED.getValue(), id).get();
    }
}
