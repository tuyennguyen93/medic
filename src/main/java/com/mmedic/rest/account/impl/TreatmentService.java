package com.mmedic.rest.account.impl;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.MedicalDepartment;
import com.mmedic.entity.Role;
import com.mmedic.enums.DeleteStatus;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.dto.TreatmentDTO;
import com.mmedic.rest.account.repo.TreatmentRepository;
import com.mmedic.rest.doctor.repo.MedicalDepartmentRepository;
import com.mmedic.rest.role.dto.RoleDTO;
import com.mmedic.rest.role.repo.RoleRepository;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * Treatment Service.
 */
@RequiredArgsConstructor
@Service
public class TreatmentService extends AbstractStaffAccountService {

    private final TreatmentRepository treatmentRepository;

    private final MedicalDepartmentRepository medicalDepartmentRepository;

    private final RoleRepository roleRepository;

    /**
     * Get accounts.
     *
     * @param pageable pagination
     * @return list doctor, nurse
     */
    public Page<TreatmentDTO> getAccountList(Pageable pageable) {
        Page<TreatmentDTO> accountList = treatmentRepository.findAvailableTreatmentAccounts(pageable);
        return accountList;
    }

    /**
     * Create new doctor/nurse account.
     *
     * @param dto the data of account
     * @return the account is created successfully
     */
    @Transactional
    public TreatmentDTO createAccount(TreatmentDTO dto) {
        AccountDTO accountDTO = convertToAccountDTO(dto.getEmail(), dto.getName());
        RoleDTO roleDTO = new RoleDTO();
        String roleName = "";
        switch (dto.getPosition()) {
            case Constants.ROLE_DOCTOR_ADMIN:
            case Constants.ROLE_DOCTOR_STAFF:
            case Constants.ROLE_NURSE_ADMIN:
            case Constants.ROLE_NURSE_STAFF:
                roleName = dto.getPosition();
                break;
            default:
                throw new MedicException(MedicException.ERROR_NOT_FOUND_ROLE, "Not found valid role.");

        }

        roleDTO.setName(roleName);
        accountDTO.setRole(roleDTO);
        AccountDTO newAccountDTO = save(accountDTO);

        // Create establishment for DOCTOR_ADMIN / NURSE_ADMIN
        if (dto.getPosition().equals(Constants.ROLE_DOCTOR_ADMIN) ||
                dto.getPosition().equals(Constants.ROLE_NURSE_ADMIN)
        ) {
            MedicalDepartment mD = medicalDepartmentRepository.findById(dto.getIdDepartment()).orElseThrow(
                    () -> new MedicException(MedicException.ERROR_MEDICAL_DEPARTMENT_NOT_EXIST, "Medical department is not found.")
            );

            Establishment establishment = new Establishment();
            establishment.setName(accountDTO.getName());
            establishment.setEstablishmentType(EstablishmentType.MEDICAL_TREATMENT);
            establishment.setMedicalDepartment(mD);
            establishment.setCreateAt(LocalDateTime.now());
            Establishment savedEst = establishmentRepository.save(establishment);

            // Update establishment for account
            Account account = treatmentRepository.findById(newAccountDTO.getId()).get();
            account.setEstablishment(savedEst);
            treatmentRepository.saveAndFlush(account);
        }

        return treatmentRepository.findTreatmentAccountsById(DeleteStatus.AVAILABLE.getValue(), newAccountDTO.getId()).get();
    }

    /**
     * Update account.
     *
     * @param id  the id of account
     * @param dto the data to update
     * @return the account is updated successfully
     */
    @Transactional
    public TreatmentDTO updateAccount(Integer id, TreatmentDTO dto) {
        update(id, dto.getName(), dto.getDefaultPassword());


        // Update role
        String roleName = "";
        switch (dto.getPosition()) {
            case Constants.ROLE_DOCTOR_ADMIN:
            case Constants.ROLE_NURSE_ADMIN:
                roleName = dto.getPosition();
                break;
            default:
                throw new MedicException(MedicException.ERROR_NOT_FOUND_ROLE, "Not found valid role.");

        }

        Role role = roleRepository.findRoleByName(roleName).get();
        Account account = treatmentRepository.findById(id).get();
        account.setRole(role);
        treatmentRepository.saveAndFlush(account);
        // End update role

        return treatmentRepository.findTreatmentAccountsById(DeleteStatus.AVAILABLE.getValue(), id).get();

    }

    @Override
    protected Object getDtoAfterDelete(Integer id){
        return treatmentRepository.findTreatmentAccountsById(DeleteStatus.IS_DELETED.getValue(), id).get();
    }
}
