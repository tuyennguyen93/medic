package com.mmedic.rest.account.impl;

import com.mmedic.config.prop.ApplicationProperties;
import com.mmedic.entity.*;
import com.mmedic.enums.AuthType;
import com.mmedic.enums.DeleteStatus;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.dto.AuthMethodDTO;
import com.mmedic.rest.account.dto.ResetPasswordDTO;
import com.mmedic.rest.account.mapper.AccountMapper;
import com.mmedic.rest.authentication.dto.OtpDTO;
import com.mmedic.rest.account.repo.AuthMethodRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRepository;
import com.mmedic.rest.patient.repo.PatientRepository;
import com.mmedic.rest.patient.dto.PatientDTO;
import com.mmedic.rest.role.dto.RoleDTO;
import com.mmedic.rest.role.repo.RoleRepository;
import com.mmedic.security.AccountPrincipal;
import com.mmedic.security.JWTAuthenticationToken;
import com.mmedic.service.email.EmailSender;
import com.mmedic.service.firebase.FirebaseNotification;
import com.mmedic.service.sms.SMSSender;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Base58;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import com.mmedic.utils.OtpGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Account Service implementation.
 *
 * @author hungp
 */
@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    public static final String EMAIL_TEMPLATE_VERIFY_ACCOUNT = "verifyAccountStaff";
    public static final String EMAIL_TEMPLATE_RESET_PASSWORD = "resetPassword";

    private AccountRepository accountRepository;

    private RoleRepository roleRepository;

    private PatientRepository patientRepository;

    private AuthMethodRepository authMethodRepository;

    private EstablishmentRepository establishmentRepository;

    private MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    private MedicalServiceRepository medicalServiceRepository;

    private SMSSender smsSender;

    private EmailSender emailSender;

    private AccountMapper accountMapper;

    private PasswordEncoder passwordEncoder;

    private ApplicationProperties applicationProperties;

    private FirebaseNotification firebaseNotification;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Value("${email.no_reply}")
    private String noReplyEmail;

    public AccountServiceImpl(AccountRepository accountRepository, RoleRepository roleRepository,
                              PatientRepository patientRepository, AuthMethodRepository authMethodRepository,
                              EstablishmentRepository establishmentRepository, EmailSender emailSender,
                              AccountMapper accountMapper, SMSSender smsSender,
                              @Lazy PasswordEncoder passwordEncoder,
                              ApplicationProperties applicationProperties,
                              MedicalServiceRegistryRepository medicalServiceRegistryRepository,
                              MedicalServiceRepository medicalServiceRepository,
                              FirebaseNotification firebaseNotification) {
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.patientRepository = patientRepository;
        this.authMethodRepository = authMethodRepository;
        this.establishmentRepository = establishmentRepository;
        this.emailSender = emailSender;
        this.accountMapper = accountMapper;
        this.passwordEncoder = passwordEncoder;
        this.applicationProperties = applicationProperties;
        this.smsSender = smsSender;
        this.medicalServiceRegistryRepository = medicalServiceRegistryRepository;
        this.medicalServiceRepository = medicalServiceRepository;
        this.firebaseNotification = firebaseNotification;
    }

    @Override
    public Optional<Account> findAccountByEmail(String email) {
        return accountRepository.findAccountByEmail(email);
    }

    @Override
    public Account findPatientAccountByPatientId(int patientId) {
        Optional<Account> account = accountRepository.findPatientAccountByPatientId(patientId);
        if (!account.isPresent()) {
            throw new MedicException(MedicException.ERROR_ACCOUNT_NOT_EXIST, "Patient Account doesn't exist");
        }
        return account.get();
    }

    @Override
    @Transactional
    public AccountDTO saveAdminAccount(AccountDTO accountDTO) throws MedicException {
        Account account = accountMapper.toAccount(accountDTO);
        if (validateAdminAccount(account)) {
            if (!Helper.accountHasAnyRole(account, Constants.ROLE_SUPER_ADMIN, Constants.ROLE_ADMIN)) {
                throw new MedicException(MedicException.ERROR_ADMIN_ACCOUNT_NOT_HAVE_ROLE_ADMIN, "The admin account doesn't have the role of admin or super admin");
            }
            if (account.getAuthMethods().size() > 1) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_HAVE_MORE_THAN_TWO_AUTH_METHOD, "The admin account can only have an email authentication method");
            }
            if (account.getAuthMethods().stream().noneMatch(authMethod -> authMethod.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD))) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_NOT_HAVE_EMAIL_AUTH_METHOD, "The admin account can only have an email authentication method");
            }
            Optional<Account> optionalAccount = this.findAccountByEmail(account.getAuthMethods().get(0).getAuthData1());
            if (optionalAccount.isPresent()) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_ALREADY_EXIST, "The admin account already exists");
            }
            setEntityRoleForAccount(account);
            account.setIsDeleted(DeleteStatus.AVAILABLE.getValue());
            account.setIsActived(Boolean.TRUE);
            // Save new admin account
            return accountMapper.toAccountDto(accountRepository.save(account));
        }
        return null;
    }

    @Override
    @Transactional
    public AccountDTO savePatientAccount(PatientDTO patientDTO) throws MedicException {
        AccountDTO accountDTO = convertPatientDTOToAccountDTO(patientDTO);
        Account account = accountMapper.toAccount(accountDTO);
        if (validatePatientAccount(account)) {
            // Set entity role for account
            setEntityRoleForAccount(account);
            account.setIsActived(true);
            account.setIsDeleted(false);
            // Save account to database
            account = accountRepository.save(account);
            // Set entity account for patient
            account.getPatient().setAccount(account);
            // Save patient to database
            Patient patient = patientRepository.save(account.getPatient());
            account.setPatient(patient);

            String otp = generateTOTP(account);
            AccountDTO dto = accountMapper.toAccountDto(account);
            smsSender.sendSms(account.getAuthMethods().get(0).getAuthData1(), Helper.formatOTPMessageVerify(otp));
            return dto;
        }
        return null;
    }

    @Override
    @Transactional
    public AccountDTO saveStaffAccount(AccountDTO accountDTO) throws MedicException {
        Account account = accountMapper.toAccount(accountDTO);
        if (validateStaffAccount(account)) {
            if (Helper.accountHasAnyRole(account, Constants.ROLE_SUPER_ADMIN, Constants.ROLE_ADMIN, Constants.ROLE_PATIENT)) {
                throw new MedicException(MedicException.ERROR_STAFF_ACCOUNT_NOT_HAVE_ROLE_STAFF, "The staff account doesn't have the role of staff");
            }
            if (account.getAuthMethods().size() > 1) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_HAVE_MORE_THAN_TWO_AUTH_METHOD, "The staff account can only have an email authentication method");
            }
            if (account.getAuthMethods().stream().noneMatch(authMethod -> authMethod.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD))) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_NOT_HAVE_EMAIL_AUTH_METHOD, "The staff account can only have an email authentication method");
            }
            Optional<Account> optionalAccount = this.findAccountByEmail(account.getAuthMethods().get(0).getAuthData1());
            if (optionalAccount.isPresent()) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_ALREADY_EXIST, "The staff account already exists");
            }
            setEntityRoleForAccount(account);
            if (null != account.getEstablishment()) {
                setEntityEstablishmentForAccount(account);
            }
//            account.getAuthMethods().get(0).setAuthData2(Base58.generate());
            account.setIsActived(false);
            account.setSecret(Base58.generate());
            account.setSecretCreateTime(LocalDateTime.now());
            account = accountRepository.save(account);

            // Send email to verify account.
            Map<String, String> emailMap = new HashMap<>();
            emailMap.put("link", applicationProperties.getWebUrl() +
                    applicationProperties.getVerifyAccount().getLink() + Constants.SLASH +
                    account.getSecret());
            emailSender.sendMail(noReplyEmail, account.getAuthMethods().get(0).getAuthData1(), EMAIL_TEMPLATE_VERIFY_ACCOUNT, emailMap);
            return accountMapper.toAccountDto(account);
        }
        return null;
    }

    @Transactional
    @Override
    public AccountDTO createNewAccount(AccountDTO accountDTO) throws MedicException {
        Account account = accountMapper.toAccount(accountDTO);
        if (validateStaffAccount(account)) {
            if (account.getAuthMethods().size() > 1) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_HAVE_MORE_THAN_TWO_AUTH_METHOD, "The staff account can only have an email authentication method");
            }
            if (account.getAuthMethods().stream().noneMatch(authMethod -> authMethod.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD))) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_NOT_HAVE_EMAIL_AUTH_METHOD, "The staff account can only have an email authentication method");
            }
            Optional<Account> optionalAccount = this.findAccountByEmail(account.getAuthMethods().get(0).getAuthData1());
            if (optionalAccount.isPresent()) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_ALREADY_EXIST, "The staff account already exists");
            }
            setEntityRoleForAccount(account);

//            account.getAuthMethods().get(0).setAuthData2(passwordEncoder.encode(accountDTO.getAuthMethods().get(0).getAuthData2()));
            account.setIsActived(false);
            account.setIsDeleted(DeleteStatus.AVAILABLE.getValue());
            account.setSecret(Base58.generate());
            account.setSecretCreateTime(LocalDateTime.now());
            account = accountRepository.save(account);

            // Send email to verify account.
            Map<String, String> emailMap = new HashMap<>();
            emailMap.put("link", applicationProperties.getWebUrl() +
                    applicationProperties.getVerifyAccount().getLink() + Constants.SLASH +
                    account.getSecret());
            emailSender.sendMail(noReplyEmail, account.getAuthMethods().get(0).getAuthData1(), EMAIL_TEMPLATE_VERIFY_ACCOUNT, emailMap);
            return accountMapper.toAccountDto(account);
        }
        return null;
    }

    @Transactional
    @Override
    public String requestResetPassword(String email) throws MedicException {
        if (!Helper.validateEmail(email)) {
            throw new MedicException(MedicException.ERROR_EMAIL_INVALID, "The email is incorrect format");
        }
        accountRepository.findAccountByEmail(email).ifPresent(account -> {
            account.setSecret(Base58.generate());
            account.setSecretCreateTime(LocalDateTime.now());
            account.getAuthMethods().stream().
                    filter(auth -> auth.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD)).
                    findFirst().
                    ifPresent(auth -> {
                        // Send email reset password
                        Map<String, String> emailMap = new HashMap<>();
                        emailMap.put("link", applicationProperties.getWebUrl() +
                                applicationProperties.getResetPassword().getLink() + Constants.SLASH +
                                account.getSecret());
                        emailSender.sendMail(noReplyEmail, auth.getAuthData1(), EMAIL_TEMPLATE_RESET_PASSWORD, emailMap);
                    });
        });
        return email;
    }

    @Override
    @Transactional
    public boolean resetPassword(ResetPasswordDTO resetPasswordDTO) throws MedicException {
        if (null != resetPasswordDTO) {
            Account account = accountRepository.findAccountBySecret(resetPasswordDTO.getCode()).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_VERIFY_CODE_NOT_EXIST, "Verify code doesn't exist"));
            if (LocalDateTime.now().getSecond() - account.getSecretCreateTime().getSecond() > applicationProperties.getResetPassword().getTimeLimitSeconds()) {
                throw new MedicException(MedicException.ERROR_VERIFY_CODE_IS_EXPIRE, "The verify code is expiration");
            }
            account.getAuthMethods().stream().
                    filter(authMethod -> authMethod.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD)).
                    findFirst().
                    ifPresent(auth -> {
                        auth.setAuthData2(passwordEncoder.encode(resetPasswordDTO.getPassword()));
                    });
            account.setSecret(null);
            account.setSecretCreateTime(null);
            accountRepository.save(account);
            return true;
        }
        return false;
    }

    @Override
    public boolean generateAndSendTOTP(OtpDTO otpDTO) throws MedicException {
        if (null != otpDTO) {
            Optional<Account> optionalAccount = accountRepository.findPatientAccountByPatientId(otpDTO.getPatientId());
            if (!optionalAccount.isPresent()) {
                throw new MedicException(MedicException.ERROR_ACCOUNT_NOT_EXIST, "Patient Account doesn't exist");
            }
            if (optionalAccount.get().getAuthMethods().stream().noneMatch(authMethod -> authMethod.getAuthType().equals(AuthType.PHONE_NUMBER))) {
                throw new MedicException(MedicException.ERROR_PATIENT_NOT_HAVE_PHONE_NUMBER_AUTH_METHOD, "Not found phone number authentication");
            }
            // Generate otp value
            String otp = generateTOTP(optionalAccount.get());
            // Send OTP value to phone number of user
            smsSender.sendSms(optionalAccount.get().getAuthMethods().get(0).getAuthData1(), Helper.formatOTPMessageVerify(otp));
            return true;
        }
        return false;
    }

    /**
     * Generate OTP.
     *
     * @param account the account
     * @return OTP value
     * @throws MedicException
     */
    private String generateTOTP(Account account) throws MedicException {
        if (null != account && account.getId() > 0) {
            if (!account.getRole().getName().equals(Constants.ROLE_PATIENT)) {
                throw new MedicException(MedicException.ERROR_PATIENT_ACCOUNT_NOT_HAVE_ROLE_PATIENT, "This account doesn't have the PATIENT role");
            }
            AuthMethod authMethod = account.getAuthMethods().stream().
                    filter(auth -> auth.getAuthType().equals(AuthType.PHONE_NUMBER)).findFirst().orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_HAVE_PHONE_NUMBER_AUTH_METHOD, "Not found phone number authentication"));
            if (authMethod.getId() > 0) {
                long T0 = Instant.now().toEpochMilli();
                authMethod.setAuthData2(Base58.generate());
                authMethod.setAuthData3(String.valueOf(T0));
                authMethodRepository.save(authMethod);
                long time = (Instant.now().toEpochMilli() - T0) / 1000 / applicationProperties.getTotp().getTimeLimitSeconds();
                return OtpGenerator.generateTOTP512(authMethod.getAuthData2(), String.valueOf(time), String.valueOf(applicationProperties.getTotp().getDigit()));
            }
        }
        return null;
    }

    @Override
    public List<AccountDTO> findAdminAccounts() {
        return accountMapper.toListAccountDTO(accountRepository.findAccountsByRole_Name(Constants.ROLE_SUPER_ADMIN, Constants.ROLE_ADMIN));
    }

    @Override
    public UserDetails loadUserByPatientIdAndPhone(int patientId, String phone) {
        UserDetails userDetails = null;

        Optional<Account> optionalAccount = accountRepository.findAccountByIdAndPhone(patientId, phone);
        if (optionalAccount.isPresent()) {
            try {
                userDetails = AccountPrincipal.create(optionalAccount.get());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return userDetails;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails userDetails = null;

        Optional<Account> optionalAccount = accountRepository.findAccountByEmail(username);
        if (optionalAccount.isPresent()) {
            try {
                userDetails = AccountPrincipal.create(optionalAccount.get());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        return userDetails;
    }

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        UserDetails userDetails = null;
        JWTAuthenticationToken jwtToken = (JWTAuthenticationToken) token;
        AccountPrincipal principal = (AccountPrincipal) jwtToken.getAuthentication().getPrincipal();

        Optional<Account> optionalAccount = Optional.empty();
        if (null != principal) {
            if (principal.getAuthType().equals(AuthType.PHONE_NUMBER)) {
                optionalAccount = accountRepository.findAccountByIdAndPhone(principal.getId(), principal.getUsername());
            } else if (principal.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD)) {
                optionalAccount = accountRepository.findAccountByEmail(principal.getUsername());
            }
        }
        if (optionalAccount.isPresent()) {
            try {
                userDetails = AccountPrincipal.create(optionalAccount.get());
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        return userDetails;
    }

    @Override
    @Transactional
    public boolean verifyEmailAccount(ResetPasswordDTO resetPasswordDTO) throws MedicException {
        Account account = accountRepository.findAccountBySecret(resetPasswordDTO.getCode()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_VERIFY_CODE_NOT_EXIST, "Verify code doesn't exist"));
        if (LocalDateTime.now().getSecond() - account.getSecretCreateTime().getSecond() > applicationProperties.getVerifyAccount().getTimeLimitSeconds()) {
            throw new MedicException(MedicException.ERROR_VERIFY_CODE_IS_EXPIRE, "The verify code is expiration");
        }
        AuthMethod authMethod = account.getAuthMethods().stream().
                filter(auth -> auth.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD)).
                findFirst().
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_NOT_HAVE_EMAIL_AUTH_METHOD));
        authMethod.setAuthData2(passwordEncoder.encode(resetPasswordDTO.getPassword()));
        account.setIsActived(true);
        account.setSecret(null);
        account.setSecretCreateTime(null);
        accountRepository.save(account);

        // Register default service for Doctor/Drug Store account
        // Doctor: 1: Khám bệnh; 2: Di chuyển
        // Drug Store: 3: Cấp thuốc; 4: Giao thuốc tận nhà
        int start = 0;
        int end = 0;
        if (account.getRole().getName().equals(Constants.ROLE_DOCTOR_ADMIN)) {
            start = 1;
            end = 3;
        } else if (account.getRole().getName().equals(Constants.ROLE_DRUG_STORE_ADMIN)) {
            start = 3;
            end = 5;
        } else if (account.getRole().getName().equals(Constants.ROLE_PRECLINIC_ADMIN)) {
            start = 5;
            end = 7;
        } else {
            return true;
        }

        List<MedicalServiceRegistry> lst = new ArrayList<>();

        for (int i = start; i < end; i++) {
            MedicalServiceRegistry medicalServiceRegistry = new MedicalServiceRegistry();
            medicalServiceRegistry.setEstablishment(establishmentRepository.findById(account.getEstablishment().getId()).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_NOT_FOUND_ESTABLISHMENT)));
            medicalServiceRegistry.setMedicalService(medicalServiceRepository.findByIdAndIsDeletedIsFalse(i).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Medical service not found")));
            medicalServiceRegistry.setActive(true);
            medicalServiceRegistry.setCreateAt(LocalDateTime.now());
            lst.add(medicalServiceRegistry);
        }

        medicalServiceRegistryRepository.saveAll(lst);


        return true;
    }

    @Override
    @Transactional
    public boolean resendVerifyEmailAccount(String email) throws MedicException {
        Account account = accountRepository.findAccountByEmail(email).orElse(null);
        if (null != account && !account.getIsActived()) {
            account.setSecret(Base58.generate());
            account.setSecretCreateTime(LocalDateTime.now());
            accountRepository.save(account);
            // Send email to verify account.
            Map<String, String> emailMap = new HashMap<>();
            emailMap.put("link", applicationProperties.getWebUrl() +
                    applicationProperties.getVerifyAccount().getLink() + Constants.SLASH +
                    account.getSecret());
            emailSender.sendMail(noReplyEmail, account.getAuthMethods().get(0).getAuthData1(), EMAIL_TEMPLATE_VERIFY_ACCOUNT, emailMap);
            return true;
        }
        return false;
    }

    /**
     * Convert patient dto to account dto.
     *
     * @param patientDTO the patient data.
     * @return account dto
     */
    private AccountDTO convertPatientDTOToAccountDTO(PatientDTO patientDTO) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(patientDTO.getName());
        accountDTO.setDateOfBirth(patientDTO.getDateOfBirth());
        // Set role
        RoleDTO patientRole = new RoleDTO();
        patientRole.setName(Constants.ROLE_PATIENT);
        accountDTO.setRole(patientRole);
        // Set auth method
        AuthMethodDTO authMethodDTO = new AuthMethodDTO();
        authMethodDTO.setAuthType(AuthType.PHONE_NUMBER);
        authMethodDTO.setAuthData1(patientDTO.getPhoneNumber());
        accountDTO.setAuthMethods(Collections.singletonList(authMethodDTO));
        // Set patient
        accountDTO.setPatient(patientDTO);
        return accountDTO;
    }

    /**
     * Set entity establishment for account.
     *
     * @param account the account
     */
    private void setEntityEstablishmentForAccount(Account account) {
        Establishment establishment = establishmentRepository.findById(account.getEstablishment().getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_NOT_FOUND_ESTABLISHMENT));
        account.setEstablishment(establishment);
    }

    /**
     * Set entity role for account.
     *
     * @param account the account
     * @throws MedicException
     */
    private void setEntityRoleForAccount(Account account) throws MedicException {
        Role adminRole = roleRepository.findRoleByName(account.getRole().getName()).orElseThrow(() -> new MedicException(MedicException.ERROR_NOT_FOUND_ROLE));
        if (adminRole.getName().equals(Constants.ROLE_SUPER_ADMIN) && accountRepository.countAccountByRole(adminRole) >= 1) {
            throw new MedicException(MedicException.ERROR_SYSTEM_ALREADY_HAVE_SUPER_ADMIN, "The system had been the super admin account");
        }
        account.setRole(adminRole);
    }

    /**
     * Validation for admin account.
     *
     * @param account the account to validate
     * @return true if all date is valid
     * @throws Exception
     */
    private boolean validateAdminAccount(Account account) throws MedicException {
        // Validation account
        validateAccount(account);
        validateAuthMethod(account.getAuthMethods(), false);
        return true;
    }

    /**
     * Validate for patient account.
     *
     * @param account the account to validate
     * @return true if all data is valid
     * @throws MedicException
     */
    private boolean validatePatientAccount(Account account) throws MedicException {
        // Validation account
        validateAccount(account);
        validateAuthMethod(account.getAuthMethods(), false);
        Set<ConstraintViolation<Patient>> constraintViolationsPatient = validator.validate(account.getPatient());
        if (constraintViolationsPatient.size() > 0) {
            throw new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "The account data is invalid");
        }
        return true;
    }

    /**
     * Validate for staff account.
     *
     * @param account the account to validate
     * @return true if all data is valid
     * @throws MedicException
     */
    private boolean validateStaffAccount(Account account) throws MedicException {
        // Validation account
        validateAccount(account);
        validateAuthMethod(account.getAuthMethods(), false);
        return true;
    }

    /**
     * Validate account
     *
     * @param account the account to validate
     * @return true if all data is valid
     * @throws MedicException
     */
    private boolean validateAccount(Account account) throws MedicException {
        Set<ConstraintViolation<Account>> constraintViolationsAccount = validator.validate(account);
        if (constraintViolationsAccount.size() > 0) {
            throw new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "The account data is invalid");
        }
        return true;
    }

    /**
     * Validate auth method.
     *
     * @param authMethods     list auth method
     * @param verifyAuthData2 if this value is true then validate field auth_data_2
     * @return true if all data is valid
     * @throws MedicException
     */
    private boolean validateAuthMethod(List<AuthMethod> authMethods, boolean verifyAuthData2) throws MedicException {
        if (!CollectionUtils.isEmpty(authMethods)) {
            for (AuthMethod authMethod : authMethods) {
                Set<ConstraintViolation<AuthMethod>> constraintViolationsAuthMethod = validator.validate(authMethod);
                if (constraintViolationsAuthMethod.size() > 0) {
                    throw new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "The account data is invalid");
                }
                if (StringUtils.isEmpty(authMethod.getAuthData1()) || (verifyAuthData2 && StringUtils.isEmpty(authMethod.getAuthData2()))) {
                    throw new MedicException(MedicException.ERROR_ACCOUNT_AUTH_DATA_INVALID, "The authentication data of account is invalid");
                }
                if (authMethod.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD) && !Helper.validateEmail(authMethod.getAuthData1())) {
                    throw new MedicException(MedicException.ERROR_EMAIL_INVALID, "The email of account is invalid");
                } else if (authMethod.getAuthType().equals(AuthType.PHONE_NUMBER) && !Helper.validatePhoneNumber(authMethod.getAuthData1())) {
                    throw new MedicException(MedicException.ERROR_PHONE_INVALID, "The phone number of account is invalid");
                }
            }
        }
        return true;
    }

    @Override
    public List<String> getNotificationToken(Account currentAccount) throws MedicException {
        List<String> token = null;
        if (null != currentAccount) {
            token = Arrays.asList(Helper.convertNullToEmpty(currentAccount.getNotificationToken()).split(Constants.SEPARATE_TOKEN_REGEX));
        }
        return token;
    }


    @Override
    @Transactional
    public boolean registrationTokenForNotification(String registrationToken, Account currentAccount) throws MedicException {
        if (!StringUtils.isEmpty(registrationToken)) {
            // Set new notification token array
            currentAccount.setNotificationToken(registrationToken);
            accountRepository.save(currentAccount);

            // Subscribe topic for establishment
            if (null != currentAccount.getEstablishment()) {
                firebaseNotification.subscribeTopic(getTopicNameFromEstablishmentType(currentAccount.getEstablishment()), registrationToken);
            }
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean appendRegistrationTokenForNotification(String registrationToken, Account currentAccount) throws MedicException {
        if (!StringUtils.isEmpty(registrationToken)) {
            String[] notificationTokens = Helper.convertNullToEmpty(currentAccount.getNotificationToken()).split(Constants.SEPARATE_TOKEN_REGEX);
            if (!Arrays.asList(notificationTokens).contains(registrationToken)) {
                String currentToken = Helper.convertNullToEmpty(currentAccount.getNotificationToken());
                String notificationToken = (!StringUtils.isEmpty(currentToken) ? currentToken + Constants.SEPARATE_TOKEN : currentToken) +
                        registrationToken;

                // Set new notification token array
                currentAccount.setNotificationToken(notificationToken);
                accountRepository.save(currentAccount);

                // Subscribe topic for establishment
                if (null != currentAccount.getEstablishment()) {
                    firebaseNotification.subscribeTopic(getTopicNameFromEstablishmentType(currentAccount.getEstablishment()), registrationToken);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    @Transactional
    public boolean removeRegistrationTokenForNotification(String registrationToken) throws MedicException {
        if (!StringUtils.isEmpty(registrationToken)) {
            List<Account> accounts = accountRepository.findByNotificationTokenLike(Helper.convertQueryParamToSql(registrationToken));
            if (!CollectionUtils.isEmpty(accounts)) {
                List<Account> lstAccountChange = new ArrayList<>();
                accounts.forEach(account -> {
                    List<String> notificationTokens = new ArrayList<>(Arrays.asList(Helper.convertNullToEmpty(account.getNotificationToken()).split(Constants.SEPARATE_TOKEN_REGEX)));
                    if (notificationTokens.contains(registrationToken)) {
                        notificationTokens.remove(registrationToken);

                        // Update notification token array
                        account.setNotificationToken(String.join(Constants.SEPARATE_TOKEN, notificationTokens));
                        lstAccountChange.add(account);

                        // Unsubscribe topic for establishment
                        if (null != account.getEstablishment()) {
                            firebaseNotification.unsubscribeTopic(getTopicNameFromEstablishmentType(account.getEstablishment()), registrationToken);
                        }
                    }
                });
                if (!CollectionUtils.isEmpty(lstAccountChange)) {
                    accountRepository.saveAll(accounts);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get topic name from establishment type.
     *
     * @param establishment establishment
     * @return prefix of topic
     */
    private String getTopicNameFromEstablishmentType(Establishment establishment) {
        switch (establishment.getEstablishmentType()) {
            case DRUG_STORE:
                return FirebaseNotification.DRUG_STORE_TOPIC + establishment.getId();
            case DRUG_WAREHOUSE:
                return FirebaseNotification.DRUG_WAREHOUSE_TOPIC + establishment.getId();
            case PRECLINICAL:
                return FirebaseNotification.PRECLINICAL_TOPIC + establishment.getId();
            case MEDICAL_TREATMENT:
                return FirebaseNotification.MEDICAL_TREATMENT_TOPIC + establishment.getId();
            default:
                return Constants.EMPTY_STRING;
        }
    }

    @Transactional
    @Override
    public boolean updateLanguage(Account currentAccount, String language) throws MedicException {
        if (null != currentAccount && !StringUtils.isEmpty(language) && language.length() == 2) {
            currentAccount.setLanguage(language);
            accountRepository.save(currentAccount);
            return true;
        }
        return false;
    }
}
