package com.mmedic.rest.account.impl;

import com.mmedic.enums.DeleteStatus;
import com.mmedic.rest.account.repo.UserRepository;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.dto.UserDTO;
import com.mmedic.rest.role.dto.RoleDTO;
import com.mmedic.utils.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * User service.
 */
@RequiredArgsConstructor
@Service
public class UserService extends AbstractStaffAccountService {

    private final UserRepository userRepository;

    /**
     * Get all user account.
     *
     * @param pageable pagination
     * @return list user account
     */
    public Page<UserDTO> getAccountList(Pageable pageable) {
        Page<UserDTO> accountList = userRepository.findAvailableUserAccounts(pageable);
        return accountList;
    }

    /**
     * Create new user account.
     *
     * @param dto the account data
     * @return the account is created successfully
     */
    @Transactional
    public UserDTO createAccount(UserDTO dto) {
        AccountDTO accountDTO = convertToAccountDTO(dto.getEmail(), dto.getName());
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setName(Constants.ROLE_ADMIN);
        accountDTO.setRole(roleDTO);
        AccountDTO newAccountDTO = save(accountDTO);
        return userRepository.findUserAccountsById(DeleteStatus.AVAILABLE.getValue(), newAccountDTO.getId()).get();
    }

    /**
     * Update account.
     *
     * @param id  the id of account
     * @param dto the data to udpate
     * @return the account is updated successfully
     */
    @Transactional
    public UserDTO updateAccount(Integer id, UserDTO dto) {
        update(id, dto.getName(), dto.getDefaultPassword());
        return userRepository.findUserAccountsById(DeleteStatus.AVAILABLE.getValue(), id).get();

    }


    @Override
    protected Object getDtoAfterDelete(Integer id){
        return userRepository.findUserAccountsById(DeleteStatus.IS_DELETED.getValue(), id).get();
    }

}
