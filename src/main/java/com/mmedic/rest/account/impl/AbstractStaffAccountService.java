package com.mmedic.rest.account.impl;

import com.mmedic.entity.Account;
import com.mmedic.entity.AuthMethod;
import com.mmedic.entity.Establishment;
import com.mmedic.enums.AuthType;
import com.mmedic.enums.DeleteStatus;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.dto.AuthMethodDTO;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.account.repo.AuthMethodRepository;
import com.mmedic.rest.certification.repo.CertificationRepository;
import com.mmedic.rest.establishment.repo.EstablishmentDayOffRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.spring.errors.MedicException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Abstract Staff Account Service.
 */
public abstract class AbstractStaffAccountService {

    @Autowired
    protected AccountService accountService;

    @Autowired
    protected AccountRepository accountRepository;

    @Autowired
    protected AuthMethodRepository authMethodRepository;

    @Autowired
    protected EstablishmentRepository establishmentRepository;

    @Autowired
    protected EstablishmentDayOffRepository establishmentDayOffRepository;

    @Autowired
    protected EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    @Autowired
    protected CertificationRepository certificationRepository;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    /**
     * Save account.
     *
     * @param accountDTO the account data
     * @return the account is created successfully
     */
    protected AccountDTO save(AccountDTO accountDTO) {
        return accountService.createNewAccount(accountDTO);
    }

    /**
     * Update name field of account.
     *
     * @param id              the id of account
     * @param nameAccount     new name of account
     * @param defaultPassword default password
     */
    protected void update(Integer id, String nameAccount, String defaultPassword) {
        Account acc = accountRepository.
                findById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_NOT_EXIST, "Account is not found."));
        acc.setName(nameAccount);
        AuthMethod authMethod = acc.getAuthMethods().stream().filter(r -> r.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD)).findFirst().
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_NOT_HAVE_EMAIL_AUTH_METHOD, "Account is not have email auth method."));
        if (StringUtils.isNotEmpty(defaultPassword)) {
            authMethod.setAuthData2(passwordEncoder.encode(defaultPassword));

        }
        accountRepository.save(acc);
    }

    /**
     * Delete account.
     *
     * @param id the id of account
     */
    private void setIsDeleteStatus(Integer id) {
        Account acc = accountRepository.
                findById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_NOT_EXIST, "Account is not found."));
        acc.setIsDeleted(DeleteStatus.IS_DELETED.getValue());
        accountRepository.saveAndFlush(acc);
    }

    /**
     * Get data after deleted
     *
     * @param id account Id
     * @return data object
     */
    protected abstract Object getDtoAfterDelete(Integer id);

    /**
     * Delete account.
     *
     * @param accountId account id
     * @return data object
     */
    @Transactional
    public Object deleteAccount(Integer accountId){
        setIsDeleteStatus(accountId);
        Object data = getDtoAfterDelete(accountId);
        checkUsedAndRemoveAccount(accountId);
        return data;
    }

    /**
     * Check account is not using then remove from database.
     *
     * @param accountId accountId
     */
    private void checkUsedAndRemoveAccount(Integer accountId){
        Account acc = accountRepository.
                findById(accountId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_NOT_EXIST, "Account is not found."));

        // Remove account and auth methods
        if(!accountRepository.isUsingAccount(accountId)){
            authMethodRepository.deleteAll(acc.getAuthMethods());
            accountRepository.deleteById(accountId);
        }

        // Remove establishment of account in case it is a staff admin
        if (acc.getRole().getName().endsWith("_ADMIN")){
            checkUsedAndRemoveEstablishment(acc.getEstablishment().getId());
        }
    }

    /**
     * Check establishment is not using then remove from database.
     *
     * @param establishtmentId
     */
    private void checkUsedAndRemoveEstablishment(Integer establishtmentId){
        if(!accountRepository.isUsingEstablishment(establishtmentId)){
            Establishment establishment = establishmentRepository.getOne(establishtmentId);
            establishmentDayOffRepository.deleteAll(establishment.getEstablishmentDayOffs());
            establishmentWorkingScheduleRepository.deleteAll(establishment.getEstablishmentWorkingSchedules());
            certificationRepository.deleteAll(establishment.getEstablishmentCertifications());
            establishmentRepository.deleteById(establishtmentId);
        }
    }

    /**
     * Convert to account dto.
     *
     * @param email       the email of account
     * @param accountName the account name
     * @return the account dto
     */
    protected AccountDTO convertToAccountDTO(String email, String accountName) {
        AuthMethodDTO mailAuthMethodDTO = new AuthMethodDTO();

        mailAuthMethodDTO.setAuthType(AuthType.EMAIL_AND_PASSWORD);
        mailAuthMethodDTO.setAuthData1(email);
//        mailAuthMethodDTO.setAuthData2(defaultPassword);

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setName(accountName);
        accountDTO.setAuthMethods(new ArrayList<>());
        accountDTO.getAuthMethods().add(mailAuthMethodDTO);

        return accountDTO;
    }
}
