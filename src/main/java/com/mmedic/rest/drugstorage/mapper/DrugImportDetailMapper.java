package com.mmedic.rest.drugstorage.mapper;

import com.mmedic.entity.DrugImportDetail;
import com.mmedic.rest.drugstorage.dto.DrugInfoImportDTO;
import com.mmedic.utils.DateMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = DateMapper.class)
public interface DrugImportDetailMapper {

    DrugInfoImportDTO convertToDTO(DrugImportDetail entity);

    DrugImportDetail convertToEntity(DrugInfoImportDTO dto);

}
