package com.mmedic.rest.drugstorage.mapper;

import com.mmedic.entity.Establishment;
import com.mmedic.rest.drugstorage.dto.WarehouseDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WarehouseMapper {

    WarehouseDTO toWarehouseDto(Establishment establishment);

    List<WarehouseDTO> toListWarehouseDTO(List<Establishment> establishments);
}
