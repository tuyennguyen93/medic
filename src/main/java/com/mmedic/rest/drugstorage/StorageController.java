package com.mmedic.rest.drugstorage;

import com.mmedic.entity.Account;
import com.mmedic.enums.ImportType;
import com.mmedic.enums.WareHouseExcelType;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.drugstorage.dto.*;
import com.mmedic.rest.drugstorage.service.StorageExportService;
import com.mmedic.rest.drugstorage.service.StorageImportService;
import com.mmedic.rest.drugstorage.service.WarehouseService;
import com.mmedic.service.file.FileStorageService;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

/**
 * Storage controller.
 */
@Slf4j
@RestController
@RequestMapping(StorageController.BASE_URL)
@Api(value = "Storage management", description = "Storage CRUD for Admin page")
@RequiredArgsConstructor
public class StorageController {
    static final String BASE_URL = "/api/v1/storages";

    private final StorageImportService storageImportService;

    private final StorageExportService storageExportService;

    private final FileStorageService fileStorageService;

    private final WarehouseService warehouseService;

    private final BaseService baseService;

    /**
     * Get list warehouse.
     *
     * @param query condition
     * @return list ware house
     */
    @ApiOperation("Get warehouses (DONE)")
    @GetMapping("/warehouses")
    public MedicResponse<List<WarehouseDTO>> getListWarehouse(@RequestParam(required = false) @ApiParam("Condition") String query) {
        return MedicResponse.okStatus(warehouseService.getAllWarehouse(query));
    }

    /**
     * Get warehouse detail.
     *
     * @param warehouseId the warehouse id
     * @return warehouse detail
     */
    @ApiOperation("Get warehouse detail (DONE)")
    @GetMapping("/warehouses/{warehouseId}")
    public MedicResponse<WarehouseDTO> getWarehouse(@PathVariable("warehouseId") int warehouseId) {
        return MedicResponse.okStatus(warehouseService.getWarehouseDetail(warehouseId));
    }

    /**
     * Creates new warehouse.
     *
     * @param warehouseDTO the warehouse data
     * @return warehouse is created
     */
    @ApiOperation("Create new warehouse (DONE)")
    @PostMapping("/warehouses")
    public MedicResponse<WarehouseDTO> createWarehouse(@RequestBody @ApiParam("Warehouse data") WarehouseDTO warehouseDTO) {
        return MedicResponse.okStatus(warehouseService.createNewWarehouse(warehouseDTO));
    }

    /**
     * Updates data for warehouse.
     *
     * @param warehouseDTO the warehouse data
     * @return warehouse is updated
     */
    @ApiOperation("Update data for warehouse (DONE)")
    @PutMapping("/warehouses/{warehouseId}")
    public MedicResponse<WarehouseDTO> updateWarehouse(@PathVariable("warehouseId") @ApiParam("Warehouse Id") int warehouseId,
                                                       @RequestBody @ApiParam("Warehouse data") WarehouseDTO warehouseDTO) {
        return MedicResponse.okStatus(warehouseService.updateWarehouseDetail(warehouseId, warehouseDTO));
    }

    /**
     * Deletes warehouse.
     *
     * @param warehouseId the warehouse id
     * @return warehouse is deleted
     */
    @ApiOperation("Delete warehouse (DONE)")
    @DeleteMapping("/warehouses/{warehouseId}")
    public MedicResponse<WarehouseDTO> deleteWarehouse(@PathVariable("warehouseId") @ApiParam("Warehouse Id") int warehouseId) {
        return MedicResponse.okStatus(warehouseService.deleteWarehouse(warehouseId));
    }

    /**
     * Get statistic of drug.
     *
     * @param pageable pagination
     * @return list drug
     */
    @ApiOperation("Get all root drug in storage (DONE)")
    @GetMapping("/drugs")
    public MedicResponse<Page<StatisticalDrugDTO>> getListStatisticDrug(@RequestParam(required = false) @ApiParam("Condition") String query,
                                                                        @ApiParam("Pagination") Pageable pageable) {
        return MedicResponse.okStatus(warehouseService.findStatisticDrugInDrugStore(baseService.getCurrentEstablishment(), query, pageable));
    }

    @ApiOperation("Get statistic detail of drug (DONE)")
    @GetMapping("/drugs/{drugId}")
    public MedicResponse<List<StatisticalDrugDetailDTO>> getStatisticDetailDrug(@PathVariable("drugId") int drugId) {
        return MedicResponse.okStatus(warehouseService.findStatisticDrugDetailInDrugStore(drugId, baseService.getCurrentEstablishment()));
    }

    /**
     * Get List import.
     *
     * @param pageable pagination
     * @return list import
     */
    @ApiOperation("Get storage import list (DONE)")
    @GetMapping("/imports")
    public MedicResponse<Page<StorageImportDTO>> getStorageImportList(@RequestParam(required = false) @ApiParam("Establishment Id (For Admin Page)") Integer establishmentId,
                                                                      @ApiParam("Pagination") Pageable pageable) {
        establishmentId = getEstablishmentIdOfAccount(baseService.getCurrentAccountLogin(), establishmentId);
        return MedicResponse.okStatus(storageImportService.getAllImport(establishmentId, pageable));
    }

    /**
     * Get detail of import.
     *
     * @param importId the import id
     * @param type     External or Internal import
     * @return detail of import
     */
    @ApiOperation("Get storage import detail (DONE)")
    @GetMapping("/imports/{importId}")
    public MedicResponse<StorageImportDetailDTO> getStorageImportDetail(@PathVariable("importId") @ApiParam("Import Id") int importId,
                                                                        @RequestParam @ApiParam("Import Type") ImportType type) {
        return MedicResponse.okStatus(storageImportService.getImportDetail(importId, type));
    }

    /**
     * Creates new import.
     *
     * @return result
     */
    @ApiOperation("Create new import (DONE)")
    @PostMapping("/imports")
    public MedicResponse<StorageImportDTO> createImport(@RequestBody ImportParamDTO importParam) {
        return MedicResponse.okStatus(storageImportService.createImport(importParam, baseService.getCurrentAccountLogin()));
    }

    /**
     * Get list export.
     *
     * @param pageable Pagination
     * @return list export
     */
    @ApiOperation("Get storage export list (DONE)")
    @GetMapping("/exports")
    public MedicResponse<Page<StorageExportDTO>> getStorageExportList(@RequestParam(required = false) @ApiParam("Establishment Id (For Admin Page)") Integer establishmentId,
                                                                      @ApiParam("Pagination") Pageable pageable) {
        establishmentId = getEstablishmentIdOfAccount(baseService.getCurrentAccountLogin(), establishmentId);
        return MedicResponse.okStatus(storageExportService.getAllExport(establishmentId, pageable));
    }

    /**
     * Get export detail.
     *
     * @param exportId the export id
     * @return export detail
     */
    @ApiOperation("Get storage export detail (DONE)")
    @GetMapping("/exports/{exportId}")
    public MedicResponse<StorageExportDetailDTO> getStorageExportDetail(@PathVariable("exportId") int exportId) {
        return MedicResponse.okStatus(storageExportService.getExportDetail(exportId));
    }

    /**
     * Creates new export.
     *
     * @return result
     */
    @ApiOperation("Create new export (DONE)")
    @PostMapping("/exports")
    public MedicResponse<StorageExportDTO> createExport(@RequestBody ExportParamDTO exportParam) {
        Account currentAccount = baseService.getCurrentAccountLogin();
        if (Helper.accountHasAnyRole(currentAccount, Constants.ROLE_DRUG_STORE_ADMIN, Constants.ROLE_DRUG_STORE_STAFF)) {
            if (null == currentAccount.getEstablishment()) {
                throw new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Establishment don't exist");
            }
            exportParam.setFromId(currentAccount.getEstablishment().getId());
        }
        return MedicResponse.okStatus(storageExportService.createNewExport(exportParam, baseService.getCurrentAccountLogin()));
    }

    /**
     * Get all drug in storage.
     *
     * @param pageable pagination
     * @return list drug
     */
    @ApiOperation("Get all drugs of warehouse (DONE)")
    @GetMapping("/statistics/drugs")
    public MedicResponse<Page<StatisticalDrugAdminDTO>> getStatisticDrugForAdmin(@RequestParam @ApiParam("Drug Store Id") Integer establishmentId,
                                                                                 @ApiParam("Pagination") Pageable pageable) {
        return MedicResponse.okStatus(warehouseService.findStatisticalDrugInWarehouse(establishmentId, pageable));
    }

    /**
     * Get all batch of drug in storage.
     *
     * @param establishmentId establishment id.
     * @param drugId          the drug id
     * @param pageable        pagination
     * @return list batch
     */
    @ApiOperation("Get list medicine batch of drug (DONE)")
    @GetMapping("/statistics/drugs/{drugId}/batchs")
    public MedicResponse<Page<StatisticalDrugBatchAdminDTO>> getStatisticDrugBatchForAdmin(@RequestParam @ApiParam("Drug Store Id") Integer establishmentId,
                                                                                           @PathVariable("drugId") @ApiParam("Drug Id") int drugId,
                                                                                           @ApiParam("Pagination") Pageable pageable) {
        return MedicResponse.okStatus(warehouseService.findStatisticMedicineBatch(establishmentId, drugId, pageable));
    }

    /**
     * Get detail of drug and batch in storage.
     *
     * @param drugId  the drug id
     * @param batchId the batch id
     * @return detail drug and batch
     */
    @ApiOperation("Statistic for medicine batch for admin (DONE)")
    @GetMapping("/statistics/drugs/{drugId}/batchs/{batchId}")
    public MedicResponse<StatisticalDrugDetailAdminDTO> getStatisticDrugDetailForAdmin(@RequestParam @ApiParam("Drug Store Id") Integer establishmentId,
                                                                                       @PathVariable("drugId") @ApiParam("Drug Id") int drugId,
                                                                                       @PathVariable("batchId") @ApiParam("Drug Id") int batchId) {
        return MedicResponse.okStatus(warehouseService.findStatisticalDrugDetailForAdmin(establishmentId, drugId, batchId));
    }

    /**
     * Get establishment of account, if current account is admin then establishment can be null.
     *
     * @param account current login account
     * @return Establishment
     */
    private Integer getEstablishmentIdOfAccount(Account account, Integer establishmentId) {
        if ((Constants.ROLE_DRUG_STORE_ADMIN.equals(account.getRole().getName()) ||
                Constants.ROLE_DRUG_STORE_STAFF.equals(account.getRole().getName()))) {
            if (null == account.getEstablishment()) {
                throw new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Establishment don't exist");
            }
            establishmentId = account.getEstablishment().getId();
        }
        return establishmentId;
    }

    /**
     * Download import template.
     *
     * @return template file
     * @throws Exception
     */
    @ApiOperation("Download import template (DONE)")
    @GetMapping("/downloads/templates/imports")
    public ResponseEntity<Resource> downloadWarehouseImportTemplate() throws Exception {

        Account currentAccount = baseService.getCurrentAccountLogin();
        ExcelTemplateTitleDTO excelTile = fileStorageService.
                getExcelTemplateTitleDTO(currentAccount.getLanguage(), WareHouseExcelType.IMPORT);
        byte[] excelData = fileStorageService.generateImportTemplate(excelTile);
        String filePath =
                 fileStorageService.
                         generateWareHouseTemplateFilePath("templates", "import-template.xlsx", excelData);

        File file = new File(filePath);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName())
                .body(resource);
    }


    /**
     * Download export template.
     *
     * @return template file
     * @throws Exception
     */
    @ApiOperation("Download export template (DONE)")
    @GetMapping("/downloads/templates/exports")
    public ResponseEntity<Resource> downloadWarehouseExportTemplate() throws Exception {

        Account currentAccount = baseService.getCurrentAccountLogin();
        ExcelTemplateTitleDTO excelTile = fileStorageService.
                getExcelTemplateTitleDTO(currentAccount.getLanguage(), WareHouseExcelType.EXPORT);
        byte[] excelData = fileStorageService.generateExportTemplate(excelTile);
        String filePath =
                fileStorageService.
                        generateWareHouseTemplateFilePath("templates", "export-template.xlsx", excelData);

        File file = new File(filePath);
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName())
                .body(resource);
    }


    /**
     * Upload import data.
     *
     * @param file
     * @return
     * @throws Exception
     */
    @ApiOperation("Upload import file (DONE)")
    @PostMapping("/uploads/imports")
    public MedicResponse<StorageImportDTO> uploadWarehouseImport(@RequestParam("file") MultipartFile file) throws Exception {
        String filepath = fileStorageService.storeFile(file);
        ImportParamDTO importParamDTO = (ImportParamDTO) fileStorageService.readExcelFile(filepath, WareHouseExcelType.IMPORT);
        return createImport(importParamDTO);

    }


    /**
     * Upload export data.
     *
     * @param file
     * @return storage export
     * @throws Exception
     */
    @ApiOperation("Upload export file (DONE)")
    @PostMapping("/uploads/exports")
    public MedicResponse<StorageExportDTO> uploadWarehouseExport(@RequestParam("file") MultipartFile file) throws Exception {
        String filepath = fileStorageService.storeFile(file);
        ExportParamDTO exportParamDTO = (ExportParamDTO) fileStorageService.readExcelFile(filepath, WareHouseExcelType.EXPORT);
        storageExportService.setDrugImportDetailForExcelExport(exportParamDTO);
        return createExport(exportParamDTO);
    }
}
