package com.mmedic.rest.drugstorage.service;

import com.mmedic.entity.*;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.enums.ImportType;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.drug.repo.DrugRepository;
import com.mmedic.rest.drug.repo.DrugUnitRepository;
import com.mmedic.rest.drugstorage.dto.DrugInfoImportDTO;
import com.mmedic.rest.drugstorage.dto.ImportParamDTO;
import com.mmedic.rest.drugstorage.dto.StorageImportDTO;
import com.mmedic.rest.drugstorage.dto.StorageImportDetailDTO;
import com.mmedic.rest.drugstorage.mapper.DrugImportDetailMapper;
import com.mmedic.rest.drugstorage.repo.DrugImportDetailRepository;
import com.mmedic.rest.drugstorage.repo.StorageExportRepository;
import com.mmedic.rest.drugstorage.repo.StorageImportRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.spring.errors.MedicException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Storage import service.
 */
@RequiredArgsConstructor
@Service
public class StorageImportService {

    private final StorageImportRepository storageImportRepository;

    private final StorageExportRepository storageExportRepository;

    private final DrugImportDetailRepository drugImportDetailRepository;

    private final EstablishmentRepository establishmentRepository;

    private final DrugRepository drugRepository;

    /**
     * Get list import.
     *
     * @param establishmentId establishment id of current login account
     * @param pageable        Pagination
     * @return list import
     */
    public Page<StorageImportDTO> getAllImport(Integer establishmentId, Pageable pageable) {
        return storageImportRepository.findAllImportByEstablishment(establishmentId, pageable);
    }

    /**
     * Get detail of import.
     *
     * @param importId the import id
     * @param type     External or Internal import
     * @return detail of import
     */
    public StorageImportDetailDTO getImportDetail(int importId, ImportType type) {
        if (ImportType.EXTERNAL.equals(type)) {
            return getExternalImportDetail(importId);
        } else {
            return getInternalImportDetail(importId);
        }
    }

    /**
     * Gets detail of external import.
     *
     * @param importId the import id
     * @return detail of external import
     */
    private StorageImportDetailDTO getExternalImportDetail(int importId) {
        DrugImport drugImport = storageImportRepository.findById(importId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_IMPORT_NOT_EXIST, "Drug external import don't exist"));

        StorageImportDetailDTO result = new StorageImportDetailDTO();
        result.setType(ImportType.EXTERNAL);
        result.setId(drugImport.getId());
        result.setImportDate(drugImport.getCreateAt());
        if (null != drugImport.getCreateBy()) {
            result.setStaffId(drugImport.getCreateBy().getId());
            result.setStaffName(drugImport.getCreateBy().getName());
        }
        if (null != drugImport.getToEstablishment()) {
            result.setToId(drugImport.getToEstablishment().getId());
            result.setToName(drugImport.getToEstablishment().getName());
        }
        if (!CollectionUtils.isEmpty(drugImport.getDrugImportDetails())) {
            List<DrugInfoImportDTO> drugInfoImportDTOS = new ArrayList<>();
            drugImport.getDrugImportDetails().forEach(drugImportDetail -> {
                DrugInfoImportDTO drugInfoImportDTO = new DrugInfoImportDTO();
                drugInfoImportDTO.setId(drugImportDetail.getId());
                drugInfoImportDTO.setDrugId(drugImportDetail.getDrugUnit().getDrug().getId());
                drugInfoImportDTO.setDrugName(drugImportDetail.getDrugUnit().getDrug().getName());
                drugInfoImportDTO.setUnitId(drugImportDetail.getDrugUnit().getId());
                drugInfoImportDTO.setUnitName(drugImportDetail.getDrugUnit().getName());
                drugInfoImportDTO.setAmount(drugImportDetail.getAmount());
                drugInfoImportDTO.setPrice(drugImportDetail.getAmount() * drugImportDetail.getPricePerUnit());
                drugInfoImportDTO.setExpiration(drugImportDetail.getExpiration().toLocalDate());
                drugInfoImportDTO.setImportId(drugImport.getId());
                drugInfoImportDTOS.add(drugInfoImportDTO);
            });
            result.setDrugInfoImports(drugInfoImportDTOS);
        }
        return result;
    }

    /**
     * Gets detail of internal import.
     *
     * @param importId the import id
     * @return detail of internal import
     */
    private StorageImportDetailDTO getInternalImportDetail(int importId) {
        DrugExport internalExport = storageExportRepository.findInternalExport(importId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_IMPORT_NOT_EXIST, "Internal import don't exist"));
        StorageImportDetailDTO result = new StorageImportDetailDTO();
        result.setType(ImportType.INTERNAL);
        result.setId(internalExport.getId());
        result.setImportDate(internalExport.getCreateAt());
        if (null != internalExport.getDrugInternalExport().getCreateBy()) {
            result.setStaffId(internalExport.getDrugInternalExport().getCreateBy().getId());
            result.setStaffName(internalExport.getDrugInternalExport().getCreateBy().getName());
        }
        if (null != internalExport.getExportFrom()) {
            result.setFromId(internalExport.getExportFrom().getId());
            result.setFromName(internalExport.getExportFrom().getName());
        }
        if (null != internalExport.getDrugInternalExport().getExportTo()) {
            result.setToId(internalExport.getDrugInternalExport().getExportTo().getId());
            result.setToName(internalExport.getDrugInternalExport().getExportTo().getName());
        }
        if (!CollectionUtils.isEmpty(internalExport.getDrugExportDetails())) {
            List<DrugInfoImportDTO> drugInfoImportDTOS = new ArrayList<>();
            internalExport.getDrugExportDetails().forEach(drugExportDetail -> {
                DrugInfoImportDTO drugInfoImportDTO = new DrugInfoImportDTO();
                drugInfoImportDTO.setId(drugExportDetail.getId());
                drugInfoImportDTO.setDrugId(drugExportDetail.getDrugUnit().getDrug().getId());
                drugInfoImportDTO.setDrugName(drugExportDetail.getDrugUnit().getDrug().getName());
                drugInfoImportDTO.setUnitId(drugExportDetail.getDrugUnit().getId());
                drugInfoImportDTO.setUnitName(drugExportDetail.getDrugUnit().getName());
                drugInfoImportDTO.setAmount(drugExportDetail.getAmount());
                drugInfoImportDTO.setPrice(drugExportDetail.getAmount() * drugExportDetail.getPricePerUnit());
                drugInfoImportDTO.setExpiration(drugExportDetail.getDrugImportDetail().getExpiration().toLocalDate());
                drugInfoImportDTO.setImportId(drugExportDetail.getDrugImportDetail().getDrugImport().getId());
                drugInfoImportDTOS.add(drugInfoImportDTO);
            });
            result.setDrugInfoImports(drugInfoImportDTOS);
        }
        return result;
    }

    /**
     * Create new import.
     *
     * @param importParam import param
     * @return returns true if it's create successfully
     */
    @Transactional
    public StorageImportDTO createImport(ImportParamDTO importParam, Account currentAccount) {
        Establishment importTo = establishmentRepository.findByIdAndEstablishmentType(importParam.getToId(), EstablishmentType.DRUG_WAREHOUSE).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_WAREHOUSE_NOT_EXIST, "Drug warehouse don't exist"));
        if (CollectionUtils.isEmpty(importParam.getDrugInfoImports())) {
            throw new MedicException(MedicException.ERROR_DRUG_IMPORT_NOT_HAVE_DRUG, "The import don't has drugs");
        }

        DrugImport drugImport = new DrugImport();
        drugImport.setCreateBy(currentAccount);
        drugImport.setToEstablishment(importTo);
        // Save drug import
        DrugImport drugImportResult = storageImportRepository.save(drugImport);

        // Get list id of drugs
        List<Integer> drugIds = importParam.getDrugInfoImports().stream().map(DrugInfoImportDTO::getDrugId).distinct().collect(Collectors.toList());
        List<Drug> drugs = drugRepository.findAllByIdInAndCanPrescribeTrueAndIsDisabledIsFalse(drugIds);
        Map<Integer, Drug> mapDrugs = drugs.stream().collect(Collectors.toMap(Drug::getId, drug -> drug));

        List<DrugImportDetail> drugImportDetails = new ArrayList<>();
        importParam.getDrugInfoImports().forEach(drugInfoImportDTO -> {
            Drug drug = mapDrugs.get(drugInfoImportDTO.getDrugId());
            if (null != drug) {
                if (null == drugInfoImportDTO.getExpiration() || !drugInfoImportDTO.getExpiration().isAfter(LocalDate.now())) {
                    throw new MedicException(MedicException.ERROR_DRUG_IS_EXPIRATION, "Drug is expiration");
                }
                if (null == drugInfoImportDTO.getAmount() || drugInfoImportDTO.getAmount() <= 0) {
                    throw new MedicException(MedicException.ERROR_DRUG_AMOUNT_IS_LESS_THAN_ZERO, "Drug amount must be greater than 0");
                }
                if (null == drugInfoImportDTO.getPrice() || drugInfoImportDTO.getPrice() <= 0) {
                    throw new MedicException(MedicException.ERROR_DRUG_PRICE_IS_LESS_THAN_ZERO, "Drug price must be greater than 0");
                }
                DrugUnit drugUnit = drug.getDrugUnits().stream().filter(unit -> unit.getId() == drugInfoImportDTO.getUnitId()).
                        findFirst().
                        orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_UNIT_NOT_EXIST, "Drug unit don't exist"));

                DrugImportDetail drugImportDetail = new DrugImportDetail();
                drugImportDetail.setDrugImport(drugImportResult);
                drugImportDetail.setDrugUnit(drugUnit);
                drugImportDetail.setAmount(drugInfoImportDTO.getAmount());
                drugImportDetail.setPricePerUnit(drugInfoImportDTO.getPrice());
                drugImportDetail.setExpiration(LocalDateTime.of(drugInfoImportDTO.getExpiration(), LocalTime.of(0, 0)));
                drugImportDetails.add(drugImportDetail);
            } else {
                throw new MedicException(MedicException.ERROR_DRUG_NOT_EXIST, "Drug don't exist");
            }
        });
        if (!CollectionUtils.isEmpty(drugImportDetails)) {
            drugImportDetailRepository.saveAll(drugImportDetails);
        }
        return new StorageImportDTO() {
            @Override
            public int getId() {
                return drugImportResult.getId();
            }

            @Override
            public String getName() {
                return "Phiếu nhập kho " + drugImportResult.getId();
            }

            @Override
            public ImportType getType() {
                return ImportType.EXTERNAL;
            }

            @Override
            public LocalDateTime getImportDate() {
                return drugImportResult.getCreateAt();
            }
        };
    }
}
