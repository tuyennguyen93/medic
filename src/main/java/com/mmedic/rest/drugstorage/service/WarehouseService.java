package com.mmedic.rest.drugstorage.service;

import com.mmedic.entity.Drug;
import com.mmedic.entity.DrugImport;
import com.mmedic.entity.DrugImportDetail;
import com.mmedic.entity.Establishment;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.rest.drug.dto.DrugDTO;
import com.mmedic.rest.drug.dto.DrugUnitDTO;
import com.mmedic.rest.drug.dto.MedicineBatchDTO;
import com.mmedic.rest.drug.DrugService;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugAdminDTO;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugBatchAdminDTO;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugBatchDTO;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugDTO;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugDetailAdminDTO;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugDetailDTO;
import com.mmedic.rest.drugstorage.dto.WarehouseDTO;
import com.mmedic.rest.drugstorage.mapper.WarehouseMapper;
import com.mmedic.rest.drugstorage.repo.StorageImportRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Warehouse service.
 */
@Service
@RequiredArgsConstructor
public class WarehouseService {

    private final EstablishmentRepository establishmentRepository;

    private final StorageImportRepository storageImportRepository;

    private final DrugService drugService;

    private final WarehouseMapper warehouseMapper;

    /**
     * Create new warehouse.
     *
     * @param warehouse the warehouse data
     * @return warehouse is created
     */
    @Transactional
    public WarehouseDTO createNewWarehouse(WarehouseDTO warehouse) {
        WarehouseDTO result = null;
        if (null != warehouse) {
            Establishment establishment = new Establishment();
            establishment.setName(warehouse.getName());
            establishment.setAddress(warehouse.getAddress());
            establishment.setPhoneNumber(warehouse.getPhoneNumber());
            establishment.setEstablishmentType(EstablishmentType.DRUG_WAREHOUSE);
            establishment.setIsDisabled(false);
            establishment = establishmentRepository.save(establishment);
            result = warehouseMapper.toWarehouseDto(establishment);
        }
        return result;
    }

    /**
     * Get list warehouse.
     *
     * @return list warehouse
     */
    public List<WarehouseDTO> getAllWarehouse(String condition) {
        return warehouseMapper.toListWarehouseDTO(establishmentRepository.findAllWarehouse(Helper.convertNullToEmpty(condition)));
    }

    /**
     * Get warehouse detail.
     *
     * @param warehouseId the warehouse id.
     * @return warehouse detail
     */
    public WarehouseDTO getWarehouseDetail(int warehouseId) {
        Establishment establishment = establishmentRepository.findByIdAndEstablishmentType(warehouseId, EstablishmentType.DRUG_WAREHOUSE).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_WAREHOUSE_NOT_EXIST, "Drug warehouse don't exist"));
        return warehouseMapper.toWarehouseDto(establishment);
    }

    /**
     * Update warehouse detail.
     *
     * @param warehouseId the warehouse id
     * @param warehouse   the data to update
     * @return warehouse is updated
     */
    @Transactional
    public WarehouseDTO updateWarehouseDetail(int warehouseId, WarehouseDTO warehouse) {
        WarehouseDTO result = null;
        if (null != warehouse) {
            Establishment establishment = establishmentRepository.findByIdAndEstablishmentType(warehouseId, EstablishmentType.DRUG_WAREHOUSE).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_WAREHOUSE_NOT_EXIST, "Drug warehouse don't exist"));
            establishment.setName(warehouse.getName());
            establishment.setAddress(warehouse.getAddress());
            establishment.setPhoneNumber(warehouse.getPhoneNumber());
            establishment = establishmentRepository.save(establishment);
            result = warehouseMapper.toWarehouseDto(establishment);
        }
        return result;
    }

    /**
     * Deletes warehouse.
     *
     * @param warehouseId the warehouse to delete
     * @return true if warehouse is deleted successfully
     */
    @Transactional
    public WarehouseDTO deleteWarehouse(int warehouseId) {
        Establishment establishment = establishmentRepository.findByIdAndEstablishmentType(warehouseId, EstablishmentType.DRUG_WAREHOUSE).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_WAREHOUSE_NOT_EXIST, "Drug warehouse don't exist"));
        establishment.setIsDisabled(true);
        establishmentRepository.save(establishment);
        return warehouseMapper.toWarehouseDto(establishment);
    }

    /**
     * Statistical drug in warehouse.
     *
     * @param establishmentId the establishment id
     * @return list drug
     */
    public Page<StatisticalDrugAdminDTO> findStatisticalDrugInWarehouse(Integer establishmentId, Pageable pageable) {
        return storageImportRepository.findStatisticalDrugInWarehouse(establishmentId, pageable);
    }

    /**
     * Statistical medicine batch.
     *
     * @param establishmentId the establishment id
     * @param drugId          the drug id
     * @param pageable        pagination
     * @return list medicine batch
     */
    public Page<StatisticalDrugBatchAdminDTO> findStatisticMedicineBatch(Integer establishmentId, int drugId, Pageable pageable) {
        return storageImportRepository.findStatisticalMedicalBatch(establishmentId, drugId, pageable);
    }

    /**
     * Find statistic drug detail.
     *
     * @param drugId  the drug id.
     * @param batchId the batch id
     * @return drug detail
     */
    public StatisticalDrugDetailAdminDTO findStatisticalDrugDetailForAdmin(int establishmentId, int drugId, int batchId) {
        DrugImport drugImport = storageImportRepository.findById(batchId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_IMPORT_NOT_EXIST));
        Establishment establishment = establishmentRepository.findByIdAndEstablishmentType(establishmentId, EstablishmentType.DRUG_WAREHOUSE).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_WAREHOUSE_NOT_EXIST));

        StatisticalDrugDetailAdminDTO statisticalDrugDetailAdminDTO = new StatisticalDrugDetailAdminDTO();
        statisticalDrugDetailAdminDTO.setBatchId(drugImport.getId());
        statisticalDrugDetailAdminDTO.setImportDate(drugImport.getCreateAt());
        statisticalDrugDetailAdminDTO.setStaffId(drugImport.getCreateBy().getId());
        statisticalDrugDetailAdminDTO.setStaffName(drugImport.getCreateBy().getName());

        if (!CollectionUtils.isEmpty(drugImport.getDrugImportDetails())) {
            DrugImportDetail drugImportDetail = drugImport.getDrugImportDetails().stream().
                    filter(importDetail -> importDetail.getDrugUnit().getDrug().getId() == drugId).
                    findFirst().orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_IMPORT_DETAIL_NOT_EXIST));
            Drug drug = drugImportDetail.getDrugUnit().getDrug();
            statisticalDrugDetailAdminDTO.setDrugId(drug.getId());
            statisticalDrugDetailAdminDTO.setDrugName(drug.getName());
            statisticalDrugDetailAdminDTO.setUnitId(drugImportDetail.getDrugUnit().getId());
            statisticalDrugDetailAdminDTO.setUnitName(drugImportDetail.getDrugUnit().getName());
            statisticalDrugDetailAdminDTO.setExpiration(drugImportDetail.getExpiration().toLocalDate());
            MedicineBatchDTO medicineBatchDTO = drugService.getRemainAmountOfDrugInImport(establishment, drug, drugImportDetail.getDrugUnit(), batchId);
            statisticalDrugDetailAdminDTO.setAmount(medicineBatchDTO.getAmount());
            statisticalDrugDetailAdminDTO.setRemainAmount(medicineBatchDTO.getRemainAmount());
        }

        return statisticalDrugDetailAdminDTO;
    }

    /**
     * Find drug in drug store.
     *
     * @param condition the condition value
     * @return list drug
     */
    public Page<StatisticalDrugDTO> findStatisticDrugInDrugStore(Establishment currentEstablishment, String condition, Pageable pageable) {
        String con = Constants.PERCENT + Helper.convertNullToEmpty(condition) + Constants.PERCENT;
        return storageImportRepository.findStatisticalDrugInDrugStore(currentEstablishment.getId(), con, pageable);
    }

    /**
     * Find statistic drug store.
     *
     * @param drugId the drug id
     * @return list drug detail
     */
    public List<StatisticalDrugDetailDTO> findStatisticDrugDetailInDrugStore(int drugId, Establishment currentEstablishment) {
        List<StatisticalDrugDetailDTO> statisticalDrugDetails = new ArrayList<>();

        // Get drugs same tree
        List<DrugDTO> drugDTOS = drugService.getDrugTree(drugId, false);
        if (!CollectionUtils.isEmpty(drugDTOS)) {
            // For each drug
            drugDTOS.forEach(drugDTO -> {
                if (!CollectionUtils.isEmpty(drugDTO.getUnits())) {
                    List<Integer> drugUnitParentIds = getListDrugUnitParentId(drugDTO.getUnits());
                    if (!CollectionUtils.isEmpty(drugUnitParentIds)) {
                        // For each root unit
                        drugUnitParentIds.forEach(unitParentId -> {
                            // Statistic of drug base on each root unit
                            DrugUnitDTO targetUnit = drugDTO.getUnits().stream().filter(drugUnitDTO -> drugUnitDTO.getId().equals(unitParentId)).findFirst().orElse(null);
                            if (null != targetUnit) {
                                List<MedicineBatchDTO> medicineBatchDTOS = drugService.getListMedicineBatchByDrugId(drugDTO.getId(), targetUnit.getId(), currentEstablishment);
                                if (!CollectionUtils.isEmpty(medicineBatchDTOS)) {
                                    StatisticalDrugDetailDTO statisticalDrugDetail = new StatisticalDrugDetailDTO();
                                    statisticalDrugDetail.setDrugId(drugDTO.getId());
                                    statisticalDrugDetail.setDrugName(drugDTO.getName());
                                    statisticalDrugDetail.setDrugUnitId(targetUnit.getId());
                                    statisticalDrugDetail.setDrugUnitName(targetUnit.getName());
                                    List<StatisticalDrugBatchDTO> batchs = new ArrayList<>();
                                    medicineBatchDTOS.forEach(medicineBatchDTO -> {
                                        StatisticalDrugBatchDTO statisticalDrugBatch = new StatisticalDrugBatchDTO();
                                        statisticalDrugBatch.setAmount(medicineBatchDTO.getAmount());
                                        statisticalDrugBatch.setId(medicineBatchDTO.getId());
                                        statisticalDrugBatch.setRemainAmount(medicineBatchDTO.getRemainAmount());
                                        statisticalDrugBatch.setExpiration(medicineBatchDTO.getExpiration());
                                        batchs.add(statisticalDrugBatch);
                                    });
                                    statisticalDrugDetail.setDrugBatchs(batchs);
                                    statisticalDrugDetails.add(statisticalDrugDetail);
                                }
                            }
                        });
                    }
                }
            });
        }
        return statisticalDrugDetails;
    }

    /**
     * Get list drug unit parent.
     *
     * @param drugUnits list drug unit
     * @return list drug unit parent id;
     */
    private List<Integer> getListDrugUnitParentId(List<DrugUnitDTO> drugUnits) {
        List<Integer> drugUnitParentIds = null;
        if (!CollectionUtils.isEmpty(drugUnits)) {
            drugUnitParentIds = drugUnits.stream().map(this::getRootParentUnitId).
                    distinct()
                    .collect(Collectors.toList());
        }
        return drugUnitParentIds;
    }

    /**
     * Get root parent of unit id.
     *
     * @param drugUnit drug unit
     * @return id of root parent unit
     */
    private int getRootParentUnitId(DrugUnitDTO drugUnit) {
        int parentDrugUnitId = drugUnit.getId();
        if (!StringUtils.isBlank(drugUnit.getPath())) {
            parentDrugUnitId = getPathsOfDrugUnit(drugUnit).get(0);
        }
        return parentDrugUnitId;
    }

    /**
     * Get path of drug unit.
     *
     * @param drugUnit drug unit
     * @return path
     */
    private List<Integer> getPathsOfDrugUnit(DrugUnitDTO drugUnit) {
        String[] p = Helper.convertNullToEmpty(drugUnit.getPath()).split(Constants.DOT_REGEX);
        List<Integer> path = Arrays.stream(p).filter(Helper::isNumeric).map(Integer::valueOf).collect(Collectors.toList());
        path.add(drugUnit.getId());
        return path;
    }

}
