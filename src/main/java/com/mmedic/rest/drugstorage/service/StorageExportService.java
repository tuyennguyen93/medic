package com.mmedic.rest.drugstorage.service;

import com.mmedic.entity.*;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.enums.ExportType;
import com.mmedic.rest.drug.DrugService;
import com.mmedic.rest.drug.repo.DrugRepository;
import com.mmedic.rest.drugstorage.dto.DrugInfoExportDTO;
import com.mmedic.rest.drugstorage.dto.ExportParamDTO;
import com.mmedic.rest.drugstorage.dto.StorageExportDTO;
import com.mmedic.rest.drugstorage.dto.StorageExportDetailDTO;
import com.mmedic.rest.drugstorage.repo.*;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Storage export service.
 */
@RequiredArgsConstructor
@Service
public class StorageExportService {

    private final StorageExportRepository storageExportRepository;

    private final StorageImportRepository storageImportRepository;

    private final DrugExportDetailRepository drugExportDetailRepository;

    private final DrugImportDetailRepository drugImportDetailRepository;

    private final DrugInternalExportRepository drugInternalExportRepository;

    private final EstablishmentRepository establishmentRepository;

    private final DrugRepository drugRepository;

    private final DrugService drugService;

    /**
     * Get all export.
     *
     * @param establishmentId the establishment id
     * @param pageable        pagination
     * @return list export
     */
    public Page<StorageExportDTO> getAllExport(Integer establishmentId, Pageable pageable) {
        return storageExportRepository.findAllExportByEstablishment(establishmentId, pageable);
    }

    /**
     * Get export detail.
     *
     * @param exportId the export id
     * @return export detail
     */
    public StorageExportDetailDTO getExportDetail(int exportId) {
        DrugExport drugExport = storageExportRepository.findById(exportId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_EXPORT_NOT_EXIST, "Drug export don't exist"));
        StorageExportDetailDTO result = new StorageExportDetailDTO();
        result.setId(drugExport.getId());
        result.setExportDate(drugExport.getCreateAt());

        if (null != drugExport.getExportFrom()) {
            result.setFromId(drugExport.getExportFrom().getId());
            result.setFromName(drugExport.getExportFrom().getName());
        }

        // Get internal export detail
        if (null != drugExport.getDrugInternalExport()) {
            result.setType(ExportType.INTERNAL);
            // Set account handle for internal export
            if (null != drugExport.getDrugInternalExport().getCreateBy()) {
                result.setStaffId(drugExport.getDrugInternalExport().getCreateBy().getId());
                result.setStaffName(drugExport.getDrugInternalExport().getCreateBy().getName());
            }
            // Set export to
            if (null != drugExport.getDrugInternalExport().getExportTo()) {
                result.setToId(drugExport.getDrugInternalExport().getExportTo().getId());
                result.setToName(drugExport.getDrugInternalExport().getExportTo().getName());
            }
        } else if (null != drugExport.getDrugOrderExport()) { // Get export detail for order
            result.setType(ExportType.PATIENT);

            PrescriptionRecord prescriptionRecord = drugExport.getDrugOrderExport().getPrescriptionRecord();
            if (null != prescriptionRecord) {
                // Set account handle by for order export
                if (null != prescriptionRecord.getMedicalServiceOrder()) {
                    List<MedicalServiceOrderDetail> serviceOrderDetails = prescriptionRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails();
                    if (!CollectionUtils.isEmpty(serviceOrderDetails)) {
                        MedicalServiceOrderDetail supplyMedicineService = serviceOrderDetails.stream().
                                filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY).
                                findFirst().
                                orElse(null);
                        if (null != supplyMedicineService && null != supplyMedicineService.getHandleBy()) {
                            result.setStaffId(supplyMedicineService.getHandleBy().getId());
                            result.setStaffName(supplyMedicineService.getHandleBy().getName());
                        }
                    }
                }
                // Set medical record id for export to
                if (null != prescriptionRecord.getMedicalRecord()) {
                    result.setMedicalRecordId(prescriptionRecord.getMedicalRecord().getId());
                } else {
                    result.setRetailPrescription(true);
                }
            }
        }

        if (!CollectionUtils.isEmpty(drugExport.getDrugExportDetails())) {
            List<DrugInfoExportDTO> drugInfoImportDTOS = new ArrayList<>();
            drugExport.getDrugExportDetails().forEach(drugExportDetail -> {
                DrugInfoExportDTO drugInfoExportDTO = new DrugInfoExportDTO();
                drugInfoExportDTO.setId(drugExportDetail.getId());
                drugInfoExportDTO.setDrugId(drugExportDetail.getDrugUnit().getDrug().getId());
                drugInfoExportDTO.setDrugName(drugExportDetail.getDrugUnit().getDrug().getName());
                drugInfoExportDTO.setUnitId(drugExportDetail.getDrugUnit().getId());
                drugInfoExportDTO.setUnitName(drugExportDetail.getDrugUnit().getName());
                drugInfoExportDTO.setAmount(drugExportDetail.getAmount());
                drugInfoExportDTO.setPrice(drugExportDetail.getAmount() * drugExportDetail.getPricePerUnit());
                if (null != drugExportDetail.getDrugImportDetail()) {
                    drugInfoExportDTO.setExpiration(drugExportDetail.getDrugImportDetail().getExpiration().toLocalDate());
                    drugInfoExportDTO.setImportId(drugExportDetail.getDrugImportDetail().getDrugImport().getId());
                }
                drugInfoImportDTOS.add(drugInfoExportDTO);
            });
            result.setDrugInfoExports(drugInfoImportDTOS);
        }

        return result;
    }

    /**
     * Set drug import detail Id for data export from excel uploaded.
     *
     * @param excelExport excel data
     */
    public void setDrugImportDetailForExcelExport (ExportParamDTO excelExport) {
        List<DrugInfoExportDTO> drugInfoExports = excelExport.getDrugInfoExports();
        for (DrugInfoExportDTO drugImport : drugInfoExports) {
            storageImportRepository.
                    findDrugImportOfEstablishment(excelExport.getFromId(), drugImport.getImportId()).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DATA_INVALID,
                            String.format("Can not find importId %d of wareHouseID %d .",drugImport.getImportId() , excelExport.getFromId())));

            Integer importDetailId = storageImportRepository.
                    findDrugImportDetailId(drugImport.getImportId(), drugImport.getDrugId()).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DATA_INVALID,
                    String.format("Can not find detail of importId = %d and drugId = %d .",drugImport.getImportId(), drugImport.getDrugId())));

            drugImport.setDrugImportDetailId(importDetailId);
        }

    }

    /**
     * Creates new export.
     *
     * @param exportParam    export param
     * @param currentAccount current account
     * @return new export
     */
    @Transactional
    public StorageExportDTO createNewExport(ExportParamDTO exportParam, Account currentAccount) {
        if (null == exportParam.getFromId()) {
            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Can't find the establishment to export");
        }
        Establishment importFrom = null;
        if (Helper.accountHasAnyRole(currentAccount, Constants.ROLE_SUPER_ADMIN, Constants.ROLE_ADMIN)) {
            importFrom = establishmentRepository.findByIdAndEstablishmentType(exportParam.getFromId(), EstablishmentType.DRUG_WAREHOUSE).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_WAREHOUSE_NOT_EXIST, "Drug warehouse don't exist"));
        } else {
            importFrom = establishmentRepository.findByIdAndEstablishmentType(exportParam.getFromId(), EstablishmentType.DRUG_STORE).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_STORE_NOT_EXIST, "Drug store don't exist"));
        }
        Establishment exportTo = establishmentRepository.findByIdAndEstablishmentType(exportParam.getToId(), EstablishmentType.DRUG_STORE).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_STORE_NOT_EXIST, "Drug store don't exist"));
        if (CollectionUtils.isEmpty(exportParam.getDrugInfoExports())) {
            throw new MedicException(MedicException.ERROR_DRUG_EXPORT_NOT_HAVE_DRUG, "The export don't has drugs");
        }

        // Create drug export
        DrugExport drugExport = new DrugExport();
        drugExport.setExportFrom(importFrom);
        DrugExport drugExportResult = storageExportRepository.save(drugExport);

        // Get list id of drugs
        List<Integer> drugIds = exportParam.getDrugInfoExports().stream().map(DrugInfoExportDTO::getDrugId).distinct().collect(Collectors.toList());
        List<Drug> drugs = drugRepository.findAllByIdInAndCanPrescribeTrueAndIsDisabledIsFalse(drugIds);
        Map<Integer, Drug> mapDrugs = drugs.stream().collect(Collectors.toMap(Drug::getId, drug -> drug));

        // Get list DrugImportDetail
        List<Integer> drugImportDetailIds = exportParam.getDrugInfoExports().stream().map(DrugInfoExportDTO::getDrugImportDetailId).collect(Collectors.toList());
        List<DrugImportDetail> drugImportDetails = drugImportDetailRepository.findAllByIdIn(drugImportDetailIds);
        Map<Integer, DrugImportDetail> mapDrugImport = drugImportDetails.stream().collect(Collectors.toMap(DrugImportDetail::getId, drugImportDetail -> drugImportDetail));

        List<DrugInfoExportDTO> drugInfoExportDTOs = prepareListDrugInfoExport(exportParam.getDrugInfoExports());

        // Verify amount of drug
        if (!drugService.verifyAmountDrugOfMedicineBatch(drugInfoExportDTOs, importFrom)) {
            throw new MedicException(MedicException.ERROR_NUMBER_OF_AMOUNT_NOT_ENOUGH);
        }

        List<DrugExportDetail> drugExportDetails = new ArrayList<>();
        drugInfoExportDTOs.forEach(drugInfoExportDTO -> {
            Drug drug = mapDrugs.get(drugInfoExportDTO.getDrugId());
            if (null != drug) {
                if (null == drugInfoExportDTO.getAmount() || drugInfoExportDTO.getAmount() <= 0) {
                    throw new MedicException(MedicException.ERROR_DRUG_AMOUNT_IS_LESS_THAN_ZERO, "Drug amount must be greater than 0");
                }
                DrugUnit drugUnit = drug.getDrugUnits().stream().filter(unit -> unit.getId() == drugInfoExportDTO.getUnitId()).
                        findFirst().
                        orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_UNIT_NOT_EXIST, "Drug unit don't exist"));
                DrugImportDetail drugImportDetail = mapDrugImport.get(drugInfoExportDTO.getDrugImportDetailId());
                if (null != drugImportDetail) {
                    DrugExportDetail drugExportDetail = new DrugExportDetail();
                    drugExportDetail.setDrugExport(drugExportResult);
                    drugExportDetail.setDrugUnit(drugUnit);
                    drugExportDetail.setAmount(drugInfoExportDTO.getAmount());
                    drugExportDetail.setDrugImportDetail(drugImportDetail);
                    drugExportDetail.setPricePerUnit(getPricePerUnit(drugUnit));
                    drugExportDetails.add(drugExportDetail);
                } else {
                    throw new MedicException(MedicException.ERROR_DRUG_IMPORT_DETAIL_NOT_EXIST);
                }
            } else {
                throw new MedicException(MedicException.ERROR_DRUG_NOT_EXIST, "Drug don't exist");
            }
        });
        if (!CollectionUtils.isEmpty(drugExportDetails)) {
            drugExportDetailRepository.saveAll(drugExportDetails);
        }

        // Create internal export
        DrugInternalExport drugInternalExport = new DrugInternalExport();
        drugInternalExport.setDrugExport(drugExportResult);
        drugInternalExport.setExportTo(exportTo);
        drugInternalExport.setCreateBy(currentAccount);
        drugInternalExportRepository.save(drugInternalExport);
        return new StorageExportDTO() {
            @Override
            public int getId() {
                return drugExportResult.getId();
            }

            @Override
            public String getName() {
                return "Phiếu xuất kho " + drugExportResult.getId();
            }

            @Override
            public ExportType getType() {
                return ExportType.INTERNAL;
            }

            @Override
            public Integer getMedicalRecordId() {
                return null;
            }

            @Override
            public LocalDateTime getExportDate() {
                return drugExportResult.getCreateAt();
            }
        };
    }

    /**
     * Get Price per unit.
     *
     * @param drugUnit the drug unit
     * @return price per unit
     */
    private int getPricePerUnit(DrugUnit drugUnit) {
        int pricePerUnit = 0;
        if (null != drugUnit && !CollectionUtils.isEmpty(drugUnit.getDrugUnitPriceHistories())) {
            pricePerUnit = drugUnit.getDrugUnitPriceHistories().stream().
                    filter(priceHistory -> !priceHistory.getEffectiveAt().isAfter(LocalDateTime.now())).
                    max((price1, price2) -> price1.getEffectiveAt().compareTo(price2.getEffectiveAt())).
                    map(DrugUnitPriceHistory::getPricePerUnit).orElse(0);
        }
        return pricePerUnit;
    }

    /**
     * Prepare list drug info export.
     *
     * @param drugInfoExportDTOS list drug info export
     * @return list drug info export
     */
    private List<DrugInfoExportDTO> prepareListDrugInfoExport(List<DrugInfoExportDTO> drugInfoExportDTOS) {
        Map<String, DrugInfoExportDTO> mapDrugInfoExport = new HashMap<>();
        if (!CollectionUtils.isEmpty(drugInfoExportDTOS)) {
            drugInfoExportDTOS.forEach(drugInfoExportDTO -> {
                String key = drugInfoExportDTO.getDrugId() + Constants.UNDERSCORE + drugInfoExportDTO.getUnitId() + Constants.UNDERSCORE +
                        drugInfoExportDTO.getImportId() + Constants.UNDERSCORE + drugInfoExportDTO.getDrugImportDetailId();
                DrugInfoExportDTO drugInfo = mapDrugInfoExport.get(key);
                if (null == drugInfo) {
                    drugInfo = drugInfoExportDTO;
                } else {
                    drugInfo.setAmount(drugInfo.getAmount() + drugInfoExportDTO.getAmount());
                }
                mapDrugInfoExport.put(key, drugInfo);
            });
        }
        return new ArrayList<>(mapDrugInfoExport.values());
    }
}
