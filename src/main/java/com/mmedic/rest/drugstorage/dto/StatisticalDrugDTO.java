package com.mmedic.rest.drugstorage.dto;

public interface StatisticalDrugDTO {

    int getDrugId();

    String getDrugName();

    Boolean getCanPrescribe();
}
