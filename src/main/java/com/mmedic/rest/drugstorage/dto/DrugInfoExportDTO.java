package com.mmedic.rest.drugstorage.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class DrugInfoExportDTO {

    private Integer id;

    private Integer drugId;

    private String drugName;

    private Integer amount;

    private Integer unitId;

    private String unitName;

    private Integer price;

    private LocalDate expiration;

    private int importId;

    private int drugImportDetailId;
}
