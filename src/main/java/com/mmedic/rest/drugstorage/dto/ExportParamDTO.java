package com.mmedic.rest.drugstorage.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ExportParamDTO {

    private Integer fromId;

    private int toId;

    private List<DrugInfoExportDTO> drugInfoExports;
}
