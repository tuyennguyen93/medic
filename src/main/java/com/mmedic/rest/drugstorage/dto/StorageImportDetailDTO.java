package com.mmedic.rest.drugstorage.dto;

import com.mmedic.enums.ImportType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StorageImportDetailDTO {

    private int id;

    private LocalDateTime importDate;

    private int staffId;

    private String staffName;

    private Integer fromId;

    private String fromName;

    private Integer toId;

    private String toName;

    private ImportType type;

    private List<DrugInfoImportDTO> drugInfoImports;

}
