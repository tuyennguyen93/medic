package com.mmedic.rest.drugstorage.dto;

public interface StatisticalDrugAdminDTO {

    int getDrugId();

    String getDrugName();

    int getNumberOfBatch();
}
