package com.mmedic.rest.drugstorage.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class WarehouseDTO {

    private int id;

    private String name;

    private String address;

    private String phoneNumber;
}
