package com.mmedic.rest.drugstorage.dto;

import com.mmedic.rest.account.dto.AccountNameDTO;
import com.mmedic.rest.establishment.dto.EstablishmentShortInfoDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DrugInfoDTO {

    private Integer id;

    private AccountNameDTO createBy;

    private EstablishmentShortInfoDTO toEstablishment;

    private EstablishmentShortInfoDTO fromEstablishment;

    private LocalDateTime createAt;

    private String nameDrug;

    private Integer amount;

    private Integer price;

    private LocalDateTime expiration;
}
