package com.mmedic.rest.drugstorage.dto;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class DrugInfoImportDTO {

    private Integer id;

    private Integer drugId;

    private String drugName;

    private Integer amount;

    private Integer unitId;

    private String  unitName;

    private Integer importId;

    private Integer price;

    private LocalDate expiration;
}
