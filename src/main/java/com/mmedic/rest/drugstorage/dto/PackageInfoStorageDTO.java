package com.mmedic.rest.drugstorage.dto;

import com.mmedic.rest.account.dto.AccountNameDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PackageInfoStorageDTO {

    private Integer id;

    private String drugInfo;

    private LocalDateTime importDate;

    private AccountNameDTO importAccount;

    private String packageName;

    private String drugName;

    private Integer amount;

    private Integer importPrice;

    private Integer exportPrice;

    private LocalDateTime expiration;
}
