package com.mmedic.rest.drugstorage.dto;

import com.mmedic.enums.ImportType;

import java.time.LocalDateTime;

public interface StorageImportDTO {

    int getId();

    String getName();

    ImportType getType();

    LocalDateTime getImportDate();

}
