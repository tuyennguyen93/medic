package com.mmedic.rest.drugstorage.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DrugInfoStorageDTO {

    private Integer id;

    private String drugName;

    private Integer packageNumber;
}
