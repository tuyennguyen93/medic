package com.mmedic.rest.drugstorage.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StatisticalDrugDetailDTO {

    private int drugId;

    private String drugName;

    private int drugUnitId;

    private String drugUnitName;

    List<StatisticalDrugBatchDTO> drugBatchs;
}
