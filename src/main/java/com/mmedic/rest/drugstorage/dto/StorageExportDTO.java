package com.mmedic.rest.drugstorage.dto;

import com.mmedic.enums.ExportType;
import com.mmedic.enums.ImportType;

import java.time.LocalDateTime;

public interface StorageExportDTO {

    int getId();

    String getName();

    ExportType getType();

    Integer getMedicalRecordId();

    LocalDateTime getExportDate();
}
