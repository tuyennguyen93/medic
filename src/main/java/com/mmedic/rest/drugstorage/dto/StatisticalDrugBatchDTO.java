package com.mmedic.rest.drugstorage.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StatisticalDrugBatchDTO {

    private int id;

    private float amount;

    private float remainAmount;

    private LocalDate expiration;
}
