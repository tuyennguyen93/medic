package com.mmedic.rest.drugstorage.dto;

import com.mmedic.enums.ExportType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StorageExportDetailDTO {

    private int id;

    private LocalDateTime exportDate;

    private int staffId;

    private String staffName;

    private Integer fromId;

    private String fromName;

    private Integer toId;

    private String toName;

    private Integer medicalRecordId;

    private Boolean retailPrescription;

    private ExportType type;

    private List<DrugInfoExportDTO> drugInfoExports;
}
