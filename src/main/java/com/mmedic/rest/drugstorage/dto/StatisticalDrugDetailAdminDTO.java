package com.mmedic.rest.drugstorage.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class StatisticalDrugDetailAdminDTO {

    private LocalDateTime importDate;

    private int staffId;

    private String staffName;

    private int batchId;

    private int drugId;

    private String drugName;

    private int unitId;

    private String unitName;

    private LocalDate expiration;

    private float amount;

    private float remainAmount;
}
