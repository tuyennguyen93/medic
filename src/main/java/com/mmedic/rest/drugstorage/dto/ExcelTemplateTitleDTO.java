package com.mmedic.rest.drugstorage.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ExcelTemplateTitleDTO {
    private String  sheetName;
    private String  title;
    private String  toName;
    private String  toId;
    private String  fromName;
    private String  fromId;
    private String  detail;
    private String  no;
    private String  drugId;
    private String  drugName;
    private String  packageId;
    private String  drugUnitId;
    private String  drugUnitName;
    private String  amount;
    private String  price;
    private String  expiration;
}
