package com.mmedic.rest.drugstorage.dto;

public interface StatisticalDrugBatchAdminDTO {

    int getBatchId();
}
