package com.mmedic.rest.drugstorage.repo;

import com.mmedic.entity.DrugExportDetail;
import com.mmedic.entity.Establishment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DrugExportDetailRepository extends JpaRepository<DrugExportDetail, Integer> {

//    @Query("SELECT new com.mmedic.rest.drugstorage.dto.DrugInfoExportDTO(detail.id, " +
//            "detail.code, " +
//            "drug.id AS drugId, " +
//            "drug.name AS drugName, " +
//            "detail.amount, " +
//            "unit.id AS unitId, " +
//            "unit.name AS unitName, " +
//            "detail.price, " +
//            "importDetail.expiration ) " +
//            "FROM DrugExportDetail detail " +
//            "INNER JOIN detail.drugExport export " +
//            "LEFT JOIN detail.drugImportDetail importDetail " +
//            "LEFT JOIN detail.drugUnit unit " +
//            "LEFT JOIN unit.drug drug "  +
//            "WHERE export.id = :exportId ")
//    List<DrugInfoExportDTO> findDrugInfoExport (@Param("exportId") Integer id);

    /**
     * Find all drug export detail by import detail id.
     *
     * @param drugImportDetailIds the list import detail id
     * @return list export detail
     */
//    @Query("SELECT ded FROM DrugExportDetail ded " +
//            "LEFT JOIN ded.drugExport de " +
//            "WHERE ded.drugImportDetail.id IN (:drugImportDetailIds)" +
//            "AND de.exportFrom = :establishment")
    List<DrugExportDetail> findAllByDrugImportDetail_IdInAndDrugExport_ExportFrom(List<Integer> drugImportDetailIds,
                                                                                  Establishment establishment);

    // Copy to establishment repository
    @Query("SELECT ded FROM DrugExportDetail ded " +
            "LEFT JOIN ded.drugExport de " +
            "LEFT JOIN de.drugInternalExport die " +
            "LEFT JOIN ded.drugUnit du " +
            "LEFT JOIN du.drug dr " +
            "WHERE dr.id = :drugId " +
            "AND dr.isDisabled = 0 " +
            "AND die.exportTo = :establishment ")
    List<DrugExportDetail> findInternalExportByDrugIdAndEstablishment(@Param("drugId") int drugId,
                                                                      @Param("establishment") Establishment establishment);

    Integer countByDrugUnit_Id(Integer drugId);
}
