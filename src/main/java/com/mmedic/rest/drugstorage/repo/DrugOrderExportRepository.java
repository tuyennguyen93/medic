package com.mmedic.rest.drugstorage.repo;

import com.mmedic.entity.DrugOrderExport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrugOrderExportRepository extends JpaRepository<DrugOrderExport, Integer> {
}
