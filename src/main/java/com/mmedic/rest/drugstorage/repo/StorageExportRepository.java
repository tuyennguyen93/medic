package com.mmedic.rest.drugstorage.repo;

import com.mmedic.entity.DrugExport;
import com.mmedic.rest.drugstorage.dto.StorageExportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StorageExportRepository extends JpaRepository<DrugExport, Integer> {

    @Query("SELECT de FROM DrugExport de " +
            "LEFT JOIN de.drugInternalExport die " +
            "WHERE de.id = :exportId " +
            "AND die IS NOT NULL")
    Optional<DrugExport> findInternalExport(@Param("exportId") int exportId);

    @Query(nativeQuery = true,
            value = "SELECT de.id                                                                                   AS id, " +
                    "       CONCAT('Phiếu xuất kho ', de.id)                                                        AS name, " +
                    "       CASE WHEN die.id IS NOT NULL THEN 'INTERNAL' WHEN doe.id IS NOT NULL THEN 'PATIENT' END AS type, " +
                    "       pr.medical_record_id                                                                    AS medicalRecordId, " +
                    "       de.create_at                                                                            AS exportDate " +
                    "FROM drug_export de " +
                    "         LEFT JOIN establishment e ON de.export_from = e.id " +
                    "         LEFT JOIN drug_internal_export die on de.id = die.export_id " +
                    "         LEFT JOIN drug_order_export doe on de.id = doe.drug_export_id " +
                    "         LEFT JOIN prescription_record pr on doe.prescription_record_id = pr.id " +
                    "WHERE (:establishmentId IS NULL AND e.establishment_type = 'DRUG_WAREHOUSE') OR de.export_from = :establishmentId " +
                    "ORDER BY de.create_at DESC",
            countQuery = "SELECT COUNT(de.id) " +
                    "FROM drug_export de " +
                    "         LEFT JOIN establishment e ON de.export_from = e.id " +
                    "         LEFT JOIN drug_internal_export die on de.id = die.export_id " +
                    "         LEFT JOIN drug_order_export doe on de.id = doe.drug_export_id " +
                    "         LEFT JOIN prescription_record pr on doe.prescription_record_id = pr.id " +
                    "WHERE (:establishmentId IS NULL AND e.establishment_type = 'DRUG_WAREHOUSE') OR de.export_from = :establishmentId")
    Page<StorageExportDTO> findAllExportByEstablishment(@Param("establishmentId") Integer establishmentId, Pageable pageable);
}
