package com.mmedic.rest.drugstorage.repo;

import com.mmedic.entity.DrugImportDetail;
import com.mmedic.entity.Establishment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DrugImportDetailRepository extends JpaRepository<DrugImportDetail, Integer> {

//    @Query("SELECT new com.mmedic.rest.drugstorage.dto.DrugInfoImportDTO(detail.id, " +
//            "drug.id AS drugId, " +
//            "drug.name AS drugName, " +
//            "detail.amount, " +
//            "unit.id AS unitId, " +
//            "unit.name AS unitName, " +
//            "detail.pricePerUnit AS price , " +
//            "detail.expiration ) " +
//            "FROM DrugImportDetail detail " +
//            "INNER JOIN detail.drugImport import " +
//            "INNER JOIN import.createBy account " +
//            "INNER JOIN import.toEstablishment toEstablish " +
//            "LEFT JOIN detail.drugUnit unit " +
//            "LEFT JOIN unit.drug drug " +
//            "WHERE import.id = :importId AND toEstablish.id = :estId")
//    List<DrugInfoImportDTO> findDrugInfoImport(
//            @Param("estId") Integer estId,
//            @Param("importId") Integer id);

    /**
     * Find all medicine batch of drug.
     *
     * @param drugId the drug id
     * @return list medicine batch of drug
     */
    @Query("SELECT did " +
            "FROM DrugImportDetail did " +
            "LEFT JOIN did.drugImport di " +
            "LEFT JOIN did.drugUnit du " +
            "LEFT JOIN du.drug dr " +
            "WHERE dr.id = :drugId " +
            "AND dr.isDisabled = 0 " +
            "AND di.toEstablishment = :establishment " +
            "AND did.expiration >= CURRENT_DATE")
    List<DrugImportDetail> findAllMedicineBatchByDrugIdAndEstablishment(@Param("drugId") int drugId, @Param("establishment") Establishment establishment);

    /**
     * Find all medicine batch by drug id and import id.
     *
     * @param drugId   the drug id
     * @param importId the import id
     * @return list medicine batch
     */
    @Query("SELECT did " +
            "FROM DrugImportDetail did " +
            "LEFT JOIN did.drugImport di " +
            "LEFT JOIN did.drugUnit du " +
            "LEFT JOIN du.drug dr " +
            "WHERE dr.id = :drugId " +
            "AND dr.isDisabled = 0 " +
            "AND di.id = :importId " +
            "AND did.expiration >= CURRENT_DATE")
    List<DrugImportDetail> findAllMedicineBatchByDrugIdAndImportId(@Param("drugId") int drugId, @Param("importId") int importId);

    /**
     * Get list drug import details
     *
     * @param importDetailIds list import detail ids
     * @return list drug import detail
     */
    List<DrugImportDetail> findAllByIdIn(List<Integer> importDetailIds);

    Integer countByDrugUnit_Id(Integer drugId);
}
