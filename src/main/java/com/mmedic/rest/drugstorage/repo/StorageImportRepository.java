package com.mmedic.rest.drugstorage.repo;

import com.mmedic.entity.DrugImport;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugAdminDTO;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugBatchAdminDTO;
import com.mmedic.rest.drugstorage.dto.StatisticalDrugDTO;
import com.mmedic.rest.drugstorage.dto.StorageImportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface StorageImportRepository extends JpaRepository<DrugImport, Integer> {

    @Query("SELECT import " +
            "FROM DrugImport import " +
            "INNER JOIN import.createBy account " +
            "INNER JOIN import.toEstablishment toEstablish " +
            "WHERE  " +
            " toEstablish.id = :estId " +
            "AND import.id = :importId ")
    Optional<DrugImport> findDrugImportOfEstablishment(
            @Param("estId") Integer estId,
            @Param("importId") Integer id);


    /**
     * Find drug import detail id.
     *
     * Each of drugs only have and drug import detail with any drug unit.
     *
     * @param importId import id
     * @param drugId drug id
     * @return import detail id
     */
    @Query(nativeQuery = true, value = "" +
            "SELECT drug_import_detail.id  " +
            "FROM drug_import_detail Inner join drug_import di on drug_import_detail.drug_import_id = di.id " +
            "inner join drug_unit du on drug_import_detail.drug_unit_id = du.id " +
            "inner join drug d on du.drug_id = d.id " +
            "WHERE di.id = :importId AND d.id = :drugId ")
    Optional<Integer> findDrugImportDetailId(
            @Param("importId") Integer importId,
            @Param("drugId") Integer drugId);

    /**
     * Gets list base info of import.
     *
     * @param establishmentId establishment id
     * @return List import
     */
    @Query(nativeQuery = true,
            value = "SELECT data.id         AS id, " +
                    "       data.name       AS name, " +
                    "       data.type       AS type, " +
                    "       data.importDate AS importDate " +
                    "FROM ( " +
                    "         SELECT di.id                            AS id, " +
                    "                CONCAT('Phiếu nhập kho ', di.id) AS name, " +
                    "                'EXTERNAL'                       AS type, " +
                    "                di.create_at                     AS importDate " +
                    "         FROM drug_import di " +
                    "                  LEFT JOIN establishment e ON di.to_establishment = e.id" +
                    "         WHERE (:establishmentId IS NULL AND e.establishment_type = 'DRUG_WAREHOUSE') " +
                    "            OR di.to_establishment = :establishmentId " +
                    "         UNION " +
                    "         SELECT de.id                            AS id, " +
                    "                CONCAT('Phiếu nhập kho ', de.id) AS name, " +
                    "                'INTERNAL'                        AS type, " +
                    "                de.create_at                      AS importDate " +
                    "         FROM drug_internal_export die " +
                    "                  LEFT JOIN drug_export de ON die.export_id = de.id " +
                    "                  LEFT JOIN establishment e2 on die.export_to = e2.id" +
                    "         WHERE (:establishmentId IS NULL AND e2.establishment_type = 'DRUG_WAREHOUSE') " +
                    "            OR die.export_to = :establishmentId " +
                    "     ) AS data " +
                    "ORDER BY data.importDate DESC",
            countQuery = "SELECT COUNT(*) " +
                    "FROM ( " +
                    "         SELECT di.id AS id " +
                    "         FROM drug_import di " +
                    "                  LEFT JOIN establishment e ON di.to_establishment = e.id" +
                    "         WHERE (:establishmentId IS NULL AND e.establishment_type = 'DRUG_WAREHOUSE') " +
                    "            OR di.to_establishment = :establishmentId " +
                    "         UNION " +
                    "         SELECT de.id AS id " +
                    "         FROM drug_internal_export die " +
                    "                  LEFT JOIN drug_export de on die.export_id = de.id " +
                    "                  LEFT JOIN establishment e2 on die.export_to = e2.id" +
                    "         WHERE (:establishmentId IS NULL AND e2.establishment_type = 'DRUG_WAREHOUSE') " +
                    "            OR die.export_to = :establishmentId " +
                    "     ) AS data")
    Page<StorageImportDTO> findAllImportByEstablishment(@Param("establishmentId") Integer establishmentId, Pageable pageable);

    /**
     * Find drug in warehouse.
     *
     * @param establishmentId the establishment id
     * @param pageable        pagination
     * @return list drug
     */
    @Query(nativeQuery = true,
            value = "SELECT d.id AS drugId, d.name AS drugName, COUNT(DISTINCT di.id) AS numberOfBatch " +
                    "FROM drug_import di " +
                    "         LEFT JOIN drug_import_detail die ON di.id = die.drug_import_id " +
                    "         LEFT JOIN drug_unit du ON die.drug_unit_id = du.id " +
                    "         LEFT JOIN drug d on du.drug_id = d.id " +
                    "WHERE :establishmentId IS NULL OR di.to_establishment = :establishmentId " +
                    "AND d.is_disabled = 0 " +
                    "GROUP BY d.id, d.name",
            countQuery = "SELECT COUNT(*) " +
                    "FROM ( " +
                    "         SELECT d.id AS drugId, d.name AS drugName, COUNT(DISTINCT di.id) AS numberOfBatch " +
                    "         FROM drug_import di " +
                    "                  LEFT JOIN drug_import_detail die ON di.id = die.drug_import_id " +
                    "                  LEFT JOIN drug_unit du ON die.drug_unit_id = du.id " +
                    "                  LEFT JOIN drug d on du.drug_id = d.id " +
                    "         WHERE di.to_establishment = 10 " +
                    "           AND d.is_disabled = 0 " +
                    "         GROUP BY d.id, d.name " +
                    "     ) AS data")
    Page<StatisticalDrugAdminDTO> findStatisticalDrugInWarehouse(@Param("establishmentId") Integer establishmentId, Pageable pageable);

    /**
     * Get medical batch of warehouse.
     *
     * @param establishmentId the establishment id
     * @param drugId          the drug id
     * @param pageable        pagination
     * @return list medical batch
     */
    @Query(nativeQuery = true,
            value = "SELECT DISTINCT di.id AS batchId " +
                    "FROM drug_import di " +
                    "         LEFT JOIN drug_import_detail die ON di.id = die.drug_import_id " +
                    "         LEFT JOIN drug_unit du ON die.drug_unit_id = du.id " +
                    "         LEFT JOIN drug d on du.drug_id = d.id " +
                    "WHERE (:establishmentId IS NULL OR di.to_establishment = :establishmentId) " +
                    "    AND d.id = :drugId " +
                    "    AND d.is_disabled = 0",
            countQuery = "SELECT COUNT(*) " +
                    "FROM ( " +
                    "         SELECT DISTINCT di.id AS id " +
                    "         FROM drug_import di " +
                    "                  LEFT JOIN drug_import_detail die ON di.id = die.drug_import_id " +
                    "                  LEFT JOIN drug_unit du ON die.drug_unit_id = du.id " +
                    "                  LEFT JOIN drug d on du.drug_id = d.id " +
                    "         WHERE :establishmentId IS NULL OR di.to_establishment = :establishmentId " +
                    "             AND d.id = :drugId " +
                    "             AND d.is_disabled = 0 " +
                    "     ) AS data")
    Page<StatisticalDrugBatchAdminDTO> findStatisticalMedicalBatch(@Param("establishmentId") Integer establishmentId,
                                                                   @Param("drugId") int drugId,
                                                                   Pageable pageable);

    /**
     * Find drug in drug store.
     *
     * @param condition the condition value
     * @param pageable  pagination
     * @return list drug
     */
    @Query(nativeQuery = true,
            value = "SELECT DISTINCT d.id AS drugId, d.name AS drugName, d.can_prescribe AS canPrescribe " +
                    "FROM drug_internal_export die " +
                    "         LEFT JOIN drug_export de ON die.export_id = de.id " +
                    "         LEFT JOIN drug_export_detail ded ON de.id = ded.drug_export_id " +
                    "         LEFT JOIN drug_unit du ON ded.drug_unit_id = du.id " +
                    "         LEFT JOIN drug d ON du.drug_id = d.id " +
                    "WHERE export_to = :establishmentId " +
                    "AND d.name LIKE :condition " +
                    "AND d.is_disabled = 0 " +
                    "AND d.can_prescribe = 1")
    Page<StatisticalDrugDTO> findStatisticalDrugInDrugStore(@Param("establishmentId") int establishmentId, @Param("condition") String condition, Pageable pageable);
}
