package com.mmedic.rest.drugstorage.repo;

import com.mmedic.entity.DrugInternalExport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrugInternalExportRepository extends JpaRepository<DrugInternalExport, Integer> {

}
