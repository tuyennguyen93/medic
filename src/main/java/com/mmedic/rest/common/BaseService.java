package com.mmedic.rest.common;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.common.dto.ImageUploadDTO;
import com.mmedic.rest.common.dto.PreSignedAWS;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.security.AccountPrincipal;
import com.mmedic.spring.errors.MedicException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Service
public class BaseService {

    private final AccountService accountService;

    private final EstablishmentRepository establishmentRepository;

    private final AccountRepository accountRepository;

    @Autowired
    private AWSService awsService;

    public List<PreSignedAWS> uploadImages(List<ImageUploadDTO> imageUploadDTOs) {
        List<PreSignedAWS> preSignedAWSResponse = new ArrayList<PreSignedAWS>();
        imageUploadDTOs.forEach(imageUploadDTO -> {
            String fileName = imageUploadDTO.getFileName() + "_" + new Date().getTime() + "." + imageUploadDTO.getFileType();
            PreSignedAWS preSignedAWS = this.awsService.uploadPresignedUrl(fileName);
            preSignedAWSResponse.add(preSignedAWS);
        });

        return preSignedAWSResponse;
    }

    public Establishment getRawEstablishment(Principal principal) {
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));

        return establishmentRepository.findEstablishmentByAccountId(account.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found establishment of account : " + principal.getName()));

    }


    /**
     * Get authentication.
     *
     * @return Authentication instance
     */
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    /**
     * Get principal log in.
     *
     * @return the principal
     */
    public AccountPrincipal getPrincipal() {
        AccountPrincipal accountPrincipal = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (authentication.getPrincipal() instanceof AccountPrincipal) {
            accountPrincipal = (AccountPrincipal) authentication.getPrincipal();
        }

        return accountPrincipal;
    }

    /**
     * Get current Establishment.
     *
     * @return Establishment
     */
    public Establishment getCurrentEstablishment() {
        AccountPrincipal principal = getPrincipal();
        if (null == principal) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        return establishmentRepository.findEstablishmentByAccountId(principal.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Can't find the establishment of this account"));
    }

    /**
     * Get current account is login.
     *
     * @return current account
     */
    public Account getCurrentAccountLogin() {
        AccountPrincipal principal = getPrincipal();
        if (null == principal) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        return accountRepository.findAccountById(principal.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_NOT_EXIST, "Account don't exist"));
    }
}
