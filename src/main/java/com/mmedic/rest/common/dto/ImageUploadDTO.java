package com.mmedic.rest.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

/**
 * A DTO representing a image information
 *
 */
@Getter
@Setter
@Data
public class ImageUploadDTO {
	@NotEmpty(message = "File name is required")
	@ApiModelProperty(notes = "File name", required = true)
	private String fileName;
	
	@NotEmpty(message = "File type is required")
	@ApiModelProperty(notes = "File type", required = true)
	private String fileType;
}