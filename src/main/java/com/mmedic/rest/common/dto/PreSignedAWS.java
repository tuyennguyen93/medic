package com.mmedic.rest.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;

/**
 * A value object representing a pre-signed url
 *
 */
@Getter
@Setter
public class PreSignedAWS {
	@ApiModelProperty(hidden = true)
	private URL preSignedURL;
	private String url;
}
