package com.mmedic.rest.common;

import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.common.dto.ImageUploadDTO;
import com.mmedic.rest.common.dto.PreSignedAWS;
import com.mmedic.service.firebase.FirebaseNotification;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller to managing common function in application
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/")
@Api(value = "Common API for application")
@RequiredArgsConstructor
public class BaseController {

    private final BaseService baseService;

    /**
     * Upload info to create pre-signed url in s3
     *
     * @param images
     * @return
     */
    @PostMapping("/images/pre-signed")
    @ApiOperation(value = "Upload images with pre-signed URL (DONE)")
    public MedicResponse<List<PreSignedAWS>> uploadImages(
            @ApiParam(value = "List fileName", required = true) @RequestBody List<ImageUploadDTO> images) {
        return MedicResponse.okStatus(this.baseService.uploadImages(images));
    }

}