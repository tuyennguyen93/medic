package com.mmedic.rest.doctor;

import com.mmedic.entity.Establishment;
import com.mmedic.enums.TreatmentType;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.certification.CertificationService;
import com.mmedic.rest.certification.dto.CertificationDTO;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.doctor.dto.*;
import com.mmedic.rest.drug.DrugService;
import com.mmedic.rest.establishment.EstablishmentService;
import com.mmedic.rest.establishment.dto.WorkTimeDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalDetailDoctorDTO;
import com.mmedic.rest.medicalService.dto.MedicalServiceGroupDTO;
import com.mmedic.security.AccountPrincipal;
import com.mmedic.spring.errors.MedicException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Api(value = "Doctor Controller", description = "Doctor, Medical Department, Disease Management")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class DoctorController {

    private final MedicalDepartmentService medicalDepartmentService;

    private final DiseaseService diseaseService;

    private final DoctorService doctorService;

    private final DrugService drugService;

    private final EstablishmentService establishmentService;

    private final CertificationService certificationService;

    private final BaseService baseService;

    @ApiOperation("Search Doctor (DONE)")
    @GetMapping("/doctors")
    public MedicResponse<Page<DoctorSearchResultDTO>> searchDoctors(@RequestParam(required = false) @ApiParam("Search condition") String query,
                                                                    int departmentId,
                                                                    @RequestParam(required = false) @ApiParam("Latitude") Double latitude,
                                                                    @RequestParam(required = false) @ApiParam("Longitude") Double longitude,
                                                                    @PageableDefault(sort = "price") @ApiParam("Pagination") Pageable pageable) {
        // Set value for location
        double locationLatitude = null != latitude ? latitude : 0;
        double locationLongitude = null != longitude ? longitude : 0;
        // Prepare sort column
        Sort.Direction direction = null;
        String property = null;
        Sort sort = pageable.getSort();
        if (sort.isSorted()) {
            if (null != sort.getOrderFor("price")) {
                property = "data.price";
                direction = sort.getOrderFor("price").getDirection();
            } else if (null != sort.getOrderFor("distance")) {
                property = "data.distance";
                direction = sort.getOrderFor("distance").getDirection();
            } else {
                property = "data.price";
                direction = Sort.Direction.ASC;
            }
        }
        return MedicResponse.okStatus(doctorService.findDoctors(query, departmentId, locationLatitude, locationLongitude,
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), direction, property)));
    }

    @ApiOperation("Get doctor work time (DONE)")
    @GetMapping("/doctors/{id}/work-time")
    public MedicResponse<List<WorkTimeDTO>> getDoctorWorkTime(@PathVariable("id") @ApiParam("Doctor Id") int doctorId,
                                                              @RequestParam("startDate") @ApiParam("Start Date") LocalDate startDate,
                                                              @RequestParam("endDate") @ApiParam("End Date") LocalDate endDate) {
        return MedicResponse.okStatus(doctorService.findDoctorsWorkTime(startDate, endDate, doctorId));
    }

    @ApiOperation("Calculate the bill for treatment (DONE)")
    @GetMapping("/doctors/{id}/bill")
    public MedicResponse<List<TreatmentBillDTO>> calculateCost(@PathVariable("id") @ApiParam("Doctor Id") int doctorId,
                                                               @RequestParam(value = "patientId", required = true) @ApiParam("Patient Id") int patientId,
                                                               @RequestParam(value = "treatmentType", required = true) @ApiParam("Treatment Type") TreatmentType treatmentType) {
        return MedicResponse.okStatus(doctorService.calculateCost(doctorId, patientId, treatmentType));
    }

    @ApiOperation("Get certification of doctor (DONE)")
    @GetMapping("/doctors/{id}/certifications")
    public MedicResponse<List<CertificationDTO>> getDoctorCertification(@PathVariable("id") @ApiParam("Doctor Id") int doctorId) {
        return MedicResponse.okStatus(certificationService.getCertificationByAccountId(doctorId));
    }

    @ApiOperation("Create new department (DONE)")
    @PostMapping("/medical-departments")
    public MedicResponse<DepartmentDTO> createDepartment(@RequestBody DepartmentDTO reqDto) {
        DepartmentDTO result = medicalDepartmentService.createDepartment(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update department (DONE)")
    @PutMapping("/medical-departments/{id}")
    public MedicResponse<DepartmentDTO> updateDepartment(@PathVariable(value = "id") Integer id, @RequestBody DepartmentDTO reqDto) {
        DepartmentDTO result = medicalDepartmentService.updateDepartment(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete department (DONE)")
    @DeleteMapping("/medical-departments/{id}")
    public MedicResponse<DepartmentDTO> deleteDepartment(@PathVariable("id") Integer id) {
        DepartmentDTO result = medicalDepartmentService.deleteDepartment(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get list department (DONE)")
    @GetMapping("/medical-departments")
    public MedicResponse<List<DepartmentDTO>> getDepartmentList(@RequestParam(value = "query", required = false)
                                                                @ApiParam("Condition Search") String query) {
        List<DepartmentDTO> result = medicalDepartmentService.findMedicalDepartment(query);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get list disease of department (DONE)")
    @GetMapping("/medical-departments/{departmentId}/diseases")
    public MedicResponse<List<DiseaseDTO>> getDiseasesFollowDepartment(@PathVariable("departmentId") Integer id) {
        List<DiseaseDTO> result = diseaseService.getDiseasesFollowDepartment(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create new disease (DONE)")
    @PostMapping("/diseases")
    public MedicResponse<DiseaseDTO> createDisease(@RequestBody DiseaseDTO reqDto) {
        DiseaseDTO result = diseaseService.createDisease(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update information for disease (DONE)")
    @PutMapping("/diseases/{id}")
    public MedicResponse<DiseaseDTO> updateDiseaseFollowDepartment(@PathVariable(value = "id") Integer diseaseId,
                                                                   @RequestBody DiseaseDTO reqDto) {
        DiseaseDTO result = diseaseService.updateDisease(diseaseId, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete disease (DONE)")
    @DeleteMapping("/diseases/{id}")
    public MedicResponse<DiseaseDTO> deleteDisease(@PathVariable(value = "id") Integer diseaseId) {
        DiseaseDTO result = diseaseService.deleteDisease(diseaseId);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Search disease follow name (DONE)")
    @GetMapping("/diseases")
    public MedicResponse<List<PatientDiseaseHistoryDTO>> searchDiseaseFollowName(
            @RequestParam(value = "query", required = false)
            @ApiParam("Condition Search") String query) {
        List<PatientDiseaseHistoryDTO> result = diseaseService.searchDiseaseFollowName(query);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get all health care service is ordered of doctor (DONE)")
    @GetMapping("/doctors/health-cares/filter")
    public MedicResponse<List<HealthCareServiceDTO>> getDoctorHealthCare(@RequestParam(required = false) Integer serviceId,
                                                                         @RequestParam(required = false) LocalDate date) {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(doctorService.getDoctorHealthCareServiceOrdered(accountPrincipal.getId(), serviceId, date));
        }
        return MedicResponse.okStatus(null);
    }

    @ApiOperation("Get detail health care service is ordered (DONE)")
    @GetMapping("/doctors/health-cares/{id}")
    public MedicResponse<HealthCareServiceDetailDTO> getHealthCareServiceDetail(@PathVariable("id") int medicalRecordId) {
        return MedicResponse.okStatus(doctorService.getHealthCareServiceDetail(medicalRecordId));
    }

    @ApiOperation("Update status for service ordered (DONE)")
    @PutMapping("/doctors/health-cares/{id}/process")
    public MedicResponse<HealthCareServiceDTO> processHealthCareService(@PathVariable("id") int medicalRecordId,
                                                                        @RequestBody @ApiParam("Doctor Message") DoctorPackageStatusParamDTO doctorPackageStatusParam) {
        return MedicResponse.okStatus(doctorService.processHealthCareService(medicalRecordId, doctorPackageStatusParam));
    }

    @ApiOperation("Get doctor health care services (DONE)")
    @GetMapping("/doctors/health-cares/services")
    public MedicResponse<List<MedicalServiceGroupDTO>> getDoctorServices() {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(doctorService.getDoctorHealthCareServices(accountPrincipal.getId()));
        }
        return MedicResponse.okStatus(null);
    }

    @ApiOperation("Get all package of service doctor is registry (DONE)")
    @GetMapping("/doctors/health-cares/services/{id}/packages")
    public MedicResponse<List<DoctorPackageDTO>> getDoctorPackages(@PathVariable(value = "id") int serviceId,
                                                                   @RequestParam(required = false) Boolean all) {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(doctorService.getDoctorOrNursePackages(accountPrincipal.getId(), serviceId, all));
        }
        return MedicResponse.okStatus(null);
    }

    @ApiOperation("Active or Inactive health care service (DONE)")
    @PostMapping("/doctors/health-cares/services/{id}/packages/{packageId}/active")
    public MedicResponse<Boolean> activeHealthCarePackage(@PathVariable("id") @ApiParam("Health Care Service Id") int serviceId,
                                                          @PathVariable("packageId") @ApiParam("Health Care Package Id") int packageId,
                                                          @RequestBody DoctorPackageActiveParamDTO doctorPackageActiveParam) {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(doctorService.activeDoctorOrNursePackage(accountPrincipal.getId(), serviceId, packageId, doctorPackageActiveParam));
        }
        return MedicResponse.okStatus(null);
    }

    @ApiOperation("Update price for health care service (DONE)")
    @PutMapping("/doctors/health-cares/services/{id}/packages/{packageId}/price")
    public MedicResponse<Boolean> updatePriceHealthCarePackage(@PathVariable("id") @ApiParam("Health Care Service Id") int serviceId,
                                                               @PathVariable("packageId") @ApiParam("Health Care Package Id") int packageId,
                                                               @RequestBody DoctorPackagePriceParamDTO doctorPackagePriceParam) {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(doctorService.updatePriceDoctorOrNursePackage(accountPrincipal.getId(), serviceId, packageId, doctorPackagePriceParam));
        }
        return MedicResponse.okStatus(null);
    }


    @ApiOperation("Get all treatment requests today (DONE)")
    @GetMapping("/doctors/treatments/today")
    public MedicResponse<List<TreatmentRequestDTO>> getTreatmentRequestsFollowDoctors(Principal principal) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);
        List<TreatmentRequestDTO> result = doctorService.
                getTreatmentRequestListFollowEstablishmentToday(establishment);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Deny treatment requests by Id (DONE)")
    @DeleteMapping("/doctors/treatments/today/{id}")
    public MedicResponse<List<TreatmentRequestDTO>> denyTreatmentRequestsFollowDoctorsById(Principal principal, @PathVariable(value = "id") int treatmentId) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);
        doctorService.denyTreatmentRequestDetailFollowId(establishment, treatmentId);
        List<TreatmentRequestDTO> result = doctorService.
                getTreatmentRequestListFollowEstablishmentToday(establishment);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get all treatment requests of establishment (DONE)")
    @GetMapping("/doctors/treatments/all")
    public MedicResponse<Page<TreatmentRequestDTO>> getTreatmentRequestByEstablishment(Principal principal,@PageableDefault(sort = "appointmentDate", direction= Sort.Direction.DESC) Pageable pageable) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);
        Page<TreatmentRequestDTO> result = doctorService.
                getTreatmentRequestListFollowEstablishmentAll(pageable, establishment);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get treatment requests by Id (DONE)")
    @GetMapping("/doctors/treatments/details/{id}")
    public MedicResponse<TreatmentRequestDetailDTO> getTreatmentRequestsFollowDoctorsById(Principal principal, @PathVariable(value = "id") int treatmentId) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);

        TreatmentRequestDetailDTO result = doctorService.
                getTreatmentRequestDetailFollowId(establishment, treatmentId);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Push notification time to transport (DONE)")
    @PostMapping("/doctors/treatments/details/{id}/transport-notification")
    public MedicResponse<Boolean> notificationTransportTime(@PathVariable(value = "id") int treatmentId) {
        return MedicResponse.okStatus(doctorService.notificationTreatmentTransportTime(treatmentId, baseService.getCurrentEstablishment()));
    }

    @ApiOperation("Get treatment requests by Id (DONE)")
    @GetMapping("/doctors/treatments/histories/patients/{id}")
    public MedicResponse<Page<TreatmentRequestDTO>> getTreatmentRequestHistoryByPatient(@PageableDefault(sort = "appointmentDate", direction= Sort.Direction.DESC) Pageable pageable,
                                                                                        @PathVariable(value = "id") int patientId,
                                                                                        @RequestParam(value = "departmentId", required = false) Integer medicalDepartmentId ) {
        Page<TreatmentRequestDTO> result = doctorService.
                getTreatmentRequestHistoryByPatient(pageable, patientId, medicalDepartmentId);
        return MedicResponse.okStatus(result);
    }


    @ApiOperation("Update symptom by treatment Id (DONE)")
    @PutMapping("/doctors/treatments/details/{id}/symptoms")
    public MedicResponse<TreatmentRequestDetailDTO> updateSymptoms(Principal principal,
                                                                   @PathVariable(value = "id") int treatmentId,
                                                                   @RequestBody String symptoms) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);
        doctorService.updateSymptomByTreatmentId(establishment, treatmentId, symptoms);
        return getTreatmentRequestsFollowDoctorsById(principal, treatmentId);
    }

    @ApiOperation("Update diagnose by treatment Id (DONE)")
    @PutMapping("/doctors/treatments/details/{id}/diagnoses")
    public MedicResponse<TreatmentRequestDetailDTO> updateDiagnoses(Principal principal,
                                                                    @PathVariable(value = "id") int treatmentId,
                                                                    @RequestBody String diagnoses) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);
        doctorService.updateDiagnoseByTreatmentId(establishment, treatmentId, diagnoses);
        return getTreatmentRequestsFollowDoctorsById(principal, treatmentId);
    }

    @ApiOperation("Update preclinical by treatment Id (DONE)")
    @PutMapping("/doctors/treatments/details/{id}/preclinicals")
    public MedicResponse<TreatmentRequestDetailDTO> updatePreclinicals(Principal principal,
                                                                       @PathVariable(value = "id") int treatmentId,
                                                                       @RequestBody List<PreclinicalDetailDoctorDTO> preclinicalDetails) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);

        doctorService.updatePreclinicalByTreatmentId(establishment, treatmentId, preclinicalDetails);
        return getTreatmentRequestsFollowDoctorsById(principal, treatmentId);
    }

    @ApiOperation("Update prescription by treatment Id (DONE)")
    @PutMapping("/doctors/treatments/details/{id}/prescriptions")
    public MedicResponse<TreatmentRequestDetailDTO> updatePrescription(Principal principal,
                                                                       @PathVariable(value = "id") int treatmentId,
                                                                       @RequestBody PrescriptionCreateDTO prescriptionCreate) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);
        if (prescriptionCreate.getIsConfirmed() != null && prescriptionCreate.getIsConfirmed() == Boolean.TRUE){
            doctorService.confirmPrescriptionByTreatmentId(establishment, treatmentId);
        }
        else {
            drugService.isValidDrugAmountRequest(prescriptionCreate);
            doctorService.
                    updatePrescriptionByTreatmentId(establishment, treatmentId, prescriptionCreate);
        }
        return getTreatmentRequestsFollowDoctorsById(principal, treatmentId);
    }

    @ApiOperation("Delete prescription by treatment Id (DONE)")
    @DeleteMapping("/doctors/treatments/details/{id}/prescriptions")
    public MedicResponse<TreatmentRequestDetailDTO> deletePrescription(Principal principal,
                                                                       @PathVariable(value = "id") int treatmentId) {
        Establishment establishment = establishmentService.getRawEstablishment(principal);
        doctorService.
                deletePrescriptionRecord(establishment, treatmentId);
        return getTreatmentRequestsFollowDoctorsById(principal, treatmentId);
    }

    @ApiOperation("Get doctors and nurse, who are register medical health service (DONE)")
    @GetMapping("/medical-staffs")
    public MedicResponse<Page<MedicalStaffSearchResultDTO>> getMedicalStaff(@RequestParam(required = false) @ApiParam("Search condition") String query,
                                                                            @RequestParam(required = false) Integer packageId,
                                                                            @PageableDefault(sort = "price") @ApiParam("Pagination") Pageable pageable) {
        // Prepare sort column
        Sort.Direction direction = Sort.Direction.ASC;
        String property = "data.price";
        Sort sort = pageable.getSort();
        if (sort.isSorted()) {
            if (null != sort.getOrderFor("price")) {
                direction = sort.getOrderFor("price").getDirection();
            }
        }
        return MedicResponse.okStatus(doctorService.findMedicalStaffs(query, packageId,
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), direction, property)));
    }

    @ApiOperation("Get basic info of doctors and nurse (DONE)")
    @GetMapping("/medical-staffs/basic-info")
    public MedicResponse<Page<MedicalStaffSearchResultDTO>> getBasicInfoOfMedicalStaff(@RequestParam(required = false) @ApiParam("Search condition") String query,
                                                                                       @RequestParam(required = false) @ApiParam("Doctor/Nurse") String staffRole,
                                                                                       @RequestParam(required = false) @ApiParam("Department Id") Integer departmentId,
                                                                                       @ApiParam("Pagination") Pageable pageable) {
        return MedicResponse.okStatus(doctorService.findBasicInfoOfMedicalStaffs(query, staffRole, departmentId,
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize())));
    }


    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}
