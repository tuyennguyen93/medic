package com.mmedic.rest.doctor.mapper;

import com.mmedic.entity.MedicalServiceGroup;
import com.mmedic.rest.medicalService.dto.MedicalServiceGroupDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Health care mapper.
 */
@Mapper(componentModel = "spring")
public interface HealthCareServiceMapper {

    /**
     * Convert list medical service group entity to dto.
     *
     * @param medicalServiceGroups list medical service group entity
     * @return list medical service group dto
     */
    List<MedicalServiceGroupDTO> toListMedicalServiceGroupDTO(List<MedicalServiceGroup> medicalServiceGroups);
}
