package com.mmedic.rest.doctor.mapper;

import com.mmedic.entity.MedicalDepartment;
import com.mmedic.rest.doctor.dto.DepartmentDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Department mapper.
 */
@Mapper(componentModel = "spring")
public interface DepartmentMapper {

    /**
     * Convert department entity to dto.
     *
     * @param md department entity
     * @return department dto
     */
    DepartmentDTO convertToDTO(MedicalDepartment md);

    /**
     * Convert department dto to entity.
     *
     * @param dto department dto
     * @return department entity
     */
    MedicalDepartment convertToEntity(DepartmentDTO dto);

    /**
     * Convert list department entity to list dto
     *
     * @param medicalDepartments list department entity
     * @return list department dto
     */
    List<DepartmentDTO> convertToListDepartmentDTO(List<MedicalDepartment> medicalDepartments);
}
