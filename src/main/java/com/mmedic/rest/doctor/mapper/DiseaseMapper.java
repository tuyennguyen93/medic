package com.mmedic.rest.doctor.mapper;

import com.mmedic.entity.Disease;
import com.mmedic.rest.doctor.dto.DiseaseDTO;
import com.mmedic.rest.doctor.dto.PatientDiseaseHistoryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Disease mapper.
 */
@Mapper(componentModel = "spring")
public interface DiseaseMapper {

    /**
     * Convert disease entity to dto.
     *
     * @param entity disease entity
     * @return disease dto
     */
    @Mapping(target = "departmentId", source = "entity.medicalDepartment.id")
    DiseaseDTO convertToDTO(Disease entity);

    /**
     * Convert disease dto to entity.
     *
     * @param dto disease dto
     * @return disease entity
     */
    Disease convertToEntity(DiseaseDTO dto);

    /**
     * Convert disease entity to patient disease history dto.
     *
     * @param entity disease entity
     * @return patient disease history
     */
    PatientDiseaseHistoryDTO convertToPatientDiseaseHistoryDTO(Disease entity);
}
