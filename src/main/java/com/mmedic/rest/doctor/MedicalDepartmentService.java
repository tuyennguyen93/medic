package com.mmedic.rest.doctor;

import com.mmedic.rest.doctor.dto.DepartmentDTO;

import java.util.List;

/**
 * Medical department service.
 */
public interface MedicalDepartmentService {

    /**
     * Find medical department.
     *
     * @param condition condition search value
     * @return list department dto
     */
    List<DepartmentDTO> findMedicalDepartment(String condition);

    /**
     * Create new department.
     *
     * @param reqDto department data
     * @return department is created
     */
    DepartmentDTO createDepartment(DepartmentDTO reqDto);

    /**
     * Update department.
     *
     * @param id     id of department
     * @param reqDto department data to update
     * @return department is updated
     */
    DepartmentDTO updateDepartment(Integer id, DepartmentDTO reqDto);

    /**
     * Delete department.
     *
     * @param id id of department
     * @return department is deleted
     */
    DepartmentDTO deleteDepartment(Integer id);

}
