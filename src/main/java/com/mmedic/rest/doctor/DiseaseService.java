package com.mmedic.rest.doctor;

import com.mmedic.rest.doctor.dto.DiseaseDTO;
import com.mmedic.rest.doctor.dto.PatientDiseaseHistoryDTO;

import java.util.List;

/**
 * Disease service.
 */
public interface DiseaseService {

    /**
     * Get diseases base on department.
     *
     * @param departmentId department id
     * @return list diseases
     */
    List<DiseaseDTO> getDiseasesFollowDepartment(Integer departmentId);

    /**
     * Create new disease.
     *
     * @param reqDto disease data
     * @return disease is created
     */
    DiseaseDTO createDisease(DiseaseDTO reqDto);

    /**
     * Update disease.
     *
     * @param id     the id of disease
     * @param reqDto the data to update
     * @return disease is updated
     */
    DiseaseDTO updateDisease(Integer id, DiseaseDTO reqDto);

    /**
     * Delete disease.
     *
     * @param id the id of disease
     * @return disease is deleted
     */
    DiseaseDTO deleteDisease(Integer id);

    /**
     * Search disease by name.
     *
     * @param name the disease name
     * @return list diseases
     */
    List<PatientDiseaseHistoryDTO> searchDiseaseFollowName(String name);
}
