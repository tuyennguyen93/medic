package com.mmedic.rest.doctor.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DoctorPackageStatusParamDTO {

    MedicalServiceOrderStatus status;

    String message;
}
