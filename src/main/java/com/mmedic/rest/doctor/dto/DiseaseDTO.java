package com.mmedic.rest.doctor.dto;

import io.swagger.models.auth.In;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DiseaseDTO {

    private Integer id;

    private Integer departmentId;

    private String name;

    private String description;

}
