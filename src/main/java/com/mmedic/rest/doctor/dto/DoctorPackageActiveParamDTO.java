package com.mmedic.rest.doctor.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DoctorPackageActiveParamDTO {

    private Boolean active;

//    private Integer initPrice;

//    private LocalDateTime effectiveDate;

}
