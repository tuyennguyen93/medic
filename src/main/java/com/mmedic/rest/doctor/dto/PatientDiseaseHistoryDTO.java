package com.mmedic.rest.doctor.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PatientDiseaseHistoryDTO {

    private Integer id;

    private String name;

}
