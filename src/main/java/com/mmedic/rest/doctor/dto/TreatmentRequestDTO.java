package com.mmedic.rest.doctor.dto;

import com.mmedic.enums.TreatmentRequestStatus;
import com.mmedic.enums.TreatmentType;
import com.mmedic.rest.account.dto.AccountNameDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class TreatmentRequestDTO {

    @ApiModelProperty("Treatment Request Id")
    private Integer id;

    @ApiModelProperty("Medical Record Id")
    private Integer medicalRecordId;

    @ApiModelProperty("Patient Id")
    private int patientId;

    @ApiModelProperty("Patient Name")
    private String patientName;

    @ApiModelProperty("Patient Age")
    private Integer patientAge;

    @ApiModelProperty("Patient Avartar")
    private String avatarUrl;

    @ApiModelProperty("Treatment Department")
    private String departmentName;

    @ApiModelProperty("Treatment Department Id")
    private Integer departmentId;

    @ApiModelProperty("Appointment Date")
    private String appointmentDate;

    @ApiModelProperty("Process Date")
    private String processDate;

    @ApiModelProperty("Result Date")
    private String resultDate;

    @ApiModelProperty("Sequence Number")
    private Integer sequenceNumber;

    @ApiModelProperty("Treatment Type")
    private TreatmentType treatmentType;

    @ApiModelProperty("Treatment Request Status")
    private TreatmentRequestStatus TreatmentRequestStatus;

    @ApiModelProperty("Handle by")
    private AccountNameDTO handleBy;

    private boolean isNotificationTransport;
}
