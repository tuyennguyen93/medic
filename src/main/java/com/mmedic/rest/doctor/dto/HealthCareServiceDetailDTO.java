package com.mmedic.rest.doctor.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class HealthCareServiceDetailDTO {

    private int id;

    private int patientId;

    private String patientName;

//    private LocalDate dateOfBirth;

    private int serviceId;

    private String serviceName;

    private int packageId;

    private String packageName;

    private LocalDate startDate;

    private MedicalServiceOrderStatus status;

    private PaymentStatus paymentStatus;

    private String patientMessage;

    private String doctorMessage;
}
