package com.mmedic.rest.doctor.dto;

import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDoctorDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PrescriptionCreateDTO {

    private String remind;

    private Boolean isConfirmed;

    private List<PrescriptionRecordDetailDoctorDTO> details;
}

