package com.mmedic.rest.doctor.dto;

import com.mmedic.rest.medicalRecord.dto.PreclinicalDetailDoctorDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionDetailDoctorDTO;
import com.mmedic.rest.patient.dto.PatientDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class TreatmentRequestDetailDTO {

    private TreatmentRequestDTO treatmentRequestInfo;

    private PatientDTO patientInfo;

    private String symptom;

    private String diagnose;

    private String remind;

    private Long prescriptionTimeout;

    private boolean prescriptionIsConfirmed;

    private List<PrescriptionDetailDoctorDTO> prescriptionDetails;

    private List<PreclinicalDetailDoctorDTO> preclinicalDetails;

}
