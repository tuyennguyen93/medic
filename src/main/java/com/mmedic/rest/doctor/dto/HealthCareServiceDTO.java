package com.mmedic.rest.doctor.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;

import java.time.LocalDate;

public interface HealthCareServiceDTO {

    int getId();

    int getPatientId();

    String getPatientName();

    LocalDate getDateOfBirth();

    String getAvatarUrl();

    int getServiceId();

    String getServiceName();

    int getPackageId();

    String getPackageName();

    LocalDate getStartDate();

    MedicalServiceOrderStatus getStatus();

    PaymentStatus getPaymentStatus();

}
