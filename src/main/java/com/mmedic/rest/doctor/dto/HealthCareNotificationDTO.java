package com.mmedic.rest.doctor.dto;

public interface HealthCareNotificationDTO {

    int getMedicalRecordId();

    int getPatientId();

    String getPatientName();

    String getNotificationToken();

    String getLanguage();

    int getStaffId();

    String getStaffName();

    int getPackageId();

    String getPackageName();
}
