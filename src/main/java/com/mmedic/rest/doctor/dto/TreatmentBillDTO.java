package com.mmedic.rest.doctor.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("Bill for treatment")
public class TreatmentBillDTO {

    @ApiModelProperty("Type of Fee")
    private String type;

    @ApiModelProperty("Name")
    private String name;

    @ApiModelProperty("Price")
    private int price;
}
