package com.mmedic.rest.doctor.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Doctor Search Result")
public interface DoctorSearchResultDTO {

    @ApiModelProperty("Doctor Id")
    int getId();

    @ApiModelProperty("Doctor Name")
    String getName();

    @ApiModelProperty("Establishment Address")
    String getAddress();

    @ApiModelProperty("Establishment Phone Number")
    String getPhoneNumber();

    @ApiModelProperty("Avatar URL")
    String getAvatarUrl();

    @ApiModelProperty("Price")
    int getPrice();

    @ApiModelProperty("Available")
    Boolean getAvailable();

    @ApiModelProperty("Rating Average")
    float getRatingAverage();

    @ApiModelProperty("Establishment Id")
    int getEstablishmentId();
}
