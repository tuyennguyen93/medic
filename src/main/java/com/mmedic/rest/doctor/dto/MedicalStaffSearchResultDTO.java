package com.mmedic.rest.doctor.dto;

import io.swagger.annotations.ApiModelProperty;

public interface MedicalStaffSearchResultDTO {

    @ApiModelProperty("Staff Id")
    int getId();

    @ApiModelProperty("Staff Name")
    String getName();

    @ApiModelProperty("Staff Title (Doctor | Nurse)")
    String getTitle();

    @ApiModelProperty("Establishment Address")
    String getAddress();

    @ApiModelProperty("Establishment Phone Number")
    String getPhoneNumber();

    @ApiModelProperty("Avatar URL")
    String getAvatarUrl();

    @ApiModelProperty("Price")
    Integer getPrice();

//    @ApiModelProperty("Active")
//    boolean getActive();

    @ApiModelProperty("Rating Average")
    float getRatingAverage();

}
