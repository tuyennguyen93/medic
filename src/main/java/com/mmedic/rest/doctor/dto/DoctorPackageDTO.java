package com.mmedic.rest.doctor.dto;

public interface DoctorPackageDTO {

    int getId();

    String getName();

    String getDescription();

    Boolean getActive();

    Integer getServiceRegistryId();

    Integer getPrice();
}
