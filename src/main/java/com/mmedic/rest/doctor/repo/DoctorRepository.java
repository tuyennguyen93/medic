package com.mmedic.rest.doctor.repo;

import com.mmedic.entity.Account;
import com.mmedic.entity.MedicalServiceGroup;
import com.mmedic.rest.doctor.dto.DoctorPackageDTO;
import com.mmedic.rest.doctor.dto.DoctorSearchResultDTO;
import com.mmedic.rest.doctor.dto.HealthCareServiceDTO;
import com.mmedic.rest.doctor.dto.MedicalStaffSearchResultDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Account, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT data.id      AS id, " +
                    "       data.name             AS name, " +
                    "       data.establishmentId  AS establishmentId, " +
                    "       data.phone_number     AS phoneNumber, " +
                    "       data.address          AS address, " +
                    "       data.avatar_url       AS avatarUrl, " +
                    "       data.rating_average   AS ratingAverage, " +
                    "       data.price            AS price," +
                    "       data.distance         AS distance " +
                    "FROM (SELECT account.id, " +
                    "             account.name, " +
                    "             establishment.id AS establishmentId, " +
                    "             establishment.phone_number, " +
                    "             establishment.address, " +
                    "             account.avatar_url, " +
                    "             establishment.rating_average, " +
                    "             servicePrice.price, " +
                    "             serviceRegistry.id AS serviceRegistryId, " +
                    "             servicePrice.effective_at, " +
                    "             ( " +
                    "                6371 * ACOS( " +
                    "                   COS(RADIANS(:latitude)) * COS(RADIANS(establishment.latitude)) * COS(RADIANS(establishment.longitude) - RADIANS(:longitude)) + SIN(RADIANS(:latitude)) * SIN(RADIANS(establishment.latitude)) " +
                    "                ) " +
                    "             )  AS distance" +
                    "      FROM account account " +
                    "      LEFT OUTER JOIN role role ON account.role_id = role.id" +
                    "      LEFT OUTER JOIN establishment establishment ON account.establishment_id = establishment.id" +
                    "      LEFT OUTER JOIN medical_department department ON establishment.medical_department_id = department.id AND department.is_deleted = 0" +
                    "      LEFT OUTER JOIN medical_service_registry serviceRegistry ON establishment.id = serviceRegistry.establishment_id AND serviceRegistry.active = 1 " +
                    "      LEFT OUTER JOIN medical_service service ON serviceRegistry.medical_service_id = service.id AND service.is_deleted = 0" +
                    "      LEFT OUTER JOIN medical_service_price_history servicePrice ON serviceRegistry.id = servicePrice.medical_service_registry_id" +
                    "      WHERE (account.name LIKE :condition" +
                    "        OR establishment.address LIKE :condition)" +
                    "        AND account.is_deleted <> 1" +
                    "        AND account.is_actived = 1" +
                    "        AND role.name IN ('DOCTOR_ADMIN', 'DOCTOR_STAFF')" +
                    "        AND department.id = :departmentId" +
                    "        AND service.id = 1" +
                    ") AS data " +
                    "INNER JOIN (SELECT medical_service_registry_id, MAX(effective_at) AS max_eff" +
                    "    FROM medical_service_price_history" +
                    "    WHERE effective_at < CURRENT_TIME" +
                    "    GROUP BY medical_service_registry_id" +
                    ") AS price " +
                    "    ON data.serviceRegistryId = price.medical_service_registry_id" +
                    "    AND data.effective_at = price.max_eff" +
                    "    AND data.price > 0",
            countQuery = "SELECT COUNT(DISTINCT data.id, data.price)" +
                    "FROM (SELECT account.id, " +
                    "             serviceRegistry.id AS serviceRegistryId, " +
                    "             servicePrice.effective_at, " +
                    "             servicePrice.price " +
                    "      FROM account account " +
                    "      LEFT OUTER JOIN role role ON account.role_id = role.id" +
                    "      LEFT OUTER JOIN establishment establishment ON account.establishment_id = establishment.id" +
                    "      LEFT OUTER JOIN medical_department department ON establishment.medical_department_id = department.id AND department.is_deleted = 0" +
                    "      LEFT OUTER JOIN medical_service_registry serviceRegistry ON establishment.id = serviceRegistry.establishment_id AND serviceRegistry.active = 1 " +
                    "      LEFT OUTER JOIN medical_service service ON serviceRegistry.medical_service_id = service.id AND service.is_deleted = 0" +
                    "      LEFT OUTER JOIN medical_service_price_history servicePrice ON serviceRegistry.id = servicePrice.medical_service_registry_id" +
                    "      WHERE (account.name LIKE :condition" +
                    "        OR establishment.address LIKE :condition)" +
                    "        AND account.is_deleted <> 1" +
                    "        AND account.is_actived = 1" +
                    "        AND role.name IN ('DOCTOR_ADMIN', 'DOCTOR_STAFF')" +
                    "        AND department.id = :departmentId" +
                    "        AND service.id = 1" +
                    ") AS data " +
                    "INNER JOIN (SELECT medical_service_registry_id, MAX(effective_at) AS max_eff" +
                    "    FROM medical_service_price_history" +
                    "    WHERE effective_at < CURRENT_TIME" +
                    "    GROUP BY medical_service_registry_id" +
                    ") AS price " +
                    "    ON data.serviceRegistryId = price.medical_service_registry_id" +
                    "    AND data.effective_at = price.max_eff" +
                    "    AND data.price > 0")
    Page<DoctorSearchResultDTO> findDoctors(@Param("condition") String condition, @Param("departmentId") int departmentId, @Param("latitude") double latitude, @Param("longitude") double longitude, Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT servicePrice.price " +
                    "FROM medical_service_registry serviceRegistry " +
                    "INNER JOIN medical_service_price_history servicePrice " +
                    "  ON serviceRegistry.id = servicePrice.medical_service_registry_id " +
                    "WHERE serviceRegistry.establishment_id = :establishmentId " +
                    "  AND serviceRegistry.medical_service_id = 1 " +
                    "  AND (serviceRegistry.id, servicePrice.effective_at) = ( " +
                    "    SELECT medical_service_registry_id, MAX(effective_at) AS max_eff " +
                    "    FROM medical_service_price_history " +
                    "    WHERE effective_at < CURRENT_TIME " +
                    "       AND medical_service_registry_id = serviceRegistry.id " +
                    "    GROUP BY medical_service_registry_id " +
                    ")")
    Integer findTreatmentPrice(@Param("establishmentId") int establishmentId);

    @Query("SELECT mr.id AS id, " +
            "p.id AS patientId, " +
            "p.name AS patientName, " +
            "p.dateOfBirth AS dateOfBirth," +
            "acc.avatarUrl AS avatarUrl," +
            "msg.id AS serviceId, " +
            "msg.name AS serviceName, " +
            "ms.id AS packageId, " +
            "ms.name AS packageName, " +
            "msod.appointmentDate AS startDate, " +
            "msod.status AS status, " +
            "mso.paymentStatus AS paymentStatus " +
            "FROM MedicalRecord mr " +
            "LEFT JOIN mr.patient p " +
            "LEFT JOIN p.account acc " +
            "LEFT JOIN mr.serviceRecord sr " +
            "LEFT JOIN sr.medicalServiceOrder mso " +
            "LEFT JOIN mso.medicalServiceOrderDetails msod " +
            "LEFT JOIN msod.medicalService ms " +
            "LEFT JOIN ms.medicalServiceGroup msg " +
            "WHERE mr.recordType = 'SERVICE' " +
            "AND msod.handleBy.id = :doctorId " +
            "AND (:serviceId IS NULL OR msg.id = :serviceId) " +
            "AND ( " +
            "   msod.status IN ('REQUEST_SENT', 'REQUEST_ACCEPTED') " +
            "   OR ( " +
            "      msod.status IN ('REQUEST_FULFILLED', 'REQUEST_DENIED', 'REQUEST_CANCELLED') AND DATE(msod.resultDate) = DATE(:date) " +
            "   )" +
            ")" +
            "ORDER BY msod.appointmentDate, msod.proccessDate, msod.resultDate")
    List<HealthCareServiceDTO> findDoctorHealthCareServiceOrdered(@Param("doctorId") int doctorId,
                                                                  @Param("serviceId") Integer serviceId,
                                                                  @Param("date") LocalDate date);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT data.id                AS id, " +
                    "       data.name              AS name, " +
                    "       data.description       AS description, " +
                    "       data.active            AS active, " +
                    "       data.serviceRegistryId AS serviceRegistryId, " +
                    "       price.price            AS price " +
                    "FROM ( " +
                    "         SELECT ms.id          AS id, " +
                    "                ms.name        AS name, " +
                    "                ms.description AS description, " +
                    "                msr.active     AS active, " +
                    "                msr.id         AS serviceRegistryId " +
                    "         FROM medical_service ms " +
                    "                  LEFT JOIN medical_service_group msg ON ms.medical_service_group_id = msg.id " +
                    "                  LEFT JOIN medical_service_registry msr ON ms.id = msr.medical_service_id AND msr.establishment_id = :establishmentId " +
                    "                  LEFT JOIN establishment es ON msr.establishment_id = es.id " +
                    "         WHERE ms.is_deleted = 0 " +
                    "           AND msg.id = :serviceId " +
                    "           AND ((:all = 0 AND msr.active = 1) OR (:all = 1)) " +
                    "           AND (msr.establishment_id = :establishmentId OR msr.establishment_id IS NULL) " +
                    "     ) AS data " +
                    "         LEFT JOIN ( " +
                    "    SELECT history.medical_service_registry_id, history.price " +
                    "    FROM medical_service_price_history history " +
                    "             INNER JOIN ( " +
                    "        SELECT medical_service_registry_id, " +
                    "               MAX(effective_at) AS max_eff " +
                    "        FROM medical_service_price_history " +
                    "        WHERE effective_at <= CURRENT_TIME " +
                    "        GROUP BY medical_service_registry_id " +
                    "    ) AS current_price ON history.medical_service_registry_id = current_price.medical_service_registry_id " +
                    "        AND history.effective_at = current_price.max_eff " +
                    ") AS price ON data.serviceRegistryId = price.medical_service_registry_id")
    List<DoctorPackageDTO> findDoctorPackages(@Param("establishmentId") int establishmentId, @Param("serviceId") int serviceId, @Param("all") boolean all);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT data.id             AS id, " +
                    "       data.name           AS name, " +
                    "       data.title          AS title, " +
                    "       data.phone_number   AS phoneNumber, " +
                    "       data.address        AS address, " +
                    "       data.avatar_url     AS avatarUrl, " +
                    "       data.rating_average AS ratingAverage, " +
                    "       data.price          AS price " +
                    "FROM (SELECT account.id, " +
                    "             account.name, " +
                    "             account.title, " +
                    "             establishment.phone_number, " +
                    "             establishment.address, " +
                    "             account.avatar_url, " +
                    "             establishment.rating_average, " +
                    "             servicePrice.price, " +
                    "             serviceRegistry.id AS serviceRegistryId, " +
                    "             servicePrice.effective_at " +
                    "      FROM account account " +
                    "               LEFT OUTER JOIN role role ON account.role_id = role.id " +
                    "               LEFT OUTER JOIN establishment establishment ON account.establishment_id = establishment.id " +
                    "               LEFT OUTER JOIN medical_service_registry serviceRegistry " +
                    "                               ON establishment.id = serviceRegistry.establishment_id AND serviceRegistry.active = 1 " +
                    "               LEFT OUTER JOIN medical_service service " +
                    "                               ON serviceRegistry.medical_service_id = service.id AND service.is_deleted = 0 " +
                    "               LEFT OUTER JOIN medical_service_price_history servicePrice " +
                    "                               ON serviceRegistry.id = servicePrice.medical_service_registry_id " +
                    "      WHERE ( " +
                    "              account.name LIKE :condition " +
                    "              OR establishment.address LIKE :condition " +
                    "          ) " +
                    "        AND account.is_deleted <> 1 " +
                    "        AND account.is_actived = 1 " +
                    "        AND role.name IN ( " +
                    "                          'DOCTOR_ADMIN', 'DOCTOR_STAFF', 'NURSE_ADMIN', 'NURSE_STAFF' " +
                    "          ) " +
                    "        AND service.medical_service_group_id >= 5 " +
                    "        AND (:packageId IS NULL OR service.id = :packageId)) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT medical_service_registry_id, " +
                    "           MAX(effective_at) AS max_eff " +
                    "    FROM medical_service_price_history " +
                    "    WHERE effective_at < CURRENT_TIME " +
                    "    GROUP BY medical_service_registry_id " +
                    ") AS price ON data.serviceRegistryId = price.medical_service_registry_id " +
                    "    AND data.effective_at = price.max_eff ",
            countQuery = "SELECT COUNT(DISTINCT data.id) " +
                    "FROM (SELECT account.id, " +
                    "             serviceRegistry.id AS serviceRegistryId, " +
                    "             servicePrice.effective_at " +
                    "      FROM account account " +
                    "               LEFT OUTER JOIN role role ON account.role_id = role.id " +
                    "               LEFT OUTER JOIN establishment establishment ON account.establishment_id = establishment.id " +
                    "               LEFT OUTER JOIN medical_service_registry serviceRegistry " +
                    "                               ON establishment.id = serviceRegistry.establishment_id AND serviceRegistry.active = 1 " +
                    "               LEFT OUTER JOIN medical_service service " +
                    "                               ON serviceRegistry.medical_service_id = service.id AND service.is_deleted = 0 " +
                    "               LEFT OUTER JOIN medical_service_price_history servicePrice " +
                    "                               ON serviceRegistry.id = servicePrice.medical_service_registry_id " +
                    "      WHERE ( " +
                    "              account.name LIKE :condition " +
                    "              OR establishment.address LIKE :condition " +
                    "          ) " +
                    "        AND account.is_deleted <> 1 " +
                    "        AND account.is_actived = 1 " +
                    "        AND role.name IN ( " +
                    "                          'DOCTOR_ADMIN', 'DOCTOR_STAFF', 'NURSE_ADMIN', 'NURSE_STAFF' " +
                    "          ) " +
                    "        AND service.medical_service_group_id >= 5 " +
                    "        AND (:packageId IS NULL OR service.id = :packageId)) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT medical_service_registry_id, " +
                    "           MAX(effective_at) AS max_eff " +
                    "    FROM medical_service_price_history " +
                    "    WHERE effective_at < CURRENT_TIME " +
                    "    GROUP BY medical_service_registry_id " +
                    ") AS price ON data.serviceRegistryId = price.medical_service_registry_id " +
                    "    AND data.effective_at = price.max_eff ")
    Page<MedicalStaffSearchResultDTO> findAllMedicalStaffByPackageId(@Param("condition") String condition,
                                                                     @Param("packageId") Integer packageId,
                                                                     Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT account.id AS id, " +
                    "                account.name AS name, " +
                    "                account.title AS title, " +
                    "                establishment.phone_number AS phoneNumber, " +
                    "                establishment.address AS address, " +
                    "                account.avatar_url AS avatarUrl, " +
                    "                establishment.rating_average AS ratingAverage " +
                    "FROM account account " +
                    "         LEFT OUTER JOIN role role ON account.role_id = role.id " +
                    "         LEFT OUTER JOIN establishment establishment ON account.establishment_id = establishment.id " +
                    "         LEFT OUTER JOIN medical_department md ON establishment.medical_department_id = md.id" +
                    "         LEFT OUTER JOIN medical_service_registry serviceRegistry " +
                    "                         ON establishment.id = serviceRegistry.establishment_id AND serviceRegistry.active = 1 " +
                    "         LEFT OUTER JOIN medical_service service " +
                    "                         ON serviceRegistry.medical_service_id = service.id AND service.is_deleted = 0 " +
                    "WHERE ( " +
                    "        account.name LIKE :condition " +
                    "        OR establishment.address LIKE :condition " +
                    "    ) " +
                    "  AND account.is_deleted <> 1 " +
                    "  AND account.is_actived = 1 " +
                    "  AND role.name IN ( :roles ) " +
                    "  AND (:departmentId IS NULL OR md.id = :departmentId)",
            countQuery = "SELECT COUNT(DISTINCT account.id) " +
                    "FROM account account " +
                    "         LEFT OUTER JOIN role role ON account.role_id = role.id " +
                    "         LEFT OUTER JOIN establishment establishment ON account.establishment_id = establishment.id " +
                    "         LEFT OUTER JOIN medical_department md ON establishment.medical_department_id = md.id" +
                    "         LEFT OUTER JOIN medical_service_registry serviceRegistry " +
                    "                         ON establishment.id = serviceRegistry.establishment_id AND serviceRegistry.active = 1 " +
                    "         LEFT OUTER JOIN medical_service service " +
                    "                         ON serviceRegistry.medical_service_id = service.id AND service.is_deleted = 0 " +
                    "WHERE ( " +
                    "        account.name LIKE :condition " +
                    "        OR establishment.address LIKE :condition " +
                    "    ) " +
                    "  AND account.is_deleted <> 1 " +
                    "  AND account.is_actived = 1 " +
                    "  AND role.name IN ( :roles )" +
                    "  AND (:departmentId IS NULL OR md.id = :departmentId)")
    Page<MedicalStaffSearchResultDTO> findBasicInfoOfMedicalStaff(@Param("condition") String condition,
                                                                  @Param("roles") List<String> roles,
                                                                  @Param("departmentId") Integer departmentId,
                                                                  Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT servicePrice.price " +
                    "FROM medical_service_registry serviceRegistry " +
                    "         INNER JOIN medical_service_price_history servicePrice " +
                    "                    ON serviceRegistry.id = servicePrice.medical_service_registry_id " +
                    "WHERE serviceRegistry.establishment_id = :establishmentId " +
                    "  AND serviceRegistry.medical_service_id = :packageId " +
                    "  AND (serviceRegistry.id, servicePrice.effective_at) = ( " +
                    "    SELECT medical_service_registry_id, MAX(effective_at) AS max_eff " +
                    "    FROM medical_service_price_history " +
                    "    WHERE effective_at < CURRENT_TIME " +
                    "      AND medical_service_registry_id = serviceRegistry.id " +
                    "    GROUP BY medical_service_registry_id " +
                    ")")
    Integer findMedicalStaffPriceByPackageId(@Param("packageId") int packageId, @Param("establishmentId") int establishmentId);

//    @Query("SELECT ms FROM MedicalService ms " +
//            "WHERE ms.medicalServiceGroup.id = :serviceId " +
//            "AND ms.id = :packageId " +
//            "AND ms.isDeleted = 0")
//    Optional<MedicalService> findHealthCareServiceByServiceIdAndPackageId();

    @Query("SELECT DISTINCT msg FROM MedicalServiceGroup msg " +
            "LEFT JOIN msg.medicalServices ms " +
            "LEFT JOIN ms.medicalServiceRegistries msr " +
            "LEFT JOIN msr.establishment es " +
            "LEFT JOIN es.accounts acc " +
            "WHERE (ms.isDeleted = 0 OR ms.isDeleted IS NULL ) " +
            "AND (msg.isDisabled = 0 OR msg.isDisabled IS NULL )" +
            "AND msr.active = 1 " +
            "AND msg.id >= 5 " +
            "AND acc.id = :doctorId ")
    List<MedicalServiceGroup> getDoctorMedicalServiceGroups(@Param("doctorId") int doctorId);

}
