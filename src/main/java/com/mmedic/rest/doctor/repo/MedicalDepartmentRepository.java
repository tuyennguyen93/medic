package com.mmedic.rest.doctor.repo;

import com.mmedic.entity.MedicalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MedicalDepartmentRepository extends JpaRepository<MedicalDepartment, Integer> {

    List<MedicalDepartment> findMedicalDepartmentsByIsDeletedIsFalse();

    Optional<MedicalDepartment> findByIdAndAndIsDeletedIsFalse(Integer id);

    @Query("SELECT department FROM MedicalDepartment department " +
            "WHERE (department.name LIKE %:condition% " +
            "OR department.description LIKE %:condition%) " +
            "AND department.isDeleted = 0")
    List<MedicalDepartment> findMedicalDepartment(@Param("condition") String condition);

}
