package com.mmedic.rest.doctor.repo;

import com.mmedic.entity.Disease;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DiseaseRepository extends JpaRepository<Disease, Integer> {

    List<Disease> findDiseasesByMedicalDepartment_IdAndIsDeletedIsFalse (Integer departmentId);

    Optional<Disease> findByIdAndIsDeletedIsFalse (Integer id);

    List<Disease> findByNameContains (String name);

}
