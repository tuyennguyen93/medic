package com.mmedic.rest.doctor;

import com.mmedic.entity.Establishment;
import com.mmedic.enums.TreatmentType;
import com.mmedic.rest.doctor.dto.*;
import com.mmedic.rest.establishment.dto.WorkTimeDTO;
import com.mmedic.rest.medicalRecord.dto.PreclinicalDetailDoctorDTO;
import com.mmedic.rest.medicalService.dto.MedicalServiceGroupDTO;
import com.mmedic.spring.errors.MedicException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

/**
 * Doctor service
 */
public interface DoctorService {

    /**
     * Find doctors base on condition (name, address), department id. Sort by distance or price.
     *
     * @param condition    condition to search (name, address)
     * @param departmentId department id
     * @param latitude     latitude
     * @param longitude    longitude
     * @param pageable     pagination
     * @return list doctor
     */
    Page<DoctorSearchResultDTO> findDoctors(String condition, int departmentId, double latitude, double longitude, Pageable pageable);

    /**
     * Find doctor work time.
     *
     * @param startDate start date
     * @param endDate   end date
     * @param doctorId  doctor id
     * @return list work time of doctor
     */
    List<WorkTimeDTO> findDoctorsWorkTime(LocalDate startDate, LocalDate endDate, int doctorId);

    /**
     * Calculate the cost for treatment.
     *
     * @param doctorId      doctor id
     * @param patientId     patient id
     * @param treatmentType treatment type (HOME/CLINIC)
     * @return bill
     */
    List<TreatmentBillDTO> calculateCost(int doctorId, int patientId, TreatmentType treatmentType);

    /**
     * Get all health care service are ordered of doctor/nurse.
     *
     * @param doctorId  doctor/nurse id
     * @param serviceId service id
     * @param date      date
     * @return list health care service
     * @throws MedicException
     */
    List<HealthCareServiceDTO> getDoctorHealthCareServiceOrdered(int doctorId, Integer serviceId, LocalDate date) throws MedicException;

    /**
     * Get detail health care service is ordered.
     *
     * @param medicalRecordId medical record id
     * @return detail health care service.
     * @throws MedicException
     */
    HealthCareServiceDetailDTO getHealthCareServiceDetail(int medicalRecordId) throws MedicException;

    /**
     * Process health care service (Accept/Denied).
     *
     * @param medicalRecordId          medical record id
     * @param doctorPackageStatusParam status and message
     * @return health care service after update status
     * @throws MedicException
     */
    HealthCareServiceDTO processHealthCareService(int medicalRecordId, DoctorPackageStatusParamDTO doctorPackageStatusParam) throws MedicException;

    /**
     * Get packages health care service of doctor or nurse.
     *
     * @param doctorId  doctor/nurse id
     * @param serviceId package id
     * @param all       for true value, include services which isn't registry
     * @return list packages
     * @throws MedicException
     */
    List<DoctorPackageDTO> getDoctorOrNursePackages(int doctorId, int serviceId, Boolean all) throws MedicException;

    /**
     * Active/Deactive package.
     *
     * @param doctorId                 doctor/nurse id
     * @param serviceId                service id
     * @param packageId                package id
     * @param doctorPackageActiveParam active or deactive
     * @return true if it updated successfully
     * @throws MedicException
     */
    boolean activeDoctorOrNursePackage(int doctorId, int serviceId, int packageId, DoctorPackageActiveParamDTO doctorPackageActiveParam) throws MedicException;

    /**
     * Update price for packages.
     *
     * @param doctorId                doctor/nurse id
     * @param serviceId               service id
     * @param packageId               package id
     * @param doctorPackagePriceParam price of package
     * @return true if it updated successfully
     * @throws MedicException
     */
    boolean updatePriceDoctorOrNursePackage(int doctorId, int serviceId, int packageId, DoctorPackagePriceParamDTO doctorPackagePriceParam) throws MedicException;

    /**
     * Find medical staff (doctor/nurse).
     *
     * @param condition condition value (name/address)
     * @param packageId package id
     * @param pageable  pagination
     * @return list staff
     */
    Page<MedicalStaffSearchResultDTO> findMedicalStaffs(String condition, Integer packageId, Pageable pageable);

    /**
     * Find basic info of medical staff (doctor/nurse).
     *
     * @param condition    condition value (name/address)
     * @param staffRole    Doctor/Nurse
     * @param departmentId Department Id
     * @param pageable     pagination
     * @return list basic info of medical staff
     */
    Page<MedicalStaffSearchResultDTO> findBasicInfoOfMedicalStaffs(String condition, String staffRole, Integer departmentId, Pageable pageable);

    /**
     * Get all health care service that doctor/nurse is registry.
     *
     * @param doctorId doctor/nurse id
     * @return list health care service
     */
    List<MedicalServiceGroupDTO> getDoctorHealthCareServices(int doctorId);

    /**
     * Get all treatment service of establishment.
     *
     * @param est current establishment
     * @return list treatment service
     */
    List<TreatmentRequestDTO> getTreatmentRequestListFollowEstablishmentToday(Establishment est);

    /**
     * Get treatment request follow establisment all day.
     *
     * @param pageable pageable
     * @param est      current establishment
     * @return treatment service list
     */
    Page<TreatmentRequestDTO> getTreatmentRequestListFollowEstablishmentAll(Pageable pageable, Establishment est);

    /**
     * Get treatment history of patient.
     *
     * @param pageable  pagination
     * @param patientId patientId
     * @return treatment request dto
     */
    Page<TreatmentRequestDTO> getTreatmentRequestHistoryByPatient(Pageable pageable, Integer patientId, Integer medicalDepartmentId);

    /**
     * Get detail treatment service.
     *
     * @param est       current establishment
     * @param detailsId detail id
     * @return detail treatment service
     */
    TreatmentRequestDetailDTO getTreatmentRequestDetailFollowId(Establishment est, Integer detailsId);

    /**
     * Deny treatment request detail
     *
     * @param est       current establishment
     * @param detailsId order detail Id
     */
    void denyTreatmentRequestDetailFollowId(Establishment est, Integer detailsId);

    /**
     * Update symptom by treatment id.
     *
     * @param est         current establishment
     * @param treatmentId treatment id
     * @param symptom     symptom
     * @return
     */
    String updateSymptomByTreatmentId(Establishment est, Integer treatmentId, String symptom);

    /**
     * Update diagnose by treatment id.
     *
     * @param est         current establishment
     * @param treatmentId treatment id
     * @param diagnose    diagnose
     * @return
     */
    String updateDiagnoseByTreatmentId(Establishment est, Integer treatmentId, String diagnose);

    /**
     * Set list tests for treatment.
     *
     * @param est                current establishment
     * @param treatmentId        treatment id
     * @param preclinicalDetails list test preclinical
     * @return
     */
    String updatePreclinicalByTreatmentId(Establishment est,
                                          Integer treatmentId,
                                          List<PreclinicalDetailDoctorDTO> preclinicalDetails);

    /**
     * Set prescription for treatment.
     *
     * @param est         current establishment
     * @param treatmentId treatment id
     * @param dto         prescription
     * @return
     */
    String updatePrescriptionByTreatmentId(Establishment est, Integer treatmentId, PrescriptionCreateDTO dto);

    /**
     * Delete prescription of treatment.
     *
     * @param est         current establishment
     * @param treatmentId treatment id
     * @return
     */
    String deletePrescriptionRecord(Establishment est, Integer treatmentId);

    /**
     * Push notification treatment transport time to patient.
     *
     * @param treatmentId          treatment id
     * @param currentEstablishment current establishment
     * @return true if push notification successfully
     */
    boolean notificationTreatmentTransportTime(int treatmentId, Establishment currentEstablishment);


    /**
     * Confirm prescription by treatment id.
     *
     * @param est current establishment
     * @param treatmentId treatmentId
     * @return OK if successfully
     */
    String confirmPrescriptionByTreatmentId(Establishment est, Integer treatmentId);
}
