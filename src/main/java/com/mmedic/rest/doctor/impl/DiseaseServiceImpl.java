package com.mmedic.rest.doctor.impl;

import com.mmedic.entity.Disease;
import com.mmedic.entity.MedicalDepartment;
import com.mmedic.enums.DeleteStatus;
import com.mmedic.rest.doctor.repo.MedicalDepartmentRepository;
import com.mmedic.rest.doctor.repo.DiseaseRepository;
import com.mmedic.rest.doctor.DiseaseService;
import com.mmedic.rest.doctor.dto.DiseaseDTO;
import com.mmedic.rest.doctor.dto.PatientDiseaseHistoryDTO;
import com.mmedic.rest.doctor.mapper.DiseaseMapper;
import com.mmedic.spring.errors.MedicException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Disease service implementation.
 */
@RequiredArgsConstructor
@Service
public class DiseaseServiceImpl implements DiseaseService {

    private final DiseaseRepository diseaseRepository;

    private final MedicalDepartmentRepository medicalDepartmentRepository;

    private final DiseaseMapper diseaseMapper;

    @Override
    public List<DiseaseDTO> getDiseasesFollowDepartment(Integer departmentId) {
        List<Disease> rawList = diseaseRepository.
                findDiseasesByMedicalDepartment_IdAndIsDeletedIsFalse(departmentId);
        List<DiseaseDTO> resList = new ArrayList<>();
        rawList.forEach(md -> resList.add(diseaseMapper.convertToDTO(md)));
        return resList;
    }

    @Transactional
    @Override
    public DiseaseDTO createDisease(DiseaseDTO reqDto) {
        MedicalDepartment medicalDepartment =
                medicalDepartmentRepository.findById(reqDto.getDepartmentId()).orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_DEPARTMENT_NOT_EXIST, "Medical department is not exist."));

        Disease disease = diseaseMapper.convertToEntity(reqDto);
        disease.setMedicalDepartment(medicalDepartment);
        disease.setIsDeleted(DeleteStatus.AVAILABLE.getValue());
        disease.setCreateAt(LocalDateTime.now());
        disease.setUpdateAt(LocalDateTime.now());

        Disease savedRaw = diseaseRepository.save(disease);
        DiseaseDTO resDto = diseaseMapper.convertToDTO(savedRaw);
        return resDto;
    }

    @Transactional
    @Override
    public DiseaseDTO updateDisease(Integer id, DiseaseDTO reqDto) {
        Disease raw = diseaseRepository.
                findByIdAndIsDeletedIsFalse(id).orElseThrow(() -> new MedicException(MedicException.ERROR_DISEASE_NOT_EXIST, "Disease is not exists."));

        MedicalDepartment medicalDepartment =
                medicalDepartmentRepository.findById(reqDto.getDepartmentId()).orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_DEPARTMENT_NOT_EXIST, "Medical department is not exist."));

        raw.setName(reqDto.getName());
        raw.setDescription(reqDto.getDescription());
        raw.setMedicalDepartment(medicalDepartment);
        raw.setUpdateAt(LocalDateTime.now());
        Disease rawUpdated = diseaseRepository.save(raw);
        DiseaseDTO resDto = diseaseMapper.convertToDTO(rawUpdated);
        return resDto;
    }

    @Transactional
    @Override
    public DiseaseDTO deleteDisease(Integer id) {
        Disease raw = diseaseRepository.
                findByIdAndIsDeletedIsFalse(id).orElseThrow(() -> new MedicException(MedicException.ERROR_DISEASE_NOT_EXIST, "Disease is not exists."));
        raw.setIsDeleted(DeleteStatus.IS_DELETED.getValue());
        raw.setUpdateAt(LocalDateTime.now());
        Disease updatedRaw = diseaseRepository.save(raw);
        return diseaseMapper.convertToDTO(updatedRaw);
    }

    @Override
    public List<PatientDiseaseHistoryDTO> searchDiseaseFollowName(String query) {
        List<Disease> rawList = StringUtils.isNotBlank(query) ?
                diseaseRepository.findByNameContains(query) : diseaseRepository.findAll();
        List<PatientDiseaseHistoryDTO> resList = new ArrayList<>();
        rawList.forEach(md -> resList.add(diseaseMapper.convertToPatientDiseaseHistoryDTO(md)));
        return resList;
    }
}
