package com.mmedic.rest.doctor.impl;

import com.mmedic.entity.MedicalDepartment;
import com.mmedic.enums.DeleteStatus;
import com.mmedic.rest.doctor.MedicalDepartmentService;
import com.mmedic.rest.doctor.dto.DepartmentDTO;
import com.mmedic.rest.doctor.mapper.DepartmentMapper;
import com.mmedic.rest.doctor.repo.MedicalDepartmentRepository;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Medical department service implementation.
 */
@RequiredArgsConstructor
@Service
public class MedicalDepartmentServiceImpl implements MedicalDepartmentService {

    private final MedicalDepartmentRepository medicalDepartmentRepository;

    private final DepartmentMapper departmentMapper;

    @Override
    public List<DepartmentDTO> findMedicalDepartment(String condition) {
        List<MedicalDepartment> rawList = medicalDepartmentRepository.findMedicalDepartment(Helper.convertNullToEmpty(condition));
        return departmentMapper.convertToListDepartmentDTO(rawList);
    }

    @Transactional
    @Override
    public DepartmentDTO createDepartment(DepartmentDTO reqDto) {
        MedicalDepartment md = departmentMapper.convertToEntity(reqDto);
        md.setCreateAt(LocalDateTime.now());
        md.setUpdateAt(LocalDateTime.now());
        md.setIsDeleted(DeleteStatus.AVAILABLE.getValue());
        MedicalDepartment raw = medicalDepartmentRepository.save(md);
        DepartmentDTO resDto = departmentMapper.convertToDTO(raw);
        return resDto;
    }

    @Transactional
    @Override
    public DepartmentDTO updateDepartment(Integer id, DepartmentDTO reqDto) {
        MedicalDepartment raw = medicalDepartmentRepository.
                findByIdAndAndIsDeletedIsFalse(id).orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_DEPARTMENT_NOT_EXIST, "Medical department is not exist."));
        raw.setName(reqDto.getName());
        raw.setDescription(reqDto.getDescription());
        raw.setUpdateAt(LocalDateTime.now());
        MedicalDepartment rawUpdated = medicalDepartmentRepository.save(raw);
        DepartmentDTO resDto = departmentMapper.convertToDTO(rawUpdated);
        return resDto;
    }

    @Transactional
    @Override
    public DepartmentDTO deleteDepartment(Integer id) {
        MedicalDepartment raw = medicalDepartmentRepository.
                findByIdAndAndIsDeletedIsFalse(id).orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_DEPARTMENT_NOT_EXIST, "Medical department is not exist."));
        raw.setIsDeleted(DeleteStatus.IS_DELETED.getValue());
        raw.setUpdateAt(LocalDateTime.now());
        MedicalDepartment updatedRaw = medicalDepartmentRepository.save(raw);
        return departmentMapper.convertToDTO(raw);
    }
}
