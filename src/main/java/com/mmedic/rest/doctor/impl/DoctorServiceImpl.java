package com.mmedic.rest.doctor.impl;

import com.mmedic.entity.Account;
import com.mmedic.entity.DrugUnit;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.EstablishmentWorkingSchedule;
import com.mmedic.entity.ExamineRecord;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalService;
import com.mmedic.entity.MedicalServiceGroup;
import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.entity.MedicalServiceOrderDiscussion;
import com.mmedic.entity.MedicalServicePriceHistory;
import com.mmedic.entity.MedicalServiceRegistry;
import com.mmedic.entity.Patient;
import com.mmedic.entity.PreclinicalRecord;
import com.mmedic.entity.PrescriptionDetail;
import com.mmedic.entity.PrescriptionRecord;
import com.mmedic.entity.TravelPricePolicy;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.enums.MedicalRecordType;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.enums.TreatmentRequestStatus;
import com.mmedic.enums.TreatmentType;
import com.mmedic.rest.account.dto.AccountNameDTO;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.doctor.DoctorService;
import com.mmedic.rest.doctor.dto.DoctorPackageActiveParamDTO;
import com.mmedic.rest.doctor.dto.DoctorPackageDTO;
import com.mmedic.rest.doctor.dto.DoctorPackagePriceParamDTO;
import com.mmedic.rest.doctor.dto.DoctorPackageStatusParamDTO;
import com.mmedic.rest.doctor.dto.DoctorSearchResultDTO;
import com.mmedic.rest.doctor.dto.HealthCareNotificationDTO;
import com.mmedic.rest.doctor.dto.HealthCareServiceDTO;
import com.mmedic.rest.doctor.dto.HealthCareServiceDetailDTO;
import com.mmedic.rest.doctor.dto.MedicalStaffSearchResultDTO;
import com.mmedic.rest.doctor.dto.PrescriptionCreateDTO;
import com.mmedic.rest.doctor.dto.TreatmentBillDTO;
import com.mmedic.rest.doctor.dto.TreatmentRequestDTO;
import com.mmedic.rest.doctor.dto.TreatmentRequestDetailDTO;
import com.mmedic.rest.doctor.mapper.HealthCareServiceMapper;
import com.mmedic.rest.doctor.repo.DoctorRepository;
import com.mmedic.rest.drug.repo.DrugUnitRepository;
import com.mmedic.rest.establishment.EstablishmentService;
import com.mmedic.rest.establishment.dto.WorkTimeDTO;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.rest.medicalRecord.dto.PreclinicalDetailDoctorDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionDetailDoctorDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDoctorDTO;
import com.mmedic.rest.medicalRecord.repo.ExamineRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalServiceOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.PreclinicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.PrescriptionDetailRepository;
import com.mmedic.rest.medicalRecord.repo.PrescriptionRecordRepository;
import com.mmedic.rest.medicalService.dto.MedicalServiceGroupDTO;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderDiscussionRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServicePriceHistoryRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.rest.patient.PatientService;
import com.mmedic.rest.patient.dto.PatientDTO;
import com.mmedic.rest.travelprice.repo.TravelPricePolicyRepository;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Doctor service implementation.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;

    private final AccountRepository accountRepository;

    private final TravelPricePolicyRepository travelPricePolicyRepository;

    private final EstablishmentService establishmentService;

    private final MedicalServiceRepository medicalServiceRepository;

    private final MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    private final MedicalServicePriceHistoryRepository medicalServicePriceHistoryRepository;

    private final MedicalRecordRepository medicalRecordRepository;

    private final MedicalServiceOrderDiscussionRepository medicalServiceOrderDiscussionRepository;

    private final MedicalServiceOrderDetailRepository medicalServiceOrderDetailRepository;

    private final HealthCareServiceMapper healthCareServiceMapper;

    private final PreclinicalRecordRepository preclinicalRecordRepository;

    private final PatientService patientService;

    private final ExamineRecordRepository examineRecordRepository;

    private final MedicalServiceOrderRepository medicalServiceOrderRepository;

    private final EstablishmentRepository establishmentRepository;

    private final PrescriptionRecordRepository prescriptionRecordRepository;

    private final DrugUnitRepository drugUnitRepository;

    private final PrescriptionDetailRepository prescriptionDetailRepository;

    private final EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    private final NotificationService notificationService;

    @Override
    public Page<DoctorSearchResultDTO> findDoctors(String condition, int departmentId, double latitude, double longitude, Pageable pageable) {
        Page<DoctorSearchResultDTO> doctors = doctorRepository.findDoctors("%" + Helper.convertNullToEmpty(condition) + "%", departmentId, latitude, longitude, pageable);
        if (!CollectionUtils.isEmpty(doctors.getContent())) {
            List<DoctorSearchResultDTO> result = new ArrayList<>();
            List<Integer> establishmentIds = doctors.getContent().stream().map(DoctorSearchResultDTO::getEstablishmentId).collect(Collectors.toList());
            List<Integer> lstEstablishmentAvailable = establishmentService.calculateAvailable(establishmentIds);
            if (!CollectionUtils.isEmpty(lstEstablishmentAvailable)) {
                doctors.getContent().forEach(doctor -> {
                    if (lstEstablishmentAvailable.contains(doctor.getEstablishmentId())) {
                        DoctorSearchResultDTO doctorAvai = new DoctorSearchResultDTO() {
                            @Override
                            public int getId() {
                                return doctor.getId();
                            }

                            @Override
                            public String getName() {
                                return doctor.getName();
                            }

                            @Override
                            public String getAddress() {
                                return doctor.getAddress();
                            }

                            @Override
                            public String getPhoneNumber() {
                                return doctor.getPhoneNumber();
                            }

                            @Override
                            public String getAvatarUrl() {
                                return doctor.getAvatarUrl();
                            }

                            @Override
                            public int getPrice() {
                                return doctor.getPrice();
                            }

                            @Override
                            public float getRatingAverage() {
                                return doctor.getRatingAverage();
                            }

                            @Override
                            public int getEstablishmentId() {
                                return doctor.getEstablishmentId();
                            }

                            @Override
                            public Boolean getAvailable() {
                                return true;
                            }
                        };
                        result.add(doctorAvai);
                    } else {
                        result.add(doctor);
                    }
                });
                doctors = new PageImpl<>(result, pageable, doctors.getTotalElements());
            }
        }
        return doctors;
    }

    @Override
    public List<WorkTimeDTO> findDoctorsWorkTime(LocalDate startDate, LocalDate endDate, int doctorId) {
        Account doctor = accountRepository.findDoctorAccountById(doctorId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_EXIST, "Doctor account don't exist"));

        List<WorkTimeDTO> result = establishmentService.findWorkTimeEstablishment(startDate, endDate, doctor.getEstablishment().getId());
        return result.stream().
                sorted(Comparator.comparing(WorkTimeDTO::getDate).
                        thenComparing(WorkTimeDTO::getStartTime)).
                collect(Collectors.toList());
    }

    @Override
    public List<TreatmentBillDTO> calculateCost(int doctorId, int patientId, TreatmentType treatmentType) {
        List<TreatmentBillDTO> treatmentBills = new ArrayList<>();
        Account doctor = accountRepository.findDoctorAccountById(doctorId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_EXIST, "Doctor account don't exist"));
        Account patient = accountRepository.findPatientAccountByPatientId(patientId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_PATIENT_NOT_EXIST, "Patient account don't exist"));

        if (null == doctor.getEstablishment()) {
            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Establishment of doctor don't exist");
        }

        // Calculate fee of treatment
        Integer treatmentPrice = doctorRepository.findTreatmentPrice(doctor.getEstablishment().getId());
        if (null == treatmentPrice || treatmentPrice.compareTo(0) == 0) {
            throw new MedicException(MedicException.ERROR_DOCTOR_PRICE_IS_0, "Price is null or equal 0");
        }
        TreatmentBillDTO treatmentBill = new TreatmentBillDTO();
        treatmentBill.setType(Constants.TREATMENT_FEE_TYPE);
        treatmentBill.setName("Phí Khám Bệnh");
        treatmentBill.setPrice(treatmentPrice);
        treatmentBills.add(treatmentBill);

        // Calculate fee of transport
        if (treatmentType.equals(TreatmentType.HOME)) {
            if (doctor.getEstablishment().getLatitude() == 0 && doctor.getEstablishment().getLongitude() == 0) {
                throw new MedicException(MedicException.ERROR_ESTABLISHMENT_DONT_HAVE_LOCATION, "Establishment don't have location (latitude, longitude)");
            } else if (patient.getPatient().getLatitude() == 0 && patient.getPatient().getLongitude() == 0) {
                throw new MedicException(MedicException.ERROR_PATIENT_DONT_HAVE_LOCATION, "Patient don't have location (latitude, longitude)");
            }
            long distanceKm = Math.round(Helper.distance(doctor.getEstablishment().getLatitude(), doctor.getEstablishment().getLongitude(),
                    patient.getPatient().getLatitude(), patient.getPatient().getLongitude()) / 1000);

            int distancePrice = 0;
            List<TravelPricePolicy> travelPricePolicies = travelPricePolicyRepository.findAll();
            travelPricePolicies = travelPricePolicies.stream().sorted(Comparator.comparingInt(TravelPricePolicy::getOrder)).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(travelPricePolicies)) {
                int order = 0;
                while (distanceKm > 0) {
                    int pricePerKm = 0;
                    int addKm = 0;
                    if (order <= travelPricePolicies.size() - 1) {
                        pricePerKm = travelPricePolicies.get(order).getPricePerKm();
                        addKm = travelPricePolicies.get(order).getAddedKm();
                        if (order < travelPricePolicies.size() - 1) {
                            order += 1;
                        }
                    }
                    int km = (int) Math.min(distanceKm, addKm);
                    distancePrice += km * pricePerKm;
                    distanceKm -= km;
                }
            }
            TreatmentBillDTO transportBill = new TreatmentBillDTO();
            transportBill.setType(Constants.TRANSPORT_FEE_TYPE);
            transportBill.setName("Phí Di Chuyển");
            transportBill.setPrice(distancePrice);
            treatmentBills.add(transportBill);
        }

        return treatmentBills;
    }

    @Override
    public List<HealthCareServiceDTO> getDoctorHealthCareServiceOrdered(int doctorId, Integer serviceId, LocalDate date) throws MedicException {
        return doctorRepository.findDoctorHealthCareServiceOrdered(doctorId, serviceId, date);
    }

    @Override
    public HealthCareServiceDetailDTO getHealthCareServiceDetail(int medicalRecordId) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Can't find medical record"));

        if (medicalRecord.getRecordType().equals(MedicalRecordType.SERVICE) && null != medicalRecord.getServiceRecord()) {
            MedicalServiceOrder serviceOrder = medicalRecord.getServiceRecord().getMedicalServiceOrder();
            if (null == serviceOrder) {
                throw new MedicException(MedicException.ERROR_ORDER_HEALTH_CARE_NOT_EXIST);
            }
            List<MedicalServiceOrderDetail> serviceOrderDetails = serviceOrder.getMedicalServiceOrderDetails();
            if (CollectionUtils.isEmpty(serviceOrderDetails)) {
                throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST);
            }
            MedicalServiceOrderDetail orderDetail = serviceOrderDetails.get(0);

            // Convert to HealthCareServiceDetail dto
            HealthCareServiceDetailDTO detailDTO = new HealthCareServiceDetailDTO();
            detailDTO.setId(medicalRecord.getId());
            detailDTO.setPatientId(medicalRecord.getPatient().getId());
            detailDTO.setPatientName(medicalRecord.getPatient().getName());
            detailDTO.setServiceId(orderDetail.getMedicalService().getMedicalServiceGroup().getId());
            detailDTO.setServiceName(orderDetail.getMedicalService().getMedicalServiceGroup().getName());
            detailDTO.setPackageId(orderDetail.getMedicalService().getId());
            detailDTO.setPackageName(orderDetail.getMedicalService().getName());
            detailDTO.setStartDate(orderDetail.getAppointmentDate().toLocalDate());
            detailDTO.setStatus(orderDetail.getStatus());
            detailDTO.setPaymentStatus(serviceOrder.getPaymentStatus());

            List<MedicalServiceOrderDiscussion> discussions = orderDetail.getMedicalServiceOrderDiscussions();
            if (!CollectionUtils.isEmpty(discussions)) {
                discussions.forEach(discussion -> {
                    if (discussion.getMessageSender().equals(medicalRecord.getPatient().getAccount())) {
                        detailDTO.setPatientMessage(discussion.getMessage());
                    } else if (discussion.getMessageSender().equals(orderDetail.getHandleBy())) {
                        detailDTO.setDoctorMessage(discussion.getMessage());
                    }
                });
            }
            return detailDTO;
        }

        return null;
    }

    @Override
    @Transactional
    public HealthCareServiceDTO processHealthCareService(int medicalRecordId, DoctorPackageStatusParamDTO doctorPackageStatusParam) throws MedicException {
        MedicalRecord medicalRecord = medicalRecordRepository.findById(medicalRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST, "Can't find medical record"));
        if (!medicalRecord.getRecordType().equals(MedicalRecordType.SERVICE)) {
            throw new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST);
        }

        MedicalServiceOrder serviceOrder = medicalRecord.getServiceRecord().getMedicalServiceOrder();

        if (null == serviceOrder) {
            throw new MedicException(MedicException.ERROR_ORDER_HEALTH_CARE_NOT_EXIST);
        }
        if (serviceOrder.getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_HAS_PAID);
        }

        List<MedicalServiceOrderDetail> serviceOrderDetails = serviceOrder.getMedicalServiceOrderDetails();

        if (CollectionUtils.isEmpty(serviceOrderDetails)) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST);
        }
        MedicalServiceOrderDetail orderDetail = serviceOrderDetails.get(0);
        if (MedicalServiceOrderStatus.REQUEST_ACCEPTED.equals(doctorPackageStatusParam.getStatus())) {
            orderDetail.setProccessDate(LocalDateTime.now());
        } else if (MedicalServiceOrderStatus.REQUEST_DENIED.equals(doctorPackageStatusParam.getStatus()) ||
                MedicalServiceOrderStatus.REQUEST_CANCELLED.equals(doctorPackageStatusParam.getStatus())) {
            orderDetail.setResultDate(LocalDateTime.now());
        }
        orderDetail.setStatus(doctorPackageStatusParam.getStatus());
        MedicalServiceOrderDetail orderDetailUpdate = medicalServiceOrderDetailRepository.save(orderDetail);

        // Notification
        notificationService.notificationHealthCareStatus(orderDetailUpdate, medicalRecord.getPatient().getAccount());

        if (!StringUtils.isEmpty(doctorPackageStatusParam.getMessage())) {
            MedicalServiceOrderDiscussion discussion = new MedicalServiceOrderDiscussion();
            discussion.setMedicalServiceOrderDetail(orderDetail);
            discussion.setMessage(doctorPackageStatusParam.getMessage());
            discussion.setMessageSender(orderDetail.getHandleBy());
            medicalServiceOrderDiscussionRepository.save(discussion);
        }

        return new HealthCareServiceDTO() {
            @Override
            public int getId() {
                return medicalRecord.getId();
            }

            @Override
            public int getPatientId() {
                return medicalRecord.getPatient().getId();
            }

            @Override
            public String getPatientName() {
                return medicalRecord.getPatient().getName();
            }

            @Override
            public LocalDate getDateOfBirth() {
                return medicalRecord.getPatient().getDateOfBirth();
            }

            @Override
            public String getAvatarUrl() {
                return medicalRecord.getPatient().getAccount().getAvatarUrl();
            }

            @Override
            public int getServiceId() {
                return orderDetailUpdate.getMedicalService().getMedicalServiceGroup().getId();
            }

            @Override
            public String getServiceName() {
                return orderDetailUpdate.getMedicalService().getMedicalServiceGroup().getName();
            }

            @Override
            public int getPackageId() {
                return orderDetailUpdate.getMedicalService().getId();
            }

            @Override
            public String getPackageName() {
                return orderDetailUpdate.getMedicalService().getName();
            }

            @Override
            public LocalDate getStartDate() {
                return orderDetailUpdate.getAppointmentDate().toLocalDate();
            }

            @Override
            public MedicalServiceOrderStatus getStatus() {
                return orderDetailUpdate.getStatus();
            }

            @Override
            public PaymentStatus getPaymentStatus() {
                return serviceOrder.getPaymentStatus();
            }
        };
    }

    @Override
    public List<DoctorPackageDTO> getDoctorOrNursePackages(int doctorId, int serviceId, Boolean all) throws MedicException {
        Account doctor = accountRepository.findDoctorOrNurseAccountById(doctorId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_OR_NURSE_NOT_EXIST, "Account doctor or nurse don't exist"));
        if (null == doctor.getEstablishment()) {
            throw new MedicException(MedicException.ERROR_NOT_FOUND_ESTABLISHMENT, "Doctor/Nurse don't have establishment");
        }
        boolean showAll = null != all && all;
        return doctorRepository.findDoctorPackages(doctor.getEstablishment().getId(), serviceId, showAll);
    }

    @Override
    @Transactional
    public boolean activeDoctorOrNursePackage(int doctorId, int serviceId, int packageId, DoctorPackageActiveParamDTO doctorPackageActiveParam) throws MedicException {
        Account doctor = accountRepository.findDoctorOrNurseAccountById(doctorId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_EXIST, "Account doctor don't exist"));
        if (null == doctor.getEstablishment()) {
            throw new MedicException(MedicException.ERROR_NOT_FOUND_ESTABLISHMENT, "Doctor don't have establishment");
        }

        MedicalService medicalService = medicalServiceRepository.findByIdAndMedicalServiceGroup_IdAndIsDeletedIsFalse(packageId, serviceId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_HEALTH_CARE_SERVICE_NOT_FOUND, "Can't find the health care service"));

        if (medicalService.getMedicalServiceGroup().getId() < 5) {
            throw new MedicException(MedicException.ERROR_SERVICE_NOT_HEALTH_CARE_TYPE, "This service isn't health care service");
        }

        MedicalServiceRegistry serviceRegistry = medicalServiceRegistryRepository.findByMedicalService_IdAndEstablishment_Id(packageId, doctor.getEstablishment().getId()).
                orElse(null);

        if (null == serviceRegistry) {
            serviceRegistry = new MedicalServiceRegistry();
            serviceRegistry.setMedicalService(medicalService);
            serviceRegistry.setEstablishment(doctor.getEstablishment());
        }
        serviceRegistry.setActive(doctorPackageActiveParam.getActive());
        medicalServiceRegistryRepository.save(serviceRegistry);
        return true;
    }

    @Override
    @Transactional
    public boolean updatePriceDoctorOrNursePackage(int doctorId, int serviceId, int packageId, DoctorPackagePriceParamDTO doctorPackagePriceParam) throws MedicException {
        Account doctor = accountRepository.findDoctorOrNurseAccountById(doctorId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_EXIST, "Account doctor don't exist"));
        if (null == doctor.getEstablishment()) {
            throw new MedicException(MedicException.ERROR_NOT_FOUND_ESTABLISHMENT, "Doctor don't have establishment");
        }

        MedicalService medicalService = medicalServiceRepository.findByIdAndMedicalServiceGroup_IdAndIsDeletedIsFalse(packageId, serviceId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_HEALTH_CARE_SERVICE_NOT_FOUND, "Can't find the health care service"));

        if (medicalService.getMedicalServiceGroup().getId() < 5) {
            throw new MedicException(MedicException.ERROR_SERVICE_NOT_HEALTH_CARE_TYPE, "This service isn't health care service");
        }

        MedicalServiceRegistry serviceRegistry = medicalServiceRegistryRepository.findByMedicalService_IdAndEstablishment_IdAndActiveTrue(packageId, doctor.getEstablishment().getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_REGISTRY_HEALTH_CARE_SERVICE, "This service isn't registry for establishment or it's disabled"));

        LocalDateTime now = LocalDateTime.now();
        if (null != serviceRegistry) {
            if (checkEffectiveDateExist(now, serviceRegistry.getMedicalServicePriceHistories())) {
                MedicalServicePriceHistory price = new MedicalServicePriceHistory();
                price.setMedicalServiceRegistry(serviceRegistry);
                price.setPrice(doctorPackagePriceParam.getPrice());
                price.setEffectiveAt(now);

                notificationPriceChangeForPatient(doctorId, packageId);
                medicalServicePriceHistoryRepository.save(price);
            } else {
                throw new MedicException(MedicException.ERROR_EFFECTIVE_IS_EXIST, "This effective date is exist");
            }
        }
        return true;
    }

    /**
     * Notification price change for patient.
     *
     * @param staffId   staff id have health care service's price is changed
     * @param packageId package id
     */
    private void notificationPriceChangeForPatient(int staffId, int packageId) {
        new Thread(() -> {
            List<HealthCareNotificationDTO> healthCareNotifications = medicalRecordRepository.findHealthCarePriceChange(staffId, packageId);
            if (!CollectionUtils.isEmpty(healthCareNotifications)) {
                notificationService.notificationHealthCareServicePriceChange(healthCareNotifications);
            }
        }).start();
    }

    /**
     * Check effective date exist.
     *
     * @param effectiveDate  effective date
     * @param priceHistories price history
     * @return true if none match
     */
    private boolean checkEffectiveDateExist(LocalDateTime effectiveDate, List<MedicalServicePriceHistory> priceHistories) {
        return priceHistories.stream().noneMatch(priceHistory -> priceHistory.getEffectiveAt().isEqual(effectiveDate));
    }

    @Override
    public Page<MedicalStaffSearchResultDTO> findMedicalStaffs(String condition, Integer packageId, Pageable pageable) {
        return doctorRepository.findAllMedicalStaffByPackageId("%" + Helper.convertNullToEmpty(condition) + "%", packageId, pageable);
    }

    @Override
    public Page<MedicalStaffSearchResultDTO> findBasicInfoOfMedicalStaffs(String condition, String staffRole, Integer departmentId, Pageable pageable) {
        List<String> roles = Arrays.asList(Constants.ROLE_DOCTOR_ADMIN, Constants.ROLE_DOCTOR_STAFF, Constants.ROLE_NURSE_ADMIN, Constants.ROLE_NURSE_STAFF);
        if ("doctor".equalsIgnoreCase(staffRole)) {
            roles = Arrays.asList(Constants.ROLE_DOCTOR_ADMIN, Constants.ROLE_DOCTOR_STAFF);
        } else if ("nurse".equalsIgnoreCase(staffRole)) {
            roles = Arrays.asList(Constants.ROLE_NURSE_ADMIN, Constants.ROLE_NURSE_STAFF);
        }
        return doctorRepository.findBasicInfoOfMedicalStaff("%" + Helper.convertNullToEmpty(condition) + "%", roles, departmentId, pageable);
    }

    @Override
    public List<MedicalServiceGroupDTO> getDoctorHealthCareServices(int doctorId) {
        List<MedicalServiceGroup> serviceGroups = doctorRepository.getDoctorMedicalServiceGroups(doctorId);
        return healthCareServiceMapper.toListMedicalServiceGroupDTO(serviceGroups);
    }

    @Override
    public List<TreatmentRequestDTO> getTreatmentRequestListFollowEstablishmentToday(Establishment est) {
        List<TreatmentRequestDTO> treatmentRequestDTOS = new ArrayList<>();

        List<MedicalServiceOrderDetail> medicalServiceOrderDetails = medicalServiceOrderDetailRepository.
                findAllTreatmentRequest(est.getId(), 1);

        // Convert detail to DTO
        for (MedicalServiceOrderDetail detail : medicalServiceOrderDetails) {
            treatmentRequestDTOS.add(convertDetailOrderToTreatmentRequest(detail));
        }

        return treatmentRequestDTOS;
    }

    @Override
    public Page<TreatmentRequestDTO> getTreatmentRequestListFollowEstablishmentAll(Pageable pageable, Establishment est) {
        List<TreatmentRequestDTO> treatmentRequestDTOS = new ArrayList<>();
        Page<MedicalServiceOrderDetail> medicalServiceOrderDetails = medicalServiceOrderDetailRepository.
                findAllByEstablishment_IdAndMedicalService_IdAndStatusIn(pageable,
                        est.getId(), 1,
                        MedicalServiceOrderStatus.REQUEST_DENIED,
                        MedicalServiceOrderStatus.REQUEST_ACCEPTED,
                        MedicalServiceOrderStatus.REQUEST_PROCESSING,
                        MedicalServiceOrderStatus.REQUEST_FULFILLED
                );

        // Convert detail to DTO
        for (MedicalServiceOrderDetail detail : medicalServiceOrderDetails) {
            treatmentRequestDTOS.add(convertDetailOrderToTreatmentRequest(detail));
        }

        final Page<TreatmentRequestDTO> results = new PageImpl<>(treatmentRequestDTOS,
                pageable,
                medicalServiceOrderDetails.getTotalElements());

        return results;
    }

    @Override
    public void denyTreatmentRequestDetailFollowId(Establishment est, Integer detailsId) {
        MedicalServiceOrderDetail detail = medicalServiceOrderDetailRepository.findByEstablishment_IdAndId(est.getId(), detailsId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Can not found order detail with Id = " + detailsId));
        if (detail.getStatus().equals(MedicalServiceOrderStatus.REQUEST_ACCEPTED)) {
            updateTreatmentRequestStatus(est.getId(), detailsId, MedicalServiceOrderStatus.REQUEST_DENIED);
        } else {
            throw new MedicException(MedicException.ERROR_DATA_INVALID, "Request status is processing, can not be denied ");
        }

    }

    @Override
    public TreatmentRequestDetailDTO getTreatmentRequestDetailFollowId(Establishment est,
                                                                       Integer detailsId) {
        MedicalServiceOrderDetail detail = medicalServiceOrderDetailRepository.findByEstablishment_IdAndId(est.getId(), detailsId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Cant not found order detail with Id = " + detailsId));

        MedicalRecord medicalRecord;
        if (detail.getMedicalServiceOrder().getExamineRecord() != null) {
            medicalRecord = detail.getMedicalServiceOrder().getExamineRecord().getMedicalRecord();
        } else {
            throw new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST);
        }


        List<PreclinicalDetailDoctorDTO> preclincalDetailsList = new ArrayList<>();
        Optional<PreclinicalRecord> precRecord = preclinicalRecordRepository.findByMedicalRecord_Id(medicalRecord.getId());
        if (precRecord.isPresent()) {
            preclincalDetailsList = medicalServiceOrderDetailRepository.
                    findPreclinicalServiceByPreclinicalRecord(precRecord.get().getId());


            // Get startTime/endTime on appointmentDate of each preclinical.
            preclincalDetailsList.forEach(rec -> {
                if (rec.getSequenceNumber() != null) {
                    Map<String, Integer> sequenceMap = Helper.getSequenceNumber(rec.getSequenceNumber());
                    int rowIndex = sequenceMap.get(Constants.SEQUENCE_NUMBER_INDEX);
                    EstablishmentWorkingSchedule workingSchedule =
                            establishmentWorkingScheduleRepository.findEstablishmentWorkingByIndex(rec.getEstablishmentId(), rowIndex - 1).orElseThrow(() ->
                                    new MedicException(MedicException.ERROR_DATA_INVALID, "Can not find working schedule time"));
                    rec.setStartTime(workingSchedule.getStartTime());
                    rec.setEndTime(workingSchedule.getEndTime());
                    rec.setWorkTimeId(workingSchedule.getId());
                }
            });

        }

        boolean prescriptionIsConfirmed = false;
        Long prescriptionCreateTime = 0L;
        List<PrescriptionDetailDoctorDTO> prescriptDetailsList = new ArrayList<>();
        Optional<PrescriptionRecord> prescriptRecord =
                prescriptionRecordRepository.findByMedicalRecord_Id(medicalRecord.getId());
        if (prescriptRecord.isPresent()) {
            prescriptionCreateTime = 180 - Helper.getTimeDuration(prescriptRecord.get().getCreateAt());
            if (prescriptionCreateTime <= 0) prescriptionCreateTime = 0L;
            prescriptDetailsList = prescriptionDetailRepository.findByPrescriptionRecord(prescriptRecord.get().getId());
            prescriptionIsConfirmed = prescriptRecord.get().getIsConfirmed();
        }

        ExamineRecord examineRecord = detail.getMedicalServiceOrder().getExamineRecord();
        Patient patient = medicalRecord.getPatient();

        PatientDTO patientDTO = patientService.getPatientInfo(patient.getId());
        TreatmentRequestDTO treatmentRequestDTO = convertDetailOrderToTreatmentRequest(detail);

        // Set isNotificationTransport for treatment


        TreatmentRequestDetailDTO requestDetail = new TreatmentRequestDetailDTO();
        requestDetail.setSymptom(examineRecord.getSymptom());
        requestDetail.setDiagnose(examineRecord.getDiagnose());
        requestDetail.setRemind(examineRecord.getRemind());
        requestDetail.setTreatmentRequestInfo(treatmentRequestDTO);
        requestDetail.setPatientInfo(patientDTO);
        requestDetail.setPrescriptionDetails(prescriptDetailsList);
        requestDetail.setPreclinicalDetails(preclincalDetailsList);
        requestDetail.setPrescriptionTimeout(prescriptionCreateTime);
        requestDetail.setPrescriptionIsConfirmed(prescriptionIsConfirmed);
        return requestDetail;
    }

    @Override
    public Page<TreatmentRequestDTO> getTreatmentRequestHistoryByPatient(Pageable pageable, Integer patientId, Integer medicalDeparmentId) {
        List<TreatmentRequestDTO> treatmentRequestDTOS = new ArrayList<>();
        Page<MedicalServiceOrderDetail> medicalServiceOrderDetails = medicalServiceOrderDetailRepository.findByPatientId(pageable,
                medicalDeparmentId,
                patientId,
//                MedicalServiceOrderStatus.REQUEST_DENIED,
//                MedicalServiceOrderStatus.REQUEST_ACCEPTED,
//                MedicalServiceOrderStatus.REQUEST_PROCESSING,
                MedicalServiceOrderStatus.REQUEST_FULFILLED
        );
        // Convert detail to DTO
        for (MedicalServiceOrderDetail detail : medicalServiceOrderDetails) {
            treatmentRequestDTOS.add(convertDetailOrderToTreatmentRequest(detail));
        }

        final Page<TreatmentRequestDTO> results = new PageImpl<>(treatmentRequestDTOS,
                pageable,
                medicalServiceOrderDetails.getTotalElements());

        return results;
    }

    @Override
    @Transactional
    public String updateSymptomByTreatmentId(Establishment est, Integer treatmentId, String symptom) {
        ExamineRecord examineRecord = examineRecordRepository.findByOrderDetailId(est.getId(), treatmentId).orElseThrow(() ->
                new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Can not find examine record"));
        examineRecord.setSymptom(symptom);
        examineRecordRepository.save(examineRecord);

        // Update status of transport service (if exist)
        List<MedicalServiceOrderDetail> serviceOrderDetails = examineRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails();
        if (!CollectionUtils.isEmpty(serviceOrderDetails)) {
            MedicalServiceOrderDetail transportService = serviceOrderDetails.stream().
                    filter(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_TRANSPORT).
                    findFirst().
                    orElse(null);
            if (null != transportService) {
                transportService.setStatus(MedicalServiceOrderStatus.REQUEST_FULFILLED);
                medicalServiceOrderDetailRepository.save(transportService);
            }
        }
        return "OK";
    }

    @Override
    @Transactional
    public String updateDiagnoseByTreatmentId(Establishment est, Integer treatmentId, String diagnose) {
        ExamineRecord examineRecord = examineRecordRepository.findByOrderDetailId(est.getId(), treatmentId).orElseThrow(() ->
                new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Can not find examine record"));
        if (org.apache.commons.lang3.StringUtils.isBlank(diagnose)) {
            throw new MedicException(MedicException.ERROR_DATA_INVALID, "Diagnose cant be empty");
        }
        examineRecord.setDiagnose(diagnose);
        examineRecordRepository.save(examineRecord);
        updateTreatmentRequestStatus(est.getId(), treatmentId, MedicalServiceOrderStatus.REQUEST_PROCESSING);
        return "OK";
    }

    private void validatePreclinicalDetailDoctor(PreclinicalDetailDoctorDTO rec) {
        if (null != rec.getEstablishmentId() &&
                !establishmentRepository.isValidEstablishmentFollowConditions(rec.getEstablishmentId(),
                        EstablishmentType.PRECLINICAL.getCode(), rec.getWorkTimeId(), rec.getServiceId())) {
            throw new MedicException(MedicException.ERROR_DATA_INVALID,
                    "Establishment is invalid with id = " + rec.getEstablishmentId());
        }

        if (null != rec.getEstablishmentId() &&
                (null == rec.getWorkTimeId() ^ null == rec.getAppointmentDate())) {
            throw new MedicException(MedicException.ERROR_DATA_INVALID,
                    String.format("EstablishmentId %d is invalid with WorkTimeId and appointmentDate .",
                            rec.getEstablishmentId()));
        } else if (null != rec.getEstablishmentId() &&
                null != rec.getWorkTimeId() &&
                null != rec.getAppointmentDate()) {
            List<WorkTimeDTO> workTimeDTOS = establishmentService.findWorkTimeEstablishment(rec.getAppointmentDate().toLocalDate(),
                    rec.getAppointmentDate().toLocalDate(), rec.getEstablishmentId());
            workTimeDTOS.stream().filter(w -> w.getId() == rec.getWorkTimeId()).findFirst().orElseThrow(() ->
                    new MedicException(MedicException.ERROR_DATA_INVALID, "Work time schedule is invalid with id = " + rec.getWorkTimeId()));
        }

    }

    private Integer getSequenceNumber(Integer establishmentId, Integer workTimeId) {
        List<EstablishmentWorkingSchedule> eWS =
                establishmentWorkingScheduleRepository.
                        findAllByEstablishment_IdOrderById(establishmentId);
        Integer index = eWS.stream().map(r -> r.getId()).collect(Collectors.toList()).indexOf(workTimeId);

        return index >= 0 ? 10000 + (index + 1) : null;
    }

    @Override
    @Transactional
    public String updatePreclinicalByTreatmentId(Establishment est,
                                                 Integer treatmentId,
                                                 List<PreclinicalDetailDoctorDTO> preclinicalDetails) {
        MedicalRecord medicalRecord = getMedicalRecord(est, treatmentId);

        preclinicalRecordRepository.findByMedicalRecord_Id(medicalRecord.getId()).ifPresent(prec -> {
            throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST,
                    "Preclinical services is processing, can not send any request.");
        });

        if (medicalRecord.getExamineRecord().getSkipPreclinical() != null &&
                medicalRecord.getExamineRecord().getSkipPreclinical().equals(Boolean.TRUE)) {
            throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST,
                    "Preclinical services is processing, can not send any request.");
        }

        updateTreatmentRequestStatus(est.getId(), treatmentId, MedicalServiceOrderStatus.REQUEST_PROCESSING);
        if (preclinicalDetails.size() == 0) {
            ExamineRecord examineRecord = medicalRecord.getExamineRecord();
            examineRecord.setSkipPreclinical(Boolean.TRUE);
            examineRecordRepository.save(examineRecord);
            return "OK";
        }

        MedicalServiceOrder medicalServiceOrder = new MedicalServiceOrder();
        medicalServiceOrder.setPaymentStatus(PaymentStatus.PENDING);
        MedicalServiceOrder savedServiceOrder = medicalServiceOrderRepository.save(medicalServiceOrder);

        PreclinicalRecord prec = new PreclinicalRecord();
        prec.setMedicalRecord(medicalRecord);
        prec.setMedicalServiceOrder(savedServiceOrder);
        preclinicalRecordRepository.save(prec);

        List<MedicalServiceOrderDetail> details = new ArrayList<>();
        preclinicalDetails.forEach(rec -> {
            validatePreclinicalDetailDoctor(rec);
            MedicalServiceOrderDetail d = new MedicalServiceOrderDetail();

            MedicalService medicalService = medicalServiceRepository.findByIdAndIsDeletedIsFalse(rec.getServiceId()).orElseThrow(
                    () -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                            "Can not find medical service with id = " + rec.getServiceId()));
            Integer establishmentId = rec.getEstablishmentId() == null ? -1 : rec.getEstablishmentId();
            Establishment establishment = establishmentRepository.findById(establishmentId).orElse(null);

            d.setSequenceNumber(getSequenceNumber(establishmentId, rec.getWorkTimeId()));
            d.setAppointmentDate(rec.getAppointmentDate());
            d.setEstablishment(establishment);
            d.setStatus(MedicalServiceOrderStatus.REQUEST_SENT);
            d.setMedicalService(medicalService);
            d.setMedicalServiceOrder(savedServiceOrder);
            details.add(d);
        });

        medicalServiceOrderDetailRepository.saveAll(details);

        // Send notification
        Account doctor = medicalRecord.getExamineRecord().
                getMedicalServiceOrder().
                getMedicalServiceOrderDetails().
                stream().filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_MEDICAL).
                findFirst().
                map(MedicalServiceOrderDetail::getHandleBy).
                orElse(null);
        notificationService.notificationPreclinicalPayment(medicalRecord, doctor);

        return "OK";
    }

    @Override
    @Transactional
    public String confirmPrescriptionByTreatmentId(Establishment est, Integer treatmentId) {
        MedicalRecord medicalRecord = getMedicalRecord(est, treatmentId);

        PrescriptionRecord prescriptionRecord =
                prescriptionRecordRepository.findByMedicalRecord_Id(medicalRecord.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DATA_INVALID,"Can not find prescription record."));

        prescriptionRecord.setIsConfirmed(Boolean.TRUE);
        prescriptionRecordRepository.save(prescriptionRecord);
        return "OK";
    }

    @Override
    @Transactional
    public String updatePrescriptionByTreatmentId(Establishment est, Integer treatmentId, PrescriptionCreateDTO dto) {
        MedicalRecord medicalRecord = getMedicalRecord(est, treatmentId);

        prescriptionRecordRepository.findByMedicalRecord_Id(medicalRecord.getId()).
                ifPresent(item -> {
                    throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST, "Prescription Record already exist ");
                });

        medicalRecord.getExamineRecord().setRemind(dto.getRemind());
        examineRecordRepository.saveAndFlush(medicalRecord.getExamineRecord());


        updateTreatmentRequestStatus(est.getId(), treatmentId, MedicalServiceOrderStatus.REQUEST_FULFILLED);
        if (dto.getDetails().size() == 0) {
            return "OK";
        }

        // Save medical service order of prescription record
        MedicalServiceOrder medicalServiceOrder = new MedicalServiceOrder();
        medicalServiceOrder.setPaymentStatus(PaymentStatus.PENDING);
        MedicalServiceOrder savedServiceOrder = medicalServiceOrderRepository.save(medicalServiceOrder);

        // Save prescription record
        PrescriptionRecord pres = new PrescriptionRecord();
        pres.setMedicalRecord(medicalRecord);
        pres.setMedicalServiceOrder(savedServiceOrder);
        PrescriptionRecord savedPres = prescriptionRecordRepository.save(pres);


        List<PrescriptionDetail> records = new ArrayList<>();
        List<PrescriptionRecordDetailDoctorDTO> detailParams = dto.getDetails();
        detailParams.forEach(rec -> {

            MedicalServiceOrderDetail savedOrderDetail =
                    saveMedicalServiceOrderDetail(rec.getEstablishmentId(), 3, savedServiceOrder);

            DrugUnit drugUnit = drugUnitRepository.findById(rec.getDrugUnitId()).orElseThrow(() ->
                    new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                            "Can not find drug unit with id = " + rec.getDrugUnitId()));
            PrescriptionDetail detail = new PrescriptionDetail();
            detail.setPrescriptionRecord(savedPres);
            detail.setUsage(rec.getUsage());
            detail.setNote(rec.getNote());
            detail.setAmount(rec.getAmount());
            detail.setDrugUnit(drugUnit);
            detail.setMedicalServiceOrderDetail(savedOrderDetail);
            records.add(detail);
        });
        prescriptionDetailRepository.saveAll(records);

        // Notification
        Account doctor = medicalRecord.getExamineRecord().
                getMedicalServiceOrder().
                getMedicalServiceOrderDetails().
                stream().filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_MEDICAL).
                findFirst().
                map(MedicalServiceOrderDetail::getHandleBy).
                orElse(null);
        notificationService.notificationPrescriptionPayment(medicalRecord, doctor);

        return "OK";
    }

    @Override
    @Transactional
    public String deletePrescriptionRecord(Establishment est, Integer treatmentId) {
        MedicalRecord medicalRecord = getMedicalRecord(est, treatmentId);
        PrescriptionRecord prescriptionRecord = prescriptionRecordRepository.findByMedicalRecord_Id(medicalRecord.getId()).orElseThrow(
                () -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Can not find presciption record id = " + treatmentId)
        );

        if ((prescriptionRecord.getIsConfirmed() != null &&
                prescriptionRecord.getIsConfirmed() == Boolean.TRUE) ||
                Helper.isOver3Minutes(prescriptionRecord.getCreateAt())) {
            throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST, "Is over three minutes, can not cancel this prescription");
        }

        medicalRecord.getExamineRecord().setRemind("");
        examineRecordRepository.saveAndFlush(medicalRecord.getExamineRecord());

        prescriptionDetailRepository.deleteAll(prescriptionRecord.getPrescriptionDetails());
        prescriptionRecordRepository.deleteById(prescriptionRecord.getId());
        medicalServiceOrderDetailRepository.deleteAll(prescriptionRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails());
        medicalServiceOrderRepository.deleteById(prescriptionRecord.getMedicalServiceOrder().getId());

        updateTreatmentRequestStatus(est.getId(), treatmentId, MedicalServiceOrderStatus.REQUEST_PROCESSING);

        return "OK";
    }

    /**
     * Get medical record.
     *
     * @param est       establishment
     * @param detailsId detail id
     * @return medical record
     */
    private MedicalRecord getMedicalRecord(Establishment est, Integer detailsId) {
        MedicalServiceOrderDetail detail = medicalServiceOrderDetailRepository.findByEstablishment_IdAndId(est.getId(), detailsId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Cant not found order detail with Id = " + detailsId));

        MedicalRecord medicalRecord;
        if (detail.getMedicalServiceOrder().getExamineRecord() != null) {
            medicalRecord = detail.getMedicalServiceOrder().getExamineRecord().getMedicalRecord();
            return medicalRecord;
        } else {
            throw new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST);
        }
    }

    private TreatmentRequestDTO convertDetailOrderToTreatmentRequest(MedicalServiceOrderDetail detail) {
        TreatmentRequestDTO treatmentRequestDTO = new TreatmentRequestDTO();
        treatmentRequestDTO.setId(detail.getId());

        TreatmentType treatmentType = TreatmentType.CLINIC;

        MedicalRecord medicalRecord;
        if (detail.getMedicalServiceOrder().getExamineRecord() != null) {
            medicalRecord = detail.getMedicalServiceOrder().getExamineRecord().getMedicalRecord();
        } else {
            throw new MedicException(MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST);
        }

//        if (medicalServiceOrderDetailRepository.
//                existsMedicalServiceOrderDetailByMedicalService_IdAndMedicalServiceOrder_Id(2, detail.getMedicalServiceOrder().getId())) {
//            treatmentType = TreatmentType.HOME;
//        }

        List<MedicalServiceOrderDetail> serviceOrderDetails = detail.getMedicalServiceOrder().getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
        MedicalServiceOrderDetail transportService = serviceOrderDetails.stream().
                filter(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_TRANSPORT).
                findFirst().
                orElse(null);
        if (null != transportService) {
            treatmentType = TreatmentType.HOME;
            treatmentRequestDTO.setNotificationTransport(Helper.isNotificationTransport(transportService));
        }

        TreatmentRequestStatus requestStatus = getTreatmentRequestStatusByMedicalId(detail, medicalRecord.getId());

        Patient patientOrder = medicalRecord.getPatient();
        AccountNameDTO handleBy = new AccountNameDTO();
        handleBy.setId(detail.getHandleBy().getId());
        handleBy.setName(detail.getHandleBy().getName());

        treatmentRequestDTO.setId(detail.getId());
        treatmentRequestDTO.setMedicalRecordId(medicalRecord.getId());
        treatmentRequestDTO.setAppointmentDate(detail.getAppointmentDate().toString());
        treatmentRequestDTO.setProcessDate(detail.getProccessDate() != null ? detail.getProccessDate().toString() : "");
        treatmentRequestDTO.setResultDate(detail.getResultDate() != null ? detail.getResultDate().toString() : "");

        Map<String, Integer> mapSequence = Helper.getSequenceNumber(detail.getSequenceNumber());
        if (!CollectionUtils.isEmpty(mapSequence) && mapSequence.get(Constants.SEQUENCE_NUMBER) > 0) {
            treatmentRequestDTO.setSequenceNumber(mapSequence.get(Constants.SEQUENCE_NUMBER));
        }
        treatmentRequestDTO.setHandleBy(handleBy);
        treatmentRequestDTO.setTreatmentType(treatmentType);
        treatmentRequestDTO.setPatientId(patientOrder.getId());
        treatmentRequestDTO.setPatientName(patientOrder.getName());
        treatmentRequestDTO.setPatientAge(Helper.getCurrentAge(patientOrder.getDateOfBirth()));
        treatmentRequestDTO.setAvatarUrl(patientOrder.getAccount().getAvatarUrl());
        treatmentRequestDTO.setDepartmentName(detail.getEstablishment().getMedicalDepartment().getName());
        treatmentRequestDTO.setDepartmentId(detail.getEstablishment().getMedicalDepartment().getId());
        treatmentRequestDTO.setTreatmentRequestStatus(requestStatus);
        return treatmentRequestDTO;
    }


    private TreatmentRequestStatus getTreatmentRequestStatusByMedicalId(MedicalServiceOrderDetail detail, Integer id) {

        TreatmentRequestStatus status = TreatmentRequestStatus.WAITING_TREAT;

        if (detail.getStatus().equals(MedicalServiceOrderStatus.REQUEST_DENIED)) {
            return TreatmentRequestStatus.DENIED;
        }

        if (detail.getStatus().equals(MedicalServiceOrderStatus.REQUEST_PROCESSING)) {
            status = TreatmentRequestStatus.WAITING_TEST;
        }
        // Check status preclinical and
        Optional<PreclinicalRecord> preclinicalRecord = preclinicalRecordRepository.findByMedicalRecord_Id(id);
        if (preclinicalRecord.isPresent()) {
            List<MedicalServiceOrderDetail> serviceOrderDetails = medicalServiceOrderDetailRepository.findAllByMedicalServiceOrder_IdAndStatusIn(preclinicalRecord.get().getMedicalServiceOrder().getId(),
                    MedicalServiceOrderStatus.REQUEST_SENT,
//                    MedicalServiceOrderStatus.REQUEST_CANCELLED,
//                    MedicalServiceOrderStatus.REQUEST_DENIED,
                    MedicalServiceOrderStatus.REQUEST_ACCEPTED,
                    MedicalServiceOrderStatus.REQUEST_PROCESSING
            );
            serviceOrderDetails = serviceOrderDetails.stream().filter(Helper::checkPreclinicalOrderDetailIsService).collect(Collectors.toList());
            if (serviceOrderDetails.size() == 0) {
                status = TreatmentRequestStatus.WAITING_DIAGNOSE;
            }
        }

        if (detail.getMedicalServiceOrder().getExamineRecord().getSkipPreclinical() != null &&
                detail.getMedicalServiceOrder().getExamineRecord().getSkipPreclinical().equals(Boolean.TRUE)) {
            status = TreatmentRequestStatus.WAITING_DIAGNOSE;
        }

        if (org.apache.commons.lang3.StringUtils.isNotEmpty(detail.getMedicalServiceOrder().getExamineRecord().getDiagnose())) {
            status = TreatmentRequestStatus.WAITING_DIAGNOSE;
        }

        if (detail.getStatus().equals(MedicalServiceOrderStatus.REQUEST_FULFILLED)) {
            status = TreatmentRequestStatus.COMPLETE;
        }

        return status;
    }

    private void updateTreatmentRequestStatus(Integer estId, Integer detailId, MedicalServiceOrderStatus status) {
        MedicalServiceOrderDetail detail = medicalServiceOrderDetailRepository.findByEstablishment_IdAndId(estId, detailId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Cant not found order detail with Id = " + detailId));

        // set process date if null
        if (null == detail.getProccessDate()) {
            detail.setProccessDate(LocalDateTime.now());
        }

        // set result date if status equal fulfilled
        if (status.equals(MedicalServiceOrderStatus.REQUEST_FULFILLED)) {
            detail.setResultDate(LocalDateTime.now());
        } else {
            detail.setResultDate(null);
        }

        detail.setStatus(status);
        medicalServiceOrderDetailRepository.save(detail);

    }

    private MedicalServiceOrderDetail saveMedicalServiceOrderDetail(Integer estId,
                                                                    Integer medicalServiceId,
                                                                    MedicalServiceOrder serviceOrder) {

        if (!establishmentRepository.isValidEstablishmentFollowConditions(estId,
                EstablishmentType.DRUG_STORE.getCode(), null, medicalServiceId)) {
            throw new MedicException(MedicException.ERROR_DATA_INVALID,
                    "Establishment is invalid with id = " + estId);
        }

        MedicalServiceOrderDetail d = new MedicalServiceOrderDetail();
        MedicalService medicalService = medicalServiceRepository.findByIdAndIsDeletedIsFalse(medicalServiceId).orElseThrow(
                () -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                        "Can not find medical service with id = " + medicalServiceId));

        Establishment establishment = establishmentRepository.findById(estId).orElseThrow(
                () -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                        "Can not find establishment with id = " + estId));

        d.setStatus(MedicalServiceOrderStatus.REQUEST_SENT);
        d.setEstablishment(establishment);
        d.setMedicalService(medicalService);
        d.setMedicalServiceOrder(serviceOrder);

        return medicalServiceOrderDetailRepository.saveAndFlush(d);
    }

    @Override
    @Transactional
    public boolean notificationTreatmentTransportTime(int treatmentId, Establishment currentEstablishment) {
        boolean result = false;
        MedicalServiceOrderDetail detail = medicalServiceOrderDetailRepository.findByEstablishment_IdAndId(currentEstablishment.getId(), treatmentId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Cant not found order detail with Id = " + treatmentId));

        List<MedicalServiceOrderDetail> serviceOrderDetails = detail.getMedicalServiceOrder().getMedicalServiceOrderDetails();
        MedicalServiceOrderDetail transportService = serviceOrderDetails.stream().
                filter(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_TRANSPORT).
                findFirst().
                orElse(null);

        MedicalRecord medicalRecord = detail.getMedicalServiceOrder().getExamineRecord().getMedicalRecord();

        if (null != medicalRecord && null != transportService && MedicalServiceOrderStatus.REQUEST_ACCEPTED.equals(transportService.getStatus())) {
            // Update status
            transportService.setStatus(MedicalServiceOrderStatus.REQUEST_PROCESSING);
            medicalServiceOrderDetailRepository.save(transportService);
            Account patient = medicalRecord.getPatient().getAccount();
            Account doctor = detail.getHandleBy();
            notificationService.notificationTreatmentTransport(doctor, patient, medicalRecord.getId());
            result = true;
        }
        return result;
    }
}
