package com.mmedic.rest.drug.mapper;

import com.mmedic.entity.DrugUnit;
import com.mmedic.rest.drug.dto.DrugUnitDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface DrugUnitMapper {

    @Mapping(target = "prices", source = "drugUnitPriceHistories")
    DrugUnitDTO convertToDTO(DrugUnit entity);

    DrugUnit convertToEntity(DrugUnitDTO entity);

}
