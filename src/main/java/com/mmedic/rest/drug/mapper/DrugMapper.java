package com.mmedic.rest.drug.mapper;

import com.mmedic.entity.Drug;
import com.mmedic.entity.DrugUnit;
import com.mmedic.rest.drug.dto.AllergyDrugDTO;
import com.mmedic.rest.drug.dto.DrugDTO;
import com.mmedic.rest.drug.dto.DrugUnitDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface DrugMapper {

    @Mapping(target = "units", source = "drugUnits")
    DrugDTO convertToDTO(Drug entity);

    Drug convertToEntity(DrugDTO dto);

    List<DrugDTO> toListDrugDTO(List<Drug> drugs);

    @Mapping(target = "drugId", source = "drug.id")
    DrugUnitDTO toDrugUnitDTO(DrugUnit drugUnit);

    List<DrugUnitDTO> toListDrugUnitDTO(List<DrugUnit> drugUnits);

    AllergyDrugDTO convertToAllergyDTO(Drug entity);

}
