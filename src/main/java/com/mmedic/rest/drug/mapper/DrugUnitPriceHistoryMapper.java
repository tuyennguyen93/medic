package com.mmedic.rest.drug.mapper;

import com.mmedic.entity.DrugUnitPriceHistory;
import com.mmedic.rest.drug.dto.DrugUnitPriceDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public interface DrugUnitPriceHistoryMapper {

    DrugUnitPriceDTO convertToDTO(DrugUnitPriceHistory entity);

    DrugUnitPriceHistory convertToEntity(DrugUnitPriceDTO entity);

}
