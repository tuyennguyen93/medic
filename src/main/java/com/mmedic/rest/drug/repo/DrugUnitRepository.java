package com.mmedic.rest.drug.repo;

import com.mmedic.entity.DrugUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface DrugUnitRepository extends JpaRepository<DrugUnit, Integer> {

    List<DrugUnit> findByDrug_Id(Integer drugId);

    @Query("SELECT du FROM DrugUnit du " +
            "LEFT JOIN FETCH du.drug d " +
            "WHERE du.id IN (:ids)")
    List<DrugUnit> findAllByIdIn(@Param("ids") List<Integer> ids);


    @Query("SELECT du FROM DrugUnit du " +
            "LEFT JOIN FETCH du.drugUnitPriceHistories p " +
            "WHERE du.id = :unitId ")
    Optional<DrugUnit> findDrugUnitWithPriceById(@Param("unitId") Integer unitId);


    Integer countByParentId (Integer parentId);
}
