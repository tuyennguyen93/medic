package com.mmedic.rest.drug.repo;

import com.mmedic.entity.DrugUnitPriceHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DrugUnitPriceHistoryRepository extends JpaRepository<DrugUnitPriceHistory, Integer> {

    List<DrugUnitPriceHistory> findByDrugUnit_Id(Integer id);
}
