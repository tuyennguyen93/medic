package com.mmedic.rest.drug.repo;

import com.mmedic.entity.Drug;
import com.mmedic.rest.drug.dto.DrugCurrentPriceDTO;
import com.mmedic.rest.drug.dto.DrugShortInfoDTO;
import com.mmedic.rest.drug.dto.PrescriptionNotificationDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface DrugRepository extends JpaRepository<Drug, Integer> {

    List<Drug> findByNameContainsAndIsDisabledIsFalse(String name);

    Optional<Drug> findByIdAndIsDisabledIsFalse(Integer id);

    List<Drug> findByPathStartingWithAndIsDisabledIsFalse(String path);

    List<Drug> findByParentIdAndIsDisabledIsFalse(Integer parentId);

    List<Drug> findByIsDisabledIsFalse();


    @Query(nativeQuery = true, value = "" +
            "SELECT      d.id               AS id,  " +
            "            d.name             AS name,  " +
            "            d.can_prescribe    AS canPrescribe,  " +
            "            d.parent_id        AS parentId,  " +
            "            d.path             AS path,  " +
            "            CONCAT(((SELECT COUNT(a.id) FROM drug a where a.is_disabled = 0 AND a.parent_id = d.id)>0),'') AS hasChild " +
            "            FROM drug as d  " +
            "            WHERE d.is_disabled = 0 " +
            "AND (( :parentId IS NULL AND d.parent_id IS NULL )  OR d.parent_id = :parentId ) ")
    List<DrugShortInfoDTO> findAllDrugShortInfo(@Param("parentId") Integer id);


    @Query(nativeQuery = true, value = "" +
            "           SELECT      " +
            "               d.id               AS id, " +
            "               d.name             AS name, " +
            "               d.can_prescribe    AS canPrescribe, " +
            "               d.parent_id        AS parentId, " +
            "               d.path             AS path, " +
            "               CONCAT(((SELECT COUNT(a.id) FROM drug a where a.is_disabled = 0 AND  a.parent_id = d.id)>0),'') AS hasChild " +
            "            FROM drug as d " +
            "            WHERE d.is_disabled = 0 " +
            "                 AND ( d.id IN     (SELECT DISTINCT " +
            "                                    (CASE " +
            "                                       WHEN ISNULL(path) THEN id " +
            "                                       WHEN TRIM(path) = '' THEN id " +
            "                                       ELSE SUBSTRING_INDEX(path,'.',1) END) AS drugId " +
            "                                   FROM drug where drug.name like :query ) );")
    List<DrugShortInfoDTO> findParentDrugShortInfoFollowQuery(@Param("query") String query);

    @Query(nativeQuery = true, value = "" +
            "           SELECT      " +
            "               d.id               AS id, " +
            "               d.name             AS name, " +
            "               d.can_prescribe    AS canPrescribe, " +
            "               d.parent_id        AS parentId, " +
            "               d.path             AS path, " +
            "               CONCAT(((SELECT COUNT(a.id) FROM drug a where  a.is_disabled = 0 AND  a.parent_id = d.id)>0),'') AS hasChild " +
            "           FROM drug as d " +
            "           WHERE d.is_disabled = 0 " +
            "                 AND  d.name LIKE :query  ")
    List<DrugShortInfoDTO> findDrugShortInfoFollowQuery(@Param("query") String query);

    @Query(nativeQuery = true, value = "" +
            "           SELECT      " +
            "               d.id               AS id, " +
            "               d.name             AS name, " +
            "               d.can_prescribe    AS canPrescribe, " +
            "               d.parent_id        AS parentId, " +
            "               d.path             AS path, " +
            "               CONCAT(((SELECT COUNT(a.id) FROM drug a where  a.is_disabled = 0 AND  a.parent_id = d.id)>0),'') AS hasChild " +
            "           FROM drug as d " +
            "           WHERE d.is_disabled = 0 " +
            "                 AND  d.id IN :drugIds ")
    List<DrugShortInfoDTO> findDrugShortInfoFollowId(@Param("drugIds") List<Integer> drugIds);

    /**
     * Find all drug base on parent id.
     *
     * @param parentId parent id
     * @param path     path
     * @param pathLike path like condition
     * @return
     */
    @Query("SELECT DISTINCT drug FROM Drug drug " +
            "LEFT JOIN FETCH drug.drugUnits du " +
            "WHERE drug.isDisabled = 0 " +
            "AND (drug.id = :parentId " +
            "   OR drug.path = :path " +
            "   OR drug.path LIKE :pathLike%) " +
            "ORDER BY drug.id")
    List<Drug> findAllDrugByParentId(@Param("parentId") int parentId, @Param("path") String path, @Param("pathLike") String pathLike);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT data.id           AS id, " +
                    "       data.drugUnitId   AS drugUnitId, " +
                    "       data.pricePerUnit AS pricePerUnit " +
                    "FROM ( " +
                    "         SELECT DISTINCT dr.id               AS id, " +
                    "                         duph.price_per_unit AS pricePerUnit, " +
                    "                         du.id               AS drugUnitId, " +
                    "                         duph.effective_at   AS effectiveAt " +
                    "         FROM drug_unit du " +
                    "                  LEFT JOIN drug_unit_price_history duph ON du.id = duph.drug_unit_id " +
                    "                  LEFT JOIN drug dr ON du.drug_id = dr.id " +
                    "         WHERE dr.id = :parentId " +
                    "            OR dr.path = :parentId " +
                    "            OR dr.path LIKE :pathLike " +
                    "     ) AS data " +
                    "         INNER JOIN " +
                    "     ( " +
                    "         SELECT drug_unit_id, " +
                    "                MAX(effective_at) AS max_eff " +
                    "         FROM drug_unit_price_history " +
                    "         WHERE effective_at <= :effDate " +
                    "         GROUP BY drug_unit_id " +
                    "     ) AS price " +
                    "     ON data.drugUnitId = price.drug_unit_id AND data.effectiveAt = price.max_eff")
    List<DrugCurrentPriceDTO> findDrugCurrentPricePerUnit(@Param("parentId") int parentId, @Param("pathLike") String pathLike, @Param("effDate") LocalDateTime effDate);

    @Query(nativeQuery = true,
            value = "SELECT data.id           AS id, " +
                    "       data.drugUnitId   AS drugUnitId, " +
                    "       data.pricePerUnit AS pricePerUnit " +
                    "FROM ( " +
                    "         SELECT DISTINCT dr.id               AS id, " +
                    "                         duph.price_per_unit AS pricePerUnit, " +
                    "                         du.id               AS drugUnitId, " +
                    "                         duph.effective_at   AS effectiveAt " +
                    "         FROM drug_unit du " +
                    "                  LEFT JOIN drug_unit_price_history duph ON du.id = duph.drug_unit_id " +
                    "                  LEFT JOIN drug dr ON du.drug_id = dr.id " +
                    "         WHERE dr.id IN (:ids) " +
                    "     ) AS data " +
                    "         INNER JOIN " +
                    "     ( " +
                    "         SELECT drug_unit_id, " +
                    "                MAX(effective_at) AS max_eff " +
                    "         FROM drug_unit_price_history " +
                    "         WHERE effective_at <= :effDate " +
                    "         GROUP BY drug_unit_id " +
                    "     ) AS price " +
                    "     ON data.drugUnitId = price.drug_unit_id AND data.effectiveAt = price.max_eff")
    List<DrugCurrentPriceDTO> findDrugCurrentPricePerUnitInIds(@Param("ids") List<Integer> ids, @Param("effDate") LocalDateTime effDate);

    /**
     * Find drug can prescribe.
     *
     * @param name condition to search
     * @return list drug.
     */
    List<Drug> findByNameContainsAndCanPrescribeTrueAndIsDisabledIsFalse(String name);

    /**
     * Get drug can prescribe by ids.
     *
     * @param drugIds list drug id
     * @return list drug
     */
    List<Drug> findAllByIdInAndCanPrescribeTrueAndIsDisabledIsFalse(List<Integer> drugIds);

    /**
     * Find all prescription not payment to notification price is changed.
     *
     * @param drugUnitId drug unit id
     * @return list prescription and patient detail
     */
    @Query(nativeQuery = true,
            value = "SELECT DISTINCT mr.id                AS medicalRecordId, " +
                    "                p.id                 AS patientId, " +
                    "                p.name               AS patientName, " +
                    "                a.notification_token AS notificationToken, " +
                    "                a.language           AS language, " +
                    "                d.id                 AS drugId," +
                    "                d.name               AS drugName " +
                    "FROM prescription_record pr " +
                    "         LEFT JOIN prescription_detail pd ON pr.id = pd.prescription_record_id " +
                    "         LEFT JOIN medical_record mr ON pr.medical_record_id = mr.id " +
                    "         LEFT JOIN patient p ON mr.patient_id = p.id " +
                    "         LEFT JOIN account a ON p.account_id = a.id " +
                    "         LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "         LEFT JOIN drug_unit du ON pd.drug_unit_id = du.id " +
                    "         LEFT JOIN drug d ON du.drug_id = d.id " +
                    "WHERE mso.payment_status = 'PENDING' " +
                    "  AND pd.drug_unit_id = :drugUnitId")
    List<PrescriptionNotificationDTO> findPrescriptionPriceChange(@Param("drugUnitId") int drugUnitId);

}
