package com.mmedic.rest.drug;

import com.mmedic.entity.*;
import com.mmedic.enums.DeleteStatus;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.rest.doctor.dto.PrescriptionCreateDTO;
import com.mmedic.rest.drug.dto.*;
import com.mmedic.rest.drug.mapper.DrugMapper;
import com.mmedic.rest.drug.mapper.DrugUnitMapper;
import com.mmedic.rest.drug.mapper.DrugUnitPriceHistoryMapper;
import com.mmedic.rest.drug.repo.DrugRepository;
import com.mmedic.rest.drug.repo.DrugUnitPriceHistoryRepository;
import com.mmedic.rest.drug.repo.DrugUnitRepository;
import com.mmedic.rest.drugstorage.dto.DrugInfoExportDTO;
import com.mmedic.rest.drugstorage.repo.DrugExportDetailRepository;
import com.mmedic.rest.drugstorage.repo.DrugImportDetailRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.healthFacility.dto.DrugMedicineBatchDTO;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDoctorDTO;
import com.mmedic.rest.medicalRecord.repo.PrescriptionDetailRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Drug service.
 */
@RequiredArgsConstructor
@Service
public class DrugService {

    private final DrugRepository drugRepository;

    private final DrugUnitRepository drugUnitRepository;

    private final DrugUnitPriceHistoryRepository drugUnitPriceHistoryRepository;

    private final DrugMapper drugMapper;

    private final DrugUnitMapper drugUnitMapper;

    private final DrugUnitPriceHistoryMapper drugUnitPriceHistoryMapper;

    private final DrugImportDetailRepository drugImportDetailRepository;

    private final DrugExportDetailRepository drugExportDetailRepository;

    private final PrescriptionDetailRepository prescriptionDetailRepository;

    private final EstablishmentRepository establishmentRepository;

    private final NotificationService notificationService;

    private DrugDTO getDrugInfoDto(Drug drug) {
        List<DrugUnit> drugUnitList = drug.getDrugUnits();
        List<DrugUnitDTO> drugUnitDTOList = new ArrayList<>();
        drugUnitList.forEach(unit -> {
            DrugUnitDTO drugUnitDTO = drugUnitMapper.convertToDTO(unit);

            List<DrugUnitPriceDTO> priceDTOList = new ArrayList<>();
            List<DrugUnitPriceHistory> priceHistories = unit.getDrugUnitPriceHistories();

            priceHistories.forEach(price -> priceDTOList.add(drugUnitPriceHistoryMapper.convertToDTO(price)));
            drugUnitDTO.setPrices(priceDTOList);
            drugUnitDTOList.add(drugUnitDTO);
        });
        DrugDTO drugDTO = drugMapper.convertToDTO(drug);
        drugDTO.setUnits(drugUnitDTOList);
        return drugDTO;
    }

    @Transactional
    public List<DrugShortInfoDTO> getDrugsShortInfo(String query, Integer id) {
        List<DrugShortInfoDTO> drugShortInfoDTOList = new ArrayList<>();

        if (StringUtils.isNotEmpty(query)){
            String querySql = Helper.convertQueryParamToSql(query);
            drugShortInfoDTOList = drugRepository.
                    findDrugShortInfoFollowQuery(querySql);

            List<Integer> allDrugIds = new ArrayList<>();
            drugShortInfoDTOList.forEach(rec -> {
                if (StringUtils.isNotBlank(rec.getPath())){
                    List<Integer> pathDrugId = Arrays.stream(rec.getPath().split("\\."))
                            .map(Integer::parseInt)
                            .collect(Collectors.toList());
                    allDrugIds.addAll(pathDrugId);
                }
            });
            drugShortInfoDTOList.forEach(rec -> allDrugIds.removeAll(Arrays.asList(rec.getId())));
            if (allDrugIds.size() > 0) {
                drugShortInfoDTOList.addAll(drugRepository.findDrugShortInfoFollowId(allDrugIds));
            }
        }
        else {
            drugShortInfoDTOList = drugRepository.findAllDrugShortInfo(id);
        }

        return drugShortInfoDTOList;
    }

    @Transactional
    public DrugDTO getInfoDrugFollowId(Integer drugId) {
        Drug drug = drugRepository.findByIdAndIsDisabledIsFalse(drugId).orElseThrow(() ->
                new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Can not find drugId = " + drugId));

        return getDrugInfoDto(drug);
    }

    @Transactional
    public List<DrugDTO> getDrugsFollowParent(Integer parentId) {
        List<Drug> drugList = drugRepository.findByParentIdAndIsDisabledIsFalse(parentId);

        List<DrugDTO> drugDTOList = new ArrayList<>();
        drugList.forEach(r -> drugDTOList.add(getDrugInfoDto(r)));
        return drugDTOList;
    }

    @Transactional
    public List<DrugDTO> getAllAvailableDrug(boolean hasPrice) {
        List<Drug> drugList = drugRepository.findByIsDisabledIsFalse();
        if (hasPrice) {
            drugList = drugList.stream().filter(this::checkDrugHasPrice).collect(Collectors.toList());
        }
        return drugList.stream().map(this::getDrugInfoDto).collect(Collectors.toList());
    }

    /**
     * Check drug has price.
     *
     * @param drug the drug
     * @return result
     */
    private boolean checkDrugHasPrice(Drug drug) {
        boolean result = false;
        if (null != drug && !CollectionUtils.isEmpty(drug.getDrugUnits())) {
            result = drug.getDrugUnits().stream().anyMatch(drugUnit -> !CollectionUtils.isEmpty(drugUnit.getDrugUnitPriceHistories()));
        }
        return result;
    }

    @Transactional
    public DrugDTO createDrug(DrugDTO reqDto) {
        String path = getPathDrug(reqDto.getParentId());
        Drug drug = drugMapper.convertToEntity(reqDto);
        drug.setIsDisabled(DeleteStatus.AVAILABLE.getValue());
        drug.setPath(path);
        Drug savedDrug = drugRepository.save(drug);
        return drugMapper.convertToDTO(savedDrug);
    }

    @Transactional
    public DrugDTO updateDrug(DrugDTO reqDto) {

        Drug drug = drugRepository.findByIdAndIsDisabledIsFalse(reqDto.getId()).
                orElseThrow(() ->
                        new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                                "Drug not found : " + reqDto.getId()));

        String path = getPathDrug(reqDto.getParentId());

        Drug inputDrug = drugMapper.convertToEntity(reqDto);
        inputDrug.setCreateAt(drug.getCreateAt());
        inputDrug.setIsDisabled(drug.getIsDisabled());
        inputDrug.setUpdateAt(LocalDateTime.now());
        inputDrug.setPath(path);
        Drug savedDrug = drugRepository.save(inputDrug);
        return drugMapper.convertToDTO(savedDrug);
    }

    @Transactional
    public DrugDTO deleteDrug(Integer id) {

        LocalDateTime currentTime = LocalDateTime.now();

        // Delete current
        Drug drug = drugRepository.findByIdAndIsDisabledIsFalse(id).
                orElseThrow(() ->
                        new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                                "Drug not found : " + id));

        drug.setIsDisabled(DeleteStatus.IS_DELETED.getValue());
        drug.setUpdateAt(currentTime);
        Drug savedDrug = drugRepository.save(drug);
        // End

        // Delete child
        String path = createPath(drug.getPath(), String.valueOf(drug.getId()));
        List<Drug> drugList = drugRepository.findByPathStartingWithAndIsDisabledIsFalse(path);

        drugList.forEach(r -> {
            r.setIsDisabled(DeleteStatus.IS_DELETED.getValue());
            r.setUpdateAt(currentTime);
        });

        drugRepository.saveAll(drugList);
        // End

        return drugMapper.convertToDTO(savedDrug);
    }

    @Transactional
    public List<DrugUnitDTO> getAllAvailableUnitFollowDrug(Integer drugId) {
        List<DrugUnit> drugUnitList = drugUnitRepository.findByDrug_Id(drugId);
        List<DrugUnitDTO> drugUnitDTOList = new ArrayList<>();
        drugUnitList.forEach(r -> drugUnitDTOList.add(drugUnitMapper.convertToDTO(r)));
        return drugUnitDTOList;
    }

    @Transactional
    public DrugUnitDTO createDrugUnit(DrugUnitDTO reqDto) {
        String path = getPathDrugUnit(reqDto.getParentId());

        DrugUnit drugUnit = drugUnitMapper.convertToEntity(reqDto);
        Drug drug = drugRepository.findByIdAndIsDisabledIsFalse(reqDto.getDrugId()).
                orElseThrow(() ->
                        new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                                "Drug unit not found : " + reqDto.getDrugId()));

        if (!drug.isCanPrescribe()){
            throw new MedicException(MedicException.ERROR_DATA_INVALID,
                    "Can not set unit for drug which can not prescribe.");
        }

        drugUnit.setDrug(drug);
        drugUnit.setPath(path);
        DrugUnit savedDrugUnit = drugUnitRepository.saveAndFlush(drugUnit);
        reqDto.setId(savedDrugUnit.getId()); // set Id for drug unit
        //Set price for drug unit
        DrugUnitPriceHistory price = createDrugPriceInDrugUnit(reqDto, false);
        savedDrugUnit.setDrugUnitPriceHistories(new ArrayList<>());
        savedDrugUnit.getDrugUnitPriceHistories().add(price);
        //End
        return drugUnitMapper.convertToDTO(savedDrugUnit);
    }


    @Transactional
    public DrugUnitDTO updateDrugUnit(DrugUnitDTO reqDto) {
        DrugUnit drugUnit = drugUnitRepository.findDrugUnitWithPriceById(reqDto.getId()).
                orElseThrow(() ->
                        new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                                "Drug unit not found : " + reqDto.getId()));

//        String path = getPathDrugUnit(reqDto.getParentId());

        DrugUnit inputDrugUnit = drugUnitMapper.convertToEntity(reqDto);
        inputDrugUnit.setDrug(drugUnit.getDrug());
        inputDrugUnit.setCreateAt(drugUnit.getCreateAt()); // reset CreateAt at new instance
        inputDrugUnit.setPath(drugUnit.getPath());
        drugUnitRepository.save(inputDrugUnit);
        // Update price
        createDrugPriceInDrugUnit(reqDto, true);
        inputDrugUnit.setDrugUnitPriceHistories(drugUnitPriceHistoryRepository.findByDrugUnit_Id(reqDto.getId()));
        // End
        return drugUnitMapper.convertToDTO(inputDrugUnit);
    }

    @Transactional
    public String deleteDrugUnit(Integer id) {
        DrugUnit drugUnit = drugUnitRepository.findDrugUnitWithPriceById(id).
                orElseThrow(() ->
                        new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                                "Drug unit not found : " + id));

        if (usedUnit(id)) {
            throw new MedicException(MedicException.ERROR_DATA_INVALID, "Drug unit is used, can not delete.");
        }

        drugUnitPriceHistoryRepository.deleteAll(drugUnit.getDrugUnitPriceHistories());
        drugUnitRepository.deleteById(id);

        return "OK";
    }

    private boolean usedUnit(Integer id) {
        if (drugUnitRepository.countByParentId(id) > 0) return true;
        if (prescriptionDetailRepository.countByDrugUnit_Id(id) > 0) return true;
        if (drugImportDetailRepository.countByDrugUnit_Id(id) > 0) return true;
        if (drugExportDetailRepository.countByDrugUnit_Id(id) > 0) return true;
        return false;
    }

    @Transactional
    public List<AllergyDrugDTO> getAllergyDrugList(String query) {
        List<Drug> drugList = StringUtils.isNotBlank(query) ?
                drugRepository.findByNameContainsAndIsDisabledIsFalse(query) :
                drugRepository.findByIsDisabledIsFalse();
        List<AllergyDrugDTO> allergyDrugDTOList = new ArrayList<>();
        drugList.forEach(r -> allergyDrugDTOList.add(drugMapper.convertToAllergyDTO(r)));
        return allergyDrugDTOList;
    }


    private DrugUnitPriceHistory createDrugPriceInDrugUnit(DrugUnitDTO drugUnitDTO, boolean isUpdate) {
        if (CollectionUtils.isEmpty(drugUnitDTO.getPrices())) {
            return null;
        }
        DrugUnitPriceDTO pricePerUnit = drugUnitDTO.getPrices().get(0);
        pricePerUnit.setDrugUnitId(drugUnitDTO.getId());

        pricePerUnit.setEffectiveAt(LocalDateTime.now());

        DrugUnit drugUnit = drugUnitRepository.findById(pricePerUnit.getDrugUnitId()).
                orElseThrow(() ->
                        new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                                "Drug unit not found : " + pricePerUnit.getDrugUnitId()));

        DrugUnitPriceHistory priceHistory = drugUnitPriceHistoryMapper.convertToEntity(pricePerUnit);
        priceHistory.setDrugUnit(drugUnit);

        // Notification
        if (isUpdate) {
            notificationPriceChangeForPatient(drugUnitDTO.getId());
        }
        return drugUnitPriceHistoryRepository.saveAndFlush(priceHistory);
    }

    /**
     * Notification price change for patient.
     *
     * @param drugUnitId drug unit id is change price
     */
    private void notificationPriceChangeForPatient(int drugUnitId) {
        new Thread(() -> {
            List<PrescriptionNotificationDTO> prescriptionNotifications = drugRepository.findPrescriptionPriceChange(drugUnitId);
            if (!CollectionUtils.isEmpty(prescriptionNotifications)) {
                notificationService.notificationDrugUnitPriceChange(prescriptionNotifications);
            }
        }).start();
    }

    private String getPathDrug(Integer parentId) {
        String path = "";
        if (parentId != null) {
            Drug parentDrug = drugRepository.findByIdAndIsDisabledIsFalse(parentId).
                    orElseThrow(() ->
                            new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                                    "Parent drug not found : " + parentId));
            path = createPath(parentDrug.getPath(), parentId.toString());
        }
        return path;
    }

    private String getPathDrugUnit(Integer parentId) {
        String path = "";
        if (parentId != null) {
            DrugUnit parentDrug = drugUnitRepository.findById(parentId).
                    orElseThrow(() ->
                            new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                                    "Parent drug unit not found : " + parentId));
            path = createPath(parentDrug.getPath(), parentId.toString());
        }
        return path;
    }

    private String createPath(String currentPath, String parentId) {
        if (StringUtils.isNotEmpty(currentPath)) {
            return currentPath + "." + parentId;
        }
        return parentId;
    }

    /**
     * Get drug tree of once drug.
     *
     * @param drugId drug id
     * @return list drug
     */
    public List<DrugDTO> getDrugTree(int drugId, boolean hasPrice) {
        Drug drug = drugRepository.findByIdAndIsDisabledIsFalse(drugId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_NOT_EXIST, "Drug don't exist"));

        int parentId = getRootParentId(drug);

        return getTreeOfDrug(parentId, hasPrice);
    }

    /**
     * Get parent of drug base on path and id.
     *
     * @param drug the drug
     * @return the parent id
     */
    private int getRootParentId(Drug drug) {
        int parentDrugId = drug.getId();
        if (!StringUtils.isBlank(drug.getPath())) {
            parentDrugId = Integer.parseInt(drug.getPath().split(Constants.DOT_REGEX)[0]);
        }
        return parentDrugId;
    }

    /**
     * Get tree of drug base on parent id.
     *
     * @param parentId the parent id
     * @return list drug
     */
    private List<DrugDTO> getTreeOfDrug(int parentId, boolean hasPrice) {
        List<DrugDTO> result = new ArrayList<>();
        String path = String.valueOf(parentId);
        String pathLike = path + Constants.DOT;
        String pathLikePercent = pathLike + Constants.PERCENT;
        List<Drug> drugs = drugRepository.findAllDrugByParentId(parentId, path, pathLike);
        List<DrugDTO> drugDTOS = drugMapper.toListDrugDTO(drugs);

        // Get price for per unit
        List<DrugCurrentPriceDTO> drugCurrentPrices = drugRepository.
                findDrugCurrentPricePerUnit(parentId, pathLikePercent, LocalDateTime.now());
        if (!CollectionUtils.isEmpty(drugCurrentPrices)) {
            Map<String, Integer> mapPrices = drugCurrentPrices.stream().
                    collect(Collectors.toMap(drugCurrentPriceDTO -> drugCurrentPriceDTO.getId() + Constants.UNDERSCORE + drugCurrentPriceDTO.getDrugUnitId(),
                            DrugCurrentPriceDTO::getPricePerUnit));

            for (DrugDTO drugDTO : drugDTOS) {
                if (!CollectionUtils.isEmpty(drugDTO.getUnits())) {
                    List<DrugUnitDTO> units = new ArrayList<>();
                    drugDTO.getUnits().forEach(unit -> {
                        String key = unit.getDrugId() + Constants.UNDERSCORE + unit.getId();
                        if (mapPrices.containsKey(key)) {
                            DrugUnitPriceDTO pricePerUnit = new DrugUnitPriceDTO();
                            pricePerUnit.setPricePerUnit(mapPrices.get(key));
                            unit.setPrices(Collections.singletonList(pricePerUnit));
                            units.add(unit);
                        } else if (!hasPrice) {
                            units.add(unit);
                        }
                    });

                    if (!hasPrice || units.size() > 0) {
                        drugDTO.setUnits(units);
                        result.add(drugDTO);
                    }
                } else if (!hasPrice || !drugDTO.isCanPrescribe()) {
                    result.add(drugDTO);
                }
            }
        }
        return result;
    }

    /**
     * Search drug tree by condition value.
     *
     * @param condition the condition value
     * @return list drug
     */
    public List<DrugDTO> searchDrugTree(String condition, boolean hasPrice) {
        List<DrugDTO> drugDTOS = new ArrayList<>();
        List<Drug> drugs = drugRepository.findByNameContainsAndIsDisabledIsFalse(condition);
        if (!CollectionUtils.isEmpty(drugs)) {
            List<Integer> drugParentIds = drugs.stream().
                    map(this::getRootParentId).distinct().collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(drugParentIds)) {
                for (Integer drugParentId : drugParentIds) {
                    List<DrugDTO> lstDrug = getTreeOfDrug(drugParentId, hasPrice);
                    drugDTOS.addAll(lstDrug);
                }
            }
        }
        return drugDTOS;
    }

    /**
     * Search drug can prescribe.
     *
     * @param condition the condition value
     * @return list drug
     */
    public List<DrugDTO> searchDrugCanPrescribe(String condition, boolean hasUnit) {
        List<Drug> drugs = drugRepository.findByNameContainsAndCanPrescribeTrueAndIsDisabledIsFalse(Helper.convertNullToEmpty(condition));
        List<DrugDTO> drugDTOS = drugMapper.toListDrugDTO(drugs);
        if (hasUnit) {
            drugDTOS = drugDTOS.stream().filter(drugDTO -> !CollectionUtils.isEmpty(drugDTO.getUnits())).collect(Collectors.toList());
        }
        return drugDTOS;
    }

    /**
     * Verify amount of medicine batch.
     *
     * @param drugInfoExportDTOs list drug info export
     * @param establishment      the establishment
     * @return true or false
     */
    public boolean verifyAmountDrugOfMedicineBatch(List<DrugInfoExportDTO> drugInfoExportDTOs, Establishment establishment) {
        boolean result = true;
        Map<Integer, List<DrugInfoExportDTO>> mapListDrugInfo = new HashMap<>();
        if (!CollectionUtils.isEmpty(drugInfoExportDTOs)) {
            drugInfoExportDTOs.forEach(drugInfoExportDTO -> {
                List<DrugInfoExportDTO> lstDrugInfo = mapListDrugInfo.get(drugInfoExportDTO.getDrugId());
                if (CollectionUtils.isEmpty(lstDrugInfo)) {
                    lstDrugInfo = new ArrayList<>();
                }
                lstDrugInfo.add(drugInfoExportDTO);
                mapListDrugInfo.put(drugInfoExportDTO.getDrugId(), lstDrugInfo);
            });

            for (Integer key : mapListDrugInfo.keySet()) {
                List<DrugInfoExportDTO> lstDrugInfo = mapListDrugInfo.get(key);
                int drugId = lstDrugInfo.get(0).getDrugId();
                int unitId = lstDrugInfo.get(0).getUnitId();
                Drug drug = drugRepository.findById(drugId).orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_NOT_EXIST));
                List<MedicineBatchDTO> medicineBatchDTOS = getListMedicineBatchByDrugId(drugId, unitId, establishment);
                if (!verifyAmountDrugOfMedicineBatch(lstDrugInfo, medicineBatchDTOS, drug.getDrugUnits(), unitId)) {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Verify.
     *
     * @param drugInfoExportDTOs
     * @param medicineBatchDTOS
     * @param units
     * @param targetUnit
     * @return
     */
    private boolean verifyAmountDrugOfMedicineBatch(List<DrugInfoExportDTO> drugInfoExportDTOs, List<MedicineBatchDTO> medicineBatchDTOS, List<DrugUnit> units, int targetUnit) {
        boolean result = true;
        if (!CollectionUtils.isEmpty(medicineBatchDTOS) && !CollectionUtils.isEmpty(drugInfoExportDTOs)) {
            Map<String, List<MedicineBatchDTO>> mapMedicineBatchDTO = new HashMap<>();
            medicineBatchDTOS.forEach(medicineBatchDTO -> {
                String keyMedicineBatch = medicineBatchDTO.getId() + Constants.UNDERSCORE + medicineBatchDTO.getDrugImportDetailId();
                List<MedicineBatchDTO> lstMedicineBatchDTO = mapMedicineBatchDTO.get(keyMedicineBatch);
                if (CollectionUtils.isEmpty(lstMedicineBatchDTO)) {
                    lstMedicineBatchDTO = new ArrayList<>();
                }
                lstMedicineBatchDTO.add(medicineBatchDTO);
                mapMedicineBatchDTO.put(keyMedicineBatch, lstMedicineBatchDTO);
            });

            Map<String, List<DrugInfoExportDTO>> mapDrugInfo = new HashMap<>();
            drugInfoExportDTOs.forEach(drugInfoExportDTO -> {
                String key = drugInfoExportDTO.getImportId() + Constants.UNDERSCORE + drugInfoExportDTO.getDrugImportDetailId();
                List<DrugInfoExportDTO> lstDrugInfo = mapDrugInfo.get(key);
                if (CollectionUtils.isEmpty(lstDrugInfo)) {
                    lstDrugInfo = new ArrayList<>();
                }
                lstDrugInfo.add(drugInfoExportDTO);
                mapDrugInfo.put(key, lstDrugInfo);
            });

            for (String key : mapDrugInfo.keySet()) {
                List<DrugInfoExportDTO> lstDrugInfo = mapDrugInfo.get(key);
                float needAmount = 0;
                for (DrugInfoExportDTO drugInfoExportDTO : lstDrugInfo) {
                    DrugUnit drugUnit = units.stream().filter(unit -> unit.getId() == drugInfoExportDTO.getUnitId()).findFirst().orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_UNIT_NOT_EXIST));
                    needAmount += convertAmountToSpecifiedUnit(units, drugUnit, drugInfoExportDTO.getAmount(), targetUnit);
                }
                if (mapMedicineBatchDTO.containsKey(key)) {
                    double remainAmount = mapMedicineBatchDTO.get(key).stream().mapToDouble(MedicineBatchDTO::getRemainAmount).sum();
                    if (remainAmount < needAmount) {
                        result = false;
                        break;
                    }
                } else {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Verify amount of medicine batch.
     *
     * @param medicineBatchs list medicine batch
     * @param drugId         drug id
     * @param drugUnitId     drug unit id
     * @param establishment  current establishment
     * @return true or false
     */
    public boolean verifyAmountDrugOfMedicineBatch(List<DrugMedicineBatchDTO> medicineBatchs, int drugId, int drugUnitId, Establishment establishment) {
        boolean result = true;
        List<MedicineBatchDTO> medicineBatchDTOS = getListMedicineBatchByDrugId(drugId, drugUnitId, establishment);
        if (!CollectionUtils.isEmpty(medicineBatchDTOS) && !CollectionUtils.isEmpty(medicineBatchs)) {
            Map<String, List<MedicineBatchDTO>> mapMedicineBatchDTO = new HashMap<>();
            medicineBatchDTOS.forEach(medicineBatchDTO -> {
                String keyMedicineBatch = medicineBatchDTO.getId() + Constants.UNDERSCORE + medicineBatchDTO.getDrugImportDetailId();
                List<MedicineBatchDTO> lstMedicineBatchDTO = mapMedicineBatchDTO.get(keyMedicineBatch);
                if (CollectionUtils.isEmpty(lstMedicineBatchDTO)) {
                    lstMedicineBatchDTO = new ArrayList<>();
                }
                lstMedicineBatchDTO.add(medicineBatchDTO);
                mapMedicineBatchDTO.put(keyMedicineBatch, lstMedicineBatchDTO);
            });

            for (DrugMedicineBatchDTO medicineBatch : medicineBatchs) {
                String verifyKey = medicineBatch.getId() + Constants.UNDERSCORE + medicineBatch.getDrugImportDetailId();
                if (mapMedicineBatchDTO.containsKey(verifyKey)) {
                    double remainAmount = mapMedicineBatchDTO.get(verifyKey).stream().mapToDouble(MedicineBatchDTO::getRemainAmount).sum();
                    if (remainAmount < medicineBatch.getAmount()) {
                        result = false;
                        break;
                    }
                } else {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Find all medicine batch of drug.
     *
     * @param drugId the drug id
     * @return list medicine batch of drug
     */
    public List<MedicineBatchDTO> getListMedicineBatchByDrugId(int drugId, int drugUnitId, Establishment establishment) {
        List<MedicineBatchDTO> result = new ArrayList<>();
        result.addAll(externalImport(drugId, drugUnitId, establishment));
        result.addAll(internalImport(drugId, drugUnitId, establishment));
        return result;
    }

    public List<DrugAmountInStoreDTO> getListMedicineBatchDrugStoreDrugId(int drugId, int drugUnitId, String query) {
        String nameEstQuery = Helper.convertQueryParamToSql(query);
        List<DrugAmountInStoreDTO> drugAmountInStoreDTOS = new ArrayList<>();
        List<Establishment> establishments = establishmentRepository.findDrugStoreByDrugId(drugId, nameEstQuery);
        List<DrugUnit> drugUnits = drugUnitRepository.findByDrug_Id(drugId);
        for (Establishment est : establishments) {
            DrugAmountInStoreDTO drugAmountInStore = new DrugAmountInStoreDTO();
            List<MedicineBatchDTO> medicineBatch = internalImport(drugId, drugUnitId, est);
            Double remainAmount = Math.floor(medicineBatch.stream().mapToDouble(x -> x.getRemainAmount()).sum());
            if (remainAmount.equals(Double.valueOf(0))) continue; // ignore establishments which no have drug in store.
            Double waitingAmount = calculateDrugAmountWaitings(est, drugUnits, drugUnitId, drugId);

            drugAmountInStore.setDrugStoreId(Integer.valueOf(est.getId()));
            drugAmountInStore.setDrugStoreName(est.getName());
            drugAmountInStore.setDrugStoreAddress(est.getAddress());
            drugAmountInStore.setRemainAmount(remainAmount);
            drugAmountInStore.setWaitingAmount(waitingAmount);
            drugAmountInStoreDTOS.add(drugAmountInStore);
        }
        return drugAmountInStoreDTOS;
    }


    /**
     * Check is valid drug amount request.
     *
     * @param prescriptionCreates
     * @return true if valid
     */
    public Boolean isValidDrugAmountRequest(PrescriptionCreateDTO prescriptionCreates) {

        List<Integer> drugIds = new ArrayList<>();
        for (PrescriptionRecordDetailDoctorDTO request : prescriptionCreates.getDetails()) {

            if (!establishmentRepository.isValidEstablishmentFollowConditions(request.getEstablishmentId(),
                    EstablishmentType.DRUG_STORE.getCode(), null, 3)) {
                throw new MedicException(MedicException.ERROR_DATA_INVALID,
                        "Establishment is invalid with id = " + request.getEstablishmentId());
            }

            Establishment est = establishmentRepository.findById(request.getEstablishmentId()).get();

            DrugUnit duRequest = drugUnitRepository.findById(request.getDrugUnitId()).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DATA_INVALID, "Can not find drug unit."));

            if (drugIds.contains(Integer.valueOf(duRequest.getDrug().getId()))){
                throw new MedicException(MedicException.ERROR_DATA_INVALID,
                        String.format("%s already exist more than 1 time in prescription record. ", duRequest.getDrug().getName()));
            }
            drugIds.add(duRequest.getDrug().getId());


            List<DrugUnit> drugUnits = drugUnitRepository.findByDrug_Id(duRequest.getDrug().getId());

            List<MedicineBatchDTO> medicineBatch = internalImport(duRequest.getDrug().getId(),
                    request.getDrugUnitId(),
                    est);

            Double remainAmount = Math.floor(medicineBatch.stream().mapToDouble(x -> x.getRemainAmount()).sum());

            Double waitingAmount = calculateDrugAmountWaitings(est,
                    drugUnits, request.getDrugUnitId(),
                    duRequest.getDrug().getId());
            if (waitingAmount + Double.valueOf(request.getAmount()) > remainAmount){
               throw new MedicException(MedicException.ERROR_DATA_INVALID, "Drug remain amount is invalid.");
            }
        }

        return true;
    }

    private Double calculateDrugAmountWaitings(Establishment establishment,
                                               List<DrugUnit> drugUnits,
                                               Integer drugUnitIdRequest,
                                               Integer drugIdRequest){
        List<DrugAmountWaitingDTO> drugAmountWaitings = prescriptionDetailRepository.
                findDrugAmountWaiting(drugIdRequest, establishment.getId());
        Double amount = Double.valueOf(0);
        for (DrugAmountWaitingDTO drugAmountWaiting : drugAmountWaitings) {
            DrugUnit drugUnitSource =
                    drugUnits.stream().filter(r -> r.getId() == drugAmountWaiting.getDrugUnitId()).
                    findFirst().
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DATA_INVALID));
            amount += convertAmountToSpecifiedUnit(drugUnits,
                    drugUnitSource,
                    drugAmountWaiting.getWaitingAmount() == null ? 0 : drugAmountWaiting.getWaitingAmount(),
                    drugUnitIdRequest);
        }

        return Math.floor(amount);
    }

    /**
     * Get all medicine batch have type is INTERNAL import.
     *
     * @param drugId        the drug id
     * @param drugUnitId    the drug unit id
     * @param establishment current establishment
     * @return list medicine batch
     */
    private List<MedicineBatchDTO> internalImport(int drugId, int drugUnitId, Establishment establishment) {
        List<MedicineBatchDTO> result = new ArrayList<>();
        DrugUnit drugUnit = drugUnitRepository.findById(drugUnitId).orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_UNIT_NOT_EXIST));
        int rootParentTargetDrugUnitId = getRootParentUnitId(drugUnit);
        // Get list internal export
        List<DrugExportDetail> internalExports = drugExportDetailRepository.findInternalExportByDrugIdAndEstablishment(drugId, establishment);

        if (!CollectionUtils.isEmpty(internalExports)) {
            // Create map internal import from internal export
            Map<Integer, List<DrugExportDetail>> mapInternalImport = new HashMap<>();
            internalExports.forEach(internalExport -> {
                Integer drugImportDetailId = internalExport.getDrugImportDetail().getId();
                List<DrugExportDetail> lstInternalExportDetail = mapInternalImport.get(drugImportDetailId);
                if (CollectionUtils.isEmpty(lstInternalExportDetail)) {
                    lstInternalExportDetail = new ArrayList<>();
                }
                int rootParentSourceDrugUnitId = getRootParentUnitId(internalExport.getDrugUnit());
                if (rootParentTargetDrugUnitId == rootParentSourceDrugUnitId) {
                    lstInternalExportDetail.add(internalExport);
                }
                mapInternalImport.put(drugImportDetailId, lstInternalExportDetail);
            });
            // Get list drug import details id from internal export
            List<Integer> drugImportDetailIds = internalExports.stream().map(drugExportDetail -> drugExportDetail.getDrugImportDetail().getId()).distinct().collect(Collectors.toList());
            // Get list export detail of current establishment
            List<DrugExportDetail> drugExportDetails = drugExportDetailRepository.findAllByDrugImportDetail_IdInAndDrugExport_ExportFrom(drugImportDetailIds, establishment);
            // Create map import detail id and drug export detail
            Map<Integer, List<DrugExportDetail>> mapImportDetailIdAndExportDetail = new HashMap<>();
            drugExportDetails.forEach(drugExportDetail -> {
                Integer drugImportDetailId = drugExportDetail.getDrugImportDetail().getId();
                List<DrugExportDetail> lstDrugExportDetail = mapImportDetailIdAndExportDetail.get(drugImportDetailId);
                if (CollectionUtils.isEmpty(lstDrugExportDetail)) {
                    lstDrugExportDetail = new ArrayList<>();
                }
                lstDrugExportDetail.add(drugExportDetail);
                mapImportDetailIdAndExportDetail.put(drugImportDetailId, lstDrugExportDetail);
            });

            mapInternalImport.keySet().forEach(key -> {
                float remainAmount = calculateInternalRemainAmountByUnitDrug(mapInternalImport.get(key), mapImportDetailIdAndExportDetail, drugUnitId);
                if (remainAmount > 0) {
                    DrugExportDetail internalExport = mapInternalImport.get(key).get(0);
                    MedicineBatchDTO medicineBatchDTO = new MedicineBatchDTO();
                    medicineBatchDTO.setId(internalExport.getDrugImportDetail().getDrugImport().getId());
                    medicineBatchDTO.setDrugImportDetailId(internalExport.getDrugImportDetail().getId());
                    medicineBatchDTO.setAmount(calculateInternalImportAmount(mapInternalImport.get(key), drugUnitId));
                    medicineBatchDTO.setRemainAmount(remainAmount);
                    medicineBatchDTO.setExpiration(internalExport.getDrugImportDetail().getExpiration().toLocalDate());
                    result.add(medicineBatchDTO);
                }
            });
        }
        return result;
    }

    /**
     * Calculate internal import amount.
     *
     * @param internalExports list internal import (from internal export)
     * @param drugUnitId      the drug unit id
     * @return amount of import
     */
    private float calculateInternalImportAmount(List<DrugExportDetail> internalExports, int drugUnitId) {
        float amount = 0;
        if (!CollectionUtils.isEmpty(internalExports)) {
            List<DrugUnit> drugUnits = internalExports.get(0).getDrugUnit().getDrug().getDrugUnits();
            for (DrugExportDetail internalExport : internalExports) {
                amount += convertAmountToSpecifiedUnit(drugUnits, internalExport.getDrugUnit(), internalExport.getAmount(), drugUnitId);
            }
        }
        return amount;
    }

    /**
     * Calculate the remain amount of internal import.
     *
     * @param internalExports                  list internal import (from internal export)
     * @param mapImportDetailIdAndExportDetail map import detail id and export detail
     * @param drugUnitId                       the drug unit id
     * @return remain amount
     */
    private float calculateInternalRemainAmountByUnitDrug(List<DrugExportDetail> internalExports,
                                                          Map<Integer, List<DrugExportDetail>> mapImportDetailIdAndExportDetail,
                                                          int drugUnitId) {
        float amount = 0;
        if (!CollectionUtils.isEmpty(internalExports)) {
            List<DrugUnit> drugUnits = internalExports.get(0).getDrugUnit().getDrug().getDrugUnits();
            float importAmount = 0;
            for (DrugExportDetail internalExport : internalExports) {
                importAmount += convertAmountToSpecifiedUnit(drugUnits, internalExport.getDrugUnit(), internalExport.getAmount(), drugUnitId);
            }

            float exportAmount = 0;
            Integer importDetailIdKey = internalExports.get(0).getDrugImportDetail().getId();
            if (importAmount > 0 && mapImportDetailIdAndExportDetail.containsKey(importDetailIdKey)) {
                List<DrugExportDetail> drugExportDetails = mapImportDetailIdAndExportDetail.get(importDetailIdKey);
                for (DrugExportDetail drugExportDetail : drugExportDetails) {
                    exportAmount += convertAmountToSpecifiedUnit(drugUnits, drugExportDetail.getDrugUnit(), drugExportDetail.getAmount(), drugUnitId);
                }
            }

            amount = importAmount - exportAmount;
        }
        return amount;
    }

    /**
     * Calculate external import.
     *
     * @param drugId        the drug id
     * @param drugUnitId    the drug unit id
     * @param establishment current establishment
     * @return list medicine batch
     */
    private List<MedicineBatchDTO> externalImport(int drugId, int drugUnitId, Establishment establishment) {
        List<MedicineBatchDTO> result = new ArrayList<>();
        // Get all drug import detail of drug
        List<DrugImportDetail> drugImportDetails = drugImportDetailRepository.findAllMedicineBatchByDrugIdAndEstablishment(drugId, establishment);

        if (!CollectionUtils.isEmpty(drugImportDetails)) {
            // Get list drug import detail id
            List<Integer> drugImportDetailIds = drugImportDetails.stream().map(DrugImportDetail::getId).collect(Collectors.toList());
            // Get list export detail
            List<DrugExportDetail> drugExportDetails = drugExportDetailRepository.findAllByDrugImportDetail_IdInAndDrugExport_ExportFrom(drugImportDetailIds, establishment);
            // Create map import detail id and drug export detail
            Map<Integer, List<DrugExportDetail>> mapImportDetailIdAndExportDetail = new HashMap<>();
            drugExportDetails.forEach(drugExportDetail -> {
                Integer drugImportDetailId = drugExportDetail.getDrugImportDetail().getId();
                List<DrugExportDetail> lstDrugExportDetail = mapImportDetailIdAndExportDetail.get(drugImportDetailId);
                if (CollectionUtils.isEmpty(lstDrugExportDetail)) {
                    lstDrugExportDetail = new ArrayList<>();
                }
                lstDrugExportDetail.add(drugExportDetail);
                mapImportDetailIdAndExportDetail.put(drugImportDetailId, lstDrugExportDetail);
            });

            drugImportDetails.forEach(drugImportDetail -> {
                float remainAmount = calculateExternalRemainAmountByUnitDrug(drugImportDetail, mapImportDetailIdAndExportDetail, drugUnitId);
                if (remainAmount > 0) {
                    MedicineBatchDTO medicineBatchDTO = new MedicineBatchDTO();
                    medicineBatchDTO.setId(drugImportDetail.getDrugImport().getId());
                    medicineBatchDTO.setDrugImportDetailId(drugImportDetail.getId());
                    medicineBatchDTO.setAmount(calculateExternalImportAmount(drugImportDetail, drugUnitId));
                    medicineBatchDTO.setRemainAmount(remainAmount);
                    medicineBatchDTO.setExpiration(drugImportDetail.getExpiration().toLocalDate());
                    result.add(medicineBatchDTO);
                }
            });
        }
        return result;
    }

    /**
     * Calculate amount of drug import.
     *
     * @param drugImportDetail drug import detail
     * @param drugUnitId       drug unit id
     * @return amount of drug import
     */
    private float calculateExternalImportAmount(DrugImportDetail drugImportDetail, int drugUnitId) {
        float resultAmount = 0;
        if (null != drugImportDetail) {
            List<DrugUnit> drugUnits = drugImportDetail.getDrugUnit().getDrug().getDrugUnits();
            resultAmount = convertAmountToSpecifiedUnit(drugUnits, drugImportDetail.getDrugUnit(), drugImportDetail.getAmount(), drugUnitId);
        }
        return resultAmount;
    }

    /**
     * Calculate amount remain of drug.
     *
     * @param drugImportDetail                 drug import detail
     * @param mapImportDetailIdAndExportDetail map of drug import detail id and list export detail
     * @param drugUnitId                       drug unit id
     * @return amount remain of drug
     */
    private float calculateExternalRemainAmountByUnitDrug(DrugImportDetail drugImportDetail,
                                                          Map<Integer, List<DrugExportDetail>> mapImportDetailIdAndExportDetail,
                                                          int drugUnitId) {
        float resultAmount = 0;
        if (null != drugImportDetail) {
            List<DrugUnit> drugUnits = drugImportDetail.getDrugUnit().getDrug().getDrugUnits();
            float importAmount = convertAmountToSpecifiedUnit(drugUnits, drugImportDetail.getDrugUnit(), drugImportDetail.getAmount(), drugUnitId);
            float exportAmount = 0;
            if (importAmount > 0 && mapImportDetailIdAndExportDetail.containsKey(drugImportDetail.getId())) {
                List<DrugExportDetail> drugExportDetails = mapImportDetailIdAndExportDetail.get(drugImportDetail.getId());
                for (DrugExportDetail drugExportDetail : drugExportDetails) {
                    exportAmount += convertAmountToSpecifiedUnit(drugUnits, drugExportDetail.getDrugUnit(), drugExportDetail.getAmount(), drugUnitId);
                }
            }
            resultAmount = importAmount - exportAmount;
        }
        return resultAmount;
    }

    /**
     * Convert amount from unit to another unit.
     *
     * @param drugUnits       list unit of drug
     * @param sourceUnit      source unit
     * @param amountSource    amount
     * @param specifiedUnitId specified unit id
     * @return amount after converted
     */
    private float convertAmountToSpecifiedUnit(List<DrugUnit> drugUnits, DrugUnit sourceUnit, int amountSource, int specifiedUnitId) {
        float amount = 0;
        if (!CollectionUtils.isEmpty(drugUnits)) {
            DrugUnit targetUnit = drugUnits.stream().filter(tDrugUnit -> tDrugUnit.getId() == specifiedUnitId).findFirst().
                    orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_UNIT_NOT_EXIST, "Drug Unit don't exist"));
            int rootParentSourceDrugUnitId = getRootParentUnitId(sourceUnit);
            int rootParentTargetDrugUnitId = getRootParentUnitId(targetUnit);
            if (rootParentSourceDrugUnitId == rootParentTargetDrugUnitId) {
                if (sourceUnit.getId() == targetUnit.getId()) {
                    amount = amountSource;
                } else {
                    List<Integer> sourceUnitPaths = getPathsOfDrugUnit(sourceUnit);
                    List<Integer> targetUnitPaths = getPathsOfDrugUnit(targetUnit);
                    if (sourceUnitPaths.size() < targetUnitPaths.size()) {
                        int tempAmount = 1;
                        for (int i = 0; i < targetUnitPaths.size(); i++) {
                            if (i >= sourceUnitPaths.size()) {
                                int drugId = targetUnitPaths.get(i);
                                DrugUnit unit = drugUnits.stream().filter(tDrugUnit -> tDrugUnit.getId() == drugId).findFirst().
                                        orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_UNIT_NOT_EXIST, "Drug Unit don't exist"));
                                tempAmount = tempAmount * unit.getParentEquivalentAmount();
                            } else if (!sourceUnitPaths.get(i).equals(targetUnitPaths.get(i))) {
                                tempAmount = 0;
                                break;
                            }
                        }
                        amount = tempAmount * amountSource;
                    } else if (sourceUnitPaths.size() > targetUnitPaths.size()) {
                        float tempAmount = 1;
                        for (int i = 0; i < sourceUnitPaths.size(); i++) {
                            if (i >= targetUnitPaths.size()) {
                                int drugId = sourceUnitPaths.get(i);
                                DrugUnit unit = drugUnits.stream().filter(tDrugUnit -> tDrugUnit.getId() == drugId).findFirst().
                                        orElseThrow(() -> new MedicException(MedicException.ERROR_DRUG_UNIT_NOT_EXIST, "Drug Unit don't exist"));
                                tempAmount = tempAmount / unit.getParentEquivalentAmount();
                            } else if (!sourceUnitPaths.get(i).equals(targetUnitPaths.get(i))) {
                                tempAmount = 0;
                                break;
                            }
                        }
                        amount = tempAmount * amountSource;
                    }
                }
            }
        }
        return amount;
    }

    /**
     * Get root parent of unit id.
     *
     * @param drugUnit drug unit
     * @return id of root parent unit
     */
    private int getRootParentUnitId(DrugUnit drugUnit) {
        int parentDrugUnitId = drugUnit.getId();
        if (!StringUtils.isBlank(drugUnit.getPath())) {
            parentDrugUnitId = getPathsOfDrugUnit(drugUnit).get(0);
        }
        return parentDrugUnitId;
    }

    /**
     * Get path of drug unit.
     *
     * @param drugUnit drug unit
     * @return path
     */
    private List<Integer> getPathsOfDrugUnit(DrugUnit drugUnit) {
        List<Integer> path = new ArrayList<>();
        String[] p = Helper.convertNullToEmpty(drugUnit.getPath()).split(Constants.DOT_REGEX);
        path.addAll(Arrays.stream(p).filter(Helper::isNumeric).map(Integer::valueOf).collect(Collectors.toList()));
        path.add(drugUnit.getId());
        return path;
    }

    /**
     * Get remain amount of drug.
     *
     * @param establishment drug store
     * @param drug          drug
     * @param targetUnit    unit of drug
     * @param importId      import drug id
     * @return remain amount of batch medicine
     */
    public MedicineBatchDTO getRemainAmountOfDrugInImport(Establishment establishment, Drug drug, DrugUnit targetUnit, int importId) {
        MedicineBatchDTO medicineBatchDTO = new MedicineBatchDTO();
        List<DrugUnit> drugUnits = drug.getDrugUnits();
        List<DrugImportDetail> drugImportDetails = drugImportDetailRepository.findAllMedicineBatchByDrugIdAndImportId(drug.getId(), importId);
        if (!CollectionUtils.isEmpty(drugUnits) && !CollectionUtils.isEmpty(drugImportDetails)) {
            float totalAmount = 0;
            for (DrugImportDetail drugImportDetail : drugImportDetails) {
                totalAmount += convertAmountToSpecifiedUnit(drugUnits, drugImportDetail.getDrugUnit(), drugImportDetail.getAmount(), targetUnit.getId());
            }

            float remainAmount = 0;
            if (totalAmount > 0) {
                List<Integer> drugImportDetailIds = drugImportDetails.stream().map(DrugImportDetail::getId).collect(Collectors.toList());
                List<DrugExportDetail> drugExportDetails = drugExportDetailRepository.findAllByDrugImportDetail_IdInAndDrugExport_ExportFrom(drugImportDetailIds, establishment);
                Map<Integer, List<DrugExportDetail>> mapImportDetailIdAndExportDetail = new HashMap<>();
                drugExportDetails.forEach(drugExportDetail -> {
                    Integer drugImportDetailId = drugExportDetail.getDrugImportDetail().getId();
                    List<DrugExportDetail> lstDrugExportDetail = mapImportDetailIdAndExportDetail.get(drugImportDetailId);
                    if (CollectionUtils.isEmpty(lstDrugExportDetail)) {
                        lstDrugExportDetail = new ArrayList<>();
                    }
                    lstDrugExportDetail.add(drugExportDetail);
                    mapImportDetailIdAndExportDetail.put(drugImportDetailId, lstDrugExportDetail);
                });
                for (DrugImportDetail drugImportDetail : drugImportDetails) {
                    remainAmount += calculateExternalRemainAmountByUnitDrug(drugImportDetail, mapImportDetailIdAndExportDetail, targetUnit.getId());
                }
            }
            medicineBatchDTO.setAmount(totalAmount);
            medicineBatchDTO.setRemainAmount(remainAmount);
        }
        return medicineBatchDTO;
    }
}
