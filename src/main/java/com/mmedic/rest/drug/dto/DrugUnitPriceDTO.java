package com.mmedic.rest.drug.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrugUnitPriceDTO {

    private Integer id;

    private Integer drugUnitId;

    private int pricePerUnit;

    private LocalDateTime effectiveAt;

}
