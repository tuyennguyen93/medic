package com.mmedic.rest.drug.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DrugAmountInStoreDTO {

    private Integer drugStoreId;

    private String drugStoreName;

    private String drugStoreAddress;

    private Double remainAmount;

    private Double waitingAmount;

}
