package com.mmedic.rest.drug.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class MedicineBatchDTO {

    private int id;

    private int drugImportDetailId;

    private float amount;

    private float remainAmount;

    private LocalDate expiration;

//    private int drugId;
//
//    private int drugName;
//
//    private int drugUnitId;
//
//    private int drugUnitName;
}
