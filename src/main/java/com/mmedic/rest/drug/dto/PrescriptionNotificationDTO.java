package com.mmedic.rest.drug.dto;

public interface PrescriptionNotificationDTO {

    int getMedicalRecordId();

    int getPatientId();

    String getPatientName();

    String getNotificationToken();

    String getLanguage();

    Integer getDrugId();

    String getDrugName();
}
