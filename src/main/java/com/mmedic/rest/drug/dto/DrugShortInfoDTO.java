package com.mmedic.rest.drug.dto;

public interface DrugShortInfoDTO {

    Integer getId();

    String getName();

    String getPath();

    Integer getParentId();

    Boolean getCanPrescribe();

    Boolean getHasChild();

}
