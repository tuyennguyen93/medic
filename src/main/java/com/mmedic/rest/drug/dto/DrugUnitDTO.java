package com.mmedic.rest.drug.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DrugUnitDTO {

    private Integer id;

    private Integer drugId;

    private String name;

    private Integer parentId;

    private String path;

    private Integer parentEquivalentAmount;

    private List<DrugUnitPriceDTO> prices;
}
