package com.mmedic.rest.drug.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AllergyDrugDTO {

    private Integer id;

    private String name;
}
