package com.mmedic.rest.drug.dto;

public interface DrugCurrentPriceDTO {

    int getId();

    int getDrugUnitId();

    int getPricePerUnit();

}
