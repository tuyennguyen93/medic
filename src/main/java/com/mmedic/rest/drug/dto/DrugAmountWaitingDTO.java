package com.mmedic.rest.drug.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DrugAmountWaitingDTO {

    private Integer drugUnitId;

    private Integer waitingAmount;

}
