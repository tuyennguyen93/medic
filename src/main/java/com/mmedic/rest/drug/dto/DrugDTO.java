package com.mmedic.rest.drug.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DrugDTO {

    private Integer id;

    private String name;

    private Integer parentId;

    private String path;

    private boolean canPrescribe;

    private String origin;

    private String contraindication;

    private String composition;

    private String concentration;

    private String form;

    private String dosage;

    private List<DrugUnitDTO> units;

}
