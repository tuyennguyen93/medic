package com.mmedic.rest.drug;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.drug.dto.*;
import com.mmedic.rest.establishment.EstablishmentService;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Drug controller.
 */
@Slf4j
@RestController
@RequestMapping(DrugController.BASE_URL)
@Api(value = "Drug Controller", description = "Drug Management")
@RequiredArgsConstructor
public class DrugController {

    static final String BASE_URL = "/api/v1";

    private final DrugService drugService;

    private final EstablishmentService establishmentService;

    private final BaseService baseService;

    @ApiOperation("Get allergy drug list for patient")
    @GetMapping("/allergy-drugs")
    public MedicResponse<List<AllergyDrugDTO>> getAllergyDrugList(
            @RequestParam(value = "query", required = false)
            @ApiParam("Condition Search") String query) {
        List<AllergyDrugDTO> result = drugService.getAllergyDrugList(query);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get all available drug")
    @GetMapping("/drugs/all")
    public MedicResponse<List<DrugDTO>> getDrugList(@RequestParam(required = false) Boolean hasPrice) {
        boolean hasPriceValue = null != hasPrice ? hasPrice : false;
        List<DrugDTO> result = drugService.getAllAvailableDrug(hasPriceValue);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get drug list without parent")
    @GetMapping("/drugs")
    public MedicResponse<List<DrugDTO>> getDrugListWithoutParent() {
        List<DrugDTO> result = drugService.getDrugsFollowParent(null);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get short info drug list without parent ")
    @GetMapping("/drugs/short-infos")
    public MedicResponse<List<DrugShortInfoDTO>> getDrugsShortInfoWithoutParent(@RequestParam(value = "query", required = false) String query) {
        List<DrugShortInfoDTO> result = drugService.getDrugsShortInfo(query, null);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get short info drug list with parent id ")
    @GetMapping("/drugs/short-infos/{id}")
    public MedicResponse<List<DrugShortInfoDTO>> getDrugsShortInfoWithId(@RequestParam(value = "query", required = false) String query,
                                                                         @PathVariable("id") Integer id) {
        List<DrugShortInfoDTO> result = drugService.getDrugsShortInfo(query, id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get full info drug follow drugId")
    @GetMapping("/drugs/{id}/infos")
    public MedicResponse<DrugDTO> getInfoDrugFollowId(@PathVariable("id") Integer drugId) {
        DrugDTO result = drugService.getInfoDrugFollowId(drugId);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get drug list with parent")
    @GetMapping("/drugs/{id}")
    public MedicResponse<List<DrugDTO>> getDrugListWithoutParent(@PathVariable("id") Integer parentId) {
        List<DrugDTO> result = drugService.getDrugsFollowParent(parentId);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create drug")
    @PostMapping("/drugs")
    public MedicResponse<DrugDTO> createDrug(@RequestBody DrugDTO reqDto) {
        DrugDTO result = drugService.createDrug(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update drug")
    @PutMapping("/drugs")
    public MedicResponse<DrugDTO> updateDrug(@RequestBody DrugDTO reqDto) {
        DrugDTO result = drugService.updateDrug(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete drug")
    @DeleteMapping("/drugs/{id}")
    public MedicResponse<DrugDTO> deleteDrug(@PathVariable("id") Integer id) {
        DrugDTO result = drugService.deleteDrug(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get unit follow drug")
    @GetMapping("/drugs/{id}/units")
    public MedicResponse<List<DrugUnitDTO>> getUnitfollowDrug(@PathVariable("id") Integer id) {
        List<DrugUnitDTO> result = drugService.getAllAvailableUnitFollowDrug(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create drug unit")
    @PostMapping("/drugs/{id}/units")
    public MedicResponse<DrugUnitDTO> createDrugUnit(@RequestBody DrugUnitDTO reqDto) {
        return MedicResponse.okStatus(drugService.createDrugUnit(reqDto));
    }

    @ApiOperation("Update drug unit")
    @PutMapping("/drugs/{id}/units")
    public MedicResponse<DrugUnitDTO> updateDrugUnit(@RequestBody DrugUnitDTO reqDto) {
        return MedicResponse.okStatus(drugService.updateDrugUnit(reqDto));
    }

    @ApiOperation("Delete drug unit")
    @DeleteMapping("/drugs/{id}/units/{id}")
    public MedicResponse<String> deleteDrugUnit(@PathVariable("id") Integer id) {
        return MedicResponse.okStatus(drugService.deleteDrugUnit(id));
    }

    /**
     * Find drug tree.
     *
     * @param drugId the drug id which is element of tree
     * @return the drug tree
     */
    @ApiOperation("Get drug tree of drug (DONE)")
    @GetMapping("/drugs/tree")
    public MedicResponse<List<DrugDTO>> getDrugTree(@ApiParam("Drug Id") @RequestParam int drugId,
                                                    @ApiParam("Has Price") @RequestParam(required = false) Boolean hasPrice) {
        return MedicResponse.okStatus(drugService.getDrugTree(drugId, hasPrice != null ? hasPrice : false));
    }

    /**
     * Search drug and it's tree.
     *
     * @param query the condition value
     * @return list drug
     */
    @ApiOperation("Search drug and it's tree (DONE)")
    @GetMapping("/drugs/search")
    public MedicResponse<List<DrugDTO>> searchDrugTree(@ApiParam("Condition") @RequestParam(required = false) String query,
                                                       @ApiParam("Has Price") @RequestParam(required = false) Boolean hasPrice) {
        return MedicResponse.okStatus(drugService.searchDrugTree(Helper.convertNullToEmpty(query), null != hasPrice ? hasPrice : false));
    }

    /**
     * Search drug can be prescribe.
     *
     * @param query condition value
     * @return list drug
     */
    @GetMapping("/drugs/can-prescribe")
    public MedicResponse<List<DrugDTO>> searchCanPrescribeDrug(@ApiParam("Condition") @RequestParam(required = false) String query,
                                                               @ApiParam("Has Unit") @RequestParam(required = false) Boolean hasUnit) {
        return MedicResponse.okStatus(drugService.searchDrugCanPrescribe(query, null != hasUnit ? hasUnit : false));
    }

    /**
     * Gets all medicine batch of drug.
     *
     * @param drugId the drug id
     * @return List medicine batch
     */
    @ApiOperation("Get all medicine batch of drug (DONE)")
    @GetMapping("/drugs/{drugId}/medicines-batch")
    public MedicResponse<List<MedicineBatchDTO>> getMedicineBatchByDrugId(@PathVariable("drugId") @ApiParam("Drug Id") int drugId,
                                                                          @RequestParam @ApiParam("Drug Unit Id") int drugUnitId,
                                                                          @RequestParam(required = false) @ApiParam("Establishment Id (for admin)") Integer establishmentId) {
        Establishment establishment = getEstablishmentBaseOnRole(baseService.getCurrentAccountLogin(), establishmentId);
        return MedicResponse.okStatus(drugService.getListMedicineBatchByDrugId(drugId, drugUnitId, establishment));
    }

    /**
     * Get establishment base on role.
     *
     * @param account         the account is login
     * @param establishmentId the establishment id
     * @return establishment
     */
    private Establishment getEstablishmentBaseOnRole(Account account, Integer establishmentId) {
        Establishment establishment = null;
        if ((Constants.ROLE_SUPER_ADMIN.equals(account.getRole().getName()) ||
                Constants.ROLE_ADMIN.equals(account.getRole().getName()))) {
            if (null == establishmentId) {
                throw new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Establishment don't exist");
            }
            establishment = establishmentService.getEstablishmentById(establishmentId);
        } else {
            establishment = baseService.getCurrentEstablishment();
        }
        return establishment;
    }

    @ApiOperation("Get all medicine batch of drug for prescription (DONE)")
    @GetMapping("/drugs/{drugId}/medicines-batch/drug-stores")
    public MedicResponse<List<DrugAmountInStoreDTO>> getMedicineBatchDrugStoreDrugId(@PathVariable("drugId") @ApiParam("Drug Id") int drugId,
                                                                                     @RequestParam("drugUnitId") @ApiParam("Drug Unit Id") int drugUnitId,
                                                                                     @RequestParam(value = "query", required = false) @ApiParam("Query") String query) {
        return MedicResponse.okStatus(drugService.getListMedicineBatchDrugStoreDrugId(drugId, drugUnitId, query));
    }

}
