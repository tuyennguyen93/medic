package com.mmedic.rest.medicalService.repo;

import com.mmedic.entity.MedicalService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MedicalServiceRepository extends JpaRepository<MedicalService, Integer> {

    List<MedicalService> findByMedicalServiceGroup_IdAndIsDeletedIsFalse(Integer id);

    Optional<MedicalService> findByNameEqualsAndIsDeletedIsFalse(String name);

    Optional<MedicalService> findByIdIsNotLikeAndNameEqualsAndIsDeletedIsFalse(Integer id, String name);

    Optional<MedicalService> findByIdAndIsDeletedIsFalse(Integer id);

    List<MedicalService> findByMedicalServiceGroup_Id(Integer id);

    List<MedicalService> findByMedicalServiceGroup_IdAndMedicalServiceGroup_IsDisabledFalseAndIsDeletedFalse(Integer id);

    Optional<MedicalService> findByIdAndMedicalServiceGroup_IdAndIsDeletedIsFalse(int id, int groupId);

    List<MedicalService> findByIsDeletedIsFalse();

}
