package com.mmedic.rest.medicalService.repo;

import com.mmedic.entity.MedicalServiceOrderDiscussion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalServiceOrderDiscussionRepository extends JpaRepository<MedicalServiceOrderDiscussion, Integer> {

}
