package com.mmedic.rest.medicalService.repo;

import com.mmedic.entity.MedicalServicePriceHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalServicePriceHistoryRepository extends JpaRepository<MedicalServicePriceHistory, Integer> {
}
