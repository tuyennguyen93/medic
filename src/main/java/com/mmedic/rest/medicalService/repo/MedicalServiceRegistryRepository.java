package com.mmedic.rest.medicalService.repo;

import com.mmedic.entity.MedicalServiceRegistry;
import com.mmedic.rest.establishment.dto.DoctorPriceHistoryDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MedicalServiceRegistryRepository extends JpaRepository<MedicalServiceRegistry, Integer> {

    Optional<MedicalServiceRegistry> findByEstablishment_IdAndMedicalService_IdAndActiveTrue(int establishmentId, int medicalServiceId);

    List<MedicalServiceRegistry> findAllByEstablishment_IdInAndMedicalService_IdAndActiveTrue(List<Integer> establishmentId, int medicalServiceId);

    @Query("SELECT servicePrice.id AS id, servicePrice.effectiveAt AS effectiveAt,  servicePrice.price AS price " +
            "FROM MedicalServiceRegistry serviceRegistry " +
            "LEFT JOIN serviceRegistry.medicalService medicalService " +
            "INNER JOIN serviceRegistry.medicalServicePriceHistories servicePrice " +
            "WHERE serviceRegistry.establishment.id = :establishmentId " +
            "AND medicalService.id = 1" +
            "AND servicePrice.effectiveAt < CURRENT_DATE + 1" +
            "ORDER BY servicePrice.effectiveAt DESC")
    List<DoctorPriceHistoryDTO> findDoctorPriceHistoryByEstablishmentId(@Param("establishmentId") int establishmentId);

    List<MedicalServiceRegistry> findAllByEstablishment_IdAndMedicalService_MedicalServiceGroup_Id(int establishmentId, int groupId);

    Optional<MedicalServiceRegistry> findByIdAndEstablishment_Id(int id, int establishmentId);

    Optional<MedicalServiceRegistry> findByMedicalService_Id(int id);

    Optional<MedicalServiceRegistry> findByMedicalService_IdAndEstablishment_Id(int medicalServiceId, int establishmentId);

    Optional<MedicalServiceRegistry> findByMedicalService_IdAndEstablishment_IdAndActiveTrue(int medicalServiceId, int establishmentId);

}
