package com.mmedic.rest.medicalService.repo;

import com.mmedic.entity.MedicalServiceGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MedicalServiceGroupRepository extends JpaRepository<MedicalServiceGroup, Integer> {

    Optional<MedicalServiceGroup> findByNameEqualsAndIsDisabledIsFalse(String name);
    Optional<MedicalServiceGroup> findByIdIsNotLikeAndNameEqualsAndIsDisabledIsFalse(int id,String name);

}
