package com.mmedic.rest.medicalService.repo;

import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.rest.medicalService.dto.OrderDetailDTO;
import com.mmedic.rest.medicalService.dto.PreclinicalNotificationDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface MedicalServiceOrderRepository extends JpaRepository<MedicalServiceOrder, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT COUNT(*) " +
                    "FROM medical_service_order mso " +
                    "LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "WHERE medical_service_id = 1 " +
                    "  AND mso.payment_status = 'SUCCESS' " +
                    "  AND establishment_id = :establishmentId " +
                    "  AND handle_by = :doctorId " +
                    "  AND msod.appointment_date >= :appointment AND msod.appointment_date <= :appointment")
    int countTreatmentServiceOrderByInAppointmentDate(@Param("establishmentId") int establishmentId, @Param("doctorId") int doctorId,
                                                      @Param("appointment") LocalDate appointment);

    @Query(nativeQuery = true,
            value = "SELECT COUNT(*) " +
                    "FROM medical_service_order mso " +
                    "LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "WHERE medical_service_id = 1 " +
                    "  AND mso.payment_status = 'SUCCESS' " +
                    "  AND establishment_id = :establishmentId " +
                    "  AND handle_by = :doctorId " +
                    "  AND msod.appointment_date >= :appointmentStart AND msod.appointment_date <= :appointmentEnd")
    int countTreatmentServiceOrderByInAppointmentDate(@Param("establishmentId") int establishmentId, @Param("doctorId") int doctorId,
                                                      @Param("appointmentStart") LocalDateTime appointmentStart,
                                                      @Param("appointmentEnd") LocalDateTime appointmentEnd);

    @Query(nativeQuery = true,
            value = "SELECT msod.id               AS id, " +
                    "       msod.appointment_date AS appointmentDate ," +
                    "       msod.sequence_number  AS sequenceNumber " +
                    "FROM medical_service_order mso " +
                    "LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "WHERE medical_service_id = 1 " +
                    "  AND mso.payment_status = 'SUCCESS' " +
                    "  AND establishment_id = :establishmentId " +
                    "  AND handle_by = :doctorId " +
                    "  AND msod.appointment_date >= :appointmentStart AND msod.appointment_date <= :appointmentEnd " +
                    "ORDER BY msod.appointment_date")
    List<OrderDetailDTO> findTreatmentServiceOrderByInAppointmentDate(@Param("establishmentId") int establishmentId, @Param("doctorId") int doctorId,
                                                                      @Param("appointmentStart") LocalDateTime appointmentStart,
                                                                      @Param("appointmentEnd") LocalDateTime appointmentEnd);

    @Query(nativeQuery = true,
            value = "SELECT COUNT(*) " +
                    "FROM medical_service_order mso " +
                    "LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "WHERE mso.payment_status = 'SUCCESS' " +
                    "  AND establishment_id = :establishmentId " +
                    "  AND msod.appointment_date >= :appointmentStart AND msod.appointment_date <= :appointmentEnd")
    int countServiceOrderByInAppointmentDate(@Param("establishmentId") int establishmentId,
                                             @Param("appointmentStart") LocalDateTime appointmentStart,
                                             @Param("appointmentEnd") LocalDateTime appointmentEnd);

    @Query(nativeQuery = true,
            value = "SELECT msod.id               AS id, " +
                    "       msod.appointment_date AS appointmentDate, " +
                    "       msod.sequence_number  AS sequenceNumber " +
                    "FROM medical_service_order mso " +
                    "LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "WHERE mso.payment_status = 'SUCCESS' " +
                    "  AND establishment_id = :establishmentId " +
                    "  AND msod.appointment_date >= :appointmentStart AND msod.appointment_date <= :appointmentEnd")
    List<OrderDetailDTO> findServiceOrderByInAppointmentDate(@Param("establishmentId") int establishmentId,
                                             @Param("appointmentStart") LocalDateTime appointmentStart,
                                             @Param("appointmentEnd") LocalDateTime appointmentEnd);

    /**
     * Find order preclinical not payment.
     *
     * @param registryServiceId service id
     * @return list order preclinical to notification
     */
    @Query(nativeQuery = true,
            value = "SELECT DISTINCT mr.id                AS medicalRecordId, " +
                    "                p.id                 AS patientId, " +
                    "                p.name               AS patientName, " +
                    "                a.notification_token AS notificationToken, " +
                    "                a.language           AS language, " +
                    "                ms.id                AS serviceId, " +
                    "                ms.name              AS serviceName " +
                    "FROM preclinical_record pr " +
                    "         LEFT JOIN medical_record mr ON pr.medical_record_id = mr.id " +
                    "         LEFT JOIN patient p ON mr.patient_id = p.id " +
                    "         LEFT JOIN account a ON p.account_id = a.id " +
                    "         LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "         LEFT JOIN medical_service_order_detail msod ON mso.id = msod.medical_service_order_id " +
                    "         LEFT JOIN medical_service ms ON msod.medical_service_id = ms.id " +
                    "         LEFT JOIN medical_service_registry msr ON ms.id = msr.medical_service_id AND msod.establishment_id = msr.establishment_id " +
                    "WHERE mso.payment_status = 'PENDING' " +
                    "AND msr.id = :registryServiceId")
    List<PreclinicalNotificationDTO> findPreclinicalPriceChange(@Param("registryServiceId") int registryServiceId);

    /**
     * Find all medical service order by ids
     *
     * @param ids list id
     * @return llist medical service order
     */
    List<MedicalServiceOrder> findByIdIn(List<Integer> ids);
}
