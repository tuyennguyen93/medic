package com.mmedic.rest.medicalService;

import com.mmedic.rest.medicalService.dto.MedicalServiceDTO;
import com.mmedic.spring.errors.MedicException;

import java.util.List;

/**
 * Health care service.
 */
public interface HealthCareService {

    List<MedicalServiceDTO> getHealthCarePackagesByGroupId(int groupId) throws MedicException;

    List<MedicalServiceDTO> getAllAvailableServices() throws MedicException;
}
