package com.mmedic.rest.medicalService.impl;

import com.mmedic.enums.MedicalServiceGroupType;
import org.springframework.stereotype.Service;

@Service
public class DiagnoseMedicalServiceImp  extends  AbstractBaseSubclinicalService{

    @Override
    protected Integer getMedicalServiceGroup() {
        return MedicalServiceGroupType.DIAGNOSE.getId();
    }
}
