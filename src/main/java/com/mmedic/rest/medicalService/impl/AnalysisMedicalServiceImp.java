package com.mmedic.rest.medicalService.impl;

import com.mmedic.enums.MedicalServiceGroupType;
import org.springframework.stereotype.Service;


@Service
public class AnalysisMedicalServiceImp extends AbstractBaseSubclinicalService {

    @Override
    protected Integer getMedicalServiceGroup() {
        return MedicalServiceGroupType.ANALYSIS.getId();
    }
}
