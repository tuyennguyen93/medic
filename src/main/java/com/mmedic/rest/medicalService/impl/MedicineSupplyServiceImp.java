package com.mmedic.rest.medicalService.impl;

import com.mmedic.enums.MedicalServiceGroupType;
import org.springframework.stereotype.Service;


@Service
public class MedicineSupplyServiceImp extends AbstractBaseSubclinicalService {

    @Override
    protected Integer getMedicalServiceGroup() {
        return MedicalServiceGroupType.MEDICINE_SUPPLY.getId();
    }
}
