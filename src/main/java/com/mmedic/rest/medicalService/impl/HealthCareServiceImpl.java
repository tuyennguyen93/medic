package com.mmedic.rest.medicalService.impl;

import com.mmedic.rest.medicalService.HealthCareService;
import com.mmedic.rest.medicalService.repo.MedicalServiceRepository;
import com.mmedic.rest.medicalService.dto.MedicalServiceDTO;
import com.mmedic.rest.medicalService.mapper.MedicalServiceMapper;
import com.mmedic.spring.errors.MedicException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HealthCareServiceImpl implements HealthCareService {

    private final MedicalServiceRepository medicalServiceRepository;

    private final MedicalServiceMapper medicalServiceMapper;

    @Override
    public List<MedicalServiceDTO> getHealthCarePackagesByGroupId(int groupId) throws MedicException {
        if (groupId < 5) {
            throw new MedicException(MedicException.ERROR_SERVICE_NOT_HEALTH_CARE_TYPE, "This service isn't health care service");
        }
        return medicalServiceMapper.toListMedicalServiceDTO(medicalServiceRepository.findByMedicalServiceGroup_IdAndMedicalServiceGroup_IsDisabledFalseAndIsDeletedFalse(groupId));
    }

    @Override
    public List<MedicalServiceDTO> getAllAvailableServices() throws MedicException {
        return medicalServiceMapper.toListMedicalServiceDTO(medicalServiceRepository.findByIsDeletedIsFalse());
    }
}
