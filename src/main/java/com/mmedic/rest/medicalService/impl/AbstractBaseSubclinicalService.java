package com.mmedic.rest.medicalService.impl;

import com.mmedic.entity.*;
import com.mmedic.enums.DeleteStatus;
import com.mmedic.enums.MedicalServiceGroupType;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.medicalService.repo.MedicalServiceGroupRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServicePriceHistoryRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRepository;
import com.mmedic.rest.medicalService.dto.*;
import com.mmedic.rest.medicalService.mapper.MedicalServiceGroupMapper;
import com.mmedic.rest.medicalService.mapper.MedicalServiceMapper;
import com.mmedic.rest.medicalService.mapper.MedicalServicePriceHistoryMapper;
import com.mmedic.rest.medicalService.mapper.MedicalServiceRegistryMapper;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBaseSubclinicalService {

    @Autowired
    protected MedicalServiceRepository medicalServiceRepository;

    @Autowired
    protected MedicalServiceGroupRepository medicalServiceGroupRepository;

    @Autowired
    protected MedicalServiceMapper medicalServiceMapper;

    @Autowired
    protected MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    @Autowired
    protected BaseService baseService;

    @Autowired
    protected MedicalServiceRegistryMapper medicalServiceRegistryMapper;

    @Autowired
    protected MedicalServicePriceHistoryMapper medicalServicePriceHistoryMapper;

    @Autowired
    protected MedicalServicePriceHistoryRepository medicalServicePriceHistoryRepository;

    @Autowired
    protected MedicalServiceOrderRepository medicalServiceOrderRepository;

    @Autowired
    protected MedicalServiceGroupMapper medicalServiceGroupMapper;

    @Autowired
    protected NotificationService notificationService;


    /**
     * @Author: toantc
     */
    protected abstract Integer getMedicalServiceGroup();


    public List<MedicalServiceDTO> getSubclinicalServiceList() {
        List<MedicalService> list = medicalServiceRepository.
                findByMedicalServiceGroup_IdAndIsDeletedIsFalse(getMedicalServiceGroup());
        List<MedicalServiceDTO> listDTO = new ArrayList<>();
        if (MedicalServiceGroupType.ANALYSIS.getId().equals(getMedicalServiceGroup())) {
            list = list.stream().filter(service -> service.getId() != Constants.SERVICE_TEST_HOME &&
                    service.getId() != Constants.SERVICE_TEST_CLINIC).collect(Collectors.toList());
        }
        list.forEach(r -> {
            MedicalServiceDTO dto = medicalServiceMapper.convertToDTO(r);
            dto.setMedicalServiceGroupId(getMedicalServiceGroup());
            listDTO.add(dto);
        });
        return listDTO;
    }

    @Transactional
    public MedicalServiceDTO createSubclinicalService(MedicalServiceDTO reqDto) {
        medicalServiceRepository.findByNameEqualsAndIsDeletedIsFalse(reqDto.getName()).
                ifPresent(s -> {
                    throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST, "Service already exist with name : "
                            + reqDto.getName());
                });

        MedicalServiceGroup serviceGroup = medicalServiceGroupRepository.
                findById(getMedicalServiceGroup()).orElseThrow(()
                -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Service group not found with id : "
                + getMedicalServiceGroup()));

        MedicalService mS = medicalServiceMapper.convertToEntity(reqDto);
        mS.setMedicalServiceGroup(serviceGroup);
        mS.setIsDeleted(DeleteStatus.AVAILABLE.getValue());
        mS.setCreateAt(LocalDateTime.now());
        MedicalService savedService = medicalServiceRepository.save(mS);

        return medicalServiceMapper.convertToDTO(savedService);
    }

    @Transactional
    public MedicalServiceDTO updateSubclinicalService(Integer id, MedicalServiceDTO reqDto) {
        MedicalService mS = medicalServiceRepository.findByIdAndIsDeletedIsFalse(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Service not found with id : " + id));

        medicalServiceRepository.findByIdIsNotLikeAndNameEqualsAndIsDeletedIsFalse(id, reqDto.getName()).
                ifPresent(s -> {
                    throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST, "Service already exist with name : "
                            + reqDto.getName());
                });

        mS.setName(reqDto.getName());
        mS.setDescription(reqDto.getDescription());
        mS.setUpdateAt(LocalDateTime.now());

        MedicalService savedService = medicalServiceRepository.save(mS);

        return medicalServiceMapper.convertToDTO(savedService);
    }

    @Transactional
    public MedicalServiceDTO deleteSubclinicalService(Integer id) {
        MedicalService mS = medicalServiceRepository.findByIdAndIsDeletedIsFalse(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Service not found with id : " + id));

        mS.setIsDeleted(DeleteStatus.IS_DELETED.getValue());
        mS.setUpdateAt(LocalDateTime.now());

        MedicalService savedService = medicalServiceRepository.save(mS);

        return medicalServiceMapper.convertToDTO(savedService);
    }

    /**
     * @Author: tuyennv
     */
    public List<MedicalServiceDTO> getServiceList() {
        List<MedicalService> list = medicalServiceRepository.
                findByMedicalServiceGroup_IdAndIsDeletedIsFalse(getMedicalServiceGroup());

        List<MedicalServiceDTO> listDTO = new ArrayList<>();
        list.forEach(item -> {
            MedicalServiceDTO rs = medicalServiceMapper.convertToDTO(item);
            rs.setMedicalServiceGroupId(item.getMedicalServiceGroup().getId());
            listDTO.add(rs);
        });
        return listDTO;
    }

    public List<MedicalServiceDTO> getHealthcareServiceList() {
        List<MedicalService> list = medicalServiceRepository.
                findByIsDeletedIsFalse();

        for (int i = 0; i < list.size(); i++) {
            if ((list.get(i).getMedicalServiceGroup().getId() == 1) || (list.get(i).getMedicalServiceGroup().getId() == 2)
                    || (list.get(i).getMedicalServiceGroup().getId() == 3) || (list.get(i).getMedicalServiceGroup().getId() == 4)) {
                list.remove(i);
                i = i - 1;
            }
        }
        List<MedicalServiceDTO> listDTO = new ArrayList<>();
        list.forEach(item -> {
            MedicalServiceDTO rs = medicalServiceMapper.convertToDTO(item);
            rs.setMedicalServiceGroupId(item.getMedicalServiceGroup().getId());
            listDTO.add(rs);
        });
        return listDTO;
    }

    @Transactional
    public MedicalServiceDTO createService(MedicalServiceDTO reqDto) {
        medicalServiceRepository.findByNameEqualsAndIsDeletedIsFalse(reqDto.getName()).
                ifPresent(itemMS -> {
                    throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST, "Service already exist with name : "
                            + reqDto.getName());
                });

        MedicalServiceGroup serviceGroup = medicalServiceGroupRepository.
                findById(reqDto.getMedicalServiceGroupId()).orElseThrow(()
                -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Service group not found with id : "
                + reqDto.getMedicalServiceGroupId()));

        MedicalService mS = medicalServiceMapper.convertToEntity(reqDto);
        mS.setMedicalServiceGroup(serviceGroup);
        mS.setIsDeleted(DeleteStatus.AVAILABLE.getValue());
        mS.setCreateAt(LocalDateTime.now());
        MedicalService savedService = medicalServiceRepository.save(mS);

        MedicalServiceDTO rs = medicalServiceMapper.convertToDTO(savedService);
        rs.setMedicalServiceGroupId(reqDto.getMedicalServiceGroupId());
        return rs;
    }

    @Transactional
    public MedicalServiceDTO updateService(Integer id, MedicalServiceDTO reqDto) {
        MedicalService mS = medicalServiceRepository.findByIdAndIsDeletedIsFalse(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Service not found with id : " + id));

        medicalServiceRepository.findByIdIsNotLikeAndNameEqualsAndIsDeletedIsFalse(id, reqDto.getName()).
                ifPresent(s -> {
                    throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST, "Service already exist with name : "
                            + reqDto.getName());
                });

        mS.setName(reqDto.getName());
        mS.setDescription(reqDto.getDescription());
        mS.setUpdateAt(LocalDateTime.now());
        mS.setIsDeleted(DeleteStatus.AVAILABLE.getValue());

        MedicalService savedService = medicalServiceRepository.save(mS);

        MedicalServiceDTO rs = medicalServiceMapper.convertToDTO(savedService);
        rs.setMedicalServiceGroupId(reqDto.getMedicalServiceGroupId());

        return rs;
    }

    @Transactional
    public MedicalServiceDTO deleteService(Integer id) {
        MedicalService mS = medicalServiceRepository.findByIdAndIsDeletedIsFalse(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Service not found with id : " + id));

        mS.setIsDeleted(DeleteStatus.IS_DELETED.getValue());
        mS.setUpdateAt(LocalDateTime.now());

        MedicalService savedService = medicalServiceRepository.save(mS);

        return medicalServiceMapper.convertToDTO(savedService);
    }

    public List<MedicalServiceFullDTO> getMedicalServiceList(Principal principal) {
        Establishment establishment = baseService.getRawEstablishment(principal);

        List<MedicalService> medicalServices = medicalServiceRepository.findByMedicalServiceGroup_IdAndIsDeletedIsFalse(getMedicalServiceGroup());
        List<MedicalServiceFullDTO> medicalServicesDtos = new ArrayList<>();

        if (MedicalServiceGroupType.ANALYSIS.getId().equals(getMedicalServiceGroup())) {
            medicalServices = medicalServices.stream().
                    filter(service -> service.getId() != Constants.SERVICE_TEST_HOME &&
                            service.getId() != Constants.SERVICE_TEST_CLINIC).collect(Collectors.toList());
        }

        medicalServices.forEach(itemMedicalService -> {
            if (itemMedicalService.getMedicalServiceGroup().getIsDisabled() == false) {
                MedicalServiceFullDTO medicalServiceDto = new MedicalServiceFullDTO();
                medicalServiceDto.setDescription(itemMedicalService.getDescription());
                medicalServiceDto.setName(itemMedicalService.getName());
                medicalServiceDto.setId(itemMedicalService.getId());
                List<MedicalServiceRegistryDTO> medicalServiceRegistryDtos = new ArrayList<>();

                itemMedicalService.getMedicalServiceRegistries().forEach(itemMedicalServiceRegistry -> {
                    if (itemMedicalServiceRegistry.getEstablishment().getId() == establishment.getId()) {
                        MedicalServiceRegistryDTO medicalServiceRegistryDto = medicalServiceRegistryMapper.convertToDTO(itemMedicalServiceRegistry);
                        List<MedicalServicePriceHistoryDTO> medicalServicePriceHistoriesDto = new ArrayList<>();
                        itemMedicalServiceRegistry.getMedicalServicePriceHistories().forEach(itemMSP -> {
                            medicalServicePriceHistoriesDto.add(medicalServicePriceHistoryMapper.convertToDTO(itemMSP));
                        });
                        Collections.sort(medicalServicePriceHistoriesDto, Comparator.comparing(MedicalServicePriceHistoryDTO::getEffectiveAt).reversed());
                        medicalServiceRegistryDto.setMedicalServicePriceHistoriesDTO(medicalServicePriceHistoriesDto);
                        medicalServiceRegistryDtos.add(medicalServiceRegistryDto);
                    }
                });
                medicalServiceDto.setMedicalServiceRegistrieDtos(medicalServiceRegistryDtos);
                medicalServicesDtos.add(medicalServiceDto);
            }
        });
        return medicalServicesDtos;
    }

    @Transactional
    public MedicalServicePriceHistoryDTO updateMedicalServicePrice(Establishment establishment, Integer id, PriceDTO reqDto) {

        // search
        Optional<MedicalServiceRegistry> medicalServiceRegistry = medicalServiceRegistryRepository.findByIdAndEstablishment_Id(id, establishment.getId());
        LocalDateTime now = LocalDateTime.now();
        if (medicalServiceRegistry.isPresent() && checkEffectiveDateExist(now, medicalServiceRegistry.get().getMedicalServicePriceHistories())) {
            // a new record
            MedicalServicePriceHistory data = new MedicalServicePriceHistory();
            data.setMedicalServiceRegistry(medicalServiceRegistry.get());
            data.setPrice(reqDto.getPrice());
            data.setEffectiveAt(LocalDateTime.now());
            MedicalServicePriceHistory savedService = medicalServicePriceHistoryRepository.save(data);

            // Notification
            notificationPriceChangeForPatient(id);
            return medicalServicePriceHistoryMapper.convertToDTO(savedService);
        }
        return null;
    }

    /**
     * Notification price change for patient.
     *
     * @param registryServiceId registry service id is change price
     */
    private void notificationPriceChangeForPatient(int registryServiceId) {
        new Thread(() -> {
            List<PreclinicalNotificationDTO> preclinicalNotifications = medicalServiceOrderRepository.findPreclinicalPriceChange(registryServiceId);
            if (!CollectionUtils.isEmpty(preclinicalNotifications)) {
                notificationService.notificationPreclinicalServicePriceChange(preclinicalNotifications);
            }
        }).start();
    }

    private boolean checkEffectiveDateExist(LocalDateTime effectiveDate, List<MedicalServicePriceHistory> priceHistories) {
        priceHistories.stream().forEach(medicalServicePriceHistory -> System.out.println("####" + medicalServicePriceHistory.getEffectiveAt()));
        return !priceHistories.stream().anyMatch(priceHistory -> priceHistory.getEffectiveAt().isEqual(effectiveDate));
    }

    @Transactional
    public MedicalServiceRegistryDTO updateMedicalServiceStatus(Principal principal, Integer medicalServiceId, StatusDTO reqDto) {

        Establishment establishment = baseService.getRawEstablishment(principal);

        Optional<MedicalService> medicalService = medicalServiceRepository.findById(medicalServiceId);
        if (medicalService.isPresent()) {
            Optional<MedicalServiceRegistry> medicalServiceRegistry = medicalServiceRegistryRepository.
                    findByMedicalService_IdAndEstablishment_Id(medicalServiceId, establishment.getId());
            if (medicalServiceRegistry.isPresent()) {
                // update record
                medicalServiceRegistry.get().setActive(reqDto.isActive());
                MedicalServiceRegistry data = medicalServiceRegistryRepository.save(medicalServiceRegistry.get());
                return medicalServiceRegistryMapper.convertToDTO(data);
            } else {
                // create new record
                MedicalServiceRegistry medicalServiceRegistry1 = new MedicalServiceRegistry();
                medicalServiceRegistry1.setActive(true);
                medicalServiceRegistry1.setEstablishment(establishment);
                medicalServiceRegistry1.setMedicalService(medicalService.get());
                MedicalServiceRegistry data = medicalServiceRegistryRepository.save(medicalServiceRegistry1);
                return medicalServiceRegistryMapper.convertToDTO(data);
            }
        }
        return null;
    }

    public List<MedicalServiceGroupDTO> getServiceGroupsList() {
        List<MedicalServiceGroup> list = medicalServiceGroupRepository.
                findAll();
        for (int i = 0; i < list.size(); i++) {
            if ((list.get(i).getId() == 1) || (list.get(i).getId() == 2) || (list.get(i).getId() == 3 || (list.get(i).getId() == 4)) || (list.get(i).getIsDisabled() == DeleteStatus.IS_DELETED.getValue())) {
                list.remove(i);
                i = i - 1;

            }
        }
        List<MedicalServiceGroupDTO> listDTO = new ArrayList<>();
        list.forEach(r -> listDTO.add(medicalServiceGroupMapper.convertToDTO(r)));
        return listDTO;
    }

    @Transactional
    public MedicalServiceGroupDTO createServiceGroups(Principal principal, MedicalServiceGroupDTO reqDto) {

        medicalServiceGroupRepository.findByNameEqualsAndIsDisabledIsFalse(reqDto.getName()).
                ifPresent(s -> {
                    throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST, "Service Group already exist with name : "
                            + reqDto.getName());
                });
        MedicalServiceGroup mS = medicalServiceGroupMapper.convertToEntity(reqDto);
        MedicalServiceGroup savedServiceGroup = medicalServiceGroupRepository.save(mS);
        mS.setIsDisabled(DeleteStatus.AVAILABLE.getValue());

        return medicalServiceGroupMapper.convertToDTO(savedServiceGroup);
    }


    @Transactional
    public MedicalServiceGroupDTO updateServiceGroups(Principal principal, Integer id, MedicalServiceGroupDTO reqDto) {
        MedicalServiceGroup mS = medicalServiceGroupRepository.findById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Service Group not found with id : " + id));

        medicalServiceGroupRepository.findByIdIsNotLikeAndNameEqualsAndIsDisabledIsFalse(id, reqDto.getName()).
                ifPresent(s -> {
                    throw new MedicException(MedicException.ERROR_SERVICE_ALREADY_EXIST, "Service Group already exist with name : "
                            + reqDto.getName());
                });

        mS.setId(id);
        mS.setDescription(reqDto.getDescription());
        mS.setName(reqDto.getName());
        mS.setUpdateAt(LocalDateTime.now());
        MedicalServiceGroup savedServiceGroup = medicalServiceGroupRepository.save(mS);
        return medicalServiceGroupMapper.convertToDTO(savedServiceGroup);
    }

    @Transactional
    public MedicalServiceGroupDTO deleteServiceGroups(Principal principal, Integer id) {
        MedicalServiceGroup mS = medicalServiceGroupRepository.findById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Service group not found with id : " + id));
        mS.setIsDisabled(DeleteStatus.IS_DELETED.getValue());
        medicalServiceGroupRepository.save(mS);
        return null;
    }


}
