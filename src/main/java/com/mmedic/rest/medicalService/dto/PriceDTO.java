package com.mmedic.rest.medicalService.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PriceDTO {

    private int price;

//    private LocalDateTime effectiveAt;

}
