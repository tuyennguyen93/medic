package com.mmedic.rest.medicalService.dto;

import com.mmedic.enums.TreatmentType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class TreatmentTypeDTO {

    private TreatmentType type;

    private String name;

    private String description;
}
