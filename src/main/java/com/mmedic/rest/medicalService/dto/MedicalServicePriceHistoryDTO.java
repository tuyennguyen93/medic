package com.mmedic.rest.medicalService.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class MedicalServicePriceHistoryDTO {

    private Integer id;

    @JsonIgnore
    private MedicalServiceRegistryDTO medicalServiceRegistryDTO;

    private int price;

    private LocalDateTime effectiveAt;


}
