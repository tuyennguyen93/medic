package com.mmedic.rest.medicalService.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MedicalServiceDTO {

    private Integer id;

    private String name;

    private String description;

    private Integer medicalServiceGroupId;

}
