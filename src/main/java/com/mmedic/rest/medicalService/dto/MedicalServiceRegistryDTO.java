package com.mmedic.rest.medicalService.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmedic.rest.establishment.dto.EstablishmentDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class MedicalServiceRegistryDTO {

    private Integer id;

    @JsonIgnore
    private EstablishmentDTO establishmentDTO;

    @JsonIgnore
    private MedicalServiceDTO medicalServiceDTO;

    private boolean active;

    private List<MedicalServicePriceHistoryDTO> medicalServicePriceHistoriesDTO;


}
