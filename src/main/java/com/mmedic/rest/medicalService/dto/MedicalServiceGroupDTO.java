package com.mmedic.rest.medicalService.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MedicalServiceGroupDTO {

    private Integer id;

    private String name;

    private String description;

}
