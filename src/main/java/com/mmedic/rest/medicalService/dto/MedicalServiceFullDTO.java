package com.mmedic.rest.medicalService.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class MedicalServiceFullDTO {

    private Integer id;

    private String name;

    private String description;

    private List<MedicalServiceRegistryDTO> medicalServiceRegistrieDtos;

}
