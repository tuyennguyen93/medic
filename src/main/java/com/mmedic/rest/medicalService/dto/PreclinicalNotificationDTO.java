package com.mmedic.rest.medicalService.dto;

public interface PreclinicalNotificationDTO {

    int getMedicalRecordId();

    int getPatientId();

    String getPatientName();

    String getNotificationToken();

    String getLanguage();

    int getServiceId();

    String getServiceName();
}
