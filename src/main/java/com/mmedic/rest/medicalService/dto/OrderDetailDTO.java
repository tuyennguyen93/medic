package com.mmedic.rest.medicalService.dto;

import java.time.LocalDateTime;

public interface OrderDetailDTO {

    int getId();

    LocalDateTime getAppointmentDate();

    Integer getSequenceNumber();
}
