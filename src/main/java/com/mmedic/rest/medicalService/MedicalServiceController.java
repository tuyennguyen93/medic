package com.mmedic.rest.medicalService;

import com.mmedic.enums.TreatmentType;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.medicalService.dto.*;
import com.mmedic.rest.medicalService.impl.AnalysisMedicalServiceImp;
import com.mmedic.rest.medicalService.impl.DiagnoseMedicalServiceImp;
import com.mmedic.rest.medicalService.impl.MedicineSupplyServiceImp;
import com.mmedic.spring.errors.MedicException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/services")
@Api(value = "Medical Service Controller", description = "Medical Service Management")
public class MedicalServiceController {


    private DiagnoseMedicalServiceImp diagnoseMedicalService;

    private AnalysisMedicalServiceImp analysisMedicalServiceImp;

    private MedicineSupplyServiceImp medicineSupplyServiceImp;

    private HealthCareService healthCareService;

    private BaseService baseService;

    public MedicalServiceController(DiagnoseMedicalServiceImp diagnoseMedicalService,
                                    AnalysisMedicalServiceImp analysisMedicalServiceImp,
                                    MedicineSupplyServiceImp medicineSupplyServiceImp,
                                    HealthCareService healthCareService,
                                    BaseService baseService) {
        this.diagnoseMedicalService = diagnoseMedicalService;
        this.analysisMedicalServiceImp = analysisMedicalServiceImp;
        this.medicineSupplyServiceImp = medicineSupplyServiceImp;
        this.healthCareService = healthCareService;
        this.baseService = baseService;
    }

    @ApiOperation("Get available services transactions (DONE)")
    @GetMapping("/all")
    public MedicResponse<List<MedicalServiceDTO>> getAllServcies() {
        List<MedicalServiceDTO> result = healthCareService.getAllAvailableServices();
        return MedicResponse.okStatus(result);
    }


    @ApiOperation("Get available services (DONE)")
    @GetMapping("/available-services")
    public MedicResponse<List<MedicalServiceDTO>> geAvailableServiceList() {
        List<MedicalServiceDTO> analysis = analysisMedicalServiceImp.getSubclinicalServiceList();
        List<MedicalServiceDTO> diagnose = diagnoseMedicalService.getSubclinicalServiceList();
        List<MedicalServiceDTO> result = new ArrayList<>();
        result.addAll(analysis);
        result.addAll(diagnose);
        return MedicResponse.okStatus(result);
    }


    @ApiOperation("Get list analysis services (DONE)")
    @GetMapping("/analyses")
    public MedicResponse<List<MedicalServiceDTO>> getAnalysisServiceList() {
        List<MedicalServiceDTO> result = analysisMedicalServiceImp.getSubclinicalServiceList();
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create analysis service (DONE)")
    @PostMapping("/analyses")
    public MedicResponse<MedicalServiceDTO> createAnalysisService(@RequestBody MedicalServiceDTO reqDto) {
        MedicalServiceDTO result = analysisMedicalServiceImp.createSubclinicalService(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update analysis service (DONE)")
    @PutMapping("/analyses/{id}")
    public MedicResponse<MedicalServiceDTO> updateAnalysisService(@PathVariable(value = "id") Integer id, @RequestBody MedicalServiceDTO reqDto) {
        MedicalServiceDTO result = analysisMedicalServiceImp.updateSubclinicalService(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete analysis service (DONE)")
    @DeleteMapping("/analyses/{id}")
    public MedicResponse<MedicalServiceDTO> deleteAnalysisService(@PathVariable("id") Integer id) {
        MedicalServiceDTO result = analysisMedicalServiceImp.deleteSubclinicalService(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get list diagnose services (DONE)")
    @GetMapping("/diagnoses")
    public MedicResponse<List<MedicalServiceDTO>> getServiceList() {
        List<MedicalServiceDTO> result = diagnoseMedicalService.getSubclinicalServiceList();
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create diagnose service (DONE)")
    @PostMapping("/diagnoses")
    public MedicResponse<MedicalServiceDTO> createService(@RequestBody MedicalServiceDTO reqDto) {
        MedicalServiceDTO result = diagnoseMedicalService.createSubclinicalService(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update diagnose service (DONE)")
    @PutMapping("/diagnoses/{id}")
    public MedicResponse<MedicalServiceDTO> updateService(@PathVariable(value = "id") Integer id, @RequestBody MedicalServiceDTO reqDto) {
        MedicalServiceDTO result = diagnoseMedicalService.updateSubclinicalService(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete diagnose service (DONE)")
    @DeleteMapping("/diagnoses/{id}")
    public MedicResponse<MedicalServiceDTO> deleteService(@PathVariable("id") Integer id) {
        MedicalServiceDTO result = diagnoseMedicalService.deleteSubclinicalService(id);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get treatment types (DONE)")
    @GetMapping("/treatment-types")
    public MedicResponse<List<TreatmentTypeDTO>> getTreatmentTypes() {
        // HOME
        TreatmentTypeDTO home = new TreatmentTypeDTO();
        home.setType(TreatmentType.HOME);
        home.setName("Khám tại nhà");
        home.setDescription("Bác sĩ sẽ đến tận nhà để khám và chữa bệnh. Dịch vụ này sẽ có thu thêm phí.");

        // CLINIC
        TreatmentTypeDTO clinic = new TreatmentTypeDTO();
        clinic.setType(TreatmentType.CLINIC);
        clinic.setName("Khám tại phòng mạch");
        clinic.setDescription("Bạn sẽ đến tại phòng mạch bác sĩ để khám chữa bệnh.");
        return MedicResponse.okStatus(Arrays.asList(home, clinic));
    }

    @ApiOperation("Get list analysis medical services (DONE)")
    @GetMapping("/medical-services/analysis")
    public MedicResponse<List<MedicalServiceFullDTO>> getAnalysisMedicalServiceList(Principal principal) {
        List<MedicalServiceFullDTO> result = analysisMedicalServiceImp.getMedicalServiceList(principal);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get list diagnose medical services (DONE)")
    @GetMapping("/medical-services/diagnoses")
    public MedicResponse<List<MedicalServiceFullDTO>> getDiagnoseMedicalServiceList(Principal principal) {
        List<MedicalServiceFullDTO> result = diagnoseMedicalService.getMedicalServiceList(principal);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update medical services price (DONE)")
    @PutMapping("/medical-services/price/{id}")
    public MedicResponse<MedicalServicePriceHistoryDTO> updateMedicalServicePrice(@PathVariable(value = "id") @ApiParam("medicalServiceRegistry_Id") Integer id,
                                                                                  @RequestBody PriceDTO reqDto) {
        MedicalServicePriceHistoryDTO result = diagnoseMedicalService.updateMedicalServicePrice(baseService.getCurrentEstablishment(), id, reqDto);
        if (result == null) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_MEDICALSERVICE_REGISTRY, "Can't find medical Service Registry data or time after the current day");
        }
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update medical services status (DONE)")
    @PutMapping("/medical-services/{id}/status")
    public MedicResponse<MedicalServiceRegistryDTO> updateMedicalServiceStatus(Principal principal, @PathVariable(value = "id") @ApiParam("medicalService_Id") Integer id, @RequestBody StatusDTO reqDto) {
        MedicalServiceRegistryDTO result = diagnoseMedicalService.updateMedicalServiceStatus(principal, id, reqDto);
        if (result == null) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_MEDICALSERVICE_REGISTRY, "Can't find medical Service Registry data");
        }
        return MedicResponse.okStatus(result);
    }

    // group services => save con

    @ApiOperation("Get list services packages (DONE)")
    @GetMapping("/servicegroups")
    public MedicResponse<List<MedicalServiceDTO>> getHealthcareServiceList() {
        List<MedicalServiceDTO> result = medicineSupplyServiceImp.getHealthcareServiceList();
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create services packages (DONE)")
    @PostMapping("/servicegroups")
    public MedicResponse<MedicalServiceDTO> createHealthcareService(@RequestBody MedicalServiceDTO reqDto) {
        MedicalServiceDTO result = medicineSupplyServiceImp.createService(reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update services packages (DONE)")
    @PutMapping("/servicegroups/{id}")
    public MedicResponse<MedicalServiceDTO> updateHealthcareService(@PathVariable(value = "id") Integer id, @RequestBody MedicalServiceDTO reqDto) {
        MedicalServiceDTO result = medicineSupplyServiceImp.updateService(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete services packages (DONE)")
    @DeleteMapping("/servicegroups/{id}")
    public MedicResponse<MedicalServiceDTO> deleteHealthcareService(@PathVariable("id") Integer id) {
        MedicalServiceDTO result = medicineSupplyServiceImp.deleteService(id);
        return MedicResponse.okStatus(result);
    }

    // medicine-supplies => cha
    @ApiOperation("Get list medicine Supply services (DONE)")
    @GetMapping("/medicine-supplies")
    public MedicResponse<List<MedicalServiceGroupDTO>> getServiceGroupsList() {
        List<MedicalServiceGroupDTO> result = diagnoseMedicalService.getServiceGroupsList();
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Create  medicine Supply  (DONE)")
    @PostMapping("/medicine-supplies")
    public MedicResponse<MedicalServiceGroupDTO> createServiceGroups(Principal principal, @RequestBody MedicalServiceGroupDTO reqDto) {
        MedicalServiceGroupDTO result = analysisMedicalServiceImp.createServiceGroups(principal, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update  medicine Supply  (DONE)")
    @PutMapping("/medicine-supplies/{id}")
    public MedicResponse<MedicalServiceGroupDTO> updateServiceGroups(Principal principal, @PathVariable(value = "id") Integer id, @RequestBody MedicalServiceGroupDTO reqDto) {
        MedicalServiceGroupDTO result = analysisMedicalServiceImp.updateServiceGroups(principal, id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Delete  medicine Supply  (DONE)")
    @DeleteMapping("/medicine-supplies/{id}")
    public MedicResponse<MedicalServiceGroupDTO> deleteServiceGroups(Principal principal, @PathVariable("id") Integer id) {
        analysisMedicalServiceImp.deleteServiceGroups(principal, id);
        return MedicResponse.okStatus(null);
    }

    @ApiOperation("Get Health Care packages (DONE)")
    @GetMapping("/servicegroups/{id}/services")
    public MedicResponse<List<MedicalServiceDTO>> getHealthCarePackagesByGroupId(@PathVariable("id") int groupId) {
        return MedicResponse.okStatus(healthCareService.getHealthCarePackagesByGroupId(groupId));
    }

}
