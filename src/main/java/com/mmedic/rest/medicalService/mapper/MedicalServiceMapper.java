package com.mmedic.rest.medicalService.mapper;

import com.mmedic.entity.MedicalService;
import com.mmedic.rest.medicalService.dto.MedicalServiceDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MedicalServiceMapper {


    MedicalServiceDTO convertToDTO(MedicalService entity);

    MedicalService convertToEntity(MedicalServiceDTO dto);

    List<MedicalServiceDTO> toListMedicalServiceDTO(List<MedicalService> medicalServices);

}
