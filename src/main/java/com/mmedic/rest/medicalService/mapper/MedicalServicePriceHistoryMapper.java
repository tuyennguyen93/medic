package com.mmedic.rest.medicalService.mapper;

import com.mmedic.entity.MedicalServicePriceHistory;
import com.mmedic.rest.medicalService.dto.MedicalServicePriceHistoryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MedicalServicePriceHistoryMapper {


    MedicalServicePriceHistoryDTO convertToDTO(MedicalServicePriceHistory entity);

    MedicalServicePriceHistory convertToEntity(MedicalServicePriceHistoryDTO dto);

}
