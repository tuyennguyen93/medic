package com.mmedic.rest.medicalService.mapper;

import com.mmedic.entity.MedicalServiceGroup;
import com.mmedic.rest.medicalService.dto.MedicalServiceGroupDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MedicalServiceGroupMapper {


    MedicalServiceGroupDTO convertToDTO(MedicalServiceGroup entity);

    MedicalServiceGroup convertToEntity(MedicalServiceGroupDTO dto);

}
