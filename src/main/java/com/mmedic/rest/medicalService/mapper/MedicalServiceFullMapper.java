package com.mmedic.rest.medicalService.mapper;

import com.mmedic.entity.MedicalService;
import com.mmedic.rest.medicalService.dto.MedicalServiceFullDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MedicalServiceFullMapper {


    MedicalServiceFullDTO convertToDTO(MedicalService entity);

    MedicalService convertToEntity(MedicalServiceFullDTO dto);

}
