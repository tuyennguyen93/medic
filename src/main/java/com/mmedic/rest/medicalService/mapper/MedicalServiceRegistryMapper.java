package com.mmedic.rest.medicalService.mapper;

import com.mmedic.entity.MedicalServiceRegistry;
import com.mmedic.rest.medicalService.dto.MedicalServiceRegistryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MedicalServiceRegistryMapper {


    MedicalServiceRegistryDTO convertToDTO(MedicalServiceRegistry entity);

    MedicalServiceRegistry convertToEntity(MedicalServiceRegistryDTO dto);

}
