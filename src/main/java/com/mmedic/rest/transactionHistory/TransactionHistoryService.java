package com.mmedic.rest.transactionHistory;

import com.mmedic.entity.Establishment;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.medicalOrderServiceDetail.repo.MedicalOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.transactionHistory.dto.EstablishmentTransactionHistoryDTO;
import com.mmedic.rest.transactionHistory.dto.TransactionHistoryDTO;
import com.mmedic.rest.transactionHistory.dto.TransactionStatisticDTO;
import com.mmedic.rest.transactionHistory.mapper.TransactionHistoryMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Transaction history service.
 */
@Service
@RequiredArgsConstructor
public class TransactionHistoryService {

    private final BaseService baseService;

    private final MedicalOrderDetailRepository medicalOrderDetailRepository;

    private final TransactionHistoryMapper transactionHistoryMapper;

    private final MedicalRecordRepository medicalRecordRepository;


    public Page<TransactionHistoryDTO> getTransactionHistory(Principal principal, Pageable pageable, int medicalServiceId, LocalDateTime fromDay, LocalDateTime untilDay) {
        Establishment establishment = baseService.getRawEstablishment(principal);

        Page<MedicalServiceOrderDetail> medicalServiceOrderDetails = medicalOrderDetailRepository.findByEstablishmentBetween(pageable, medicalServiceId, establishment.getId(), fromDay, untilDay);
        final Page<TransactionHistoryDTO> listDto = medicalServiceOrderDetails.map(transactionHistoryMapper::convertModelToDTO);

        return listDto;
    }

    public Page<EstablishmentTransactionHistoryDTO> getTransHistoryByAdmin(Pageable pageable, String establishmentName, Integer serviceId, Integer month, Integer year) {
        Page<EstablishmentTransactionHistoryDTO> medicalServiceOrderDetails = medicalOrderDetailRepository.getTransHistoryByAdmin(pageable, establishmentName, serviceId, month, year);
        return medicalServiceOrderDetails;
    }

    public Page<TransactionHistoryDTO> getTransHistoryByEstablishment(Pageable pageable, Integer establishmentId, Integer serviceId, Integer month, Integer year) {
        Page<MedicalServiceOrderDetail> medicalServiceOrderDetails = medicalOrderDetailRepository.getTransHistoryByEstablishment(pageable, establishmentId, serviceId, month, year);
        final Page<TransactionHistoryDTO> lstDto = medicalServiceOrderDetails.map(transactionHistoryMapper::convertModelToDTO);

        return lstDto;
    }

    public TransactionStatisticDTO getStatisticByAdmin(String establishmentName, Integer serviceId, Integer month, Integer year) {
        TransactionStatisticDTO medicalServiceOrderDetails = medicalOrderDetailRepository.getStatisticByAdmin(establishmentName, serviceId, month, year);

        return medicalServiceOrderDetails;
    }

    public TransactionStatisticDTO getStatisticByEstablishment(Integer establishmentId, Integer serviceId, Integer month, Integer year) {
        TransactionStatisticDTO medicalServiceOrderDetails = medicalOrderDetailRepository.getStatisticByEstablishment(establishmentId, serviceId, month, year);

        return medicalServiceOrderDetails;
    }

    public List<TransactionHistoryDTO> getByMedicalRecord(Integer medicalRecordId) {
        Optional<MedicalRecord> medicalRecord = medicalRecordRepository.findById(medicalRecordId);
        final Integer EXAMINE = 1, PRECLINICAL = 2, PRESCRIPTION = 3, SERVICE = 4;

        if (medicalRecord.isPresent()) {
            List<TransactionHistoryDTO> medicalRecordTransactionHistoryDTO = new ArrayList<>();

            // MedicalRecord -> ExamineRecord -> MedicalServiceOrder -> List<MedicalServiceOrderDetail>
            if (medicalRecord.get().getExamineRecord() != null && medicalRecord.get().getExamineRecord().getMedicalServiceOrder() != null) {
                List<TransactionHistoryDTO> medicalServiceExamineRecords = transactionHistoryMapper.convertListModelToDTO(medicalRecord.get().getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails());
                for (TransactionHistoryDTO dto : medicalServiceExamineRecords) {
                    dto.setType(EXAMINE);
                }
                medicalRecordTransactionHistoryDTO.addAll(medicalServiceExamineRecords);
            }

            // MedicalRecord -> PreclinicalRecord -> MedicalServiceOrder -> List<MedicalServiceOrderDetail>
            if (medicalRecord.get().getPreclinicalRecord() != null && medicalRecord.get().getPreclinicalRecord().getMedicalServiceOrder() != null) {
                List<TransactionHistoryDTO> medicalServicePreclinicalRecords = transactionHistoryMapper.convertListModelToDTO(medicalRecord.get().getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails());

                for (TransactionHistoryDTO dto : medicalServicePreclinicalRecords) {
                    dto.setType(PRECLINICAL);
                }
                medicalRecordTransactionHistoryDTO.addAll(medicalServicePreclinicalRecords);
            }


            // MedicalRecord -> PrescriptionRecord -> MedicalServiceOrder -> List<MedicalServiceOrderDetail>
            if (medicalRecord.get().getPrescriptionRecord() != null && medicalRecord.get().getPrescriptionRecord().getMedicalServiceOrder() != null) {
                List<TransactionHistoryDTO> prescriptionRecords = transactionHistoryMapper.convertListModelToDTO(medicalRecord.get().getPrescriptionRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails());

                for (TransactionHistoryDTO dto : prescriptionRecords) {
                    dto.setType(PRESCRIPTION);
                }
                medicalRecordTransactionHistoryDTO.addAll(prescriptionRecords);
            }

            // MedicalRecord -> ServiceRecord -> MedicalServiceOrder -> List<MedicalServiceOrderDetail>
            if (medicalRecord.get().getServiceRecord() != null && medicalRecord.get().getServiceRecord().getMedicalServiceOrder() != null) {
                List<TransactionHistoryDTO> serviceRecord = transactionHistoryMapper.convertListModelToDTO(medicalRecord.get().getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails());
                for (TransactionHistoryDTO dto : serviceRecord) {
                    dto.setType(SERVICE);
                }
                medicalRecordTransactionHistoryDTO.addAll(serviceRecord);
            }
            return medicalRecordTransactionHistoryDTO;
        }
        return null;
    }


}
