package com.mmedic.rest.transactionHistory;

import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.transactionHistory.dto.EstablishmentTransactionHistoryDTO;
import com.mmedic.rest.transactionHistory.dto.TransactionHistoryDTO;
import com.mmedic.rest.transactionHistory.dto.TransactionStatisticDTO;
import com.mmedic.spring.errors.MedicException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Transaction history controller.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/transaction-histories")
@Api(value = "Transaction history Controller", description = "Transaction History Management")
@RequiredArgsConstructor
public class TransactionHistoryController {

    private final TransactionHistoryService medicalServiceDetailOrderService;

    @ApiOperation("Get transaction history of a time period(DONE)")
    @GetMapping("/")
    public MedicResponse<Page<TransactionHistoryDTO>> getTransactionHistory(Principal principal, Pageable pageable,
                                                                            @RequestParam(value = "medicalServiceId", required = false) @ApiParam("Medical Service Id") Integer medicalServiceId,
                                                                            @RequestParam("fromDate") @ApiParam("fromDate") LocalDateTime fromDate, @RequestParam("untilDate") @ApiParam("untilDate") LocalDateTime untilDate) {
        if (untilDate.isBefore(fromDate)) {
            throw new MedicException(MedicException.ERROR_FROM_DAY_AND_UNTIL_DAY_INVALID, "Data from day must is before until day ");
        }

        medicalServiceId = medicalServiceId == null ? -1 : medicalServiceId;
        Page<TransactionHistoryDTO> result = medicalServiceDetailOrderService.getTransactionHistory(principal, pageable, medicalServiceId, fromDate, untilDate);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get all transaction history by admin (DONE)")
    @GetMapping("/admin")
    public MedicResponse<Page<EstablishmentTransactionHistoryDTO>> getTransHistoryByAdmin(Pageable pageable,
                                                                                          @RequestParam(value = "establishmentName", required = false) @ApiParam("Establishment Name") String establishmentName,
                                                                                          @RequestParam(value = "serviceId", required = false) @ApiParam("Medical Service Id") Integer serviceId,
                                                                                          @RequestParam("month") @ApiParam("Month") Integer month,
                                                                                          @RequestParam("year") @ApiParam("Year") Integer year) {
        serviceId = serviceId == null ? -1 : serviceId;
        establishmentName = establishmentName == null ? "" : establishmentName;
        Page<EstablishmentTransactionHistoryDTO> result = medicalServiceDetailOrderService.getTransHistoryByAdmin(pageable, establishmentName, serviceId, month, year);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get all transaction history by establishment (DONE)")
    @GetMapping("/admin/{establishmentId}")
    public MedicResponse<Page<TransactionHistoryDTO>> getTransHistoryByEstablishment(Pageable pageable,
                                                                                     @PathVariable("establishmentId") Integer establishmentId,
                                                                                     @RequestParam(value = "serviceId", required = false) @ApiParam("Medical Service Id") Integer medicalServiceId,
                                                                                     @RequestParam("month") @ApiParam("Month") Integer month,
                                                                                     @RequestParam("year") @ApiParam("Year") Integer year) {
        medicalServiceId = medicalServiceId == null ? -1 : medicalServiceId;
        Page<TransactionHistoryDTO> result = medicalServiceDetailOrderService.getTransHistoryByEstablishment(pageable, establishmentId, medicalServiceId, month, year);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get statistic transaction history by admin (DONE)")
    @GetMapping("/admin/statistic")
    public MedicResponse<TransactionStatisticDTO> getStatisticByAdmin(@RequestParam(value = "establishmentName", required = false) @ApiParam("Establishment Name") String establishmentName,
                                                                      @RequestParam(value = "serviceId", required = false) @ApiParam("Medical Service Id") Integer serviceId,
                                                                      @RequestParam("month") @ApiParam("Month") Integer month,
                                                                      @RequestParam("year") @ApiParam("Year") Integer year) {
        serviceId = serviceId == null ? -1 : serviceId;
        establishmentName = establishmentName == null ? "" : establishmentName;
        TransactionStatisticDTO result = medicalServiceDetailOrderService.getStatisticByAdmin(establishmentName, serviceId, month, year);

        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get statistic transaction history by establishment id (DONE)")
    @GetMapping("/admin/statistic/{establishmentId}")
    public MedicResponse<TransactionStatisticDTO> getStatisticByEstablishment(@PathVariable("establishmentId") Integer establishmentId,
                                                                              @RequestParam(value = "serviceId", required = false) @ApiParam("Medical Service Id") Integer serviceId,
                                                                              @RequestParam("month") @ApiParam("Month") Integer month,
                                                                              @RequestParam("year") @ApiParam("Year") Integer year) {
        serviceId = serviceId == null ? -1 : serviceId;
        TransactionStatisticDTO result = medicalServiceDetailOrderService.getStatisticByEstablishment(establishmentId, serviceId, month, year);

        return MedicResponse.okStatus(result);
    }


    @ApiOperation("Get transaction history of medical record (DONE)")
    @GetMapping("/medical-records/{medicalRecordId}")
    public MedicResponse<List<TransactionHistoryDTO>> getByMedicalRecord(@PathVariable("medicalRecordId") Integer medicalRecordId) {
        List<TransactionHistoryDTO> result = medicalServiceDetailOrderService.getByMedicalRecord(medicalRecordId);
        return MedicResponse.okStatus(result);
    }

}
