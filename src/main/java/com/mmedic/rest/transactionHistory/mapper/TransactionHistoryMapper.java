package com.mmedic.rest.transactionHistory.mapper;

import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.rest.medicalService.dto.MedicalServiceDTO;
import com.mmedic.rest.medicalService.mapper.MedicalServiceMapper;
import com.mmedic.rest.transactionHistory.dto.TransactionHistoryDTO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionHistoryMapper {

    private final MedicalServiceMapper medicalServiceMapper;
    public List<TransactionHistoryDTO> convertListModelToDTO(List<MedicalServiceOrderDetail> medicalServiceOrderDetails) {
        List<TransactionHistoryDTO> dtos = new ArrayList<>();
        medicalServiceOrderDetails.forEach(item -> {
            dtos.add(convertModelToDTO(item));
        });
        return dtos;
    }

    public TransactionHistoryDTO convertModelToDTO(MedicalServiceOrderDetail item) {
        TransactionHistoryDTO medic = new TransactionHistoryDTO();
        medic.setId(item.getId());
        medic.setMedicalRecordId(this.getMedicalRecordId(item));
        medic.setPaymentDate(null == item.getMedicalServiceOrder() ? null : item.getMedicalServiceOrder().getPaymentDate());
        medic.setHandleBy(null == item.getHandleBy() ? StringUtils.EMPTY : item.getHandleBy().getName());
        medic.setEstablishmentId(null == item.getEstablishment() ? null : item.getEstablishment().getId());
        medic.setTxAmount(item.getTxAmount());
        medic.setPatientName(this.getPatientName(item));
        MedicalServiceDTO meDto = medicalServiceMapper.convertToDTO(item.getMedicalService());
        meDto.setMedicalServiceGroupId(item.getMedicalService().getMedicalServiceGroup().getId());
        medic.setMedicalService(meDto);

        return medic;
    }

    private Integer getMedicalRecordId(MedicalServiceOrderDetail orderDetail) {

        MedicalServiceOrder serviceOrder = orderDetail.getMedicalServiceOrder();
        if (null != serviceOrder.getExamineRecord()) {
            return serviceOrder.getExamineRecord().getMedicalRecord().getId();
        } else if (null != serviceOrder.getPreclinicalRecord()) {
            return serviceOrder.getPreclinicalRecord().getMedicalRecord().getId();
        } else if (null != serviceOrder.getPrescriptionRecord()) {
            return null != serviceOrder.getPrescriptionRecord().getMedicalRecord() ? serviceOrder.getPrescriptionRecord().getMedicalRecord().getId() : null;
        } else if (null != serviceOrder.getServiceRecord()) {
            return serviceOrder.getServiceRecord().getMedicalRecord().getId();
        }

        return null;
    }

    private String getPatientName(MedicalServiceOrderDetail orderDetail) {

        MedicalServiceOrder serviceOrder = orderDetail.getMedicalServiceOrder();
        if (null != serviceOrder.getExamineRecord()) {
            return serviceOrder.getExamineRecord().getMedicalRecord().getPatient().getName();
        } else if (null != serviceOrder.getPreclinicalRecord()) {
            return serviceOrder.getPreclinicalRecord().getMedicalRecord().getPatient().getName();
        } else if (null != serviceOrder.getPrescriptionRecord()) {
            return null != serviceOrder.getPrescriptionRecord().getMedicalRecord() ? serviceOrder.getPrescriptionRecord().getMedicalRecord().getPatient().getName() : null;
        } else if (null != serviceOrder.getServiceRecord()) {
            return serviceOrder.getServiceRecord().getMedicalRecord().getPatient().getName();
        }

        return null;
    }
}
