package com.mmedic.rest.transactionHistory.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionStatisticDTO {

    @ApiModelProperty("Total Transaction")
    private Long totalTransaction;

    @ApiModelProperty("Total Amount")
    private Long totalAmount;
}
