package com.mmedic.rest.transactionHistory.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EstablishmentTransactionHistoryDTO {

    @ApiModelProperty("Establishment Id")
    private Integer establishmentId;

    @ApiModelProperty("Establishment Name")
    private String establishmentName;

    @ApiModelProperty("Total Transaction")
    private Long totalTransaction;

    @ApiModelProperty("Total Amount")
    private Long totalAmount;
}
