package com.mmedic.rest.transactionHistory.dto;

import com.mmedic.rest.medicalService.dto.MedicalServiceDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class TransactionHistoryDTO {

    @ApiModelProperty("Service Order Detail Id")
    private Integer id;

    @ApiModelProperty("Medical Record Id")
    private Integer medicalRecordId;

    @ApiModelProperty("Establishment Id")
    private Integer establishmentId;

    @ApiModelProperty("Patient Name")
    private String patientName;

    @ApiModelProperty("Payment Date")
    private LocalDateTime paymentDate;

    @ApiModelProperty("Handle By")
    private String handleBy;

    @ApiModelProperty("Medical Service")
    private MedicalServiceDTO medicalService;

    @ApiModelProperty("Amount")
    private Integer txAmount;

    @ApiModelProperty("Type: EXAMINE = 1, PRECLINICAL = 2, PRESCRIPTION = 3, SERVICE = 4")
    private Integer type;
}
