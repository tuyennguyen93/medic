package com.mmedic.rest.travelprice.repo;

import com.mmedic.entity.TravelPricePolicy;
import com.mmedic.rest.sysSetting.dto.TravelPriceSettingDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TravelPricePolicyRepository extends JpaRepository<TravelPricePolicy, Integer> {

    /**
     * Get travel price.
     *
     * @return travel price policy
     */
    @Query("SELECT travelPrice FROM TravelPricePolicy travelPrice ORDER BY travelPrice.order ASC")
    List<TravelPricePolicy> findAll();

    /**
     * Get travel price setting dto.
     *
     * @return travel price list
     */
    @Query("SELECT NEW com.mmedic.rest.sysSetting.dto.TravelPriceSettingDTO(" +
            "    travelPrice.id, " +
            "    travelPrice.addedKm, " +
            "    travelPrice.pricePerKm " +
            ") " +
            "FROM TravelPricePolicy travelPrice ORDER BY travelPrice.order ASC")
    List<TravelPriceSettingDTO> findAllTravelPriceDTO();

    /**
     * Get current order.
     *
     * @return current order
     */
    @Query("SELECT MAX(travelPrice.order) + 1 AS currentOrder FROM TravelPricePolicy travelPrice ")
    Integer findCurrentMaxOrder();
}
