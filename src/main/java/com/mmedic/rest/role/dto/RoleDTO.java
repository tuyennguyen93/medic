package com.mmedic.rest.role.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * Role data transfer object.
 *
 * @author hungp
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class RoleDTO {
    private String name;

    private List<RolePermissionDTO> rolePermissions;
}
