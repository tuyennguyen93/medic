package com.mmedic.rest.role.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Role Permission data transfer object.
 *
 * @author hungp
 */
@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class RolePermissionDTO {
    private String scope;

    private String condition;

    private String conditionValue;
}
