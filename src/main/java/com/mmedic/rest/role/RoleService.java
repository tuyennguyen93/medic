package com.mmedic.rest.role;

import com.mmedic.entity.Role;

import java.util.Optional;

/**
 * Role Service interface.
 *
 * @author hungp
 */
public interface RoleService {

    /**
     * Find role by name.
     *
     * @param name the role name
     * @return Role instance
     */
    Optional<Role> findRoleByName(String name);
}
