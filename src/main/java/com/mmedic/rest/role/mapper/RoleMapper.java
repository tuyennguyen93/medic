package com.mmedic.rest.role.mapper;

import com.mmedic.entity.Role;
import com.mmedic.entity.RolePermission;
import com.mmedic.rest.role.dto.RoleDTO;
import com.mmedic.rest.role.dto.RolePermissionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Role mapper.
 *
 * @author hungp
 */
@Mapper
public interface RoleMapper {

    RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);

    RoleDTO toRoleDto(Role role);

    Role toRole(RoleDTO roleDTO);

    List<RolePermissionDTO> toListRolePermissionDto(List<RolePermission> rolePermissions);

    List<RolePermission> toListRolePermission(List<RolePermissionDTO> rolePermissions);
}
