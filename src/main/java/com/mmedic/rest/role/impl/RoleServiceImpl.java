package com.mmedic.rest.role.impl;

import com.mmedic.entity.Role;
import com.mmedic.rest.role.RoleService;
import com.mmedic.rest.role.repo.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Role Service implementation.
 *
 * @author hungp
 */
@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Optional<Role> findRoleByName(String name) {
        return roleRepository.findRoleByName(name);
    }
}
