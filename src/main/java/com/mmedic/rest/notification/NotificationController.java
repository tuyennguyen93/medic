package com.mmedic.rest.notification;

import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.common.BaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Notification Controller.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/notifications")
@Api(value = "Notification Controller")
@RequiredArgsConstructor
public class NotificationController {

    private final BaseService baseService;

    private final AccountService accountService;

//    /**
//     * Subscribe receive notification.
//     *
//     * @param token registration token
//     * @return number of token is subscribed
//     */
//    @ApiOperation("Subscribe receive notification (DONE)")
//    @PostMapping("/subscribe")
//    public MedicResponse<Integer> subscribeTopic(@RequestBody String token) {
//        return MedicResponse.okStatus(firebaseNotification.subscribeTopic(FirebaseNotification.TEST_TOPIC, token));
//    }
//
//    /**
//     * Unsubscribe to receive notification.
//     *
//     * @param token registration token
//     * @return number of token is subscribed
//     */
//    @ApiOperation("Unsubscribe receive notification (DONE)")
//    @DeleteMapping("/unsubscribe")
//    public MedicResponse<Integer> unsubscribeTopic(@RequestBody String token) {
//        return MedicResponse.okStatus(firebaseNotification.unsubscribeTopic(FirebaseNotification.TEST_TOPIC, token));
//    }

    /**
     * Get notification token.
     *
     * @return the notification token
     */
    @ApiOperation("Get notification token (DONE)")
    @GetMapping("/registration-token")
    public MedicResponse<List<String>> getNotificationToken() {
        return MedicResponse.okStatus(accountService.getNotificationToken(baseService.getCurrentAccountLogin()));
    }

    /**
     * Registration token to receive notification.
     *
     * @param token registration token
     * @return true if registration is success
     */
    @ApiOperation("Registration token to receive notification (DONE)")
    @PostMapping("/registration-token")
    public MedicResponse<Boolean> registrationToken(@RequestBody String token) {
        return MedicResponse.okStatus(accountService.registrationTokenForNotification(token, baseService.getCurrentAccountLogin()));
    }

    /**
     * Registration token to receive notification.
     *
     * @param token registration token
     * @return true if registration is success
     */
    @ApiOperation("Registration token to receive notification (DONE)")
    @PutMapping("/registration-token")
    public MedicResponse<Boolean> appendRegistrationToken(@RequestBody String token) {
        return MedicResponse.okStatus(accountService.appendRegistrationTokenForNotification(token, baseService.getCurrentAccountLogin()));
    }

    /**
     * Remove registration token to receive notification.
     *
     * @param token registration token
     * @return true if registration token is removed successfully
     */
    @ApiOperation("Registration token to receive notification (DONE)")
    @DeleteMapping("/registration-token")
    public MedicResponse<Boolean> removeRegistrationToken(@RequestBody String token) {
        return MedicResponse.okStatus(accountService.removeRegistrationTokenForNotification(token));
    }
}
