package com.mmedic.rest.notification;

import com.google.firebase.messaging.Notification;
import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.entity.Patient;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.NotificationType;
import com.mmedic.message.Message;
import com.mmedic.rest.doctor.dto.HealthCareNotificationDTO;
import com.mmedic.rest.drug.dto.PrescriptionNotificationDTO;
import com.mmedic.rest.establishment.dto.ExamineNotificationDTO;
import com.mmedic.rest.medicalService.dto.PreclinicalNotificationDTO;
import com.mmedic.service.firebase.FirebaseNotification;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Notification service.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationService {

    private static final String NOTIFICATION_TREATMENT_TITLE = "notification.treatment.title";
    private static final String NOTIFICATION_TREATMENT_BODY = "notification.treatment.body";
    private static final String NOTIFICATION_PRECLINICAL_TITLE = "notification.preclinical.title";
    private static final String NOTIFICATION_PRECLINICAL_BODY = "notification.preclinical.body";
    private static final String NOTIFICATION_PRECLINICAL_PAYMENT_TITLE = "notification.preclinical.payment.title";
    private static final String NOTIFICATION_PRECLINICAL_PAYMENT_BODY = "notification.preclinical.payment.body";
    private static final String NOTIFICATION_PRECLINICAL_STATUS_TITLE = "notification.preclinical.status.title";
    private static final String NOTIFICATION_PRECLINICAL_STATUS_BODY = "notification.preclinical.status.body";
    private static final String NOTIFICATION_PRECLINICAL_RESULT_TITLE = "notification.preclinical.result.title";
    private static final String NOTIFICATION_PRECLINICAL_RESULT_BODY = "notification.preclinical.result.body";
    private static final String NOTIFICATION_PRESCRIPTION_TITLE = "notification.prescription.title";
    private static final String NOTIFICATION_PRESCRIPTION_BODY = "notification.prescription.body";
    private static final String NOTIFICATION_PRESCRIPTION_PAYMENT_TITLE = "notification.prescription.payment.title";
    private static final String NOTIFICATION_PRESCRIPTION_PAYMENT_BODY = "notification.prescription.payment.body";
    private static final String NOTIFICATION_PRESCRIPTION_STATUS_TITLE = "notification.prescription.status.title";
    private static final String NOTIFICATION_PRESCRIPTION_STATUS_BODY = "notification.prescription.status.body";
    private static final String NOTIFICATION_HEALTHCARE_TITLE = "notification.healthcare.title";
    private static final String NOTIFICATION_HEALTHCARE_BODY = "notification.healthcare.body";
    private static final String NOTIFICATION_HEALTHCARE_STATUS_TITLE = "notification.healthcare.status.title";
    private static final String NOTIFICATION_HEALTHCARE_STATUS_BODY = "notification.healthcare.status.body";
    private static final String NOTIFICATION_HEALTHCARE_PAYMENT_TITLE = "notification.healthcare.payment.title";
    private static final String NOTIFICATION_HEALTHCARE_PAYMENT_BODY = "notification.healthcare.payment.body";
    private static final String NOTIFICATION_PRESCRIPTION_PRICE_TITLE = "notification.prescription.price.title";
    private static final String NOTIFICATION_PRESCRIPTION_PRICE_BODY = "notification.prescription.price.body";
    private static final String NOTIFICATION_PRECLINICAL_PRICE_TITLE = "notification.preclinical.price.title";
    private static final String NOTIFICATION_PRECLINICAL_PRICE_BODY = "notification.preclinical.price.body";
    private static final String NOTIFICATION_EXAMINE_PRICE_TITLE = "notification.examine.price.title";
    private static final String NOTIFICATION_EXAMINE_PRICE_BODY = "notification.examine.price.body";
    private static final String NOTIFICATION_HEALTHCARE_PRICE_TITLE = "notification.healthcare.price.title";
    private static final String NOTIFICATION_HEALTHCARE_PRICE_BODY = "notification.healthcare.price.body";
    private static final String NOTIFICATION_STATUS_PROCESSING = "notification.status.processing";
    private static final String NOTIFICATION_STATUS_FULFILLED = "notification.status.fulfilled";
    private static final String NOTIFICATION_STATUS_DENIED = "notification.status.denied";
    private static final String NOTIFICATION_STATUS_CANCELED = "notification.status.canceled";
    private static final String NOTIFICATION_STATUS_ACCEPTED = "notification.status.accepted";
    private static final String NOTIFICATION_TRANSPORT_TREATMENT_TITLE = "notification.transport.treatment.title";
    private static final String NOTIFICATION_TRANSPORT_TREATMENT_BODY = "notification.transport.treatment.body";
    private static final String NOTIFICATION_TRANSPORT_PRECLINICAL_TITLE = "notification.transport.preclinical.title";
    private static final String NOTIFICATION_TRANSPORT_PRECLINICAL_BODY = "notification.transport.preclinical.body";
    private static final String NOTIFICATION_TRANSPORT_PRESCRIPTION_TITLE = "notification.transport.prescription.title";
    private static final String NOTIFICATION_TRANSPORT_PRESCRIPTION_BODY = "notification.transport.prescription.body";


    private final FirebaseNotification firebaseNotification;

    private final Message message;

    /**
     * Push notification treatment service for doctor
     *
     * @param medicalRecord               medical record
     * @param treatmentServiceOrderDetail treatment service
     * @param doctor                      doctor
     */
    public void notificationTreatment(MedicalRecord medicalRecord,
                                      MedicalServiceOrderDetail treatmentServiceOrderDetail,
                                      Account doctor) {
        // Data
        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.TREATMENT.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
        notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
        // Notification
        String title = message.getMessage(NOTIFICATION_TREATMENT_TITLE, doctor.getLanguage());
        String body = message.getMessage(NOTIFICATION_TREATMENT_BODY, new Object[]{
                medicalRecord.getPatient().getName(),
                Helper.formatDate(treatmentServiceOrderDetail.getAppointmentDate())
        }, doctor.getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(doctor.getNotificationToken(), notification, notificationData);
    }

    /**
     * Push notification to preclinical.
     *
     * @param medicalRecord  medical record
     * @param serviceDetails service order detail
     */
    public void notificationPreclinical(MedicalRecord medicalRecord, List<MedicalServiceOrderDetail> serviceDetails) {
        Map<String, List<Establishment>> mapAppointmentDateAndEstablishment = new HashMap<>();
        serviceDetails.forEach(medicalServiceOrderDetail -> {
            String date = Helper.formatDate(medicalServiceOrderDetail.getAppointmentDate());
            List<Establishment> establishments = mapAppointmentDateAndEstablishment.get(date);
            if (CollectionUtils.isEmpty(establishments)) {
                establishments = new ArrayList<>();
            }
            if (!establishments.contains(medicalServiceOrderDetail.getEstablishment())) {
                establishments.add(medicalServiceOrderDetail.getEstablishment());
            }
            mapAppointmentDateAndEstablishment.put(date, establishments);
        });

        if (!CollectionUtils.isEmpty(mapAppointmentDateAndEstablishment)) {
            for (String date : mapAppointmentDateAndEstablishment.keySet()) {
                List<Establishment> establishments = mapAppointmentDateAndEstablishment.get(date);
                establishments.forEach(establishment -> {
                    List<Account> accounts = establishment.getAccounts();
                    if (!CollectionUtils.isEmpty(accounts)) {
                        List<com.google.firebase.messaging.Message> messages = new ArrayList<>();
                        accounts.forEach(account -> {
                            Map<String, String> notificationData = new HashMap<>();
                            notificationData.put("notificationType", NotificationType.PRECLINICAL.getCode());
                            notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
                            // Notification
                            String title = message.getMessage(NOTIFICATION_PRECLINICAL_TITLE, account.getLanguage());
                            String body = message.getMessage(NOTIFICATION_PRECLINICAL_BODY, new Object[]{
                                    medicalRecord.getPatient().getName(),
                                    date
                            }, account.getLanguage());

                            log.info(title);
                            log.info(body);
                            Notification notification = firebaseNotification.generateNotification(title, body);

                            List<String> tokens = firebaseNotification.getTokens(account.getNotificationToken());
                            if (!CollectionUtils.isEmpty(tokens)) {
                                tokens.forEach(token -> {
                                    messages.add(
                                            com.google.firebase.messaging.Message.builder()
                                                    .setNotification(notification)
                                                    .putAllData(notificationData)
                                                    .setToken(token)
                                                    .build()
                                    );
                                });
                            }
                        });

                        firebaseNotification.sendBatchMessage(messages);
//                        Map<String, String> notificationData = new HashMap<>();
//                        notificationData.put("notificationType", NotificationType.PRECLINICAL.getCode());
//                        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
//                        // Notification
//                        String title = message.getMessage(NOTIFICATION_PRECLINICAL_TITLE);
//                        String body = message.getMessage(NOTIFICATION_PRECLINICAL_BODY, new Object[]{
//                                medicalRecord.getPatient().getName(),
//                                date
//                        });
//                        Notification notification = firebaseNotification.generateNotification(title, body);
//                        String topic = FirebaseNotification.PRECLINICAL_TOPIC + establishment.getId();
//                        firebaseNotification.sendToTopic(topic, notification, notificationData);
                    }
                });
            }
        }
    }

    /**
     * Push notification preclinical for patient: Payment notification
     *
     * @param medicalRecord medical record
     * @param doctor        doctor
     */
    public void notificationPreclinicalPayment(MedicalRecord medicalRecord, Account doctor) {
        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.PRECLINICAL_PAYMENT.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
        notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
        // Notification
        String title = message.getMessage(NOTIFICATION_PRECLINICAL_PAYMENT_TITLE, medicalRecord.getPatient().getAccount().getLanguage());
        String body = message.getMessage(NOTIFICATION_PRECLINICAL_PAYMENT_BODY, new Object[]{
                null != doctor ? doctor.getName() : Constants.EMPTY_STRING,
                medicalRecord.getPatient().getName()
        }, medicalRecord.getPatient().getAccount().getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(medicalRecord.getPatient().getAccount().getNotificationToken(),
                notification,
                notificationData);
    }

    /**
     * Push notification preclinical status.
     *
     * @param medicalServiceOrderDetail order detail
     */
    public void notificationPreclinicalStatus(MedicalServiceOrderDetail medicalServiceOrderDetail) {
        MedicalRecord medicalRecord = medicalServiceOrderDetail.getMedicalServiceOrder().getPreclinicalRecord().getMedicalRecord();

        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.PRECLINICAL_STATUS.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
        notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
        notificationData.put("serviceId", String.valueOf(medicalServiceOrderDetail.getMedicalService().getId()));
        notificationData.put("status", String.valueOf(medicalServiceOrderDetail.getStatus()));

        // Notification
        String title = message.getMessage(NOTIFICATION_PRECLINICAL_STATUS_TITLE, medicalRecord.getPatient().getAccount().getLanguage());
        String body = message.getMessage(NOTIFICATION_PRECLINICAL_STATUS_BODY, new Object[]{
                medicalServiceOrderDetail.getMedicalService().getName(),
                medicalRecord.getPatient().getName(),
                convertStatus(medicalServiceOrderDetail.getStatus(), medicalRecord.getPatient().getAccount().getLanguage())
        }, medicalRecord.getPatient().getAccount().getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(medicalRecord.getPatient().getAccount().getNotificationToken(),
                notification,
                notificationData);
    }

    /**
     * Push notification of preclinical service to doctor after it completed.
     *
     * @param medicalServiceOrderDetail service order detail
     */
    public void notificationPreclinicalResult(MedicalServiceOrderDetail medicalServiceOrderDetail) {
        MedicalRecord medicalRecord = medicalServiceOrderDetail.getMedicalServiceOrder().getPreclinicalRecord().getMedicalRecord();
        Account doctor = medicalRecord.getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails().
                stream().
                filter(treatmentOrderDetail -> treatmentOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_MEDICAL).
                findFirst().
                map(MedicalServiceOrderDetail::getHandleBy).orElse(null);

        if (null != doctor) {
            Map<String, String> notificationData = new HashMap<>();
            notificationData.put("notificationType", NotificationType.PRECLINICAL_DONE.getCode());
            notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
            notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
            notificationData.put("serviceId", String.valueOf(medicalServiceOrderDetail.getMedicalService().getId()));
            notificationData.put("status", String.valueOf(medicalServiceOrderDetail.getStatus()));
            // Notification
            String title = message.getMessage(NOTIFICATION_PRECLINICAL_RESULT_TITLE, new Object[]{
                    medicalServiceOrderDetail.getMedicalService().getName(),
                    medicalRecord.getPatient().getName()
            }, doctor.getLanguage());
            String body = message.getMessage(NOTIFICATION_PRECLINICAL_RESULT_BODY, new Object[]{
                    medicalServiceOrderDetail.getMedicalService().getName()
            }, doctor.getLanguage());
            Notification notification = firebaseNotification.generateNotification(title, body);
            firebaseNotification.sendToRegistrationToken(doctor.getNotificationToken(), notification, notificationData);
        }
    }

    /**
     * Push notification prescription for patient: Payment notification
     *
     * @param medicalRecord medical record
     * @param doctor        doctor
     */
    public void notificationPrescriptionPayment(MedicalRecord medicalRecord, Account doctor) {
        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.PRESCRIPTION_PAYMENT.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
        notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
        // Notification
        String title = message.getMessage(NOTIFICATION_PRESCRIPTION_PAYMENT_TITLE, medicalRecord.getPatient().getAccount().getLanguage());
        String body = message.getMessage(NOTIFICATION_PRESCRIPTION_PAYMENT_BODY, new Object[]{
                null != doctor ? doctor.getName() : Constants.EMPTY_STRING,
                medicalRecord.getPatient().getName()
        }, medicalRecord.getPatient().getAccount().getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(medicalRecord.getPatient().getAccount().getNotificationToken(),
                notification,
                notificationData);
    }

    /**
     * Push notification to prescription.
     *
     * @param medicalRecord  medical record
     * @param serviceDetails service order detail
     */
    public void notificationPrescription(MedicalRecord medicalRecord, List<MedicalServiceOrderDetail> serviceDetails) {
        List<Establishment> establishments = serviceDetails.stream().map(MedicalServiceOrderDetail::getEstablishment).
                distinct().
                collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(establishments)) {
            establishments.forEach(establishment -> {
                List<Account> accounts = establishment.getAccounts();
                if (!CollectionUtils.isEmpty(accounts)) {
                    List<com.google.firebase.messaging.Message> messages = new ArrayList<>();
                    accounts.forEach(account -> {
                        Map<String, String> notificationData = new HashMap<>();
                        notificationData.put("notificationType", NotificationType.PRESCRIPTION.getCode());
                        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
                        // Notification
                        String title = message.getMessage(NOTIFICATION_PRESCRIPTION_TITLE, account.getLanguage());
                        String body = message.getMessage(NOTIFICATION_PRESCRIPTION_BODY, new Object[]{
                                medicalRecord.getPatient().getName()
                        }, account.getLanguage());

                        Notification notification = firebaseNotification.generateNotification(title, body);

                        List<String> tokens = firebaseNotification.getTokens(account.getNotificationToken());
                        if (!CollectionUtils.isEmpty(tokens)) {
                            tokens.forEach(token -> {
                                messages.add(
                                        com.google.firebase.messaging.Message.builder()
                                                .setNotification(notification)
                                                .putAllData(notificationData)
                                                .setToken(token)
                                                .build()
                                );
                            });
                        }
                    });

                    firebaseNotification.sendBatchMessage(messages);
                }
//                Map<String, String> notificationData = new HashMap<>();
//                notificationData.put("notificationType", NotificationType.PRESCRIPTION.getCode());
//                notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
//                // Notification
//                String title = message.getMessage(NOTIFICATION_PRESCRIPTION_TITLE);
//                String body = message.getMessage(NOTIFICATION_PRESCRIPTION_BODY, new Object[]{
//                        medicalRecord.getPatient().getName()
//                });
//                Notification notification = firebaseNotification.generateNotification(title, body);
//                String topic = FirebaseNotification.DRUG_STORE_TOPIC + establishment.getId();
//                firebaseNotification.sendToTopic(topic, notification, notificationData);
            });
        }
    }

    /**
     * Push notification prescription status.
     *
     * @param medicalServiceOrderDetail order detail
     */
    public void notificationPrescriptionStatus(MedicalServiceOrderDetail medicalServiceOrderDetail) {
        MedicalRecord medicalRecord = medicalServiceOrderDetail.getMedicalServiceOrder().getPrescriptionRecord().getMedicalRecord();

        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.PRESCRIPTION_STATUS.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
        notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
        notificationData.put("status", String.valueOf(medicalServiceOrderDetail.getStatus()));

        // Notification
        String title = message.getMessage(NOTIFICATION_PRESCRIPTION_STATUS_TITLE, medicalRecord.getPatient().getAccount().getLanguage());
        String body = message.getMessage(NOTIFICATION_PRESCRIPTION_STATUS_BODY, new Object[]{
                medicalRecord.getPatient().getName(),
                convertStatus(medicalServiceOrderDetail.getStatus(), medicalRecord.getPatient().getAccount().getLanguage())
        }, medicalRecord.getPatient().getAccount().getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(medicalRecord.getPatient().getAccount().getNotificationToken(),
                notification,
                notificationData);
    }

    /**
     * Push notification treatment service for doctor
     *
     * @param medicalRecord                medical record
     * @param healthCareServiceOrderDetail treatment service
     * @param medicalStaff                 medical staff
     */
    public void notificationHealthCare(MedicalRecord medicalRecord,
                                       MedicalServiceOrderDetail healthCareServiceOrderDetail,
                                       Account medicalStaff) {
        // Data
        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.HEALTH_CARE.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
        notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
        // Notification
        String title = message.getMessage(NOTIFICATION_HEALTHCARE_TITLE, medicalStaff.getLanguage());
        String body = message.getMessage(NOTIFICATION_HEALTHCARE_BODY, new Object[]{
                medicalRecord.getPatient().getName(),
                healthCareServiceOrderDetail.getMedicalService().getName(),
                Helper.formatDate(healthCareServiceOrderDetail.getAppointmentDate())
        }, medicalStaff.getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(medicalStaff.getNotificationToken(), notification, notificationData);
    }

    /**
     * Push notification health care status.
     *
     * @param medicalServiceOrderDetail order detail
     */
    public void notificationHealthCareStatus(MedicalServiceOrderDetail medicalServiceOrderDetail, Account receiver) {
        MedicalRecord medicalRecord = medicalServiceOrderDetail.getMedicalServiceOrder().getServiceRecord().getMedicalRecord();

        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.HEALTH_CARE_STATUS.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
        notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
        notificationData.put("status", String.valueOf(medicalServiceOrderDetail.getStatus()));

        // Notification
        String title = message.getMessage(NOTIFICATION_HEALTHCARE_STATUS_TITLE, receiver.getLanguage());
        String body = message.getMessage(NOTIFICATION_HEALTHCARE_STATUS_BODY, new Object[]{
                medicalServiceOrderDetail.getMedicalService().getName(),
                medicalRecord.getPatient().getName(),
                convertStatus(medicalServiceOrderDetail.getStatus(), receiver.getLanguage())
        }, receiver.getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(receiver.getNotificationToken(),
                notification,
                notificationData);
    }

    /**
     * Push notification health care status.
     *
     * @param medicalServiceOrderDetail order detail
     */
    public void notificationHealthCareHadPayment(MedicalServiceOrderDetail medicalServiceOrderDetail) {
        MedicalRecord medicalRecord = medicalServiceOrderDetail.getMedicalServiceOrder().getServiceRecord().getMedicalRecord();

        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.HEALTH_CARE_HAD_PAYMENT.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecord.getId()));
        notificationData.put("patientId", String.valueOf(medicalRecord.getPatient().getId()));
        notificationData.put("status", String.valueOf(medicalServiceOrderDetail.getStatus()));

        // Notification
        String title = message.getMessage(NOTIFICATION_HEALTHCARE_PAYMENT_TITLE, new Object[]{
                medicalRecord.getPatient().getName()
        }, medicalServiceOrderDetail.getHandleBy().getLanguage());
        String body = message.getMessage(NOTIFICATION_HEALTHCARE_PAYMENT_BODY, new Object[]{
                medicalServiceOrderDetail.getMedicalService().getName(),
                medicalRecord.getPatient().getName()
        }, medicalServiceOrderDetail.getHandleBy().getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(medicalServiceOrderDetail.getHandleBy().getNotificationToken(),
                notification,
                notificationData);
    }

    /**
     * Push notification prescription have drug is change and not payment.
     *
     * @param prescriptionNotifications data
     */
    public void notificationDrugUnitPriceChange(List<PrescriptionNotificationDTO> prescriptionNotifications) {
        if (!CollectionUtils.isEmpty(prescriptionNotifications)) {
            List<String> tokenSent = new ArrayList<>();
            prescriptionNotifications.forEach(prescriptionNotification -> {
                String key = prescriptionNotification.getPatientId() + Constants.UNDERSCORE +
                        prescriptionNotification.getDrugId() + Constants.UNDERSCORE +
                        prescriptionNotification.getNotificationToken();
                if (!tokenSent.contains(key)) {
                    Map<String, String> notificationData = new HashMap<>();
                    notificationData.put("notificationType", NotificationType.DRUG_PRICE_CHANGE.getCode());
                    notificationData.put("medicalRecordId", String.valueOf(prescriptionNotification.getMedicalRecordId()));
                    notificationData.put("patientId", String.valueOf(prescriptionNotification.getPatientId()));
                    notificationData.put("drugId", String.valueOf(prescriptionNotification.getDrugId()));

                    // Notification
                    String title = message.getMessage(NOTIFICATION_PRESCRIPTION_PRICE_TITLE, new Object[]{
                            prescriptionNotification.getPatientName()
                    }, prescriptionNotification.getLanguage());
                    String body = message.getMessage(NOTIFICATION_PRESCRIPTION_PRICE_BODY, new Object[]{
                            prescriptionNotification.getDrugName()
                    }, prescriptionNotification.getLanguage());

                    Notification notification = firebaseNotification.generateNotification(title, body);
                    firebaseNotification.sendToRegistrationToken(prescriptionNotification.getNotificationToken(),
                            notification,
                            notificationData);
                    tokenSent.add(key);
                }
            });
        }
    }

    /**
     * Push notification preclinical have service is change and not payment.
     *
     * @param preclinicalNotifications data
     */
    public void notificationPreclinicalServicePriceChange(List<PreclinicalNotificationDTO> preclinicalNotifications) {
        if (!CollectionUtils.isEmpty(preclinicalNotifications)) {
            List<String> tokenSent = new ArrayList<>();
            preclinicalNotifications.forEach(preclinicalNotification -> {
                String key = preclinicalNotification.getPatientId() + Constants.UNDERSCORE +
                        preclinicalNotification.getServiceId() + Constants.UNDERSCORE +
                        preclinicalNotification.getNotificationToken();
                if (!tokenSent.contains(key)) {
                    Map<String, String> notificationData = new HashMap<>();
                    notificationData.put("notificationType", NotificationType.PRECLINICAL_SERVICE_PRICE_CHANGE.getCode());
                    notificationData.put("medicalRecordId", String.valueOf(preclinicalNotification.getMedicalRecordId()));
                    notificationData.put("patientId", String.valueOf(preclinicalNotification.getPatientId()));
                    notificationData.put("serviceId", String.valueOf(preclinicalNotification.getServiceId()));

                    // Notification
                    String title = message.getMessage(NOTIFICATION_PRECLINICAL_PRICE_TITLE, new Object[]{
                            preclinicalNotification.getServiceName(),
                            preclinicalNotification.getPatientName()
                    }, preclinicalNotification.getLanguage());
                    String body = message.getMessage(NOTIFICATION_PRECLINICAL_PRICE_BODY, new Object[]{
                            preclinicalNotification.getServiceName()
                    }, preclinicalNotification.getLanguage());

                    Notification notification = firebaseNotification.generateNotification(title, body);
                    firebaseNotification.sendToRegistrationToken(preclinicalNotification.getNotificationToken(),
                            notification,
                            notificationData);
                    tokenSent.add(key);
                }
            });
        }
    }

    /**
     * Push notification examine have price is change and not payment.
     *
     * @param examineNotifications data
     */
    public void notificationExamineServicePriceChange(List<ExamineNotificationDTO> examineNotifications) {
        if (!CollectionUtils.isEmpty(examineNotifications)) {
            List<String> tokenSent = new ArrayList<>();
            examineNotifications.forEach(examineNotification -> {
                String key = examineNotification.getPatientId() + Constants.UNDERSCORE +
                        examineNotification.getDoctorId() + Constants.UNDERSCORE +
                        examineNotification.getNotificationToken();
                if (!tokenSent.contains(key)) {
                    Map<String, String> notificationData = new HashMap<>();
                    notificationData.put("notificationType", NotificationType.EXAMINE_SERVICE_PRICE_CHANGE.getCode());
                    notificationData.put("medicalRecordId", String.valueOf(examineNotification.getMedicalRecordId()));
                    notificationData.put("patientId", String.valueOf(examineNotification.getPatientId()));

                    // Notification
                    String title = message.getMessage(NOTIFICATION_EXAMINE_PRICE_TITLE, new Object[]{
                            examineNotification.getPatientName()
                    }, examineNotification.getLanguage());
                    String body = message.getMessage(NOTIFICATION_EXAMINE_PRICE_BODY, new Object[]{
                            examineNotification.getDoctorName()
                    }, examineNotification.getLanguage());

                    Notification notification = firebaseNotification.generateNotification(title, body);
                    firebaseNotification.sendToRegistrationToken(examineNotification.getNotificationToken(),
                            notification,
                            notificationData);
                    tokenSent.add(key);
                }
            });
        }
    }

    /**
     * Push notification health care have price is change and not payment.
     *
     * @param healthCareNotifications data
     */
    public void notificationHealthCareServicePriceChange(List<HealthCareNotificationDTO> healthCareNotifications) {
        if (!CollectionUtils.isEmpty(healthCareNotifications)) {
            List<String> tokenSent = new ArrayList<>();
            healthCareNotifications.forEach(healthCareNotification -> {
                String key = healthCareNotification.getPatientId() + Constants.UNDERSCORE +
                        healthCareNotification.getStaffId() + Constants.UNDERSCORE +
                        healthCareNotification.getPackageId() + Constants.UNDERSCORE +
                        healthCareNotification.getNotificationToken();
                if (!tokenSent.contains(key)) {
                    Map<String, String> notificationData = new HashMap<>();
                    notificationData.put("notificationType", NotificationType.HEALTH_CARE_SERVICE_PRICE_CHANGE.getCode());
                    notificationData.put("medicalRecordId", String.valueOf(healthCareNotification.getMedicalRecordId()));
                    notificationData.put("patientId", String.valueOf(healthCareNotification.getPatientId()));
                    notificationData.put("packageId", String.valueOf(healthCareNotification.getPackageId()));

                    // Notification
                    String title = message.getMessage(NOTIFICATION_HEALTHCARE_PRICE_TITLE, new Object[]{
                            healthCareNotification.getPatientName()
                    }, healthCareNotification.getLanguage());
                    String body = message.getMessage(NOTIFICATION_HEALTHCARE_PRICE_BODY, new Object[]{
                            healthCareNotification.getPackageName()
                    }, healthCareNotification.getLanguage());

                    Notification notification = firebaseNotification.generateNotification(title, body);
                    firebaseNotification.sendToRegistrationToken(healthCareNotification.getNotificationToken(),
                            notification,
                            notificationData);
                    tokenSent.add(key);
                }
            });
        }
    }

    /**
     * Notification treatment transport.
     *
     * @param doctor        doctor account
     * @param patient       patient account
     */
    public void notificationTreatmentTransport(Account doctor, Account patient, int medicalRecordId) {
        // Data
        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.TRANSPORT_TREATMENT.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecordId));
        notificationData.put("patientId", String.valueOf(patient.getPatient().getId()));

        // Notification
        String title = message.getMessage(NOTIFICATION_TRANSPORT_TREATMENT_TITLE, patient.getLanguage());
        String body = message.getMessage(NOTIFICATION_TRANSPORT_TREATMENT_BODY, new Object[]{
                doctor.getName(),
                patient.getName()
        }, patient.getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(patient.getNotificationToken(), notification, notificationData);
    }

    /**
     * Notification Preclinical Transport.
     *
     * @param serviceName   service name
     * @param patient       patient account
     */
    public void notificationPreclinicalTransport(String serviceName, Account patient, int medicalRecordId) {
        // Data
        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.TRANSPORT_PRECLINICAL.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecordId));
        notificationData.put("patientId", String.valueOf(patient.getPatient().getId()));

        // Notification
        String title = message.getMessage(NOTIFICATION_TRANSPORT_PRECLINICAL_TITLE, new Object[]{
                serviceName
        }, patient.getLanguage());
        String body = message.getMessage(NOTIFICATION_TRANSPORT_PRECLINICAL_BODY, new Object[]{
                patient.getName()
        }, patient.getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(patient.getNotificationToken(), notification, notificationData);
    }

    /**
     * Notification Prescription Transport.
     *
     * @param patient       patient account
     */
    public void notificationPrescriptionTransport(String drugStoreName, Account patient, int medicalRecordId) {
        // Data
        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("notificationType", NotificationType.TRANSPORT_PRESCRIPTION.getCode());
        notificationData.put("medicalRecordId", String.valueOf(medicalRecordId));
        notificationData.put("patientId", String.valueOf(patient.getPatient().getId()));

        // Notification
        String title = message.getMessage(NOTIFICATION_TRANSPORT_PRESCRIPTION_TITLE, patient.getLanguage());
        String body = message.getMessage(NOTIFICATION_TRANSPORT_PRESCRIPTION_BODY, new Object[]{
                patient.getName(),
                drugStoreName
        }, patient.getLanguage());
        Notification notification = firebaseNotification.generateNotification(title, body);
        firebaseNotification.sendToRegistrationToken(patient.getNotificationToken(), notification, notificationData);
    }

    /**
     * Convert status
     *
     * @param status order status
     * @return status
     */
    private String convertStatus(MedicalServiceOrderStatus status, String language) {
        switch (status) {
            case REQUEST_PROCESSING:
                return message.getMessage(NOTIFICATION_STATUS_PROCESSING, language);
            case REQUEST_FULFILLED:
                return message.getMessage(NOTIFICATION_STATUS_FULFILLED, language);
            case REQUEST_DENIED:
                return message.getMessage(NOTIFICATION_STATUS_DENIED, language);
            case REQUEST_CANCELLED:
                return message.getMessage(NOTIFICATION_STATUS_CANCELED, language);
            case REQUEST_ACCEPTED:
                return message.getMessage(NOTIFICATION_STATUS_ACCEPTED, language);
            default:
                return Constants.EMPTY_STRING;
        }
    }
}
