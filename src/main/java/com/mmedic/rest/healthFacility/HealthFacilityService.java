package com.mmedic.rest.healthFacility;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.rest.establishment.dto.WorkTimeDTO;
import com.mmedic.rest.healthFacility.dto.*;
import com.mmedic.rest.patient.dto.PatientDTO;
import com.mmedic.spring.errors.MedicException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

public interface HealthFacilityService {

    /**
     * Find all preclinical by condition.
     *
     * @param condition            condition value to search (name/address)
     * @param preclinicalServiceId service id
     * @param latitude             latitude
     * @param longitude            longtitude
     * @param pageable             pagination
     * @return page data
     */
    Page<PreclinicalQueryDTO> findPreclinicals(String condition, int preclinicalServiceId, double latitude, double longitude, Pageable pageable);

    /**
     * Find basic info of preclinical.
     *
     * @param condition condition value to search (name/address)
     * @param pageable  pagination
     * @return list basic info of preclinical
     */
    Page<PreclinicalQueryDTO> findBasicInfoPreclinicals(String condition, Pageable pageable);

    /**
     * Find preclinical work time.
     *
     * @param startDate     start date
     * @param endDate       end date
     * @param preclinicalId preclinical id
     * @return list preclinical work time
     * @throws MedicException
     */
    List<WorkTimeDTO> findPreclinicalWorkTime(LocalDate startDate, LocalDate endDate, int preclinicalId) throws MedicException;

    /**
     * Find all drug store.
     *
     * @param condition condition to search (name/address)
     * @param latitude  latitude
     * @param longitude longitude
     * @param pageable  pagination
     * @return page data
     */
    Page<DrugStoreQueryDTO> findDrugStore(String condition, double latitude, double longitude, Pageable pageable);

    /**
     * Find drug store by name.
     *
     * @param condition the condition value
     * @return list drug store
     */
    List<DrugStoreQueryDTO> findDrugStore(String condition);

    /**
     * Get Prescription is ordered in today.
     *
     * @param establishmentId the establishment id
     * @return List Prescription Order
     */
    List<PrescriptionOrderTodayDTO> findPrescriptionOrderToday(int establishmentId);

    /**
     * Get all Prescription is ordered.
     *
     * @param condition       condition value
     * @param establishmentId the establishment id
     * @param pageable        pagination
     * @return List Prescription Order
     */
    Page<PrescriptionOrderDTO> findPrescriptionOrderAll(String condition, int establishmentId, Pageable pageable);

    /**
     * Find Prescription Detail.
     *
     * @param prescriptionOrderId  the prescription order id.
     * @param currentEstablishment current establishment
     * @return Prescription detail
     */
    PrescriptionOrderDetailDTO findPrescriptionDetail(int prescriptionOrderId, Establishment currentEstablishment);

    /**
     * Process prescription.
     *
     * @param prescriptionRecordId the prescription id
     * @param currentEstablishment current establishment
     * @return true if update successfully
     */
    boolean processPrescription(int prescriptionRecordId, Establishment currentEstablishment);

    /**
     * Supply medicine.
     *
     * @param prescriptionOrderId Prescription Record Id
     * @param drugDetails         drug details
     * @param exportFrom          Drug store process this prescription
     * @return return true if process success
     */
    PrescriptionOrderDetailDTO supplyMedicine(int prescriptionOrderId, List<DrugDetailDTO> drugDetails, Establishment exportFrom, Account currentAccount);

    /**
     * Create new retail prescription.
     *
     * @param drugDetails   the prescription
     * @param establishment the drug store supply drug
     * @return returns result if it creates successfully
     */
    PrescriptionOrderDetailDTO createRetailPrescription(List<DrugDetailDTO> drugDetails, Establishment establishment);

    /**
     * Denied prescription
     *
     * @param prescriptionRecordId prescription id
     * @param currentEstablishment current establishment
     * @return true if update successfully
     */
    boolean deniedPrescription(int prescriptionRecordId, Establishment currentEstablishment);

    /**
     * Get prescription img dto.
     *
     * @param prescriptionId
     * @param patientDTO
     * @return dto
     */
    PrescriptionImageDto getPrescriptionImgDto(Integer prescriptionId, PatientDTO patientDTO);

    /**
     * Push notification to notify transport time.
     *
     * @param prescriptionId       prescription id
     * @param currentEstablishment current establishment
     * @return true if push successfullt
     */
    boolean notificationPrescriptionTransportTime(int prescriptionId, Establishment currentEstablishment);
}
