package com.mmedic.rest.healthFacility;

import com.mmedic.entity.Establishment;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.certification.CertificationService;
import com.mmedic.rest.certification.dto.CertificationDTO;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.establishment.dto.WorkTimeDTO;
import com.mmedic.rest.healthFacility.dto.DrugDetailDTO;
import com.mmedic.rest.healthFacility.dto.DrugStoreQueryDTO;
import com.mmedic.rest.healthFacility.dto.PreclinicalQueryDTO;
import com.mmedic.rest.healthFacility.dto.PrescriptionOrderDTO;
import com.mmedic.rest.healthFacility.dto.PrescriptionOrderDetailDTO;
import com.mmedic.rest.healthFacility.dto.PrescriptionOrderTodayDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

/**
 * Health Facility Controller.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/health-facilities")
@Api(value = "Health Facility Controller", description = "Health Facility Management")
@RequiredArgsConstructor
public class HealthFacilityController {

    private final BaseService baseService;

    private final HealthFacilityService healthFacilityService;

    private final CertificationService certificationService;

    @ApiOperation("Search Preclinical (DONE)")
    @GetMapping("/preclinicals")
    public MedicResponse<Page<PreclinicalQueryDTO>> searchPreclinicals(@RequestParam(required = false) @ApiParam("Search condition") String query,
                                                                       @ApiParam("Preclinical Service Id") int preclinicalServiceId,
                                                                       @RequestParam(required = false) @ApiParam("Latitude") Double latitude,
                                                                       @RequestParam(required = false) @ApiParam("Longitude") Double longitude,
                                                                       @PageableDefault(sort = "price") @ApiParam("Pagination") Pageable pageable) {
        // Set value for location
        double locationLatitude = null != latitude ? latitude : 0;
        double locationLongitude = null != longitude ? longitude : 0;
        // Prepare sort column
        Sort.Direction direction = Sort.Direction.ASC;
        String property = "data.price";
        Sort sort = pageable.getSort();
        if (sort.isSorted()) {
            if (null != sort.getOrderFor("price")) {
                property = "data.price";
                direction = sort.getOrderFor("price").getDirection();
            } else if (null != sort.getOrderFor("distance")) {
                property = "data.distance";
                direction = sort.getOrderFor("distance").getDirection();
            }
        }
        return MedicResponse.okStatus(healthFacilityService.findPreclinicals(query, preclinicalServiceId, locationLatitude, locationLongitude,
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), direction, property)));
    }

    @ApiOperation("Search basic info of preclinical (DONE)")
    @GetMapping("/preclinicals/basic-info")
    public MedicResponse<Page<PreclinicalQueryDTO>> searchBasicInfoOfPreclinicals(@RequestParam(required = false) @ApiParam("Search condition") String query,
                                                                                  @ApiParam("Pagination") Pageable pageable) {
        return MedicResponse.okStatus(healthFacilityService.findBasicInfoPreclinicals(query,
                PageRequest.of(pageable.getPageNumber(), pageable.getPageSize())));
    }

    @ApiOperation("Get preclinical work time (DONE)")
    @GetMapping("/preclinicals/{id}/work-time")
    public MedicResponse<List<WorkTimeDTO>> getPreclinicalWorkTime(@PathVariable("id") @ApiParam("Preclinical Id") int preclinicalId,
                                                                   @RequestParam("startDate") @ApiParam("Start Date") LocalDate startDate,
                                                                   @RequestParam("endDate") @ApiParam("End Date") LocalDate endDate) {
        return MedicResponse.okStatus(healthFacilityService.findPreclinicalWorkTime(startDate, endDate, preclinicalId));
    }

    @ApiOperation("Get certification by establishment id (DONE)")
    @GetMapping("/preclinicals/{id}/certifications")
    public MedicResponse<List<CertificationDTO>> getCertificationOfPreclinical(@PathVariable("id") @ApiParam("Perclinical Id") int preclinicalId) {
        return MedicResponse.okStatus(certificationService.getCertificationByEstablishmentId(preclinicalId));
    }

    @ApiOperation("Find drug stores (DONE)")
    @GetMapping("/drug-stores")
    public MedicResponse<Page<DrugStoreQueryDTO>> findDrugStores(@RequestParam(required = false) @ApiParam("Search Condition") String query,
                                                                 @RequestParam(required = false) @ApiParam("Latitude") Double latitude,
                                                                 @RequestParam(required = false) @ApiParam("Longitude") Double longitude,
                                                                 @PageableDefault(sort = "distance") @ApiParam("Pagination") Pageable pageable) {
        // Set value for location
        double locationLatitude = null != latitude ? latitude : 0;
        double locationLongitude = null != longitude ? longitude : 0;
        Sort.Direction direction = Sort.Direction.ASC;
        String property = "data.distance";
        Sort sort = pageable.getSort();
        if (sort.isSorted()) {
            if (null != sort.getOrderFor("distance")) {
                direction = sort.getOrderFor("distance").getDirection();
            }
        }
        return MedicResponse.okStatus(healthFacilityService.findDrugStore(query, locationLatitude, locationLongitude, PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), direction, property)));
    }

    /**
     * Get list drug store
     *
     * @param query the condition
     * @return list drug store
     */
    @ApiOperation("Find drug stores (DONE)")
    @GetMapping("/drug-stores/all")
    public MedicResponse<List<DrugStoreQueryDTO>> findAllDrugStore(@RequestParam(required = false) @ApiParam("Search condition") String query) {
        return MedicResponse.okStatus(healthFacilityService.findDrugStore(query));
    }

    /**
     * Get Prescription is ordered in today.
     *
     * @return List Prescription Order
     */
    @ApiOperation("Find all prescription of drug store in day (DONE)")
    @GetMapping("/drug-stores/prescriptions/today")
    public MedicResponse<List<PrescriptionOrderTodayDTO>> getPrescriptionOrderToday() {
        List<PrescriptionOrderTodayDTO> prescriptionOrderDetails = null;

        Establishment establishment = baseService.getCurrentEstablishment();
        if (null != establishment) {
            prescriptionOrderDetails = healthFacilityService.findPrescriptionOrderToday(establishment.getId());
        }

        return MedicResponse.okStatus(prescriptionOrderDetails);
    }

    /**
     * Get Prescription is ordered in today.
     *
     * @return List Prescription Order
     */
    @ApiOperation("Find all prescription of drug store (DONE)")
    @GetMapping("/drug-stores/prescriptions/all")
    public MedicResponse<Page<PrescriptionOrderDTO>> getPrescriptionOrderAll(@RequestParam(required = false) @ApiParam("Condition Value") String query,
                                                                             @ApiParam("Pagination") Pageable pageable) {
        Page<PrescriptionOrderDTO> prescriptionOrderDetails = null;

        Establishment establishment = baseService.getCurrentEstablishment();
        if (null != establishment) {
            prescriptionOrderDetails = healthFacilityService.findPrescriptionOrderAll(query, establishment.getId(), PageRequest.of(pageable.getPageNumber(), pageable.getPageSize()));
        }

        return MedicResponse.okStatus(prescriptionOrderDetails);
    }

    /**
     * Get Prescription Detail.
     *
     * @param id the prescription record id
     * @return Prescription Detail
     */
    @ApiOperation("Get prescription detail (DONE)")
    @GetMapping("/drug-stores/prescriptions/{id}")
    public MedicResponse<PrescriptionOrderDetailDTO> getPrescriptionOrderDetail(@PathVariable("id") @ApiParam("Prescription Id") int id) {
        return MedicResponse.okStatus(healthFacilityService.findPrescriptionDetail(id, baseService.getCurrentEstablishment()));
    }

    /**
     * Push notification to notify time transport.
     *
     * @param id id of prescription
     * @return result
     */
    @ApiOperation("Push notification notify time transport (DONE)")
    @PostMapping("/drug-stores/prescriptions/{id}/transport-notification")
    public MedicResponse<Boolean> notificationPrescriptionTransport(@PathVariable("id") @ApiParam("Prescription Id") int id) {
        return MedicResponse.okStatus(healthFacilityService.notificationPrescriptionTransportTime(id, baseService.getCurrentEstablishment()));
    }

    /**
     * Deny prescription order.
     *
     * @param id prescription id.
     * @return true if deny successfully
     */
    @ApiOperation("Denied prescription detail (DONE)")
    @DeleteMapping("/drug-stores/prescriptions/{id}")
    public MedicResponse<Boolean> deniedPrescriptionOrder(@PathVariable("id") @ApiParam("Prescription Id") int id) {
        return MedicResponse.okStatus(healthFacilityService.deniedPrescription(id, baseService.getCurrentEstablishment()));
    }

    /**
     * Update status of prescription to processing.
     *
     * @param id the id prescription
     * @return status of prescription
     */
    @ApiOperation("Update status of prescription to processing (DONE)")
    @PutMapping("/drug-stores/prescriptions/{id}/process")
    public MedicResponse<Boolean> processPrescription(@PathVariable("id") @ApiParam("Prescription Id") int id) {
        return MedicResponse.okStatus(healthFacilityService.processPrescription(id, baseService.getCurrentEstablishment()));
    }

    /**
     * Supply medicine for prescription.
     *
     * @param id          the id of prescription
     * @param drugDetails the drugs is supplied for prescription
     * @return returns true if supply successfully
     */
    @ApiOperation("Supply Medicine (DONE)")
    @PostMapping("/drug-stores/prescriptions/{id}")
    public MedicResponse<PrescriptionOrderDetailDTO> supplyMedicine(@PathVariable("id") @ApiParam("Prescription Id") int id,
                                                                    @RequestBody @ApiParam("Drug Details") List<DrugDetailDTO> drugDetails) {
        return MedicResponse.okStatus(healthFacilityService.supplyMedicine(id, drugDetails,
                baseService.getCurrentEstablishment(), baseService.getCurrentAccountLogin()));
    }

    /**
     * Create new retail prescription record.
     *
     * @return returns result if prescription creates successfully
     */
    @ApiOperation("Create new retail prescription record (DONE)")
    @PostMapping("/drug-stores/retails")
    public MedicResponse<PrescriptionOrderDetailDTO> createRetailPrescription(@RequestBody @ApiParam("Drug Details") List<DrugDetailDTO> drugDetails) {
        return MedicResponse.okStatus(healthFacilityService.createRetailPrescription(drugDetails, baseService.getCurrentEstablishment()));
    }
}
