package com.mmedic.rest.healthFacility.impl;

import com.mmedic.entity.*;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.enums.PrescriptionSupplyType;
import com.mmedic.rest.drug.DrugService;
import com.mmedic.rest.drug.repo.DrugUnitRepository;
import com.mmedic.rest.drugstorage.repo.DrugExportDetailRepository;
import com.mmedic.rest.drugstorage.repo.DrugImportDetailRepository;
import com.mmedic.rest.drugstorage.repo.DrugOrderExportRepository;
import com.mmedic.rest.drugstorage.repo.StorageExportRepository;
import com.mmedic.rest.establishment.EstablishmentService;
import com.mmedic.rest.establishment.dto.WorkTimeDTO;
import com.mmedic.rest.healthFacility.HealthFacilityService;
import com.mmedic.rest.healthFacility.dto.*;
import com.mmedic.rest.healthFacility.repo.HealthFacilityRepository;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDTO;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalServiceOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.PrescriptionDetailRepository;
import com.mmedic.rest.medicalRecord.repo.PrescriptionRecordRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.rest.patient.dto.PatientDTO;
import com.mmedic.rest.patient.dto.PatientPrescriptionDTO;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Heal Facility service implementation.
 */
@Service
@RequiredArgsConstructor
public class HealthFacilityServiceImpl implements HealthFacilityService {

    private final HealthFacilityRepository healthFacilityRepository;

    private final EstablishmentService establishmentService;

    private final PrescriptionRecordRepository prescriptionRecordRepository;

    private final MedicalServiceOrderRepository medicalServiceOrderRepository;

    private final MedicalServiceOrderDetailRepository medicalServiceOrderDetailRepository;

    private final DrugUnitRepository drugUnitRepository;

    private final StorageExportRepository storageExportRepository;

    private final DrugExportDetailRepository drugExportDetailRepository;

    private final DrugOrderExportRepository drugOrderExportRepository;

    private final MedicalServiceRepository medicalServiceRepository;

    private final PrescriptionDetailRepository prescriptionDetailRepository;

    private final DrugService drugService;

    private final DrugImportDetailRepository drugImportDetailRepository;

    private final NotificationService notificationService;

    private final MedicalRecordRepository medicalRecordRepository;

    @Override
    public Page<DrugStoreQueryDTO> findDrugStore(String condition, double latitude, double longitude, Pageable pageable) {
        return healthFacilityRepository.findDrugStore("%" + Helper.convertNullToEmpty(condition) + "%", latitude, longitude, pageable);
    }

    public List<DrugStoreQueryDTO> findDrugStore(String condition) {
        return healthFacilityRepository.findDrugStore(Helper.convertNullToEmpty(condition));
    }

    @Override
    public Page<PreclinicalQueryDTO> findPreclinicals(String condition, int preclinicalServiceId, double latitude, double longitude, Pageable pageable) {
        Page<PreclinicalQueryDTO> preclinicals = healthFacilityRepository.findPreclinicals("%" + Helper.convertNullToEmpty(condition) + "%", preclinicalServiceId, latitude, longitude, pageable);
        if (!CollectionUtils.isEmpty(preclinicals.getContent())) {
            List<PreclinicalQueryDTO> result = new ArrayList<>();
            List<Integer> establishmentIds = preclinicals.getContent().stream().map(PreclinicalQueryDTO::getId).collect(Collectors.toList());
            List<Integer> lstEstablishmentAvailable = establishmentService.calculateAvailable(establishmentIds);
            if (!CollectionUtils.isEmpty(lstEstablishmentAvailable)) {
                preclinicals.getContent().forEach(preclinical -> {
                    if (lstEstablishmentAvailable.contains(preclinical.getId())) {
                        PreclinicalQueryDTO preclinicalAvai = new PreclinicalQueryDTO() {
                            @Override
                            public int getId() {
                                return preclinical.getId();
                            }

                            @Override
                            public String getName() {
                                return preclinical.getName();
                            }

                            @Override
                            public String getAddress() {
                                return preclinical.getAddress();
                            }

                            @Override
                            public String getPhoneNumber() {
                                return preclinical.getPhoneNumber();
                            }

                            @Override
                            public String getAvatarUrl() {
                                return preclinical.getAvatarUrl();
                            }

                            @Override
                            public Integer getPrice() {
                                return preclinical.getPrice();
                            }

                            @Override
                            public float getRatingAverage() {
                                return preclinical.getRatingAverage();
                            }

                            @Override
                            public Boolean getAvailable() {
                                return true;
                            }
                        };
                        result.add(preclinicalAvai);
                    } else {
                        result.add(preclinical);
                    }
                });
                preclinicals = new PageImpl<>(result, pageable, preclinicals.getTotalElements());
            }
        }
        return preclinicals;
    }

    @Override
    public Page<PreclinicalQueryDTO> findBasicInfoPreclinicals(String condition, Pageable pageable) {
        return healthFacilityRepository.findBasicInfoOfPreclinicals("%" + Helper.convertNullToEmpty(condition) + "%", pageable);
    }

    @Override
    public List<WorkTimeDTO> findPreclinicalWorkTime(LocalDate startDate, LocalDate endDate, int preclinicalId) throws MedicException {
        List<WorkTimeDTO> result = establishmentService.findWorkTimeEstablishment(startDate, endDate, preclinicalId);
        return result.stream().
                sorted(Comparator.comparing(WorkTimeDTO::getDate).
                        thenComparing(WorkTimeDTO::getStartTime)).
                collect(Collectors.toList());
    }

    @Override
    public List<PrescriptionOrderTodayDTO> findPrescriptionOrderToday(int establishmentId) {
        List<PrescriptionOrderTodayDTO> result = new ArrayList<>();
        String condition = Constants.PERCENT + Constants.EMPTY_STRING + Constants.PERCENT;
        List<PrescriptionOrderDTO> data = prescriptionRecordRepository.findPrescriptionOrderedInDate(condition, establishmentId, Constants.SERVICE_MEDICINE_SUPPLY, null, null).getContent();
        if (!CollectionUtils.isEmpty(data)) {
            List<Integer> prescriptionIds = data.stream().map(PrescriptionOrderDTO::getId).collect(Collectors.toList());
            List<String> prescriptionIdAndEstablishmentId = prescriptionRecordRepository.
                    findEstablishmentIdOfTransportServiceByMedicalRecordId(prescriptionIds);

            data.forEach(prescriptionOrderDTO -> {
                PrescriptionOrderTodayDTO dto = new PrescriptionOrderTodayDTO();
                dto.setId(prescriptionOrderDTO.getId());
                dto.setAvatarUrl(prescriptionOrderDTO.getAvatarUrl());
                dto.setDateOfBirth(prescriptionOrderDTO.getDateOfBirth());
                dto.setName(prescriptionOrderDTO.getName());
                dto.setPatientId(prescriptionOrderDTO.getPatientId());
                dto.setPaymentStatus(prescriptionOrderDTO.getPaymentStatus());
                dto.setStatus(prescriptionOrderDTO.getStatus());

                // Set Type
                String key = prescriptionOrderDTO.getId() + Constants.UNDERSCORE + establishmentId;
                if (!CollectionUtils.isEmpty(prescriptionIdAndEstablishmentId) && prescriptionIdAndEstablishmentId.contains(key)) {
                    dto.setDeliveryType(PrescriptionSupplyType.HOME);
                } else {
                    dto.setDeliveryType(PrescriptionSupplyType.DRUG_STORE);
                }
                result.add(dto);
            });
        }
        return result;
    }

    @Override
    public Page<PrescriptionOrderDTO> findPrescriptionOrderAll(String condition, int establishmentId, Pageable pageable) {
        condition = Constants.PERCENT + Helper.convertNullToEmpty(condition) + Constants.PERCENT;
        return prescriptionRecordRepository.findPrescriptionOrderedInDate(condition, establishmentId, Constants.SERVICE_MEDICINE_SUPPLY, true, pageable);
    }

    @Override
    @Transactional
    public boolean processPrescription(int prescriptionRecordId, Establishment currentEstablishment) {
        PrescriptionRecord prescriptionRecord = prescriptionRecordRepository.findById(prescriptionRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRESCRIPTION_NOT_EXIST, "Prescription don't exist"));

        MedicalServiceOrder prescriptionServiceOrder = prescriptionRecord.getMedicalServiceOrder();
        if (CollectionUtils.isEmpty(prescriptionServiceOrder.getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "Order detail don't exist");
        }
        if (null != prescriptionRecord.getMedicalRecord() && !PaymentStatus.SUCCESS.equals(prescriptionServiceOrder.getPaymentStatus())) {
            throw new MedicException(MedicException.ERROR_SERVICE_ORDER_NOT_PAID);
        }

        List<MedicalServiceOrderDetail> serviceOrderDetails = new ArrayList<>();
        prescriptionServiceOrder.getMedicalServiceOrderDetails().forEach(medicalServiceOrderDetail -> {
            if (medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY &&
                    medicalServiceOrderDetail.getEstablishment().getId() == currentEstablishment.getId() &&
                    !MedicalServiceOrderStatus.REQUEST_FULFILLED.equals(medicalServiceOrderDetail.getStatus())) {
                medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_PROCESSING);
                medicalServiceOrderDetail.setProccessDate(LocalDateTime.now());
                serviceOrderDetails.add(medicalServiceOrderDetail);
            } else if (medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_DELIVERY &&
                    medicalServiceOrderDetail.getEstablishment().getId() == currentEstablishment.getId()) {
                medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_FULFILLED);
                serviceOrderDetails.add(medicalServiceOrderDetail);
            }
        });
        if (!CollectionUtils.isEmpty(serviceOrderDetails)) {
            medicalServiceOrderDetailRepository.saveAll(serviceOrderDetails);

            if (null != prescriptionRecord.getMedicalRecord()) {
                notificationService.notificationPrescriptionStatus(serviceOrderDetails.get(0));
            }
        }
        return true;
    }

    @Override
    public boolean deniedPrescription(int prescriptionRecordId, Establishment currentEstablishment) {
        PrescriptionRecord prescriptionRecord = prescriptionRecordRepository.findById(prescriptionRecordId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRESCRIPTION_NOT_EXIST, "Prescription don't exist"));

        MedicalServiceOrder prescriptionServiceOrder = prescriptionRecord.getMedicalServiceOrder();
        if (CollectionUtils.isEmpty(prescriptionServiceOrder.getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "Order detail don't exist");
        }
        List<MedicalServiceOrderDetail> serviceOrderDetails = new ArrayList<>();
        prescriptionServiceOrder.getMedicalServiceOrderDetails().forEach(medicalServiceOrderDetail -> {
            if (medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY &&
                    medicalServiceOrderDetail.getEstablishment().getId() == currentEstablishment.getId() &&
                    !MedicalServiceOrderStatus.REQUEST_FULFILLED.equals(medicalServiceOrderDetail.getStatus()) &&
                    !MedicalServiceOrderStatus.REQUEST_PROCESSING.equals(medicalServiceOrderDetail.getStatus())) {
                medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_DENIED);
                serviceOrderDetails.add(medicalServiceOrderDetail);
            }
        });
        if (!CollectionUtils.isEmpty(serviceOrderDetails)) {
            medicalServiceOrderDetailRepository.saveAll(serviceOrderDetails);
            return true;
        }
        return false;
    }

    @Override
    public PrescriptionOrderDetailDTO findPrescriptionDetail(int prescriptionOrderId, Establishment currentEstablishment) {
        PrescriptionRecord prescriptionRecord = prescriptionRecordRepository.findById(prescriptionOrderId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRESCRIPTION_NOT_EXIST, "Prescription don't exist"));

        return convertPrescriptionRecordToDTO(prescriptionRecord, currentEstablishment);
    }

    /**
     * Convert prescription to DTO.
     *
     * @param prescriptionRecord Prescription Record
     * @return The DTO of Prescription Order Detail
     */
    private PrescriptionOrderDetailDTO convertPrescriptionRecordToDTO(PrescriptionRecord prescriptionRecord, Establishment currentEstablishment) {
        MedicalServiceOrder prescriptionServiceOrder = prescriptionRecord.getMedicalServiceOrder();
        if (CollectionUtils.isEmpty(prescriptionServiceOrder.getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "Order detail don't exist");
        }
        MedicalServiceOrderDetail prescriptionOrderDetail = prescriptionServiceOrder.getMedicalServiceOrderDetails().stream().
                filter(prescriptionServiceOrderDetail -> prescriptionServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY &&
                        prescriptionServiceOrderDetail.getEstablishment().getId() == currentEstablishment.getId()).
                findFirst().
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "Order detail don't exist"));

        // Delivery
        MedicalServiceOrderDetail deliveryOrderDetail = prescriptionServiceOrder.getMedicalServiceOrderDetails().stream().
                filter(prescriptionServiceOrderDetail -> prescriptionServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_DELIVERY &&
                        prescriptionServiceOrderDetail.getEstablishment().getId() == currentEstablishment.getId()).
                findFirst().
                orElse(null);

        PrescriptionOrderDetailDTO detail = new PrescriptionOrderDetailDTO();
        detail.setId(prescriptionRecord.getId());
        if (null != prescriptionRecord.getMedicalRecord()) {
            ExamineRecord examineRecord = prescriptionRecord.getMedicalRecord().getExamineRecord();

            if (CollectionUtils.isEmpty(examineRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
                throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST, "Treatment order don't exist");
            }
            MedicalServiceOrderDetail treatmentDetail = examineRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().
                    filter(treatmentOrderDetail -> treatmentOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_MEDICAL).
                    findFirst().
                    orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST, "Treatment order don't exist"));

            detail.setMedicalRecordId(prescriptionRecord.getMedicalRecord().getId());
            detail.setDoctorId(treatmentDetail.getHandleBy().getId());
            detail.setDoctorName(treatmentDetail.getHandleBy().getName());
            detail.setDiagnose(examineRecord.getDiagnose());
        }
        if (null != prescriptionOrderDetail.getHandleBy()) {
            detail.setHandleById(prescriptionOrderDetail.getHandleBy().getId());
            detail.setHandleByName(prescriptionOrderDetail.getHandleBy().getName());
        }
        detail.setStatus(prescriptionOrderDetail.getStatus());
        detail.setPaymentStatus(prescriptionServiceOrder.getPaymentStatus());

        // Set Type
        if (null != deliveryOrderDetail) {
            detail.setDeliveryType(PrescriptionSupplyType.HOME);
            detail.setNotificationTransport(Helper.isNotificationTransport(deliveryOrderDetail));
        } else {
            detail.setDeliveryType(PrescriptionSupplyType.DRUG_STORE);
        }


        // Price
        List<PrescriptionRecordDetailDTO> prices = prescriptionRecordRepository.findPrescriptionPrice(prescriptionRecord.getId(), LocalDateTime.now());
        Map<Integer, Integer> mapPrices = prices.stream().collect(Collectors.toMap(PrescriptionRecordDetailDTO::getId, PrescriptionRecordDetailDTO::getPricePerUnit));

        // Prescription Details
        List<PrescriptionDetail> prescriptionDetails = prescriptionRecord.getPrescriptionDetails();
        List<DrugDetailDTO> drugDetails = new ArrayList<>();
        if (!CollectionUtils.isEmpty(prescriptionDetails)) {
            prescriptionDetails.forEach(prescriptionDetail -> {
                if (prescriptionDetail.getMedicalServiceOrderDetail().getEstablishment().getId() == currentEstablishment.getId()) {
                    DrugDetailDTO drugDetail = new DrugDetailDTO();
                    drugDetail.setDrugId(prescriptionDetail.getDrugUnit().getDrug().getId());
                    drugDetail.setDrugName(prescriptionDetail.getDrugUnit().getDrug().getName());
                    drugDetail.setDrugUnitId(prescriptionDetail.getDrugUnit().getId());
                    drugDetail.setDrugUnitName(prescriptionDetail.getDrugUnit().getName());
                    drugDetail.setAmount(prescriptionDetail.getAmount());
                    if (!CollectionUtils.isEmpty(mapPrices)) {
                        drugDetail.setPricePerUnit(mapPrices.get(drugDetail.getDrugId()));
                    }
                    drugDetails.add(drugDetail);
                }
            });
        }
        detail.setDrugDetails(drugDetails);

        // Drug export detail
        if (!CollectionUtils.isEmpty(prescriptionRecord.getDrugOrderExport())) {
            DrugOrderExport drugOrderExport = prescriptionRecord.getDrugOrderExport().stream().
                    filter(orderExport -> null != orderExport.getDrugExport() && orderExport.getDrugExport().getExportFrom().getId() == currentEstablishment.getId()).
                    findFirst().orElse(null);
            if (null != drugOrderExport && !CollectionUtils.isEmpty(drugOrderExport.getDrugExport().getDrugExportDetails())) {
                List<DrugExportDetail> drugExportDetails = drugOrderExport.getDrugExport().getDrugExportDetails();
                Map<Integer, DrugDetailDTO> mapDrugExportDetail = new HashMap<>();
                drugExportDetails.forEach(drugExportDetail -> {
                    Integer drugId = drugExportDetail.getDrugUnit().getDrug().getId();
                    DrugDetailDTO drugDetail = mapDrugExportDetail.get(drugId);
                    if (null == drugDetail) {
                        drugDetail = new DrugDetailDTO();
                        drugDetail.setDrugId(drugExportDetail.getDrugUnit().getDrug().getId());
                        drugDetail.setDrugName(drugExportDetail.getDrugUnit().getDrug().getName());
                        drugDetail.setDrugUnitId(drugExportDetail.getDrugUnit().getId());
                        drugDetail.setDrugUnitName(drugExportDetail.getDrugUnit().getName());
                        drugDetail.setPricePerUnit(drugExportDetail.getPricePerUnit());
                    }
                    List<DrugMedicineBatchDTO> medicineBatchs = drugDetail.getMedicineBatchs();
                    if (CollectionUtils.isEmpty(medicineBatchs)) {
                        medicineBatchs = new ArrayList<>();
                    }
                    DrugMedicineBatchDTO drugMedicineBatch = new DrugMedicineBatchDTO();
                    if (null != drugExportDetail.getDrugImportDetail()) {
                        drugMedicineBatch.setId(drugExportDetail.getDrugImportDetail().getDrugImport().getId());
                        drugMedicineBatch.setDrugImportDetailId(drugExportDetail.getDrugImportDetail().getId());
                        drugMedicineBatch.setAmount(drugExportDetail.getAmount());
                    }
                    medicineBatchs.add(drugMedicineBatch);
                    drugDetail.setMedicineBatchs(medicineBatchs);
                    mapDrugExportDetail.put(drugId, drugDetail);
                });
                // Set amount
                mapDrugExportDetail.values().forEach(drugDetailDTO -> {
                    drugDetailDTO.setAmount(drugDetailDTO.getMedicineBatchs().stream().mapToInt(DrugMedicineBatchDTO::getAmount).sum());
                });

                // Set export Id
                detail.setExportId(drugOrderExport.getDrugExport().getId());
                detail.setDrugExportDetails(new ArrayList<>(mapDrugExportDetail.values()));
            }
        }
        return detail;
    }

    @Override
    public PrescriptionImageDto getPrescriptionImgDto(Integer prescriptionId, PatientDTO patientDTO) {

        PrescriptionRecord prescriptionRecord = prescriptionRecordRepository.findById(prescriptionId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRESCRIPTION_NOT_EXIST, "Prescription don't exist"));

        MedicalServiceOrder prescriptionServiceOrder = prescriptionRecord.getMedicalServiceOrder();
        if (CollectionUtils.isEmpty(prescriptionServiceOrder.getMedicalServiceOrderDetails())) {
            throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "Order detail don't exist");
        }

        Set<Integer> establishments = prescriptionServiceOrder.getMedicalServiceOrderDetails().stream().
                filter(prescriptionServiceOrderDetail -> prescriptionServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY).
                map(pres -> pres.getEstablishment().getId()).collect(Collectors.toSet());

        PatientPrescriptionDTO patientInfo = new PatientPrescriptionDTO();
        patientInfo.setId(patientDTO.getId());
        patientInfo.setName(patientDTO.getName());
        patientInfo.setAddress(patientDTO.getAddress());
        patientInfo.setDateOfBirth(patientDTO.getDateOfBirth().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

        PrescriptionImageDto detail = new PrescriptionImageDto();
        detail.setPatientInfo(patientInfo);
        detail.setId(prescriptionRecord.getId());
        if (null != prescriptionRecord.getMedicalRecord()) {
            ExamineRecord examineRecord = prescriptionRecord.getMedicalRecord().getExamineRecord();

            if (CollectionUtils.isEmpty(examineRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails())) {
                throw new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST, "Treatment order don't exist");
            }
            MedicalServiceOrderDetail treatmentDetail = examineRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails().stream().
                    filter(treatmentOrderDetail -> treatmentOrderDetail.getMedicalService().getId() == Constants.SERVICE_TREATMENT_MEDICAL).
                    findFirst().
                    orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST, "Treatment order don't exist"));

            detail.setMedicalRecordId(prescriptionRecord.getMedicalRecord().getId());
            detail.setDoctorId(treatmentDetail.getHandleBy().getId());
            detail.setDoctorName(treatmentDetail.getHandleBy().getName());
            detail.setDiagnose(examineRecord.getDiagnose());
        }

//        detail.setStatus(prescriptionOrderDetail.getStatus());
        detail.setPaymentStatus(prescriptionServiceOrder.getPaymentStatus());

        // Price
        List<PrescriptionRecordDetailDTO> prices = prescriptionRecordRepository.findPrescriptionPrice(prescriptionRecord.getId(), LocalDateTime.now());
        Map<Integer, Integer> mapPrices = prices.stream().collect(Collectors.toMap(PrescriptionRecordDetailDTO::getId, PrescriptionRecordDetailDTO::getPricePerUnit));

        // Drug export detail
        if (!CollectionUtils.isEmpty(prescriptionRecord.getDrugOrderExport())) {
            for (Integer establishmentId : establishments) {

                DrugOrderExport drugOrderExport = prescriptionRecord.getDrugOrderExport().stream().
                        filter(orderExport -> null != orderExport.getDrugExport() && orderExport.getDrugExport().getExportFrom().getId() == establishmentId).
                        findFirst().orElse(null);
                if (null != drugOrderExport && !CollectionUtils.isEmpty(drugOrderExport.getDrugExport().getDrugExportDetails())) {
                    List<DrugExportDetail> drugExportDetails = drugOrderExport.getDrugExport().getDrugExportDetails();
                    Map<Integer, DrugDetailImageDTO> mapDrugExportDetail = new HashMap<>();
                    drugExportDetails.forEach(drugExportDetail -> {
                        Integer drugId = drugExportDetail.getDrugUnit().getDrug().getId();
                        DrugDetailImageDTO drugDetail = mapDrugExportDetail.get(drugId);
                        if (null == drugDetail) {
                            drugDetail = new DrugDetailImageDTO();
                            drugDetail.setDrugId(drugExportDetail.getDrugUnit().getDrug().getId());
                            drugDetail.setDrugName(drugExportDetail.getDrugUnit().getDrug().getName());
                            drugDetail.setDrugUnitId(drugExportDetail.getDrugUnit().getId());
                            drugDetail.setDrugUnitName(drugExportDetail.getDrugUnit().getName());

                            drugDetail.setPricePerUnitNumber(drugExportDetail.getPricePerUnit());
                            drugDetail.setPricePerUnit(String.format("%,d", drugExportDetail.getPricePerUnit()));
                            drugDetail.setDrugStoreId(drugOrderExport.getDrugExport().getExportFrom().getId());
                            drugDetail.setDrugStoreName(drugOrderExport.getDrugExport().getExportFrom().getName());
                            drugDetail.setCreateDate(drugOrderExport.getDrugExport().getCreateAt().format(DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy")));
                        }
                        List<DrugMedicineBatchDTO> medicineBatchs = drugDetail.getMedicineBatchs();
                        if (CollectionUtils.isEmpty(medicineBatchs)) {
                            medicineBatchs = new ArrayList<>();
                        }
                        DrugMedicineBatchDTO drugMedicineBatch = new DrugMedicineBatchDTO();
                        if (null != drugExportDetail.getDrugImportDetail()) {
                            drugMedicineBatch.setId(drugExportDetail.getDrugImportDetail().getDrugImport().getId());
                            drugMedicineBatch.setDrugImportDetailId(drugExportDetail.getDrugImportDetail().getId());
                            drugMedicineBatch.setAmount(drugExportDetail.getAmount());
                        }
                        medicineBatchs.add(drugMedicineBatch);
                        drugDetail.setMedicineBatchs(medicineBatchs);
                        mapDrugExportDetail.put(drugId, drugDetail);
                    });

                    // Set amount
                    mapDrugExportDetail.values().forEach(drugDetailDTO -> {
                        drugDetailDTO.setAmount(drugDetailDTO.getMedicineBatchs().stream().mapToInt(DrugMedicineBatchDTO::getAmount).sum());
                        int price = drugDetailDTO.getAmount() * drugDetailDTO.getPricePerUnitNumber();
                        drugDetailDTO.setTotalPrice(String.format("%,d", price));
                        drugDetailDTO.setTotalPriceNumber(price);
                    });

                    Integer payAmount =
                            mapDrugExportDetail.values().stream().mapToInt(rec -> rec.getTotalPriceNumber()).sum();
                    detail.setTotalPayAmount(payAmount);
                    detail.setTotalPayAmountShow(String.format("%,d", payAmount));

                    detail.setDrugExportDetails(new ArrayList<>(mapDrugExportDetail.values()));
                }

            }
        } else {
            // Prescription Details
            List<PrescriptionDetail> prescriptionDetails = prescriptionRecord.getPrescriptionDetails();
            List<DrugDetailImageDTO> drugExportDetails = new ArrayList<>();
            if (!CollectionUtils.isEmpty(prescriptionDetails)) {
                for (Integer establishmentId : establishments) {
                    prescriptionDetails.forEach(prescriptionDetail -> {
                        if (prescriptionDetail.getMedicalServiceOrderDetail().getEstablishment().getId() == establishmentId) {
                            DrugDetailImageDTO drugDetail = new DrugDetailImageDTO();
                            drugDetail.setDrugId(prescriptionDetail.getDrugUnit().getDrug().getId());
                            drugDetail.setDrugName(prescriptionDetail.getDrugUnit().getDrug().getName());
                            drugDetail.setDrugUnitId(prescriptionDetail.getDrugUnit().getId());
                            drugDetail.setDrugUnitName(prescriptionDetail.getDrugUnit().getName());
                            drugDetail.setAmount(prescriptionDetail.getAmount());
                            if (!CollectionUtils.isEmpty(mapPrices)) {
                                drugDetail.setPricePerUnitNumber(mapPrices.get(drugDetail.getDrugId()));
                                drugDetail.setPricePerUnit(drugDetail.getPricePerUnit());
                            }
                            drugExportDetails.add(drugDetail);
                        }
                    });
                }

                drugExportDetails.forEach(rec -> {
                    int price = rec.getAmount() * rec.getPricePerUnitNumber();
                    rec.setTotalPrice(String.format("%,d", price));
                    rec.setTotalPriceNumber(price);
                });

                Integer payAmount =
                        drugExportDetails.stream().mapToInt(rec -> rec.getTotalPriceNumber()).sum();
                detail.setTotalPayAmount(payAmount);
                detail.setTotalPayAmountShow(String.format("%,d", payAmount));

            }
            detail.setDrugExportDetails(drugExportDetails);
        }
        return detail;
    }

    @Override
    @Transactional
    public PrescriptionOrderDetailDTO supplyMedicine(int prescriptionRecordId, List<DrugDetailDTO> drugDetails, Establishment exportFrom, Account currentAccount) {
        PrescriptionOrderDetailDTO result = null;
        if (!CollectionUtils.isEmpty(drugDetails)) {
            PrescriptionRecord prescriptionRecord = prescriptionRecordRepository.findById(prescriptionRecordId).
                    orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRESCRIPTION_NOT_EXIST, "Prescription don't exist"));

            MedicalServiceOrder prescriptionServiceOrder = prescriptionRecord.getMedicalServiceOrder();
            if (CollectionUtils.isEmpty(prescriptionServiceOrder.getMedicalServiceOrderDetails())) {
                throw new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "Order detail don't exist");
            }
            // Get list order detail of current establishment
            List<MedicalServiceOrderDetail> serviceOrderDetails = prescriptionServiceOrder.getMedicalServiceOrderDetails().stream().
                    filter(medicalServiceOrderDetail -> medicalServiceOrderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_SUPPLY &&
                            medicalServiceOrderDetail.getEstablishment().getId() == exportFrom.getId()).
                    collect(Collectors.toList());
            if (serviceOrderDetails.stream().
                    anyMatch(medicalServiceOrderDetail -> !MedicalServiceOrderStatus.REQUEST_PROCESSING.equals(medicalServiceOrderDetail.getStatus()))) {
                throw new MedicException(MedicException.ERROR_PRESCRIPTION_NOT_HAVE_PROCESSING_STATUS, "Prescription must have processing status");
            }

            if (CollectionUtils.isEmpty(prescriptionRecord.getPrescriptionDetails())) {
                throw new MedicException(MedicException.ERROR_PRESCRIPTION_DETAIL_NOT_EXIST, "The prescription details is empty");
            }

            // Get list prescription detail
            List<PrescriptionDetail> prescriptionDetails = prescriptionRecord.getPrescriptionDetails().stream().
                    filter(prescriptionDetail -> null != prescriptionDetail.getMedicalServiceOrderDetail() &&
                            prescriptionDetail.getMedicalServiceOrderDetail().getEstablishment().getId() == exportFrom.getId()).
                    collect(Collectors.toList());
            if (CollectionUtils.isEmpty(prescriptionDetails) ||
                    CollectionUtils.isEmpty(drugDetails) ||
                    prescriptionDetails.size() < drugDetails.size()) {
                throw new MedicException(MedicException.ERROR_PRESCRIPTION_DETAIL_IS_DIFF, "The prescription details/list drug is empty or different");
            }

            drugDetails.forEach(drugDetailDTO -> {
                if (!drugService.verifyAmountDrugOfMedicineBatch(drugDetailDTO.getMedicineBatchs(), drugDetailDTO.getDrugId(), drugDetailDTO.getDrugUnitId(), exportFrom)) {
                    throw new MedicException(MedicException.ERROR_NUMBER_OF_AMOUNT_NOT_ENOUGH, "Number of amount drug isn't enough");
                }
            });

            // Get List Drug Unit
            List<Integer> drugUnitIds = drugDetails.stream().map(DrugDetailDTO::getDrugUnitId).collect(Collectors.toList());
            List<DrugUnit> drugUnits = drugUnitRepository.findAllByIdIn(drugUnitIds);
            Map<Integer, DrugUnit> mapDrugUnits = drugUnits.stream().collect(Collectors.toMap(DrugUnit::getId, drugUnit -> drugUnit));
            // Get List Drug Import Detail
            List<Integer> drugImportDetailIds = drugDetails.stream().flatMap(drugDetailDTO -> drugDetailDTO.getMedicineBatchs().stream().map(DrugMedicineBatchDTO::getDrugImportDetailId)).collect(Collectors.toList());
            List<DrugImportDetail> drugImportDetails = drugImportDetailRepository.findAllByIdIn(drugImportDetailIds);
            Map<Integer, DrugImportDetail> mapDrugImportDetail = drugImportDetails.stream().collect(Collectors.toMap(DrugImportDetail::getId, drugImportDetail -> drugImportDetail));

            // Update payment status for retail prescription
            if (null == prescriptionRecord.getMedicalRecord()) {
                prescriptionServiceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
                prescriptionServiceOrder.setPaymentDate(LocalDateTime.now());
                prescriptionServiceOrder.setTotalAmount(serviceOrderDetails.stream().mapToInt(MedicalServiceOrderDetail::getTxAmount).sum());
                medicalServiceOrderRepository.save(prescriptionServiceOrder);
            }

            serviceOrderDetails.forEach(medicalServiceOrderDetail -> {
                // Update account handle order
                if (null != currentAccount) {
                    medicalServiceOrderDetail.setHandleBy(currentAccount);
                }
                // Update status of order to fulfilled
                medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_FULFILLED);
                medicalServiceOrderDetail.setResultDate(LocalDateTime.now());
            });
            medicalServiceOrderDetailRepository.saveAll(serviceOrderDetails);

            DrugExport drugExport = new DrugExport();
            drugExport.setExportFrom(exportFrom);
            DrugExport drugExportResult = storageExportRepository.save(drugExport);

            List<DrugExportDetail> drugExportDetails = new ArrayList<>();
            drugDetails.forEach((detail -> {
                if (CollectionUtils.isEmpty(detail.getMedicineBatchs())) {
                    throw new MedicException(MedicException.ERROR_MEDICINE_BATCH_NOT_EXIST);
                }
                DrugUnit drugUnit = mapDrugUnits.get(detail.getDrugUnitId());
                if (null != drugUnit && drugUnit.getDrug().getId() == detail.getDrugId()) {
                    int amountBatch = detail.getMedicineBatchs().stream().mapToInt(DrugMedicineBatchDTO::getAmount).sum();
                    if (amountBatch != detail.getAmount()) {
                        throw new MedicException(MedicException.ERROR_NUMBER_OF_AMOUNT_NOT_EQUAL);
                    }
                    detail.getMedicineBatchs().forEach(drugMedicineBatchDTO -> {
                        DrugImportDetail drugImportDetail = mapDrugImportDetail.get(drugMedicineBatchDTO.getDrugImportDetailId());
                        if (null == drugImportDetail) {
                            throw new MedicException(MedicException.ERROR_DRUG_IMPORT_DETAIL_NOT_EXIST, "Can't find drug import detail");
                        }
                        DrugExportDetail drugExportDetail = new DrugExportDetail();
                        drugExportDetail.setDrugExport(drugExportResult);
                        drugExportDetail.setAmount(drugMedicineBatchDTO.getAmount());
                        drugExportDetail.setDrugUnit(drugUnit);
                        drugExportDetail.setPricePerUnit(getPriceOfDrug(drugUnit));
                        drugExportDetail.setDrugImportDetail(drugImportDetail);

                        drugExportDetails.add(drugExportDetail);
                    });
                } else {
                    throw new MedicException(MedicException.ERROR_DRUG_NOT_EXIST);
                }
            }));
            List<DrugExportDetail> drugExportDetailsResult = drugExportDetailRepository.saveAll(drugExportDetails);
            drugExport.setDrugExportDetails(drugExportDetailsResult);

            DrugOrderExport drugOrderExport = new DrugOrderExport();
            drugOrderExport.setDrugExport(drugExport);
            drugOrderExport.setPrescriptionRecord(prescriptionRecord);
            drugOrderExport = drugOrderExportRepository.save(drugOrderExport);
            prescriptionRecord.setDrugOrderExport(Collections.singletonList(drugOrderExport));
            result = convertPrescriptionRecordToDTO(prescriptionRecord, exportFrom);

            // Notification
            if (null != prescriptionRecord.getMedicalRecord()) {
                notificationService.notificationPrescriptionStatus(serviceOrderDetails.get(0));
            }
        }

        return result;
    }

    /**
     * Get current price of drug unit.
     *
     * @param drugUnit the drug unit
     * @return price of drug unit
     */
    private int getPriceOfDrug(DrugUnit drugUnit) {
        int price = 0;
        if (null != drugUnit && !CollectionUtils.isEmpty(drugUnit.getDrugUnitPriceHistories())) {
            price = drugUnit.getDrugUnitPriceHistories().stream().
                    filter(drugUnitPriceHistory -> !drugUnitPriceHistory.getEffectiveAt().isAfter(LocalDateTime.now())).
                    max(Comparator.comparing(DrugUnitPriceHistory::getEffectiveAt)).
                    map(DrugUnitPriceHistory::getPricePerUnit).
                    orElse(0);
        }
        return price;
    }

    @Override
    @Transactional
    public PrescriptionOrderDetailDTO createRetailPrescription(List<DrugDetailDTO> drugDetails, Establishment establishment) {
        PrescriptionOrderDetailDTO result = null;
        if (!CollectionUtils.isEmpty(drugDetails)) {
            // Get List Drug Unit
            List<Integer> drugUnitIds = drugDetails.stream().map(DrugDetailDTO::getDrugUnitId).collect(Collectors.toList());
            List<DrugUnit> drugUnits = drugUnitRepository.findAllByIdIn(drugUnitIds);
            if (!CollectionUtils.isEmpty(drugUnits)) {
                Map<Integer, DrugUnit> mapDrugUnits = drugUnits.stream().
                        filter(drugUnit -> !drugUnit.getDrug().getIsDisabled()).
                        collect(Collectors.toMap(DrugUnit::getId, drugUnit -> drugUnit));

                MedicalService supplyMedicineService = medicalServiceRepository.findById(Constants.SERVICE_MEDICINE_SUPPLY).
                        orElseThrow(() -> new MedicException(MedicException.ERROR_MEDICAL_SERVICE_NOT_EXIST, "Medicine supply service don't exist"));

                // Create Medical Service Order
                MedicalServiceOrder retailPrescriptionOrder = new MedicalServiceOrder();
                retailPrescriptionOrder.setPaymentStatus(PaymentStatus.PENDING);
                MedicalServiceOrder retailPrescriptionOrderResult = medicalServiceOrderRepository.save(retailPrescriptionOrder);

                // Create prescription record
                PrescriptionRecord prescriptionRecord = new PrescriptionRecord();
                prescriptionRecord.setMedicalServiceOrder(retailPrescriptionOrderResult);
                PrescriptionRecord prescriptionRecordResult = prescriptionRecordRepository.save(prescriptionRecord);
                prescriptionRecordResult.setMedicalServiceOrder(retailPrescriptionOrderResult);

                List<MedicalServiceOrderDetail> serviceOrderDetails = new ArrayList<>();
                List<PrescriptionDetail> prescriptionDetails = new ArrayList<>();
                drugDetails.forEach(drugDetailDTO -> {
                    DrugUnit drugUnit = mapDrugUnits.get(drugDetailDTO.getDrugUnitId());
                    if (null != drugUnit && drugUnit.getDrug().getId() == drugDetailDTO.getDrugId()) {
                        // Create medical service order for each drug.
                        MedicalServiceOrderDetail retailPrescriptionOrderDetail = new MedicalServiceOrderDetail();
                        retailPrescriptionOrderDetail.setMedicalServiceOrder(retailPrescriptionOrderResult);
                        retailPrescriptionOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
                        retailPrescriptionOrderDetail.setEstablishment(establishment);
                        retailPrescriptionOrderDetail.setMedicalService(supplyMedicineService);
                        retailPrescriptionOrderDetail.setTxAmount(drugDetailDTO.getAmount() * getPriceOfDrug(drugUnit));
                        retailPrescriptionOrderDetail = medicalServiceOrderDetailRepository.save(retailPrescriptionOrderDetail);
                        serviceOrderDetails.add(retailPrescriptionOrderDetail);

                        // Create prescription detail
                        PrescriptionDetail prescriptionDetail = new PrescriptionDetail();
                        prescriptionDetail.setAmount(drugDetailDTO.getAmount());
                        prescriptionDetail.setDrugUnit(drugUnit);
                        prescriptionDetail.setPrescriptionRecord(prescriptionRecordResult);
                        prescriptionDetail.setMedicalServiceOrderDetail(retailPrescriptionOrderDetail);
                        prescriptionDetails.add(prescriptionDetail);
                    }
                });
                retailPrescriptionOrderResult.setMedicalServiceOrderDetails(serviceOrderDetails);
                if (!CollectionUtils.isEmpty(prescriptionDetails)) {
                    List<PrescriptionDetail> prescriptionDetailResults = prescriptionDetailRepository.saveAll(prescriptionDetails);
                    prescriptionRecord.setPrescriptionDetails(prescriptionDetailResults);
                }
                result = convertPrescriptionRecordToDTO(prescriptionRecordResult, establishment);
            } else {
                throw new MedicException(MedicException.ERROR_LIST_DRUG_IS_EMPTY, "List drug is empty");
            }
        }
        return result;
    }

    @Override
    public boolean notificationPrescriptionTransportTime(int prescriptionId, Establishment currentEstablishment) {
        boolean result = false;
        PrescriptionRecord prescriptionRecord = prescriptionRecordRepository.findById(prescriptionId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_PRESCRIPTION_NOT_EXIST, "Prescription don't exist"));

        List<MedicalServiceOrderDetail> serviceOrderDetails = prescriptionRecord.getMedicalServiceOrder().getMedicalServiceOrderDetails();
        MedicalServiceOrderDetail transportService = serviceOrderDetails.stream().
                filter(orderDetail -> orderDetail.getMedicalService().getId() == Constants.SERVICE_MEDICINE_DELIVERY &&
                        orderDetail.getEstablishment().getId() == currentEstablishment.getId()).
                findFirst().
                orElse(null);
        MedicalRecord medicalRecord = prescriptionRecord.getMedicalRecord();
        if (null != medicalRecord && null != transportService &&
                MedicalServiceOrderStatus.REQUEST_ACCEPTED.equals(transportService.getStatus())) {
            transportService.setStatus(MedicalServiceOrderStatus.REQUEST_PROCESSING);
            medicalServiceOrderDetailRepository.save(transportService);
            String drugStoreName = currentEstablishment.getName();
            Account patient = medicalRecord.getPatient().getAccount();
            notificationService.notificationPrescriptionTransport(drugStoreName, patient, medicalRecord.getId());
            result = true;
        }
        return result;
    }
}
