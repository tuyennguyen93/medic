package com.mmedic.rest.healthFacility.repo;

import com.mmedic.entity.Establishment;
import com.mmedic.rest.healthFacility.dto.DrugStoreQueryDTO;
import com.mmedic.rest.healthFacility.dto.PreclinicalQueryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface HealthFacilityRepository extends JpaRepository<Establishment, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT data.id AS id, data.name AS name, data.address AS address, data.phone_number AS phoneNumber, data.avatar_url AS avatarUrl, data.rating_average AS ratingAverage, data.distance AS distance FROM ( " +
                    "SELECT es.id, " +
                    "       es.name, " +
                    "       es.address, " +
                    "       es.phone_number, " +
                    "       es.avatar_url, " +
                    "       es.rating_average, " +
                    "       ( " +
                    "           6371 * ACOS( " +
                    "                       COS(RADIANS(:latitude)) * COS(RADIANS(es.latitude)) * " +
                    "                       COS(RADIANS(es.longitude) - RADIANS(:longitude)) + " +
                    "                       SIN(RADIANS(:latitude)) * SIN(RADIANS(es.latitude)) " +
                    "           ) " +
                    "       ) AS distance " +
                    "FROM establishment es " +
                    "LEFT JOIN medical_service_registry msr on es.id = msr.establishment_id " +
                    "WHERE establishment_type = 'DRUG_STORE' " +
                    "  AND msr.medical_service_id = 3 " +
                    "  AND msr.active = 1 " +
                    "  AND (es.name LIKE :condition OR es.address LIKE :condition)" +
                    ") AS data",
            countQuery = "SELECT COUNT(*) " +
                    "FROM establishment es " +
                    "LEFT JOIN medical_service_registry msr on es.id = msr.establishment_id " +
                    "WHERE establishment_type = 'DRUG_STORE' " +
                    "  AND msr.medical_service_id = 3 " +
                    "  AND msr.active = 1 " +
                    "  AND (es.name LIKE :condition OR es.address LIKE :condition)")
    Page<DrugStoreQueryDTO> findDrugStore(@Param("condition") String condition, @Param("latitude") double latitude, @Param("longitude") double longitude, Pageable pageable);

    /**
     * Find list drug store by name.
     *
     * @param condition the condition value
     * @return list drug
     */
    @Query("SELECT es.id AS id, es.name AS name FROM Establishment es " +
            "LEFT JOIN es.medicalServiceRegistries msr " +
            "LEFT JOIN msr.medicalService ms " +
            "WHERE es.establishmentType = 'DRUG_STORE' " +
            "AND ms.id = 3 " +
            "AND msr.active = 1 " +
            "AND es.name LIKE %:condition%")
    List<DrugStoreQueryDTO> findDrugStore(@Param("condition") String condition);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT data.id             AS id, " +
                    "       data.name           AS name, " +
                    "       data.address        AS address, " +
                    "       data.phone_number   AS phoneNumber, " +
                    "       data.avatar_url     AS avatarUrl, " +
                    "       data.rating_average AS ratingAverage, " +
                    "       data.price          AS price," +
                    "       data.distance       AS distance " +
                    "FROM ( " +
                    "         SELECT es.id, " +
                    "                es.name, " +
                    "                es.address, " +
                    "                es.phone_number, " +
                    "                es.avatar_url, " +
                    "                es.rating_average, " +
                    "                ( " +
                    "                     6371 * ACOS( " +
                    "                                 COS(RADIANS(:latitude)) * COS(RADIANS(es.latitude)) * " +
                    "                                 COS(RADIANS(es.longitude) - RADIANS(:longitude)) + " +
                    "                                 SIN(RADIANS(:latitude)) * SIN(RADIANS(es.latitude)) " +
                    "                     ) " +
                    "                 ) AS distance, " +
                    "                msr.id            AS serviceRegistryId, " +
                    "                msph.effective_at AS effectiveAt, " +
                    "                msph.price " +
                    "         FROM establishment es " +
                    "                  LEFT JOIN medical_service_registry msr on es.id = msr.establishment_id " +
                    "                  LEFT JOIN medical_service_price_history msph on msr.id = msph.medical_service_registry_id " +
                    "         WHERE es.establishment_type = 'PRECLINICAL' " +
                    "           AND msr.medical_service_id = :preclinicalServiceId " +
                    "           AND msr.active = 1 " +
                    "           AND ( " +
                    "                 es.name LIKE :condition " +
                    "                 OR es.address LIKE :condition " +
                    "             ) " +
                    "     ) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT medical_service_registry_id, MAX(effective_at) AS max_eff " +
                    "    FROM medical_service_price_history " +
                    "    WHERE effective_at <= CURRENT_TIME " +
                    "    GROUP BY medical_service_registry_id " +
                    ") AS price ON data.serviceRegistryId = price.medical_service_registry_id AND data.effectiveAt = price.max_eff",
            countQuery = "SELECT COUNT(DISTINCT data.id) " +
                    "FROM ( " +
                    "         SELECT es.id, " +
                    "                msr.id            AS serviceRegistryId, " +
                    "                msph.effective_at AS effectiveAt " +
                    "         FROM establishment es " +
                    "                  LEFT JOIN medical_service_registry msr on es.id = msr.establishment_id " +
                    "                  LEFT JOIN medical_service_price_history msph on msr.id = msph.medical_service_registry_id " +
                    "         WHERE es.establishment_type = 'PRECLINICAL' " +
                    "           AND msr.medical_service_id = 4 " +
                    "           AND msr.active = 1 " +
                    "           AND ( " +
                    "                 es.name LIKE :condition " +
                    "                 OR es.address LIKE :condition " +
                    "             ) " +
                    "     ) AS data " +
                    "         INNER JOIN ( " +
                    "    SELECT medical_service_registry_id, MAX(effective_at) AS max_eff " +
                    "    FROM medical_service_price_history " +
                    "    WHERE effective_at <= CURRENT_TIME " +
                    "    GROUP BY medical_service_registry_id " +
                    ") AS price ON data.serviceRegistryId = price.medical_service_registry_id AND data.effectiveAt = price.max_eff")
    Page<PreclinicalQueryDTO> findPreclinicals(@Param("condition") String condition, @Param("preclinicalServiceId") int preclinicalServiceId, @Param("latitude") double latitude, @Param("longitude") double longitude, Pageable pageable);

    @Query(nativeQuery = true,
            value = "SELECT DISTINCT es.id             AS id, " +
                    "                es.name           AS name, " +
                    "                es.address        AS address, " +
                    "                es.phone_number   AS phoneNumber, " +
                    "                es.avatar_url     AS avatarUrl, " +
                    "                es.rating_average AS ratingAverage " +
                    "FROM establishment es " +
                    "         LEFT JOIN medical_service_registry msr on es.id = msr.establishment_id " +
                    "WHERE es.establishment_type = 'PRECLINICAL' " +
                    "  AND msr.active = 1 " +
                    "  AND ( " +
                    "        es.name LIKE :condition " +
                    "        OR es.address LIKE :condition " +
                    "    )",
            countQuery = "SELECT COUNT(DISTINCT es.id) " +
                    "FROM establishment es " +
                    "         LEFT JOIN medical_service_registry msr on es.id = msr.establishment_id " +
                    "WHERE es.establishment_type = 'PRECLINICAL' " +
                    "  AND msr.active = 1 " +
                    "  AND ( " +
                    "        es.name LIKE :condition " +
                    "        OR es.address LIKE :condition " +
                    "    )")
    Page<PreclinicalQueryDTO> findBasicInfoOfPreclinicals(@Param("condition") String condition, Pageable pageable);
}
