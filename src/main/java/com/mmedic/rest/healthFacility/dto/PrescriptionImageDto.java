package com.mmedic.rest.healthFacility.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.rest.patient.dto.PatientPrescriptionDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PrescriptionImageDto {

    private int id;

    private Integer medicalRecordId;

    private PatientPrescriptionDTO patientInfo;

    private Integer doctorId;

    private String doctorName;

    private String diagnose;

    private List<DrugDetailImageDTO> drugExportDetails;

    private MedicalServiceOrderStatus status;

    private PaymentStatus paymentStatus;

    private Integer totalPayAmount;

    private String totalPayAmountShow;

}
