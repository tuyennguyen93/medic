package com.mmedic.rest.healthFacility.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.enums.PrescriptionSupplyType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PrescriptionOrderTodayDTO {

    private int id;

    private Integer patientId;

    private String name;

    private String avatarUrl;

    private LocalDate dateOfBirth;

    private MedicalServiceOrderStatus status;

    private PaymentStatus paymentStatus;

    private PrescriptionSupplyType deliveryType;
}
