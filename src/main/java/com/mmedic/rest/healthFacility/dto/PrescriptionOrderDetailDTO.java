package com.mmedic.rest.healthFacility.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.enums.PrescriptionSupplyType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PrescriptionOrderDetailDTO {

    private int id;

    private Integer medicalRecordId;

    private Integer doctorId;

    private String doctorName;

    private String diagnose;

    private Integer handleById;

    private String handleByName;

    private Integer exportId;

    private List<DrugDetailDTO> drugDetails;

    private List<DrugDetailDTO> drugExportDetails;

    private MedicalServiceOrderStatus status;

    private PaymentStatus paymentStatus;

    private PrescriptionSupplyType deliveryType;

    private boolean isNotificationTransport;
}
