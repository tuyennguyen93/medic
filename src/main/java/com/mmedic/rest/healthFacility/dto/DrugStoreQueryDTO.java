package com.mmedic.rest.healthFacility.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Drug Store Search Result")
public interface DrugStoreQueryDTO {

    int getId();

    String getName();

    @ApiModelProperty("Establishment Address")
    String getAddress();

    @ApiModelProperty("Establishment Phone Number")
    String getPhoneNumber();

    @ApiModelProperty("Avatar URL")
    String getAvatarUrl();

    @ApiModelProperty("Rating Average")
    Float getRatingAverage();
}
