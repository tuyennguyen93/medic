package com.mmedic.rest.healthFacility.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Preclinical Search Result")
public interface PreclinicalQueryDTO {

    @ApiModelProperty("Preclinical Id")
    int getId();

    @ApiModelProperty("Preclinical Name")
    String getName();

    @ApiModelProperty("Establishment Address")
    String getAddress();

    @ApiModelProperty("Establishment Phone Number")
    String getPhoneNumber();

    @ApiModelProperty("Avatar URL")
    String getAvatarUrl();

    @ApiModelProperty("Price")
    Integer getPrice();

    @ApiModelProperty("Rating Average")
    float getRatingAverage();

    @ApiModelProperty("Available")
    Boolean getAvailable();
}
