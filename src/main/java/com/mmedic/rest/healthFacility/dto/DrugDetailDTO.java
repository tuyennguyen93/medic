package com.mmedic.rest.healthFacility.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DrugDetailDTO {

    private int drugId;

    private String drugName;

    private int drugUnitId;

    private String drugUnitName;

    private int amount;

    private int pricePerUnit;

    List<DrugMedicineBatchDTO> medicineBatchs;
}
