package com.mmedic.rest.healthFacility.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;

import java.time.LocalDate;

public interface PrescriptionOrderDTO {

    int getId();

    Integer getPatientId();

    String getName();

    String getAvatarUrl();

    LocalDate getDateOfBirth();

    MedicalServiceOrderStatus getStatus();

    PaymentStatus getPaymentStatus();
}
