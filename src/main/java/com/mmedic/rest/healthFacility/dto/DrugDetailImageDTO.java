package com.mmedic.rest.healthFacility.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DrugDetailImageDTO {

    private int drugId;

    private String drugName;

    private int drugUnitId;

    private String drugUnitName;

    private int amount;

    private String pricePerUnit;

    private int pricePerUnitNumber;

    List<DrugMedicineBatchDTO> medicineBatchs;

    private String totalPrice;

    private int totalPriceNumber;

    private Integer drugStoreId;

    private String drugStoreName;

    private Integer handleById;

    private String handleByName;

    private String createDate;

}
