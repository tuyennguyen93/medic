package com.mmedic.rest.healthFacility.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class DrugMedicineBatchDTO {

    @ApiModelProperty("Medicine Batch Number")
    private int id;

    private int drugImportDetailId;

    private int amount;
}
