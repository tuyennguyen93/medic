package com.mmedic.rest.payment;

import com.mmedic.config.prop.PaymentProperties;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.medicalRecord.MedicalRecordService;
import com.mmedic.service.payment.zalopay.dto.PaymentOrderResultDTO;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Helper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Mac;
import javax.xml.bind.DatatypeConverter;

/**
 * Payment Controller.
 */
@Slf4j
@Api(value = "Payment Controller")
@RestController
@RequestMapping("/api/v1/payment")
@RequiredArgsConstructor
public class PaymentController {

    private final MedicalRecordService medicalRecordService;

    private final PaymentProperties paymentProperties;

    /**
     * Create zalopay order for examine.
     *
     * @param medicalRecordId medical record id
     * @return zalopay result
     */
    @ApiOperation("Create payment order for examine (DONE)")
    @PostMapping("/medical-records/{id}/payment-order/examine")
    public MedicResponse<PaymentOrderResultDTO> createOrderExamine(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId) {
        return MedicResponse.okStatus(medicalRecordService.createPaymentOrderExamine(medicalRecordId));
    }

    /**
     * Create zalopay order for prescription.
     *
     * @param medicalRecordId medical record id
     * @return zalopay result
     */
    @ApiOperation("Create payment order for prescription (DONE)")
    @PostMapping("/medical-records/{id}/payment-order/prescription")
    public MedicResponse<PaymentOrderResultDTO> createOrderPrescription(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId) {
        return MedicResponse.okStatus(medicalRecordService.createPaymentOrderPrescription(medicalRecordId));
    }

    /**
     * Create zalopay order for preclinical.
     *
     * @param medicalRecordId medical record id
     * @return zalopay result
     */
    @ApiOperation("Create payment order for prescription (DONE)")
    @PostMapping("/medical-records/{id}/payment-order/preclinical")
    public MedicResponse<PaymentOrderResultDTO> createOrderPreclinical(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId) {
        return MedicResponse.okStatus(medicalRecordService.createPaymentOrderPreclinical(medicalRecordId));
    }

    /**
     * Create zalopay order for health care service.
     *
     * @param medicalRecordId medical record id
     * @return zalopay result
     */
    @ApiOperation("Create payment order for prescription (DONE)")
    @PostMapping("/medical-records/{id}/payment-order/health-care")
    public MedicResponse<PaymentOrderResultDTO> createOrderHealthCareService(@PathVariable("id") @ApiParam("Medical Record Id") int medicalRecordId) {
        return MedicResponse.okStatus(medicalRecordService.createPaymentOrderHealthCareService(medicalRecordId));
    }

    /**
     * Callback function for zalopay server, this is api to update order status.
     *
     * @param paymentResultJson payment result from zalopay
     * @return result to zalopay server
     */
    @ApiOperation("Callback function for payment (DONE)")
    @PostMapping("/callback")
    public String processCallback(@RequestBody String paymentResultJson) {
        Mac HmacSHA256 = Helper.getHmacSHA256(paymentProperties.getZalopay().getKey2());
        JSONObject result = new JSONObject();
        log.info("############################################################");
        log.info("############################################################");
        log.info("### ZALOPAY CALLBACK: " + paymentResultJson);
        log.info("############################################################");
        log.info("############################################################");

        try {
            JSONObject paymentResult = new JSONObject(paymentResultJson);
            String resultData = paymentResult.getString("data");
            String resultMac = paymentResult.getString("mac");

            byte[] hashBytes = HmacSHA256.doFinal(resultData.getBytes());
            String mac = DatatypeConverter.printHexBinary(hashBytes).toLowerCase();

            // Check data is valid (from ZaloPay server)
            if (!resultMac.equals(mac)) {
                // callback data invalid
                result.put("returncode", -1);
                result.put("returnmessage", "MAC not equal");
                log.error("### ERROR: zalopay MAC not equal");
            } else {
                // Payment successfully
                JSONObject data = new JSONObject(resultData);
                log.info("### Update order's status = success where apptransid = " + data.getString("apptransid"));

                if (medicalRecordService.updateOrderStatus(data)) {
                    result.put("returncode", 1);
                    result.put("returnmessage", "success");
                } else {
                    result.put("returncode", 0);
                    result.put("returnmessage", "Error update order status");
                }
            }
        } catch (Exception ex) {
            if (ex instanceof MedicException &&
                    MedicException.ERROR_SERVICE_ORDER_HAS_PAID.equals(((MedicException) ex).getMessageCode())) {
                result.put("returncode", -1);
            } else {
                log.error("### ERROR: callback again", ex);
                result.put("returncode", 0); // ZaloPay server will callback again (max is 3 times)
            }
            result.put("returnmessage", ex.getMessage());
        }

        // Notify ZaloPay server
        return result.toString();
    }

}
