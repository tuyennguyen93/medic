package com.mmedic.rest.sysSetting.repo;

import com.mmedic.entity.SysSetting;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * SysSetting Repository.
 *
 * @author tuyennv
 */
public interface SysSettingRepository extends JpaRepository<SysSetting, Integer> {
    Optional<SysSetting> findByKey(String key);
}
