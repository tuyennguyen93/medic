package com.mmedic.rest.sysSetting.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SysSettingDTO {

    @ApiModelProperty("Sys-setting Id")
    private Integer id;

    @ApiModelProperty("Key")
    private String key;

    @ApiModelProperty("Value")
    private String value;

    @ApiModelProperty("updateBy")
    private Integer updateBy;

}
