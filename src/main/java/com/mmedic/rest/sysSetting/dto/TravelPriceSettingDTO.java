package com.mmedic.rest.sysSetting.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TravelPriceSettingDTO {

    @ApiModelProperty("Id")
    private Integer id;

    @ApiModelProperty("Added Km")
    private Integer addedKm;

    @ApiModelProperty("Price Per Km")
    private int pricePerKm;

}
