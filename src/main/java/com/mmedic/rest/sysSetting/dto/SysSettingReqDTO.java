package com.mmedic.rest.sysSetting.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SysSettingReqDTO {

    @ApiModelProperty("Key")
    private String key;

    @ApiModelProperty("Value")
    private String value;

}
