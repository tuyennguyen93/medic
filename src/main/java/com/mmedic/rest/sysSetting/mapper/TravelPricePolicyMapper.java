package com.mmedic.rest.sysSetting.mapper;

import com.mmedic.entity.TravelPricePolicy;
import com.mmedic.rest.sysSetting.dto.TravelPriceSettingDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel="spring")
public interface TravelPricePolicyMapper {

    TravelPriceSettingDTO convertToDTO(TravelPricePolicy entity);

    TravelPricePolicy convertToEntity(TravelPriceSettingDTO dto);

}
