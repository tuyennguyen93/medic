package com.mmedic.rest.sysSetting.mapper;

import com.mmedic.entity.Drug;
import com.mmedic.entity.DrugUnit;
import com.mmedic.entity.SysSetting;
import com.mmedic.rest.drug.dto.AllergyDrugDTO;
import com.mmedic.rest.drug.dto.DrugDTO;
import com.mmedic.rest.drug.dto.DrugUnitDTO;
import com.mmedic.rest.sysSetting.dto.SysSettingDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface SysSettingMapper {

    SysSettingDTO convertToDTO(SysSetting entity);

    SysSetting convertToEntity(SysSettingDTO dto);

    List<SysSettingDTO> toListDTO(List<SysSetting> list);


}
