package com.mmedic.rest.sysSetting;

import com.mmedic.entity.Account;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.sysSetting.dto.SysSettingDTO;
import com.mmedic.rest.sysSetting.dto.SysSettingReqDTO;
import com.mmedic.rest.sysSetting.dto.TravelPriceSettingDTO;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * System Setting controller.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/sys-settings")
@Api(value = "Sys Settings Controller", description = "Sys Settings Management")
@RequiredArgsConstructor
public class SysSettingController {

    private final SysSettingService sysSettingService;

    private final AccountService accountService;

    private final BaseService baseService;

    @ApiOperation("Get sys-setting(DONE)")
    @GetMapping("/")
    public MedicResponse<List<SysSettingDTO>> getSysSetting() {
        List<SysSettingDTO> result = sysSettingService.getSysSetting();
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("update phone numbers sys-setting(DONE)")
    @PutMapping("/phone-numbers")
    public MedicResponse<SysSettingDTO> updateSysSetting(@RequestBody SysSettingReqDTO sysSettingReqDTO) {
        SysSettingDTO result = sysSettingService.updateSysSetting(sysSettingReqDTO);
        if (null == result) {
            throw new MedicException(MedicException.ERROR_SYS_SETTING_INVALID, "id sys-setting invalid : ");
        }
        return MedicResponse.okStatus(result);
    }

    /**
     * Get travel prices settings.
     *
     * @return travel prices list
     */
    @ApiOperation("Get travel prices(DONE)")
    @GetMapping("/travel-prices")
    public MedicResponse<List<TravelPriceSettingDTO>> getTravelPriceSetting() {
        List<TravelPriceSettingDTO> result = sysSettingService.getTravelPriceSetting();
        return MedicResponse.okStatus(result);
    }

    /**
     * Create travel prices setting.
     *
     * @param reqDto travel price
     * @return travel price created
     */
    @ApiOperation("Create travel prices(DONE)")
    @PostMapping("/travel-prices")
    public MedicResponse<TravelPriceSettingDTO> createTravelPriceSetting(@RequestBody TravelPriceSettingDTO reqDto) {
        TravelPriceSettingDTO result = sysSettingService.createTravelPrice(reqDto);
        return MedicResponse.okStatus(result);
    }


    /**
     * Update travel prices settings.
     *
     * @param id     travel price
     * @param reqDto travel price
     * @return travel price updated
     */
    @ApiOperation("Update travel prices(DONE)")
    @PutMapping("/travel-prices/{id}")
    public MedicResponse<TravelPriceSettingDTO> updateTravelPriceSetting(@PathVariable("id") Integer id,
                                                                         @RequestBody TravelPriceSettingDTO reqDto) {
        TravelPriceSettingDTO result = sysSettingService.updateTravelPrice(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    /**
     * Delete travel prices.
     *
     * @param id travel price
     * @return 'OK if success'
     */
    @ApiOperation("Delete travel prices(DONE)")
    @DeleteMapping("/travel-prices/{id}")
    public MedicResponse<String> deleteTravelPriceSetting(@PathVariable("id") Integer id) {
        return MedicResponse.okStatus(sysSettingService.deleteTravelPrice(id));
    }

    /**
     * Update language for account.
     *
     * @param language language
     * @return true if updated successfully
     */
    @ApiOperation("Language (DONE)")
    @PutMapping("/language")
    public MedicResponse<Boolean> updateLanguage(@ApiParam("Language") @RequestBody String language) {
        return MedicResponse.okStatus(accountService.updateLanguage(baseService.getCurrentAccountLogin(), language));
    }

    /**
     * Get language of account.
     *
     * @return language
     */
    @ApiOperation("Language (DONE)")
    @GetMapping("/language")
    public MedicResponse<String> getLanguage() {
        Account currentAccount = baseService.getCurrentAccountLogin();
        String language = Constants.EMPTY_STRING;
        if (null != currentAccount) {
            language = Helper.convertNullToEmpty(currentAccount.getLanguage());
        }
        return MedicResponse.okStatus(language);
    }
}
