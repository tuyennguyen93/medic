package com.mmedic.rest.sysSetting;

import com.mmedic.entity.SysSetting;
import com.mmedic.entity.TravelPricePolicy;
import com.mmedic.rest.sysSetting.dto.SysSettingDTO;
import com.mmedic.rest.sysSetting.dto.SysSettingReqDTO;
import com.mmedic.rest.sysSetting.dto.TravelPriceSettingDTO;
import com.mmedic.rest.sysSetting.mapper.SysSettingMapper;
import com.mmedic.rest.sysSetting.mapper.TravelPricePolicyMapper;
import com.mmedic.rest.sysSetting.repo.SysSettingRepository;
import com.mmedic.rest.travelprice.repo.TravelPricePolicyRepository;
import com.mmedic.spring.errors.MedicException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * System Setting service.
 */
@Service
@RequiredArgsConstructor
public class SysSettingService {

    private final SysSettingRepository sysSettingRepository;

    private final TravelPricePolicyRepository travelPricePolicyRepository;

    private final SysSettingMapper sysSettingMapper;

    private final TravelPricePolicyMapper travelPricePolicyMapper;


    public List<SysSettingDTO> getSysSetting() {
        List<SysSettingDTO> listDto = sysSettingMapper.toListDTO(sysSettingRepository.findAll());

        return listDto;
    }

    public SysSettingDTO updateSysSetting(SysSettingReqDTO sysSettingReqDTO) {
        Optional<SysSetting> data = sysSettingRepository.findByKey(sysSettingReqDTO.getKey());
        if (data.isPresent()) {
            SysSetting savedData = data.get();
            savedData.setKey(sysSettingReqDTO.getKey());
            savedData.setValue(sysSettingReqDTO.getValue());
            SysSetting save = sysSettingRepository.save(savedData);
            return sysSettingMapper.convertToDTO(save);
        } else {
            return null;
        }
    }

    /**
     * Get travel price settings.
     *
     * @return travel price list
     */
    public List<TravelPriceSettingDTO> getTravelPriceSetting() {
        return travelPricePolicyRepository.findAllTravelPriceDTO();
    }

    /**
     * Create travel price.
     *
     * @param reqDto travel price
     * @return travel price created
     */
    public TravelPriceSettingDTO createTravelPrice(TravelPriceSettingDTO reqDto) {
        TravelPricePolicy travelPricePolicy = travelPricePolicyMapper.convertToEntity(reqDto);
        Integer order = travelPricePolicyRepository.findCurrentMaxOrder();
        if (order  == null) order = 1;
        travelPricePolicy.setOrder(order);
        TravelPricePolicy saved = travelPricePolicyRepository.save(travelPricePolicy);
        return travelPricePolicyMapper.convertToDTO(saved);
    }

    /**
     * Update travel price.
     *
     * @param id     travel price
     * @param reqDto travel price
     * @return travel price updated
     */
    public TravelPriceSettingDTO updateTravelPrice(Integer id, TravelPriceSettingDTO reqDto) {
        TravelPricePolicy travelPricePolicy = travelPricePolicyRepository.findById(id).orElseThrow(() ->
                new MedicException(MedicException.ERROR_RECORD_NOT_FOUND, "Can not find travel price with id = " + id));
        travelPricePolicy.setAddedKm(reqDto.getAddedKm());
        travelPricePolicy.setPricePerKm(reqDto.getPricePerKm());
        TravelPricePolicy saved = travelPricePolicyRepository.save(travelPricePolicy);
        return travelPricePolicyMapper.convertToDTO(saved);
    }

    /**
     * Delete travel price.
     *
     * @param id travel price
     * @return 'OK' if success
     */
    public String deleteTravelPrice(Integer id) {
        travelPricePolicyRepository.deleteById(id);
        return "OK";
    }
}
