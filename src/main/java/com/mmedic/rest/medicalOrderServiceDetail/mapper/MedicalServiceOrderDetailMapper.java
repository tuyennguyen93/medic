package com.mmedic.rest.medicalOrderServiceDetail.mapper;

import com.mmedic.entity.*;
import com.mmedic.rest.medicalOrderServiceDetail.dto.MedicalServiceOrderDetailDTO;
import com.mmedic.rest.patient.PatientService;
import com.mmedic.rest.patient.dto.PatientDTO;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class MedicalServiceOrderDetailMapper {

    private final PatientService patientService;

    public List<MedicalServiceOrderDetailDTO> convertListModelToDTO(List<MedicalServiceOrderDetail> medicalServiceOrderDetails) {
        List<MedicalServiceOrderDetailDTO> dtos = new ArrayList<>();
        for (MedicalServiceOrderDetail model : medicalServiceOrderDetails) {
            MedicalServiceOrderDetailDTO dto = this.convertModelToDTO(model);
            dtos.add(dto);
        }
        return dtos;
    }

    public MedicalServiceOrderDetailDTO convertModelToDTO(MedicalServiceOrderDetail model) {
        MedicalServiceOrderDetailDTO dto = new MedicalServiceOrderDetailDTO();
        dto.setId(model.getId());
        Map<String, Integer> mapSequence = Helper.getSequenceNumber(model.getSequenceNumber());
        if (!CollectionUtils.isEmpty(mapSequence) && mapSequence.get(Constants.SEQUENCE_NUMBER) > 0) {
            dto.setSequenceNumber(mapSequence.get(Constants.SEQUENCE_NUMBER));
        }
        dto.setMedicalRecordId(model.getMedicalServiceOrder().getPreclinicalRecord().getMedicalRecord().getId());
        dto.setServiceName(model.getMedicalService().getName());
        if (null != model.getProccessDate()) {
            dto.setTestDate(model.getProccessDate());
        } else {
            dto.setTestDate(model.getAppointmentDate());
        }
        dto.setResultDate(model.getResultDate());
        //dto.setResult(model.getResult());

        List<String> fileList = new ArrayList<>();

        String string =  model.getResult();
        if(string != null) {
            String[] link = string.split(Constants.SEPARATE_PRECLINICAL_RESULT_LINK);
            for (String item : link) {
                fileList.add(item);
            }
            dto.setResult(fileList);
        }
        dto.setStatus(model.getStatus());


        PatientDTO patientDTO = patientService.getPatientInfo(model.getMedicalServiceOrder().getPreclinicalRecord().getMedicalRecord().getPatient().getId());
        dto.setPatientInfo(patientDTO);
        return dto;
    }
}
