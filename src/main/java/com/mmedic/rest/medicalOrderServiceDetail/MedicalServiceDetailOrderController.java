package com.mmedic.rest.medicalOrderServiceDetail;

import com.mmedic.entity.Establishment;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.establishment.EstablishmentService;
import com.mmedic.rest.medicalOrderServiceDetail.dto.MedicalServiceOrderDetailDTO;
import com.mmedic.spring.errors.MedicException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Medical Order Detail Controller.
 *
 * @author quangtkMedicalServiceDetailOrderController
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/services-order-detail")
@Api(value = "Medical Service Order Detail Controller", description = "Medical Service Order Detail Management")
@RequiredArgsConstructor
public class MedicalServiceDetailOrderController {

    private final EstablishmentService establishmentService;

    private final MedicalOrderDetailService medicalOrderDetailService;

    private final BaseService baseService;

    @ApiOperation("Get all tests today (DONE)")
    @GetMapping("/today-tests")
    public MedicResponse<List<MedicalServiceOrderDetailDTO>> getTodayTests(Principal principal) {

        Establishment establishment = establishmentService.getRawEstablishment(principal);
        List<MedicalServiceOrderDetailDTO> medicalServiceOrderDetailDTOS = medicalOrderDetailService.findAllTestToday(establishment.getId());

        return MedicResponse.okStatus(medicalServiceOrderDetailDTOS);
    }

    @ApiOperation("Get all tests (DONE)")
    @GetMapping("/all")
    public MedicResponse<Page<MedicalServiceOrderDetailDTO>> getAllTests(Principal principal,
                                                                         @ApiParam("Paging Object") Pageable pageable,
                                                                         @RequestParam(required = false) @ApiParam("Medical Service Id") Integer serviceId) {

        serviceId = serviceId == null ? -1 : serviceId;
        Establishment establishment = establishmentService.getRawEstablishment(principal);

        Page<MedicalServiceOrderDetailDTO> medicalServiceOrderDetailDTOPage =
                medicalOrderDetailService.findAllTest(pageable, establishment.getId(), serviceId);

        return MedicResponse.okStatus(medicalServiceOrderDetailDTOPage);
    }

    @ApiOperation("Get test detail by id (DONE)")
    @GetMapping("/{id}")
    public MedicResponse<MedicalServiceOrderDetailDTO> getServiceOrderDetailById(@PathVariable("id") @ApiParam("Service Order Detail Id") Integer orderDetailId) {

        MedicalServiceOrderDetailDTO medicalServiceOrderDetailDTO = medicalOrderDetailService.findById(orderDetailId);
        if (null == medicalServiceOrderDetailDTO) {
            throw new MedicException(MedicException.ERROR_MEDICAL_SERVICE_ORDER_DETAIL_NOT_FOUND, "Medical service order detail not found : " + orderDetailId);
        }

        return MedicResponse.okStatus(medicalServiceOrderDetailDTO);
    }

    @ApiOperation("Push notification transport time for preclinical (DONE)")
    @PostMapping("/{id}/transport-notification")
    public MedicResponse<Boolean> notificationPreclinicalTransportTime(@PathVariable("id") @ApiParam("Service Order Detail Id") int orderDetailId) {
        return MedicResponse.okStatus(medicalOrderDetailService.notificationPreclinicalTransport(orderDetailId, baseService.getCurrentEstablishment()));
    }

    @ApiOperation("Denied preclinical order (DONE)")
    @DeleteMapping("/{id}")
    public MedicResponse<Boolean> deniedPreclinicalOrder(@PathVariable("id") int serviceOrderDetailId) {
        return MedicResponse.okStatus(medicalOrderDetailService.
                deniedPreclinicalOrder(serviceOrderDetailId, baseService.getCurrentEstablishment()));
    }

    @ApiOperation("Update test result time (DONE)")
    @PutMapping("/set-result-time/{id}")
    public MedicResponse<MedicalServiceOrderDetailDTO> setResultTime(@PathVariable("id") Integer id,
                                                                     @RequestBody MedicalServiceOrderDetailDTO reqDto) {

        MedicalServiceOrderDetailDTO result = medicalOrderDetailService.setResultTime(id, reqDto, baseService.getCurrentAccountLogin());
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Update test result image (DONE)")
    @PutMapping("/set-result-image/{id}")
    public MedicResponse<MedicalServiceOrderDetailDTO> setResultImage(@PathVariable("id") Integer id,
                                                                      @RequestBody MedicalServiceOrderDetailDTO reqDto) {

        MedicalServiceOrderDetailDTO result = medicalOrderDetailService.setResultImage(id, reqDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get test histories by patient (DONE)")
    @GetMapping("/test-history")
    public MedicResponse<Page<MedicalServiceOrderDetailDTO>> getTestHistory(Principal principal,
                                                                            @ApiParam("Paging Object") Pageable pageable,
                                                                            @RequestParam @ApiParam("Patient Id") Integer patientId,
                                                                            @RequestParam(required = false) @ApiParam("Medical Service Id") Integer serviceId) {

        serviceId = serviceId == null ? -1 : serviceId;
        Establishment establishment = establishmentService.getRawEstablishment(principal);

        Page<MedicalServiceOrderDetailDTO> testHistoryByPatientPage =
                medicalOrderDetailService.findTestHistoryByPatient(pageable, patientId, establishment.getId(), serviceId);

        return MedicResponse.okStatus(testHistoryByPatientPage);
    }
}
