package com.mmedic.rest.medicalOrderServiceDetail.dto;

import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PreclinicalTestType;
import com.mmedic.rest.patient.dto.PatientDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Medical Order Detail DTO.
 *
 * @author quangtk
 */
@Data
@NoArgsConstructor
public class MedicalServiceOrderDetailDTO {

    @ApiModelProperty("Service Order Detail Id")
    private Integer id;

    @ApiModelProperty("Sequence Number")
    private Integer sequenceNumber;

    @ApiModelProperty("Patient Information")
    private PatientDTO patientInfo;

    @ApiModelProperty("Medical Record Id")
    private Integer medicalRecordId;

    @ApiModelProperty("Service Name")
    private String serviceName;

    @ApiModelProperty("Status")
    private MedicalServiceOrderStatus status;

    @ApiModelProperty("Test Date")
    private LocalDateTime testDate;

    @ApiModelProperty("Result Date")
    private LocalDateTime resultDate;

    //@ApiModelProperty("Result File")
    // private String result;

    @ApiModelProperty("Result all File")
    private List<String> result;

    @ApiModelProperty("Preclinical Test Type")
    private PreclinicalTestType preclinicalTestType;

    private boolean isNotificationTransport;
}
