package com.mmedic.rest.medicalOrderServiceDetail;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PreclinicalTestType;
import com.mmedic.rest.medicalOrderServiceDetail.dto.MedicalServiceOrderDetailDTO;
import com.mmedic.rest.medicalOrderServiceDetail.mapper.MedicalServiceOrderDetailMapper;
import com.mmedic.rest.medicalOrderServiceDetail.repo.MedicalOrderDetailRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Medical Order Detail Service.
 *
 * @author quangtk
 */
@Service
@RequiredArgsConstructor
public class MedicalOrderDetailService {

    private final MedicalOrderDetailRepository medicalOrderDetailRepository;

    private final MedicalServiceOrderDetailMapper mapper;

    private final NotificationService notificationService;

    /**
     * Find all today test of preclinical.
     *
     * @param establishmentId establishmentId
     * @return list of MedicalServiceOrderDetailDTO
     */
    public List<MedicalServiceOrderDetailDTO> findAllTestToday(Integer establishmentId) {
        List<MedicalServiceOrderDetail> medicalServiceOrderDetails = medicalOrderDetailRepository.findAllTestForToday(establishmentId);

        List<MedicalServiceOrderDetail> orderDetails = medicalServiceOrderDetails.stream().
                filter(Helper::checkPreclinicalOrderDetailIsService).
                collect(Collectors.toList());
        List<MedicalServiceOrderDetailDTO> medicalServiceOrderDetailDTOS = mapper.convertListModelToDTO(orderDetails);

        if (!CollectionUtils.isEmpty(medicalServiceOrderDetails) && !CollectionUtils.isEmpty(medicalServiceOrderDetailDTOS)) {
            Map<Integer, MedicalServiceOrderDetail> mapTransportService = medicalServiceOrderDetails.stream().
                    filter(Helper::checkPreclinicalOrderDetailIsNotService).
                    collect(Collectors.toMap(MedicalServiceOrderDetail::getParentId, orderDetail -> orderDetail));

            medicalServiceOrderDetailDTOS.forEach(orderDetailDTO -> {
                MedicalServiceOrderDetail transport = mapTransportService.get(orderDetailDTO.getId());
                if (null != transport) {
                    if (Constants.SERVICE_TEST_HOME == transport.getMedicalService().getId()) {
                        orderDetailDTO.setPreclinicalTestType(PreclinicalTestType.HOME);
                    } else if (Constants.SERVICE_TEST_CLINIC == transport.getMedicalService().getId()) {
                        orderDetailDTO.setPreclinicalTestType(PreclinicalTestType.CLINIC);
                    }
                } else {
                    orderDetailDTO.setPreclinicalTestType(PreclinicalTestType.PRECLINICAL);
                }
            });
        }
        return medicalServiceOrderDetailDTOS;
    }

    /**
     * Find all test of preclinical.
     *
     * @param establishmentId establishmentId
     * @return list of MedicalServiceOrderDetailDTO
     */
    public Page<MedicalServiceOrderDetailDTO> findAllTest(Pageable pageable, Integer establishmentId, Integer serviceId) {

        Page<MedicalServiceOrderDetail> medicalServiceOrderDetailPage
                = medicalOrderDetailRepository.findAllTest(pageable, establishmentId, serviceId);
        return medicalServiceOrderDetailPage.map(mapper::convertModelToDTO);
    }

    /**
     * Find Medical Service Order Detail by Id
     *
     * @param id Medical Service Order Detail Id
     * @return Medical Service Order Detail object
     */
    public MedicalServiceOrderDetailDTO findById(Integer id) {
        MedicalServiceOrderDetailDTO result = null;

        Optional<MedicalServiceOrderDetail> medicalServiceOrderDetail = medicalOrderDetailRepository.findById(id);
        result = medicalServiceOrderDetail.map(mapper::convertModelToDTO).orElse(null);

        if (null != result) {
            Optional<MedicalServiceOrderDetail> transportService = medicalOrderDetailRepository.findByParentId(id);
            if (transportService.isPresent()) {
                if (Helper.checkPreclinicalOrderDetailIsNotService(transportService.get())) {
                    if (transportService.get().getMedicalService().getId() == Constants.SERVICE_TEST_HOME) {
                        result.setPreclinicalTestType(PreclinicalTestType.HOME);
                    } else if (transportService.get().getMedicalService().getId() == Constants.SERVICE_TEST_CLINIC) {
                        result.setPreclinicalTestType(PreclinicalTestType.CLINIC);
                    }
                }
                result.setNotificationTransport(Helper.isNotificationTransport(transportService.get()));
            } else {
                result.setPreclinicalTestType(PreclinicalTestType.PRECLINICAL);
            }
        }

        return result;
    }

    /**
     * Update Result Time.
     */
    @Transactional
    public MedicalServiceOrderDetailDTO setResultTime(Integer id, MedicalServiceOrderDetailDTO dto, Account currentAccount) {

        MedicalServiceOrderDetail medicalServiceOrderDetail = medicalOrderDetailRepository.findById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                        "Medical service order detail not found : " + id));
        if (dto.getResultDate() == null || dto.getResultDate().isBefore(LocalDateTime.now())) {
            throw new MedicException(MedicException.ERROR_RESULT_TIME_BE_EARLY, "Result Time must be after current time");
        }
        medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_PROCESSING);
        medicalServiceOrderDetail.setResultDate(dto.getResultDate());
        medicalServiceOrderDetail.setHandleBy(currentAccount);
        medicalServiceOrderDetail.setProccessDate(LocalDateTime.now());
        medicalServiceOrderDetail = medicalOrderDetailRepository.save(medicalServiceOrderDetail);

        // Change status of transport service
        Optional<MedicalServiceOrderDetail> transportService = medicalOrderDetailRepository.findByParentId(id);
        transportService.ifPresent(transportOrder -> {
            transportOrder.setStatus(MedicalServiceOrderStatus.REQUEST_FULFILLED);
            medicalOrderDetailRepository.save(transportOrder);
        });

        // Notification
        notificationService.notificationPreclinicalStatus(medicalServiceOrderDetail);
        return mapper.convertModelToDTO(medicalServiceOrderDetail);
    }

    /**
     * Update Result Image.
     */
    @Transactional
    public MedicalServiceOrderDetailDTO setResultImage(Integer id, MedicalServiceOrderDetailDTO dto) {

        medicalOrderDetailRepository.findById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_RECORD_NOT_FOUND,
                        "Medical service order detail not found : " + id));
        if (dto.getResult().isEmpty()) {
            throw new MedicException(MedicException.ERROR_RESULT_IMAGE_BE_NULL, "Result Image must be not null");
        }
        MedicalServiceOrderDetail medicalServiceOrderDetail = medicalOrderDetailRepository.findById(id).get();
        medicalServiceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_FULFILLED);
        //medicalServiceOrderDetail.setResult(dto.getResult());

        List<String> urlList = dto.getResult();
        String urlString = "";
        for (int i = 0; i < urlList.size(); i++) {
            if (i != 0)
                urlString = String.format("%s%s%s", urlString, Constants.SEPARATE_PRECLINICAL_RESULT_LINK, urlList.get(i));
            else urlString = urlList.get(i);

        }
        medicalServiceOrderDetail.setResult(urlString);
        medicalServiceOrderDetail.setResultDate(LocalDateTime.now());
        medicalServiceOrderDetail = medicalOrderDetailRepository.save(medicalServiceOrderDetail);

        // Notification
        notificationService.notificationPreclinicalStatus(medicalServiceOrderDetail);
        notificationService.notificationPreclinicalResult(medicalServiceOrderDetail);
        return mapper.convertModelToDTO(medicalServiceOrderDetail);
    }

    /**
     * Find test history by patient.
     *
     * @param pageable
     * @param patientId       patient id
     * @param establishmentId establishment Id
     * @param serviceId       service id
     * @return page of MedicalServiceOrderDetailDTO object
     */
    public Page<MedicalServiceOrderDetailDTO> findTestHistoryByPatient(Pageable pageable, Integer patientId, Integer establishmentId, Integer serviceId) {

        Page<MedicalServiceOrderDetail> medicalServiceOrderDetailPage
                = medicalOrderDetailRepository.findTestHistoryByPatient(pageable, patientId, establishmentId, serviceId);
        return medicalServiceOrderDetailPage.map(mapper::convertModelToDTO);
    }

    /**
     * Denied preclinical order detail.
     *
     * @param serviceOrderDetailId service order detail id
     * @param currentEstablishment current establishment
     * @return true if update status successfully
     */
    @Transactional
    public boolean deniedPreclinicalOrder(int serviceOrderDetailId, Establishment currentEstablishment) {
        MedicalServiceOrderDetail serviceOrderDetail = medicalOrderDetailRepository.findByIdAndEstablishment(serviceOrderDetailId, currentEstablishment).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "Order detail don't exist"));
        if (MedicalServiceOrderStatus.REQUEST_ACCEPTED.equals(serviceOrderDetail.getStatus())) {
            serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_DENIED);
            medicalOrderDetailRepository.save(serviceOrderDetail);
            return true;
        }
        return false;
    }

    /**
     * Push notification to notify patient about time to staff need to go location.
     *
     * @param orderDetailId        order detail id
     * @param currentEstablishment current establishment
     * @return true if push successfully
     */
    @Transactional
    public boolean notificationPreclinicalTransport(int orderDetailId, Establishment currentEstablishment) {
        boolean result = false;
        MedicalServiceOrderDetail serviceOrderDetail = medicalOrderDetailRepository.findByIdAndEstablishment(orderDetailId, currentEstablishment).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ORDER_DETAIL_NOT_EXIST, "Order detail don't exist"));

        List<MedicalServiceOrderDetail> serviceOrderDetails = serviceOrderDetail.getMedicalServiceOrder().getMedicalServiceOrderDetails();
        MedicalServiceOrderDetail transportService = serviceOrderDetails.stream().
                filter(orderDetail -> null != orderDetail.getParentId() && orderDetail.getParentId() == serviceOrderDetail.getId()).
                findFirst().
                orElse(null);

        MedicalRecord medicalRecord = serviceOrderDetail.getMedicalServiceOrder().getPreclinicalRecord().getMedicalRecord();
        if (null != medicalRecord &&
                null != transportService &&
                MedicalServiceOrderStatus.REQUEST_ACCEPTED.equals(transportService.getStatus())) {
            transportService.setStatus(MedicalServiceOrderStatus.REQUEST_PROCESSING);
            medicalOrderDetailRepository.save(transportService);
            String serviceName = serviceOrderDetail.getMedicalService().getName();
            Account patient = medicalRecord.getPatient().getAccount();
            notificationService.notificationPreclinicalTransport(serviceName, patient, medicalRecord.getId());
            result = true;
        }
        return result;
    }
}
