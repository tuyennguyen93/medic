package com.mmedic.rest.medicalOrderServiceDetail.repo;

import com.mmedic.entity.Establishment;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.rest.transactionHistory.dto.EstablishmentTransactionHistoryDTO;
import com.mmedic.rest.transactionHistory.dto.TransactionStatisticDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Medical Order Detail Repository.
 *
 * @author quangtk
 */
public interface MedicalOrderDetailRepository extends JpaRepository<MedicalServiceOrderDetail, Integer> {

    @Query("SELECT orderDetail " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.medicalService medicalService " +
            "INNER JOIN medicalService.medicalServiceGroup medicalServiceGroup " +
            "INNER JOIN orderDetail.medicalServiceOrder medicalServiceOrder " +
            "INNER JOIN medicalServiceOrder.preclinicalRecord preclinicalRecord " +
            "WHERE (orderDetail.establishment.id = :establishmentId) " +
            "AND medicalServiceOrder.paymentStatus = 'SUCCESS' " +
            "AND ( " +
            "   orderDetail.status IN ('REQUEST_ACCEPTED', 'REQUEST_PROCESSING') " +
            "   OR ( " +
            "      orderDetail.status = 'REQUEST_FULFILLED' " +
            "      AND date(orderDetail.resultDate) = CURRENT_DATE " +
            "   ) " +
            ")" +
            "AND medicalServiceGroup.id IN (2,3)" +
            "ORDER BY orderDetail.resultDate, orderDetail.proccessDate, orderDetail.appointmentDate ASC")
    List<MedicalServiceOrderDetail> findAllTestForToday(@Param("establishmentId") Integer establishmentId);

    @Query("SELECT orderDetail " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.medicalService medicalService " +
            "INNER JOIN medicalService.medicalServiceGroup medicalServiceGroup " +
            "INNER JOIN orderDetail.medicalServiceOrder medicalServiceOrder " +
            "INNER JOIN medicalServiceOrder.preclinicalRecord preclinicalRecord " +
            "WHERE (orderDetail.establishment.id = :establishmentId) " +
            "AND medicalServiceOrder.paymentStatus = 'SUCCESS' " +
            "AND (:serviceId = -1 OR medicalService.id = :serviceId)" +
            "AND medicalServiceGroup.id IN (2,3)" +
            "AND medicalService.id NOT IN (5, 6)")
    Page<MedicalServiceOrderDetail> findAllTest(Pageable pageable, @Param("establishmentId") Integer establishmentId,
                                                @Param("serviceId") Integer serviceId);


    @Query("SELECT orderDetail " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.medicalService medicalService " +
            "WHERE (orderDetail.establishment.id = :establishmentId) " +
            "AND (:medicalServiceId = -1 OR medicalService.id = :medicalServiceId)" +
            "AND orderDetail.medicalServiceOrder.paymentDate BETWEEN :from AND :to ORDER BY orderDetail.id DESC")
    Page<MedicalServiceOrderDetail> findByEstablishmentBetween(Pageable pageable,
                                                               @Param("medicalServiceId") int medicalServiceId,
                                                               @Param("establishmentId") int establishmentId, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    @Query("SELECT  new com.mmedic.rest.transactionHistory.dto.EstablishmentTransactionHistoryDTO(" +
            "orderDetail.establishment.id as establishmentId," +
            "orderDetail.establishment.name as establishmentName," +
            "COUNT(orderDetail.establishment.id) as totalTransaction," +
            "SUM(orderDetail.txAmount) as totalAmount) " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.medicalService medicalService " +
            "WHERE (orderDetail.establishment.name LIKE %:establishmentName%) " +
            "AND (:medicalServicedId = -1 OR medicalService.id = :medicalServicedId) " +
            "AND MONTH(orderDetail.medicalServiceOrder.paymentDate)=:month " +
            "AND YEAR(orderDetail.medicalServiceOrder.paymentDate)=:year " +
            "GROUP BY orderDetail.establishment.id"
    )
    Page<EstablishmentTransactionHistoryDTO> getTransHistoryByAdmin(Pageable pageable,
                                                                    @Param("establishmentName") String establishmentName,
                                                                    @Param("medicalServicedId") Integer medicalServicedId,
                                                                    @Param("month") Integer month,
                                                                    @Param("year") Integer year);

    @Query("SELECT orderDetail " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.medicalService medicalService " +
            "WHERE (orderDetail.establishment.id = :establishmentId)" +
            "AND (:medicalServiceId = -1 OR medicalService.id = :medicalServiceId) " +
            "AND MONTH(orderDetail.medicalServiceOrder.paymentDate)=:month " +
            "AND YEAR(orderDetail.medicalServiceOrder.paymentDate)=:year")
    Page<MedicalServiceOrderDetail> getTransHistoryByEstablishment(Pageable pageable,
                                                                   @Param("establishmentId") Integer establishmentId,
                                                                   @Param("medicalServiceId") Integer medicalServiceId,
                                                                   @Param("month") Integer month,
                                                                   @Param("year") Integer year);

    @Query("SELECT new com.mmedic.rest.transactionHistory.dto.TransactionStatisticDTO(" +
            "count(o.id) AS totalTransaction, " +
            "sum(o.txAmount) AS totalAmount)  " +
            "FROM MedicalServiceOrderDetail o " +
            "LEFT JOIN o.medicalService m " +
            "WHERE (:establishmentName = '' OR o.establishment.name LIKE %:establishmentName%) " +
            "AND (:medicalServiceId = -1 OR o.medicalService.id = :medicalServiceId) " +
            "AND (month(o.medicalServiceOrder.paymentDate) = :month) " +
            "AND (year(o.medicalServiceOrder.paymentDate) = :year) " +
            "AND (o.txAmount > 0) ")
    TransactionStatisticDTO getStatisticByAdmin(@Param("establishmentName") String establishmentName,
                                                @Param("medicalServiceId") Integer medicalServiceId,
                                                @Param("month") Integer month,
                                                @Param("year") Integer year);

    @Query("SELECT new com.mmedic.rest.transactionHistory.dto.TransactionStatisticDTO(" +
            "count(o.id) AS totalTransaction, " +
            "sum(o.txAmount) AS totalAmount)  " +
            "FROM MedicalServiceOrderDetail o " +
            "LEFT JOIN o.medicalService m " +
            "WHERE (o.establishment.id = :establishmentId) " +
            "AND (:medicalServiceId = -1 OR o.medicalService.id = :medicalServiceId) " +
            "AND (month(o.medicalServiceOrder.paymentDate) = :month) " +
            "AND (year(o.medicalServiceOrder.paymentDate) = :year) " +
            "AND (o.txAmount > 0) ")
    TransactionStatisticDTO getStatisticByEstablishment(@Param("establishmentId") Integer establishmentId,
                                                        @Param("medicalServiceId") Integer medicalServiceId,
                                                        @Param("month") Integer month,
                                                        @Param("year") Integer year);

    @Query("SELECT orderDetail " +
            "FROM MedicalServiceOrderDetail orderDetail " +
            "INNER JOIN orderDetail.medicalService medicalService " +
            "INNER JOIN medicalService.medicalServiceGroup medicalServiceGroup " +
            "INNER JOIN orderDetail.medicalServiceOrder medicalServiceOrder " +
            "INNER JOIN medicalServiceOrder.preclinicalRecord preclinicalRecord " +
            "WHERE (orderDetail.establishment.id = :establishmentId) " +
            "AND (preclinicalRecord.medicalRecord.patient.id = :patientId)" +
            "AND (:serviceId = -1 OR medicalService.id = :serviceId)" +
            "AND medicalServiceGroup.id IN (2,3)")
    Page<MedicalServiceOrderDetail> findTestHistoryByPatient(Pageable pageable,
                                                             @Param("patientId") Integer patientId,
                                                             @Param("establishmentId") Integer establishmentId,
                                                             @Param("serviceId") Integer serviceId);

    /**
     * Find service order detail by id and establishment.
     *
     * @param serviceOrderDetailId service order detail id
     * @param establishment        establishment
     * @return medical service order detail
     */
    Optional<MedicalServiceOrderDetail> findByIdAndEstablishment(int serviceOrderDetailId, Establishment establishment);

    /**
     * Find service order detail by parent id.
     *
     * @param parentId parent id
     * @return medical service order detail
     */
    Optional<MedicalServiceOrderDetail> findByParentId(int parentId);
}
