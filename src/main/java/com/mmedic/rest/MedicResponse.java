package com.mmedic.rest;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

public class MedicResponse<T> extends ResponseEntity {

    public MedicResponse(HttpStatus status) {
        this(MedicBody.builder().status(true).build(), null, status);
    }

    public MedicResponse(T body, HttpStatus status) {
        this(MedicBody.builder().status(true).data(body).build(), null, status);
    }

    public MedicResponse(boolean status, T body) {
        this(MedicBody.builder().status(status).data(body).build(), null, HttpStatus.OK);
    }

    public MedicResponse(boolean status, T body, HttpStatus httpStatus) {
        this(MedicBody.builder().status(status).data(body).build(), null, httpStatus);
    }

    public MedicResponse(boolean status, T body, MultiValueMap<String, String> headers, HttpStatus httpStatus) {
        this(MedicBody.builder().status(status).data(body).build(), headers, httpStatus);
    }

    public MedicResponse(MedicBody body, MultiValueMap<String, String> headers, HttpStatus httpStatus) {
        super(body, headers, httpStatus);
    }

    public static <T> MedicResponse<T> okStatus(T body) {
        return new MedicResponse<T>(body, HttpStatus.OK);
    }

    public static <T> MedicResponse<T> errorStatus(T body) {
        return new MedicResponse<T>(false, body, HttpStatus.OK);
    }

    @Getter
    @Setter
    @Builder
    public static class MedicBody<T> {
        boolean status;
        T data;
    }
}
