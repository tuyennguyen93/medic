package com.mmedic.rest.certification.mapper;

import com.mmedic.entity.EstablishmentCertification;
import com.mmedic.rest.certification.dto.CertificationDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Certification mapper.
 */
@Mapper(componentModel = "spring")
public interface CertificationMapper {

    /**
     * Convert certification entity to dto.
     *
     * @param entity certification entity
     * @return certification dto
     */
    CertificationDTO convertToDTO(EstablishmentCertification entity);

    /**
     * Convert certification dto to entity.
     *
     * @param to certification dto
     * @return certification entity
     */
    EstablishmentCertification convertToEntity(CertificationDTO to);

    /**
     * Convert list certification entity to list dto.
     *
     * @param establishmentCertifications list certification entity
     * @return list certification dto
     */
    List<CertificationDTO> toListCertificationDtos(List<EstablishmentCertification> establishmentCertifications);
}
