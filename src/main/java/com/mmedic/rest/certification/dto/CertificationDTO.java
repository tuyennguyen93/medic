package com.mmedic.rest.certification.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CertificationDTO {

    private Integer id;

    private String name;

    private String certificationUrl;

    private Boolean isLicence;

}
