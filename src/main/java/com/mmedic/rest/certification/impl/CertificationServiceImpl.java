package com.mmedic.rest.certification.impl;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.EstablishmentCertification;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.certification.dto.CertificationDTO;
import com.mmedic.rest.certification.mapper.CertificationMapper;
import com.mmedic.rest.certification.repo.CertificationRepository;
import com.mmedic.rest.certification.CertificationService;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.spring.errors.MedicException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Certification service.
 */
@RequiredArgsConstructor
@Service
public class CertificationServiceImpl implements CertificationService {

    private final AccountService accountService;

    private final EstablishmentRepository establishmentRepository;

    private final CertificationMapper certificationMapper;

    private final CertificationRepository certificationRepository;

    @Override
    public List<CertificationDTO> getCertifications(Principal principal) {
        Establishment establishment = getRawEstablishment(principal);
        return convertEstablishmentToCertificationDTO(establishment);
    }

    @Transactional
    @Override
    public CertificationDTO createCertification(Principal principal, CertificationDTO reqDto) {
        Establishment establishment = getRawEstablishment(principal);
        EstablishmentCertification establishmentCertification = certificationMapper.convertToEntity(reqDto);
        establishmentCertification.setEstablishment(establishment);
        establishmentCertification.setId(0);
        EstablishmentCertification rs = certificationRepository.save(establishmentCertification);
        if (rs == null) {
            return null;
        }
        reqDto.setId(rs.getId());
        return reqDto;
    }

    @Transactional
    @Override
    public CertificationDTO updateCertification(Principal principal, int id, CertificationDTO reqDto) {
        Establishment establishment = getRawEstablishment(principal);
        EstablishmentCertification establishmentCertification = certificationMapper.convertToEntity(reqDto);
        establishmentCertification.setEstablishment(establishment);
        Optional<EstablishmentCertification> getOldData = certificationRepository.findByIdAndEstablishment_Id(id, establishment.getId());
        if (getOldData.isPresent()) {
            establishmentCertification.setCreateAt(getOldData.get().getCreateAt());
            establishmentCertification.setId(id);
            EstablishmentCertification rs = certificationRepository.save(establishmentCertification);

            return certificationMapper.convertToDTO(rs);
        }
        throw new MedicException(MedicException.ERROR_CERTIFICATION_OR_LICENCE_NOT_EXIST, "Certification or licence is not exists.");
    }

    @Transactional
    @Override
    public CertificationDTO deleteCertification(Principal principal, int id) {
        Establishment establishment = getRawEstablishment(principal);
        Optional<EstablishmentCertification> getOldData = certificationRepository.findByIdAndEstablishment_Id(id, establishment.getId());
        if (getOldData.isPresent()) {
            certificationRepository.deleteById(id);
            return certificationMapper.convertToDTO(getOldData.get());
        } else
            throw new MedicException(MedicException.ERROR_CERTIFICATION_OR_LICENCE_NOT_EXIST, "Certification or licence is not exists.");
    }

    /**
     * Get establishment.
     *
     * @param principal current account
     * @return establishment of current account
     */
    private Establishment getRawEstablishment(Principal principal) {
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));

        return establishmentRepository.findEstablishmentByAccountId(account.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found establishment of account : " + principal.getName()));

    }

    /**
     * Convert certification to dto.
     *
     * @param entity certification entity
     * @return certification dto
     */
    private List<CertificationDTO> convertEstablishmentToCertificationDTO(Establishment entity) {

        List<CertificationDTO> certificationDTOList = new ArrayList<>();
        List<EstablishmentCertification> cerList = entity.getEstablishmentCertifications();
        cerList.forEach(r -> {
            certificationDTOList.add(certificationMapper.convertToDTO(r));
        });
        return certificationDTOList;
    }

    @Override
    public List<CertificationDTO> getCertificationByAccountId(int accountId) {
        Establishment establishment = establishmentRepository.findEstablishmentByAccountId(accountId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Establishment don't exist"));
        return certificationMapper.toListCertificationDtos(establishment.getEstablishmentCertifications());
    }

    @Override
    public List<CertificationDTO> getCertificationByEstablishmentId(int establishmentId) {
        return certificationMapper.toListCertificationDtos(certificationRepository.findAllByEstablishment_Id(establishmentId));
    }
}
