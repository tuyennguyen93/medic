package com.mmedic.rest.certification.repo;

import com.mmedic.entity.EstablishmentCertification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CertificationRepository extends JpaRepository<EstablishmentCertification, Integer> {
    Optional<EstablishmentCertification> findByIdAndEstablishment_Id(int id, int certificationId);

    /**
     * Find all certification by establishment id.
     *
     * @param establishmentId the establishment id
     * @return list certification of establishment
     */
    List<EstablishmentCertification> findAllByEstablishment_Id(int establishmentId);
}
