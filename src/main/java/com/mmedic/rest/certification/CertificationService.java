package com.mmedic.rest.certification;

import com.mmedic.rest.certification.dto.CertificationDTO;

import java.security.Principal;
import java.util.List;

public interface CertificationService {

    /**
     * Get certifications.
     *
     * @param principal current account
     * @return list certification dto
     */
    List<CertificationDTO> getCertifications(Principal principal);

    /**
     * Create new certification.
     *
     * @param principal current account
     * @param reqDto    certification data
     * @return certification is created
     */
    CertificationDTO createCertification(Principal principal, CertificationDTO reqDto);

    /**
     * Update certification.
     *
     * @param principal current account
     * @param id        the id of certification
     * @param reqDto    certification data
     * @return certification is updated
     */
    CertificationDTO updateCertification(Principal principal, int id, CertificationDTO reqDto);

    /**
     * Delete certification.
     *
     * @param principal current account
     * @param id        the id of certification
     * @return certification is deleted
     */
    CertificationDTO deleteCertification(Principal principal, int id);

    /**
     * Get list certification by account id.
     *
     * @param accountId the account id
     * @return list certification
     */
    List<CertificationDTO> getCertificationByAccountId(int accountId);

    /**
     * Get list certification by establishment id
     *
     * @param establishmentId the establishment id
     * @return list certification
     */
    List<CertificationDTO> getCertificationByEstablishmentId(int establishmentId);

}
