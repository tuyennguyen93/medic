package com.mmedic.rest.certification;

import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.certification.dto.CertificationDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

/**
 * Certification Controller.
 */
@Slf4j
@RestController
@RequestMapping(CertificationController.BASE_URL)
@Api(value = "Certification Management", description = "Edit Certification profiles")
@RequiredArgsConstructor
public class CertificationController {

    static final String BASE_URL = "/api/v1/certifications";

    private final CertificationService certificationService;

    @ApiOperation("get Certification (or licence) list (DONE)")
    @GetMapping
    public MedicResponse<List<CertificationDTO>> getCertificationList(Principal principal) {
        List<CertificationDTO> result = certificationService.getCertifications(principal);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("create Certification (or licence) (DONE)")
    @PostMapping
    public MedicResponse<CertificationDTO> createCertification(Principal principal, @RequestBody CertificationDTO certificationDto) {
        CertificationDTO result = certificationService.createCertification(principal, certificationDto);

        return MedicResponse.okStatus(result);
    }

    @ApiOperation("update Certification (or licence) (DONE)")
    @PutMapping("{id}")
    public MedicResponse<CertificationDTO> updateCertification(Principal principal, @PathVariable(value = "id") Integer id, @RequestBody CertificationDTO certificationDto) {
        CertificationDTO result = certificationService.updateCertification(principal, id, certificationDto);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("delete Certification (or licence) (DONE)")
    @DeleteMapping("{id}")
    public MedicResponse<CertificationDTO> deleteCertification(Principal principal, @PathVariable("id") Integer id) {
        CertificationDTO result = certificationService.deleteCertification(principal, id);
        return MedicResponse.okStatus(result);
    }
}
