package com.mmedic.rest.establishment.dto;

import com.mmedic.enums.EstablishmentType;
import com.mmedic.rest.doctor.dto.DepartmentDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class EstablishmentDTO {

    private int id;

    private String name;

    private String address;

    private String phoneNumber;

    private String avatarUrl;

    private float ratingAverage;

    private String workingSchedule;

    private EstablishmentType establishmentType;

    private DepartmentDTO medicalDepartment;
}
