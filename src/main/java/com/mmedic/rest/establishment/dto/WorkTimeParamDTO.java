package com.mmedic.rest.establishment.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mmedic.enums.Day;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalTime;
import java.util.List;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class WorkTimeParamDTO {

    @JsonFormat(pattern = "H:m:s")
    private LocalTime startTime;

    @JsonFormat(pattern = "H:m:s")
    private LocalTime endTime;

    private List<Day> days;
}
