package com.mmedic.rest.establishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@ApiModel("Work Time")
public class WorkTimeDTO {

    @ApiModelProperty("Schedule Id")
    private int id;

    @ApiModelProperty("Date")
    private LocalDate date;

    @ApiModelProperty("Start time")
    private LocalTime startTime;

    @ApiModelProperty("End time")
    private LocalTime endTime;

}
