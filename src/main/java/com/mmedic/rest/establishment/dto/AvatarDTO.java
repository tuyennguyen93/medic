package com.mmedic.rest.establishment.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AvatarDTO {

    private String url;
}
