package com.mmedic.rest.establishment.dto;

import com.mmedic.enums.Day;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class EstablishmentWorkingScheduleDTO {

    private int id;

    private LocalTime startTime;

    private LocalTime endTime;

    private Day dayOfWeek;
}
