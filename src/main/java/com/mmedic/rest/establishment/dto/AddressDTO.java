package com.mmedic.rest.establishment.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddressDTO {

    private String address;

    private double latitude;

    private double longitude;
}
