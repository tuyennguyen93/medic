package com.mmedic.rest.establishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class EstablishmentShortInfoDTO {

    private int id;

    private String name;
}
