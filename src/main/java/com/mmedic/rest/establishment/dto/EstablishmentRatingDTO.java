package com.mmedic.rest.establishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class EstablishmentRatingDTO {

    private int id;

    private int rate;

    private String comment;

    //private LocalDateTime createAt;

    private int accountRatingId;

    private int medicalRecordId;


}
