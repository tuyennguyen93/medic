package com.mmedic.rest.establishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class EstablishmentRatingFullDTO {

    private int id;

    private int rate;

    private String comment;

    private LocalDateTime createAt;

    private int establishmentId;

    private int medicalRecordId;

    private String name;


}
