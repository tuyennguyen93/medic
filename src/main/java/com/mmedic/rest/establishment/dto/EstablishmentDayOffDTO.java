package com.mmedic.rest.establishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class EstablishmentDayOffDTO {

    private int id;

    private LocalDateTime startDate;

    private LocalDateTime endDate;
}
