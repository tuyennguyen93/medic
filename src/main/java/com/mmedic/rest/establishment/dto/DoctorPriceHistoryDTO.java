package com.mmedic.rest.establishment.dto;

import java.time.LocalDateTime;

public interface DoctorPriceHistoryDTO {

    int getId();

    LocalDateTime getEffectiveAt();

    int getPrice();
}
