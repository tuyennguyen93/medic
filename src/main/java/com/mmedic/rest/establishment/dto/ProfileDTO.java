package com.mmedic.rest.establishment.dto;

import com.mmedic.rest.certification.dto.CertificationDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ProfileDTO {

    private String name;

    private String phoneNumber;

    private String address;

    private String avartarUrl;

    private List<CertificationDTO> certificationList;

    private List<CertificationDTO> licenceList;

    private String degree;

    private String medicalDepartment;

    private String houseAddress;

    private double latitude;

    private double longitude;

    private double houseLatitude;

    private double houseLongitude;

}
