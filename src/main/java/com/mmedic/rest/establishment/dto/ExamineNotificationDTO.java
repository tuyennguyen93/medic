package com.mmedic.rest.establishment.dto;

public interface ExamineNotificationDTO {

    int getMedicalRecordId();

    int getPatientId();

    int getDoctorId();

    String getDoctorName();

    String getPatientName();

    String getNotificationToken();

    String getLanguage();
}
