package com.mmedic.rest.establishment.impl;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.EstablishmentDayOff;
import com.mmedic.entity.EstablishmentRating;
import com.mmedic.entity.EstablishmentWorkingSchedule;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.entity.MedicalServicePriceHistory;
import com.mmedic.entity.MedicalServiceRegistry;
import com.mmedic.enums.Day;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.dto.StaffDayOffDTO;
import com.mmedic.rest.account.dto.StaffWorkingScheduleDTO;
import com.mmedic.rest.account.mapper.AccountMapper;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.establishment.EstablishmentService;
import com.mmedic.rest.establishment.dto.AddressDTO;
import com.mmedic.rest.establishment.dto.DayOffParamDTO;
import com.mmedic.rest.establishment.dto.DoctorPriceDTO;
import com.mmedic.rest.establishment.dto.DoctorPriceHistoryDTO;
import com.mmedic.rest.establishment.dto.ExamineNotificationDTO;
import com.mmedic.rest.establishment.dto.EstablishmentDayOffDTO;
import com.mmedic.rest.establishment.dto.EstablishmentRatingDTO;
import com.mmedic.rest.establishment.dto.EstablishmentRatingFullDTO;
import com.mmedic.rest.establishment.dto.EstablishmentWorkingScheduleDTO;
import com.mmedic.rest.establishment.dto.ProfileDTO;
import com.mmedic.rest.establishment.dto.WorkTimeDTO;
import com.mmedic.rest.establishment.dto.WorkTimeParamDTO;
import com.mmedic.rest.establishment.mapper.EstablishmentMapper;
import com.mmedic.rest.establishment.mapper.ProfileMapper;
import com.mmedic.rest.establishment.repo.EstablishmentDayOffRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRatingRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalService.repo.MedicalServicePriceHistoryRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.security.Principal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Establishment Service implementation.
 */
@RequiredArgsConstructor
@Service
public class EstablishmentServiceImpl implements EstablishmentService {

    private final AccountService accountService;

    private final EstablishmentRepository establishmentRepository;

    private final EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    private final EstablishmentDayOffRepository establishmentDayOffRepository;

    private final EstablishmentMapper establishmentMapper;

    private final AccountRepository accountRepository;

    private final MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    private final MedicalServicePriceHistoryRepository medicalServicePriceHistoryRepository;

    private final EstablishmentRatingRepository establishmentRatingRepository;

    private final MedicalRecordRepository medicalRecordRepository;

    private final AccountMapper accountMapper;

    private final ProfileMapper profileMapper;

    private final NotificationService notificationService;

    @Override
    public ProfileDTO getProfile(Principal principal) {
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));
        Establishment establishment = establishmentRepository.findEstablishmentByAccountId(account.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found establishment of account : " + principal.getName()));
        establishment.setAvatarUrl(account.getAvatarUrl());
        ProfileDTO profileDto = profileMapper.convertEstablishmentToProfileDTO(establishment);
        profileDto.setDegree(account.getDegree());
        profileDto.setHouseAddress(account.getHouseAddress());
        profileDto.setHouseLongitude(account.getLongitude());
        profileDto.setHouseLatitude(account.getLatitude());
        if (null != establishment.getMedicalDepartment())
            profileDto.setMedicalDepartment(establishment.getMedicalDepartment().getName());
        return profileDto;
    }

    @Override
    @Transactional
    public ProfileDTO updatePhoneNumber(Principal principal, String phoneNumber) {
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));
        Establishment establishment = getRawEstablishment(principal);
        establishment.setPhoneNumber(phoneNumber);
        establishmentRepository.save(establishment);
        Establishment updatedEst = getRawEstablishment(principal);

        ProfileDTO profileDto = profileMapper.convertEstablishmentToProfileDTO(updatedEst);
        profileDto.setDegree(account.getDegree());
        profileDto.setHouseAddress(account.getHouseAddress());
        profileDto.setHouseLongitude(account.getLongitude());
        profileDto.setHouseLatitude(account.getLatitude());
        if (null != establishment.getMedicalDepartment())
            profileDto.setMedicalDepartment(establishment.getMedicalDepartment().getName());
        return profileDto;
    }

    @Override
    @Transactional
    public ProfileDTO updateAddress(Principal principal, AddressDTO address) {
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));
        Establishment establishment = getRawEstablishment(principal);
        if (null != address) {
            establishment.setAddress(address.getAddress());
            establishment.setLatitude(address.getLatitude());
            establishment.setLongitude(address.getLongitude());
        }
        establishmentRepository.save(establishment);
        Establishment updatedEst = getRawEstablishment(principal);

        ProfileDTO profileDto = profileMapper.convertEstablishmentToProfileDTO(updatedEst);
        profileDto.setDegree(account.getDegree());
        profileDto.setHouseAddress(account.getHouseAddress());
        profileDto.setHouseLongitude(account.getLongitude());
        profileDto.setHouseLatitude(account.getLatitude());
        if (null != establishment.getMedicalDepartment())
            profileDto.setMedicalDepartment(establishment.getMedicalDepartment().getName());
        return profileDto;
    }

    @Override
    @Transactional
    public AccountDTO updateAvatar(Principal principal, String file) {
        // up avatar at account
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));
        account.setAvatarUrl(file);
        Account accountUpdate = accountRepository.save(account);

        return accountMapper.toAccountDto(accountUpdate);
    }

    @Override
    public AccountDTO updateDegrees(Principal principal, String degree) {
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));
        account.setDegree(degree);
        Account accountUpdate = accountRepository.save(account);
        return accountMapper.toAccountDto(accountUpdate);
    }

    @Override
    public AccountDTO updateHouseAddress(Principal principal, AddressDTO address) {
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));
        account.setHouseAddress(address.getAddress());
        account.setLatitude(address.getLatitude());
        account.setLongitude(address.getLongitude());
        Account accountUpdate = accountRepository.save(account);
        return accountMapper.toAccountDto(accountUpdate);
    }

    //    @Override
    public ProfileDTO updateProfile() {
        return null;
    }

    @Override
    public Establishment getRawEstablishment(Principal principal) {
        Account account = accountService.
                findAccountByEmail(principal.getName()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + principal.getName()));

        return establishmentRepository.findEstablishmentByAccountId(account.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found establishment of account : " + principal.getName()));

    }


    @Override
    public List<EstablishmentWorkingScheduleDTO> findWorkingScheduleByAccountId(int accountId) throws MedicException {
        Establishment establishment = establishmentRepository.findEstablishmentByAccountId(accountId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Can't find establishment of account"));

        return establishmentMapper.toListEstablishmentWorkingScheduleDto(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalse(establishment.getId()));
    }

    @Override
    @Transactional
    public List<EstablishmentWorkingScheduleDTO> addWorkingSchedule(Account account, WorkTimeParamDTO workTimeParamDTO) throws MedicException {
        if (!Helper.accountHasAnyRole(account,
                Constants.ROLE_DOCTOR_ADMIN,
                Constants.ROLE_DOCTOR_STAFF,
                Constants.ROLE_PRECLINIC_ADMIN,
                Constants.ROLE_PRECLINIC_STAFF)) {
            throw new MedicException(MedicException.ERROR_ACCOUNT_CAN_NOT_ADD_WORK_TIME, "This account can not add work time");
        }
        if (CollectionUtils.isEmpty(workTimeParamDTO.getDays())) {
            throw new MedicException(MedicException.ERROR_WORKING_SCHEDULE_DAY_IS_EMPTY, "Working Schedule days is empty");
        }
        if (workTimeParamDTO.getStartTime().compareTo(workTimeParamDTO.getEndTime()) >= 0) {
            throw new MedicException(MedicException.ERROR_WORKING_SCHEDULE_START_TIME_AFTER_OR_EQUAL_END_TIME, "Start time is after or equal end time");
        }
        Establishment establishment = establishmentRepository.findEstablishmentByAccountId(account.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Can't find establishment of account"));

        List<EstablishmentWorkingSchedule> alreadyList = establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalse(establishment.getId());

        List<EstablishmentWorkingSchedule> establishmentWorkingSchedules = new ArrayList<>();
        List<EstablishmentWorkingSchedule> result = new ArrayList<>();
        workTimeParamDTO.getDays().forEach(day -> {
            EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
            workingSchedule.setEstablishment(establishment);
            workingSchedule.setDayOfWeek(day);
            workingSchedule.setStartTime(LocalTime.of(workTimeParamDTO.getStartTime().getHour(), workTimeParamDTO.getStartTime().getMinute()));
            workingSchedule.setEndTime(LocalTime.of(workTimeParamDTO.getEndTime().getHour(), workTimeParamDTO.getEndTime().getMinute()));
            if (checkWorkingScheduleDay(workingSchedule, alreadyList)) {
                establishmentWorkingSchedules.add(workingSchedule);
            }
        });

        if (establishmentWorkingSchedules.size() > 0) {
            result = establishmentWorkingScheduleRepository.saveAll(establishmentWorkingSchedules);
        }

        return establishmentMapper.toListEstablishmentWorkingScheduleDto(result);
    }

    private boolean checkWorkingScheduleDay(EstablishmentWorkingSchedule newWorkingSchedule, List<EstablishmentWorkingSchedule> alreadyList) {
        if (!CollectionUtils.isEmpty(alreadyList)) {
            for (EstablishmentWorkingSchedule workingSchedule : alreadyList) {
                if (newWorkingSchedule.getDayOfWeek().equals(workingSchedule.getDayOfWeek())
                        && !(newWorkingSchedule.getEndTime().isBefore(workingSchedule.getStartTime())
                        || newWorkingSchedule.getStartTime().isAfter(workingSchedule.getEndTime()))) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    @Transactional
    public boolean removeWorkingSchedule(int workingScheduleId) throws MedicException {
        EstablishmentWorkingSchedule workingSchedule = establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(workingScheduleId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_WORKING_SCHEDULE_NOT_EXIST, "Working Schedule don't exist"));
        workingSchedule.setDeleted(true);
        establishmentWorkingScheduleRepository.save(workingSchedule);
        return true;
    }

    @Override
    public List<EstablishmentDayOffDTO> findDayOffByAccountId(int accountId) throws MedicException {
        Establishment establishment = establishmentRepository.findEstablishmentByAccountId(accountId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Can't find establishment of account"));

        return establishmentMapper.toListEstablishmentDayOffDto(establishmentDayOffRepository.findAllByEstablishment_IdAndEndDateGreaterThanEqual(establishment.getId(), LocalDateTime.now()));
    }

    @Override
    @Transactional
    public EstablishmentDayOffDTO addDayOff(Account account, DayOffParamDTO dayOffParamDTO) throws MedicException {
        if (!Helper.accountHasAnyRole(account,
                Constants.ROLE_DOCTOR_ADMIN,
                Constants.ROLE_DOCTOR_STAFF,
                Constants.ROLE_PRECLINIC_ADMIN,
                Constants.ROLE_PRECLINIC_STAFF)) {
            throw new MedicException(MedicException.ERROR_ACCOUNT_CAN_NOT_ADD_DAY_OFF, "This account can not add day off");
        }
        if (dayOffParamDTO.getStartDate().compareTo(dayOffParamDTO.getEndDate()) >= 0) {
            throw new MedicException(MedicException.ERROR_DAY_OFF_START_DATE_AFTER_OR_EQUAL_END_DATE, "Start date is after or equal end date");
        }
        Establishment establishment = establishmentRepository.findEstablishmentByAccountId(account.getId()).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Can't find establishment of account"));

        List<EstablishmentDayOff> alreadyList = establishmentDayOffRepository.findAllByEstablishment_IdAndEndDateGreaterThanEqual(establishment.getId(), LocalDateTime.now());

        // Prepare new day off
        EstablishmentDayOff newDayOff = new EstablishmentDayOff();
        newDayOff.setEstablishment(establishment);
        newDayOff.setStartDate(dayOffParamDTO.getStartDate());
        newDayOff.setEndDate(dayOffParamDTO.getEndDate());

        // Create new one if it doesn't exist
        EstablishmentDayOff result = null;
        if (checkDayOff(newDayOff, alreadyList)) {
            result = establishmentDayOffRepository.save(newDayOff);
        }
        return establishmentMapper.toEstablishmentDayOffDto(result);
    }

    private boolean checkDayOff(EstablishmentDayOff newDayOff, List<EstablishmentDayOff> alreadyList) {
        if (!CollectionUtils.isEmpty(alreadyList)) {
            for (EstablishmentDayOff dayOff : alreadyList) {
                if (!(newDayOff.getEndDate().isBefore(dayOff.getStartDate())
                        || newDayOff.getStartDate().isAfter(dayOff.getEndDate()))) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    @Transactional
    public boolean removeDayOff(int dayOffId) throws MedicException {
        EstablishmentDayOff dayOff = establishmentDayOffRepository.findById(dayOffId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DAY_OFF_NOT_EXIST, "Day Off don't exist"));
        establishmentDayOffRepository.delete(dayOff);
        return true;
    }

    @Override
    public Page<EstablishmentRatingFullDTO> getRattingList(Integer establishmentId, Pageable pageable) {
        Page<EstablishmentRating> page = establishmentRatingRepository.findByEstablishment_Id(establishmentId, pageable);
        return page.map(establishmentMapper::convertToEstablishmentRatingFullDTO);
    }


    @Override
    public EstablishmentRatingDTO createDoctorRating(int principalId, EstablishmentRatingDTO establishmentRatingDto) {
        Optional<Account> account = accountRepository.findById(establishmentRatingDto.getAccountRatingId());
        if (!account.isPresent()) return null;
        Establishment establishment = account.get().getEstablishment();
        if (establishment != null) {
            Optional<MedicalRecord> medicalRecord = medicalRecordRepository.findByIdAndPatient_Account_Id(establishmentRatingDto.getMedicalRecordId(), principalId);
            // Optional<MedicalRecord> medicalRecord = medicalRecordRepository.findById(establishmentRatingDto.getMedicalRecordId() );

            if (!medicalRecord.isPresent()) return null;
            // list establishment
            List<Establishment> establishmentList = new ArrayList<>();

//       medicalrecord -> exmaine_record -> medical_service_order -> medical_service_order_detail -> establishment

            if (medicalRecord.get().getExamineRecord() != null) {
                List<MedicalServiceOrderDetail> exmaineLst = medicalRecord.get().getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
                exmaineLst.forEach(item -> {
                    establishmentList.add(item.getEstablishment());
                });
            }
//        medicalrecord -> prescription_record -> medical_service_order -> medical_service_order_detail -> establishment
            if (medicalRecord.get().getPrescriptionRecord() != null) {
                List<MedicalServiceOrderDetail> prescriptionLst = medicalRecord.get().getPrescriptionRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
                prescriptionLst.forEach(item -> {
                    establishmentList.add(item.getEstablishment());
                });
            }
//        medicalrecord -> preclinical_record ->medical_service_order -> medical_service_order_detail -> establishment
            if (medicalRecord.get().getPreclinicalRecord() != null) {
                List<MedicalServiceOrderDetail> preclinicalLst = medicalRecord.get().getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
                preclinicalLst.forEach(item -> {
                    establishmentList.add(item.getEstablishment());
                });
            }

//        medicalrecord -> service_record ->medical_service_order -> medical_service_order_detail -> establishment
            if (medicalRecord.get().getServiceRecord() != null) {
                List<MedicalServiceOrderDetail> serviceLst = medicalRecord.get().getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
                serviceLst.forEach(item -> {
                    establishmentList.add(item.getEstablishment());
                });
            }

            AtomicInteger flag = new AtomicInteger();
            establishmentList.forEach(estItem -> {
                if (estItem.getId() == establishment.getId()) {
                    flag.set(1);
                }
            });

            if (flag.get() == 1) {

                Optional<EstablishmentRating> oldData = establishmentRatingRepository.findByEstablishment_IdAndMedicalRecord_Id(establishment.getId(), medicalRecord.get().getId());
                if (oldData.isPresent()) {
                    // find old record
                    throw new MedicException(MedicException.ERROR_RATING_ALREADY_EXIST, "rating already exist");
                } else {
                    // create a new record
                    EstablishmentRating establishmentRating = new EstablishmentRating();
                    establishmentRating.setComment(establishmentRatingDto.getComment());
                    establishmentRating.setEstablishment(establishment);
                    establishmentRating.setMedicalRecord(medicalRecord.get());
                    establishmentRating.setRate(establishmentRatingDto.getRate());

                    EstablishmentRating dataRs = establishmentRatingRepository.save(establishmentRating);
                    EstablishmentRatingDTO rs = establishmentMapper.convertToDTO(dataRs);
                    rs.setAccountRatingId(establishment.getId());
                    rs.setMedicalRecordId(medicalRecord.get().getId());

                    // rating_average => establishment
                    float ratingAvg = establishmentRepository.ratingAverage(establishment.getId());
                    establishment.setRatingAverage(ratingAvg);
                    establishmentRepository.save(establishment);

                    return rs;
                }
            }
            return null;
        }
        return null;
    }

    @Override
    public EstablishmentRatingDTO createClsRating(int principalId, EstablishmentRatingDTO establishmentRatingDto) {
        Optional<Establishment> establishment = establishmentRepository.findById(establishmentRatingDto.getAccountRatingId());
        if (establishment.isPresent()) {
            Optional<MedicalRecord> medicalRecord = medicalRecordRepository.findByIdAndPatient_Account_Id(establishmentRatingDto.getMedicalRecordId(), principalId);
            if (!medicalRecord.isPresent()) return null;
            // list establishment
            List<Establishment> establishmentList = new ArrayList<>();

//       medicalrecord -> exmaine_record -> medical_service_order -> medical_service_order_detail -> establishment

            if (medicalRecord.get().getExamineRecord() != null) {
                List<MedicalServiceOrderDetail> exmaineLst = medicalRecord.get().getExamineRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
                exmaineLst.forEach(item -> {
                    establishmentList.add(item.getEstablishment());
                });
            }
//        medicalrecord -> prescription_record -> medical_service_order -> medical_service_order_detail -> establishment
            if (medicalRecord.get().getPrescriptionRecord() != null) {
                List<MedicalServiceOrderDetail> prescriptionLst = medicalRecord.get().getPrescriptionRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
                prescriptionLst.forEach(item -> {
                    establishmentList.add(item.getEstablishment());
                });
            }
//        medicalrecord -> preclinical_record ->medical_service_order -> medical_service_order_detail -> establishment
            if (medicalRecord.get().getPreclinicalRecord() != null) {
                List<MedicalServiceOrderDetail> preclinicalLst = medicalRecord.get().getPreclinicalRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
                preclinicalLst.forEach(item -> {
                    establishmentList.add(item.getEstablishment());
                });
            }

//        medicalrecord -> service_record ->medical_service_order -> medical_service_order_detail -> establishment
            if (medicalRecord.get().getServiceRecord() != null) {
                List<MedicalServiceOrderDetail> serviceLst = medicalRecord.get().getServiceRecord().getMedicalServiceOrder().getMedicalServiceOrderDetails();
                serviceLst.forEach(item -> {
                    establishmentList.add(item.getEstablishment());
                });
            }

            AtomicInteger flag = new AtomicInteger();
            establishmentList.forEach(estItem -> {
                if (estItem.getId() == establishment.get().getId()) {
                    flag.set(1);
                }
            });

            if (flag.get() == 1) {

                Optional<EstablishmentRating> oldData = establishmentRatingRepository.findByEstablishment_IdAndMedicalRecord_Id(establishment.get().getId(), medicalRecord.get().getId());
                if (oldData.isPresent()) {
                    // find old record
                    throw new MedicException(MedicException.ERROR_RATING_ALREADY_EXIST, "rating already exist");
                } else {
                    // create a new record
                    EstablishmentRating establishmentRating = new EstablishmentRating();
                    establishmentRating.setComment(establishmentRatingDto.getComment());
                    establishmentRating.setEstablishment(establishment.get());
                    establishmentRating.setMedicalRecord(medicalRecord.get());
                    establishmentRating.setRate(establishmentRatingDto.getRate());

                    EstablishmentRating dataRs = establishmentRatingRepository.save(establishmentRating);
                    EstablishmentRatingDTO rs = establishmentMapper.convertToDTO(dataRs);
                    rs.setAccountRatingId(establishment.get().getId());
                    rs.setMedicalRecordId(medicalRecord.get().getId());

                    // rating_average => establishment
                    float ratingAvg = establishmentRepository.ratingAverage(establishment.get().getId());
                    establishment.get().setRatingAverage(ratingAvg);
                    establishmentRepository.save(establishment.get());

                    return rs;
                }
            }
            return null;
        }
        return null;
    }

    @Override
    public List<DoctorPriceHistoryDTO> getDoctorPriceHistory(int doctorId) throws MedicException {
        Account doctor = accountRepository.findDoctorAccountById(doctorId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_EXIST, "Doctor account don't exist"));
        return medicalServiceRegistryRepository.findDoctorPriceHistoryByEstablishmentId(doctor.getEstablishment().getId());
    }

    @Override
    @Transactional
    public DoctorPriceDTO addNewDoctorPrice(int doctorId, DoctorPriceDTO doctorPriceDTO) throws MedicException {
        Account doctor = accountRepository.findDoctorAccountById(doctorId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_EXIST, "Doctor account don't exist"));

        MedicalServiceRegistry serviceRegistry = medicalServiceRegistryRepository.
                findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_MEDICAL).
                orElseThrow(() -> new MedicException(MedicException.ERROR_DOCTOR_NOT_REGISTRY_TREATMENT_SERVICE, "Doctor don't registry treatment service or it's disabled"));

        MedicalServicePriceHistory price = null;
        LocalDateTime now = LocalDateTime.now();
        if (null != serviceRegistry) {
            if (checkEffectiveDateExist(now, serviceRegistry.getMedicalServicePriceHistories())) {
                price = new MedicalServicePriceHistory();
                price.setMedicalServiceRegistry(serviceRegistry);
                price.setPrice(doctorPriceDTO.getPrice());
                price.setEffectiveAt(now);
                price = medicalServicePriceHistoryRepository.save(price);

                // Notification
                notificationPriceChangeForPatient(doctorId);
            } else {
                throw new MedicException(MedicException.ERROR_EFFECTIVE_IS_EXIST, "This effective date is exist");
            }
        }
//        if (doctorPriceDTO.getEffectiveAt().isBefore(now)) {
//            if (CollectionUtils.isEmpty(serviceRegistry.getMedicalServicePriceHistories())) {
//                price.setEffectiveAt(now);
//            } else {
//                throw new MedicException(MedicException.ERROR_EFFECTIVE_IS_BACK_DATE, "Effective day is back date");
//            }
//        } else {
//            price.setEffectiveAt(doctorPriceDTO.getEffectiveAt());
//        }
//        price.setMedicalServiceRegistry(serviceRegistry);
//        price.setPrice(doctorPriceDTO.getPrice());

//        price = medicalServicePriceHistoryRepository.save(price);
        return establishmentMapper.toDoctorPriceDto(price);
    }

    /**
     * Notification price change for patient.
     *
     * @param doctorId doctor id have price is changed
     */
    private void notificationPriceChangeForPatient(int doctorId) {
        new Thread(() -> {
            List<ExamineNotificationDTO> examineNotifications = medicalRecordRepository.findExaminePriceChange(doctorId);
            if (!CollectionUtils.isEmpty(examineNotifications)) {
                notificationService.notificationExamineServicePriceChange(examineNotifications);
            }
        }).start();
    }

    private boolean checkEffectiveDateExist(LocalDateTime effectiveDate, List<MedicalServicePriceHistory> priceHistories) {
        return priceHistories.stream().noneMatch(priceHistory -> priceHistory.getEffectiveAt().isEqual(effectiveDate));
    }

    @Override
    public List<WorkTimeDTO> findWorkTimeEstablishment(LocalDate startDate, LocalDate endDate, int establishmentId) throws MedicException {
        // Get Map Day Off of establishment
        Map<String, List<StaffDayOffDTO>> dayOffMap = getEstablishmentDayOff(startDate, endDate, establishmentId);
        // Get Map Working Schedule of establishment
        Map<String, List<StaffWorkingScheduleDTO>> workingScheduleMap = getEstablishmentWorkingSchedule(startDate, endDate, establishmentId);

        // Calculate work time for establishment
        return calculateEstablishmentWorkingSchedule(establishmentId, startDate, endDate, dayOffMap, workingScheduleMap);
    }

    private List<WorkTimeDTO> calculateEstablishmentWorkingSchedule(int establishmentId, LocalDate startDate, LocalDate endDate,
                                                                    Map<String, List<StaffDayOffDTO>> dayOffMap, Map<String, List<StaffWorkingScheduleDTO>> workingScheduleMap) {
        List<WorkTimeDTO> workTimes = new ArrayList<>();
        if (!CollectionUtils.isEmpty(workingScheduleMap)) {
            // Get list day off of doctor
            List<StaffDayOffDTO> dayOffs = dayOffMap.get(String.valueOf(establishmentId));
            // Get list working schedule of doctor
            List<StaffWorkingScheduleDTO> workingSchedules = workingScheduleMap.get(String.valueOf(establishmentId));

            // To map date by MONDAY, TUESDAY, ...
            Map<DayOfWeek, List<StaffWorkingScheduleDTO>> mapDay = new HashMap<>();
            if (!CollectionUtils.isEmpty(workingSchedules)) {
                workingSchedules.forEach(staffWorkingSchedule -> {
                    List<StaffWorkingScheduleDTO> lstSchedule = mapDay.get(staffWorkingSchedule.getDay().getDayOfWeek());
                    if (CollectionUtils.isEmpty(lstSchedule)) {
                        lstSchedule = new ArrayList<>();
                    }
                    lstSchedule.add(staffWorkingSchedule);
                    mapDay.put(staffWorkingSchedule.getDay().getDayOfWeek(), lstSchedule);
                });
            }

            // Generate list date in range startDate, endDate
            LocalDate date = startDate.minusDays(1);
            while (date.isBefore(endDate)) {
                date = date.plusDays(1);
                List<StaffWorkingScheduleDTO> workingScheduleDay = mapDay.get(date.getDayOfWeek());

                if (!CollectionUtils.isEmpty(workingScheduleDay)) {
                    for (StaffWorkingScheduleDTO scheduleDay : workingScheduleDay) {
                        LocalDateTime startDateTime = LocalDateTime.of(date, scheduleDay.getStartTime());
                        LocalDateTime endDateTime = LocalDateTime.of(date, scheduleDay.getEndTime());
                        if (checkWorkingScheduleWithDayOff(startDateTime, endDateTime, dayOffs)) {
                            WorkTimeDTO workTimeDTO = new WorkTimeDTO();
                            workTimeDTO.setDate(LocalDate.from(date));
                            workTimeDTO.setStartTime(scheduleDay.getStartTime());
                            workTimeDTO.setEndTime(scheduleDay.getEndTime());
                            workTimeDTO.setId(scheduleDay.getId());
                            workTimes.add(workTimeDTO);
                        }
                    }
                }
            }
        }

        return workTimes;
    }

    private boolean checkWorkingScheduleWithDayOff(LocalDateTime startDate, LocalDateTime endDate, List<StaffDayOffDTO> dayOffs) {
        if (!CollectionUtils.isEmpty(dayOffs)) {
            for (StaffDayOffDTO dayOff : dayOffs) {
                if (!(endDate.compareTo(dayOff.getStartDate()) <= 0 || startDate.compareTo(dayOff.getEndDate()) >= 0)) {
                    return false;
                }
            }
        }
        return true;
    }

    private Map<String, List<StaffWorkingScheduleDTO>> getEstablishmentWorkingSchedule(LocalDate startDate, LocalDate endDate, int... establishmentIds) {
        List<StaffWorkingScheduleDTO> staffWorkingScheduleDTOs = establishmentRepository.findEstablishmentWorkingSchedule(establishmentIds);

        Map<String, List<StaffWorkingScheduleDTO>> workingScheduleMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(staffWorkingScheduleDTOs)) {
            staffWorkingScheduleDTOs.forEach(staffWorkingSchedule -> {
                String key = String.valueOf(staffWorkingSchedule.getEstablishmentId());
                List<StaffWorkingScheduleDTO> lstSchedule = workingScheduleMap.get(key);
                if (CollectionUtils.isEmpty(lstSchedule)) {
                    lstSchedule = new ArrayList<>();
                }
                lstSchedule.add(staffWorkingSchedule);
                workingScheduleMap.put(key, lstSchedule);
            });
        }
        return workingScheduleMap;
    }

    private Map<String, List<StaffDayOffDTO>> getEstablishmentDayOff(LocalDate startDate, LocalDate endDate, int... establishmentIds) {
        LocalDateTime sDate = LocalDateTime.of(startDate, LocalTime.of(0, 0));
        LocalDateTime eDate = LocalDateTime.of(endDate, LocalTime.of(23, 59, 59));

        List<StaffDayOffDTO> staffDayOffDTOs = establishmentRepository.findEstablishmentDayOff(sDate, eDate, establishmentIds);

        Map<String, List<StaffDayOffDTO>> dayOffMap = new HashMap<>();

        if (!CollectionUtils.isEmpty(staffDayOffDTOs)) {
            staffDayOffDTOs.forEach(dayOff -> {
                String key = String.valueOf(dayOff.getEstablishmentId());
                List<StaffDayOffDTO> accountDayOffs = dayOffMap.get(key);
                if (CollectionUtils.isEmpty(accountDayOffs)) {
                    accountDayOffs = new ArrayList<>();
                }
                accountDayOffs.add(dayOff);
                dayOffMap.put(key, accountDayOffs);
            });
        }

        return dayOffMap;
    }

    @Override
    public Establishment getEstablishmentById(int id) {
        return establishmentRepository.findById(id).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Establishment don't exist"));
    }

    public Establishment getEstablishmentByAccount(Integer accountId) {
        Account account = accountRepository.findById(accountId).
                orElseThrow(() -> new MedicException(MedicException.ERROR_ACCOUNT_DATA_INVALID, "Can not found account : " + accountId));
        return account.getEstablishment();
    }

    @Override
    public List<Integer> calculateAvailable(List<Integer> establishmentIds) {
        LocalDateTime now = LocalDateTime.now();
        Day today = Day.convert(now.getDayOfWeek());
        return establishmentWorkingScheduleRepository.findEstablishmentAvailable(establishmentIds, today.getCode());
    }

    @Override
    @Transactional
    public boolean setAverageTime(Establishment establishment, int timeInMinute) {
        boolean result = false;
        if (timeInMinute > 0) {
            establishment.setAverageTime(timeInMinute);
            establishmentRepository.save(establishment);
            result = true;
        }
        return result;
    }

    @Override
    public Integer getAverageTimeById(int establishmentId) {
        Integer averageTime = establishmentRepository.findEstablishmentAverageTimeById(establishmentId);
        return null != averageTime ? averageTime : 0;
    }

    @Override
    public EstablishmentRatingFullDTO getRattingByMedicalRecordId(int medicalRecordId,int establishmentId) {
        Optional<EstablishmentRating> data = establishmentRatingRepository.findByEstablishment_IdAndMedicalRecord_Id(establishmentId, medicalRecordId);
        if (data.isPresent()) {
            return establishmentMapper.convertToEstablishmentRatingFullDTO(data.get());
        }
        return null;
    }
}
