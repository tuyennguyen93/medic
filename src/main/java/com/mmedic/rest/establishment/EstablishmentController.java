package com.mmedic.rest.establishment;

import com.mmedic.entity.Establishment;
import com.mmedic.rest.MedicResponse;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.common.BaseService;
import com.mmedic.rest.establishment.dto.*;
import com.mmedic.security.AccountPrincipal;
import com.mmedic.spring.errors.MedicException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


import java.security.Principal;
import java.util.Collections;
import java.util.List;

/**
 * Establishment controller.
 */
@Slf4j
@RestController
@RequestMapping(EstablishmentController.BASE_URL)
@Api(value = "Establishment profiles", description = "Edit Establishment profiles (section 3.3 / 4.3 / 5.5 )")
@RequiredArgsConstructor
public class EstablishmentController {

    static final String BASE_URL = "/api/v1/establishments";

    private final EstablishmentService establishmentService;

    private final BaseService baseService;

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @ApiOperation("Get establishment's  profiles (DONE)")
    @GetMapping("/profiles")
    public MedicResponse<ProfileDTO> getProfile(Principal principal) {
        ProfileDTO result = establishmentService.getProfile(principal);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Edit establishment's  phone number (DONE)")
    @PutMapping("/profiles/phone-numbers")
    public MedicResponse<ProfileDTO> updatePhoneNumber(Principal principal, @RequestBody @ApiParam("phone number") PhoneNumberDTO phone) {
        ProfileDTO result = establishmentService.updatePhoneNumber(principal, phone.getPhoneNumber());
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Edit establishment's address (DONE)")
    @PutMapping("/profiles/addresses")
    public MedicResponse<ProfileDTO> updateAddress(Principal principal, @RequestBody @ApiParam("address") AddressDTO address) {
        ProfileDTO result = establishmentService.updateAddress(principal, address);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Edit account avatar (DONE)")
    @PutMapping("/profiles/avatars")
    public MedicResponse<AccountDTO> updateAvatars(Principal principal, @RequestBody @ApiParam("avatar") AvatarDTO avatar) {
        AccountDTO result = establishmentService.updateAvatar(principal, avatar.getUrl());
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Edit account degree (DONE)")
    @PutMapping("/profiles/degrees")
    public MedicResponse<AccountDTO> updateDegree(Principal principal, @RequestBody @ApiParam("degree") DegreeDTO degreeDto) {
        AccountDTO result = establishmentService.updateDegrees(principal, degreeDto.getDegree());
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Edit account address (DONE)")
    @PutMapping("/profiles/house-addresses")
    public MedicResponse<AccountDTO> updateHouseAddress(Principal principal, @RequestBody @ApiParam("house-addresses") AddressDTO address) {
        AccountDTO result = establishmentService.updateHouseAddress(principal, address);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Find Establishment Working Schedule (DONE)")
    @GetMapping("/work-times")
    public MedicResponse<List<EstablishmentWorkingScheduleDTO>> getWorkingSchedule() throws MedicException {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(establishmentService.findWorkingScheduleByAccountId(accountPrincipal.getId()));
        }
        return MedicResponse.okStatus(Collections.emptyList());
    }

    @ApiOperation("Add work time schedule (DONE)")
    @PostMapping("/work-times")
    public MedicResponse<List<EstablishmentWorkingScheduleDTO>> addWorkingSchedule(@RequestBody @ApiParam("Work Time Param") WorkTimeParamDTO workTimeParamDTO) throws MedicException {
        return MedicResponse.okStatus(establishmentService.addWorkingSchedule(baseService.getCurrentAccountLogin(), workTimeParamDTO));

    }

    @ApiOperation("Remove work time schedule (DONE)")
    @DeleteMapping("/work-times/{id}")
    public MedicResponse<Boolean> removeWorkingSchedule(@PathVariable("id") @ApiParam("Working Schedule Id") int id) throws MedicException {
        return MedicResponse.okStatus(establishmentService.removeWorkingSchedule(id));
    }

    @ApiOperation("Find all day off (DONE)")
    @GetMapping("/days-off")
    public MedicResponse<List<EstablishmentDayOffDTO>> getDayOff() {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(establishmentService.findDayOffByAccountId(accountPrincipal.getId()));
        }
        return MedicResponse.okStatus(Collections.emptyList());
    }

    @ApiOperation("Add Day Off (DONE)")
    @PostMapping("/days-off")
    public MedicResponse<EstablishmentDayOffDTO> addDayOff(@RequestBody @ApiParam("Day Off") DayOffParamDTO dayOffParamDTO) throws MedicException {

        return MedicResponse.okStatus(establishmentService.addDayOff(baseService.getCurrentAccountLogin(), dayOffParamDTO));

    }

    @ApiOperation("Remove Day Off (DONE)")
    @DeleteMapping("/days-off/{id}")
    public MedicResponse<Boolean> removeDayOff(@PathVariable("id") @ApiParam("Day Off Id") int id) throws MedicException {
        return MedicResponse.okStatus(establishmentService.removeDayOff(id));
    }

    @ApiOperation("Get Price History for doctor (DONE)")
    @GetMapping("/doctor-prices")
    public MedicResponse<List<DoctorPriceHistoryDTO>> getDoctorPriceHistory() {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(establishmentService.getDoctorPriceHistory(accountPrincipal.getId()));
        }
        return MedicResponse.okStatus(null);
    }

    @ApiOperation("Add New Doctor Price (DONE)")
    @PostMapping("/doctor-prices")
    public MedicResponse<DoctorPriceDTO> addNewDoctorPrice(@RequestBody DoctorPriceDTO doctorPriceDTO) throws MedicException {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            return MedicResponse.okStatus(establishmentService.addNewDoctorPrice(accountPrincipal.getId(), doctorPriceDTO));
        }
        return MedicResponse.okStatus(null);
    }

    @ApiOperation("Get Ratings sort by user login for web (DONE)")
    @GetMapping("/rating")
    public MedicResponse<Page<EstablishmentRatingFullDTO>> getRattingTimeList(Pageable pageable, Principal principal) {
        Establishment establishment = baseService.getRawEstablishment(principal);
        Page<EstablishmentRatingFullDTO> result = establishmentService.getRattingList(establishment.getId(), pageable);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get Ratings sort by establishmentId for app (DONE)")
    @GetMapping("/rating-app/{establishmentId}")
    public MedicResponse<Page<EstablishmentRatingFullDTO>> getRattingListEApp(Pageable pageable,
                                                                              @PathVariable("establishmentId") @ApiParam("establishment Id") Integer establishmentId
    ) {
        Page<EstablishmentRatingFullDTO> result = establishmentService.getRattingList(establishmentId, pageable);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get Ratings sort by doctorId for app (DONE)")
    @GetMapping("/rating-app/doctors/{doctorId}")
    public MedicResponse<Page<EstablishmentRatingFullDTO>> getRattingListDoctorApp(
            @PathVariable("doctorId") @ApiParam("doctor Id") Integer doctorId, Pageable pageable) {
        Establishment establishment = establishmentService.getEstablishmentByAccount(doctorId);
        if (establishment == null) {
            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Can't find establishment data");
        }
        Page<EstablishmentRatingFullDTO> result = establishmentService.getRattingList(establishment.getId(), pageable);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Add New rating for doctor (DONE)")
    @Transactional
    @PostMapping("/rating/doctors")
    public MedicResponse<EstablishmentRatingDTO> createDoctorRating(@RequestBody EstablishmentRatingDTO establishmentRatingDto) throws MedicException {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            if (establishmentRatingDto.getRate() < 1 || establishmentRatingDto.getRate() > 5) {
                throw new MedicException(MedicException.ERROR_RATING_INVALID, "Data rating not in [1,2,3,4,5] ");
            }
            EstablishmentRatingDTO result = establishmentService.createDoctorRating(accountPrincipal.getId(), establishmentRatingDto);
            if (result == null) {
                throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_MEDICAL_RECORD_OR_ESTABLISHMENT, "Can't find medicalRecord or establshment data");
            }
            return MedicResponse.okStatus(result);
        }
        return null;
    }

    @ApiOperation("Add New rating for subclinical vs drugstore(DONE)")
    @Transactional
    @PostMapping("/rating/preclinical")
    public MedicResponse<EstablishmentRatingDTO> createClsRating(@RequestBody EstablishmentRatingDTO establishmentRatingDto) throws MedicException {
        Authentication authentication = getAuthentication();
        if (null == authentication || null == authentication.getPrincipal()) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_PRINCIPAL, "Can't find principal data");
        }
        if (AccountPrincipal.class.isInstance(authentication.getPrincipal())) {
            AccountPrincipal accountPrincipal = AccountPrincipal.class.cast(authentication.getPrincipal());
            if (establishmentRatingDto.getRate() < 1 || establishmentRatingDto.getRate() > 5) {
                throw new MedicException(MedicException.ERROR_RATING_INVALID, "Data rating not in [1,2,3,4,5] ");
            }
            EstablishmentRatingDTO result = establishmentService.createClsRating(accountPrincipal.getId(), establishmentRatingDto);
            if (result == null) {
                throw new MedicException(MedicException.ERROR_CAN_NOT_FIND_MEDICAL_RECORD_OR_ESTABLISHMENT, "Can't find medicalRecord or establshment data");
            }
            return MedicResponse.okStatus(result);
        }
        return null;
    }

    @ApiOperation("Get Rating  by medical Record Id and establishment Id (DONE)")
    @GetMapping("/rating-app/preclinical")
    public MedicResponse<EstablishmentRatingFullDTO> getRattingByMedicalRecordId(@RequestParam("establishmentId") @ApiParam("establishment Id") Integer establishmentId,
                                                                                 @RequestParam("medicalRecordId") @ApiParam("Medical Record Id") Integer medicalRecordId
    ) {
        EstablishmentRatingFullDTO result = establishmentService.getRattingByMedicalRecordId(medicalRecordId, establishmentId);
        return MedicResponse.okStatus(result);
    }

    @ApiOperation("Get Rating  by medical Record Id and doctor Id (DONE)")
    @GetMapping("/rating-app/doctor")
    public MedicResponse<EstablishmentRatingFullDTO> getRattingByMedicalRecordIdAndDoctorId(@RequestParam("doctorId") @ApiParam("doctor Id") Integer doctorId,
                                                                                            @RequestParam("medicalRecordId") @ApiParam("Medical Record Id") Integer medicalRecordId
    ) {
        Establishment establishment = establishmentService.getEstablishmentByAccount(doctorId);
        if (establishment == null) {
            throw new MedicException(MedicException.ERROR_ESTABLISHMENT_NOT_FOUND, "Can't find establishment data");
        }
        EstablishmentRatingFullDTO result = establishmentService.getRattingByMedicalRecordId(medicalRecordId, establishment.getId());
        return MedicResponse.okStatus(result);
    }


    /**
     * Gets average time of doctor/preclinical.
     *
     * @return the average time
     */
    @ApiOperation("Get average time of doctor/preclinical (DONE)")
    @GetMapping("/average-time")
    public MedicResponse<Integer> getAverageTime() {
        return MedicResponse.okStatus(establishmentService.getAverageTimeById(baseService.getCurrentEstablishment().getId()));
    }

    /**
     * Set average time for doctor/preclinical.
     *
     * @param timeInMinute time in minute
     * @return true if updated successfully
     */
    @ApiOperation("Set average time for doctor/preclinical (DONE)")
    @PutMapping("/average-time")
    public MedicResponse<Boolean> createAverageTime(@RequestBody int timeInMinute) {
        return MedicResponse.okStatus(establishmentService.setAverageTime(baseService.getCurrentEstablishment(), timeInMinute));
    }

}
