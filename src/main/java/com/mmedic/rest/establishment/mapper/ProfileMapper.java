package com.mmedic.rest.establishment.mapper;

import com.mmedic.entity.Establishment;
import com.mmedic.entity.EstablishmentCertification;
import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.rest.certification.dto.CertificationDTO;
import com.mmedic.rest.certification.mapper.CertificationMapper;
import com.mmedic.rest.establishment.dto.ProfileDTO;
import com.mmedic.rest.medicalService.dto.MedicalServiceDTO;
import com.mmedic.rest.medicalService.mapper.MedicalServiceMapper;
import com.mmedic.rest.transactionHistory.dto.TransactionHistoryDTO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProfileMapper {

    private final CertificationMapper certificationMapper;

    public ProfileDTO convertEstablishmentToProfileDTO(Establishment entity) {
        com.mmedic.rest.establishment.dto.ProfileDTO dto = new ProfileDTO();
        dto.setName(entity.getName());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setAddress(entity.getAddress());
        dto.setAvartarUrl(entity.getAvatarUrl());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());
        List<CertificationDTO> certificationDTOList = new ArrayList<>();
        List<CertificationDTO> licenceDTOList = new ArrayList<>();
        List<EstablishmentCertification> cerList = entity.getEstablishmentCertifications();
        cerList.forEach(item -> {
            if (item.getIsLicence()) {
                licenceDTOList.add(certificationMapper.convertToDTO(item));
            } else {
                certificationDTOList.add(certificationMapper.convertToDTO(item));
            }
        });
        dto.setCertificationList(certificationDTOList);
        dto.setLicenceList(licenceDTOList);
        return dto;
    }

}
