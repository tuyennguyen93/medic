package com.mmedic.rest.establishment.mapper;

import com.mmedic.entity.EstablishmentDayOff;
import com.mmedic.entity.EstablishmentRating;
import com.mmedic.entity.EstablishmentWorkingSchedule;
import com.mmedic.entity.MedicalServicePriceHistory;
import com.mmedic.rest.establishment.dto.DoctorPriceDTO;
import com.mmedic.rest.establishment.dto.EstablishmentDayOffDTO;
import com.mmedic.rest.establishment.dto.EstablishmentRatingDTO;
import com.mmedic.rest.establishment.dto.EstablishmentRatingFullDTO;
import com.mmedic.rest.establishment.dto.EstablishmentWorkingScheduleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EstablishmentMapper {

    EstablishmentWorkingScheduleDTO toEstablishmentWorkingScheduleDto(EstablishmentWorkingSchedule establishmentWorkingSchedule);

    List<EstablishmentWorkingScheduleDTO> toListEstablishmentWorkingScheduleDto(List<EstablishmentWorkingSchedule> establishmentWorkingSchedules);

    EstablishmentDayOffDTO toEstablishmentDayOffDto(EstablishmentDayOff establishmentDayOff);

    List<EstablishmentDayOffDTO> toListEstablishmentDayOffDto(List<EstablishmentDayOff> establishmentDayOffs);

    DoctorPriceDTO toDoctorPriceDto(MedicalServicePriceHistory medicalServicePriceHistory);

    EstablishmentRatingDTO convertToDTO(EstablishmentRating entity);

    EstablishmentRating convertToEntity(EstablishmentRatingDTO dto);

    @Mappings({
            @Mapping(target = "establishmentId", source = "establishment.id"),
            @Mapping(target = "medicalRecordId", source = "medicalRecord.id"),
            @Mapping(target = "name", source = "medicalRecord.patient.name")
    })
    EstablishmentRatingFullDTO convertToEstablishmentRatingFullDTO(EstablishmentRating establishmentRating);
}
