package com.mmedic.rest.establishment;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.establishment.dto.*;
import com.mmedic.spring.errors.MedicException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;


/**
 * Establishment service.
 */
public interface EstablishmentService {

    /**
     * Get profile of current establishment.
     *
     * @param principal current user login
     * @return profile dto
     */
    ProfileDTO getProfile(Principal principal);

    /**
     * Update phone number.
     *
     * @param principal   current account
     * @param phoneNumber phone number
     * @return profile dto
     */
    ProfileDTO updatePhoneNumber(Principal principal, String phoneNumber);

    /**
     * Update address.
     *
     * @param principal current account
     * @param address   address
     * @return profile dto
     */
    ProfileDTO updateAddress(Principal principal, AddressDTO address);

    /**
     * Update avatar.
     *
     * @param principal current account
     * @param file      url of avatar file
     * @return Account dto
     */
    AccountDTO updateAvatar(Principal principal, String file);

    /**
     * Update degrees.
     *
     * @param principal current account
     * @param degree      degrees of acount
     * @return Account dto
     */
    AccountDTO updateDegrees(Principal principal, String degree);

    /**
     * Update acount address.
     *
     * @param principal current account
     * @param address      address of acount
     * @return Account dto
     */
    AccountDTO updateHouseAddress(Principal principal, AddressDTO address);


    /**
     * Find working schedule time by account id.
     *
     * @param accountId the account id
     * @return list working schedule time
     * @throws MedicException
     */
    List<EstablishmentWorkingScheduleDTO> findWorkingScheduleByAccountId(int accountId) throws MedicException;

    /**
     * Add working schedule time.
     *
     * @param account        account
     * @param workTimeParamDTO working time data
     * @return list working schedule
     * @throws MedicException
     */
    List<EstablishmentWorkingScheduleDTO> addWorkingSchedule(Account account, WorkTimeParamDTO workTimeParamDTO) throws MedicException;

    /**
     * Remove working schedule.
     *
     * @param workingScheduleId working schedule id
     * @return true if it removed successfully
     * @throws MedicException
     */
    boolean removeWorkingSchedule(int workingScheduleId) throws MedicException;

    /**
     * Find day of of account.
     *
     * @param accountId the account id
     * @return list day off
     * @throws MedicException
     */
    List<EstablishmentDayOffDTO> findDayOffByAccountId(int accountId) throws MedicException;

    /**
     * Add new day off.
     *
     * @param account      the account
     * @param dayOffParamDTO day off data
     * @return day off is created
     * @throws MedicException
     */
    EstablishmentDayOffDTO addDayOff(Account account, DayOffParamDTO dayOffParamDTO) throws MedicException;

    /**
     * Remove day off.
     *
     * @param dayOffId the day off id
     * @return true if it removed successfully
     * @throws MedicException
     */
    boolean removeDayOff(int dayOffId) throws MedicException;

    /**
     * Get rating list
     *
     * @param establishmentId establishment id
     * @param pageable        pagination
     * @return list rating
     */
    Page<EstablishmentRatingFullDTO> getRattingList(Integer establishmentId, Pageable pageable);

    /**
     * Create rating for doctor
     *
     * @param principalId            current account
     * @param establishmentRatingDto rating data
     * @return
     */
    EstablishmentRatingDTO createDoctorRating(int principalId, EstablishmentRatingDTO establishmentRatingDto);

    EstablishmentRatingDTO createClsRating(int principalId, EstablishmentRatingDTO establishmentRatingDto);

    /**
     * Get doctor price history.
     *
     * @param doctorId the doctor id
     * @return list doctor price history
     * @throws MedicException
     */
    List<DoctorPriceHistoryDTO> getDoctorPriceHistory(int doctorId) throws MedicException;

    /**
     * Add new doctor price.
     *
     * @param doctorId       doctor id
     * @param doctorPriceDTO price
     * @return price is created
     * @throws MedicException
     */
    DoctorPriceDTO addNewDoctorPrice(int doctorId, DoctorPriceDTO doctorPriceDTO) throws MedicException;

    /**
     * Get working schedule of establishment.
     *
     * @param startDate       start date
     * @param endDate         end date
     * @param establishmentId establishment id
     * @return list working schedule
     * @throws MedicException
     */
    List<WorkTimeDTO> findWorkTimeEstablishment(LocalDate startDate, LocalDate endDate, int establishmentId) throws MedicException;

    /**
     * Get establishment entity.
     *
     * @param principal current account
     * @return establishment of current account
     */
    Establishment getRawEstablishment(Principal principal);

    /**
     * Get establishment by account id.
     *
     * @param accountId id of account
     * @return establishment of account
     */
    Establishment getEstablishmentByAccount(Integer accountId);

    /**
     * Gets establishment by id.
     *
     * @param id the id of establishment
     * @return establishment
     */
    Establishment getEstablishmentById(int id);

    /**
     * Calculate available.
     *
     * @param establishmentIds list establishment id.
     * @return list establishment id and status
     */
    List<Integer> calculateAvailable(List<Integer> establishmentIds);

    /**
     * Sets average time for establishment.
     *
     * @param establishment the establishment
     * @param timeInMinute  time in minute
     * @return true if updated successfully
     */
    boolean setAverageTime(Establishment establishment, int timeInMinute);

    /**
     * Get average time by establishment id.
     *
     * @param establishmentId the establishment id
     * @return the average time if it exist
     */
    Integer getAverageTimeById(int establishmentId);

    /**
     * Gets Ratting By medical record Id.
     *
     * @param medicalRecordId the id of medical record
     * @param establishmentId the id of establishment Id
     * @return EstablishmentRatingFullDTO
     */
    EstablishmentRatingFullDTO getRattingByMedicalRecordId(int medicalRecordId,int establishmentId);
}
