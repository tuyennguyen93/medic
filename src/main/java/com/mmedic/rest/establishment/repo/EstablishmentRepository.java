package com.mmedic.rest.establishment.repo;

import com.mmedic.entity.Establishment;
import com.mmedic.enums.EstablishmentType;
import com.mmedic.rest.account.dto.StaffDayOffDTO;
import com.mmedic.rest.account.dto.StaffWorkingScheduleDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EstablishmentRepository extends JpaRepository<Establishment, Integer> {

    Optional<Establishment> findById(int id);

    @Query("SELECT es FROM Establishment es " +
            "JOIN es.accounts accs " +
            "LEFT JOIN es.establishmentCertifications cer " +
            "WHERE accs.id = :accId ")
    Optional<Establishment> findEstablishmentByAccountId(@Param("accId") Integer accId);

    /**
     * Get establishment by id.
     *
     * @param establishmentId the establishment id
     * @return list establishment
     */
    @Query("SELECT es FROM Establishment es " +
            "JOIN es.accounts acc " +
            "LEFT JOIN es.establishmentCertifications cer " +
            "WHERE es.id = :establishmentId ")
    Optional<Establishment> findEstablishmentById(@Param("establishmentId") int establishmentId);

    @Query("SELECT establishment.id AS establishmentId, dayOff.startDate AS startDate, dayOff.endDate AS endDate " +
            "FROM Establishment establishment " +
            "INNER JOIN establishment.establishmentDayOffs dayOff " +
            "WHERE establishment.id IN (:ids) " +
            "AND dayOff.startDate < dayOff.endDate " +
            "AND dayOff.endDate >= :startDate " +
            "AND dayOff.startDate <= :endDate AND " +
            ":startDate <= :endDate")
    List<StaffDayOffDTO> findEstablishmentDayOff(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate, @Param("ids") int... establishmentIds);

    @Query("SELECT workingSchedule.id AS id, establishment.id AS establishmentId, workingSchedule.startTime AS startTime, workingSchedule.endTime AS endTime, workingSchedule.dayOfWeek AS day " +
            "FROM Establishment establishment " +
            "INNER JOIN establishment.establishmentWorkingSchedules workingSchedule " +
            "WHERE establishment.id IN (:ids) AND workingSchedule.isDeleted = 0 OR workingSchedule.isDeleted IS NULL")
    List<StaffWorkingScheduleDTO> findEstablishmentWorkingSchedule(@Param("ids") int... establishmentIds);

    /**
     * Find all warehouse by condition.
     *
     * @param condition the condition to search
     * @return list warehouse
     */
    @Query("SELECT es FROM Establishment es " +
            "WHERE es.establishmentType = 'DRUG_WAREHOUSE' " +
            "AND (es.isDisabled = 0 OR es.isDisabled IS NULL) " +
            "AND (es.name LIKE %:condition% " +
            "OR es.address LIKE %:condition%)")
    List<Establishment> findAllWarehouse(@Param("condition") String condition);

    /**
     * Get establishment by id and type.
     *
     * @param establishmentId   the establishment id
     * @param establishmentType the establishment type
     * @return establishment
     */
    @Query("SELECT es FROM Establishment es " +
            "WHERE es.establishmentType = :establishmentType " +
            "AND (es.isDisabled = 0 OR es.isDisabled IS NULL) " +
            "AND es.id = :establishmentId")
    Optional<Establishment> findByIdAndEstablishmentType(@Param("establishmentId") int establishmentId, @Param("establishmentType") EstablishmentType establishmentType);


    @Query("SELECT DISTINCT est FROM DrugExportDetail ded " +
            "LEFT JOIN ded.drugExport de " +
            "LEFT JOIN de.drugInternalExport die " +
            "LEFT JOIN die.exportTo est " +
            "LEFT JOIN ded.drugUnit du " +
            "LEFT JOIN du.drug dr " +
            "WHERE dr.id = :drugId " +
            "AND dr.isDisabled = 0 AND est IS NOT NULL AND est.name LIKE :nameEst ")
    List<Establishment> findDrugStoreByDrugId(@Param("drugId") int drugId, @Param("nameEst") String nameEst);

    @Query(value = "select avg(rate) from establishment_rating where establishment_id = :establishmentId", nativeQuery = true)
    float ratingAverage(@Param("establishmentId") Integer establishmentId);

    /**
     * Gets average time of establishment.
     *
     * @param establishmentId the establishment id
     * @return the average time if it exist
     */
    @Query("SELECT e.averageTime FROM Establishment e WHERE e.id = :id")
    Integer findEstablishmentAverageTimeById(@Param("id") int establishmentId);

    /**
     * Check Establishment is off.
     *
     * @param dateToCheck     date to check
     * @param establishmentId establishment id
     * @return number of days off of establishment is off on date
     */
    @Query("SELECT COUNT(dayOff.id) FROM Establishment establishment " +
            "INNER JOIN establishment.establishmentDayOffs dayOff " +
            "WHERE establishment.id = :id " +
            "AND dayOff.startDate < dayOff.endDate " +
            "AND dayOff.endDate >= :date " +
            "AND dayOff.startDate <= :date")
    Integer establishmentIsOff(@Param("date") LocalDateTime dateToCheck, @Param("id") int establishmentId);

    @Query(nativeQuery = true, value = "" +
            "     SELECT CASE WHEN  " +
            "       EXISTS(SELECT 1 FROM establishment e  " +
            "                       INNER JOIN medical_service_registry msr on e.id = msr.establishment_id  " +
            "                       LEFT JOIN establishment_working_schedule ews on e.id = ews.establishment_id  " +
            "               WHERE " +
            "e.id = :estId  " +
            "AND e.establishment_type = :estType " +
            "AND ( :ewsId IS NULL OR (  ews.id = :ewsId AND ( ews.is_deleted = 0 OR ews.is_deleted IS NULL ))) " +
            "AND ( :msrId IS NULL OR (  msr.medical_service_id = :msrId AND msr.active = 1))  " +
            "        )  > 0 THEN 'TRUE' ELSE 'FALSE' END AS isValid")
    Boolean isValidEstablishmentFollowConditions(@Param("estId") Integer estId,
                                                 @Param("estType") String estType,
                                                 @Param("ewsId") Integer ewsId,
                                                 @Param("msrId") Integer msrId);

}
