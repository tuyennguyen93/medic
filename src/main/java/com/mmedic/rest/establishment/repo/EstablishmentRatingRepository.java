package com.mmedic.rest.establishment.repo;

import com.mmedic.entity.EstablishmentRating;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface EstablishmentRatingRepository extends JpaRepository<EstablishmentRating, Integer> {

    Optional<EstablishmentRating> findByEstablishment_IdAndMedicalRecord_Id(int establishmentId, int medicalRecordId);

    Page<EstablishmentRating> findByEstablishment_Id(int establishmentId, Pageable pageable);

}
