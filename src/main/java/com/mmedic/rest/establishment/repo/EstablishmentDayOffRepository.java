package com.mmedic.rest.establishment.repo;

import com.mmedic.entity.EstablishmentDayOff;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface EstablishmentDayOffRepository extends JpaRepository<EstablishmentDayOff, Integer> {

    List<EstablishmentDayOff> findAllByEstablishment_IdAndEndDateGreaterThanEqual(int establishmentId, LocalDateTime now);
}
