package com.mmedic.rest.establishment.repo;

import com.mmedic.entity.EstablishmentWorkingSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface EstablishmentWorkingScheduleRepository extends JpaRepository<EstablishmentWorkingSchedule, Integer> {

    Optional<EstablishmentWorkingSchedule> findByIdAndIsDeletedFalse(int id);

    List<EstablishmentWorkingSchedule> findAllByEstablishment_IdAndIsDeletedFalse(int establishmentId);

    /**
     * Get list establishment working schedule by establishment id.
     *
     * @param establishmentId the establishment id.
     * @return list establishment working schedule
     */
    List<EstablishmentWorkingSchedule> findAllByEstablishment_IdAndIsDeletedFalseOrderById(int establishmentId);

    /**
     * Get list establishment working schedule by establishment id.
     *
     * @param establishmentId the establishment id.
     * @return list establishment working schedule
     */
    List<EstablishmentWorkingSchedule> findAllByEstablishment_IdOrderById(int establishmentId);

    /**
     * Get Establishment is available.
     *
     * @param establishmentIds the list establishment id
     * @param today            today
     * @return list establishment id is available
     */
    @Query(nativeQuery = true,
            value = "SELECT ews.establishment_id " +
                    "FROM establishment_working_schedule ews " +
                    "WHERE ews.establishment_id IN (:establishmentIds) " +
                    "  AND is_deleted = 0 " +
                    "  AND day_of_week = :today " +
                    "  AND start_time <= current_time " +
                    "  AND end_time >= current_time " +
                    "GROUP BY ews.establishment_id")
    List<Integer> findEstablishmentAvailable(@Param("establishmentIds") List<Integer> establishmentIds,
                                             @Param("today") String today);

    /**
     * Get record follow row index, Index start = 0.
     *
     * @param index index
     * @return record
     */
    @Query(nativeQuery = true, value = "SELECT * from establishment_working_schedule ews " +
            "WHERE ews.establishment_id = :estId " +
            "ORDER BY ews.id LIMIT :indexRow , 1;")
    Optional<EstablishmentWorkingSchedule> findEstablishmentWorkingByIndex(
            @Param("estId") int estId,
            @Param("indexRow") int index);

}
