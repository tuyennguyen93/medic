package com.mmedic.config;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmazonSNSConfiguration {

    @Value("${cloud.aws.region.static}")
    private String region;

    @Bean
    public AmazonSNS amzSNSProvider() {
        return AmazonSNSClientBuilder.standard().
                withRegion(Regions.fromName(region)).build();
    }
}
