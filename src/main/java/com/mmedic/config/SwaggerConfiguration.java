package com.mmedic.config;

import com.fasterxml.classmate.TypeResolver;
import com.mmedic.config.prop.SwaggerProperties;
import com.mmedic.rest.MedicResponse;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

/**
 * Swagger configuration.
 *
 * @author hungp
 */
@Configuration
@EnableSwagger2
@EnableConfigurationProperties(SwaggerProperties.class)
public class SwaggerConfiguration extends WebMvcConfigurerAdapter {

    private SwaggerProperties swaggerProperties;

    private TypeResolver resolver;

    public SwaggerConfiguration(SwaggerProperties swaggerProperties, TypeResolver resolver) {
        this.swaggerProperties = swaggerProperties;
        this.resolver = resolver;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).
                host(swaggerProperties.getHost()).
                protocols(Collections.singleton(swaggerProperties.getProtocol())).
                select().
                apis(RequestHandlerSelectors.basePackage("com.mmedic.rest")).
                paths(PathSelectors.any()).
                build().
//                pathProvider(new RelativePathProvider(null) {
//                    @Override
//                    public String getApplicationBasePath() {
//                        return "/";
//                    }
//                }).
                securityContexts(Collections.singletonList(securityContext())).
                securitySchemes(Collections.singletonList(apiKey())).
                genericModelSubstitutes(MedicResponse.class).
                alternateTypeRules(
                        newRule(resolver.resolve(MedicResponse.MedicBody.class,
                                resolver.resolve(MedicResponse.class, WildcardType.class)),
                                resolver.resolve(WildcardType.class))).
                apiInfo(apiInfo());
    }

    private ApiKey apiKey() {
        return new ApiKey("Bearer", HttpHeaders.AUTHORIZATION, "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.any()).
                forPaths(PathSelectors.ant("/api/**")).build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Collections.singletonList(new SecurityReference("Bearer", authorizationScopes));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().
                title(swaggerProperties.getTitle()).
                description(swaggerProperties.getDescription()).
                version(swaggerProperties.getVersion()).
                build();
    }
}
