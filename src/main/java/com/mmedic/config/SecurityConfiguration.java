package com.mmedic.config;

import com.mmedic.config.prop.ApplicationProperties;
import com.mmedic.config.prop.JwtProperties;
import com.mmedic.rest.account.AccountService;
import com.mmedic.security.OtpAuthenticationProvider;
import com.mmedic.security.SecurityProblemHandler;
import com.mmedic.security.jwt.JWTConfigurer;
import com.mmedic.security.jwt.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Security configuration.
 *
 * @author hungp
 */
@Configuration
@EnableWebSecurity
@EnableConfigurationProperties(JwtProperties.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };


    private final TokenProvider tokenProvider;

    private AccountService accountService;

    private ApplicationProperties applicationProperties;

    private SecurityProblemHandler securityProblemHandler;

    public SecurityConfiguration(AccountService accountService, ApplicationProperties applicationProperties,
                                 TokenProvider tokenProvider, SecurityProblemHandler securityProblemHandler) {
        this.accountService = accountService;
        this.applicationProperties = applicationProperties;
        this.tokenProvider = tokenProvider;
        this.securityProblemHandler = securityProblemHandler;
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Configuration authentication provider.
     *
     * @param auth Authentication manager builder
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        auth.
                authenticationProvider(usernamePasswordAuthenticationProvider()).
                authenticationProvider(preAuthenticatedAuthenticationProvider()).
                authenticationProvider(otpAuthenticationProvider());

    }

    @Bean
    public DaoAuthenticationProvider usernamePasswordAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder());
        provider.setUserDetailsService(accountService);
        return provider;
    }

    @Bean
    public PreAuthenticatedAuthenticationProvider preAuthenticatedAuthenticationProvider() {
        PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
        provider.setPreAuthenticatedUserDetailsService(accountService);
        return provider;
    }

    @Bean
    public OtpAuthenticationProvider otpAuthenticationProvider() {
        OtpAuthenticationProvider provider = new OtpAuthenticationProvider();
        provider.setUserDetailsService(accountService);
        provider.setTimeStep(applicationProperties.getTotp().getTimeLimitSeconds());
        provider.setDigit(applicationProperties.getTotp().getDigit());
        return provider;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(applicationProperties.getBcrypt().getStrength());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                csrf().disable().
                exceptionHandling().
                authenticationEntryPoint(securityProblemHandler).
                accessDeniedHandler(securityProblemHandler).
                and().
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                and().
                authorizeRequests().
                antMatchers("/", "/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**", "/csrf").permitAll().
                antMatchers("/api/v1/authentications/**").permitAll().
                antMatchers("/api/v1/accounts/reset-password").permitAll().
                antMatchers("/api/v1/accounts/verify-account").permitAll().
                antMatchers(HttpMethod.POST, "/api/v1/patients").permitAll().
                antMatchers(HttpMethod.GET, "/api/v1/patients",
                        "/api/v1/medical-staffs/basic-info",
                        "/api/v1/health-facilities/preclinicals/basic-info",
                        "/api/v1/health-facilities/drug-stores").permitAll().
                antMatchers("/api/v1/otp/**").permitAll().
                antMatchers("/open/**").permitAll().
                antMatchers("/api/v1/sys-settings/**").permitAll().
                antMatchers(HttpMethod.GET, "/api/v1/establishments/rating-app/**").permitAll().
                antMatchers(HttpMethod.GET, "/api/v1/medical-departments").permitAll().
                antMatchers("/api/v1/payment/callback").permitAll().
                antMatchers(HttpMethod.DELETE, "/api/v1/notifications/registration-token").permitAll().
                anyRequest().authenticated().
                and().
                httpBasic().
                and().
                apply(securityConfigurerAdapter());

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().
                antMatchers(HttpMethod.OPTIONS, "/**").
                antMatchers(AUTH_WHITELIST);
    }

    /**
     * Security configuration adapter.
     *
     * @return JWT Configurer
     */
    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/api/**", config);
        source.registerCorsConfiguration("/v2/api-docs", config);
        return new CorsFilter(source);
    }
}
