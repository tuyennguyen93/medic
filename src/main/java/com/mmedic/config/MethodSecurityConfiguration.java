package com.mmedic.config;

import com.mmedic.security.scope.ScopeMethodSecurityExpressionHandler;
import com.mmedic.security.scope.handler.AdminRecordReadOnlyHandler;
import com.mmedic.security.scope.handler.AdminRecordReadWriterHandler;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

/**
 * Method security configuration.
 *
 * @author hungp
 */
@ComponentScan
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfiguration extends GlobalMethodSecurityConfiguration {

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        ScopeMethodSecurityExpressionHandler expressionHandler = new ScopeMethodSecurityExpressionHandler();
        expressionHandler.addAnnotationHandler(new AdminRecordReadWriterHandler());
        expressionHandler.addAnnotationHandler(new AdminRecordReadOnlyHandler());
        return expressionHandler;
    }
}
