package com.mmedic.config;

import com.mmedic.security.AccountPrincipal;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class JpaAuditingConfiguration {

    @Bean
    public AuditorAware<Integer> auditorProvider() {
        return new AuditorAwareImpl();
    }

    public static class AuditorAwareImpl implements AuditorAware<Integer> {

        @Override
        public Optional<Integer> getCurrentAuditor() {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (null != auth && !String.class.isInstance(auth.getPrincipal())) {
                return Optional.ofNullable(auth).
                        filter(Authentication::isAuthenticated).
                        map(Authentication::getPrincipal).
                        map(AccountPrincipal.class::cast).
                        map(AccountPrincipal::getId);
            }
            return Optional.empty();
        }
    }
}
