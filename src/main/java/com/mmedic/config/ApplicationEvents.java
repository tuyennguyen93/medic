package com.mmedic.config;

import com.mmedic.config.prop.ApplicationProperties;
import com.mmedic.entity.Account;
import com.mmedic.enums.AuthType;
import com.mmedic.rest.account.dto.AccountDTO;
import com.mmedic.rest.account.AccountService;
import com.mmedic.rest.account.dto.AuthMethodDTO;
import com.mmedic.rest.role.dto.RoleDTO;
import com.mmedic.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

/**
 * Application Events.
 *
 * @author hungp
 */
@Slf4j
@Component
public class ApplicationEvents {

    private ApplicationProperties applicationProperties;

    private AccountService accountService;

    public ApplicationEvents(ApplicationProperties applicationProperties, AccountService accountService) {
        this.applicationProperties = applicationProperties;
        this.accountService = accountService;
    }

    /**
     * Once the app is ready, it will check for the existence of an Super Admin account,
     * if it doesn't exist, create a new one.
     *
     * @author hungp
     */
    @EventListener(ApplicationReadyEvent.class)
    public void onStartApp() {
        if (log.isDebugEnabled()) {
            log.debug("--------------------------------------------------\n" +
                    "- CHECK THE EXISTENCE OF THE SUPER ADMIN ACCOUNT -" +
                    "--------------------------------------------------\n");
        }
        try {
            String superAdminEmail = applicationProperties.getSupperAdmin().getEmail();
            if (!StringUtils.isEmpty(superAdminEmail)) {
                Optional<Account> optionalSAdminAccount = accountService.findAccountByEmail(superAdminEmail);

                optionalSAdminAccount.ifPresent(supperAdmin -> {
                    if (!supperAdmin.getRole().getName().equals(Constants.ROLE_SUPER_ADMIN)) {
                        if (log.isErrorEnabled()) {
                            log.error("This account has no role super admin");
                        }
                    } else if (supperAdmin.getAuthMethods().size() > 1) {
                        if (log.isErrorEnabled()) {
                            log.error("This super admin account has more than two authentication method");
                        }
                    }
                });

                if (!optionalSAdminAccount.isPresent()) {
                    // Get super admin role.
                    RoleDTO role = new RoleDTO();
                    role.setName(Constants.ROLE_SUPER_ADMIN);

                    // Init super admin account
                    AccountDTO supperAdmin = new AccountDTO();
                    supperAdmin.setName(applicationProperties.getSupperAdmin().getName());
                    supperAdmin.setDateOfBirth(LocalDate.now());
                    supperAdmin.setRole(role);

                    // Init Authentication method for super admin account (by email)
                    AuthMethodDTO supperAdminAuthMethod = new AuthMethodDTO();
                    supperAdminAuthMethod.setAuthType(AuthType.EMAIL_AND_PASSWORD);
                    supperAdminAuthMethod.setAuthData1(applicationProperties.getSupperAdmin().getEmail());
                    supperAdminAuthMethod.setAuthData2(applicationProperties.getSupperAdmin().getPassword());

                    supperAdmin.setAuthMethods(Collections.singletonList(supperAdminAuthMethod));

                    log.debug("\n----------------------------------\n" +
                            "- CREATE NEW SUPER ADMIN ACCOUNT -\n" +
                            "----------------------------------");
                    accountService.saveAdminAccount(supperAdmin);
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }
}
