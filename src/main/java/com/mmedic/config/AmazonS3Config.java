package com.mmedic.config;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AmazonS3Config {

    @Value("${cloud.aws.region.static}")
    private String s3Region;

    @Bean
    public AmazonS3 s3client() {
        return AmazonS3ClientBuilder.standard().
                withRegion(this.s3Region).build();
    }
}
