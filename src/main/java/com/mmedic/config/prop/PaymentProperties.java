package com.mmedic.config.prop;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "payment", ignoreUnknownFields = false)
public class PaymentProperties {

    private ZaloPay zalopay = new ZaloPay();

    @Getter
    @Setter
    public static class ZaloPay {
        private int appId;

        private String key1;

        private String key2;

        private String endpoint;
    }
}
