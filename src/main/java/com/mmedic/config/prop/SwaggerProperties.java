package com.mmedic.config.prop;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Swagger properties.
 *
 * @author hungp
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "swagger", ignoreUnknownFields = false)
public class SwaggerProperties {

    private String title;

    private String description;

    private String version;

    private String host;

    private String protocol;
}
