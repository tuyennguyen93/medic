package com.mmedic.config.prop;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Jwt properties.
 *
 * @author hungp
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "jwt", ignoreUnknownFields = false)
public class JwtProperties {

    private String signingKey = "bW1lZGljLWFwcC1zZWN1cml0eS1qd3Qta2V5OmpFNE9ESTRZelJpTmpaa09UUmhOVFUzWW1Oa01XUm1NV1l4TXprellqQXo=";

    private int accessTokenValiditySeconds = 3600;

    private int refreshTokenValiditySeconds = 86400;

    private String prefix = "";
}
