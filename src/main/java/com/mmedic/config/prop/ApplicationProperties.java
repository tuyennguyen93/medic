package com.mmedic.config.prop;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * Application Configuration Properties.
 *
 * @author hungp
 */
@Getter
@Setter
@Validated
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private String webUrl;

    @Valid
    private SupperAdmin supperAdmin = new SupperAdmin();

    private BCrypt bcrypt = new BCrypt();

    private Totp totp = new Totp();

    private ResetPassword resetPassword = new ResetPassword();

    private VerifyAccount verifyAccount = new VerifyAccount();

    private Firebase firebase = new Firebase();

    /**
     * Super Admin information.
     */
    @Getter
    @Setter
    public static class SupperAdmin {

        private String name = "Super Admin";

        @NotBlank
        private String email = "superadmin@mmedic.com";

        @NotBlank
        private String password = "123@abc";
    }

    /**
     * BCrypt information.
     */
    @Getter
    @Setter
    public static class BCrypt {

        private int strength = 12;
    }

    @Getter
    @Setter
    public static class Totp {

        private long timeLimitSeconds;

        private int digit;
    }

    @Getter
    @Setter
    public static class ResetPassword {

        private long timeLimitSeconds;

        private String link;
    }

    @Getter
    @Setter
    public static class VerifyAccount {

        private long timeLimitSeconds;

        private String link;
    }

    @Getter
    @Setter
    public static class Firebase {

        private String databaseUrl;

    }
}
