package com.mmedic.enums;

public enum DeleteStatus {

    AVAILABLE(Boolean.FALSE),

    IS_DELETED(Boolean.TRUE);

    DeleteStatus(Boolean val) {
        this.value = val;
    }

    private Boolean value;

    public Boolean getValue() {
        return value;
    }
}
