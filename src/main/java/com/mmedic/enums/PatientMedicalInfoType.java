package com.mmedic.enums;

/**
 * InfoKey in json
 */
public enum PatientMedicalInfoType {

    BLOOD_TYPE("bloodType"),

    WEIGHT("weight"),

    HEIGHT("height"),

    ALLERGY_FOOD("allergyFoods"),

    MEDICAL_HISTORY("medicalHistory");

    private String code;

    PatientMedicalInfoType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
