package com.mmedic.enums;

public enum MedicalServiceGroupType {

     TREATMENT(1),
     ANALYSIS (2),
     DIAGNOSE(3),
     MEDICINE_SUPPLY(4);

    private Integer id;

    MedicalServiceGroupType(Integer id){
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }
}
