package com.mmedic.enums;

public enum ExportType {

    INTERNAL("INTERNAL"),
    PATIENT("PATIENT");

    private String code;

    ExportType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
