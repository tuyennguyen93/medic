package com.mmedic.enums;

/**
 * Establishment Type enum.
 *
 * @author hungp
 */
public enum EstablishmentType {
    MEDICAL_TREATMENT("MEDICAL_TREATMENT"),
    PRECLINICAL("PRECLINICAL"),
    DRUG_STORE("DRUG_STORE"),
    DRUG_WAREHOUSE("DRUG_WAREHOUSE");

    private String code;

    EstablishmentType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
