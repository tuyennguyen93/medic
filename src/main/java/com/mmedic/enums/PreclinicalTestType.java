package com.mmedic.enums;

public enum PreclinicalTestType {
    HOME("HOME"),
    CLINIC("CLINIC"),
    PRECLINICAL("PRECLINICAL");

    private String code;

    PreclinicalTestType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
