package com.mmedic.enums;

/**
 * Medical Service Order Status enum.
 *
 * @author hungp
 */
public enum MedicalServiceOrderStatus {
    REQUEST_SENT("REQUEST_SENT"),
    REQUEST_CANCELLED("REQUEST_CANCELLED"),
    REQUEST_DENIED("REQUEST_DENIED"),
    REQUEST_ACCEPTED("REQUEST_ACCEPTED"),
    REQUEST_PROCESSING("REQUEST_PROCESSING"),
    REQUEST_FULFILLED("REQUEST_FULFILLED");

    private String code;

    MedicalServiceOrderStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
