package com.mmedic.enums;

public enum PrescriptionSupplyType {
    HOME("HOME"),
    DRUG_STORE("DRUG_STORE");

    private String code;

    PrescriptionSupplyType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
