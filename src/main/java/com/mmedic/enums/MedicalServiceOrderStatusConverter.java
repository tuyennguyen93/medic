package com.mmedic.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

/**
 * Converter for enum MedicalServiceOrderStatus.
 *
 * @author hungp
 */
@Converter(autoApply = true)
public class MedicalServiceOrderStatusConverter implements AttributeConverter<MedicalServiceOrderStatus, String> {
    @Override
    public String convertToDatabaseColumn(MedicalServiceOrderStatus attribute) {
        if (null == attribute) {
            return null;
        }
        return attribute.getCode();
    }

    @Override
    public MedicalServiceOrderStatus convertToEntityAttribute(String dbData) {
        if (null == dbData) {
            return null;
        }
        return Stream.of((MedicalServiceOrderStatus.values())).
                filter(status -> status.getCode().equals(dbData)).
                findFirst().
                orElseThrow(IllegalArgumentException::new);
    }
}
