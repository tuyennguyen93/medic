package com.mmedic.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

/**
 * Converter for enum EstablishmentType.
 *
 * @author hungp
 */
@Converter(autoApply = true)
public class EstablishmentTypeConverter implements AttributeConverter<EstablishmentType, String> {

    @Override
    public String convertToDatabaseColumn(EstablishmentType attribute) {
        if (null == attribute) {
            return null;
        }
        return attribute.getCode();
    }

    @Override
    public EstablishmentType convertToEntityAttribute(String dbData) {
        if (null == dbData) {
            return null;
        }
        return Stream.of(EstablishmentType.values()).
                filter(establishmentType -> establishmentType.getCode().equals(dbData)).
                findFirst().
                orElseThrow(IllegalArgumentException::new);
    }
}
