package com.mmedic.enums;

/**
 * Treatment Request Status enum.
 *
 * @author toantc
 */
public enum TreatmentRequestStatus {
    WAITING_TREAT("WAITING_TREAT"),

    WAITING_TEST("WAITING_TEST"),

    WAITING_DIAGNOSE("WAITING_DIAGNOSE"),

    COMPLETE("COMPLETE"),

    DENIED("DENIED");

    private String code;

    TreatmentRequestStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
