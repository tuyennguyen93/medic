package com.mmedic.enums;

public enum TreatmentType {
    HOME("H"),
    CLINIC("C");

    private String code;

    TreatmentType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
