package com.mmedic.enums;

import javax.persistence.AttributeConverter;
import java.util.stream.Stream;

public class TreatmentTypeConverter implements AttributeConverter<TreatmentType, String> {
    @Override
    public String convertToDatabaseColumn(TreatmentType attribute) {
        if (null == attribute) {
            return null;
        }
        return attribute.getCode();
    }

    @Override
    public TreatmentType convertToEntityAttribute(String dbData) {
        if (null == dbData) {
            return null;
        }
        return Stream.of((TreatmentType.values())).
                filter(status -> status.getCode().equals(dbData)).
                findFirst().
                orElseThrow(IllegalArgumentException::new);
    }
}
