package com.mmedic.enums;

/**
 * Authentication Type enum.
 *
 * @author hungp
 */
public enum AuthType {
    EMAIL_AND_PASSWORD("1"),
    PHONE_NUMBER("2");

    private String code;

    AuthType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
