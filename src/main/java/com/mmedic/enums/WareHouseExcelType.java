package com.mmedic.enums;

public enum  WareHouseExcelType {

    EXPORT("EXPORT"),

    IMPORT("IMPORT");

    private String code;

    WareHouseExcelType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
