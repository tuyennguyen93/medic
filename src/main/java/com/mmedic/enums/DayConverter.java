package com.mmedic.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

/**
 * Converter for enum Day.
 *
 * @author hungp
 */
@Converter(autoApply = true)
public class DayConverter implements AttributeConverter<Day, String> {
    @Override
    public String convertToDatabaseColumn(Day attribute) {
        if (null == attribute) {
            return null;
        }
        return attribute.getCode();
    }

    @Override
    public Day convertToEntityAttribute(String dbData) {
        if (null == dbData) {
            return null;
        }
        return Stream.of(Day.values()).
                filter(day -> day.getCode().equals(dbData)).
                findFirst().
                orElseThrow(IllegalArgumentException::new);
    }
}
