package com.mmedic.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

/**
 * Converter for enum MedicalRecordType.
 *
 * @author hungp
 */
@Converter(autoApply = true)
public class MedicalRecordTypeConverter implements AttributeConverter<MedicalRecordType, String> {

    @Override
    public String convertToDatabaseColumn(MedicalRecordType attribute) {
        if (null == attribute) {
            return null;
        }
        return attribute.getCode();
    }

    @Override
    public MedicalRecordType convertToEntityAttribute(String dbData) {
        if (null == dbData) {
            return null;
        }
        return Stream.of(MedicalRecordType.values()).
                filter(medicalRecordType -> medicalRecordType.getCode().equals(dbData)).
                findFirst().
                orElseThrow(IllegalArgumentException::new);
    }
}
