package com.mmedic.enums;

public enum ImportType {

    INTERNAL("INTERNAL"),
    EXTERNAL("EXTERNAL");

    private String code;

    ImportType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
