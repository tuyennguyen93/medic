package com.mmedic.enums;

/**
 * Medical Record Type enum.
 *
 * @author hungp
 */
public enum MedicalRecordType {
    TREATMENT("TREATMENT"),
    SERVICE("SERVICE");

    private String code;

    MedicalRecordType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
