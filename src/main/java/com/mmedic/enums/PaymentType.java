package com.mmedic.enums;

public enum PaymentType {

    TREATMENT("TREATMENT"),
    PRECLINICAL("PRECLINICAL"),
    PRESCRIPTION("PRESCRIPTION"),
    HEALTHCARE("HEALTHCARE");

    private String code;

    PaymentType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
