package com.mmedic.spring.errors;

import com.mmedic.rest.MedicResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Exception Controller.
 *
 * @author hungp
 */
@Slf4j
@RestControllerAdvice
public class RestExceptionHandler {

    /**
     * Handler Authentication Exception.
     *
     * @param e authentication exception
     * @return error response with code 401
     */
    @ExceptionHandler({AuthenticationException.class})
    public MedicResponse<APIError> handleAuthenticationException(AuthenticationException e) {
        log.error("401 Status Code", e);
        APIError error = APIError.builder().
                messageCode(MedicException.ERROR_UNAUTHORIZED).
                debugMessage(e.getMessage()).
                build();
        return MedicResponse.errorStatus(error);
    }

    /**
     * Handler Access Denied Exception.
     *
     * @param e access denied exception
     * @return error response with code 403
     */
    @ExceptionHandler({AccessDeniedException.class})
    public MedicResponse<APIError> handleAccessDeniedException(AccessDeniedException e) {
        log.error("403 Status Code", e);
        APIError error = APIError.builder().
                messageCode(MedicException.ERROR_ACCESS_DENIED).
                debugMessage(e.getMessage()).
                build();
        return MedicResponse.errorStatus(error);
    }

    @ExceptionHandler({BadCredentialsException.class,
            InternalAuthenticationServiceException.class})
    public MedicResponse<APIError> handleBadCredentialException(Exception e) {
        log.error(e.getMessage(), e);
        APIError error = APIError.builder().
                messageCode(MedicException.ERROR_BAD_CREDENTIAL).
                debugMessage(e.getMessage()).
                build();
        return MedicResponse.errorStatus(error);
    }

    /**
     * Handle Medic Exception.
     *
     * @param e the medic exception
     * @return error response with status code is 400
     */
    @ExceptionHandler({MedicException.class})
    public MedicResponse<APIError> handleMedicException(MedicException e) {
        log.error("Status Code " + e.messageCode, e);
        APIError error = APIError.builder().
                messageCode(e.getMessageCode()).
                debugMessage(e.getMessage()).
                build();
        return MedicResponse.errorStatus(error);
    }

    /**
     * Handle disabled exception.
     *
     * @param e disable exception
     * @return error response
     */
    @ExceptionHandler(DisabledException.class)
    public MedicResponse<APIError> handleAccountDisableException(DisabledException e) {
        log.error(e.getMessage(), e);
        APIError error = APIError.builder().
                messageCode(MedicException.ERROR_ACCOUNT_IS_DISABLED).
                debugMessage(e.getMessage()).
                build();
        return MedicResponse.errorStatus(error);
    }

    /**
     * Handler unknown exception.
     *
     * @param e exception
     * @return error response
     */
    @ExceptionHandler(Exception.class)
    public MedicResponse<APIError> handleException(Exception e) {
        log.error(e.getMessage(), e);
        APIError error = APIError.builder().
                messageCode(MedicException.ERROR_UNKNOWN).
                debugMessage(!StringUtils.isEmpty(e.getMessage()) ? e.getMessage() : "Read log to get more details").
                build();
        return MedicResponse.errorStatus(error);
    }
}
