package com.mmedic.batch.zalopayUpdateStatus.service;

import com.mmedic.batch.zalopayUpdateStatus.dto.PayProcessDTO;
import com.mmedic.batch.zalopayUpdateStatus.repo.PayProcessRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PayProcessService {

    private final PayProcessRepository payProcessRepository;

    /**
     * Find all order is paying but not update status.
     *
     * @return order have payment that is processing
     */
    public List<PayProcessDTO> findOrderPayProcessing() {
        return payProcessRepository.findOrderPayProcessing();
    }
}
