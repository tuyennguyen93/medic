package com.mmedic.batch.zalopayUpdateStatus;

import com.mmedic.batch.zalopayUpdateStatus.dto.PaymentResultDTO;
import com.mmedic.rest.medicalRecord.MedicalRecordService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class PaymentWriter implements ItemWriter<PaymentResultDTO> {

    private final MedicalRecordService medicalRecordService;

    @Override
    public void write(List<? extends PaymentResultDTO> items) throws Exception {
        if (!CollectionUtils.isEmpty(items)) {
            medicalRecordService.batchUpdateOrderStatus(items);
        }
    }
}
