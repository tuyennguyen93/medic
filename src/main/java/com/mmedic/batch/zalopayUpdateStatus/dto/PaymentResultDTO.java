package com.mmedic.batch.zalopayUpdateStatus.dto;

import com.mmedic.enums.PaymentStatus;
import com.mmedic.service.payment.zalopay.dto.PaymentStatusResultDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PaymentResultDTO {

    private int medicalRecordId;

    private int orderId;

    private PaymentStatus paymentStatus;

    private String paymentReference;

    private LocalDateTime paymentDate;

    private int amount;

    private PaymentStatusResultDTO result;
}
