package com.mmedic.batch.zalopayUpdateStatus.dto;

import com.mmedic.enums.PaymentStatus;

public interface PayProcessDTO {

    int getMedicalRecordId();

    int getOrderId();

    PaymentStatus getPaymentStatus();

    String getPaymentReference();
}
