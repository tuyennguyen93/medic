package com.mmedic.batch.zalopayUpdateStatus.repo;

import com.mmedic.batch.zalopayUpdateStatus.dto.PayProcessDTO;
import com.mmedic.entity.MedicalServiceOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PayProcessRepository extends JpaRepository<MedicalServiceOrder, Integer> {

    /**
     * Find all order is paying but not update status.
     *
     * @return order have payment that is processing
     */
    @Query(nativeQuery = true,
            value = "SELECT mr.id                 AS medicalRecordId, " +
                    "       mso.id                AS orderId, " +
                    "       mso.payment_status    AS paymentStatus, " +
                    "       mso.payment_reference AS paymentReference " +
                    "FROM medical_record mr " +
                    "         LEFT JOIN examine_record er ON mr.id = er.medical_record_id " +
                    "         LEFT JOIN medical_service_order mso ON er.medical_service_order_id = mso.id " +
                    "WHERE mso.payment_status = 'PENDING' " +
                    "  AND mso.payment_reference IS NOT NULL " +
                    "UNION " +
                    "SELECT mr.id                 AS medicalRecordId, " +
                    "       mso.id                AS orderId, " +
                    "       mso.payment_status    AS paymentStatus, " +
                    "       mso.payment_reference AS paymentReference " +
                    "FROM medical_record mr " +
                    "         LEFT JOIN preclinical_record pr ON mr.id = pr.medical_record_id " +
                    "         LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "WHERE mso.payment_status = 'PENDING' " +
                    "  AND mso.payment_reference IS NOT NULL " +
                    "UNION " +
                    "SELECT mr.id                 AS medicalRecordId, " +
                    "       mso.id                AS orderId, " +
                    "       mso.payment_status    AS paymentStatus, " +
                    "       mso.payment_reference AS paymentReference " +
                    "FROM medical_record mr " +
                    "         LEFT JOIN prescription_record pr ON mr.id = pr.medical_record_id " +
                    "         LEFT JOIN medical_service_order mso ON pr.medical_service_order_id = mso.id " +
                    "WHERE mso.payment_status = 'PENDING' " +
                    "  AND mso.payment_reference IS NOT NULL " +
                    "UNION " +
                    "SELECT mr.id                 AS medicalRecordId, " +
                    "       mso.id                AS orderId, " +
                    "       mso.payment_status    AS paymentStatus, " +
                    "       mso.payment_reference AS paymentReference " +
                    "FROM medical_record mr " +
                    "         LEFT JOIN service_record sr ON mr.id = sr.medical_record_id " +
                    "         LEFT JOIN medical_service_order mso ON sr.medical_service_order_id = mso.id " +
                    "WHERE mso.payment_status = 'PENDING' " +
                    "  AND mso.payment_reference IS NOT NULL")
    List<PayProcessDTO> findOrderPayProcessing();
}
