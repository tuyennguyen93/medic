package com.mmedic.batch.zalopayUpdateStatus;

import com.mmedic.batch.zalopayUpdateStatus.dto.PayProcessDTO;
import com.mmedic.batch.zalopayUpdateStatus.dto.PaymentResultDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Batch to update status payment of zalopay. (In case zalo pay can not callback)
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class ZaloPayUpdateStatusManualBatch {

    private static final String BATCH_NAME = "zalopay_update_status_manual";

    private static final int CHUNK_SIZE = 10;

    public final JobBuilderFactory jobBuilderFactory;

    public final StepBuilderFactory stepBuilderFactory;

    private final SimpleJobLauncher jobLauncher;

    private final PaymentReader paymentReader;

    private final PaymentProcessor paymentProcessor;

    private final PaymentWriter paymentWriter;

    @Scheduled(cron = "${batch.cron-expression.zalopay-update-status}")
    public void updateZaloPayStatus() {
        try {
            JobParameters param = new JobParametersBuilder().
                    addString("jobName", BATCH_NAME).
                    addLong("jobID", System.currentTimeMillis()).
                    toJobParameters();

            JobExecution execution = jobLauncher.run(job(), param);
        } catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException | JobParametersInvalidException e) {
            log.error(e.getMessage(), e);
        }
    }

    public Job job() {
        return jobBuilderFactory.get(BATCH_NAME)
                .incrementer(new RunIdIncrementer())
                .flow(flow()).end()
                .build();
    }

    public Step flow() {
        return stepBuilderFactory.get("createUserNotification")
                .<PayProcessDTO, PaymentResultDTO>chunk(CHUNK_SIZE)
                .reader(paymentReader)
                .processor(paymentProcessor)
                .writer(paymentWriter)
                .build();
    }
}
