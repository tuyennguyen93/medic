package com.mmedic.batch.zalopayUpdateStatus;

import com.mmedic.batch.zalopayUpdateStatus.dto.PayProcessDTO;
import com.mmedic.batch.zalopayUpdateStatus.service.PayProcessService;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.support.IteratorItemReader;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Order reader.
 */
@Component
@RequiredArgsConstructor
public class PaymentReader implements ItemReader<PayProcessDTO> {

    private final PayProcessService payProcessService;

    private ItemReader<PayProcessDTO> delegate;

    @Override
    public PayProcessDTO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if (delegate == null) {
            List<PayProcessDTO> notifications = payProcessService.findOrderPayProcessing();
            if (notifications != null) {
                delegate = new IteratorItemReader<PayProcessDTO>(notifications);
            }
        }
        PayProcessDTO order = delegate.read();
        if (order == null) {
            delegate = null;
        }
        return order;
    }
}
