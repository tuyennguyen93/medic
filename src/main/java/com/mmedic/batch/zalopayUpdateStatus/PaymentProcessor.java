package com.mmedic.batch.zalopayUpdateStatus;

import com.mmedic.batch.zalopayUpdateStatus.dto.PayProcessDTO;
import com.mmedic.batch.zalopayUpdateStatus.dto.PaymentResultDTO;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.service.payment.zalopay.OrderStatus;
import com.mmedic.service.payment.zalopay.dto.PaymentStatusResultDTO;
import com.mmedic.utils.Constants;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class PaymentProcessor implements ItemProcessor<PayProcessDTO, PaymentResultDTO> {

    private final OrderStatus orderStatus;

    @Override
    public PaymentResultDTO process(PayProcessDTO item) throws Exception {
        PaymentResultDTO result = null;
        if (!Objects.isNull(item)) {
            JSONObject paymentRef = new JSONObject(item.getPaymentReference());
            String appTransId = paymentRef.getString(Constants.ZALOPAY_TRANSACTION);
            PaymentStatusResultDTO status = orderStatus.get(appTransId);
            if (null != status) {
                long timeLength = 0;
                if (status.getAmount() > 0) {
                    LocalDateTime createTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(status.getApptime()), ZoneId.systemDefault());
                    timeLength = ChronoUnit.MINUTES.between(createTime, LocalDateTime.now());
                }

                if (!status.isIsprocessing() &&
                        !(status.getApptime() > 0 && timeLength <= 20 && (status.getReturncode() == -49 || status.getReturncode() == -117))) {
                    result = new PaymentResultDTO();
                    result.setMedicalRecordId(item.getMedicalRecordId());
                    result.setOrderId(item.getOrderId());
                    result.setPaymentReference(item.getPaymentReference());
                    if (null != status.getAmount()) {
                        result.setAmount(Integer.parseInt(status.getAmount().toString()));
                    }
                    if (status.getReturncode() == 1) {
                        result.setPaymentStatus(PaymentStatus.SUCCESS);
                        result.setPaymentDate(LocalDateTime.now());
                    } else {
                        result.setPaymentStatus(PaymentStatus.FAILED);
                    }
                    result.setResult(status);
                }
            }
        }
        return result;
    }
}
