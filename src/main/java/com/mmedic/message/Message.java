package com.mmedic.message;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Message bundle.
 *
 * @author hungp
 */
@Component
public class Message {

    private MessageSource messageSource;

    private Map<String, Locale> mapLocale;

    public Message(MessageSource messageSource) {
        this.messageSource = messageSource;
        this.mapLocale = new HashMap<>();

        // init locale
        init();
    }

    /**
     * Init map locale
     */
    private void init() {
        mapLocale.put("vi", new Locale("vi", "VN"));
        mapLocale.put("en", Locale.US);
    }

    /**
     * Get message.
     *
     * @param code   the key of message
     * @param locale the locale
     * @return message value
     */
    public String getMessage(String code, String locale) {
//        return messageSource.getMessage(code, null, LocaleContextHolder.getLocale());
        // Get message in vietnamese for temporary
        Locale loc = getLocale(locale);
        return messageSource.getMessage(code, null, loc);
    }

    /**
     * Get message with param.
     *
     * @param code   the key of message
     * @param args   the params
     * @param locale the locale
     * @return message value
     */
    public String getMessage(String code, Object[] args, String locale) {
//        return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
        Locale loc = getLocale(locale);
        return messageSource.getMessage(code, args, loc);
    }

    /**
     * Get message with param and custom locale.
     *
     * @param code   the key of message
     * @param args   the params
     * @param locale the locale
     * @return message value
     */
    public String getMessage(String code, Object[] args, Locale locale) {
        return messageSource.getMessage(code, args, locale);
    }

    /**
     * Get locale.
     *
     * @param locale the locale value
     * @return the locale
     */
    private Locale getLocale(String locale) {
        Locale loc = mapLocale.get(locale);
        if (null == loc) {
            loc = mapLocale.get("vi");
        }
        return loc;
    }
}
