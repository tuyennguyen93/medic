package com.mmedic.service.payment.zalopay.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PaymentOrderResultDTO {

    private Integer returncode;

    private String returnmessage;

    private String orderurl;

    private String zptranstoken;
}
