package com.mmedic.service.payment.zalopay;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmedic.config.prop.PaymentProperties;
import com.mmedic.service.payment.zalopay.dto.PaymentOrderDTO;
import com.mmedic.service.payment.zalopay.dto.PaymentOrderResultDTO;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.crypto.HMACUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class CreateOrder {

    private static String CREATE_ORDER_API = "/tpe/createorder";
    private static String BANK_CODE = "zalopayapp";
    private static String DATE_FORMAT_DATE = "yyMMdd";
    private static String DATE_FORMAT_TIME = "HHmmss";

    private final PaymentProperties paymentProperties;

    /**
     * Get string formatted of time.
     *
     * @param format the format
     * @return string
     */
    private String getCurrentTimeString(String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.now().format(formatter);
    }

    /**
     * Create zalopay order.
     *
     * @param paymentOrder data of payment order
     * @return result of order
     * @throws IOException
     */
    public PaymentOrderResultDTO create(PaymentOrderDTO paymentOrder) {
        String endpoint = paymentProperties.getZalopay().getEndpoint() + CREATE_ORDER_API;

        Map<String, Object> order = new HashMap<String, Object>() {{
            put("appid", paymentProperties.getZalopay().getAppId());
            put("apptransid", paymentOrder.getTransaction());
            put("apptime", System.currentTimeMillis());
            put("appuser", paymentOrder.getPatientId());
            put("amount", paymentOrder.getTotalAmount());
            put("description", paymentOrder.getDescription());
            put("bankcode", BANK_CODE);
            put("item", new JSONArray(paymentOrder.getItem()).toString());
            put("embeddata", new JSONObject(paymentOrder.getEmbedData()).toString());
        }};

        // appid +”|”+ apptransid +”|”+ appuser +”|”+ amount +"|" + apptime +”|”+ embeddata +"|" +item
        String data = order.get("appid") +
                "|" + order.get("apptransid") +
                "|" + order.get("appuser") +
                "|" + order.get("amount") +
                "|" + order.get("apptime") +
                "|" + order.get("embeddata") +
                "|" + order.get("item");
        order.put("mac", HMACUtil.HMacHexStringEncode(HMACUtil.HMACSHA256, paymentProperties.getZalopay().getKey1(), data));

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(endpoint);

        List<NameValuePair> params = new ArrayList<>();
        for (Map.Entry<String, Object> field : order.entrySet()) {
            params.add(new BasicNameValuePair(field.getKey(), field.getValue().toString()));
        }

        PaymentOrderResultDTO result;
        try {
            // Content-Type: application/x-www-form-urlencoded
            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

            CloseableHttpResponse res = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(res.getEntity().getContent()));
            StringBuilder resultJsonStr = new StringBuilder();
            String line;

            while ((line = rd.readLine()) != null) {
                resultJsonStr.append(line);
            }

            ObjectMapper mapper = new ObjectMapper();
            result = mapper.readValue(resultJsonStr.toString(), PaymentOrderResultDTO.class);
        } catch (IOException e) {
            throw new MedicException(MedicException.ERROR_CREATE_PAYMENT_ORDER, "Can't create payment order");
        }
        return result;
    }

    /**
     * Get default embed data.
     *
     * @return default embed data.
     */
    public Map<String, String> getDefaultEmbedData() {
        Map<String, String> embedData = new HashMap<>();
        embedData.put("merchantinfo", "MMedic");
        return embedData;
    }

    /**
     * Generate transaction.
     *
     * @param orderId the service order id
     * @return transaction
     */
    public String generateTransaction(int orderId) {
        return getCurrentTimeString(DATE_FORMAT_DATE) + Constants.UNDERSCORE + getCurrentTimeString(DATE_FORMAT_TIME) + orderId;
    }

}
