package com.mmedic.service.payment.zalopay;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmedic.config.prop.PaymentProperties;
import com.mmedic.service.payment.zalopay.dto.PaymentStatusResultDTO;
import com.mmedic.utils.crypto.HMACUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
@RequiredArgsConstructor
public class OrderStatus {

    private static String GET_ORDER_STATUS_API = "/tpe/getstatusbyapptransid";

    private final PaymentProperties paymentProperties;

    public PaymentStatusResultDTO get(String appTransId) {
        PaymentStatusResultDTO result = null;
        try {
            String endpoint = paymentProperties.getZalopay().getEndpoint() + GET_ORDER_STATUS_API;
            String data = paymentProperties.getZalopay().getAppId() +
                    "|" + appTransId +
                    "|" + paymentProperties.getZalopay().getKey1(); // appid|apptransid|key1
            String mac = HMACUtil.HMacHexStringEncode(HMACUtil.HMACSHA256, paymentProperties.getZalopay().getKey1(), data);

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("appid", String.valueOf(paymentProperties.getZalopay().getAppId())));
            params.add(new BasicNameValuePair("apptransid", appTransId));
            params.add(new BasicNameValuePair("mac", mac));

            URIBuilder uri = new URIBuilder(endpoint);
            uri.addParameters(params);

            CloseableHttpClient client = HttpClients.createDefault();
            HttpGet get = new HttpGet(uri.build());

            CloseableHttpResponse res = client.execute(get);
            BufferedReader rd = new BufferedReader(new InputStreamReader(res.getEntity().getContent()));
            StringBuilder resultJsonStr = new StringBuilder();
            String line;

            while ((line = rd.readLine()) != null) {
                resultJsonStr.append(line);
            }

            ObjectMapper mapper = new ObjectMapper();
            result = mapper.readValue(resultJsonStr.toString(), PaymentStatusResultDTO.class);
            result.setJsonText(resultJsonStr.toString());

        } catch (URISyntaxException | IOException e) {
            log.error(e.getMessage(), e);
        }

        return result;
    }
}
