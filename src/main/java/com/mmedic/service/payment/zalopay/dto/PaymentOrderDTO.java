package com.mmedic.service.payment.zalopay.dto;

import com.mmedic.enums.PaymentType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Map;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class PaymentOrderDTO {

    private PaymentType paymentType;

    private Map[] item;

    private Map<String, String> embedData;

    private int orderId;

    private int patientId;

    private int totalAmount;

    private String transaction;

    private String description;
}
