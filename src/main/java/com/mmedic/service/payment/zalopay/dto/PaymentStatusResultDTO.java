package com.mmedic.service.payment.zalopay.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentStatusResultDTO {

    private Integer returncode;

    private String returnmessage;

    private boolean isprocessing;

    private Long amount;

    private Long zptransid;

    private Long discountamount;

    private long apptime;

    private String jsonText;
}
