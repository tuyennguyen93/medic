package com.mmedic.service.file;

import com.mmedic.enums.WareHouseExcelType;
import com.mmedic.rest.drugstorage.dto.ExcelTemplateTitleDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStorageService {

    ExcelTemplateTitleDTO getExcelTemplateTitleDTO(String locale, WareHouseExcelType excelType);

    String generateWareHouseTemplateFilePath(String pathDirectory,
                                                    String fileName,
                                                    byte[] excelData) throws IOException;

    byte[] generateImportTemplate (ExcelTemplateTitleDTO excelTitle);

    byte[] generateExportTemplate (ExcelTemplateTitleDTO excelTitle);

    String storeFile(MultipartFile file);

    Object readExcelFile (String filePath, WareHouseExcelType excelType);
}
