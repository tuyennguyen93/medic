package com.mmedic.service.file;

import com.mmedic.enums.WareHouseExcelType;
import com.mmedic.message.Message;
import com.mmedic.rest.drugstorage.dto.*;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Helper;
import com.mmedic.utils.excel.XlsxBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RequiredArgsConstructor
@Service
public class FileStorageServcieImpl implements FileStorageService{

    private final Message message;

    @Override
    public ExcelTemplateTitleDTO getExcelTemplateTitleDTO(String locale, WareHouseExcelType excelType) {
        ExcelTemplateTitleDTO titleDTO = new ExcelTemplateTitleDTO();

        if (excelType.equals(WareHouseExcelType.IMPORT)){
            titleDTO.setSheetName(message.getMessage("storage.template.import.sheet-name",locale));
            titleDTO.setTitle(message.getMessage("storage.template.import.title",locale));
            titleDTO.setToName(message.getMessage("storage.template.import.to-name",locale));
            titleDTO.setToId(message.getMessage("storage.template.import.to-id",locale));
        }
        else {
            titleDTO.setSheetName(message.getMessage("storage.template.export.sheet-name",locale));
            titleDTO.setTitle(message.getMessage("storage.template.export.title",locale));
            titleDTO.setToName(message.getMessage("storage.template.export.to-name",locale));
            titleDTO.setToId(message.getMessage("storage.template.export.to-id",locale));
        }

        titleDTO.setFromName(message.getMessage("storage.template.export.from-name",locale));
        titleDTO.setFromId(message.getMessage("storage.template.export.from-id",locale));
        titleDTO.setDetail(message.getMessage("storage.template.import.detail",locale));
        titleDTO.setNo(message.getMessage("storage.template.import.no",locale));
        titleDTO.setDrugId(message.getMessage("storage.template.import.drug-id",locale));
        titleDTO.setDrugName(message.getMessage("storage.template.import.drug-name",locale));
        titleDTO.setDrugUnitId(message.getMessage("storage.template.import.unit-id",locale));
        titleDTO.setDrugUnitName(message.getMessage("storage.template.import.unit-name",locale));
        titleDTO.setAmount(message.getMessage("storage.template.import.amount",locale));
        titleDTO.setPrice(message.getMessage("storage.template.import.price",locale));
        titleDTO.setExpiration(message.getMessage("storage.template.import.expiration",locale));
        titleDTO.setPackageId(message.getMessage("storage.template.export.package-id",locale));

        return titleDTO;
    }

    @Override
    public String generateWareHouseTemplateFilePath(String pathDirectory,
                                                           String fileName,
                                                           byte[] excelData) throws IOException {

        String filePath = Helper.getTempFilePath(pathDirectory, fileName);

        File resFile = new File(filePath);
        if (resFile.isFile()) {
            return filePath;
        }
        FileUtils.writeByteArrayToFile(resFile, excelData);

        return filePath;
    }

    @Override
    public byte[] generateImportTemplate (ExcelTemplateTitleDTO excelTitle) {
        XlsxBuilder builder = new XlsxBuilder();

        builder.startSheet(excelTitle.getSheetName());
        builder.startRow();
        builder.setRowTitleHeight();
        builder.addTitleTextColumn(excelTitle.getTitle());
        builder.startRow();
        builder.addHeading1TextColumn(excelTitle.getToName());
        builder.startRow();
        builder.addHeading1TextColumn(excelTitle.getToId());
        builder.startRow();
        builder.startRow();
        builder.addBoldTextLeftAlignedColumn(excelTitle.getDetail());
        builder.startRow();
        builder.setRowTitleHeight();
        builder.setRowThickTopBorder();
        builder.setRowThickBottomBorder();
        builder.addBoldTextCenterAlignedColumn(excelTitle.getNo());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getDrugId());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getDrugName());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getDrugUnitId());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getDrugUnitName());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getAmount());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getPrice());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getExpiration());
        builder.startRow();
        builder.setAutoSizeColumn(1);
        builder.setAutoSizeColumn(2);
        builder.setAutoSizeColumn(3);
        builder.setAutoSizeColumn(4);
        builder.setAutoSizeColumn(5);
        builder.setAutoSizeColumn(6);
        builder.setAutoSizeColumn(7);
        builder.mergeCell(1,1,0,3);
        builder.mergeCell(2,2,0,3);

        return  builder.build();
    }

    @Override
    public byte[] generateExportTemplate (ExcelTemplateTitleDTO excelTitle) {
        XlsxBuilder builder = new XlsxBuilder();

        builder.startSheet(excelTitle.getSheetName());
        builder.startRow();
        builder.setRowTitleHeight();
        builder.addTitleTextColumn(excelTitle.getTitle());
        builder.startRow();
        builder.addHeading1TextColumn(excelTitle.getFromName());
        builder.startRow();
        builder.addHeading1TextColumn(excelTitle.getFromId());
        builder.startRow();
        builder.startRow();
        builder.addHeading1TextColumn(excelTitle.getToName());
        builder.startRow();
        builder.addHeading1TextColumn(excelTitle.getToId());
        builder.startRow();
        builder.startRow();
        builder.addBoldTextLeftAlignedColumn(excelTitle.getDetail());
        builder.startRow();
        builder.setRowTitleHeight();
        builder.setRowThickTopBorder();
        builder.setRowThickBottomBorder();
        builder.addBoldTextCenterAlignedColumn(excelTitle.getNo());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getDrugId());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getDrugName());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getPackageId());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getDrugUnitId());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getDrugUnitName());
        builder.addBoldTextCenterAlignedColumn(excelTitle.getAmount());
        builder.startRow();
        builder.setAutoSizeColumn(1);
        builder.setAutoSizeColumn(2);
        builder.setAutoSizeColumn(3);
        builder.setAutoSizeColumn(4);
        builder.setAutoSizeColumn(5);
        builder.setAutoSizeColumn(6);
        builder.mergeCell(1,1,0,3);
        builder.mergeCell(2,2,0,3);
        builder.mergeCell(4,4,0,3);
        builder.mergeCell(5,5,0,3);

        return  builder.build();
    }

    @Override
    public String storeFile(MultipartFile file)  {
        String fileNameOrigin = StringUtils.cleanPath(file.getOriginalFilename());
        String fileNameStore =
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) +
                "_" + fileNameOrigin;
        try {
            String filePath = Helper.getTempFilePath("uploads", fileNameStore);

            Path targetLocation = Paths.get(filePath);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return targetLocation.toString();
        } catch (IOException ex) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_SAVE_FILE, "Could not store file " + fileNameOrigin );

        }
    }

    @Override
    public Object readExcelFile (String filePath, WareHouseExcelType excelType) {

        try( FileInputStream file = new FileInputStream(new File(filePath)))
        {
            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            if (excelType.equals(WareHouseExcelType.EXPORT))
                return readDataFromExportFile(sheet);

            return readDataFromImportFile(sheet);

        }
        catch (FileNotFoundException e)
        {
            throw new MedicException(MedicException.ERROR_DATA_EXCEL_INVALID, "File not found.");
        }
        catch (IOException e){
            throw new MedicException(MedicException.ERROR_DATA_EXCEL_INVALID, "File can not be read.");
        }
        catch (IllegalStateException e) {
            throw new MedicException(MedicException.ERROR_DATA_EXCEL_INVALID,
                    e.getMessage());
        }
        catch (Exception e) {
            throw new MedicException(MedicException.ERROR_DATA_EXCEL_INVALID, e.getMessage());
        }
    }

    private ImportParamDTO readDataFromImportFile (XSSFSheet dataSheet)  {
        int currentRow = 0;
        int currentCell = 0;
        try{
            //Init params
            final int startRow = 0, startCell = 0;
            final int dataRow = 6;
            final int wareHouseIdRow = startRow + 2 ;
            final int wareHouseIdCell = startCell + 4 ;
            final int drugIdCell = startCell + 1 ;
            final int drugUnitIdCell = startCell + 3 ;
            final int drugAmountCell = startCell + 5 ;
            final int drugPriceCell = startCell + 6 ;
            final int drugExpirationCell = startCell + 7 ;

            ImportParamDTO importParam = new ImportParamDTO();
            List<DrugInfoImportDTO> drugInfoImports = new ArrayList<>();

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = dataSheet.iterator();

            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();

                currentRow = row.getRowNum();
                currentCell = startCell;

                if (currentRow == wareHouseIdRow){
                    Cell cell = row.getCell(wareHouseIdCell);
                    currentCell = wareHouseIdCell;
                    importParam.setToId(Double.valueOf(cell.getNumericCellValue()).intValue());
                }

                if(currentRow < dataRow) {
                    continue;
                }

                DrugInfoImportDTO drugInfoImport = new DrugInfoImportDTO();
                currentCell = drugIdCell;
                drugInfoImport.setDrugId(Double.valueOf(row.getCell(drugIdCell).getNumericCellValue()).intValue());
                currentCell = drugUnitIdCell;
                drugInfoImport.setUnitId(Double.valueOf(row.getCell(drugUnitIdCell).getNumericCellValue()).intValue());
                currentCell = drugAmountCell;
                drugInfoImport.setAmount(Double.valueOf(row.getCell(drugAmountCell).getNumericCellValue()).intValue());
                currentCell = drugPriceCell;
                drugInfoImport.setPrice(Double.valueOf(row.getCell(drugPriceCell).getNumericCellValue()).intValue());
                currentCell = drugExpirationCell;
                drugInfoImport.setExpiration(new java.sql.Date(row.getCell(drugExpirationCell).getDateCellValue().getTime()).
                        toLocalDate());
                drugInfoImports.add(drugInfoImport);
            }
            importParam.setDrugInfoImports(drugInfoImports);
            return importParam;
        }
        catch (IllegalStateException | NullPointerException  e) {
            throw new IllegalStateException(String.format("Invalid data at row %d , column %d .", currentRow + 1, currentCell));
        }
    }

    private ExportParamDTO readDataFromExportFile (XSSFSheet dataSheet)  {
        int currentRow = 0;
        int currentCell = 0;
        try{
            //Init params
            final int startRow = 0, startCell = 0;
            final int dataRow = 9;

            final int wareHouseIdRow = startRow + 2 ;
            final int wareHouseDestinationIdRow = startRow + 5 ;
            final int wareHouseIdCell = startCell + 4 ;

            final int drugIdCell = startCell + 1 ;
            final int drugPackageIdCell = startCell + 3 ;
            final int drugUnitIdCell = startCell + 4 ;
            final int drugAmountCell = startCell + 6 ;

            ExportParamDTO exportParamDTO = new ExportParamDTO();
            List<DrugInfoExportDTO> drugInfoExportDTOS = new ArrayList<>();

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = dataSheet.iterator();

            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();

                currentRow = row.getRowNum();
                currentCell = startCell;

                if (currentRow == wareHouseIdRow){
                    currentCell = wareHouseIdCell;
                    exportParamDTO.setFromId(Double.valueOf(row.getCell(currentCell).getNumericCellValue()).intValue());
                }
                if (currentRow == wareHouseDestinationIdRow){
                    currentCell = wareHouseIdCell;
                    exportParamDTO.setToId(Double.valueOf(row.getCell(currentCell).getNumericCellValue()).intValue());
                }

                if(currentRow < dataRow) {
                    continue;
                }

                DrugInfoExportDTO drugInfo = new DrugInfoExportDTO();
                currentCell = drugIdCell;
                drugInfo.setDrugId(Double.valueOf(row.getCell(currentCell).getNumericCellValue()).intValue());
                currentCell = drugPackageIdCell;
                drugInfo.setImportId(Double.valueOf(row.getCell(currentCell).getNumericCellValue()).intValue());
                currentCell = drugUnitIdCell;
                drugInfo.setUnitId(Double.valueOf(row.getCell(currentCell).getNumericCellValue()).intValue());
                currentCell = drugAmountCell;
                drugInfo.setAmount(Double.valueOf(row.getCell(currentCell).getNumericCellValue()).intValue());
                drugInfo.setDrugImportDetailId(-1);
                drugInfoExportDTOS.add(drugInfo);
            }
            exportParamDTO.setDrugInfoExports(drugInfoExportDTOS);
            return exportParamDTO;
        }
        catch (IllegalStateException | NullPointerException e) {
            throw new IllegalStateException(String.format("Invalid data at row %d , column %d .", currentRow + 1, currentCell));
        }

    }

}
