package com.mmedic.service.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.TopicManagementResponse;
import com.google.firebase.messaging.WebpushConfig;
import com.google.firebase.messaging.WebpushFcmOptions;
import com.google.firebase.messaging.WebpushNotification;
import com.mmedic.config.prop.ApplicationProperties;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Firebase push notification service.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class FirebaseNotification {

    public static final String PRECLINICAL_TOPIC = "PRECLINICAL_TOPIC_";
    public static final String DRUG_STORE_TOPIC = "DRUG_STORE_TOPIC_";
    public static final String DRUG_WAREHOUSE_TOPIC = "DRUG_WAREHOUSE_TOPIC_";
    public static final String MEDICAL_TREATMENT_TOPIC = "MEDICAL_TREATMENT_TOPIC_";
    //    private static final String FAVICON_ICON = "/favicon.png";
    public static final Notification defaultNotification = new Notification("Thông báo từ MMedic", "Thông báo từ MMedic");

    private final ApplicationProperties applicationProperties;

    /**
     * Init firebase app.
     */
    @PostConstruct
    public void initializeApp() {
        try {
            Path temp = Files.createTempFile("firebase-admin-sdk", ".json");
            Files.copy(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("firebase-adminsdk.json")),
                    temp,
                    StandardCopyOption.REPLACE_EXISTING);
            FileInputStream serviceAccount = new FileInputStream(temp.toFile());
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//                    .setCredentials(GoogleCredentials.getApplicationDefault())
                    .setDatabaseUrl(applicationProperties.getFirebase().getDatabaseUrl())
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            throw new MedicException(MedicException.ERROR_CAN_NOT_INIT_FIREBASE, e);
        }
    }

    /**
     * Generate notification.
     *
     * @param title the title of notification
     * @param body  the body of notification
     * @return the notification
     */
    public Notification generateNotification(String title, String body) {
        return new Notification(title, body);
    }

    /**
     * Push notification to topic
     *
     * @param topic        the topic name
     * @param notification notification value
     * @param data         data
     */
    public void sendToTopic(String topic, Notification notification, Map<String, String> data) {
        new Thread(() -> {
            try {
                Message message = Message.builder()
                        .putAllData(data)
                        .setNotification(null != notification ? notification : defaultNotification)
                        .setTopic(topic)
                        .build();

                log.info("*******************************************");
                log.info("* Sent to topic: " + topic);
                log.info("*******************************************");

                FirebaseMessaging.getInstance().send(message);
            } catch (FirebaseMessagingException e) {
                log.error(e.getMessage(), e);
            }
        }).start();
    }

    /**
     * Send notification to registration token.
     *
     * @param registrationToken registration token
     * @param notification      notification
     * @param data              data
     */
    public void sendToRegistrationToken(String registrationToken, Notification notification, Map<String, String> data) {
        if (!StringUtils.isEmpty(registrationToken)) {
            List<String> tokens = Arrays.stream(Helper.convertNullToEmpty(registrationToken).split(Constants.SEPARATE_TOKEN_REGEX)).
                    filter(token -> !StringUtils.isEmpty(token)).
                    distinct().
                    collect(Collectors.toList());

            new Thread(() -> {
                try {
                    MulticastMessage message = MulticastMessage.builder()
                            .putAllData(data)
                            .setWebpushConfig(WebpushConfig.builder()
                                    .setFcmOptions(WebpushFcmOptions.withLink(this.applicationProperties.getWebUrl()))
                                    .build())
                            .setNotification(null != notification ? notification : defaultNotification)
                            .addAllTokens(tokens)
                            .build();

                    log.info("*******************************************");
                    log.info("* Sent to tokens: " + tokens);
                    log.info("*******************************************");

                    // Send a message to the device corresponding to the provided registration token.
                    BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(message);
                } catch (FirebaseMessagingException e) {
                    log.error(e.getMessage(), e);
                }
            }).start();
        }
    }

    /**
     * Send notification to registration tokens.
     *
     * @param messages messages
     */
    public void sendBatchMessage(List<Message> messages) {
        if (!CollectionUtils.isEmpty(messages)) {
            new Thread(() -> {
                try {
                    BatchResponse response = FirebaseMessaging.getInstance().sendAll(messages);
                } catch (FirebaseMessagingException e) {
                    log.error(e.getMessage(), e);
                }
            }).start();
        }
    }

    /**
     * Subscribe topic.
     *
     * @param topic             the topic name
     * @param registrationToken registration token
     * @return number of token is subscribed
     */
    public int subscribeTopic(String topic, String... registrationToken) {
        int result = 0;
        if (!StringUtils.isEmpty(topic)) {
            try {
                List<String> registrationTokens = Arrays.asList(registrationToken);

                // Subscribe the devices corresponding to the registration tokens to the topic.
                TopicManagementResponse response = FirebaseMessaging.getInstance().subscribeToTopic(registrationTokens, topic);
                result = response.getSuccessCount();
            } catch (FirebaseMessagingException e) {
                throw new MedicException(MedicException.ERROR_FIREBASE_SUBSCRIBE, e);
            }
        }
        return result;
    }

    /**
     * Unsubscribe topic.
     *
     * @param topic             the topic name
     * @param registrationToken registration token
     * @return number of token is unsubscribed
     */
    public int unsubscribeTopic(String topic, String... registrationToken) {
        int result = 0;
        if (!StringUtils.isEmpty(topic)) {
            try {
                List<String> registrationTokens = Arrays.asList(registrationToken);

                // Subscribe the devices corresponding to the registration tokens to the topic.
                TopicManagementResponse response = FirebaseMessaging.getInstance().unsubscribeFromTopic(registrationTokens, topic);
                result = response.getSuccessCount();
            } catch (FirebaseMessagingException e) {
                throw new MedicException(MedicException.ERROR_FIREBASE_UNSUBSCRIBE, e);
            }
        }
        return result;
    }

    /**
     * Get list tokens.
     *
     * @param registrationToken registration token
     * @return list token
     */
    public List<String> getTokens(String registrationToken) {
        List<String> tokens = new ArrayList<>();
        if (!StringUtils.isEmpty(registrationToken)) {
            tokens = Arrays.stream(Helper.convertNullToEmpty(registrationToken).split(Constants.SEPARATE_TOKEN_REGEX)).
                    filter(token -> !StringUtils.isEmpty(token)).
                    distinct().
                    collect(Collectors.toList());
        }
        return tokens;
    }

}
