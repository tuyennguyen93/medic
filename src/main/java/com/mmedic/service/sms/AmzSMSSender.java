package com.mmedic.service.sms;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.mmedic.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class AmzSMSSender implements SMSSender {

    private AmazonSNS amazonSNS;

    public AmzSMSSender(AmazonSNS amazonSNS) {
        this.amazonSNS = amazonSNS;
    }

    @Override
    public void sendSms(String phone, String message) {
        String reformatPhone = "+84" + phone.replaceFirst("0", Constants.EMPTY_STRING);
        Map<String, MessageAttributeValue> smsAttributes = new HashMap<>();
        smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
                .withStringValue("Transactional")
                .withDataType("String"));

        PublishRequest publishRequest = new PublishRequest();
        publishRequest.setPhoneNumber(reformatPhone);
        publishRequest.setMessage(message);
        publishRequest.setMessageAttributes(smsAttributes);

        PublishResult publishResult = amazonSNS.publish(publishRequest);
        log.info("DONE" + publishResult.getMessageId());
    }
}
