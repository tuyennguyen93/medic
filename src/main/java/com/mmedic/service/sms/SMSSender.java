package com.mmedic.service.sms;

public interface SMSSender {

    void sendSms(String phone, String message);
}
