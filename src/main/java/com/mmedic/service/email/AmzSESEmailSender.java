package com.mmedic.service.email;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.*;
import lombok.RequiredArgsConstructor;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.StringWriter;
import java.net.ConnectException;
import java.util.Map;

/**
 * Using Amazon Simple Email Service to handle mail.
 *
 * @author toantc
 */
@RequiredArgsConstructor
@Service
public class AmzSESEmailSender implements EmailSender{

    private final AmazonSimpleEmailService amzSESProvider;

    private final VelocityEngine velocityEngine;

    @Value("${spring.velocity.resource-loader-path}")
    private  String mailFolder;

    @Override
    public void sendMail(String fromAddress, String toAddress, String templateMail, Map model) {
        String sender = String.format("MMedic <%s>", fromAddress);
        boolean end = false;
        int count = 0;
        while (end == false) {
            try {
                Destination destination = new Destination().withToAddresses(toAddress);

                // Create the subject and body of the message.
                Content contSubject = new Content().withData(mergeMailTemplateToContent(templateMail, model, true));
                Content contBody = new Content().withData(mergeMailTemplateToContent(templateMail, model, false));
                Body body = new Body().withHtml(contBody);

                // Create a message with the specified subject and body.
                Message message = new Message().withSubject(contSubject).withBody(body);
                // Assemble the email.
                SendEmailRequest request = new SendEmailRequest().withSource(sender)
                        .withDestination(destination)
                        .withMessage(message);

                amzSESProvider.sendEmail(request);

                end = true;
            } catch (Exception e) {
                if (count >= 5 || !(e.getCause() instanceof ConnectException)) {
                    // kick out if retry was already enough or non retry able reason
                    throw e;
                }
                ++count;
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e1) {
                    throw new RuntimeException(e1);
                }
            }
        }
    }

    private String mergeMailTemplateToContent(String templateMail, Map model, boolean isSubject) throws RuntimeException {
        StringWriter writer = new StringWriter();
        try {
            String temp = mailFolder + (isSubject ? "subject-"+templateMail : templateMail);
            temp += ".vm";
            VelocityContext velocityContext = new VelocityContext(model);
            velocityEngine.mergeTemplate(temp, "utf-8", velocityContext, writer);
        } catch (VelocityException e) {
            throw new RuntimeException(e);
        }
        return writer.toString();
    }
}
