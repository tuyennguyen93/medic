package com.mmedic.service.email;

import java.util.Map;

/**
 * Email sender.
 *
 * @author toantc
 */
public interface EmailSender {

    /**
     * Send email.
     *
     * @param fromAddress from address
     * @param toAddress to address
     * @param templateMail template mail
     * @param model model
     */
    public void sendMail(String fromAddress, String toAddress, String templateMail, Map model);
}
