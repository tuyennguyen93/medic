package com.mmedic.service.report;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.mmedic.utils.Helper;
import lombok.RequiredArgsConstructor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class ReportExportService {

    private final VelocityEngine velocityEngine;

    @Value("${spring.velocity.resource-reports}")
    private  String reportsTemplate;

    public String generatePDFFromHTML(String filename, String template, Map model) throws Exception {

        String filePath = Helper.getTempFilePath("pdf", filename + ".pdf");

        String FONT = "assets/fonts/arial.ttf";
        XMLWorkerFontProvider fontImp = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
        fontImp.register(FONT);

        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document,
                new FileOutputStream(filePath));
        document.open();

        XMLWorkerHelper.getInstance().parseXHtml(writer,
                document,
                mergeReportTemplateToContent(template, model), StandardCharsets.UTF_8, fontImp);

        document.close();
        return filePath;
    }

    public String generateImageFromPDF(String filename) throws Exception {
        String tempPDF = Helper.getTempFilePath("pdf", filename + ".pdf");
        String tempJpeg = Helper.getTempFilePath("img", filename + ".jpeg");
        PDDocument document = PDDocument.load(new File(tempPDF));
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        BufferedImage bim = pdfRenderer.renderImageWithDPI(
                0, 300, ImageType.RGB);
        ImageIOUtil.writeImage(
                bim, tempJpeg, 300);
        document.close();
        return tempJpeg;
    }


    private ByteArrayInputStream mergeReportTemplateToContent(String reportTemplate, Map model) throws RuntimeException {
        StringWriter writer = new StringWriter();
        try {
            String temp = reportsTemplate + reportTemplate ;
            temp += ".vm";
            VelocityContext velocityContext = new VelocityContext(model);
            velocityEngine.mergeTemplate(temp, "utf-8", velocityContext, writer);
        } catch (VelocityException e) {
            throw new RuntimeException(e);
        }
        return new ByteArrayInputStream(writer.toString().getBytes(StandardCharsets.UTF_8));
    }

}
