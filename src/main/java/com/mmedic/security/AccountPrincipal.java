package com.mmedic.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mmedic.entity.Account;
import com.mmedic.entity.AuthMethod;
import com.mmedic.enums.AuthType;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * User details.
 *
 * @author hungp
 */
@Getter
public class AccountPrincipal implements UserDetails {

    private int id;

    private String fullName;

    private String username;

    @JsonIgnore
    private String password;

    private String timeStep;

    private Collection<? extends GrantedAuthority> authorities;

    private AuthType authType;

    private boolean enabled;

    public AccountPrincipal(int id, String fullName, String username, String password, String timeStep, Collection<? extends GrantedAuthority> authorities, AuthType authType, boolean enabled) {
        this.id = id;
        this.fullName = fullName;
        this.authorities = authorities;
        this.authType = authType;
        this.username = username;
        this.password = password;
        this.timeStep = timeStep;
        this.enabled = enabled;
    }

    /**
     * Creates new instance Account Principal from Account.
     *
     * @param account the account entity
     * @return AccountPrincipal instance
     * @throws UsernameNotFoundException if user can't be found
     * @throws Exception                 other exception
     */
    public static AccountPrincipal create(Account account) throws UsernameNotFoundException, Exception {
        if (Objects.isNull(account)) {
            throw new UsernameNotFoundException("The account is empty");
        }
        if (CollectionUtils.isEmpty(account.getAuthMethods())) {
            throw new Exception("The account has no authentication methods");
        }

        // Role
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (null != account.getRole()) {
            MedicGrantedAuthority authority = new MedicGrantedAuthority("ROLE_" + account.getRole().getName());

            if (!CollectionUtils.isEmpty(account.getRole().getRolePermissions())) {
                List<MedicGrantedAuthority.Scope> scopes = account.getRole().getRolePermissions().stream().
                        map(rolePermission -> MedicGrantedAuthority.Scope.create(rolePermission)).collect(Collectors.toList());
                authority.setScopes(scopes);
            }
            authorities.add(authority);
        }

        AuthMethod emailAuthMethod = account.getAuthMethods().stream().
                filter(authMethod -> authMethod.getAuthType().equals(AuthType.EMAIL_AND_PASSWORD)).
                findFirst().orElse(null);

        if (null != emailAuthMethod) {
            return new AccountPrincipal(account.getId(), account.getName(), emailAuthMethod.getAuthData1(), emailAuthMethod.getAuthData2(), null, authorities, emailAuthMethod.getAuthType(), account.getIsActived());
        }

        AuthMethod phoneAuthMethod = account.getAuthMethods().stream().
                filter(authMethod -> authMethod.getAuthType().equals(AuthType.PHONE_NUMBER)).
                findFirst().orElse(null);
        if (null != phoneAuthMethod) {
            return new AccountPrincipal(account.getId(), account.getName(), phoneAuthMethod.getAuthData1(), phoneAuthMethod.getAuthData2(), phoneAuthMethod.getAuthData3(), authorities, phoneAuthMethod.getAuthType(), account.getIsActived());
        }
        return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
