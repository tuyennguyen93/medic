package com.mmedic.security;

import com.mmedic.entity.RolePermission;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.List;

/**
 * Granted authority.
 *
 * @author hungp
 */
public class MedicGrantedAuthority implements GrantedAuthority {

    private static final long serialVersionUID = 8658413654781547136L;

    private final String role;

    @Getter
    @Setter
    private List<Scope> scopes;

    public MedicGrantedAuthority(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String getAuthority() {
        return role;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof MedicGrantedAuthority) {
            return role.equals(((MedicGrantedAuthority) obj).role);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return role.hashCode();
    }

    @Override
    public String toString() {
        return role;
    }

    @Getter
    @Setter
    public static class Scope implements Serializable {

        private static final long serialVersionUID = 1547569548145824812L;

        private String scope;

        private String condition;

        private String conditionValue;

        public static Scope create(RolePermission rolePermission) {
            Scope scope = new Scope();
            scope.setScope(rolePermission.getScope());
            scope.setCondition(rolePermission.getCondition());
            scope.setConditionValue(rolePermission.getConditionValue());
            return scope;
        }
    }
}
