package com.mmedic.security.jwt;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Default class for RefreshToken.
 *
 * @author hungp
 */
public class DefaultRefreshToken implements RefreshToken, Serializable {

    private static final long serialVersionUID = 5981624924502463391L;

    private String value;

    private Date expiration;

    /**
     * Create a new refresh token.
     */
    public DefaultRefreshToken(String value) {
        this.value = value;
    }

    /**
     * Default constructor for JPA and other serialization tools.
     */
    @SuppressWarnings("unused")
    private DefaultRefreshToken() {
        this(null);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public Date getExpiration() {
        return expiration;
    }

    /**
     * Sets expiration time.
     *
     * @param expiration the expiration time
     */
    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    @Override
    public String toString() {
        return getValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DefaultRefreshToken)) {
            return false;
        }

        DefaultRefreshToken that = (DefaultRefreshToken) obj;

        if (!Objects.equals(value, that.value)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}
