package com.mmedic.security.jwt;

import com.mmedic.enums.AuthType;
import com.mmedic.security.AccountPrincipal;
import org.springframework.security.core.Authentication;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Basic key generator taking into account the username (principal name) if they exist.
 */
public class AuthenticationKeyGenerator {

    private static final String USERNAME = "email";
    private static final String ID = "id";

    /**
     * Extracts keys from authentication instance.
     *
     * @param authentication the Authentication instance
     * @return the key has been hash
     */
    public String extractKey(Authentication authentication) {
        Map<String, String> values = new LinkedHashMap<String, String>();
        AccountPrincipal principal = (AccountPrincipal) authentication.getPrincipal();
        values.put(USERNAME, authentication.getName());
        if (AuthType.PHONE_NUMBER.equals(principal.getAuthType())) {
            values.put(ID, String.valueOf(principal.getId()));
        }
        return generateKey(values);
    }

    /**
     * Generation key.
     *
     * @param values the values
     * @return key
     */
    private String generateKey(Map<String, String> values) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            byte[] bytes = digest.digest(values.toString().getBytes("UTF-8"));
            return String.format("%032x", new BigInteger(1, bytes));
        } catch (NoSuchAlgorithmException nsae) {
            throw new IllegalStateException("MD5 algorithm not available.  Fatal (should be in the JDK).", nsae);
        } catch (UnsupportedEncodingException uee) {
            throw new IllegalStateException("UTF-8 encoding not available.  Fatal (should be in the JDK).", uee);
        }
    }
}
