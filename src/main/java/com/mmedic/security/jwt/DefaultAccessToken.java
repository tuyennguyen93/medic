package com.mmedic.security.jwt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Default class for AccessToken.
 *
 * @author hungp
 */
public class DefaultAccessToken implements AccessToken, Serializable {

    private static final long serialVersionUID = 2518241862159364478L;
    private List<String> roles;
    private String value;
    private Date expiration;
    private RefreshToken refreshToken;

    /**
     * Create an access token from the value provided.
     */
    public DefaultAccessToken(String value) {
        this.value = value;
    }

    /**
     * Private constructor for JPA and other serialization tools.
     */
    @SuppressWarnings("unused")
    private DefaultAccessToken() {
        this((String) null);
    }

    @Override
    public String getValue() {
        return value;
    }

    /**
     * Sets access token value.
     *
     * @param value the access token value
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public RefreshToken getRefreshToken() {
        return refreshToken;
    }

    /**
     * Sets refresh token.
     *
     * @param refreshToken the RefreshToken instance
     */
    @Override
    public void setRefreshToken(RefreshToken refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public boolean isExpired() {
        return expiration != null && expiration.before(new Date());
    }

    @Override
    public Date getExpiration() {
        return expiration;
    }

    /**
     * Sets expiration time.
     *
     * @param expiration the expiration time
     */
    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    @Override
    public int getExpiresIn() {
        return expiration != null ? Long.valueOf((expiration.getTime() - System.currentTimeMillis()) / 1000L)
                .intValue() : 0;
    }

    /**
     * Sets seconds for expiration time.
     *
     * @param delta seconds
     */
    protected void setExpiresIn(int delta) {
        setExpiration(new Date(System.currentTimeMillis() + delta));
    }

    @Override
    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> role) {
        this.roles = role;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public String toString() {
        return String.valueOf(getValue());
    }
}
