package com.mmedic.security.jwt;

import com.mmedic.config.prop.JwtProperties;
import com.mmedic.enums.AuthType;
import com.mmedic.rest.account.AccountService;
import com.mmedic.security.AccountPrincipal;
import com.mmedic.security.JWTAuthenticationToken;
import com.mmedic.spring.errors.MedicException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Token Provider. This class use to create, read access and refresh tokens.
 *
 * @author hungp
 */
@Slf4j
@Component
public class TokenProvider {

    private static final String AUTHORITIES_KEY = "auth";
    private JwtProperties jwtProperties;
    private Key key;
    private boolean reuseRefreshToken = true;
    private long tokenValidityInMilliseconds;
    private long refreshTokenValidityInMilliseconds;

    private TokenStore tokenStore;

    private AccountService accountService;

    private AuthenticationManagerBuilder authenticationManagerBuilder;

    public TokenProvider(JwtProperties jwtProperties, TokenStore tokenStore, AccountService accountService, AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.jwtProperties = jwtProperties;
        this.tokenStore = tokenStore;
        this.accountService = accountService;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.afterPropertiesSet();
    }

    public void afterPropertiesSet() {
        byte[] keyBytes;
        String secret = jwtProperties.getSigningKey();
        if (StringUtils.isEmpty(secret)) {
            if (log.isWarnEnabled()) {
                log.warn("Warning: The JWT key is not set in properties file. We'll use default key.");
            }
            keyBytes = Decoders.BASE64.decode("bW1lZGljLWFwaS1qd3Qtc2VjcmV0LWtleS01TUdNNE1qRTRPREk0WXpSaU5qWmtPVFJoTlRVM1ltTmtNV1JtTVdZeE16a3pZ");
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Using JWT key is set in properties file");
            }
            keyBytes = secret.getBytes(StandardCharsets.UTF_8);
        }
        key = Keys.hmacShaKeyFor(keyBytes);
        this.tokenValidityInMilliseconds = jwtProperties.getAccessTokenValiditySeconds() * 1000;
        this.refreshTokenValidityInMilliseconds = jwtProperties.getRefreshTokenValiditySeconds() * 1000;
    }

    /**
     * Creates token.
     *
     * @param authentication Authentication instance
     * @param validity       expiration time
     * @return the token
     */
    private String createToken(Authentication authentication, Date validity) {
        String authorities = authentication.getAuthorities().stream().
                map(GrantedAuthority::getAuthority).
                collect(Collectors.joining(","));

        AccountPrincipal principal = (AccountPrincipal) authentication.getPrincipal();
        String name = principal.getUsername();
        if (AuthType.PHONE_NUMBER.equals(principal.getAuthType())) {
            name += principal.getId();
        }
        return Jwts.builder().
                setSubject(name).
                claim(AUTHORITIES_KEY, authorities).
                signWith(key, SignatureAlgorithm.HS512).
                setExpiration(validity).
                compact();
    }

    /**
     * Get Authentication from access token value.
     *
     * @param token the access token
     * @return Authentication instance.
     */
    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parser().
                setSigningKey(key).
                parseClaimsJws(token).
                getBody();
        Collection<? extends GrantedAuthority> authorities =
                Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(",")).
                        map(SimpleGrantedAuthority::new).
                        collect(Collectors.toList());
        User principal = new User(claims.getSubject(), "", authorities);
        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    /**
     * Validation access token.
     *
     * @param accessTokenValue the access token value.
     * @return true or false
     */
    public boolean validateToken(String accessTokenValue) {
        try {
            AccessToken accessToken = readAccessToken(accessTokenValue);
            if (null != accessToken) {
                Jwts.parser().setSigningKey(key).parseClaimsJws(accessToken.getValue());
                return true;
            }
        } catch (io.jsonwebtoken.security.SecurityException | MalformedJwtException e) {
            log.error("Invalid JWT signature trace: {}", e);
        } catch (ExpiredJwtException e) {
            log.error("Expired JWT token trace: {}", e);
        } catch (UnsupportedJwtException e) {
            log.error("Unsupported JWT token trace: {}", e);
        } catch (IllegalArgumentException e) {
            log.error("JWT token compact of handler are invalid trace: {}", e);
        }
        return false;
    }

    /**
     * Create access token and store in redis.
     *
     * @param authentication authentication instance
     * @return AccessToken instance
     */
    public AccessToken createAccessToken(Authentication authentication, boolean rememberMe) {
        AccessToken existingAccessToken = tokenStore.getAccessToken(authentication);
        RefreshToken refreshToken = null;
        if (null != existingAccessToken) {
            if (existingAccessToken.isExpired()) {
                if (null != existingAccessToken.getRefreshToken()) {
                    refreshToken = existingAccessToken.getRefreshToken();
                    // The token store could remove the refresh token when the
                    // access token is removed, but we want to
                    // be sure...
                    tokenStore.removeRefreshToken(refreshToken);
                }
                tokenStore.removeAccessToken(existingAccessToken);
            } else {
                // Re-store the access token in case the authentication has changed
                tokenStore.storeAccessToken(existingAccessToken, authentication);
                return existingAccessToken;
            }
        }

        if (null == refreshToken) {
            refreshToken = createRefreshToken(authentication, rememberMe);
        } else {
            if (System.currentTimeMillis() > refreshToken.getExpiration().getTime()) {
                refreshToken = createRefreshToken(authentication, rememberMe);
            }
        }

        AccessToken accessToken = createAccessToken(authentication, refreshToken);
        tokenStore.storeAccessToken(accessToken, authentication);
        // In case it was modified
        refreshToken = accessToken.getRefreshToken();
        if (null != refreshToken) {
            tokenStore.storeRefreshToken(refreshToken, authentication);
        }
        return accessToken;
    }

    /**
     * Refresh the access token by refresh token value.
     *
     * @param refreshTokenValue the refresh token value
     * @return AccessToken instance
     * @throws Exception exception
     */
    public AccessToken refreshAccessToken(String refreshTokenValue) throws MedicException {
        RefreshToken refreshToken = tokenStore.readRefreshToken(refreshTokenValue);
        if (null == refreshToken) {
            throw new MedicException(MedicException.ERROR_REFRESH_TOKEN_INVALID, "Invalid refresh token: " + refreshTokenValue);
        }

        Authentication authentication = tokenStore.readAuthenticationForRefreshToken(refreshToken);
        if (null != authenticationManagerBuilder) {
            // The client has already been authenticated, but the user authentication might be old now, so give it a
            // chance to re-authenticate.
            Authentication user = new JWTAuthenticationToken(authentication.getName(), "", authentication);
            user = authenticationManagerBuilder.getObject().authenticate(user);
            authentication = user;
        }

        // clear out any access tokens already associated with the refresh
        // token.
        tokenStore.removeAccessTokenUsingRefreshToken(refreshToken);

        if (isExpired(refreshToken)) {
            tokenStore.removeRefreshToken(refreshToken);
            throw new MedicException(MedicException.ERROR_REFRESH_TOKEN_EXPIRE, "Invalid refresh token (expired): " + refreshToken);
        }

        if (!reuseRefreshToken) {
            tokenStore.removeRefreshToken(refreshToken);
            refreshToken = createRefreshToken(authentication, true);
        }

        AccessToken accessToken = createAccessToken(authentication, refreshToken);
        tokenStore.storeAccessToken(accessToken, authentication);
        if (!reuseRefreshToken) {
            tokenStore.storeRefreshToken(accessToken.getRefreshToken(), authentication);
        }
        return accessToken;
    }

    /**
     * Check refresh token is expiration.
     *
     * @param refreshToken the refresh token
     * @return true or false
     */
    protected boolean isExpired(RefreshToken refreshToken) {
        return null == refreshToken.getExpiration()
                || System.currentTimeMillis() > refreshToken.getExpiration().getTime();
    }

    /**
     * Get AccessToken instance by Authentication instance.
     *
     * @param authentication the Authentication instance
     * @return AccessToken instance
     */
    public AccessToken getAccessToken(Authentication authentication) {
        return tokenStore.getAccessToken(authentication);
    }

    /**
     * Reads access token by it's value.
     *
     * @param accessToken the access token value
     * @return AccessToken value
     */
    public AccessToken readAccessToken(String accessToken) {
        return tokenStore.readAccessToken(accessToken);
    }

    /**
     * Load Authentication by access token value.
     *
     * @param accessTokenValue the access token value
     * @return Authentication instance
     * @throws Exception exception
     */
    public Authentication loadAuthentication(String accessTokenValue) throws Exception {
        AccessToken accessToken = tokenStore.readAccessToken(accessTokenValue);
        if (null == accessToken) {
            throw new Exception("Invalid access token: " + accessTokenValue);
        } else if (accessToken.isExpired()) {
            tokenStore.removeAccessToken(accessToken);
            throw new Exception("Access token expired: " + accessTokenValue);
        }

        Authentication result = tokenStore.readAuthentication(accessToken);
        if (null == result) {
            // in case of race condition
            throw new Exception("Invalid access token: " + accessTokenValue);
        }
        return result;
    }

    /**
     * Revoke access token.
     *
     * @param tokenValue the access token
     * @return true or false
     */
    public boolean revokeToken(String tokenValue) {
        AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
        if (null == accessToken) {
            return false;
        }
        if (null != accessToken.getRefreshToken()) {
            tokenStore.removeRefreshToken(accessToken.getRefreshToken());
        }
        tokenStore.removeAccessToken(accessToken);
        return true;
    }

    /**
     * Creates new RefreshToken instance.
     *
     * @param authentication the Authentication instance
     * @return new RefreshToken instance
     */
    private RefreshToken createRefreshToken(Authentication authentication, boolean rememberMe) {
        Date expiration = null;
        if (hasAuthType(authentication, AuthType.EMAIL_AND_PASSWORD)) {
            if (!rememberMe) {
                if (tokenValidityInMilliseconds > 0) {
                    expiration = new Date(System.currentTimeMillis() + tokenValidityInMilliseconds);
                } else {
                    expiration = new Date(System.currentTimeMillis() + 3600 * 1000);
                }
            } else {
                if (refreshTokenValidityInMilliseconds > 0) {
                    expiration = new Date(System.currentTimeMillis() + refreshTokenValidityInMilliseconds);
                } else {
                    expiration = new Date(System.currentTimeMillis() + 7200 * 1000);
                }
            }
        }
        DefaultRefreshToken refreshToken = new DefaultRefreshToken(createToken(authentication, expiration));
        refreshToken.setExpiration(expiration);
        return refreshToken;
    }

    /**
     * Check authentication, which is used is the same with auth type.
     *
     * @param authentication Authentication data
     * @param authType       AuthType to check
     * @return true it's same
     */
    private boolean hasAuthType(Authentication authentication, AuthType authType) {
        if (null != authentication) {
            AccountPrincipal principal = (AccountPrincipal) authentication.getPrincipal();
            return authType.equals(principal.getAuthType());
        }
        return false;
    }

    /**
     * Creates new AccessToken instance.
     *
     * @param authentication the Authentication instance
     * @param refreshToken   the RefreshToken instance
     * @return new AccessToken instance
     */
    private AccessToken createAccessToken(Authentication authentication, RefreshToken refreshToken) {
        Date expiration = null;
        if (hasAuthType(authentication, AuthType.EMAIL_AND_PASSWORD)) {
            if (tokenValidityInMilliseconds > 0) {
                expiration = new Date(System.currentTimeMillis() + tokenValidityInMilliseconds);
            } else {
                expiration = new Date(System.currentTimeMillis() + 3600 * 1000);
            }
        }
        DefaultAccessToken token = new DefaultAccessToken(createToken(authentication, expiration));
        token.setExpiration(expiration);
        token.setRefreshToken(refreshToken);
        if (!CollectionUtils.isEmpty(authentication.getAuthorities())) {
            List<String> roles = new ArrayList<>();
            for (GrantedAuthority authority : authentication.getAuthorities()) {
                roles.add(authority.getAuthority());
            }
            token.setRoles(roles);
        }
        return token;
    }
}
