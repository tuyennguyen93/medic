package com.mmedic.security.jwt;

import java.util.Date;
import java.util.List;

/**
 * Access token.
 *
 * @author hungp
 */
public interface AccessToken {

    public static String BEARER_TYPE = "Bearer";

    public static String ACCESS_TOKEN = "access_token";

    public static String EXPIRES_IN = "expires_in";

    public static String REFRESH_TOKEN = "refresh_token";

    /**
     * Gets refresh token.
     *
     * @return RefreshToken instance
     */
    RefreshToken getRefreshToken();

    /**
     * Sets refresh token.
     *
     * @param refreshToken the refresh token
     */
    void setRefreshToken(RefreshToken refreshToken);

    /**
     * Check token is expired.
     *
     * @return true or false
     */
    boolean isExpired();

    /**
     * Get expiration date.
     *
     * @return the expiration date
     */
    Date getExpiration();

    /**
     * Gets time expiration in seconds.
     *
     * @return the time expiration
     */
    int getExpiresIn();

    /**
     * Gets access token.
     *
     * @return the access token
     */
    String getValue();

    /**
     * Gets role type.
     *
     * @return the role type
     */
    List<String> getRoles();
}
