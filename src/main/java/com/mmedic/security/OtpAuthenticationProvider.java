package com.mmedic.security;

import com.mmedic.rest.account.AccountService;
import com.mmedic.utils.OtpGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.Ordered;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.util.Assert;

import java.time.Instant;

/**
 * OTP authentication provider.
 *
 * @author hungp
 */
@Slf4j
public class OtpAuthenticationProvider implements AuthenticationProvider, InitializingBean, Ordered {

    private UserDetailsService userDetailsService = null;
    private UserDetailsChecker userDetailsChecker = new AccountStatusUserDetailsChecker();
    private long timeStep;
    private int digit;

    private int order = -1; // default: same as non-ordered

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (!supports(authentication.getClass())) {
            return null;
        }

        if (authentication.getPrincipal() == null) {
            log.debug("No pre-authenticated principal found in request.");
            return null;
        }
        if (authentication.getCredentials() == null) {
            log.debug("No pre-authenticated credentials found in request.");
            return null;
        }
        OtpAuthenticationToken otpAuthentication = (OtpAuthenticationToken) authentication;
        UserDetails ud = ((AccountService) userDetailsService).loadUserByPatientIdAndPhone((int) otpAuthentication.getPatientId(), otpAuthentication.getName());
        userDetailsChecker.check(ud);

        if (!verifyOtpCode((AccountPrincipal) ud, (String) authentication.getCredentials())) {
            throw new BadCredentialsException("The code is expiration or invalid");
        }

        PreAuthenticatedAuthenticationToken result = new PreAuthenticatedAuthenticationToken(
                ud, authentication.getCredentials(), ud.getAuthorities());
        result.setDetails(authentication.getDetails());

//        OtpAuthenticationToken result = new OtpAuthenticationToken(ud.getAuthorities(), ud, "");
//        result.setDetails(authentication.getDetails());
        return result;
    }

    /**
     * Verification the otp code.
     *
     * @param principal   the account details
     * @param verifyValue the otp code
     * @return value of comparison
     */
    private boolean verifyOtpCode(AccountPrincipal principal, String verifyValue) {
        if (null != principal) {
            long T = (Instant.now().toEpochMilli() - Long.parseLong(principal.getTimeStep())) / 1000 / timeStep;
            return OtpGenerator.verifyTOTP(principal.getPassword(), String.valueOf(T), String.valueOf(digit), verifyValue);
        }
        return false;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return OtpAuthenticationToken.class.isAssignableFrom(authentication);
    }

    /**
     * Check whether all required properties have been set.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(userDetailsService, "An UserDetailsService must be set");
    }

    @Override
    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public void setTimeStep(long timeStep) {
        this.timeStep = timeStep;
    }

    public void setDigit(int digit) {
        this.digit = digit;
    }
}
