package com.mmedic.security.scope;

import com.mmedic.security.scope.handler.AnnotationHandler;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/**
 * Scope method security expression root.
 *
 * @author hungp
 */
@Slf4j
public class ScopeMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    private Object filterObject;
    private Object returnObject;
    private MethodInvocation invocation;
    private List<AnnotationHandler> handlers = new ArrayList<>();

    /**
     * Creates a new instance
     *
     * @param authentication the {@link Authentication} to use. Cannot be null.
     */
    public ScopeMethodSecurityExpressionRoot(Authentication authentication, MethodInvocation invocation) {
        super(authentication);
        this.invocation = invocation;
    }

    public boolean checkScope() {
        for (AnnotationHandler handler : handlers) {
            Annotation[] annotations = invocation.getMethod().getAnnotations();
            for (Annotation annotation : annotations) {
                if (handler.support(annotation.getClass())) {
                    return handler.handleScope(authentication);
                }
            }
        }
        return false;
    }

    @Override
    public Object getFilterObject() {
        return filterObject;
    }

    @Override
    public void setFilterObject(Object filterObject) {
        this.filterObject = filterObject;
    }

    @Override
    public Object getReturnObject() {
        return returnObject;
    }

    @Override
    public void setReturnObject(Object returnObject) {
        this.returnObject = returnObject;
    }

    public void setHandlers(List<AnnotationHandler> handlers) {
        this.handlers = handlers;
    }

    @Override
    public Object getThis() {
        return this;
    }
}
