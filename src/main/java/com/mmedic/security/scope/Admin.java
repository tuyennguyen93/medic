package com.mmedic.security.scope;

import java.lang.annotation.*;

/**
 * Admin scope.
 *
 * @author hungp
 */
@Inherited
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Admin {

    @interface Record {

        @Inherited
        @Documented
        @Target({ElementType.METHOD})
        @Retention(RetentionPolicy.RUNTIME)
        @interface ReadOnly {
            String SCOPE = "admin.record.readonly";
        }

        @Inherited
        @Documented
        @Target({ElementType.METHOD})
        @Retention(RetentionPolicy.RUNTIME)
        @interface ReadWrite {
            String SCOPE = "admin.record.readwrite";
        }
    }
}
