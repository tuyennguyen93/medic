package com.mmedic.security.scope;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.*;

/**
 * Check access annotation.
 *
 * @author hungp
 */
@Inherited
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("checkScope()")
public @interface CheckAccess {

//    @Target(ElementType.METHOD)
//    @Retention(RetentionPolicy.RUNTIME)
//    @org.springframework.security.access.prepost.PreAuthorize("hasScope()")
//    @interface PreAuthorize {
//        String value();
//    }
//
//    @Target(ElementType.METHOD)
//    @Retention(RetentionPolicy.RUNTIME)
////    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
//    @interface OnlyDevelopers {
//        String value();
//    }
}
