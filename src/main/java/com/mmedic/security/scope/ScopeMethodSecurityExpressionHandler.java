package com.mmedic.security.scope;

import com.mmedic.security.scope.handler.AnnotationHandler;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;

import java.util.ArrayList;
import java.util.List;

/**
 * Scope method security expression handler.
 *
 * @author hungp
 */
public class ScopeMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {

    private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

    private List<AnnotationHandler> handlers = new ArrayList<>();

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication, MethodInvocation invocation) {
        ScopeMethodSecurityExpressionRoot root = new ScopeMethodSecurityExpressionRoot(authentication, invocation);
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        root.setHandlers(getHandlers());
        return root;
    }

    public List<AnnotationHandler> getHandlers() {
        return handlers;
    }

    public void setHandlers(List<AnnotationHandler> handlers) {
        this.handlers = handlers;
    }

    public void addAnnotationHandler(AnnotationHandler handler) {
        handlers.add(handler);
    }
}
