package com.mmedic.security.scope.handler;

import com.mmedic.security.AccountPrincipal;
import com.mmedic.security.MedicGrantedAuthority;
import org.springframework.security.core.Authentication;
import org.springframework.util.CollectionUtils;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/**
 * Handler for Annotation check access.
 *
 * @author hungp
 */
public abstract class AnnotationHandler {

    public abstract boolean handleScope(Authentication authentication);

    public abstract boolean support(Class<? extends Annotation> clazz);

    protected List<MedicGrantedAuthority.Scope> getScopes(Authentication authentication) {
        List<MedicGrantedAuthority.Scope> scopes = new ArrayList<>();
        if (null == authentication) {
            return null;
        }
        final AccountPrincipal principal = (AccountPrincipal) authentication.getPrincipal();
        List<MedicGrantedAuthority> authorities = (List<MedicGrantedAuthority>) principal.getAuthorities();
        authorities.forEach(medicGrantedAuthority -> {
            if (!CollectionUtils.isEmpty(medicGrantedAuthority.getScopes())) {
                scopes.addAll(medicGrantedAuthority.getScopes());
            }
        });
        return scopes;
    }

}
