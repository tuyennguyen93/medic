package com.mmedic.security.scope.handler;

import com.mmedic.security.scope.Admin;
import org.springframework.security.core.Authentication;

import java.lang.annotation.Annotation;

/**
 * Handler for Admin.Record.ReadWrite scope.
 *
 * @author hungp
 */
public class AdminRecordReadWriterHandler extends AnnotationHandler {

    @Override
    public boolean handleScope(Authentication authentication) {
        return getScopes(authentication).stream().
                anyMatch(scope -> scope.getScope().equalsIgnoreCase(Admin.Record.ReadWrite.SCOPE));
    }

    @Override
    public boolean support(Class<? extends Annotation> annotation) {
        return Admin.Record.ReadWrite.class.isAssignableFrom(annotation);
    }
}
