package com.mmedic.security.scope.handler;

import com.mmedic.security.scope.Admin;
import org.springframework.security.core.Authentication;

import java.lang.annotation.Annotation;

/**
 * Handler for Admin.Record.ReadOnly scope.
 *
 * @author hungp
 */
public class AdminRecordReadOnlyHandler extends AnnotationHandler {

    @Override
    public boolean handleScope(Authentication authentication) {
        return getScopes(authentication).stream().
                anyMatch(scope -> scope.getScope().equalsIgnoreCase(Admin.Record.ReadOnly.SCOPE) ||
                        scope.getScope().equalsIgnoreCase(Admin.Record.ReadWrite.SCOPE));
    }

    @Override
    public boolean support(Class<? extends Annotation> clazz) {
        return Admin.Record.ReadOnly.class.isAssignableFrom(clazz);
    }
}
