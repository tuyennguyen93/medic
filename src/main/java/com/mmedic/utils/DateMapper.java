package com.mmedic.utils;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Component
public class DateMapper {

    public LocalDateTime toLocalDateTime(LocalDate localDate) {
        LocalDateTime localDateTime = null;
        if (null != localDate) {
            localDateTime = LocalDateTime.of(localDate, LocalTime.of(0, 0));
        }
        return localDateTime;
    }

    public LocalDate toLocalDate(LocalDateTime dateTime) {
        LocalDate localDate = null;
        if (null != dateTime) {
            localDate = dateTime.toLocalDate();
        }
        return localDate;
    }
}
