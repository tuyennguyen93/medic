package com.mmedic.utils;

public class ValidationUtils {

    public static void guardEquals(Object obj1, Object obj2, String msg) {
        if (obj1 == obj2) {
            return;
        }
        if (obj1 == null || !obj1.equals(obj2)) {
            throw new IllegalArgumentException("guardEquals failed for " + obj1 + " and " + obj2 + ": " + msg);
        }
    }



}
