package com.mmedic.utils;

/**
 * Application constants.
 */
public class Constants {

    /**
     * Spring profile development.
     */
    public static final String SPRING_PROFILE_LOCAL = "local";

    /**
     * Spring profile development.
     */
    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";

    /**
     * Spring profile  production.
     */
    public static final String SPRING_PROFILE_PRODUCTION = "prod";

    /**
     * Super Admin role.
     */
    public static final String ROLE_SUPER_ADMIN = "SUPER_ADMIN";

    /**
     * Admin role.
     */
    public static final String ROLE_ADMIN = "ADMIN";

    /**
     * Doctor admin role.
     */
    public static final String ROLE_DOCTOR_ADMIN = "DOCTOR_ADMIN";

    /**
     * Doctor staff role.
     */
    public static final String ROLE_DOCTOR_STAFF = "DOCTOR_STAFF";

    /**
     * Nurse admin role.
     */
    public static final String ROLE_NURSE_ADMIN = "NURSE_ADMIN";

    /**
     * Nurse staff role.
     */
    public static final String ROLE_NURSE_STAFF = "NURSE_STAFF";

    /**
     * Preclinic role.
     */
    public static final String ROLE_PRECLINIC_ADMIN = "PRECLINIC_ADMIN";

    /**
     * Preclinic staff role.
     */
    public static final String ROLE_PRECLINIC_STAFF = "PRECLINIC_STAFF";

    /**
     * Drug store admin role.
     */
    public static final String ROLE_DRUG_STORE_ADMIN = "DRUG_STORE_ADMIN";

    /**
     * Drug store staff role.
     */
    public static final String ROLE_DRUG_STORE_STAFF = "DRUG_STORE_STAFF";

    /**
     * Patient role.
     */
    public static final String ROLE_PATIENT = "PATIENT";

    /**
     * Empty string.
     */
    public static final String EMPTY_STRING = "";

    /**
     * Space String.
     */
    public static final String SPACE_STRING = " ";

    /**
     * Slash.
     */
    public static final String SLASH = "/";

    /**
     * Dot.
     */
    public static final String DOT = ".";

    /**
     * Dot regex.
     */
    public static final String DOT_REGEX = "\\.";

    /**
     * Underscore.
     */
    public static final String UNDERSCORE = "_";

    /**
     * Percent.
     */
    public static final String PERCENT = "%";

    /**
     * Treatment service group.
     */
    public static final int GROUP_SERVICE_TREATMENT = 1;

    /**
     * Test service group.
     */
    public static final int GROUP_SERVICE_TEST = 2;

    /**
     * Image diagnose service group.
     */
    public static final int GROUP_SERVICE_IMAGE_DIAGNOSE = 3;

    /**
     * Medicine supply service group.
     */
    public static final int GROUP_SERVICE_MEDICINE_SUPPLY = 4;

    /**
     * Treatment medical service.
     */
    public static final int SERVICE_TREATMENT_MEDICAL = 1;

    /**
     * Treatment transport service.
     */
    public static final int SERVICE_TREATMENT_TRANSPORT = 2;

    /**
     * Medicine supply service.
     */
    public static final int SERVICE_MEDICINE_SUPPLY = 3;

    /**
     * Medicine delivery service.
     */
    public static final int SERVICE_MEDICINE_DELIVERY = 4;

    /**
     * Make test in HOME.
     */
    public static final int SERVICE_TEST_HOME = 5;

    /**
     * Make test in CLINIC.
     */
    public static final int SERVICE_TEST_CLINIC = 6;

    /**
     * Treatment fee type.
     */
    public static final String TREATMENT_FEE_TYPE = "TREATMENT";

    /**
     * Transport fee type.
     */
    public static final String TRANSPORT_FEE_TYPE = "TRANSPORT";

    /**
     * Prefix of Sequence number for status process.
     */
    public static final int SEQUENCE_NUMBER_PROCESS = 1;

    /**
     * Prefix of Sequence number for status complete.
     */
    public static final int SEQUENCE_NUMBER_COMPLETE = 2;

    /**
     * Sequence status key.
     */
    public static final String SEQUENCE_NUMBER_STATUS = "status";

    /**
     * Sequence status index key.
     */
    public static final String SEQUENCE_NUMBER_INDEX = "indexOfWorkingScheduleId";

    /**
     * Sequence status number key.
     */
    public static final String SEQUENCE_NUMBER = "sequenceNumber";

    /**
     * Date format.
     */
    public static final String DATE_FORMAT = "dd-MM-yyyy";

    /**
     * Separate token regex.
     */
    public static final String SEPARATE_TOKEN_REGEX = "\\|";

    /**
     * Separate token.
     */
    public static final String SEPARATE_TOKEN = "|";

    /**
     * Separate preclinical result link.
     */
    public static final String SEPARATE_PRECLINICAL_RESULT_LINK = "  ";

    /**
     * Zalopay transaction key.
     */
    public static final String ZALOPAY_TRANSACTION = "apptransid";

    /**
     * Zalopay item key.
     */
    public static final String ZALOPAY_ITEM = "item";

    /**
     * Zalopay amount key.
     */
    public static final String ZALOPAY_AMOUNT = "amount";

    /**
     * Zalopay server time.
     */
    public static final String ZALOPAY_SERVER_TIME = "servertime";

    /**
     * Zalopay order id key.
     */
    public static final String ZALOPAY_ITEM_ORDER_ID = "orderId";

    /**
     * Zalopay medical record id key.
     */
    public static final String ZALOPAY_ITEM_MEDICAL_RECORD_ID = "medicalRecordId";

    /**
     * Zalopay type key.
     */
    public static final String ZALOPAY_ITEM_TYPE = "type";

    /**
     * Separator.
     */
    public static final String SEPARATOR = System.getProperty("file.separator");

    /**
     * Project temp directory.
     */
    public static final String PROJECT_TEMP_DIRECTORY =  System.getProperty("java.io.tmpdir") + SEPARATOR + "mmedic" + SEPARATOR;

    private Constants() {
    }
}
