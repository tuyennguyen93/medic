package com.mmedic.utils;

import com.mmedic.entity.Account;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.security.MedicGrantedAuthority;
import com.mmedic.spring.errors.MedicException;
import org.springframework.security.core.Authentication;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Helper class.
 */
public class Helper {

    private Helper() {
    }

    /**
     * Validation email value.
     *
     * @param email the email to validate
     * @return true if email value is valid
     */
    public static boolean validateEmail(String email) {
        return email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$");
    }

    /**
     * Check if string is numeric.
     *
     * @param number the number string to check
     * @return true if string is numeric
     */
    public static boolean isNumeric(String number) {
        if (!StringUtils.isEmpty(number)) {
            return number.matches("-?\\d+(\\.\\d+)?");
        }
        return false;
    }

    /**
     * Validate phone number.
     *
     * @param phoneNumber phone number
     * @return true if string is phone number
     */
    public static boolean validatePhoneNumber(String phoneNumber) {
        if (!StringUtils.isEmpty(phoneNumber)) {
            return phoneNumber.matches("(0)?[1-9][0-9]{8,9}");
        }
        return false;
    }

    /**
     * Check role for account.
     *
     * @param account the account
     * @param roles   roles need check.
     * @return true if account has any role
     */
    public static boolean accountHasAnyRole(Account account, String... roles) {
        return Arrays.asList(roles).stream().anyMatch(role -> role.equals(account.getRole().getName()));
    }

    public static boolean authenticationHasAnyRole(Authentication authentication, String... roles) {
        if (!CollectionUtils.isEmpty(authentication.getAuthorities())) {
            MedicGrantedAuthority grantedAuthority = authentication.getAuthorities().stream().
                    map(MedicGrantedAuthority.class::cast).
                    findFirst().orElse(null);
            if (grantedAuthority != null) {
                return Arrays.asList(roles).stream().map(role -> "ROLE_" + role).collect(Collectors.toList()).stream().
                        anyMatch(role -> role.equals(grantedAuthority.getRole()));
            }
        }
        return false;
    }

    public static String convertNullToEmpty(String str) {
        return null == str ? Constants.EMPTY_STRING : str;
    }

    public static String censorData(String data) {
        String censorData = Constants.EMPTY_STRING;
        StringBuilder sb = new StringBuilder();
        if (!StringUtils.isEmpty(data)) {
            String[] words = data.split(Constants.SPACE_STRING);

            Arrays.stream(words).forEach(word -> {
                if (isNumeric(word) && word.length() > 6) {
                    sb.append(word.substring(0, 3)).append("***").append(word.substring(word.length() - 3));
                } else {
                    sb.append(word.charAt(0)).append("***");
                }
                sb.append(" ");
            });
        }
        return StringUtils.trimWhitespace(sb.toString());
    }

    public static double distance(double latitude, double longitude, double latitudeDes, double longitudeDes) {
        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(latitudeDes - latitude);
        double lonDistance = Math.toRadians(longitudeDes - longitude);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(latitudeDes))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters


        distance = Math.pow(distance, 2);

        return Math.sqrt(distance);
    }

    public static int getCurrentAge(LocalDate birthday) {
        LocalDate today = LocalDate.now();                          //Today's date
        Period p = Period.between(birthday, today);
        return p.getYears();
    }

    public static boolean isOver3Minutes(LocalDateTime time) {
        Long seconds = getTimeDuration(time);
        return (seconds > 180);
    }

    public static Long getTimeDuration(LocalDateTime time) {
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(time, now);
        Long seconds = duration.getSeconds();
        return seconds;
    }

    /**
     * Format Sequence Number.
     *
     * @param status                   status
     * @param indexOfWorkingScheduleId index Of Working schedule id
     * @param sequenceNumber           sequence number
     * @return the number is formatted
     */
    public static int formatSequenceNumber(int status, int indexOfWorkingScheduleId, Integer sequenceNumber) {
        StringBuilder result = new StringBuilder("0");
        // Set status
        if (Constants.SEQUENCE_NUMBER_PROCESS == status) {
            result.setLength(0);
            result.append(status);
            result.append(String.format("%04d", indexOfWorkingScheduleId));
            if (null != sequenceNumber) {
                result.append(sequenceNumber);
            }
        } else if (Constants.SEQUENCE_NUMBER_COMPLETE == status && null != sequenceNumber) {
            result.setLength(0);
            result.append(status);
            result.append(String.format("%04d", indexOfWorkingScheduleId));
            result.append(sequenceNumber);
        }
        return Integer.parseInt(result.toString());
    }


    /**
     * Split sequence number is formatted to map.
     *
     * @param sequenceNumberFormat sequence number format
     * @return map12345
     */
    public static Map<String, Integer> getSequenceNumber(Integer sequenceNumberFormat) {
        Map<String, Integer> sequenceNumberMap = new HashMap<>();
        String statusStr = "0";
        String indexOfWorkingScheduleIdStr = "0";
        String sequenceNumberStr = "0";
        if (null != sequenceNumberFormat) {
            String sequenceNumber = String.valueOf(sequenceNumberFormat);
            if (sequenceNumber.length() >= 5) {
                statusStr = sequenceNumber.substring(0, 1);
                indexOfWorkingScheduleIdStr = sequenceNumber.substring(1, 5);
                sequenceNumberStr = sequenceNumber.substring(5);
            } else {
                sequenceNumberStr = sequenceNumber;
            }
        }
        // convert string to int
        sequenceNumberMap.put(Constants.SEQUENCE_NUMBER_STATUS, Integer.parseInt(!StringUtils.isEmpty(statusStr) ? statusStr : "0"));
        sequenceNumberMap.put(Constants.SEQUENCE_NUMBER_INDEX, Integer.parseInt(!StringUtils.isEmpty(indexOfWorkingScheduleIdStr) ? indexOfWorkingScheduleIdStr : "0"));
        sequenceNumberMap.put(Constants.SEQUENCE_NUMBER, Integer.parseInt(!StringUtils.isEmpty(sequenceNumberStr) ? sequenceNumberStr : "0"));
        return sequenceNumberMap;
    }

    public static String convertQueryParamToSql(String query) {
        return "%" + convertNullToEmpty(query) + "%";
    }

    /**
     * Get Hmac SHA256.
     *
     * @param key the secret key
     * @return Mac instance
     */
    public static Mac getHmacSHA256(String key) {
        Mac hmacSHA256;
        try {
            hmacSHA256 = Mac.getInstance("HmacSHA256");
            hmacSHA256.init(new SecretKeySpec(key.getBytes(), "HmacSHA256"));
        } catch (NoSuchAlgorithmException e) {
            throw new MedicException(MedicException.ERROR_CRYPTO_CAN_NOT_GET_INSTANCE, "Can't get instance HmacSHA256");
        } catch (InvalidKeyException e) {
            throw new MedicException(MedicException.ERROR_CRYPTO_INVALID_KEY, "HmacSHA256: Invalid key");
        }
        return hmacSHA256;
    }

    /**
     * Format date dd-MM-yyyy.
     *
     * @param date date
     * @return date is format
     */
    public static String formatDate(LocalDateTime date) {
        String formatResult = Constants.EMPTY_STRING;
        if (null != date) {
            formatResult = date.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT));
        }
        return formatResult;
    }

    /**
     * Format OTP message verify.
     *
     * @param otp otp value
     * @return message
     */
    public static String formatOTPMessageVerify(String otp) {
        return String.format("MMedic - Ma OTP la %s. Vui long giu bao mat va khong chia se OTP cho bat cu ai.", convertNullToEmpty(otp));
    }


    /**
     * Get temp file patch
     *
     * @param pathDirectory
     * @param fileName
     * @return
     * @throws IOException
     */
    public static String getTempFilePath(String pathDirectory, String fileName) throws IOException {
        Path path = Paths.get(Constants.PROJECT_TEMP_DIRECTORY + pathDirectory);
        //if directory exists?
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
        return Constants.PROJECT_TEMP_DIRECTORY + pathDirectory + Constants.SEPARATOR + fileName;
    }

    /**
     * Check preclinical order is service.
     *
     * @param serviceOrderDetail service order detail
     * @return true if it's service
     */
    public static boolean checkPreclinicalOrderDetailIsService(MedicalServiceOrderDetail serviceOrderDetail) {
        return null != serviceOrderDetail &&
                null != serviceOrderDetail.getMedicalService() &&
                null != serviceOrderDetail.getMedicalService().getMedicalServiceGroup() &&
                serviceOrderDetail.getMedicalService().getMedicalServiceGroup().getId() == Constants.GROUP_SERVICE_TEST &&
                (serviceOrderDetail.getMedicalService().getId() != Constants.SERVICE_TEST_HOME &&
                        serviceOrderDetail.getMedicalService().getId() != Constants.SERVICE_TEST_CLINIC);
    }

    /**
     * Check preclinical order is not service.
     *
     * @param serviceOrderDetail service order detail
     * @return true if it's not service
     */
    public static boolean checkPreclinicalOrderDetailIsNotService(MedicalServiceOrderDetail serviceOrderDetail) {
        return null != serviceOrderDetail &&
                null != serviceOrderDetail.getMedicalService() &&
                null != serviceOrderDetail.getMedicalService().getMedicalServiceGroup() &&
                serviceOrderDetail.getMedicalService().getMedicalServiceGroup().getId() == Constants.GROUP_SERVICE_TEST &&
                !(serviceOrderDetail.getMedicalService().getId() != Constants.SERVICE_TEST_HOME &&
                        serviceOrderDetail.getMedicalService().getId() != Constants.SERVICE_TEST_CLINIC);
    }

    public static boolean isNotificationTransport(MedicalServiceOrderDetail transportService) {
        return !MedicalServiceOrderStatus.REQUEST_SENT.equals(transportService.getStatus()) &&
                !MedicalServiceOrderStatus.REQUEST_ACCEPTED.equals(transportService.getStatus());
    }

}
