alter table medical_service_group
    add column is_disabled TINYINT(1) null default 0;