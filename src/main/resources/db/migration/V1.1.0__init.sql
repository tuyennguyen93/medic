/*------------------------------------------------
 *- SYSTEM SETTING                               -
 *-----------------------------------------------*/
create table if not exists sys_setting
(
    id        int(11) auto_increment,
    `key`     nvarchar(255) not null,
    value     nvarchar(255) not null,
    update_by int(11)       null,
    update_at timestamp     null,
    primary key (id)
) charset = utf8;

/*------------------------------------------------
 *- MEDICAL DEPARTMENT                           -
 *-----------------------------------------------*/
create table if not exists medical_department
(
    id          int(11) auto_increment,
    name        nvarchar(255) not null,
    description text          null,
    is_deleted           tinyint(1)     null default 0,
    create_at   timestamp     null,
    update_at   timestamp     null,
    primary key (id)
) charset = utf8;

/*------------------------------------------------
 *- DISEASE                                      -
 *-----------------------------------------------*/
create table if not exists disease
(
    id                    int(11) auto_increment,
    medical_department_id int(11)       not null,
    name                  nvarchar(255) not null,
    description           text          null,
    is_deleted           tinyint(1)     null default 0,
    create_at             timestamp     null,
    update_at             timestamp     null,
    primary key (id),
    foreign key (medical_department_id) references medical_department (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- ESTABLISHMENT                                -
 *-----------------------------------------------*/
create table if not exists establishment
(
    id                    int(11) auto_increment,
    name                  nvarchar(255)                                                             not null,
    address               nvarchar(255)                                                             not null,
    phone_number          nvarchar(255)                                                             null,
    avatar_url            text                                                                      null,
    rating_average        float                                                                     null default 0,
    working_schedule      json                                                                      null,
    establishment_type    enum ('MEDICAL_TREATMENT', 'PRECLINICAL', 'DRUG_STORE', 'DRUG_WAREHOUSE') not null,
    medical_department_id int(11)                                                                   null,
    create_at             timestamp                                                                 null,
    primary key (id),
    foreign key (medical_department_id) references medical_department (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- ESTABLISHMENT DAY OFF                        -
 *-----------------------------------------------*/
create table if not exists establishment_day_off
(
    id               int(11) auto_increment,
    establishment_id int(11)   not null,
    start_date       timestamp not null default current_timestamp,
    end_date         timestamp not null default current_timestamp,
    primary key (id),
    foreign key (establishment_id) references establishment (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- ESTABLISHMENT WORKING SCHEDULE               -
 *-----------------------------------------------*/
create table if not exists establishment_working_schedule
(
    id               int(11) auto_increment,
    establishment_id int(11)                                                   not null,
    start_time       time                                                      not null,
    end_time         time                                                      not null,
    day_of_week      enum ('MON', 'TUE', ' WED', 'THU', ' FRI', ' SAT', 'SUN') not null,
    primary key (id),
    foreign key (establishment_id) references establishment (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- ESTABLISHMENT CERTIFICATION                  -
 *-----------------------------------------------*/
create table if not exists establishment_certification
(
    id                int(11) auto_increment,
    establishment_id  int(11)       not null,
    name              nvarchar(255) not null,
    certification_url text          not null,
    create_at         timestamp     null,
    update_at         timestamp     null,
    primary key (id),
    foreign key (establishment_id) references establishment (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- ROLE                                         -
 *-----------------------------------------------*/
create table if not exists role
(
    id          int(11) auto_increment,
    name        nvarchar(255) not null,
    description nvarchar(255) null,
    primary key (id)
) charset = utf8;

/*------------------------------------------------
 *- ROLE PERMISSION                              -
 *-----------------------------------------------*/
create table if not exists role_permission
(
    id              int(11) auto_increment,
    role_id         int(11)       not null,
    scope           nvarchar(255) not null,
    `condition`     nvarchar(255) null,
    condition_value nvarchar(255) null,
    primary key (id),
    foreign key (role_id) references role (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- ACCOUNT                                      -
 *-----------------------------------------------*/
create table if not exists account
(
    id               int(11) auto_increment,
    role_id          int(11)       not null,
    establishment_id int(11)       null,
    name             nvarchar(255) not null,
    title            nvarchar(255) null,
    date_of_birth    date          not null,
    avatar_url       text          null,
    create_at        timestamp     null,
    primary key (id),
    foreign key (role_id) references role (id) on update restrict on delete restrict,
    foreign key (establishment_id) references establishment (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- AUTH METHOD                                  -
 *-----------------------------------------------*/
create table if not exists auth_method
(
    id          int(11) auto_increment,
    account_id  int(11)         not null,
    auth_type   enum ('1', '2') not null,
    auth_data_1 text            null,
    auth_data_2 text            null,
    auth_data_3 text            null,
    primary key (id),
    foreign key (account_id) references account (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- PATIENT                                      -
 *-----------------------------------------------*/
create table if not exists patient
(
    id            int(11) auto_increment,
    account_id    int(11)         not null,
    name          nvarchar(255)   not null,
    date_of_birth date            not null,
    gender        enum ('M', 'F') not null,
    address       nvarchar(255)   null,
    phone_number  nvarchar(255)   not null,
    create_at     timestamp       null,
    primary key (id),
    foreign key (account_id) references account (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- CONSTANT MEDICAL INFO                        -
 *-----------------------------------------------*/
create table if not exists constant_medical_info
(
    id         int(11) auto_increment,
    patient_id int(11)       not null,
    info_key   nvarchar(255) not null,
    info_value json          not null,
    info_unit  nvarchar(255) not null,
    create_at  timestamp     null,
    primary key (id),
    foreign key (patient_id) references patient (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- VARIABLE MEDICAL INFO                        -
 *-----------------------------------------------*/
create table if not exists variable_medical_info
(
    id         int(11) auto_increment,
    patient_id int(11)       not null,
    info_key   nvarchar(255) not null,
    info_value json          not null,
    info_unit  nvarchar(255) not null,
    create_at  timestamp     null,
    primary key (id),
    foreign key (patient_id) references patient (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- MEDICAL RECORD                               -
 *-----------------------------------------------*/
create table if not exists medical_record
(
    id          int(11) auto_increment,
    patient_id  int(11)                       not null,
    record_type enum ('TREATMENT', 'SERVICE') not null,
    create_at   timestamp                     null,
    primary key (id),
    foreign key (patient_id) references patient (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- TRANSACTION                                  -
 *-----------------------------------------------*/
create table if not exists transaction
(
    id           int(11) auto_increment,
    patient_id   int(11)   not null,
    total_amount bigint    not null,
    memo         text      null,
    create_at    timestamp null,
    primary key (id),
    foreign key (patient_id) references patient (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- TRANSACTION OPERATION                        -
 *-----------------------------------------------*/
create table if not exists transaction_operation
(
    id             int(11) auto_increment,
    transaction_id int(11) not null,
    amount         bigint  not null,
    memo           text    null,
    primary key (id),
    foreign key (transaction_id) references transaction (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- MEDICAL SERVICE GROUP                        -
 *-----------------------------------------------*/
create table if not exists medical_service_group
(
    id          int(11) auto_increment,
    name        nvarchar(255) not null,
    description text          null,
    create_at   timestamp     null,
    update_at   timestamp     null,
    primary key (id)
) charset = utf8;

/*------------------------------------------------
 *- MEDICAL SERVICE                              -
 *-----------------------------------------------*/
create table if not exists medical_service
(
    id                       int(11) auto_increment,
    medical_service_group_id int(11)       not null,
    name                     nvarchar(255) not null,
    description              text          null,
    create_at                timestamp     null,
    update_at                timestamp     null,
    primary key (id),
    foreign key (medical_service_group_id) references medical_service_group (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- MEDICAL SERVICE REGISTRY                     -
 *-----------------------------------------------*/
create table if not exists medical_service_registry
(
    id                 int(11) auto_increment,
    establishment_id   int(11)    not null,
    medical_service_id int(11)    not null,
    active             tinyint(1) not null,
    create_at          timestamp  null,
    update_at          timestamp  null,
    primary key (id),
    foreign key (establishment_id) references establishment (id) on update restrict on delete restrict,
    foreign key (medical_service_id) references medical_service (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- MEDICAL SERVICE PRICE HISTORY                -
 *-----------------------------------------------*/
create table if not exists medical_service_price_history
(
    id                          int(11) auto_increment,
    medical_service_registry_id int(11)   not null,
    price                       int(11)   not null,
    effective_at                timestamp not null default current_timestamp,
    create_at                   timestamp null,
    primary key (id),
    foreign key (medical_service_registry_id) references medical_service_registry (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- MEDICAL SERVICE ORDER                        -
 *-----------------------------------------------*/
create table if not exists medical_service_order
(
    id                                int(11) auto_increment,
    medical_service_id                int(11)                                                                          not null,
    establishment_working_schedule_id int(11)                                                                          not null,
    sequence_number                   int(11)                                                                          null,
    account_id                        int(11)                                                                          not null,
    status                            enum ('REQUEST_SENT', 'REQUEST_CANCELLED', 'REQUEST_DENIED', 'REQUEST_ACCEPTED') not null,
    result_appointment                timestamp                                                                        null,
    transaction_operation_id          int(11)                                                                          not null,
    create_at                         timestamp                                                                        null,
    primary key (id),
    foreign key (medical_service_id) references medical_service (id) on update restrict on delete restrict,
    foreign key (establishment_working_schedule_id) references establishment_working_schedule (id) on update restrict on delete restrict,
    foreign key (account_id) references account (id) on update restrict on delete restrict,
    foreign key (transaction_operation_id) references transaction_operation (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- SERVICE RECORD                               -
 *-----------------------------------------------*/
create table if not exists service_record
(
    id                       int(11) auto_increment,
    medical_record_id        int(11) not null,
    medical_service_order_id int(11) not null,
    primary key (id),
    foreign key (medical_record_id) references medical_record (id) on update restrict on delete restrict,
    foreign key (medical_service_order_id) references medical_service_order (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- MEDICAL SERVICE ORDER DISCUSSION             -
 *-----------------------------------------------*/
create table if not exists medical_service_order_discussion
(
    id                       int(11) auto_increment,
    medical_service_order_id int(11)   not null,
    account_id               int(11)   not null,
    message                  text      null,
    create_at                timestamp null,
    primary key (id),
    foreign key (medical_service_order_id) references medical_service_order (id) on update restrict on delete restrict,
    foreign key (account_id) references account (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- EXAMINE RECORD                               -
 *-----------------------------------------------*/
create table if not exists examine_record
(
    id                       int(11) auto_increment,
    medical_record_id        int(11)   not null,
    symptom                  text      null,
    diagnose                 text      null,
    remind                   text      null,
    medical_service_order_id int(11)   not null,
    create_at                timestamp null,
    update_at                timestamp null,
    primary key (id),
    foreign key (medical_record_id) references medical_record (id) on update restrict on delete restrict,
    foreign key (medical_service_order_id) references medical_service_order (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- PRECLINICAL RECORD                           -
 *-----------------------------------------------*/
create table if not exists preclinical_record
(
    id                       int(11) auto_increment,
    medical_record_id        int(11)   not null,
    medical_service_order_id int(11)   not null,
    result_url               text      null,
    create_at                timestamp null,
    primary key (id),
    foreign key (medical_record_id) references medical_record (id) on update restrict on delete restrict,
    foreign key (medical_service_order_id) references medical_service_order (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- DRUG                                         -
 *-----------------------------------------------*/
create table if not exists drug
(
    id               int(11) auto_increment,
    name             nvarchar(255) not null,
    parent_id        int(11)       null,
    can_prescribe    tinyint(1)    not null,
    unit             nvarchar(255) not null,
    origin           nvarchar(255) not null,
    contraindication text          null,
    create_at        timestamp     null,
    update_at        timestamp     null,
    primary key (id),
    foreign key (parent_id) references drug (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- DRUG PRICE HISTORY                           -
 *-----------------------------------------------*/
create table if not exists drug_price_history
(
    id             int(11) auto_increment,
    drug_id        int(11)   not null,
    price_per_unit int(11)   not null,
    effective_at   timestamp not null default current_timestamp,
    create_at      timestamp null,
    primary key (id),
    foreign key (drug_id) references drug (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- DRUG IMPORT                                  -
 *-----------------------------------------------*/
create table if not exists drug_import
(
    id                 int(11) auto_increment,
    to_establishment   int(11)   not null,
    from_establishment int(11)   not null,
    create_by          int(11)   null,
    create_at          timestamp null,
    primary key (id),
    foreign key (to_establishment) references establishment (id) on update restrict on delete restrict,
    foreign key (from_establishment) references establishment (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- DRUG IMPORT DETAIL                           -
 *-----------------------------------------------*/
create table if not exists drug_import_detail
(
    id             int(11) auto_increment,
    drug_import_id int(11)   not null,
    drug_id        int(11)   not null,
    amount         int(11)   not null,
    price          int(11)   not null,
    expiration     timestamp not null,
    primary key (id),
    foreign key (drug_import_id) references drug_import (id) on update restrict on delete restrict,
    foreign key (drug_id) references drug (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- DRUG EXPORT                                  -
 *-----------------------------------------------*/
create table if not exists drug_export
(
    id                 int(11) auto_increment,
    from_establishment int(11)   not null,
    create_by          int(11)   null,
    create_at          timestamp null,
    primary key (id),
    foreign key (from_establishment) references establishment (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- DRUG EXPORT DETAIL                           -
 *-----------------------------------------------*/
create table if not exists drug_export_detail
(
    id                    int(11) auto_increment,
    drug_export_id        int(11) not null,
    drug_import_detail_id int(11) not null,
    amount                int(11) not null,
    price                 int(11) not null,
    primary key (id),
    foreign key (drug_export_id) references drug_export (id) on update restrict on delete restrict,
    foreign key (drug_import_detail_id) references drug_import_detail (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- PRESCRIPTION RECORD                          -
 *-----------------------------------------------*/
create table if not exists prescription_record
(
    id                       int(11) auto_increment,
    medical_record_id        int(11)   not null,
    medical_service_order_id int(11)   not null,
    drug_export_id           int(11)   not null,
    create_at                timestamp null,
    primary key (id),
    foreign key (medical_record_id) references medical_record (id) on update restrict on delete restrict,
    foreign key (medical_service_order_id) references medical_service_order (id) on update restrict on delete restrict,
    foreign key (drug_export_id) references drug_export (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- PRESCRIPTION DETAIL                          -
 *-----------------------------------------------*/
create table if not exists prescription_detail
(
    id                     int(11) auto_increment,
    prescription_record_id int(11) not null,
    drug_id                int(11) not null,
    amount                 int(11) not null,
    `usage`                text    not null,
    primary key (id),
    foreign key (prescription_record_id) references prescription_record (id) on update restrict on delete restrict,
    foreign key (drug_id) references drug (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- MEDICINE ALLERGY                             -
 *-----------------------------------------------*/
create table if not exists medicine_allergy
(
    id         int(11) auto_increment,
    patient_id int(11)   not null,
    drug_id    int(11)   not null,
    create_at  timestamp null,
    primary key (id),
    foreign key (patient_id) references patient (id) on update restrict on delete restrict,
    foreign key (drug_id) references drug (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- ESTABLISHMENT RATING                         -
 *-----------------------------------------------*/
create table if not exists establishment_rating
(
    id                int(11) auto_increment,
    medical_record_id int(11)                    not null,
    establishment_id  int(11)                    not null,
    rate              enum ('1','2','3','4','5') not null,
    comment           text                       null,
    create_at         timestamp                  null,
    primary key (id),
    foreign key (medical_record_id) references medical_record (id) on update restrict on delete restrict,
    foreign key (establishment_id) references establishment (id) on update restrict on delete restrict
) charset = utf8;