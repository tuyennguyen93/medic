alter table prescription_detail
    add column medical_service_order_detail_id int(11) null,
    add foreign key (medical_service_order_detail_id) references medical_service_order_detail (id) on update restrict on delete restrict;