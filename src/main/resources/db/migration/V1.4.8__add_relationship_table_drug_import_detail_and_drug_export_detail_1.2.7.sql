/* -- CLEAR DATA -- */
delete from drug_export_detail;

/* -- MODIFY DRUG_EXPORT_DETAIL TABLE -- */
alter table drug_export_detail
    add column drug_import_detail_id int(11) null,
    add foreign key (drug_import_detail_id) references drug_import_detail (id) on update restrict on delete restrict;