alter table drug_import drop foreign key drug_import_ibfk_2,
  drop column from_establishment,
  drop column is_deleted;

alter table drug_export drop column is_deleted;

alter table drug_import_detail drop column code;

alter table drug_export_detail drop column code;

alter table drug_unit drop column is_deleted;

ALTER TABLE drug CHANGE is_deleted is_disabled tinyint(1) null default 0;