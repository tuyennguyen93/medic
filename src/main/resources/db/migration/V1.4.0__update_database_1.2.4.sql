/* -- MODIFY PRECLINICAL_RECORD TABLE -- */
alter table preclinical_record
    drop column result_url;

/* -- MODIFY DRUG_EXPORT TABLE -- */
alter table drug_export change from_establishment export_from int (11);
alter table drug_export
    drop column create_by;
/*------------------------------------------------
 *- DRUG INTERNAL RECORD                         -
 *-----------------------------------------------*/
create table if not exists drug_internal_export
(
    id        int(11) auto_increment,
    export_id int(11) not null,
    export_to int(11) not null,
    create_by int(11) null,
    primary key (id),
    foreign key (export_id) references drug_export (id) on update restrict on delete restrict,
    foreign key (export_to) references establishment (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- DRUG ORDER EXPORT                            -
 *-----------------------------------------------*/
create table if not exists drug_order_export
(
    id                     int(11) auto_increment,
    drug_export_id         int(11) not null,
    prescription_record_id int(11) not null,
    primary key (id),
    foreign key (drug_export_id) references drug_export (id) on update restrict on delete restrict,
    foreign key (prescription_record_id) references prescription_record (id) on update restrict on delete restrict
) charset = utf8;

/* -- MODIFY DRUG_IMPORT_DETAIL TABLE -- */
alter table drug_import_detail change price price_per_unit int (11);
ALTER TABLE drug_import_detail MODIFY price_per_unit int(11) not null;

/* -- MODIFY DRUG_EXPORT_DETAIL TABLE -- */
alter table drug_export_detail change price price_per_unit int (11);
ALTER TABLE drug_export_detail MODIFY price_per_unit int(11) not null;
alter table drug_export_detail drop foreign key drug_export_detail_ibfk_2;
alter table drug_export_detail drop column drug_import_detail_id;

/* -- DROP DRUG_PRICE_HISTORY TABLE -- */
drop table if exists drug_price_history;

/*------------------------------------------------
 *- DRUG UNIT PRICE HISTORY                      -
 *-----------------------------------------------*/
create table if not exists drug_unit_price_history
(
    id             int(11) auto_increment,
    drug_unit_id   int(11) not null,
    price_per_unit int(11) not null,
    effective_at   timestamp not null,
    create_at      timestamp null,
    primary key (id),
    foreign key (drug_unit_id) references drug_unit (id) on update restrict on delete restrict
) charset = utf8;

