/* -- DROP COLUMN MEDICAL_SERVICE_ID IN MEDICAL_SERVICE_ORDER -- */
alter table medical_service_order
    drop foreign key medical_service_order_ibfk_1;
alter table medical_service_order
    drop column medical_service_id;

/*------------------------------------------------
 *- MEDICAL SERVICE ORDER DETAIL                 -
 *-----------------------------------------------*/
create table if not exists medical_service_order_detail
(
    id                       int(11) auto_increment,
    medical_service_order_id int(11)      not null,
    medical_service_id       int(11)      not null,
    note                     varchar(255) null,
    create_at                timestamp    null,
    primary key (id),
    foreign key (medical_service_order_id) references medical_service_order (id) on update restrict on delete restrict,
    foreign key (medical_service_id) references medical_service (id) on update restrict on delete restrict
) charset = utf8;

/*------------------------------------------------
 *- DRUG UNIT                                    -
 *-----------------------------------------------*/
create table if not exists drug_unit
(
    id                       int(11) auto_increment,
    drug_id                  int(11)      not null,
    parent_id                int(11)      null,
    name                     varchar(255) not null,
    parent_equivalent_amount int(11)      null,
    create_at                timestamp    null,
    primary key (id),
    foreign key (drug_id) references drug (id) on update restrict on delete restrict,
    foreign key (parent_id) references drug_unit (id) on update restrict on delete restrict
) charset = utf8;

/** -- DROP COLUMN DRUG_ID AND ADD COLUMN DRUG_UNIT_ID IN PRESCRIPTION_DETAIL TABLE -- */
alter table prescription_detail
    drop foreign key prescription_detail_ibfk_2;
alter table prescription_detail
    drop column drug_id;

alter table prescription_detail
    add column drug_unit_id int(11) not null,
    add foreign key (drug_unit_id) references drug_unit (id) on update restrict on delete restrict;

/** -- DROP COLUMN DRUG_ID AND ADD COLUMN DRUG_UNIT_ID IN DRUG_EXPORT_DETAIL TABLE -- */
alter table drug_export_detail
    add column drug_unit_id int(11) not null,
    add foreign key (drug_unit_id) references drug_unit (id) on update restrict on delete restrict;

/** -- DROP COLUMN DRUG_ID AND ADD COLUMN DRUG_UNIT_ID IN DRUG_EXPORT_DETAIL TABLE -- */
alter table drug_import_detail
    drop foreign key drug_import_detail_ibfk_2;
alter table drug_import_detail
    drop column drug_id;

alter table drug_import_detail
    add column drug_unit_id int(11) not null,
    add foreign key (drug_unit_id) references drug_unit (id) on update restrict on delete restrict;

/*------------------------------------------------
 *- DRUG UNIT                                    -
 *-----------------------------------------------*/
create table if not exists travel_price_policy
(
    id           int(11) auto_increment,
    `order`      int(11)   not null,
    added_km     int(11)   null,
    price_per_km int(11)   not null,
    create_at    timestamp null,
    update_at    timestamp null,
    primary key (id)
) charset = utf8;

/* -- INSERT DATA FOR TRAVEL PRICE POLICY TABLE -- */
insert into travel_price_policy(`order`, added_km, price_per_km)
values (1, 5, 10000),
       (2, null, 7500);

/*------------------------------------------------
 *- FAVORITE ESTABLISHMENT                       -
 *-----------------------------------------------*/
create table if not exists favorite_establishment
(
    id               int(11) auto_increment,
    establishment_id int(11)   not null,
    patient_id       int(11)   null,
    create_at        timestamp null,
    primary key (id),
    foreign key (establishment_id) references establishment (id) on update restrict on delete restrict,
    foreign key (patient_id) references patient (id) on update restrict on delete restrict
) charset = utf8;
