alter table medical_service_order_detail
    add column parent_id int(11) null,
    add foreign key (parent_id) references medical_service_order_detail (id) on update restrict on delete restrict;