ALTER TABLE medical_service ADD is_deleted tinyint(1) null default 0;

insert into medical_service_group(name, description)
values ('TREATMENT', 'Kham benh'),
       ('ANALYSIS', 'Xet nghiem'),
       ('DIAGNOSE', 'Chuan doan'),
       ('MEDICINE_SUPPLY', 'Cap thuoc');

insert into medical_service(medical_service_group_id, name, description)
values (1, 'TREATMENT', 'Kham benh'),
       (1, 'MOVING', 'Di chuyen'),
       (4, 'MEDICINE_SUPPLY', 'Cap thuoc');