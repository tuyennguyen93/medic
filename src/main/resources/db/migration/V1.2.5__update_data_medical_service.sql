/*------------------------------------------------
 *- UPDATE DATA FOR MEDICAL SERVICE GROUP TABLE  -
 *-----------------------------------------------*/
UPDATE medical_service_group SET name = 'Khám bệnh', description = 'Khám bệnh' WHERE id = 1;
UPDATE medical_service_group SET name = 'Xét nghiệm', description = 'Xét nghiệm' WHERE id = 2;
UPDATE medical_service_group SET name = 'Chẩn đoán hình ảnh', description = 'Chẩn đoán hình ảnh' WHERE id = 3;
UPDATE medical_service_group SET name = 'Cấp thuốc', description = 'Cấp thuốc' WHERE id = 4;

/*------------------------------------------------
 *- UPDATE DATA FOR MEDICAL SERVICE TABLE        -
 *-----------------------------------------------*/
UPDATE medical_service SET name = 'Khám bệnh', description = 'Khám bệnh' WHERE id = 1;
UPDATE medical_service SET name = 'Di chuyển', description = 'Di chuyển' WHERE id = 2;
UPDATE medical_service SET name = 'Cấp thuốc', description = 'Cấp thuốc' WHERE id = 3;