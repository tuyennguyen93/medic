insert into role(name, description)
values ('SUPER_ADMIN', 'Super Administrator'),
       ('ADMIN', 'Administrator'),
       ('DOCTOR_ADMIN', 'Doctor Administrator'),
       ('DOCTOR_STAFF', 'Doctor Staff'),
       ('NURSE_ADMIN', 'Nurse Administrator'),
       ('NURSE_STAFF', 'Nurse Staff'),
       ('PRECLINIC_ADMIN', 'Preclinic Administrator'),
       ('PRECLINIC_STAFF', 'Preclinic Staff'),
       ('DRUG_STORE_ADMIN', 'Drug Store Admin'),
       ('DRUG_STORE_STAFF', 'Drug Store Staff'),
       ('PATIENT', 'Patient');

