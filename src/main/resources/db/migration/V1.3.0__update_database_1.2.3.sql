/* -- CLEAR DATA -- */
delete from examine_record;
delete from preclinical_record;
delete from prescription_detail;
delete from prescription_record;
delete from medical_record;
delete from medical_service_order_detail;
delete from medical_service_order;

/* -- MODIFY MEDICAL_SERVICE_ORDER TABLE -- */
alter table medical_service_order drop foreign key medical_service_order_ibfk_2;
alter table medical_service_order drop column establishment_working_schedule_id;
alter table medical_service_order drop foreign key medical_service_order_ibfk_3;
alter table medical_service_order drop column account_id;
alter table medical_service_order drop column sequence_number;
alter table medical_service_order drop column status;
alter table medical_service_order drop column result_appointment;
alter table medical_service_order drop foreign key medical_service_order_ibfk_4;
alter table medical_service_order drop column transaction_operation_id;
alter table medical_service_order add total_amount int(11) null;
alter table medical_service_order add payment_status enum ('PENDING', 'SUCCESS', 'FAILED') not null default 'PENDING';
alter table medical_service_order add payment_reference text null;
alter table medical_service_order add payment_date timestamp null;

/* -- MODIFY MEDICAL_SERVICE_ORDER_DETAIL TABLE -- */
alter table medical_service_order_detail drop column note;
alter table medical_service_order_detail add sequence_number int(11) null;
alter table medical_service_order_detail
    add column establishment_id int(11) null,
    add foreign key (establishment_id) references establishment (id) on update restrict on delete restrict;
alter table medical_service_order_detail
    add column handle_by int(11) null,
    add foreign key (handle_by) references account (id) on update restrict on delete restrict;
alter table medical_service_order_detail add appointment_date timestamp null;
alter table medical_service_order_detail add result_date timestamp null;
alter table medical_service_order_detail add result text null;
alter table medical_service_order_detail add status enum ('REQUEST_SENT', 'REQUEST_CANCELLED', 'REQUEST_DENIED', 'REQUEST_ACCEPTED') not null;
alter table medical_service_order_detail add tx_amount int(11) null;

/* -- DROP TRANSACTION TABLE -- */
drop table if exists transaction_operation, transaction;

/*------------------------------------------------
 *- SERVICE RECORD                               -
 *-----------------------------------------------*/
create table if not exists medical_service_order
(
    id                       int(11) auto_increment,
    medical_record_id        int(11) not null,
    medical_service_order_id int(11) not null,
    create_at                timestamp null,
    primary key (id),
    foreign key (medical_record_id) references medical_record (id) on update restrict on delete restrict,
    foreign key (medical_service_order_id) references medical_service_order (id) on update restrict on delete restrict
) charset = utf8;

/* -- MODIFY MEDICAL_SERVICE_ORDER_DETAIL TABLE -- */
alter table medical_service_order_discussion drop foreign key medical_service_order_discussion_ibfk_1;
alter table medical_service_order_discussion drop column medical_service_order_id;
alter table medical_service_order_discussion
    add column medical_service_order_detail_id int(11) not null,
    add foreign key (medical_service_order_detail_id) references medical_service_order_detail (id) on update restrict on delete restrict;
alter table medical_service_order_discussion change account_id message_sender int(11);

