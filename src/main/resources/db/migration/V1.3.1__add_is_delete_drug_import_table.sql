ALTER TABLE drug_import ADD is_deleted tinyint(1) null default 0;
ALTER TABLE drug_export ADD is_deleted tinyint(1) null default 0;
ALTER TABLE drug_import_detail ADD code nvarchar(255) not null;
ALTER TABLE drug_export_detail ADD code nvarchar(255) not null;
