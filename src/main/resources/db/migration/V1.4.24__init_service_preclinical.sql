insert into medical_service (id, medical_service_group_id, name, description, is_deleted)
values (5, 2, 'Xét nghiệm tại nhà', 'Xét nghiệm tại nhà', 0)
on duplicate key update medical_service_group_id = 2, name = 'Xét nghiệm tại nhà', description = 'Xét nghiệm tại nhà';

insert into medical_service (id, medical_service_group_id, name, description, is_deleted)
values (6, 2, 'Xét nghiệm phòng mạch', 'Xét nghiệm phòng mạch', 0)
on duplicate key update medical_service_group_id = 2, name = 'Xét nghiệm phòng mạch', description = 'Xét nghiệm phòng mạch';

insert into medical_service (id, medical_service_group_id, name, description, is_deleted)
values (7, 2, 'Xét nghiệm phòng xét nghiệm', 'Xét nghiệm phòng xét nghiệm', 0)
on duplicate key update medical_service_group_id = 2, name = 'Xét nghiệm phòng xét nghiệm', description = 'Xét nghiệm phòng xét nghiệm';