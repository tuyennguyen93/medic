ALTER TABLE drug
  ADD composition text null,
  ADD concentration text null,
  ADD form text null,
  ADD dosage text null;