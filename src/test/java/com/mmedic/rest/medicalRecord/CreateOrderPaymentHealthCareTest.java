package com.mmedic.rest.medicalRecord;

import com.mmedic.BaseTest;
import com.mmedic.config.prop.PaymentProperties;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalService;
import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.entity.Patient;
import com.mmedic.entity.ServiceRecord;
import com.mmedic.enums.MedicalRecordType;
import com.mmedic.enums.MedicalServiceOrderStatus;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.doctor.DoctorService;
import com.mmedic.rest.doctor.repo.DoctorRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.rest.medicalRecord.impl.MedicalRecordServiceImpl;
import com.mmedic.rest.medicalRecord.repo.ExamineRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalServiceOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.ServiceRecordRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderDiscussionRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.service.payment.zalopay.CreateOrder;
import com.mmedic.spring.errors.MedicException;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class CreateOrderPaymentHealthCareTest extends BaseTest {

    @Autowired
    private PaymentProperties paymentProperties;

    @MockBean
    private MedicalRecordRepository medicalRecordRepository;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    @MockBean
    private MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    @MockBean
    private MedicalServiceOrderRepository medicalServiceOrderRepository;

    @MockBean
    private MedicalServiceOrderDetailRepository medicalServiceOrderDetailRepository;

    @MockBean
    private ExamineRecordRepository examineRecordRepository;

    @MockBean
    private DoctorService doctorService;

    @MockBean
    private DoctorRepository doctorRepository;

    @MockBean
    private EstablishmentRepository establishmentRepository;

    @MockBean
    private ServiceRecordRepository serviceRecordRepository;

    @MockBean
    private MedicalServiceOrderDiscussionRepository medicalServiceOrderDiscussionRepository;

    @MockBean
    private NotificationService notificationService;

    private CreateOrder createOrder;

    private MedicalRecordService medicalRecordService;

    @Before
    public void setUp() {
        createOrder = new CreateOrder(paymentProperties);
        medicalRecordService = new MedicalRecordServiceImpl(medicalRecordRepository, accountRepository,
                establishmentWorkingScheduleRepository, medicalServiceRegistryRepository, medicalServiceOrderRepository,
                medicalServiceOrderDetailRepository, examineRecordRepository, doctorService, doctorRepository, establishmentRepository,
                serviceRecordRepository, medicalServiceOrderDiscussionRepository, createOrder, notificationService);
    }

    @Test
    public void test_createPaymentOrderHealthCareService() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.SERVICE);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Service Record
        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setId(1);
        medicalRecordExamine.setServiceRecord(serviceRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        serviceRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setServiceRecord(serviceRecord);

        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        // Health Care medical service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(1);
        serviceOrderDetail.setMedicalService(medicalService);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(doctorRepository.findMedicalStaffPriceByPackageId(serviceOrderDetail.getMedicalService().getId(), serviceOrderDetail.getEstablishment().getId())).thenReturn(10000);

        medicalRecordService.createPaymentOrderHealthCareService(medicalRecordId);
    }

    @Test
    public void test_createPaymentOrderHealthCareService_NotFoundMedicalRecord() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.SERVICE);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> notFound = new Condition<>(m -> MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST.equals(m.getMessageCode()), "ERROR_MEDICAL_RECORD_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderHealthCareService(2);
                }).has(notFound);
    }

    @Test
    public void test_createPaymentOrderHealthCareService_OrderHasPaid() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.SERVICE);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Service Record
        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setId(1);
        medicalRecordExamine.setServiceRecord(serviceRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
        serviceRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setServiceRecord(serviceRecord);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> hasPaid = new Condition<>(m -> MedicException.ERROR_SERVICE_ORDER_HAS_PAID.equals(m.getMessageCode()), "ERROR_SERVICE_ORDER_HAS_PAID");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderHealthCareService(medicalRecordId);
                }).has(hasPaid);
    }

    @Test
    public void test_createPaymentOrderHealthCareService_OrderDetailNotExist() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.SERVICE);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Service Record
        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setId(1);
        medicalRecordExamine.setServiceRecord(serviceRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        serviceRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setServiceRecord(serviceRecord);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> orderDetailNotExist = new Condition<>(m -> MedicException.ERROR_ORDER_DETAIL_NOT_EXIST.equals(m.getMessageCode()), "ERROR_ORDER_DETAIL_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderHealthCareService(medicalRecordId);
                }).has(orderDetailNotExist);
    }

    @Test
    public void test_createPaymentOrderHealthCareService_HealthCareNotExist() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Service Record
        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setId(1);
        medicalRecordExamine.setServiceRecord(serviceRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        serviceRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setServiceRecord(serviceRecord);

//        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> healthCareNotExist = new Condition<>(m -> MedicException.ERROR_ORDER_HEALTH_CARE_NOT_EXIST.equals(m.getMessageCode()), "ERROR_ORDER_HEALTH_CARE_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderHealthCareService(medicalRecordId);
                }).has(healthCareNotExist);
    }

    @Test
    public void test_createPaymentOrderHealthCareService_OrderStatusNotAccepted() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.SERVICE);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Service Record
        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setId(1);
        medicalRecordExamine.setServiceRecord(serviceRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        serviceRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setServiceRecord(serviceRecord);

        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_SENT);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> notApproved = new Condition<>(m -> MedicException.ERROR_HEALTH_CARE_SERVICE_NOT_APPROVED.equals(m.getMessageCode()), "ERROR_HEALTH_CARE_SERVICE_NOT_APPROVED");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderHealthCareService(medicalRecordId);
                }).has(notApproved);
    }

    @Test
    public void test_createPaymentOrderHealthCareService_AmountIsZero() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.SERVICE);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Service Record
        ServiceRecord serviceRecord = new ServiceRecord();
        serviceRecord.setId(1);
        medicalRecordExamine.setServiceRecord(serviceRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        serviceRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setServiceRecord(serviceRecord);

        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setStatus(MedicalServiceOrderStatus.REQUEST_ACCEPTED);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        // Health Care medical service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(1);
        serviceOrderDetail.setMedicalService(medicalService);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(doctorRepository.findMedicalStaffPriceByPackageId(serviceOrderDetail.getMedicalService().getId(), serviceOrderDetail.getEstablishment().getId())).thenReturn(0);

        Condition<MedicException> amountIsZero = new Condition<>(m -> MedicException.ERROR_AMOUNT_IS_ZERO.equals(m.getMessageCode()), "ERROR_AMOUNT_IS_ZERO");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderHealthCareService(medicalRecordId);
                }).has(amountIsZero);
    }
}
