package com.mmedic.rest.medicalRecord;

import com.mmedic.BaseTest;
import com.mmedic.config.prop.PaymentProperties;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.entity.Patient;
import com.mmedic.entity.PrescriptionRecord;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.doctor.DoctorService;
import com.mmedic.rest.doctor.repo.DoctorRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.rest.medicalRecord.dto.PrescriptionRecordDetailDTO;
import com.mmedic.rest.medicalRecord.impl.MedicalRecordServiceImpl;
import com.mmedic.rest.medicalRecord.repo.ExamineRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalServiceOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.ServiceRecordRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderDiscussionRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.service.payment.zalopay.CreateOrder;
import com.mmedic.spring.errors.MedicException;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class CreateOrderPaymentPrescriptionTest extends BaseTest {

    @Autowired
    private PaymentProperties paymentProperties;

    @MockBean
    private MedicalRecordRepository medicalRecordRepository;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    @MockBean
    private MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    @MockBean
    private MedicalServiceOrderRepository medicalServiceOrderRepository;

    @MockBean
    private MedicalServiceOrderDetailRepository medicalServiceOrderDetailRepository;

    @MockBean
    private ExamineRecordRepository examineRecordRepository;

    @MockBean
    private DoctorService doctorService;

    @MockBean
    private DoctorRepository doctorRepository;

    @MockBean
    private EstablishmentRepository establishmentRepository;

    @MockBean
    private ServiceRecordRepository serviceRecordRepository;

    @MockBean
    private MedicalServiceOrderDiscussionRepository medicalServiceOrderDiscussionRepository;

    @MockBean
    private NotificationService notificationService;

    private CreateOrder createOrder;

    private MedicalRecordService medicalRecordService;

    @Before
    public void setUp() {
        createOrder = new CreateOrder(paymentProperties);
        medicalRecordService = new MedicalRecordServiceImpl(medicalRecordRepository, accountRepository,
                establishmentWorkingScheduleRepository, medicalServiceRegistryRepository, medicalServiceOrderRepository,
                medicalServiceOrderDetailRepository, examineRecordRepository, doctorService, doctorRepository, establishmentRepository,
                serviceRecordRepository, medicalServiceOrderDiscussionRepository, createOrder, notificationService);
    }

    @Test
    public void test_createPaymentOrderPrescription() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Prescription Record
        PrescriptionRecord prescriptionRecord = new PrescriptionRecord();
        prescriptionRecord.setId(1);
        prescriptionRecord.setCreateAt(LocalDateTime.now().minusMinutes(3));
        medicalRecordExamine.setPrescriptionRecord(prescriptionRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        prescriptionRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPrescriptionRecord(prescriptionRecord);

        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        // Prices
        PrescriptionRecordDetailDTO price = createMockDatePrice();
        List<PrescriptionRecordDetailDTO> prices = Collections.singletonList(price);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(medicalRecordRepository.findPrescriptionPrice(Mockito.eq(medicalRecordId), Mockito.any())).thenReturn(prices);

        medicalRecordService.createPaymentOrderPrescription(medicalRecordId);
    }

    @Test
    public void test_createPaymentOrderPrescription_NotFoundMedicalRecord() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> notFound = new Condition<>(m -> MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST.equals(m.getMessageCode()), "ERROR_MEDICAL_RECORD_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPrescription(2);
                }).has(notFound);
    }

    @Test
    public void test_createPaymentOrderPrescription_OrderHasPaid() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Prescription Record
        PrescriptionRecord prescriptionRecord = new PrescriptionRecord();
        prescriptionRecord.setId(1);
        prescriptionRecord.setCreateAt(LocalDateTime.now().minusMinutes(3));
        medicalRecordExamine.setPrescriptionRecord(prescriptionRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
        prescriptionRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPrescriptionRecord(prescriptionRecord);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> hasPaid = new Condition<>(m -> MedicException.ERROR_SERVICE_ORDER_HAS_PAID.equals(m.getMessageCode()), "ERROR_SERVICE_ORDER_HAS_PAID");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPrescription(medicalRecordId);
                }).has(hasPaid);
    }

    @Test
    public void test_createPaymentOrderPrescription_PrescriptionNotYetEffect() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Prescription Record
        PrescriptionRecord prescriptionRecord = new PrescriptionRecord();
        prescriptionRecord.setId(1);
        prescriptionRecord.setCreateAt(LocalDateTime.now());
        medicalRecordExamine.setPrescriptionRecord(prescriptionRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        prescriptionRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPrescriptionRecord(prescriptionRecord);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> notYetEffect = new Condition<>(m -> MedicException.ERROR_PRESCRIPTION_NOT_YET_EFFECT.equals(m.getMessageCode()), "ERROR_PRESCRIPTION_NOT_YET_EFFECT");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPrescription(medicalRecordId);
                }).has(notYetEffect);
    }

    @Test
    public void test_createPaymentOrderPrescription_OrderDetailNotExist() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Prescription Record
        PrescriptionRecord prescriptionRecord = new PrescriptionRecord();
        prescriptionRecord.setId(1);
        prescriptionRecord.setCreateAt(LocalDateTime.now().minusMinutes(3));
        medicalRecordExamine.setPrescriptionRecord(prescriptionRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        prescriptionRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPrescriptionRecord(prescriptionRecord);

        // Prices
        PrescriptionRecordDetailDTO price = createMockDatePrice();
        List<PrescriptionRecordDetailDTO> prices = Collections.singletonList(price);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> orderDetailNotExist = new Condition<>(m -> MedicException.ERROR_ORDER_DETAIL_NOT_EXIST.equals(m.getMessageCode()), "ERROR_ORDER_DETAIL_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPrescription(medicalRecordId);
                }).has(orderDetailNotExist);
    }

    @Test
    public void test_createPaymentOrderPrescription_AmountIsZero() {
        int medicalRecordId = 1;

        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Prescription Record
        PrescriptionRecord prescriptionRecord = new PrescriptionRecord();
        prescriptionRecord.setId(1);
        prescriptionRecord.setCreateAt(LocalDateTime.now().minusMinutes(3));
        medicalRecordExamine.setPrescriptionRecord(prescriptionRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        prescriptionRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPrescriptionRecord(prescriptionRecord);

        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        // Prices
//        PrescriptionRecordDetailDTO price = createMockDatePrice();
//        List<PrescriptionRecordDetailDTO> prices = Collections.singletonList(price);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(medicalRecordRepository.findPrescriptionPrice(Mockito.eq(medicalRecordId), Mockito.any())).thenReturn(Collections.emptyList());

        Condition<MedicException> amountIsZero = new Condition<>(m -> MedicException.ERROR_AMOUNT_IS_ZERO.equals(m.getMessageCode()), "ERROR_AMOUNT_IS_ZERO");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPrescription(medicalRecordId);
                }).has(amountIsZero);
    }

    private PrescriptionRecordDetailDTO createMockDatePrice() {
        return new PrescriptionRecordDetailDTO() {
            @Override
            public int getId() {
                return 1;
            }

            @Override
            public int getDrugId() {
                return 1;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public String getUsage() {
                return null;
            }

            @Override
            public String getNote() {
                return null;
            }

            @Override
            public String getUnit() {
                return "Viên";
            }

            @Override
            public Integer getPricePerUnit() {
                return 1000;
            }

            @Override
            public Integer getAmount() {
                return 1;
            }

            @Override
            public int getDrugStoreId() {
                return 0;
            }

            @Override
            public String getDrugStoreName() {
                return null;
            }

            @Override
            public Float getDrugRating() {
                return null;
            }

            @Override
            public boolean getHasRating() {
                return false;
            }
        };
    }
}
