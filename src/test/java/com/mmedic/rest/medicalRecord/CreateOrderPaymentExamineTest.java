package com.mmedic.rest.medicalRecord;

import com.mmedic.BaseTest;
import com.mmedic.config.prop.PaymentProperties;
import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.EstablishmentWorkingSchedule;
import com.mmedic.entity.ExamineRecord;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalService;
import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.entity.Patient;
import com.mmedic.enums.MedicalRecordType;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.enums.TreatmentType;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.doctor.DoctorService;
import com.mmedic.rest.doctor.dto.TreatmentBillDTO;
import com.mmedic.rest.doctor.repo.DoctorRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.rest.medicalRecord.impl.MedicalRecordServiceImpl;
import com.mmedic.rest.medicalRecord.repo.ExamineRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalServiceOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.ServiceRecordRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderDiscussionRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.service.payment.zalopay.CreateOrder;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class CreateOrderPaymentExamineTest extends BaseTest {

    @Autowired
    private PaymentProperties paymentProperties;

    @MockBean
    private MedicalRecordRepository medicalRecordRepository;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    @MockBean
    private MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    @MockBean
    private MedicalServiceOrderRepository medicalServiceOrderRepository;

    @MockBean
    private MedicalServiceOrderDetailRepository medicalServiceOrderDetailRepository;

    @MockBean
    private ExamineRecordRepository examineRecordRepository;

    @MockBean
    private DoctorService doctorService;

    @MockBean
    private DoctorRepository doctorRepository;

    @MockBean
    private EstablishmentRepository establishmentRepository;

    @MockBean
    private ServiceRecordRepository serviceRecordRepository;

    @MockBean
    private MedicalServiceOrderDiscussionRepository medicalServiceOrderDiscussionRepository;

    @MockBean
    private NotificationService notificationService;


    private CreateOrder createOrder;

    private MedicalRecordService medicalRecordService;

    @Before
    public void setUp() {
        createOrder = new CreateOrder(paymentProperties);
        medicalRecordService = new MedicalRecordServiceImpl(medicalRecordRepository, accountRepository,
                establishmentWorkingScheduleRepository, medicalServiceRegistryRepository, medicalServiceOrderRepository,
                medicalServiceOrderDetailRepository, examineRecordRepository, doctorService, doctorRepository, establishmentRepository,
                serviceRecordRepository, medicalServiceOrderDiscussionRepository, createOrder, notificationService);
    }

    @Test
    public void test_createPaymentOrderExamine_CLINIC() {
        int medicalRecordId = 1;
        // Prepare data
        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);
        establishment.setAverageTime(10);
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setEstablishment(establishment);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecordExamine.setPatient(patient);
        // Examine Record
        ExamineRecord examineRecord = new ExamineRecord();
        medicalRecordExamine.setExamineRecord(examineRecord);
        // Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        examineRecord.setMedicalServiceOrder(serviceOrder);
        // Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setHandleBy(doctor);
        serviceOrderDetail.setSequenceNumber(10001);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setAppointmentDate(LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 0)));
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));
        // Treatment medical service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(Constants.SERVICE_TREATMENT_MEDICAL);
        serviceOrderDetail.setMedicalService(medicalService);
        // List bills
        TreatmentBillDTO treatmentBill = new TreatmentBillDTO();
        treatmentBill.setType(Constants.TREATMENT_FEE_TYPE);
        treatmentBill.setPrice(10000);
        treatmentBill.setName("Phí khám bệnh");
        List<TreatmentBillDTO> bills = Collections.singletonList(treatmentBill);
        // List working time
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setStartTime(LocalTime.of(7, 0));
        workingSchedule.setEndTime(LocalTime.of(20, 0));
        workingSchedule.setEstablishment(establishment);
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(doctorService.calculateCost(doctor.getId(), medicalRecordExamine.getPatient().getId(), TreatmentType.CLINIC)).thenReturn(bills);
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(establishment.getId())).thenReturn(workingSchedules);

        assertThat(medicalRecordService.createPaymentOrderExamine(medicalRecordId)).isNotNull();
        assertThat(serviceOrder.getPaymentReference()).isNotNull();
    }

    @Test
    public void test_createPaymentOrderExamine_HOME() {
        int medicalRecordId = 1;
        // Prepare data
        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);
        establishment.setAverageTime(10);
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setEstablishment(establishment);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecordExamine.setPatient(patient);
        // Examine Record
        ExamineRecord examineRecord = new ExamineRecord();
        medicalRecordExamine.setExamineRecord(examineRecord);
        // Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        examineRecord.setMedicalServiceOrder(serviceOrder);
        // Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setSequenceNumber(10001);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setAppointmentDate(LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 0)));
        serviceOrderDetail.setHandleBy(doctor);
        // Treatment medical service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(Constants.SERVICE_TREATMENT_MEDICAL);
        serviceOrderDetail.setMedicalService(medicalService);
        // Transport medical service
        MedicalServiceOrderDetail transportOrder = new MedicalServiceOrderDetail();
        transportOrder.setId(2);
        transportOrder.setHandleBy(doctor);
        serviceOrder.setMedicalServiceOrderDetails(Arrays.asList(serviceOrderDetail, transportOrder));
        // Transport service
        MedicalService transportService = new MedicalService();
        transportService.setId(Constants.SERVICE_TREATMENT_TRANSPORT);
        transportOrder.setMedicalService(transportService);
        // List bills
        TreatmentBillDTO treatmentBill = new TreatmentBillDTO();
        treatmentBill.setType(Constants.TREATMENT_FEE_TYPE);
        treatmentBill.setPrice(10000);
        treatmentBill.setName("Phí khám bệnh");
        TreatmentBillDTO transportBill = new TreatmentBillDTO();
        transportBill.setType(Constants.TREATMENT_FEE_TYPE);
        transportBill.setPrice(5000);
        transportBill.setName("Phí di chuyển");
        List<TreatmentBillDTO> bills = Arrays.asList(treatmentBill, transportBill);
        // List working time
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setStartTime(LocalTime.of(7, 0));
        workingSchedule.setEndTime(LocalTime.of(20, 0));
        workingSchedule.setEstablishment(establishment);
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(doctorService.calculateCost(doctor.getId(), medicalRecordExamine.getPatient().getId(), TreatmentType.HOME)).thenReturn(bills);
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(establishment.getId())).thenReturn(workingSchedules);

        assertThat(medicalRecordService.createPaymentOrderExamine(medicalRecordId)).isNotNull();
        assertThat(serviceOrder.getPaymentReference()).isNotNull();
    }

    @Test
    public void test_createPaymentOrderExamine_MedicalRecordNotFound() {
        int medicalRecordId = 1;
        // Prepare data
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecordExamine.setPatient(patient);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> notFound = new Condition<>(m -> MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST.equals(m.getMessageCode()), "ERROR_MEDICAL_RECORD_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderExamine(2);
                }).has(notFound);
    }

    @Test
    public void test_createPaymentOrderExamine_HadPayment() {
        int medicalRecordId = 1;
        // Prepare data
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecordExamine.setPatient(patient);
        // Examine Record
        ExamineRecord examineRecord = new ExamineRecord();
        medicalRecordExamine.setExamineRecord(examineRecord);
        // Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
        examineRecord.setMedicalServiceOrder(serviceOrder);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> hadPayment = new Condition<>(m -> MedicException.ERROR_SERVICE_ORDER_HAS_PAID.equals(m.getMessageCode()), "ERROR_SERVICE_ORDER_HAS_PAID");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderExamine(medicalRecordId);
                }).has(hadPayment);
    }

    @Test
    public void test_createPaymentOrderExamine_OrderDetailNotExist() {
        int medicalRecordId = 1;
        // Prepare data
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecordExamine.setPatient(patient);
        // Examine Record
        ExamineRecord examineRecord = new ExamineRecord();
        medicalRecordExamine.setExamineRecord(examineRecord);
        // Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        examineRecord.setMedicalServiceOrder(serviceOrder);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> orderDetailNotExist = new Condition<>(m -> MedicException.ERROR_ORDER_DETAIL_NOT_EXIST.equals(m.getMessageCode()), "ERROR_ORDER_DETAIL_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderExamine(medicalRecordId);
                }).has(orderDetailNotExist);
    }

    @Test
    public void test_createPaymentOrderExamine_RecordTypeNotTreatment() {
        int medicalRecordId = 1;
        // Prepare data
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.SERVICE);
        medicalRecordExamine.setPatient(patient);
        // Examine Record
        ExamineRecord examineRecord = new ExamineRecord();
        medicalRecordExamine.setExamineRecord(examineRecord);
        // Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        examineRecord.setMedicalServiceOrder(serviceOrder);
        // Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setHandleBy(doctor);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> treatmentServiceNotFound = new Condition<>(m -> MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST.equals(m.getMessageCode()), "ERROR_ORDER_TREATMENT_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderExamine(medicalRecordId);
                }).has(treatmentServiceNotFound);
    }

    @Test
    public void test_createPaymentOrderExamine_NotHaveTreatmentOrderService() {
        int medicalRecordId = 1;
        // Prepare data
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecordExamine.setPatient(patient);
        // Examine Record
        ExamineRecord examineRecord = new ExamineRecord();
        medicalRecordExamine.setExamineRecord(examineRecord);
        // Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        examineRecord.setMedicalServiceOrder(serviceOrder);
        // Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setHandleBy(doctor);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));
        // Treatment medical service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(Constants.SERVICE_MEDICINE_SUPPLY);
        serviceOrderDetail.setMedicalService(medicalService);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> treatmentServiceNotFound = new Condition<>(m -> MedicException.ERROR_ORDER_TREATMENT_NOT_EXIST.equals(m.getMessageCode()), "ERROR_ORDER_TREATMENT_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderExamine(medicalRecordId);
                }).has(treatmentServiceNotFound);
    }

    @Test
    public void test_createPaymentOrderExamine_AmountIsZero() {
        int medicalRecordId = 1;
        // Prepare data
        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);
        establishment.setAverageTime(10);
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setEstablishment(establishment);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecordExamine.setPatient(patient);
        // Examine Record
        ExamineRecord examineRecord = new ExamineRecord();
        medicalRecordExamine.setExamineRecord(examineRecord);
        // Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        examineRecord.setMedicalServiceOrder(serviceOrder);
        // Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setHandleBy(doctor);
        serviceOrderDetail.setSequenceNumber(10001);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setAppointmentDate(LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 0)));
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));
        // Treatment medical service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(Constants.SERVICE_TREATMENT_MEDICAL);
        serviceOrderDetail.setMedicalService(medicalService);
        // List bills
        List<TreatmentBillDTO> bills = Collections.emptyList();
        // List working time
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setStartTime(LocalTime.of(7, 0));
        workingSchedule.setEndTime(LocalTime.of(20, 0));
        workingSchedule.setEstablishment(establishment);
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(doctorService.calculateCost(doctor.getId(), medicalRecordExamine.getPatient().getId(), TreatmentType.CLINIC)).thenReturn(bills);
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(establishment.getId())).thenReturn(workingSchedules);

        Condition<MedicException> amountIsZero = new Condition<>(m -> MedicException.ERROR_AMOUNT_IS_ZERO.equals(m.getMessageCode()), "ERROR_AMOUNT_IS_ZERO");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderExamine(medicalRecordId);
                }).has(amountIsZero);
    }

    @Test
    public void test_createPaymentOrderExamine_DoctorOff() {
        int medicalRecordId = 1;
        // Prepare data
        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);
        establishment.setAverageTime(10);
        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setEstablishment(establishment);
        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        // Examine Medical Record
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);
        medicalRecordExamine.setRecordType(MedicalRecordType.TREATMENT);
        medicalRecordExamine.setPatient(patient);
        // Examine Record
        ExamineRecord examineRecord = new ExamineRecord();
        medicalRecordExamine.setExamineRecord(examineRecord);
        // Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        examineRecord.setMedicalServiceOrder(serviceOrder);
        // Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setHandleBy(doctor);
        serviceOrderDetail.setSequenceNumber(10001);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setAppointmentDate(LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 0)));
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));
        // Treatment medical service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(Constants.SERVICE_TREATMENT_MEDICAL);
        serviceOrderDetail.setMedicalService(medicalService);
        // List working time
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setStartTime(LocalTime.of(7, 0));
        workingSchedule.setEndTime(LocalTime.of(20, 0));
        workingSchedule.setEstablishment(establishment);
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        LocalDateTime appointmentTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 0));

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(establishment.getId())).thenReturn(workingSchedules);
        Mockito.when(establishmentRepository.establishmentIsOff(appointmentTime, establishment.getId())).thenReturn(1);

        Condition<MedicException> doctorIsOff = new Condition<>(m -> MedicException.ERROR_DOCTOR_IS_OFF.equals(m.getMessageCode()), "ERROR_DOCTOR_IS_OFF");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderExamine(medicalRecordId);
                }).has(doctorIsOff);
    }
}
