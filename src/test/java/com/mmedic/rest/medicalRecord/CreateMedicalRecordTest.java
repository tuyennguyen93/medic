package com.mmedic.rest.medicalRecord;

import com.mmedic.entity.Account;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.EstablishmentWorkingSchedule;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalService;
import com.mmedic.entity.MedicalServiceRegistry;
import com.mmedic.entity.Patient;
import com.mmedic.enums.Day;
import com.mmedic.enums.MedicalRecordType;
import com.mmedic.enums.TreatmentType;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.doctor.DoctorService;
import com.mmedic.rest.doctor.repo.DoctorRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordDTO;
import com.mmedic.rest.medicalRecord.dto.MedicalRecordParamDTO;
import com.mmedic.rest.medicalRecord.impl.MedicalRecordServiceImpl;
import com.mmedic.rest.medicalRecord.repo.ExamineRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalServiceOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.ServiceRecordRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderDiscussionRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.service.payment.zalopay.CreateOrder;
import com.mmedic.spring.errors.MedicException;
import com.mmedic.utils.Constants;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@RunWith(SpringRunner.class)
public class CreateMedicalRecordTest {

    @MockBean
    private MedicalRecordRepository medicalRecordRepository;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    @MockBean
    private MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    @MockBean
    private MedicalServiceOrderRepository medicalServiceOrderRepository;

    @MockBean
    private MedicalServiceOrderDetailRepository medicalServiceOrderDetailRepository;

    @MockBean
    private ExamineRecordRepository examineRecordRepository;

    @MockBean
    private DoctorService doctorService;

    @MockBean
    private DoctorRepository doctorRepository;

    @MockBean
    private EstablishmentRepository establishmentRepository;

    @MockBean
    private ServiceRecordRepository serviceRecordRepository;

    @MockBean
    private MedicalServiceOrderDiscussionRepository medicalServiceOrderDiscussionRepository;

    @MockBean
    private CreateOrder createOrder;

    @MockBean
    private NotificationService notificationService;

    @MockBean
    private MedicalRecordService medicalRecordService;

    @Before
    public void setUp() {
        medicalRecordService = new MedicalRecordServiceImpl(medicalRecordRepository, accountRepository,
                establishmentWorkingScheduleRepository, medicalServiceRegistryRepository, medicalServiceOrderRepository,
                medicalServiceOrderDetailRepository, examineRecordRepository, doctorService, doctorRepository, establishmentRepository,
                serviceRecordRepository, medicalServiceOrderDiscussionRepository, createOrder, notificationService);
    }

    @Test
    public void test_createMedicalRecord_HOME() {
        /* Prepare data */
        LocalDateTime now = LocalDateTime.now();

        // New medical record
        MedicalRecordParamDTO param = new MedicalRecordParamDTO();
        param.setDoctorId(1);
        param.setPatientId(1);
        param.setAppointment(LocalDate.now());
        param.setTreatmentType(TreatmentType.HOME);
        param.setWorkingScheduleId(1);

        // Establishment
        Establishment clinic = new Establishment();
        clinic.setId(1);
        clinic.setName("Clinic");
        clinic.setAverageTime(10);

        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setName("Doctor");
        doctor.setEstablishment(clinic);

        // Doctor registry treatment service
        MedicalService treatmentService = new MedicalService();
        treatmentService.setId(Constants.SERVICE_TREATMENT_MEDICAL);
        MedicalServiceRegistry treatmentServiceRegistry = new MedicalServiceRegistry();
        treatmentServiceRegistry.setMedicalService(treatmentService);

        // Doctor registry transport service
        MedicalService transportService = new MedicalService();
        transportService.setId(Constants.SERVICE_TREATMENT_TRANSPORT);
        MedicalServiceRegistry transportServiceRegistry = new MedicalServiceRegistry();
        transportServiceRegistry.setMedicalService(transportService);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        patient.setName("Patient");

        Account patientAcc = new Account();
        patientAcc.setId(2);
        patientAcc.setName("Patient");
        patientAcc.setPatient(patient);

        // Establishment working schedule
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setEstablishment(clinic);
        workingSchedule.setDayOfWeek(Day.convert(now.getDayOfWeek()));
        workingSchedule.setStartTime(now.toLocalTime().plusHours(1));
        workingSchedule.setEndTime(now.toLocalTime().plusHours(5));

        // List working schedule of establishment
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        // Medical Record
        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setId(1);
        medicalRecord.setPatient(patient);
        medicalRecord.setRecordType(MedicalRecordType.TREATMENT);

        /* Mock */
        Mockito.when(accountRepository.findDoctorAccountById(param.getDoctorId())).thenReturn(Optional.of(doctor));
        Mockito.when(accountRepository.findPatientAccountByPatientId(param.getPatientId())).thenReturn(Optional.of(patientAcc));
        Mockito.when(establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(param.getWorkingScheduleId())).thenReturn(Optional.of(workingSchedule));
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(workingSchedule.getEstablishment().getId())).thenReturn(workingSchedules);
        Mockito.when(medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_MEDICAL)).thenReturn(Optional.of(treatmentServiceRegistry));
        Mockito.when(medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_TRANSPORT)).thenReturn(Optional.of(transportServiceRegistry));
        Mockito.when(medicalRecordRepository.save(Mockito.any())).thenReturn(medicalRecord);

        MedicalRecordDTO result = medicalRecordService.createMedicalRecord(param);

        assertThat(result).isNotNull();
    }

    @Test
    public void test_createMedicalRecord_CLINIC() {
        /* Prepare data */
        LocalDateTime now = LocalDateTime.now();

        // New medical record
        MedicalRecordParamDTO param = new MedicalRecordParamDTO();
        param.setDoctorId(1);
        param.setPatientId(1);
        param.setAppointment(LocalDate.now());
        param.setTreatmentType(TreatmentType.CLINIC);
        param.setWorkingScheduleId(1);

        // Establishment
        Establishment clinic = new Establishment();
        clinic.setId(1);
        clinic.setName("Clinic");
        clinic.setAverageTime(10);

        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setName("Doctor");
        doctor.setEstablishment(clinic);

        // Doctor registry treatment service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(1);
        MedicalServiceRegistry serviceRegistry = new MedicalServiceRegistry();
        serviceRegistry.setMedicalService(medicalService);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        patient.setName("Patient");

        Account patientAcc = new Account();
        patientAcc.setId(2);
        patientAcc.setName("Patient");
        patientAcc.setPatient(patient);

        // Establishment working schedule
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setEstablishment(clinic);
        workingSchedule.setDayOfWeek(Day.convert(now.getDayOfWeek()));
        workingSchedule.setStartTime(now.toLocalTime().plusHours(1));
        workingSchedule.setEndTime(now.toLocalTime().plusHours(5));

        // List working schedule of establishment
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        // Medical Record
        MedicalRecord medicalRecord = new MedicalRecord();
        medicalRecord.setId(1);
        medicalRecord.setPatient(patient);
        medicalRecord.setRecordType(MedicalRecordType.TREATMENT);

        /* Mock */
        Mockito.when(accountRepository.findDoctorAccountById(param.getDoctorId())).thenReturn(Optional.of(doctor));
        Mockito.when(accountRepository.findPatientAccountByPatientId(param.getPatientId())).thenReturn(Optional.of(patientAcc));
        Mockito.when(establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(param.getWorkingScheduleId())).thenReturn(Optional.of(workingSchedule));
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(workingSchedule.getEstablishment().getId())).thenReturn(workingSchedules);
        Mockito.when(medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_MEDICAL)).thenReturn(Optional.of(serviceRegistry));
        Mockito.when(medicalRecordRepository.save(Mockito.any())).thenReturn(medicalRecord);

        MedicalRecordDTO result = medicalRecordService.createMedicalRecord(param);

        assertThat(result).isNotNull();
    }

    @Test
    public void test_createMedicalRecord_DoctorNotFound() {
        /* Prepare data */

        // New medical record
        MedicalRecordParamDTO param = new MedicalRecordParamDTO();
        param.setDoctorId(1);
        param.setPatientId(1);
        param.setAppointment(LocalDate.now());
        param.setTreatmentType(TreatmentType.CLINIC);
        param.setWorkingScheduleId(1);

        /* Mock */
        Mockito.when(accountRepository.findDoctorAccountById(param.getDoctorId())).thenReturn(Optional.empty());

        Condition<MedicException> doctorNotFound = new Condition<>(m -> MedicException.ERROR_DOCTOR_NOT_EXIST.equals(m.getMessageCode()), "ERROR_DOCTOR_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createMedicalRecord(param);
                }).has(doctorNotFound);
    }

    @Test
    public void test_createMedicalRecord_PatientNotFound() {
        /* Prepare data */
        LocalDateTime now = LocalDateTime.now();

        // New medical record
        MedicalRecordParamDTO param = new MedicalRecordParamDTO();
        param.setDoctorId(1);
        param.setPatientId(1);
        param.setAppointment(LocalDate.now());
        param.setTreatmentType(TreatmentType.CLINIC);
        param.setWorkingScheduleId(1);

        // Establishment
        Establishment clinic = new Establishment();
        clinic.setId(1);
        clinic.setName("Clinic");
        clinic.setAverageTime(10);

        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setName("Doctor");
        doctor.setEstablishment(clinic);

        /* Mock */
        Mockito.when(accountRepository.findDoctorAccountById(param.getDoctorId())).thenReturn(Optional.of(doctor));
        Mockito.when(accountRepository.findPatientAccountByPatientId(param.getPatientId())).thenReturn(Optional.empty());

        Condition<MedicException> patientNotFound = new Condition<>(m -> MedicException.ERROR_PATIENT_NOT_EXIST.equals(m.getMessageCode()), "ERROR_PATIENT_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createMedicalRecord(param);
                }).has(patientNotFound);
    }

    @Test
    public void test_createMedicalRecord_WorkingScheduleNotFound() {
        /* Prepare data */
        LocalDateTime now = LocalDateTime.now();

        // New medical record
        MedicalRecordParamDTO param = new MedicalRecordParamDTO();
        param.setDoctorId(1);
        param.setPatientId(1);
        param.setAppointment(LocalDate.now());
        param.setTreatmentType(TreatmentType.CLINIC);
        param.setWorkingScheduleId(1);

        // Establishment
        Establishment clinic = new Establishment();
        clinic.setId(1);
        clinic.setName("Clinic");
        clinic.setAverageTime(10);

        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setName("Doctor");
        doctor.setEstablishment(clinic);

        // Doctor registry treatment service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(1);
        MedicalServiceRegistry serviceRegistry = new MedicalServiceRegistry();
        serviceRegistry.setMedicalService(medicalService);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        patient.setName("Patient");

        Account patientAcc = new Account();
        patientAcc.setId(2);
        patientAcc.setName("Patient");
        patientAcc.setPatient(patient);

        /* Mock */
        Mockito.when(accountRepository.findDoctorAccountById(param.getDoctorId())).thenReturn(Optional.of(doctor));
        Mockito.when(accountRepository.findPatientAccountByPatientId(param.getPatientId())).thenReturn(Optional.of(patientAcc));
        Mockito.when(establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(param.getWorkingScheduleId())).thenReturn(Optional.empty());

        Condition<MedicException> workingScheduleNotFound = new Condition<>(m -> MedicException.ERROR_WORKING_SCHEDULE_NOT_EXIST.equals(m.getMessageCode()), "ERROR_WORKING_SCHEDULE_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createMedicalRecord(param);
                }).has(workingScheduleNotFound);
    }

    @Test
    public void test_createMedicalRecord_DiffEstablishment() {
        /* Prepare data */
        LocalDateTime now = LocalDateTime.now();

        // New medical record
        MedicalRecordParamDTO param = new MedicalRecordParamDTO();
        param.setDoctorId(1);
        param.setPatientId(1);
        param.setAppointment(LocalDate.now());
        param.setTreatmentType(TreatmentType.CLINIC);
        param.setWorkingScheduleId(1);

        // Establishment
        Establishment clinic = new Establishment();
        clinic.setId(1);
        clinic.setName("Clinic");
        clinic.setAverageTime(10);

        Establishment clinic2 = new Establishment();
        clinic.setId(2);
        clinic.setName("Clinic 2");
        clinic.setAverageTime(10);

        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setName("Doctor");
        doctor.setEstablishment(clinic);

        // Doctor registry treatment service
        MedicalService medicalService = new MedicalService();
        medicalService.setId(1);
        MedicalServiceRegistry serviceRegistry = new MedicalServiceRegistry();
        serviceRegistry.setMedicalService(medicalService);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        patient.setName("Patient");

        Account patientAcc = new Account();
        patientAcc.setId(2);
        patientAcc.setName("Patient");
        patientAcc.setPatient(patient);

        // Establishment working schedule
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setEstablishment(clinic2);
        workingSchedule.setDayOfWeek(Day.convert(now.getDayOfWeek()));
        workingSchedule.setStartTime(now.toLocalTime().plusHours(1));
        workingSchedule.setEndTime(now.toLocalTime().plusHours(5));

        // List working schedule of establishment
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);


        /* Mock */
        Mockito.when(accountRepository.findDoctorAccountById(param.getDoctorId())).thenReturn(Optional.of(doctor));
        Mockito.when(accountRepository.findPatientAccountByPatientId(param.getPatientId())).thenReturn(Optional.of(patientAcc));
        Mockito.when(establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(param.getWorkingScheduleId())).thenReturn(Optional.of(workingSchedule));
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(workingSchedule.getEstablishment().getId())).thenReturn(workingSchedules);

        Condition<MedicException> diffEstablishment = new Condition<>(m -> MedicException.ERROR_WORKING_SCHEDULE_NOT_BELONG_DOCTOR.equals(m.getMessageCode()), "ERROR_WORKING_SCHEDULE_NOT_BELONG_DOCTOR");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createMedicalRecord(param);
                }).has(diffEstablishment);
    }

    @Test
    public void test_createMedicalRecord_DoctorNotRegistryTreatmentService() {
        /* Prepare data */
        LocalDateTime now = LocalDateTime.now();

        // New medical record
        MedicalRecordParamDTO param = new MedicalRecordParamDTO();
        param.setDoctorId(1);
        param.setPatientId(1);
        param.setAppointment(LocalDate.now());
        param.setTreatmentType(TreatmentType.CLINIC);
        param.setWorkingScheduleId(1);

        // Establishment
        Establishment clinic = new Establishment();
        clinic.setId(1);
        clinic.setName("Clinic");
        clinic.setAverageTime(10);

        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setName("Doctor");
        doctor.setEstablishment(clinic);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        patient.setName("Patient");

        Account patientAcc = new Account();
        patientAcc.setId(2);
        patientAcc.setName("Patient");
        patientAcc.setPatient(patient);

        // Establishment working schedule
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setEstablishment(clinic);
        workingSchedule.setDayOfWeek(Day.convert(now.getDayOfWeek()));
        workingSchedule.setStartTime(now.toLocalTime().plusHours(1));
        workingSchedule.setEndTime(now.toLocalTime().plusHours(5));

        // List working schedule of establishment
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        /* Mock */
        Mockito.when(accountRepository.findDoctorAccountById(param.getDoctorId())).thenReturn(Optional.of(doctor));
        Mockito.when(accountRepository.findPatientAccountByPatientId(param.getPatientId())).thenReturn(Optional.of(patientAcc));
        Mockito.when(establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(param.getWorkingScheduleId())).thenReturn(Optional.of(workingSchedule));
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(workingSchedule.getEstablishment().getId())).thenReturn(workingSchedules);
        Mockito.when(medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_MEDICAL)).thenReturn(Optional.empty());

        Condition<MedicException> notRegistryTreatmentService = new Condition<>(m -> MedicException.ERROR_DOCTOR_NOT_REGISTRY_TREATMENT_SERVICE.equals(m.getMessageCode()), "ERROR_DOCTOR_NOT_REGISTRY_TREATMENT_SERVICE");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createMedicalRecord(param);
                }).has(notRegistryTreatmentService);
    }

    @Test
    public void test_createMedicalRecord_DoctorNotRegistryTransportService() {
        /* Prepare data */
        LocalDateTime now = LocalDateTime.now();

        // New medical record
        MedicalRecordParamDTO param = new MedicalRecordParamDTO();
        param.setDoctorId(1);
        param.setPatientId(1);
        param.setAppointment(LocalDate.now());
        param.setTreatmentType(TreatmentType.HOME);
        param.setWorkingScheduleId(1);

        // Establishment
        Establishment clinic = new Establishment();
        clinic.setId(1);
        clinic.setName("Clinic");
        clinic.setAverageTime(10);

        // Doctor
        Account doctor = new Account();
        doctor.setId(1);
        doctor.setName("Doctor");
        doctor.setEstablishment(clinic);

        // Doctor registry treatment service
        MedicalService treatmentService = new MedicalService();
        treatmentService.setId(Constants.SERVICE_TREATMENT_MEDICAL);
        MedicalServiceRegistry treatmentServiceRegistry = new MedicalServiceRegistry();
        treatmentServiceRegistry.setMedicalService(treatmentService);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        patient.setName("Patient");

        Account patientAcc = new Account();
        patientAcc.setId(2);
        patientAcc.setName("Patient");
        patientAcc.setPatient(patient);

        // Establishment working schedule
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setEstablishment(clinic);
        workingSchedule.setDayOfWeek(Day.convert(now.getDayOfWeek()));
        workingSchedule.setStartTime(now.toLocalTime().plusHours(1));
        workingSchedule.setEndTime(now.toLocalTime().plusHours(5));

        // List working schedule of establishment
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        /* Mock */
        Mockito.when(accountRepository.findDoctorAccountById(param.getDoctorId())).thenReturn(Optional.of(doctor));
        Mockito.when(accountRepository.findPatientAccountByPatientId(param.getPatientId())).thenReturn(Optional.of(patientAcc));
        Mockito.when(establishmentWorkingScheduleRepository.findByIdAndIsDeletedFalse(param.getWorkingScheduleId())).thenReturn(Optional.of(workingSchedule));
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(workingSchedule.getEstablishment().getId())).thenReturn(workingSchedules);
        Mockito.when(medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_MEDICAL)).thenReturn(Optional.of(treatmentServiceRegistry));
        Mockito.when(medicalServiceRegistryRepository.findByEstablishment_IdAndMedicalService_IdAndActiveTrue(doctor.getEstablishment().getId(), Constants.SERVICE_TREATMENT_TRANSPORT)).thenReturn(Optional.empty());

        Condition<MedicException> notRegistryTransportService = new Condition<>(m -> MedicException.ERROR_DOCTOR_NOT_REGISTRY_TRANSPORT_SERVICE.equals(m.getMessageCode()), "ERROR_DOCTOR_NOT_REGISTRY_TRANSPORT_SERVICE");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createMedicalRecord(param);
                }).has(notRegistryTransportService);
    }
}
