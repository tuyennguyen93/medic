package com.mmedic.rest.medicalRecord;

import com.mmedic.BaseTest;
import com.mmedic.config.prop.PaymentProperties;
import com.mmedic.entity.Establishment;
import com.mmedic.entity.EstablishmentWorkingSchedule;
import com.mmedic.entity.MedicalRecord;
import com.mmedic.entity.MedicalService;
import com.mmedic.entity.MedicalServiceOrder;
import com.mmedic.entity.MedicalServiceOrderDetail;
import com.mmedic.entity.Patient;
import com.mmedic.entity.PreclinicalRecord;
import com.mmedic.enums.PaymentStatus;
import com.mmedic.rest.account.repo.AccountRepository;
import com.mmedic.rest.doctor.DoctorService;
import com.mmedic.rest.doctor.repo.DoctorRepository;
import com.mmedic.rest.establishment.repo.EstablishmentRepository;
import com.mmedic.rest.establishment.repo.EstablishmentWorkingScheduleRepository;
import com.mmedic.rest.medicalRecord.dto.PreclinicalPriceQueryDTO;
import com.mmedic.rest.medicalRecord.impl.MedicalRecordServiceImpl;
import com.mmedic.rest.medicalRecord.repo.ExamineRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalRecordRepository;
import com.mmedic.rest.medicalRecord.repo.MedicalServiceOrderDetailRepository;
import com.mmedic.rest.medicalRecord.repo.ServiceRecordRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderDiscussionRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceOrderRepository;
import com.mmedic.rest.medicalService.repo.MedicalServiceRegistryRepository;
import com.mmedic.rest.notification.NotificationService;
import com.mmedic.service.payment.zalopay.CreateOrder;
import com.mmedic.spring.errors.MedicException;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class CreateOrderPaymentPreclinicalTest extends BaseTest {

    @Autowired
    private PaymentProperties paymentProperties;

    @MockBean
    private MedicalRecordRepository medicalRecordRepository;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private EstablishmentWorkingScheduleRepository establishmentWorkingScheduleRepository;

    @MockBean
    private MedicalServiceRegistryRepository medicalServiceRegistryRepository;

    @MockBean
    private MedicalServiceOrderRepository medicalServiceOrderRepository;

    @MockBean
    private MedicalServiceOrderDetailRepository medicalServiceOrderDetailRepository;

    @MockBean
    private ExamineRecordRepository examineRecordRepository;

    @MockBean
    private DoctorService doctorService;

    @MockBean
    private DoctorRepository doctorRepository;

    @MockBean
    private EstablishmentRepository establishmentRepository;

    @MockBean
    private ServiceRecordRepository serviceRecordRepository;

    @MockBean
    private MedicalServiceOrderDiscussionRepository medicalServiceOrderDiscussionRepository;

    @MockBean
    private NotificationService notificationService;


    private CreateOrder createOrder;

    private MedicalRecordService medicalRecordService;

    @Before
    public void setUp() {
        createOrder = new CreateOrder(paymentProperties);
        medicalRecordService = new MedicalRecordServiceImpl(medicalRecordRepository, accountRepository,
                establishmentWorkingScheduleRepository, medicalServiceRegistryRepository, medicalServiceOrderRepository,
                medicalServiceOrderDetailRepository, examineRecordRepository, doctorService, doctorRepository, establishmentRepository,
                serviceRecordRepository, medicalServiceOrderDiscussionRepository, createOrder, notificationService);
    }

    @Test
    public void test_createPaymentOrderPreclinical() {
        int medicalRecordId = 1;
        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setAverageTime(10);
        establishment.setId(1);

        // Preclinical Record
        PreclinicalRecord preclinicalRecord = new PreclinicalRecord();
        preclinicalRecord.setId(1);
        medicalRecordExamine.setPreclinicalRecord(preclinicalRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        preclinicalRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPreclinicalRecord(preclinicalRecord);

        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setAppointmentDate(LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 0)));
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setSequenceNumber(10001);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        // Preclinical Service
        MedicalService preclinicalService = new MedicalService();
        preclinicalService.setId(6);
        preclinicalService.setName("Xét Nghiệm");
        serviceOrderDetail.setMedicalService(preclinicalService);

        // List working time
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setStartTime(LocalTime.of(7, 0));
        workingSchedule.setEndTime(LocalTime.of(20, 0));
        workingSchedule.setEstablishment(establishment);
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        // Now
        LocalDateTime now = LocalDateTime.now();

        // Price
        PreclinicalPriceQueryDTO price = new PreclinicalPriceQueryDTO() {
            @Override
            public int getId() {
                return 1;
            }

            @Override
            public Integer getServiceId() {
                return 6;
            }

            @Override
            public Integer getPrice() {
                return 10000;
            }
        };
        List<PreclinicalPriceQueryDTO> prices = Collections.singletonList(price);

        LocalDateTime appointmentTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 0));

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(establishment.getId())).thenReturn(workingSchedules);
        Mockito.when(establishmentRepository.establishmentIsOff(appointmentTime, establishment.getId())).thenReturn(0);
        Mockito.when(medicalRecordRepository.findPreclinicalPrice(Mockito.eq(medicalRecordId), Mockito.any())).thenReturn(prices);


        assertThat(medicalRecordService.createPaymentOrderPreclinical(medicalRecordId)).isNotNull();
        assertThat(serviceOrder.getPaymentReference()).isNotNull();
    }

    @Test
    public void test_createPaymentOrderPreclinical_NotFoundMedicalRecord() {
        int medicalRecordId = 1;
        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> notFound = new Condition<>(m -> MedicException.ERROR_MEDICAL_RECORD_NOT_EXIST.equals(m.getMessageCode()), "ERROR_MEDICAL_RECORD_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPreclinical(2);
                }).has(notFound);
    }

    @Test
    public void test_createPaymentOrderPreclinical_HasPaid() {
        int medicalRecordId = 1;
        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Preclinical Record
        PreclinicalRecord preclinicalRecord = new PreclinicalRecord();
        preclinicalRecord.setId(1);
        medicalRecordExamine.setPreclinicalRecord(preclinicalRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.SUCCESS);
        preclinicalRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPreclinicalRecord(preclinicalRecord);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> hasPaid = new Condition<>(m -> MedicException.ERROR_SERVICE_ORDER_HAS_PAID.equals(m.getMessageCode()), "ERROR_SERVICE_ORDER_HAS_PAID");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPreclinical(medicalRecordId);
                }).has(hasPaid);
    }

    @Test
    public void test_createPaymentOrderPreclinical_OrderDetailNotExist() {
        int medicalRecordId = 1;
        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Preclinical Record
        PreclinicalRecord preclinicalRecord = new PreclinicalRecord();
        preclinicalRecord.setId(1);
        medicalRecordExamine.setPreclinicalRecord(preclinicalRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        preclinicalRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPreclinicalRecord(preclinicalRecord);

        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> orderDetailNotExist = new Condition<>(m -> MedicException.ERROR_ORDER_DETAIL_NOT_EXIST.equals(m.getMessageCode()), "ERROR_ORDER_DETAIL_NOT_EXIST");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPreclinical(medicalRecordId);
                }).has(orderDetailNotExist);
    }

    @Test
    public void test_createPaymentOrderPreclinical_AppointmentDateEmpty() {
        int medicalRecordId = 1;
        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setId(1);

        // Preclinical Record
        PreclinicalRecord preclinicalRecord = new PreclinicalRecord();
        preclinicalRecord.setId(1);
        medicalRecordExamine.setPreclinicalRecord(preclinicalRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        preclinicalRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPreclinicalRecord(preclinicalRecord);

        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        // Preclinical Service
        MedicalService preclinicalService = new MedicalService();
        preclinicalService.setId(6);
        preclinicalService.setName("Xét Nghiệm");
        serviceOrderDetail.setMedicalService(preclinicalService);

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));

        Condition<MedicException> appointmentDateEmpty = new Condition<>(m -> MedicException.ERROR_APPOINTMENT_DATE_EMPTY.equals(m.getMessageCode()), "ERROR_APPOINTMENT_DATE_EMPTY");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPreclinical(medicalRecordId);
                }).has(appointmentDateEmpty);
    }

    @Test
    public void test_createPaymentOrderPreclinical_PreclinicalOff() {
        int medicalRecordId = 1;
        // Prepare data
        MedicalRecord medicalRecordExamine = new MedicalRecord();
        medicalRecordExamine.setId(medicalRecordId);

        // Patient
        Patient patient = new Patient();
        patient.setId(1);
        medicalRecordExamine.setPatient(patient);

        // Establishment
        Establishment establishment = new Establishment();
        establishment.setAverageTime(10);
        establishment.setId(1);

        // Preclinical Record
        PreclinicalRecord preclinicalRecord = new PreclinicalRecord();
        preclinicalRecord.setId(1);
        medicalRecordExamine.setPreclinicalRecord(preclinicalRecord);

        // Medical Service Order
        MedicalServiceOrder serviceOrder = new MedicalServiceOrder();
        serviceOrder.setId(1);
        serviceOrder.setPaymentStatus(PaymentStatus.PENDING);
        preclinicalRecord.setMedicalServiceOrder(serviceOrder);
        serviceOrder.setPreclinicalRecord(preclinicalRecord);

        // Medical Service Order Detail
        MedicalServiceOrderDetail serviceOrderDetail = new MedicalServiceOrderDetail();
        serviceOrderDetail.setId(1);
        serviceOrderDetail.setAppointmentDate(LocalDateTime.of(LocalDate.now(), LocalTime.of(8, 0)));
        serviceOrderDetail.setEstablishment(establishment);
        serviceOrderDetail.setSequenceNumber(10001);
        serviceOrder.setMedicalServiceOrderDetails(Collections.singletonList(serviceOrderDetail));

        // Preclinical Service
        MedicalService preclinicalService = new MedicalService();
        preclinicalService.setId(6);
        preclinicalService.setName("Xét Nghiệm");
        serviceOrderDetail.setMedicalService(preclinicalService);

        // List working time
        EstablishmentWorkingSchedule workingSchedule = new EstablishmentWorkingSchedule();
        workingSchedule.setId(1);
        workingSchedule.setStartTime(LocalTime.of(7, 0));
        workingSchedule.setEndTime(LocalTime.of(20, 0));
        workingSchedule.setEstablishment(establishment);
        List<EstablishmentWorkingSchedule> workingSchedules = Collections.singletonList(workingSchedule);

        LocalDateTime appointmentTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 0));

        // Mock up
        Mockito.when(medicalRecordRepository.findById(medicalRecordId)).thenReturn(Optional.of(medicalRecordExamine));
        Mockito.when(establishmentWorkingScheduleRepository.findAllByEstablishment_IdAndIsDeletedFalseOrderById(establishment.getId())).thenReturn(workingSchedules);
        Mockito.when(establishmentRepository.establishmentIsOff(appointmentTime, establishment.getId())).thenReturn(1);

        Condition<MedicException> appointmentDateEmpty = new Condition<>(m -> MedicException.ERROR_ESTABLISHMENT_IS_OFF.equals(m.getMessageCode()), "ERROR_ESTABLISHMENT_IS_OFF");
        assertThatExceptionOfType(MedicException.class).
                isThrownBy(() -> {
                    medicalRecordService.createPaymentOrderPreclinical(medicalRecordId);
                }).has(appointmentDateEmpty);
    }
}
